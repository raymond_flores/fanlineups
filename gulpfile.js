var gulp = require('gulp');
var minify = require('gulp-minify');
var uglify = require('gulp-uglify');
var nodemon = require('gulp-nodemon');

gulp.task('default', ['compress', 'start', 'watch']);

gulp.task('watch', function() {
  gulp.watch(['www/controllers/**/*.js', 'www/directives/**/*.js'], ['compress']);
});

gulp.task('compress', function() {
  gulp.src(['www/controllers/**/*.js', 'www/directives/**/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('www/dist'))
});

gulp.task('start', function () {
  nodemon({ 
  		script: './bin/www', 
  		ext: 'html js',
  		watch: ['app.js', 'routes/*.js']
	})
    .on('restart', function () {
      console.log('restarted!')
    });
});