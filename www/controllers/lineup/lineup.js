define(['angularAMD', 'jquery', 'semantic-ui', 'playerTable', 'playerDetailModal', 'playerLineup', 'sweetAlert'], function (angularAMD) {
    angularAMD.controller('lineupController',['$scope', '$state', '$http', '$rootScope', '$filter', function ($scope, $state, $http, $rootScope, $filter) {
    	// init tab views
        $('#positionTabs .item').tab();
        $('.dropdown').dropdown();

        // init view
        $scope.salaryData = 'DK';
        $scope.projectionData = 'ESPN';

        // get player data
        getData($scope.salaryData, $scope.projectionData, function (err) {
            if (!err)
                initLineup();
        });

        angular.extend($scope, {
            changeSalaryData: function (source) {
                if (source !== $scope.salaryData) {
                    // get salary data
                }
            },
            changeProjectionData: function (source) {
                if (source !== $scope.projectionData) {
                    // get projection data
                    getData($scope.salaryData, source, function (err) {
                        if (!err) {
                            $scope.projectionData = source;
                            initLineup();
                        }
                    });
                }
            },
            optimizeLineup: function () {
                // sort for knapsack algorithm
                $rootScope.players.sort ( function (a, b) {
                    return b.dk_value - a.dk_value;
                });

                // get locked players
                var keys = Object.keys($rootScope.lineup);
                var lockedPlayers = [];

                for (var key = 0, len = keys.length; key < len; key++) {
                    if ($rootScope.lineup[keys[key]].locked)
                        lockedPlayers.push($rootScope.lineup[keys[key]]);
                }

                var team = optimizedLineup(lockedPlayers);
                createLineup(team);
            },
            filterByFlex: function (elem) {
                return ['RB', 'WR', 'TE'].indexOf(elem.position) !== -1;
            },
            exportToCSV: function () {
                var keys = Object.keys($rootScope.lineup);
                //var dataHeader = "QB,RB,RB,WR,WR,WR,TE,FLEX,D\n";
                var data = "";
                var csvContent = "data:text/csv;charset=utf-8,";

                data = $rootScope.lineup.qb.name + "," + $rootScope.lineup.rb1.name + "," + $rootScope.lineup.rb2.name + "," + $rootScope.lineup.wr1.name + "," + $rootScope.lineup.wr2.name + "," + $rootScope.lineup.wr3.name + "," + $rootScope.lineup.te.name + "," + $rootScope.lineup.flex.name + "," + $rootScope.lineup.dst.name + "\n";

                csvContent = csvContent + data;
                var encodedUri = encodeURI(csvContent);
                window.open(encodedUri);
            },
            saveLineup: function () {
                $http.post('/players/lineups', $rootScope.lineup)
                    .success( function (data) {
                        swal("Saved!", "Your Lineup Has Been Saved!", "success")
                    })
                    .error( function (data) {
                        console.log('ERR');
                    });
            }
        });

        $rootScope.$watch('lineup', function (newVal, oldVal) {
            $rootScope.optimizable = lineupCompare($rootScope.saveLineup, newVal);
            var keys = Object.keys($rootScope.lineup  || []);
            $scope.lineupTotal = 0;
            $scope.remainingSalary = 50000;

            for (var key = 0, len = keys.length; key < len; key++) {
                $scope.lineupTotal += parseFloat($rootScope.lineup[keys[key]].dk_total);
                $scope.remainingSalary -= parseFloat($rootScope.lineup[keys[key]].salary);
            }
        }, true);

        function getData (salSrc, proSrc, callback)  {
            $scope.loading = true;
            // if (!localStorage[salSrc + proSrc])
            //     $scope.loading = true;
            // else
            //     $rootScope.players = JSON.parse(localStorage[salSrc + proSrc]);
            $http.get('/players/list?salaries=' + salSrc + '&projections=' + proSrc)
                .success( function (data) {
                    $scope.loading = false;

                    $rootScope.players = data;
                    // localStorage[salSrc + proSrc] = JSON.stringify(data);

                    callback(null);
                })
                .error( function (data) {
                    console.log('ERROR: ' + data);
                    callback('ERR');
                });
        }

        function initLineup () {
            $rootScope.lineup = {};
            var team = optimizedLineup();

            createLineup(team);
        };

        function createLineup (team) {
            var rbCount = 0;
            var wrCount = 0;
            var teCount = 0;

            for (var i = 0, len = team.length; i < len; i++) {
                if (team[i].position === 'RB') {
                    if (rbCount === 2) {
                        if ($rootScope.lineup.flex && $rootScope.lineup.flex.included) 
                            $rootScope.lineup.flex.included = false;
                        $rootScope.lineup.flex = team[i];
                        continue;
                    }
                    if ($rootScope.lineup['rb' + (rbCount + 1)] && $rootScope.lineup['rb' + (rbCount + 1)].included)
                        $rootScope.lineup['rb' + (rbCount + 1)].included = false;
                    $rootScope.lineup['rb' + (rbCount + 1)] = team[i];
                    rbCount++;
                }
                else if (team[i].position === 'WR') {
                    if (wrCount === 3) {
                        if ($rootScope.lineup.flex && $rootScope.lineup.flex.included) 
                            $rootScope.lineup.flex.included = false;
                        $rootScope.lineup.flex = team[i];
                        continue;
                    }
                    if ($rootScope.lineup['wr' + (wrCount + 1)] && $rootScope.lineup['wr' + (wrCount + 1)].included)
                        $rootScope.lineup['wr' + (wrCount + 1)].included = false;
                    $rootScope.lineup['wr' + (wrCount + 1)] = team[i];
                    wrCount ++;
                }
                else if (team[i].position === 'TE') {
                    if (teCount === 1) {
                        if ($rootScope.lineup.flex && $rootScope.lineup.flex.included) 
                            $rootScope.lineup.flex.included = false;
                        $rootScope.lineup.flex = team[i];
                        continue;
                    }
                    if ($rootScope.lineup.te && $rootScope.lineup.te.included)
                        $rootScope.lineup.te.included = false;
                    $rootScope.lineup.te = team[i];
                    teCount ++;
                }
                else {
                    if ($rootScope.lineup[team[i].position.toLowerCase()] && $rootScope.lineup[team[i].position.toLowerCase()].included)
                        $rootScope.lineup[team[i].position.toLowerCase()].included = false;
                    $rootScope.lineup[team[i].position.toLowerCase()] = team[i];
                }
            }

            for (var i = 0, len = team.length; i < len; i++) {
                team[i].included = true;
            }

            $rootScope.saveLineup = angular.copy($rootScope.lineup)
        };

        function optimizedLineup (lockedPlayers) {
            // using knapsack algorithm
            var budget = 50000;
            var current_salary = 0;
            var constraints = {
                'QB': 1,
                'RB': 2,
                'WR': 3,
                'TE': 1,
                'DST': 1,
                'FLEX': 1
            };
            var counts = {
                'QB': 0,
                'RB': 0,
                'WR': 0,
                'TE': 0,
                'DST': 0,
                'FLEX': 0
            };

            if (lockedPlayers) {
                var team = lockedPlayers;
                for (var l in lockedPlayers) {
                    current_salary += lockedPlayers[l].salary;

                    if (counts[lockedPlayers[l].position] < constraints[lockedPlayers[l].position])
                        counts[lockedPlayers[l].position]++;
                    else
                        counts['FLEX']++; 
                }
            }
            else {
                var team = [];
            }

            for (var i in $rootScope.players) {
                if (!$rootScope.players[i].excluded && !$rootScope.players[i].included) {
                    var pos = $rootScope.players[i].position;
                    var sal = $rootScope.players[i].salary;

                    if (counts[pos] < constraints[pos] && current_salary + sal <= budget) {
                        team.push($rootScope.players[i]);
                        counts[pos]++;
                        current_salary += sal;
                    }
                    else if (counts['FLEX'] < constraints['FLEX'] && current_salary + sal <= budget && ['RB','WR','TE'].indexOf(pos) !== -1) {
                        team.push($rootScope.players[i]);
                        counts['FLEX']++;
                        current_salary += sal;
                    }
                }
            }

            $rootScope.players.sort( function (a, b) {
                return b.dk_total - a.dk_total;
            });

            for (var j in $rootScope.players) {
                if (!$rootScope.players[j].excluded) {
                    var pos = $rootScope.players[j].position;
                    var sal = $rootScope.players[j].salary;
                    var pts = parseFloat($rootScope.players[j].dk_total);

                    if (team.indexOf($rootScope.players[j]) === -1) {
                        var pos_players = team.filter( function (player) {
                            return player.position === pos && !player.locked;
                        })
                        pos_players.sort( function (a, b) {
                            return a.dk_total - b.dk_total;
                        });

                        for (var k in pos_players) {
                            if (((current_salary + sal - pos_players[k].salary) <= budget) && (pts > parseFloat(pos_players[k].dk_total))) {
                                team[team.indexOf(pos_players[k])] = $rootScope.players[j];
                                current_salary = current_salary + sal - pos_players[k].salary;
                                break;
                            }
                        }
                    }
                }
            }

            $rootScope.optimizable = false;
            return team;
        };

        function lineupCompare (obj1, obj2) {
            if (!obj1 || !obj2)
                return false;

            var keys1 = Object.keys(obj1);
            var keys2 = Object.keys(obj2);

            for (var i = 0; i < keys1.length; i++) {
                if (Boolean(obj1[keys1[i]].excluded) !== Boolean(obj2[keys2[i]].excluded) || Boolean(obj1[keys1[i]].locked) !== Boolean(obj2[keys2[i]].locked) || obj1[keys1[i]].cbs_id !== obj2[keys2[i]].cbs_id)
                    return true;
            }

            return false;
        }
    }]);
});