define(['angularAMD', 'jquery', 'semantic-ui',], function (angularAMD) {
    angularAMD.controller('fantasyNewsController',['$scope', '$state', '$rootScope', '$http', function ($scope, $state, $rootScope, $http) {
        $http.get('/players/news?limit=10')
            .success( function (data) {
                $scope.news = data;
            });
    }]);
});