define(['angularAMD', 'jquery', 'semantic-ui',], function (angularAMD) {
    angularAMD.controller('fantasyStoryController', ['$scope', '$state', '$rootScope', '$http', '$stateParams', '$sce', function ($scope, $state, $rootScope, $http, $stateParams, $sce) {
        $http.get('/players/story/' + $stateParams.id)
            .success(function (data) {
                $scope.story = data;
                $scope.story.story_body = $sce.trustAsHtml(data.story_body);
            });
    }]);
});