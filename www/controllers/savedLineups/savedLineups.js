define(['angularAMD', 'jquery', 'semantic-ui',], function (angularAMD) {
    angularAMD.controller('savedLineupsController',['$scope', '$state', '$rootScope', '$http', function ($scope, $state, $rootScope, $http) {
    	$('.dropdown').dropdown();

    	$http.get('/players/lineups')
    		.success( function (data) {
    			$scope.lineups = data.lineups;
    			$scope.mappedData = data.data;
    		});

    	$scope.exportToCSV = function () {
    		var data = "";
    		var csvContent = "data:text/csv;charset=utf-8,";

    		for (var i in $scope.lineups) {
    			for (var count = 0, len = $scope.lineups[i].count; count < len; count++) {
    				data += $scope.mappedData[$scope.lineups[i].qb].id + "," + $scope.mappedData[$scope.lineups[i].rb1].id + "," + $scope.mappedData[$scope.lineups[i].rb2].id + "," + $scope.mappedData[$scope.lineups[i].wr1].id + "," + $scope.mappedData[$scope.lineups[i].wr2].id + "," + $scope.mappedData[$scope.lineups[i].wr3].id + "," + $scope.mappedData[$scope.lineups[i].te].id + "," + $scope.mappedData[$scope.lineups[i].flex].id + "," + $scope.mappedData[$scope.lineups[i].dst].id + "\n";
    			}
    		}
            csvContent = csvContent + data;
            var encodedUri = encodeURI(csvContent);
            window.open(encodedUri);
        };
    }]);
});