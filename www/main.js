require.config({
    baseUrl: "./",
    paths: {
        'angular': '/lib/javascripts/angular/angular.min',
        'angularAMD': '/lib/javascripts/angularAMD/angularAMD.min',
        'angular-ui-router': '/lib/javascripts/angular-ui-router/release/angular-ui-router.min',
        'jquery': 'lib/javascripts/jquery/dist/jquery.min',
        'semantic-ui': '/lib/javascripts/semantic/dist/semantic.min',
        'sweetAlert': '/lib/javascripts/sweetalert/dist/sweetalert.min',
        'playerTable': '/dist/playerTable/playerTable',
        'playerDetailModal': '/dist/playerDetailModal/playerDetailModal',
        'playerLineup': '/dist/playerLineup/playerLineup',
        'selectedPlayer': '/dist/selectedPlayer/selectedPlayer',
        'unselectedPlayer': '/dist/unselectedPlayer/unselectedPlayer',
        'loginModal': '/dist/loginModal/loginModal',
        'swapPlayer': '/dist/swapPlayer/swapPlayer'
    },
    shim: {
        "angular": {exports: "angular"},
        'angular-ui-router': ['angular'],
        'semantic-ui': ['jquery']
    },
    deps: ['app']
});