define(['angularAMD', 'selectedPlayer', 'unselectedPlayer', 'swapPlayer'], function (angularAMD) {
  angularAMD.directive("playerLineup", ['$state', '$rootScope', function ($state, $rootScope) {
    return {
    	restrict: "E",
    	templateUrl: '/directives/playerLineup/playerLineup.html',
    	link: function (scope, element, attr) {
           angular.extend(scope, {
           	swapPlayer: function (position, player) {
           		$rootScope.lineup[position].included = false;
           		player.included = true;
           		$rootScope.lineup[position] = player;
           		$rootScope[position].swap = false;
           	}
           });
    	}
    };
  }]);
});