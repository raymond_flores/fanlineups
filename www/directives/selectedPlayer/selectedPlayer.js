define(['angularAMD'], function (angularAMD) {
  angularAMD.directive("selectedPlayer", ['$state', '$rootScope', '$filter', function ($state, $rootScope, $filter) {
    return {
    	restrict: "E",
        replace: true,
        scope: {
        	player: '=',
        	position: '='
        },
    	templateUrl: '/directives/selectedPlayer/selectedPlayer.html',
    	link: function (scope, element, attr) {
    		angular.extend(scope, {
    			removePlayer: function () {
                    $rootScope.lineup[scope.position.toLowerCase()].excluded = !$rootScope.lineup[scope.position.toLowerCase()].excluded;
                    $rootScope.lineup[scope.position.toLowerCase()].included = !$rootScope.lineup[scope.position.toLowerCase()].included;

                    if ($rootScope.lineup[scope.position.toLowerCase()].locked)
                        $rootScope.lineup[scope.position.toLowerCase()].locked = false;

                    // var position = scope.position;
                    // if (position.slice(0, 2) === 'RB' || position.slice(0, 2) === 'WR')
                    //     position = position.slice(0,2);

                    // if (position === 'FLEX')
                    //     var players = $filter('filter')($rootScope.players, function (player) {
                    //         return (['RB', 'WR', 'TE'].indexOf(player.position) !== -1);
                    //     });
                    // else
                    //     var players = $filter('filter')($rootScope.players, { position: position });

                    // for (var i = 0, len = players.length; i < len; i++) {
                    //     if (!players[i].excluded && !players[i].included) {
                    //         $rootScope.lineup[scope.position.toLowerCase()] = players[i];
                    //         $rootScope.lineup[scope.position.toLowerCase()].included = true;
                    //         break;
                    //     }
                    // }
    			},
    			openModal: function (player) {
    			    $rootScope.currentPlayer = player;

    			    $('#playerDetailModal').modal('show');
    			},
                lockPlayer: function () {
                    $rootScope.lineup[scope.position.toLowerCase()].locked = !$rootScope.lineup[scope.position.toLowerCase()].locked;
                },
                showSwappablePlayers: function () {
                    if (!$rootScope[scope.position.toLowerCase()])
                        $rootScope[scope.position.toLowerCase()] = {};

                    var position = scope.position;
                    if (position.slice(0, 2) === 'RB' || position.slice(0, 2) === 'WR')
                        position = position.slice(0,2);

                    if (position === 'FLEX')
                        var players = $filter('filter')($rootScope.players, function (player) {
                            return (['RB', 'WR', 'TE'].indexOf(player.position) !== -1);
                        });
                    else
                        var players = $filter('filter')($rootScope.players, { position: position });

                    var count = 0;
                    $rootScope[scope.position.toLowerCase()].swapPlayers = [];

                    for (var i = 0, len = players.length; count < 3; i++) {
                        if (!players[i].excluded && !players[i].included) {
                            $rootScope[scope.position.toLowerCase()].swapPlayers.push(players[i]);
                            count++;
                        }
                    }

                    $rootScope[scope.position.toLowerCase()].swap = !$rootScope[scope.position.toLowerCase()].swap;
                },
                calcTotal: function (player) {
                    return $rootScope.calcTotal(player);
                }
    		});
    	}
    };
  }]);
});