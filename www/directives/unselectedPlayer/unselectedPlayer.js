define(['angularAMD'], function (angularAMD) {
  angularAMD.directive("unselectedPlayer", ['$state', '$rootScope', function ($state, $rootScope) {
    return {
    	restrict: "E",
        replace: true,
    	templateUrl: '/directives/unselectedPlayer/unselectedPlayer.html',
    	link: function (scope, element, attr) {
    	}
    };
  }]);
});