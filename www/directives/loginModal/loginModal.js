define(['angularAMD'], function (angularAMD) {
  angularAMD.directive("loginModal", ['$state', '$rootScope', '$http', function ($state, $rootScope, $http) {
    return {
    	restrict: "E",
    	templateUrl: '/directives/loginModal/loginModal.html',
    	link: function (scope, element, attr) {
    		// init player detail modal
    		$('#loginModal').modal();

            scope.signIn = function () {
                $http.post('/auth/login', scope.form)
                    .success( function (data) {
                        $('#loginModal').modal('hide');
                        $state.go('lineup');
                    })
                    .error( function (data) {
                        scope.loginErr = "Wrong/Username Password.";
                    });
            }

            scope.signUp = function () {
                if (scope.form.password === scope.form.confirm_pw) {
                    $http.post('/auth/signup', scope.form)
                        .success( function (data) {
                            $('#loginModal').modal('hide');
                            $state.go('lineup');
                        })
                        .error( function (data) {
                            scope.signUpErr = 'Username Taken.';
                        });
                }
            }

    	}
    };
  }]);
});