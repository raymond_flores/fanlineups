define(['angularAMD'], function (angularAMD) {
  angularAMD.directive("playerTable", ['$state', '$rootScope', '$filter', function ($state, $rootScope, $filter) {
    return {
    	restrict: "E",
    	scope: {
    		filter: '=',
    		data: '=',
            position: '='
    	},
    	templateUrl: '/directives/playerTable/playerTable.html',
    	link: function (scope, element, attr) {
            var addToLineup = function (player, position) {
                if ($rootScope.lineup[position].locked && $rootScope.lineup[position] !== player)
                    $rootScope.lineup[position].locked = false;
                $rootScope.lineup[position].included = false;
                $rootScope.lineup[position] = player;
                $rootScope.lineup[position].included = true;
            };

            var getNextPlayer = function (position) {
                if (position === 'flex')
                    var players = $filter('filter')($rootScope.players, function (player) {
                        return (['RB', 'WR', 'TE'].indexOf(player.position) !== -1);
                    });
                else
                    var players = $filter('filter')($rootScope.players, { position: position });

                for (var i = 0, len = players.length; i < len; i++) {
                    if (!players[i].excluded && !players[i].included) {
                        $rootScope.lineup[position] = players[i];
                        $rootScope.lineup[position].included = true;
                        break;
                    }
                }
            }

            angular.extend(scope, {
                openModal: function (player) {
                    $rootScope.currentPlayer = player;
                    console.log(player);

                    $('#playerDetailModal').modal('show');
                },
                addPlayer: function (player) {
                    if (player.excluded)
                        player.excluded = false;

                    if (player.position === 'RB') {
                        if (!$rootScope.lineup.rb1.locked && !player.included) 
                            addToLineup(player, 'rb1');
                        else if (!$rootScope.lineup.rb2.locked && !player.included) 
                            addToLineup(player, 'rb2');
                    }
                    else if (player.position === 'WR') {
                        if (!$rootScope.lineup.wr1.locked && !player.included)
                            addToLineup(player, 'wr1');
                        else if (!$rootScope.lineup.wr2.locked && !player.included)
                            addToLineup(player, 'wr2');
                    }
                    else
                        addToLineup(player, player.position.toLowerCase());
                },
                removePlayer: function (player) {
                    player.excluded = !player.excluded;
                    if (player.included)
                        player.included = false;

                    if (player.locked)
                        player.locked = false;

                    $rootScope.optimizable = true;

                    // if (player.position === 'RB') {
                    //     if ($rootScope.lineup.rb1 === player)
                    //         getNextPlayer('rb1');
                    //     else if (!$rootScope.lineup.rb2 === player)
                    //         getNextPlayer('rb2');
                    // }
                    // else if (player.position === 'WR') {
                    //     if (!$rootScope.lineup.wr1 === player)
                    //         getNextPlayer('wr1');
                    //     else if (!$rootScope.lineup.wr2 === player)
                    //         getNextPlayer('wr2');
                    // }
                    // else if ($rootScope.lineup[player.position.toLowerCase()] === player)
                    //     getNextPlayer(player.position.toLowerCase());
                },
                lockPlayer: function (player) {
                    player.locked = !player.locked;
                },
                positionFilled: function () {
                    if ($rootScope.lineup) {
                        if (scope.position === 'RB')
                            return $rootScope.lineup.rb1.locked && $rootScope.lineup.rb2.locked;
                        else if (scope.position === 'WR')
                            return $rootScope.lineup.wr1.locked && $rootScope.lineup.wr2.locked && $rootScope.lineup.wr3.locked;
                        else
                            return $rootScope.lineup[scope.position.toLowerCase()].locked;
                    }
                },
                calcTotal: function (player) {
                    return $rootScope.calcTotal(player);
                }
            });
    	}
    };
  }]);
});