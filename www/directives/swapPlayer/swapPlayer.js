define(['angularAMD'], function (angularAMD) {
  angularAMD.directive("swapPlayer", ['$state', '$rootScope', function ($state, $rootScope) {
    return {
    	restrict: "E",
        replace: true,
        scope: {
        	position: '='
        },
    	templateUrl: '/directives/swapPlayer/swapPlayer.html',
    	link: function (scope, element, attr) {
    	}
    };
  }]);
});