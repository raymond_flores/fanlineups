define(['angularAMD'], function (angularAMD) {
  angularAMD.directive("playerDetailModal", ['$state', '$rootScope', function ($state, $rootScope) {
    return {
    	restrict: "E",
    	templateUrl: '/directives/playerDetailModal/playerDetailModal.html',
    	link: function (scope, element, attr) {
    		// init player detail modal
    		$('#playerDetailModal').modal();
    	}
    };
  }]);
});