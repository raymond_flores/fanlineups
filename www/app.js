define([
    'angularAMD',
    'angular-ui-router',
    'jquery',
    'semantic-ui'
], function (angularAMD) {
    var app = angular.module("webapp", ['ui.router']);

    app.run(function ($rootScope, $http, $state) {
        $rootScope.$on('$stateChangeStart', function () {
            $http.get('/auth/check')
                .success(function (data) {
                    $rootScope.signedIn = data ? true : false;
                    if (!data) {
                        $state.go('home');
                        // $('#loginModal').modal('show');
                    }
                    else {
                        $rootScope.user = {
                            name: data.name
                        };
                    }
                })
        });
    });

    app.config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', angularAMD.route({
                url: '/',
                templateUrl: 'controllers/home/home.html',
                controllerUrl: 'dist/home/home',
                controller: 'homeController'
            }))
            .state('lineup', angularAMD.route({
                url: '/lineup',
                templateUrl: 'controllers/lineup/lineup.html',
                controllerUrl: 'dist/lineup/lineup',
                controller: 'lineupController'
            }))
            .state('saved', angularAMD.route({
                url: '/saved_lineups',
                templateUrl: 'controllers/savedLineups/savedLineups.html',
                controllerUrl: 'dist/savedLineups/savedLineups',
                controller: 'savedLineupsController'
            }))
            .state('news', angularAMD.route({
                url: '/fantasy_headlines',
                templateUrl: 'controllers/fantasyNews/fantasyNews.html',
                controllerUrl: 'dist/fantasyNews/fantasyNews',
                controller: 'fantasyNewsController'
            })).state('story', angularAMD.route({
                url: '/fantasy_story/:id',
                templateUrl: 'controllers/fantasyStory/fantasyStory.html',
                controllerUrl: 'dist/fantasyStory/fantasyStory',
                controller: 'fantasyStoryController'
            }));

        $urlRouterProvider.otherwise("/");
    });
    angularAMD.bootstrap(app, false, document.getElementById('mainView'));
    return app;
});