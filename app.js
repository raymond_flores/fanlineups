var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');

var players = require('./routes/players');
var auth = require('./routes/auth');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'www')));

// required for passport
app.use(session({ secret: 't4Isi5A53cr3t' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

var checkAuth = function (req, res, next) {
  if (req.isAuthenticated())
    next();
  else
    res.send(401);
}

var denyCors = function (req, res, next) {
  var allowedSites = ['http://fanlineups.com/', 'http://localhost:3000/', 'https://www.facebook.com/'];
  if (allowedSites.indexOf(req.headers.referer) !== -1 || req.user && req.user.id === 1)
    next();
  else
    res.send(401);
}

app.use(denyCors);
app.use('/players', checkAuth, players);
app.use('/auth', auth);

app.get('/admin/dashboard', function (req, res) {
  if (req.user && req.user.id === 1)
    res.sendfile('./www/dashboard.html');
  else
    res.send(401);
});

app.get('*', function (req, res) {
  res.sendfile('./www/index.html')
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
