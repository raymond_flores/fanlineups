var pg = require('pg');
var conString = "postgres://raymond:rayflo33@localhost/lineup";

function query(queryString, args, callback) {
    pg.connect(conString, function (err, client, done) {
        if (err) {
            callback('error fetching client from pool' + err, null);
        }

        client.query(queryString, args, function (err, result) {
            //call `done()` to release the client back to the pool
            done();

            if (err) {
                callback('error running query' + err, null);
            }
            else
                callback(null, result);
        });

    });
}

module.exports = {
    query: query
};