// config/passport.js

// load all the things we need
// var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var LocalStrategy   = require('passport-local').Strategy;
var bcrypt = require('bcrypt');

// load the auth variables
var configAuth = require('./credentials');

// load db config
var pg = require('../config/dbConnection');

module.exports = function(passport) {

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        pg.query('SELECT * FROM users WHERE id = $1', [id], function (err, user) {
            done(err, user.rows[0]);
        });
    });
    
    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function (req, username, password, done) {
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            
            pg.query('SELECT EXISTS(SELECT 1 FROM users WHERE username = $1)', [username], function (err, response) {
                if (!err) {
                    if (response.rows[0].exists) {
                        return done(null, false, { message: 'Username Taken.'});
                    }
                    else {
                        // create user
                        // Generate salt
                        bcrypt.genSalt(10, function (saltErr, salt) {
                            // Use salt to hash password
                            bcrypt.hash(password, salt, function (hashErr, hash) {
                                // Insert users to database with hashed password
                                pg.query('INSERT INTO users (username, pw) VALUES ($1, $2) RETURNING *', [username, hash], function (error, user) {
                                    // Redirect to lineup page
                                    return done(null, user.rows[0]);        
                                });
                            });
                        });
                    }
                }
                else
                    return done(err);
            });

        });

    }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function (req, username, password, done) { // callback with email and password from our form
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        pg.query('SELECT * FROM users WHERE username = $1', [username], function (err, result) {
            if (result.rowCount > 0) {
                bcrypt.compare(password, result.rows[0].pw, function (err, pwCorrect) {
                    if (pwCorrect)
                        return done(null, result.rows[0]);
                    else
                        return done(null, false, { message: 'Wrong Password.'});
                });
            }
            else {
                return done(null, false, { message: 'Username Does Not Exists.'});
            }
        })
    }));
    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL
    },

    // facebook will send back the token and profile
    function (token, refreshToken, profile, done) {
        // asynchronous
        process.nextTick(function() {
            // find the user in the database based on their facebook id
            pg.query('SELECT EXISTS(SELECT 1 FROM users WHERE fb_id = $1)', [profile.id], function (err, response) {
                if (!err) {
                    if (response.rows[0].exists) {
                        // get user
                        pg.query('SELECT * FROM users WHERE fb_id = $1', [profile.id], function (find_err, user) {
                            if (!find_err)
                                return done(null, user.rows[0]);
                        });
                    }
                    else {
                        // create user
                        pg.query('INSERT INTO users (fb_id, fb_token, fname, lname, email, display_name) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *', [profile.id, token, profile.name.familyName, profile.name.givenName, profile.emails ? profile.emails[0] : '', profile.displayName], function (create_err, user) {
                            if (!create_err)
                                return done(null, user.rows[0]);
                        })
                    }
                }
                else
                    return done(err);
            });
        });

    }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,

    },
    function (token, refreshToken, profile, done) {

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {

            // try to find the user based on their google id
            pg.query('SELECT EXISTS(SELECT 1 FROM users WHERE google_id = $1)', [profile.id], function (err, response) {
                if (!err) {
                    if (response.rows[0].exists) {
                        // get user
                        pg.query('SELECT * FROM users WHERE google_id = $1', [profile.id], function (find_err, user) {
                            if (!find_err)
                                return done(null, user.rows[0]);
                        });
                    }
                    else {
                        // create user
                        pg.query('INSERT INTO users (google_id, google_token, fname, lname, email, display_name) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *', [profile.id, token, profile.name.familyName, profile.name.givenName, profile.emails[0].value, profile.displayName], function (create_err, user) {
                            if (!create_err)
                                return done(null, user.rows[0]);
                        })
                    }
                }
                else
                    return done(err);
            });
        });

    }));

};