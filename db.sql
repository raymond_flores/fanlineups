--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cbs_players; Type: TABLE; Schema: public; Owner: Raymond; Tablespace: 
--

CREATE TABLE cbs_players (
    cbs_id integer,
    fname text,
    lname text,
    full_name text,
    bye_weeks text,
    photo text,
    "position" text,
    age integer,
    elias_id text,
    status text,
    jersey text,
    team text
);


ALTER TABLE cbs_players OWNER TO "Raymond";

--
-- Name: dk; Type: TABLE; Schema: public; Owner: Raymond; Tablespace: 
--

CREATE TABLE dk (
    "position" text,
    name text,
    salary integer,
    gameinfo text,
    avgpointspergame numeric,
    week integer,
    year integer
);


ALTER TABLE dk OWNER TO "Raymond";

--
-- Name: espn_projections; Type: TABLE; Schema: public; Owner: Raymond; Tablespace: 
--

CREATE TABLE espn_projections (
    espn_id text,
    name text,
    p_c numeric,
    p_a numeric,
    p_yds numeric,
    p_tds numeric,
    p_ints numeric,
    r_avg numeric,
    r_tds numeric,
    r_yds numeric,
    rec_catches numeric,
    rec_yds numeric,
    rec_tds numeric,
    total numeric,
    team text,
    "position" text,
    status text,
    dk_total numeric,
    week integer,
    year integer
);


ALTER TABLE espn_projections OWNER TO "Raymond";

--
-- Name: ffn_players; Type: TABLE; Schema: public; Owner: Raymond; Tablespace: 
--

CREATE TABLE ffn_players (
    ffn_id integer,
    active boolean,
    fname text,
    lname text,
    full_name text,
    team text,
    "position" text,
    height text,
    weight text,
    college text,
    jersey text,
    dob text
);


ALTER TABLE ffn_players OWNER TO "Raymond";

--
-- Name: nfl_projections; Type: TABLE; Schema: public; Owner: Raymond; Tablespace: 
--

CREATE TABLE nfl_projections (
    name text,
    p_yds numeric,
    p_tds numeric,
    p_ints numeric,
    r_tds numeric,
    r_yds numeric,
    rec_catches numeric,
    rec_yds numeric,
    rec_tds numeric,
    total numeric,
    team text,
    "position" text,
    status text,
    dk_total numeric,
    week integer,
    year integer,
    nfl_id text,
    fum_td numeric,
    tpc numeric,
    fum_lost numeric,
    opp text,
    sacks numeric,
    ints numeric,
    fum_rec numeric,
    safety numeric,
    td numeric,
    ret numeric,
    pts_allow numeric
);


ALTER TABLE nfl_projections OWNER TO "Raymond";

--
-- Name: saved_lineups; Type: TABLE; Schema: public; Owner: Raymond; Tablespace: 
--

CREATE TABLE saved_lineups (
    qb integer,
    rb1 integer,
    rb2 integer,
    wr1 integer,
    wr2 integer,
    wr3 integer,
    te integer,
    dst integer,
    flex integer,
    user_id integer,
    week integer,
    year integer,
    created timestamp without time zone DEFAULT now()
);


ALTER TABLE saved_lineups OWNER TO "Raymond";

--
-- Name: users; Type: TABLE; Schema: public; Owner: Raymond; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    fname text,
    lname text,
    email text,
    google_token text,
    google_id text,
    fb_id text,
    fb_token text,
    display_name text,
    username text,
    pw text
);


ALTER TABLE users OWNER TO "Raymond";

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: Raymond
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO "Raymond";

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Raymond
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: Raymond
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: cbs_players; Type: TABLE DATA; Schema: public; Owner: Raymond
--

COPY cbs_players (cbs_id, fname, lname, full_name, bye_weeks, photo, "position", age, elias_id, status, jersey, team) FROM stdin;
1975		49ers	49ers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	515	\N	\N	SF
1619994	Johnny	Adams	Johnny Adams	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ADA428790	A	\N	IND
1996147	Bralon	Addison	Bralon Addison	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ADD481098	A	\N	DRF
2079057	Mario	Alford	Mario Alford	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2079057.png	WR	24	ALF635509	A	15	CIN
1253944	Nate	Allen	Nate Allen	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1253944.png	DB	28	ALL632895	IR	20	OAK
1242977	Marvin	Austin	Marvin Austin	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	AUS456698	IR	\N	DEN
406233	Jon	Beason	Jon Beason	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BEA530518	IR	\N	NYG
2007		Bengals	Bengals	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	326	\N	\N	CIN
1630448	Travis	Benjamin	Travis Benjamin	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630448.png	WR	26	BEN162218	A	\N	SD
1620157	Dwight	Bentley	Dwight Bentley	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BEN688876	A	\N	DET
1620001	Tramaine	Brock	Tramaine Brock	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620001.png	DB	27	BRO052785	A	26	SF
1243255	Michael	Campbell	Michael Campbell	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CAM517755	A	\N	NYJ
1868369	Frank	Clark	Frank Clark	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1868369.png	DL	22	CLA296952	A	55	SEA
1824389	David	Cobb	David Cobb	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824389.png	RB	22	COB141451	A	23	TEN
1631001	Dan	Conroy	Dan Conroy	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	CON680669	A	\N	FA
2130134	James	Davidson	James Davidson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DAV071079	A	\N	MIA
1999705	Lorenzo	Doss	Lorenzo Doss	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	21	DOS257736	A	37	DEN
1272251	Ras-I	Dowling	Ras-I Dowling	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1272251.png	DB	27	DOW584478	A	\N	CAR
1851271	B.J.	Dubose	B.J. Dubose	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	DUB546222	A	90	MIN
1877307	Bud	Dupree	Bud Dupree	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1877307.png	DL	23	DUP507860	A	48	PIT
2175123	Donteea	Dye	Donteea Dye	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2175123.png	WR	22	DYE190770	A	17	TB
2175142	Nick	Dzubnar	Nick Dzubnar	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	DZU088990	A	48	SD
1971895	Nate	Ebner	Nate Ebner	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971895.png	DB	27	EBN177981	A	43	NE
2130349	Isame	Faciane	Isame Faciane	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	FAC296636	A	76	MIN
2175024	George	Farmer	George Farmer	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	22	FAR270859	A	\N	SEA
234829	Jay	Feely	Jay Feely	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	FEE461435	A	\N	CHI
1737134	William	Gholston	William Gholston	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737134.png	DL	24	GHO405127	A	92	TB
1871347	Melvin	Gordon	Melvin Gordon	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1871347.png	RB	22	GOR275321	IR	28	SD
1631039	MarQueis	Gray	MarQueis Gray	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631039.png	TE	26	GRA739815	IR	\N	MIA
1966964	Andre	Hardy	Andre Hardy	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HAR072075	A	\N	ARI
2138869	Josh	Harris	Josh Harris	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAR453697	A	\N	PIT
1824784	Dee	Hart	Dee Hart	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAR663737	A	\N	FA
2174299	Terrell	Hartsfield	Terrell Hartsfield	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAR747574	A	\N	IND
2174851	Nick	Harwell	Nick Harwell	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	HAR829922	A	\N	TEN
1824883	D.J.	Hayden	D.J. Hayden	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824883.png	DB	25	HAY031145	A	25	OAK
1615576	William	Hayes	William Hayes	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1615576.png	DL	30	HAY474225	A	95	STL
2060094	Jeff	Heath	Jeff Heath	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060094.png	DB	24	HEA721605	A	38	DAL
1619693	Chas	Alecxih	Chas Alecxih	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	27	ALE004832	A	65	CAR
1699674	Will	Allen	Will Allen	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1699674.png	DB	33	ALL653088	A	20	PIT
2174821	Damarr	Aultman	Damarr Aultman	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	AUL428791	A	\N	MIA
1765886	Bashaud	Breeland	Bashaud Breeland	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1765886.png	DB	24	BRE183598	A	26	WAS
1824132	Tank	Carradine	Tank Carradine	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824132.png	DL	27	CAR445551	A	95	SF
2000901	Landon	Collins	Landon Collins	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2000901.png	DB	22	COL624212	A	21	NYG
1675499	Jermelle	Cudjo	Jermelle Cudjo	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675499.png	DL	29	CUD169192	A	99	DET
1630925	Tandon	Doss	Tandon Doss	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DOS560472	A	\N	JAC
2060071	Darin	Drakeford	Darin Drakeford	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DRA375523	A	\N	KC
2186287	Adam	Drake	Adam Drake	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DRA155410	A	\N	KC
2175081	Quinton	Dunbar	Quinton Dunbar	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	DUN101336	A	47	WAS
2080283	Michael	Dyer	Michael Dyer	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	DYE631435	A	\N	OAK
1737266	Dominique	Easley	Dominique Easley	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737266.png	DL	24	EAS071808	IR	99	NE
1674116	Julian	Edelman	Julian Edelman	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674116.png	WR	29	EDE425791	A	11	NE
2059207	Darius	Eubanks	Darius Eubanks	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059207.png	LB	24	EUB062826	A	49	TB
2174979	Deshazor	Everett	Deshazor Everett	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	EVE115531	A	\N	WAS
410446	Anthony	Fasano	Anthony Fasano	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/410446.png	TE	31	FAS072076	A	80	TEN
410445	Heath	Farwell	Heath Farwell	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FAR798216	A	\N	SEA
1823811	Andrew	Gachkar	Andrew Gachkar	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1823811.png	LB	27	GAC280472	A	52	DAL
411492	Kedric	Golston	Kedric Golston	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/411492.png	DL	32	GOL705804	A	64	WAS
2130205	Cameron	Gordon	Cameron Gordon	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	GOR053957	A	\N	KC
2175144	Curtis	Grant	Curtis Grant	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	GRA400136	A	\N	TEN
1620865	Cyrus	Gray	Cyrus Gray	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1620865.png	RB	26	GRA622876	A	\N	DEN
2129678	Shelby	Harris	Shelby Harris	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129678.png	DL	24	HAR536152	A	\N	OAK
1114598	Percy	Harvin	Percy Harvin	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114598.png	WR	27	HAR829482	IR	18	BUF
2058158	Cam	Henderson	Cam Henderson	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HEN045054	A	\N	NE
1977169	Buddy	Jackson	Buddy Jackson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977169.png	DB	27	JAC037049	A	\N	DAL
1102782	Fred	Jackson	Fred Jackson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1102782.png	RB	35	JAC173789	A	22	SEA
492963	Steven	Jackson	Steven Jackson	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/492963.png	RB	32	JAC560875	A	39	NE
518155	Tyson	Jackson	Tyson Jackson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518155.png	DL	29	JAC610518	A	94	ATL
2129556	Kendall	James	Kendall James	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129556.png	DB	24	JAM315243	A	\N	NYJ
187389	Sebastian	Janikowski	Sebastian Janikowski	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/187389.png	K	38	JAN400354	A	11	OAK
502629	Calvin	Johnson	Calvin Johnson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JOH088640	A	\N	DET
2130883	Derrick	Johnson	Derrick Johnson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JOH201799	A	\N	NYG
1851113	Javorius	Allen	Javorius Allen	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851113.png	RB	24	ALL469092	A	37	BAL
2130211	Justin	Anderson	Justin Anderson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	AND316789	A	\N	NO
2175137	Manny	Asprilla	Manny Asprilla	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ASP686828	A	\N	SD
1244512	Akeem	Ayers	Akeem Ayers	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244512.png	LB	26	AYE264861	A	56	STL
1853776	Bryan	Braman	Bryan Braman	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1853776.png	LB	28	BRA447369	A	56	PHI
1272449	Dezmon	Briscoe	Dezmon Briscoe	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BRI497843	A	\N	DAL
2185037	Avius	Capers	Avius Capers	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CAP097003	A	\N	CAR
2059240	Deveron	Carr	Deveron Carr	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059240.png	DB	25	CAR360857	A	\N	KC
1893136	Ha Ha	Clinton-Dix	Ha Ha Clinton-Dix	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1893136.png	DB	23	CLI769272	A	21	GB
2130245	Brock	Coyle	Brock Coyle	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130245.png	LB	25	COY195645	A	52	SEA
1727755	Johnathan	Cyprien	Johnathan Cyprien	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1727755.png	DB	25	CYP563610	A	37	JAC
2185868	Kenzel	Doe	Kenzel Doe	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DOE018456	A	\N	PIT
1616721	Leger	Douzable	Leger Douzable	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	29	DOU798721	A	78	NYJ
2129677	Jonathan	Dowling	Jonathan Dowling	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129677.png	DB	24	DOW548703	A	\N	BUF
2174824	Ray	Drew	Ray Drew	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	DRE546972	A	\N	GB
2130887	Greg	Ducre	Greg Ducre	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	DUC753589	A	\N	SD
2059228	Reggie	Dunn	Reggie Dunn	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DUN666638	A	\N	DAL
1823343	Kris	Durham	Kris Durham	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DUR319927	A	\N	OAK
1242895	Jonathan	Dwyer	Jonathan Dwyer	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	DWY425790	A	\N	ARI
1679689	Kasim	Edebali	Kasim Edebali	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	26	EDE095835	A	91	NO
2174928	Cole	Farrand	Cole Farrand	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FAR562575	A	\N	NYG
1754367	Justin	Gilbert	Justin Gilbert	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1754367.png	DB	24	GIL088998	A	21	CLE
1975901	Tashaun	Gipson	Tashaun Gipson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975901.png	DB	25	GIP543222	A	\N	JAC
1686045	Josh	Gordon	Josh Gordon	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1686045.png	WR	24	GOR190310	A	12	CLE
2174896	Corey	Grant	Corey Grant	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	GRA399769	A	33	JAC
1273176	Joe	Haden	Joe Haden	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273176.png	DB	26	HAD357325	IR	23	CLE
1137463	Greg	Hardy	Greg Hardy	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137463.png	DL	27	HAR101120	A	76	DAL
1974172	Jerrell	Harris	Jerrell Harris	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	26	HAR443823	A	\N	DAL
412454	A.J.	Hawk	A.J. Hawk	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/412454.png	LB	32	HAW076822	A	50	CIN
1616856	David	Hawthorne	David Hawthorne	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAW758019	A	\N	NO
2129555	Matt	Hazel	Matt Hazel	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129555.png	WR	24	HAZ158147	A	83	MIA
2174914	Taylor	Heinicke	Taylor Heinicke	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	23	HEI610185	A	6	MIN
2175029	Mitchell	Henry	Mitchell Henry	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	HEN645775	A	\N	GB
1117345	Nate	Irving	Nate Irving	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117345.png	LB	27	IRV733129	IR	55	IND
1749630	Bennett	Jackson	Bennett Jackson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1749630.png	DB	24	JAC000002	IR	24	NYG
1273346	Kareem	Jackson	Kareem Jackson	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273346.png	DB	28	JAC283467	A	25	HOU
494497	Mike	Adams	Mike Adams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/494497.png	DB	35	ADA515576	A	29	IND
2174986	Derek	Akunne	Derek Akunne	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	AKU444955	A	\N	DAL
1630216	Dwayne	Allen	Dwayne Allen	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630216.png	TE	26	ALL164177	IR	83	IND
2130671	Nate	Askew	Nate Askew	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ASK314361	A	\N	CAR
405833	Donnie	Avery	Donnie Avery	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	AVE422211	A	\N	KC
1971899	Justin	Bethel	Justin Bethel	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971899.png	DB	25	BET201235	A	28	ARI
1674084	Derek	Cox	Derek Cox	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	COX192709	A	\N	NE
1971919	Richard	Crawford	Richard Crawford	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971919.png	DB	25	CRA724785	A	\N	SD
1137247	Kellen	Davis	Kellen Davis	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137247.png	TE	30	DAV500592	A	47	NYJ
1630271	Everett	Dawkins	Everett Dawkins	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DAW060562	A	\N	NYG
1766827	Tyeler	Davison	Tyeler Davison	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	DAV822084	A	95	NO
1113418	Brandon	Deaderick	Brandon Deaderick	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DEA045635	A	\N	HOU
1630976	Kenny	Demens	Kenny Demens	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630976.png	LB	26	DEM305982	IR	54	ARI
2130840	Anthony	Denham	Anthony Denham	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130840.png	TE	24	DEN050390	IR	81	HOU
1688198	Pat	Devlin	Pat Devlin	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1688198.png	QB	27	DEV727245	A	3	CLE
2130426	Emmanuel	Dieke	Emmanuel Dieke	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DIE333325	A	\N	MIA
563324	Zac	Diles	Zac Diles	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DIL042593	A	\N	CLE
504682	Dennis	Dixon	Dennis Dixon	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	DIX123425	A	\N	ARI
1701513	Aaron	Dobson	Aaron Dobson	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1701513.png	WR	24	DOB744752	A	17	NE
2057733	Kevin	Dorsey	Kevin Dorsey	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DOR658637	A	\N	NE
1682150	Jamell	Fleming	Jamell Fleming	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1682150.png	DB	26	FLE257221	A	30	KC
1102778	Thomas	Gafford	Thomas Gafford	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1102778.png	TE	33	GAF646935	A	47	OAK
2138347	Jeremy	Grable	Jeremy Grable	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GRA026251	A	\N	TB
1984098	Marion	Grice	Marion Grice	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	25	GRI000785	A	\N	ARI
2174801	Justin	Hamilton	Justin Hamilton	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	HAM241071	A	\N	SEA
2132715	Caleb	Holley	Caleb Holley	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HOL223531	A	\N	BUF
1125381	Rob	Housler	Rob Housler	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125381.png	TE	28	HOU358294	A	88	CHI
2174802	Andrew	Hudson	Andrew Hudson	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HUD217099	A	\N	BUF
2059364	Chuck	Jacobs	Chuck Jacobs	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059364.png	WR	25	JAC707193	A	\N	BAL
563623	Malcolm	Jenkins	Malcolm Jenkins	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563623.png	DB	28	JEN372222	A	27	PHI
2130885	Henry	Josey	Henry Josey	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	JOS511848	A	\N	MIN
1682127	Nick	Kasa	Nick Kasa	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1682127.png	TE	25	KAS005766	A	\N	DEN
1117803	Lance	Kendricks	Lance Kendricks	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117803.png	TE	28	KEN102780	A	88	STL
2174839	Jake	Kumerow	Jake Kumerow	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	KUM425791	A	84	CIN
1254435	Travis	Lewis	Travis Lewis	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1254435.png	LB	28	LEW746912	A	\N	MIN
415313	Marcedes	Lewis	Marcedes Lewis	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/415313.png	TE	31	LEW492053	A	89	JAC
1137460	Kendrick	Lewis	Kendrick Lewis	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137460.png	DB	27	LEW430412	A	23	BAL
2000078	Jerell	Adams	Jerell Adams	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	ADA400343	A	\N	DRF
1853150	Mario	Addison	Mario Addison	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1853150.png	DL	28	ADD494264	A	97	CAR
1631825	Kiko	Alonso	Kiko Alonso	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631825.png	LB	25	ALO444955	A	\N	MIA
1243537	Larry	Asante	Larry Asante	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243537.png	DB	28	ASA595852	A	42	OAK
2175342	Donnie	Baggs	Donnie Baggs	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BAG671214	A	\N	DAL
2174758	Reggie	Bell	Reggie Bell	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	BEL534473	A	\N	NO
1891915	McLeod	Bethel-Thompson	McLeod Bethel-Thompson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1891915.png	QB	27	BET216423	A	\N	PHI
1996524	Alex	Carter	Alex Carter	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	21	CAR608019	IR	33	DET
1868418	Devante	Davis	Devante Davis	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DAV263423	A	\N	PHI
2060172	Troy	Davis	Troy Davis	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DAV780558	A	\N	DAL
2130865	Jeremy	Deering	Jeremy Deering	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DEE540220	A	\N	JAC
2007608	Jeff	Demps	Jeff Demps	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	DEM598354	A	\N	IND
409521	John	Denney	John Denney	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	37	DEN261515	A	92	MIA
1891800	Patrick	DiMarco	Patrick DiMarco	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1891800.png	RB	26	DIM110572	A	42	ATL
1968080	JoJo	Dickson	JoJo Dickson	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DIC714108	A	\N	KC
1737155	Ahmad	Dixon	Ahmad Dixon	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DIX042141	A	\N	MIN
1117687	James	Dockery	James Dockery	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DOC412666	A	\N	OAK
2130635	Chase	Dixon	Chase Dixon	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	DIX109147	A	\N	WAS
558589	Ricky Jean	Francois	Ricky Jean Francois	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/558589.png	DL	29	JEA418291	A	99	WAS
1825124	Charles	Gaines	Charles Gaines	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	GAI186048	A	43	CLE
1737669	E.J.	Gaines	E.J. Gaines	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737669.png	DB	24	GAI292759	IR	33	STL
2174298	Cody	Galea	Cody Galea	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GAL184469	A	\N	IND
1860750	Rashad	Greene	Rashad Greene	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860750.png	WR	23	GRE031322	A	13	JAC
1737590	Geneo	Grissom	Geneo Grissom	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737590.png	LB	23	GRI825263	A	92	NE
1631947	Cory	Harkey	Cory Harkey	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631947.png	TE	25	HAR162438	A	46	STL
412294	David	Harris	David Harris	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/412294.png	LB	32	HAR362681	A	52	NYJ
412346	Clark	Harris	Clark Harris	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	31	HAR351770	A	46	CIN
1621549	Mario	Harvey	Mario Harvey	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAR799820	A	\N	NYJ
559551	Lavelle	Hawkins	Lavelle Hawkins	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAW442308	A	\N	TB
1117312	Stanley	Havili	Stanley Havili	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAV606770	A	\N	SEA
502420	Nick	Hayden	Nick Hayden	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/502420.png	DL	30	HAY062620	A	96	DAL
1632180	Casey	Hayward	Casey Hayward	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632180.png	DB	26	HAY777816	A	\N	SD
2174885	Brandon	Ivory	Brandon Ivory	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	IVO241611	A	\N	HOU
2174816	Edwin	Jackson	Edwin Jackson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	JAC160420	A	\N	IND
1615582	Rob	Jackson	Rob Jackson	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JAC492068	A	\N	WAS
2130171	Justin	Jackson	Justin Jackson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JAC283137	A	\N	DAL
1251717	Rashad	Jennings	Rashad Jennings	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1251717.png	RB	31	JEN587965	A	23	NYG
1686006	Alshon	Jeffery	Alshon Jeffery	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1686006.png	WR	26	JEF498860	A	17	CHI
2129466	Maurice	Alexander	Maurice Alexander	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129466.png	DB	25	ALE513150	A	31	STL
1737140	Dion	Bailey	Dion Bailey	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737140.png	DB	24	BAI175340	A	34	NYJ
187741	Tom	Brady	Tom Brady	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/187741.png	QB	38	BRA371156	A	12	NE
1117485	Chimdi	Chekwa	Chimdi Chekwa	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117485.png	DB	27	CHE206486	IR	\N	MIA
2185674	Ethan	Davis	Ethan Davis	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DAV314711	A	\N	DET
1824582	Titus	Davis	Titus Davis	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	DAV768844	A	\N	NYJ
12320	Phil	Dawson	Phil Dawson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/12320.png	K	41	DAW705989	A	9	SF
1824781	Trey	DePriest	Trey DePriest	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DEP736508	A	\N	BAL
409505	Quintin	Demps	Quintin Demps	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/409505.png	DB	30	DEM600703	A	27	HOU
1630805	Alfonzo	Dennard	Alfonzo Dennard	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DEN095169	A	\N	ARI
2174830	Jeremiah	Detmer	Jeremiah Detmer	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	DET291274	A	\N	CHI
2058309	Jayson	DiManche	Jayson DiManche	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058309.png	LB	25	DIM093322	IR	\N	CIN
1114988	Dorin	Dickerson	Dorin Dickerson	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114988.png	TE	28	DIC263017	IR	42	TEN
2129592	Brandon	Dixon	Brandon Dixon	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129592.png	DB	25	DIX096438	A	\N	NO
492923	Darnell	Dockett	Darnell Dockett	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DOC428041	A	\N	SF
410595	Cortland	Finnegan	Cortland Finnegan	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/410595.png	DB	32	FIN524193	A	26	CAR
2130624	Mike	Flacco	Mike Flacco	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	FLA014403	A	\N	JAC
410703	Andre	Fluellen	Andre Fluellen	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	FLU079102	A	\N	DET
313800	Dwight	Freeney	Dwight Freeney	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/313800.png	LB	36	FRE417537	A	54	ARI
2129435	Phillip	Gaines	Phillip Gaines	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129435.png	DB	25	GAI428981	IR	23	KC
1664971	DeJon	Gomes	DeJon Gomes	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GOM206359	A	\N	DET
1853171	Chris	Harris	Chris Harris	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1853171.png	DB	26	HAR349333	A	25	DEN
2174954	John	Harris	John Harris	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAR446820	A	\N	ATL
563622	Brian	Hartline	Brian Hartline	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563622.png	WR	29	HAR725593	IR	83	CLE
432986	Kelvin	Hayden	Kelvin Hayden	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAY034606	A	\N	CHI
517291	Chad	Henne	Chad Henne	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517291.png	QB	30	HEN507580	A	7	JAC
2060083	Josh	Hill	Josh Hill	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060083.png	TE	25	HIL451418	A	89	NO
2175059	David	Irving	David Irving	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	IRV732218	IR	95	DAL
1633227	Asa	Jackson	Asa Jackson	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633227.png	DB	26	JAC000001	A	\N	ARI
1630782	Jerrell	Jackson	Jerrell Jackson	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JAC240753	A	\N	KC
1672870	A.J.	Jenkins	A.J. Jenkins	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JEN124150	A	\N	DAL
562807	Antonio	Johnson	Antonio Johnson	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JOH061319	A	\N	NE
2057693	Charles	Johnson	Charles Johnson	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057693.png	WR	27	JOH105729	A	12	MIN
523490	Chris	Johnson	Chris Johnson	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/523490.png	RB	30	JOH127799	IR	23	ARI
1737356	Dontae	Johnson	Dontae Johnson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737356.png	DB	24	JOH211430	A	36	SF
2130886	D.J.	Adams	D.J. Adams	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	ADA242233	A	\N	SD
2175309	Tyrell	Adams	Tyrell Adams	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	ADA679872	A	\N	KC
2062178	Jahleel	Addae	Jahleel Addae	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062178.png	DB	26	ADD129366	A	37	SD
1825212	Jay	Ajayi	Jay Ajayi	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825212.png	RB	22	AJA771256	A	23	MIA
2079150	Dominique	Alexander	Dominique Alexander	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ALE226765	A	\N	DRF
1737096	Keenan	Allen	Keenan Allen	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737096.png	WR	23	ALL532966	A	13	SD
2132703	RaShaun	Allen	RaShaun Allen	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	26	ALL639809	A	88	NO
1860917	Jace	Amaro	Jace Amaro	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860917.png	TE	23	AMA563609	IR	88	NYJ
1675102	Colt	Anderson	Colt Anderson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675102.png	DB	30	AND162908	A	32	IND
1977163	Chigbo	Anunoby	Chigbo Anunoby	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ANU444955	A	\N	MIN
1137469	Javier	Arenas	Javier Arenas	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137469.png	DB	28	ARE404040	A	\N	BUF
2058350	Josh	Aubrey	Josh Aubrey	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058350.png	DB	25	AUB563610	A	\N	TEN
2132722	Denico	Autry	Denico Autry	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132722.png	DL	25	AUT529722	A	96	OAK
133217	Champ	Bailey	Champ Bailey	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BAI582194	A	\N	NO
2107		Bengals	Bengals	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	226	\N	\N	CIN
1255016	Eric	Berry	Eric Berry	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1255016.png	DB	27	BER517115	A	29	KC
2130833	Kapri	Bibbs	Kapri Bibbs	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	BIB097874	A	\N	DEN
1674985	Kevin	Brock	Kevin Brock	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BRO030498	IR	\N	NO
407793	Jason	Campbell	Jason Campbell	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	CAM375235	A	\N	CIN
1975891	Garrett	Celek	Garrett Celek	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975891.png	TE	27	CEL270111	IR	88	SF
408163	Scott	Chandler	Scott Chandler	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408163.png	TE	30	CHA313999	A	88	NE
1737150	Gerald	Christian	Gerald Christian	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	CHR356457	IR	83	ARI
1139116	Justin	Cole	Justin Cole	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	COL128175	A	\N	OAK
1630801	Will	Compton	Will Compton	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630801.png	LB	26	COM673597	A	51	WAS
1824735	Xavier	Cooper	Xavier Cooper	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	COO814748	A	96	CLE
1763468	Geremy	Davis	Geremy Davis	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1763468.png	WR	24	DAV342290	A	18	NYG
1996847	P.J.	Dawson	P.J. Dawson	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	DAW695489	A	47	CIN
1208949	Zak	DeOssie	Zak DeOssie	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	31	DEO425791	A	51	NYG
1853187	Larry	Dean	Larry Dean	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DEA421645	A	\N	TB
2081234	Ryan	Delaire	Ryan Delaire	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	DEL053090	A	91	CAR
1762306	Darqueze	Dennard	Darqueze Dennard	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762306.png	DB	24	DEN095585	A	21	CIN
1114916	Akeem	Dent	Akeem Dent	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114916.png	LB	28	DEN660497	A	50	HOU
565631	Ed	Dickson	Ed Dickson	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/565631.png	TE	28	DIC709336	A	84	CAR
1824891	Quandre	Diggs	Quandre Diggs	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	DIG202845	A	28	DET
1116464	Anthony	Dixon	Anthony Dixon	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116464.png	RB	28	DIX077953	A	26	BUF
1114917	Demarcus	Dobbs	Demarcus Dobbs	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114917.png	DL	28	DOB286985	A	95	SEA
1995451	Larry	Donnell	Larry Donnell	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1995451.png	TE	27	DON564065	IR	84	NYG
2130721	Lee	Doss	Lee Doss	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DOS251361	A	\N	CLE
1117379	Shaun	Draughn	Shaun Draughn	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117379.png	RB	28	DRA542186	IR	24	SF
1762308	Kurtis	Drummond	Kurtis Drummond	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	DRU363898	A	40	HOU
1977147	Lance	Dunbar	Lance Dunbar	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977147.png	RB	26	DUN098604	IR	25	DAL
1273172	Carlos	Dunlap	Carlos Dunlap	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273172.png	DL	27	DUN474478	A	96	CIN
1691198	Walt	Aikens	Walt Aikens	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691198.png	DB	24	AIK288111	A	35	MIA
1759310	Jeremiah	Attaochu	Jeremiah Attaochu	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759310.png	LB	23	ATT290361	A	97	SD
1700338	Tavon	Austin	Tavon Austin	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700338.png	WR	25	AUS659596	A	11	STL
1977132	Johnson	Bademosi	Johnson Bademosi	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977132.png	DB	25	BAD600092	A	\N	DET
2057660	William	Campbell	William Campbell	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	CAM672275	A	\N	GB
303349	Ryan	Clark	Ryan Clark	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CLA544413	A	\N	WAS
1263584	Demario	Davis	Demario Davis	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1263584.png	LB	27	DAV262404	A	\N	CLE
1664705	Knile	Davis	Knile Davis	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664705.png	RB	24	DAV514200	A	34	KC
409345	Thomas	Davis	Thomas Davis	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/409345.png	LB	33	DAV768681	A	58	CAR
1115108	Vontae	Davis	Vontae Davis	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1115108.png	DB	27	DAV786545	A	21	IND
2175023	Synjyn	Days	Synjyn Days	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	DAY761366	A	\N	DAL
2174215	Andre	Debose	Andre Debose	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	DEB587551	IR	\N	OAK
1714348	Pierre	Desir	Pierre Desir	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1714348.png	DB	25	DES373418	A	26	CLE
1893138	Xzavier	Dickson	Xzavier Dickson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DIC736160	A	\N	ATL
2174952	Jordan	Dewalt-Ondijo	Jordan Dewalt-Ondijo	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DEW039392	A	\N	HOU
2000038	Stefon	Diggs	Stefon Diggs	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2000038.png	WR	22	DIG218895	A	14	MIN
2130412	Brian	Dixon	Brian Dixon	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	DIX101461	A	20	NO
518531	Tim	Dobbins	Tim Dobbins	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DOB283610	A	\N	DAL
1836058	Phillip	Dorsett	Phillip Dorsett	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1836058.png	WR	23	DOR488199	A	15	IND
409949	Elvis	Dumervil	Elvis Dumervil	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/409949.png	LB	32	DUM179959	A	58	BAL
2028383	Joe Don	Duncan	Joe Don Duncan	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	DON330922	A	\N	DEN
1865396	Eric	Ebron	Eric Ebron	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1865396.png	TE	23	EBR474619	A	85	DET
1977075	Kevin	Elliott	Kevin Elliott	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ELL334603	A	\N	BUF
1877168	Durell	Eskridge	Durell Eskridge	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	ESK558735	A	\N	ARI
2001877	Devin	Funchess	Devin Funchess	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2001877.png	WR	21	FUN405541	A	17	CAR
1630777	Blaine	Gabbert	Blaine Gabbert	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630777.png	QB	26	GAB221145	A	2	SF
1691489	Marquise	Goodwin	Marquise Goodwin	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691489.png	WR	25	GOO681722	IR	88	BUF
1619913	James	Hanna	James Hanna	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619913.png	TE	26	HAN244555	A	84	DAL
2175033	Connor	Hamlett	Connor Hamlett	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	HAM397984	A	\N	CLE
1700274	Mark	Harrison	Mark Harrison	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAR606153	A	\N	KC
1107212	Jason	Hatcher	Jason Hatcher	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1107212.png	DL	33	HAT099134	A	97	WAS
1263398	Junior	Hemingway	Junior Hemingway	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HEM180859	A	\N	KC
1701008	Ronnie	Hillman	Ronnie Hillman	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1701008.png	RB	24	HIL737842	A	23	DEN
1632047	Will	Hill	Will Hill	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632047.png	DB	26	HIL000001	A	33	BAL
397248	Israel	Idonije	Israel Idonije	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	IDO444955	A	\N	NYG
1762057	Emil	Igwenagu	Emil Igwenagu	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	IGW428791	A	\N	DET
1779111	Bruce	Irvin	Bruce Irvin	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1779111.png	LB	28	IRV115072	A	\N	OAK
1737228	Christian	Jones	Christian Jones	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	JON162414	A	59	CHI
1925		49ers	49ers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	415	\N	\N	SF
1963570	Royce	Adams	Royce Adams	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ADA567172	A	\N	CLE
1855552	Victor	Aiyewa	Victor Aiyewa	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	AIY177981	A	\N	SD
492877	Jared	Allen	Jared Allen	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ALL454745	A	\N	CAR
1700484	Denicos	Allen	Denicos Allen	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ALL099471	A	\N	TB
1631257	Jonathon	Amaya	Jonathon Amaya	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	AMA783256	A	\N	KC
1996151	Arik	Armstead	Arik Armstead	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	ARM057964	A	91	SF
405824	Jason	Avant	Jason Avant	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/405824.png	WR	32	AVA444955	A	81	KC
2174199	Kennard	Backman	Kennard Backman	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	BAC639682	A	86	GB
1272275	Allen	Bailey	Allen Bailey	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272275.png	DL	27	BAI026169	A	97	KC
2106		Bears	Bears	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	221	\N	\N	CHI
407944	Adam	Carriker	Adam Carriker	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CAR485536	A	\N	WAS
1984363	B.J.	Catalon	B.J. Catalon	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	CAT072787	A	\N	FA
1979354	Nate	Chandler	Nate Chandler	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1979354.png	DL	26	CHA276106	A	78	CAR
2174800	Andre	Davis	Andre Davis	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	22	DAV122703	A	\N	TB
1853326	Drew	Davis	Drew Davis	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DAV286887	A	\N	ATL
2059214	Kanorris	Davis	Kanorris Davis	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DAV499656	A	\N	NE
409383	Keyunta	Dawson	Keyunta Dawson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DAW601476	A	\N	NO
2058555	Skye	Dawson	Skye Dawson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DAW747636	A	\N	DET
584290	Eric	Decker	Eric Decker	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/584290.png	WR	29	DEC487021	A	87	NYJ
1977074	Antonio	Dennard	Antonio Dennard	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DEN095502	A	\N	GB
2061203	A.J.	Derby	A.J. Derby	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	DER139014	IR	\N	NE
1786771	James	Develin	James Develin	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1786771.png	RB	27	DEV145356	IR	46	NE
1824782	Quinton	Dial	Quinton Dial	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824782.png	DL	25	DIA336966	A	92	SF
1770388	Derek	Dimke	Derek Dimke	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	DIM380680	A	\N	JAC
1252339	Marcus	Dixon	Marcus Dixon	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DIX461446	A	\N	TEN
1737460	Aaron	Donald	Aaron Donald	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737460.png	DL	24	DON134977	A	99	STL
1724614	Michael	Egnew	Michael Egnew	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	EGN177981	A	\N	NO
1737233	Ego	Ferguson	Ego Ferguson	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737233.png	DL	24	FER088440	IR	95	CHI
502818	Brandon	Flowers	Brandon Flowers	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/502818.png	DB	30	FLO296357	IR	24	SD
1971908	Tim	Fugger	Tim Fugger	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FUG355964	IR	\N	NYJ
2174208	Josh	Furman	Josh Furman	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FUR466569	A	\N	DEN
1871314	Doran	Grant	Doran Grant	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1871314.png	DB	23	GRA412011	A	24	PIT
1244303	Rob	Gronkowski	Rob Gronkowski	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244303.png	TE	26	GRO135948	A	87	NE
1631775	Lawrence	Guy	Lawrence Guy	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631775.png	DL	26	GUY230716	A	93	BAL
1272283	Leonard	Hankerson	Leonard Hankerson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272283.png	WR	27	HAN169573	A	80	BUF
2060073	Frankie	Hammond	Frankie Hammond	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060073.png	WR	26	HAM500863	A	85	KC
1762379	Justin	Hardy	Justin Hardy	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762379.png	WR	24	HAR111877	A	16	ATL
2175072	Greg	Henderson	Greg Henderson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HEN060985	A	\N	NYJ
1703048	Jordan	Hill	Jordan Hill	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1703048.png	DL	25	HIL450491	A	97	SEA
1891917	Chris	Hogan	Chris Hogan	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1891917.png	WR	27	HOG008552	A	\N	NE
584340	Ziggy	Hood	Ziggy Hood	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/584340.png	DL	29	HOO071327	A	\N	WAS
1631626	Zac	Dysert	Zac Dysert	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631626.png	QB	26	DYS164481	A	\N	MIA
2174195	Randall	Evans	Randall Evans	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	EVA615132	A	41	PHI
1697210	Hebron	Fangupo	Hebron Fangupo	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	FAN090628	A	\N	KC
502618	Charles	Godfrey	Charles Godfrey	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/502618.png	DB	30	GOD267860	A	\N	ATL
1823814	Richard	Gordon	Richard Gordon	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1823814.png	TE	28	GOR319758	A	\N	DEN
411579	Stephen	Gostkowski	Stephen Gostkowski	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/411579.png	K	32	GOS721266	A	3	NE
1615574	Alex	Hall	Alex Hall	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HAL165262	A	\N	CAR
303162	James	Harrison	James Harrison	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/303162.png	LB	37	HAR600985	A	92	PIT
1616746	Steven	Hauschka	Steven Hauschka	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616746.png	K	30	HAU342462	A	4	SEA
1823355	Alex	Henery	Alex Henery	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	HEN392894	A	\N	DET
1877276	Amarlo	Herrera	Amarlo Herrera	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	HER468073	A	49	IND
1243804	Cameron	Heyward	Cameron Heyward	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243804.png	DL	26	HEY271611	A	97	PIT
2176445	Mario	Hull	Mario Hull	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HUL195644	A	\N	SF
2129585	Andrew	Jackson	Andrew Jackson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JAC035265	A	\N	IND
1631887	Malik	Jackson	Malik Jackson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631887.png	DL	26	JAC373912	A	\N	JAC
541651	Vincent	Jackson	Vincent Jackson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/541651.png	WR	33	JAC627460	A	83	TB
2130416	Nic	Jacobs	Nic Jacobs	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	JAC755342	A	85	JAC
1737753	Jawan	Jamison	Jawan Jamison	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	JAM799977	A	\N	PIT
2062160	Greg	Jenkins	Greg Jenkins	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JEN224943	A	\N	JAC
413737	Greg	Jennings	Greg Jennings	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/413737.png	WR	32	JEN480468	A	85	MIA
1860755	Timmy	Jernigan	Timmy Jernigan	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860755.png	DL	23	JER465471	A	97	BAL
559228	Peria	Jerry	Peria Jerry	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JER500192	A	\N	ATL
1824830	Anthony	Johnson	Anthony Johnson	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824830.png	DL	23	JOH057828	A	\N	WAS
1998316	Duke	Johnson	Duke Johnson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1998316.png	RB	22	JOH000009	A	29	CLE
2174205	Akeem	King	Akeem King	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	KIN276536	A	28	ATL
2167519	Corey	Knox	Corey Knox	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	KNO396285	A	\N	BUF
414962	Dawan	Landry	Dawan Landry	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LAN144473	A	\N	NYJ
1674126	Ellis	Lankster	Ellis Lankster	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LAN676799	A	\N	BUF
1889883	Cody	Latimer	Cody Latimer	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1889883.png	WR	23	LAT362049	A	14	DEN
2175028	Desmond	Lawrence	Desmond Lawrence	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LAW285507	A	\N	CIN
2130724	Rashad	Lawrence	Rashad Lawrence	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	LAW493179	A	\N	JAC
565762	Sean	Lee	Sean Lee	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565762.png	LB	29	LEE650245	A	50	DAL
1682104	Alec	Lemon	Alec Lemon	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LEM570770	A	\N	BAL
2009423	Dezmin	Lewis	Dezmin Lewis	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2009423.png	WR	23	LEW218253	A	13	BUF
1674906	LeQuan	Lewis	LeQuan Lewis	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LEW485997	A	\N	NYJ
2027518	Corbin	Louks	Corbin Louks	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LOU740254	A	\N	DEN
1243684	A.J.	Edds	A.J. Edds	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	EDD454680	A	\N	JAC
1889916	Sam	Ficken	Sam Ficken	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	FIC665846	A	\N	FA
1996129	Dorial	Green-Beckham	Dorial Green-Beckham	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996129.png	WR	22	GRE398545	A	17	TEN
2174090	Rodney	Gunter	Rodney Gunter	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	GUN730129	A	95	ARI
411957	Derek	Hagan	Derek Hagan	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAG040714	A	\N	TEN
1984580	Gerod	Holliman	Gerod Holliman	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	21	HOL254806	A	\N	TB
2130680	Gator	Hoskins	Gator Hoskins	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HOS200145	A	\N	SEA
1691258	Micah	Hyde	Micah Hyde	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691258.png	DB	25	HYD422041	A	33	GB
2185127	Tebucky	Jones	Tebucky Jones	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JON732085	A	\N	TEN
565177	DeAndre	Levy	DeAndre Levy	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565177.png	LB	29	LEV659364	IR	54	DET
2130665	Keith	Lewis	Keith Lewis	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	27	LEW427167	A	\N	KC
2175053	Nathan	Lindsey	Nathan Lindsey	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LIN381866	A	\N	DET
1633115	Kapron	Lewis-Moore	Kapron Lewis-Moore	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633115.png	DL	26	LEW814686	A	95	BAL
1632462	Zach	Line	Zach Line	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632462.png	RB	25	LIN516110	A	48	MIN
1860834	Tyler	Lockett	Tyler Lockett	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860834.png	WR	23	LOC420915	A	16	SEA
2130354	Erik	Lora	Erik Lora	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LOR103822	A	\N	JAC
1776321	Jerry	Lovelocke	Jerry Lovelocke	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	LOV459446	A	\N	BAL
2174904	Jeff	Luc	Jeff Luc	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	LUC018038	A	\N	CIN
2130320	Keon	Lyn	Keon Lyn	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LYN014763	A	\N	TB
1853333	Kamaal	McIlwain	Kamaal McIlwain	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MCI155991	A	\N	BUF
1679974	Barkevious	Mingo	Barkevious Mingo	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679974.png	LB	25	MIN139821	A	51	CLE
1245666	Koa	Misi	Koa Misi	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245666.png	LB	29	MIS374104	IR	55	MIA
2060050	Zach	Minter	Zach Minter	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MIN744105	A	\N	DAL
1692077	Arthur	Moats	Arthur Moats	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1692077.png	LB	28	MOA594964	A	55	PIT
1737547	Terrance	Mitchell	Terrance Mitchell	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737547.png	DB	23	MIT731784	A	21	DAL
1114195	Mike	Mohamed	Mike Mohamed	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MOH152616	A	\N	NO
1630282	Nick	Moody	Nick Moody	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630282.png	LB	26	MOO014418	A	53	SEA
2219914	Eze	Obiora	Eze Obiora	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	OBI461119	A	\N	IND
502524	Greg	Olsen	Greg Olsen	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/502524.png	TE	31	OLS094822	A	88	CAR
1857137	Ben	Jacobs	Ben Jacobs	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1857137.png	LB	27	JAC704560	A	53	CAR
2028397	Jeff	Janis	Jeff Janis	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2028397.png	WR	24	JAN413854	A	83	GB
1620533	Janoris	Jenkins	Janoris Jenkins	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620533.png	DB	27	JEN268608	A	20	NYG
1664147	Jelani	Jenkins	Jelani Jenkins	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664147.png	LB	24	JEN270812	A	53	MIA
552555	Adam	Jones	Adam Jones	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/552555.png	DB	32	JON026865	A	24	CIN
1682469	Taiwan	Jones	Taiwan Jones	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1682469.png	RB	27	JON731670	A	22	OAK
1989886	Kyle	Knox	Kyle Knox	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KNO593269	A	\N	DAL
2174970	Keenan	Lambert	Keenan Lambert	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LAM493820	A	\N	OAK
1688620	DeDe	Lattimore	DeDe Lattimore	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LAT679045	A	\N	CHI
2130638	Jimmy	Legree	Jimmy Legree	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LEG754142	A	\N	ARI
1113519	Sen'Derrick	Marks	Sen'Derrick Marks	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1113519.png	DL	29	MAR262155	IR	99	JAC
2175415	Hutson	Mason	Hutson Mason	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	MAS106966	A	\N	WAS
1123685	Gerald	McCoy	Gerald McCoy	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123685.png	DL	28	MCC604879	A	93	TB
1109755	Tony	McDaniel	Tony McDaniel	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1109755.png	DL	31	MCD147425	A	\N	TB
1673357	Vance	McDonald	Vance McDonald	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1673357.png	TE	25	MCD589658	A	89	SF
1242904	Derrick	Morgan	Derrick Morgan	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1242904.png	LB	27	MOR161260	IR	91	TEN
2130339	Stephen	Morris	Stephen Morris	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	23	MOR536881	A	7	IND
558668	Jerome	Murphy	Jerome Murphy	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MUR274322	A	\N	DEN
502813	Rob	Ninkovich	Rob Ninkovich	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/502813.png	DL	32	NIN311468	A	50	NE
1685976	Trent	Murphy	Trent Murphy	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1685976.png	LB	25	MUR524597	A	93	WAS
2130185	Justin	Perillo	Justin Perillo	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	25	PER083209	A	\N	GB
1884451	Marcus	Peters	Marcus Peters	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1884451.png	DB	23	PET154864	A	22	KC
1116559	Christian	Ponder	Christian Ponder	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	PON404041	A	\N	DEN
2174965	Bradon	Prate	Bradon Prate	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PRA140303	A	\N	PIT
1665374	Jordan	Poyer	Jordan Poyer	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1665374.png	DB	24	POY177981	A	33	CLE
1854552	Brandian	Ross	Brandian Ross	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1854552.png	DB	26	ROS552165	IR	27	SD
1825060	Eric	Rowe	Eric Rowe	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825060.png	DB	23	ROW332230	A	32	PHI
2175993	Jonathon	Rumph	Jonathon Rumph	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	RUM496282	A	\N	NYJ
1700418	Michael	Sam	Michael Sam	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SAM002347	A	\N	DAL
1884454	Bishop	Sankey	Bishop Sankey	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1884454.png	RB	23	SAN746476	A	20	TEN
2174214	Bud	Sasser	Bud Sasser	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SAS599648	A	\N	STL
2064881	Adam	Schiltz	Adam Schiltz	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SCH181903	A	\N	KC
1245675	R.J.	Stanford	R.J. Stanford	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	STA429584	A	\N	DET
493076	Randy	Starks	Randy Starks	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	STA608829	A	\N	CLE
2185172	Trent	Steelman	Trent Steelman	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	STE071459	A	\N	BAL
1116705	Darian	Stewart	Darian Stewart	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1116705.png	DB	27	STE755545	A	26	DEN
1265119	A.J.	Jefferson	A.J. Jefferson	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JEF096790	IR	\N	SEA
1630853	Landry	Jones	Landry Jones	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630853.png	QB	27	JON565403	A	3	PIT
1114348	Thad	Lewis	Thad Lewis	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114348.png	QB	28	LEW733214	A	\N	SF
1910		Lions	Lions	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	429	\N	\N	DET
2130319	Seth	Lobato	Seth Lobato	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	LOB054826	A	\N	TB
1716757	Shawn	Loiseau	Shawn Loiseau	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LOI593273	A	\N	IND
2174774	Ty	Long	Ty Long	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174774.png	K	23	LON645380	A	\N	PIT
1679970	Craig	Loston	Craig Loston	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	26	LOS709456	A	20	JAC
1700931	Christine	Michael	Christine Michael	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700931.png	RB	25	MIC066214	A	32	SEA
417031	Fili	Moala	Fili Moala	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MOA359918	A	\N	HOU
563070	Tony	Moeaki	Tony Moeaki	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563070.png	TE	28	MOE265365	A	\N	ATL
1878014	Donte	Moncrief	Donte Moncrief	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1878014.png	WR	22	MON117004	A	10	IND
552599	Mike	Nugent	Mike Nugent	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/552599.png	K	34	NUG461506	A	2	CIN
1664175	Alex	Okafor	Alex Okafor	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664175.png	LB	25	OKA207645	A	57	ARI
2057694	Nate	Palmer	Nate Palmer	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057694.png	LB	26	PAL534886	A	51	GB
534088	Jalen	Parmele	Jalen Parmele	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	PAR593998	A	\N	CLE
1996183	Cordarrelle	Patterson	Cordarrelle Patterson	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996183.png	WR	25	PAT387075	A	84	MIN
1620468	David	Paulson	David Paulson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	PAU667839	A	\N	SD
1631896	Nick	Perry	Nick Perry	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631896.png	LB	25	PER679695	A	53	GB
2076083	Xavier	Proctor	Xavier Proctor	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PRO148782	A	\N	NYG
2175409	Kyle	Prater	Kyle Prater	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	PRA141959	A	\N	NO
1630256	Sean	Renfree	Sean Renfree	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630256.png	QB	25	REN135405	A	12	ATL
2135505	Jerry	Rice	Jerry Rice	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	RIC134130	A	\N	WAS
2129526	Shaquille	Richardson	Shaquille Richardson	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RIC618445	A	\N	TEN
1116446	Perry	Riley	Perry Riley	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116446.png	LB	27	RIL545116	A	56	WAS
1123333	Kevin	Rutland	Kevin Rutland	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RUT622759	A	\N	KC
1262874	Coty	Sensabaugh	Coty Sensabaugh	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1262874.png	DB	27	SEN455116	A	24	TEN
1824293	JaCorey	Shepherd	JaCorey Shepherd	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	SHE508041	IR	36	PHI
2059177	Gerrard	Sheppard	Gerrard Sheppard	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SHE546538	A	\N	GB
1752364	Tharold	Simon	Tharold Simon	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1752364.png	DB	25	SIM380329	IR	27	SEA
420915	Ernie	Sims	Ernie Sims	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SIM696501	A	\N	ARI
518655	Pat	Sims	Pat Sims	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518655.png	DL	30	SIM785706	A	92	CIN
1252135	Jerrel	Jernigan	Jerrel Jernigan	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JER463741	IR	\N	NYG
1977802	Tony	Jerod-Eddie	Tony Jerod-Eddie	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977802.png	DL	26	JER467202	A	63	SF
396162	Andre	Johnson	Andre Johnson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396162.png	WR	34	JOH056462	A	81	IND
1977129	G.J.	Kinne	G.J. Kinne	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977129.png	QB	27	KIN490782	A	\N	NYG
414963	LaRon	Landry	LaRon Landry	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LAN165690	A	\N	IND
1987614	Jorvorskie	Lane	Jorvorskie Lane	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1987614.png	RB	29	LAN316310	A	46	TB
2174944	Zach	Laskey	Zach Laskey	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	LAS330705	A	45	STL
2063036	Cody	Larsen	Cody Larsen	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LAR388010	A	\N	DEN
1737120	Marcus	Lattimore	Marcus Lattimore	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	LAT682317	A	\N	SF
2167528	Shawn	Lemon	Shawn Lemon	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LEM717375	A	\N	SF
415254	Jim	Leonhard	Jim Leonhard	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LEO708542	A	\N	CLE
1619958	Mikel	Leshoure	Mikel Leshoure	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	LES202930	A	\N	DET
1823846	Korey	Lindsey	Korey Lindsey	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LIN353226	A	\N	DAL
1139141	Dwight	Lowery	Dwight Lowery	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1139141.png	DB	30	LOW556220	A	\N	SD
1979698	Jordan	Mabin	Jordan Mabin	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAB218573	A	\N	SD
1880862	Marcus	Mariota	Marcus Mariota	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1880862.png	QB	22	MAR186347	A	8	TEN
416254	Jameel	McClain	Jameel McClain	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MCC325971	A	\N	NYG
1701015	Leon	McFadden	Leon McFadden	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1701015.png	DB	25	MCF145561	A	\N	NYG
1891880	Kyle	Miller	Kyle Miller	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MIL372090	A	\N	SD
2174197	Tevin	Mitchel	Tevin Mitchel	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MIT039284	A	1	IND
417344	C.J.	Mosley	C.J. Mosley	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MOS360193	A	\N	MIA
1123691	DeMarco	Murray	DeMarco Murray	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123691.png	RB	28	MUR580353	A	\N	TEN
2174220	Marcus	Murphy	Marcus Murphy	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174220.png	RB	24	MUR517528	IR	23	NO
2174972	Quayshawn	Nealy	Quayshawn Nealy	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	NEA691042	A	\N	ARI
417708	Haloti	Ngata	Haloti Ngata	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/417708.png	DL	32	NGA622937	A	92	DET
2175037	Demarkus	Perkins	Demarkus Perkins	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PER168620	A	\N	NYJ
1116940	Corey	Peters	Corey Peters	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116940.png	DL	27	PET099667	IR	98	ARI
2129595	Walt	Powell	Walt Powell	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129595.png	WR	24	POW570795	A	19	BUF
2008675	Hayes	Pullard	Hayes Pullard	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	PUL389002	A	52	JAC
419111	Brady	Quinn	Brady Quinn	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	QUI529720	A	\N	MIA
563214	Richard	Quinn	Richard Quinn	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	QUI710607	A	\N	NO
1248550	Dominique	Rodgers-Cromartie	Dominique Rodgers-Cromartie	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1248550.png	DB	30	ROD616216	A	41	NYG
419850	Antrel	Rolle	Antrel Rolle	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/419850.png	DB	33	ROL403288	IR	26	CHI
1686185	Deji	Karim	Deji Karim	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	KAR283607	IR	\N	IND
1665192	Jake	Knott	Jake Knott	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KNO331697	A	\N	MIA
2130151	Jonathan	Krause	Jonathan Krause	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	KRA641809	A	10	PHI
1752310	Kenny	Ladler	Kenny Ladler	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LAD432619	A	\N	BUF
2174803	B.J.	Larsen	B.J. Larsen	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LAR386135	A	\N	BUF
2058177	Brian	Leonhardt	Brian Leonhardt	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058177.png	TE	26	LEO713846	A	\N	MIN
1631009	Keshawn	Martin	Keshawn Martin	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631009.png	WR	26	MAR670855	A	82	NE
1243187	LeSean	McCoy	LeSean McCoy	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243187.png	RB	27	MCC620232	A	25	BUF
1263596	Kelcie	McCray	Kelcie McCray	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1263596.png	DB	27	MCC669738	A	33	SEA
1974193	Collin	Mooney	Collin Mooney	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1974193.png	RB	30	MOO073055	IR	39	ATL
1762120	C.J.	Mosley	C.J. Mosley	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762120.png	LB	23	MOS361252	A	57	BAL
1272615	Derek	Moye	Derek Moye	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOY111430	A	\N	TEN
564165	Louis	Murphy	Louis Murphy	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564165.png	WR	28	MUR517212	IR	18	TB
1674136	Brandon	Myers	Brandon Myers	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674136.png	TE	30	MYE123904	A	82	TB
417825	Nick	Novak	Nick Novak	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/417825.png	K	34	NOV427288	A	8	HOU
2176550	Kenneth	Penny	Kenneth Penny	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PEN498378	A	\N	KC
1723466	Shaun	Prater	Shaun Prater	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1723466.png	DB	26	PRA149241	A	\N	ARI
1877258	Loucheiz	Purifoy	Loucheiz Purifoy	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PUR428258	A	\N	SEA
1700475	Denard	Robinson	Denard Robinson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700475.png	RB	25	ROB399206	A	16	JAC
2176130	Crishon	Rose	Crishon Rose	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROS153151	A	\N	MIN
1682863	James	Ruffin	James Ruffin	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	RUF428791	A	\N	TB
1749042	Jimmy	Saddler-McQueen	Jimmy Saddler-McQueen	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SAD218895	A	\N	DET
1977172	Jabin	Sambrano	Jabin Sambrano	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SAM011273	A	\N	ATL
1130570	Brian	Sanford	Brian Sanford	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SAN649409	A	\N	DEN
1619691	Sean	Spence	Sean Spence	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619691.png	LB	25	SPE433621	A	\N	TEN
421419	Darren	Sproles	Darren Sproles	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/421419.png	RB	32	SPR711296	A	43	PHI
1754279	Jawanza	Starling	Jawanza Starling	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	STA621292	A	\N	HOU
2174189	Tony	Steward	Tony Steward	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	STE730265	IR	50	BUF
1860883	Josh	Stewart	Josh Stewart	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	STE774702	A	\N	TEN
435413	Tyler	Thigpen	Tyler Thigpen	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	THI677677	A	\N	CLE
1277644	Josh	Thomas	Josh Thomas	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1277644.png	DB	26	THO264264	A	26	DAL
2058358	Lamaar	Thomas	Lamaar Thomas	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	THO281125	A	\N	JAC
2174893	Carlos	Thompson	Carlos Thompson	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	THO498693	A	44	HOU
1691421	Dre	Kirkpatrick	Dre Kirkpatrick	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691421.png	DB	26	KIR621103	A	27	CIN
1749704	Joe	Kruger	Joe Kruger	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	KRU469959	A	\N	PIT
2130845	Travis	Labhart	Travis Labhart	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LAB567555	A	\N	HOU
1754721	Nevin	Lawson	Nevin Lawson	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1754721.png	DB	24	LAW715017	A	24	DET
2174887	Khari	Lee	Khari Lee	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	LEE428943	A	\N	CHI
1133628	Orie	Lemon	Orie Lemon	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LEM716621	A	\N	KC
2174946	Bradley	Marquez	Bradley Marquez	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	MAR305700	A	15	STL
1767514	Dean	Marlowe	Dean Marlowe	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MAR268763	A	29	CAR
1137461	Dexter	McCluster	Dexter McCluster	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137461.png	RB	27	MCC467156	IR	22	TEN
302085	Josh	McCown	Josh McCown	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/302085.png	QB	36	MCC600777	IR	13	CLE
1116439	Danny	McCray	Danny McCray	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116439.png	DB	28	MCC665586	A	40	DAL
1996180	Dan	McCullers-Sanders	Dan McCullers-Sanders	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996180.png	DL	23	MCC732916	A	62	PIT
516960	Zach	Miller	Zach Miller	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MIL625718	A	\N	SEA
2117660	Henoc	Muamba	Henoc Muamba	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MUA415291	A	\N	IND
2060110	Andy	Mulumba	Andy Mulumba	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060110.png	LB	26	MUL787064	A	\N	KC
1762315	Keith	Mumphery	Keith Mumphery	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762315.png	WR	23	MUM540970	A	12	HOU
2117665	Patrick	Murray	Patrick Murray	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2117665.png	K	24	MUR681154	A	7	TB
2130306	Jordan	Najvar	Jordan Najvar	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	NAJ750719	A	\N	DAL
1272388	Chris	Neild	Chris Neild	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	NEI522034	A	\N	HOU
2130101	Roosevelt	Nix-Jones	Roosevelt Nix-Jones	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	NIX511473	A	45	PIT
1700934	Uzoma	Nwachukwu	Uzoma Nwachukwu	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	NWA118653	A	\N	MIA
2174835	Olsen	Pierre	Olsen Pierre	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	PIE579519	A	\N	ARI
1632316	Jerrell	Powe	Jerrell Powe	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632316.png	DL	29	POW036255	A	\N	WAS
1823870	Chris	Prosinski	Chris Prosinski	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1823870.png	DB	28	PRO490429	A	31	CHI
2129630	Jabari	Price	Jabari Price	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129630.png	DB	23	PRI203746	A	25	MIN
1687422	Brian	Quick	Brian Quick	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1687422.png	WR	26	QUI043372	A	83	STL
1630332	Robert	Quinn	Robert Quinn	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630332.png	DL	25	QUI715221	IR	94	STL
2174843	Trevor	Roach	Trevor Roach	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	ROA480616	A	52	CIN
2060281	Rashad	Ross	Rashad Ross	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060281.png	WR	26	ROS672152	A	19	WAS
2174891	James	Rouse	James Rouse	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROU407790	A	\N	HOU
1759955	Ezell	Ruffin	Ezell Ruffin	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	RUF427183	A	\N	IND
2174310	Marcus	Rush	Marcus Rush	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	RUS096467	A	45	SF
1664604	Logan	Ryan	Logan Ryan	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664604.png	DB	25	RYA237161	A	26	NE
1766841	Jalen	Saunders	Jalen Saunders	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SAU676312	A	\N	CHI
1823890	Cecil	Shorts	Cecil Shorts	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1823890.png	WR	28	SHO727038	A	18	HOU
2174990	Joshua	Stangby	Joshua Stangby	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	STA429754	A	\N	IND
2175065	Malcome	Kennedy	Malcome Kennedy	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	KEN459584	A	\N	NO
1824200	Tyler	Kroft	Tyler Kroft	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824200.png	TE	23	KRO074538	A	81	CIN
436019	Danny	Lansanah	Danny Lansanah	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/436019.png	LB	30	LAN698004	A	51	TB
426006	Manny	Lawson	Manny Lawson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/426006.png	DL	31	LAW697295	A	91	BUF
2174840	Matt	Lengel	Matt Lengel	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	25	LEN185694	A	88	CIN
2219912	B.J.	Lowery	B.J. Lowery	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	LOW531970	A	\N	DEN
1700713	Arthur	Lynch	Arthur Lynch	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	LYN065058	A	\N	DEN
1632241	Ryan	Mallett	Ryan Mallett	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632241.png	QB	27	MAL262949	A	7	BAL
1674128	Vaughn	Martin	Vaughn Martin	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MAR748944	A	\N	KC
2132672	Jacobbi	McDaniel	Jacobbi McDaniel	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MCD051243	A	\N	CLE
2174889	Cameron	McLeod	Cameron McLeod	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MCL638835	A	\N	HOU
564095	Mike	Neal	Mike Neal	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564095.png	LB	28	NEA261017	A	96	GB
2169092	Brian	Peters	Brian Peters	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	27	PET095053	A	52	HOU
1696988	Lonnie	Pryor	Lonnie Pryor	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	PRY458541	A	\N	TB
2058552	Mike	Purcell	Mike Purcell	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058552.png	DL	24	PUR087672	A	64	SF
2059199	LaRoy	Reynolds	LaRoy Reynolds	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059199.png	LB	25	REY431570	A	\N	ATL
2060079	Rico	Richardson	Rico Richardson	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	RIC607168	A	\N	TEN
1928408	Craig	Robertson	Craig Robertson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1928408.png	LB	28	ROB304836	A	\N	NO
1737498	Jake	Ryan	Jake Ryan	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	RYA208916	A	47	GB
2189586	Ricky	Seale	Ricky Seale	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SEA305623	A	\N	BUF
2175116	Tim	Semisch	Tim Semisch	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	SEM504298	A	\N	SD
1786001	Andrew	Sendejo	Andrew Sendejo	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1786001.png	DB	28	SEN095404	A	34	MIN
2219918	Ronnie	Shields	Ronnie Shields	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	2016	SHI228903	A	\N	SEA
2174912	Josh	Shirley	Josh Shirley	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	SHI691148	A	\N	SEA
2130881	Scott	Simonson	Scott Simonson	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	SIM449869	A	\N	CAR
1630320	Kenneth	Tate	Kenneth Tate	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	TAT339781	A	\N	FA
2185128	LaVance	Taylor	LaVance Taylor	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TAY507558	A	\N	KC
1673342	Phillip	Taylor	Phillip Taylor	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TAY612318	A	\N	CLE
1664433	Olivier	Vernon	Olivier Vernon	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664433.png	DL	25	VER613364	A	54	NYG
1759387	Clive	Walford	Clive Walford	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759387.png	TE	24	WAL062390	A	88	OAK
1850793	Darren	Waller	Darren Waller	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1850793.png	WR	23	WAL509040	IR	12	BAL
2167565	Brendan	Kelly	Brendan Kelly	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KEL503955	A	\N	SEA
2221883	Cleyon	Laing	Cleyon Laing	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	LAI322107	A	\N	MIA
2174786	Mike	Lee	Mike Lee	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LEE496750	A	\N	PHI
2042810	Donatella	Luckett	Donatella Luckett	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LUC619849	A	\N	SD
1619923	Stacy	McGee	Stacy McGee	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619923.png	DL	26	MCG197653	A	92	OAK
416861	Heath	Miller	Heath Miller	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MIL243606	A	\N	PIT
2060087	Keavon	Milton	Keavon Milton	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060087.png	TE	25	MIL818066	A	\N	NE
2175045	Braylon	Mitchell	Braylon Mitchell	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MIT132351	A	\N	OAK
1272915	Mike	Mitchell	Mike Mitchell	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272915.png	DB	28	MIT560825	A	23	PIT
1679975	Sam	Montgomery	Sam Montgomery	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MON675980	A	\N	CIN
2175054	Corey	Moore	Corey Moore	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MOO232247	A	43	HOU
396875	Kassim	Osgood	Kassim Osgood	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	OSG474619	IR	\N	SF
502816	Curtis	Painter	Curtis Painter	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	PAI701900	A	\N	NYG
1759756	David	Parry	David Parry	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	PAR702152	A	54	IND
418377	Mike	Patterson	Mike Patterson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PAT478313	A	\N	NYG
2130667	Solomon	Patton	Solomon Patton	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PAT781894	A	\N	DEN
2130176	Andrew	Peacock	Andrew Peacock	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PEA098169	A	\N	DET
1265503	Micah	Pellerin	Micah Pellerin	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PEL428038	A	\N	CLE
2059176	Will	Pericak	Will Pericak	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059176.png	DL	26	PER076275	A	74	SEA
2130186	LaDarius	Perkins	LaDarius Perkins	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	PER225693	A	\N	IND
1113452	Brooks	Reed	Brooks Reed	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1113452.png	LB	29	REE154467	A	56	ATL
1277494	Kendall	Reyes	Kendall Reyes	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1277494.png	DL	26	REY020742	A	\N	WAS
1889891	Bobby	Richardson	Bobby Richardson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	RIC390816	A	78	NO
1691614	Theo	Riddick	Theo Riddick	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691614.png	RB	24	RID187949	A	25	DET
1877259	Marcus	Roberson	Marcus Roberson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1877259.png	DB	23	ROB083134	A	47	STL
1137316	Greg	Salas	Greg Salas	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137316.png	WR	27	SAL108421	A	17	BUF
2130154	Kenny	Shaw	Kenny Shaw	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SHA646896	A	\N	OAK
1245674	Derrick	Shelby	Derrick Shelby	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245674.png	DL	27	SHE238558	A	\N	ATL
1243766	Marcus	Sherels	Marcus Sherels	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243766.png	DB	28	SHE563263	A	35	MIN
1117823	Richard	Sherman	Richard Sherman	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117823.png	DB	28	SHE669604	A	25	SEA
2174855	Jameill	Showers	Jameill Showers	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	24	SHO806190	A	\N	DAL
2088442	Tevin	McDonald	Tevin McDonald	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MCD552545	A	37	OAK
1664023	Pernell	McPhee	Pernell McPhee	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664023.png	LB	27	MCP426538	A	92	CHI
1716588	Jerron	McMillian	Jerron McMillian	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MCM604764	A	\N	KC
1630827	Cameron	Meredith	Cameron Meredith	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MER114611	A	\N	FA
1737099	Dee	Milliner	Dee Milliner	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737099.png	DB	24	MIL631115	A	27	NYJ
1253970	Carlton	Mitchell	Carlton Mitchell	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MIT150961	A	\N	ATL
1664400	T.J.	Moe	T.J. Moe	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOE221145	A	\N	STL
2185174	Kevin	Monangai	Kevin Monangai	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	MON116179	A	\N	PHI
1893146	Jeoffrey	Pagan	Jeoffrey Pagan	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1893146.png	DL	22	PAG167803	A	97	HOU
515634	Cedric	Peerman	Cedric Peerman	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/515634.png	RB	29	PEE466390	A	30	CIN
2175096	John	Peters	John Peters	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	PET151496	A	\N	CIN
1753229	Kejuan	Riley	Kejuan Riley	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RIL323794	A	\N	FA
1979735	Tracy	Robertson	Tracy Robertson	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROB360132	A	\N	CHI
1971880	Adrien	Robinson	Adrien Robinson	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971880.png	TE	27	ROB365754	A	\N	NYJ
1681961	Josh	Robinson	Josh Robinson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1681961.png	DB	25	ROB590205	A	\N	TB
2130132	Sammy	Seamster	Sammy Seamster	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	SEA453800	A	44	BUF
1737241	Josh	Shaw	Josh Shaw	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	SHA641197	A	26	CIN
1737297	Prince	Shembo	Prince Shembo	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SHE457237	A	\N	ATL
1884455	Danny	Shelton	Danny Shelton	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1884455.png	DL	22	SHE411954	A	71	CLE
2178805	Kevin	Short	Kevin Short	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	SHO599259	A	\N	NYJ
1673544	Sealver	Siliga	Sealver Siliga	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1673544.png	DL	25	SIL461116	A	\N	SEA
1272073	Jerome	Simpson	Jerome Simpson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272073.png	WR	30	SIM531556	A	14	SF
1693118	Eugene	Sims	Eugene Sims	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1693118.png	DL	30	SIM697600	A	97	STL
2130209	Deontae	Skinner	Deontae Skinner	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	SKI617685	A	54	PHI
2130289	Jocquel	Skinner	Jocquel Skinner	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SKI624510	A	\N	DET
518130	Craig	Steltz	Craig Steltz	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	STE278126	A	\N	CHI
553825	Shaun	Suisham	Shaun Suisham	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/553825.png	K	34	SUI593274	IR	6	PIT
1975965	Phillip	Supernaw	Phillip Supernaw	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975965.png	TE	26	SUP299361	A	89	TEN
1761394	Jaquiski	Tartt	Jaquiski Tartt	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1761394.png	DB	24	TAR686527	A	29	SF
2175120	Carlif	Taylor	Carlif Taylor	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TAY063562	A	\N	DAL
1971902	Keith	Tandy	Keith Tandy	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971902.png	DB	27	TAN126893	A	37	TB
2167525	Tyler	McDonald	Tyler McDonald	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MCD586066	A	\N	MIA
1631960	Rahim	Moore	Rahim Moore	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631960.png	DB	26	MOO585401	A	\N	CLE
1693867	Joseph	Morgan	Joseph Morgan	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOR166792	A	\N	BAL
1977160	Dezman	Moses	Dezman Moses	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977160.png	LB	27	MOS231304	A	54	KC
2062170	Will	Murphy	Will Murphy	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MUR526391	A	\N	PHI
1117392	Hakeem	Nicks	Hakeem Nicks	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117392.png	WR	28	NIC726593	A	88	NYG
565738	Jordan	Norwood	Jordan Norwood	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/565738.png	WR	29	NOR783179	A	11	DEN
2174834	Levi	Norwood	Levi Norwood	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	NOR784683	A	\N	PIT
2174797	Nick	Perry	Nick Perry	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	PER681390	A	\N	BAL
2174775	Terrance	Plummer	Terrance Plummer	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	PLU292278	A	\N	MIN
563138	Zach	Potter	Zach Potter	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	POT426540	A	\N	HOU
1265393	Bilal	Powell	Bilal Powell	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1265393.png	RB	27	POW162772	A	29	NYJ
1697236	Sheldon	Price	Sheldon Price	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1697236.png	DB	25	PRI303263	A	32	BAL
1674142	Glover	Quin	Glover Quin	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674142.png	DB	30	QUI360604	A	27	DET
1632189	Sean	Richardson	Sean Richardson	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632189.png	DB	26	RIC617550	IR	28	GB
1631791	Gerell	Robinson	Gerell Robinson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	ROB607112	A	\N	MIA
1631866	Jacquizz	Rodgers	Jacquizz Rodgers	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631866.png	RB	26	ROD369979	IR	35	CHI
435367	Dante	Rosario	Dante Rosario	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	ROS044493	A	\N	CHI
1123479	Ahtyba	Rubin	Ahtyba Rubin	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123479.png	DL	29	RUB425866	A	77	SEA
1754396	Ryan	Russell	Ryan Russell	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	RUS704203	A	99	DAL
1615605	Trevor	Scott	Trevor Scott	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SCO719246	A	\N	CHI
2042924	James	Shaw	James Shaw	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SHA630479	A	\N	PIT
2174210	Trevor	Siemian	Trevor Siemian	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2174210.png	QB	24	SIE461866	A	13	DEN
2129667	Tyler	Starr	Tyler Starr	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129667.png	LB	25	STA660376	A	\N	ATL
2058191	Robert	Steeples	Robert Steeples	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	STE102638	A	\N	DAL
2059634	Phillip	Steward	Phillip Steward	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	STE729085	A	\N	STL
2130198	Darryl	Surgent	Darryl Surgent	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SUR237308	A	\N	KC
1700403	Daniel	Thomas	Daniel Thomas	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700403.png	RB	28	THO092066	A	30	MIA
563224	Cam	Thomas	Cam Thomas	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563224.png	DL	29	THO063777	A	93	PIT
2130188	Joe	Thomas	Joe Thomas	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130188.png	LB	24	THO234901	A	48	GB
1755056	Julius	Thomas	Julius Thomas	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1755056.png	TE	27	THO265401	A	80	JAC
1675640	Brandon	Thompson	Brandon Thompson	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675640.png	DL	26	THO492537	IR	98	CIN
1752728	Kenbrell	Thompkins	Kenbrell Thompkins	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1752728.png	WR	27	THO445809	A	10	NYJ
2130165	Juwan	Thompson	Juwan Thompson	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2130165.png	RB	23	THO610746	A	40	DEN
1979421	Deonte	Thompson	Deonte Thompson	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979421.png	WR	27	THO528909	IR	14	CHI
1700530	Jared	Abbrederis	Jared Abbrederis	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700530.png	WR	25	ABB650964	A	84	GB
516923	Darrelle	Revis	Darrelle Revis	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/516923.png	DB	30	REV515344	A	24	NYJ
2174200	Christian	Ringo	Christian Ringo	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	RIN485973	A	97	GB
2174999	C.J.	Roberts	C.J. Roberts	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	ROB133163	A	\N	TB
1687529	Andre	Roberts	Andre Roberts	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1687529.png	WR	28	ROB102317	IR	12	WAS
2174218	Edmond	Robinson	Edmond Robinson	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	ROB413992	A	51	MIN
493053	Josh	Scobee	Josh Scobee	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493053.png	K	33	SCO003459	A	\N	NO
1616803	Jordan	Senn	Jordan Senn	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SEN224558	A	\N	CHI
1737094	Lache	Seastrunk	Lache Seastrunk	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SEA669210	A	\N	DAL
1824417	Ryan	Shazier	Ryan Shazier	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824417.png	LB	23	SHA822597	A	50	PIT
1116449	Kelvin	Sheppard	Kelvin Sheppard	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116449.png	LB	28	SHE554427	A	52	MIA
1277500	Anthony	Sherman	Anthony Sherman	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1277500.png	RB	27	SHE644991	A	42	KC
1664790	Rod	Sweeting	Rod Sweeting	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SWE661869	A	\N	DAL
1987619	Alex	Tanney	Alex Tanney	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1987619.png	QB	28	TAN743421	A	11	TEN
1113529	Ben	Tate	Ben Tate	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TAT137889	A	\N	PIT
2057659	Cooper	Taylor	Cooper Taylor	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057659.png	DB	25	TAY122887	A	\N	NYG
1631195	Mike	Taylor	Mike Taylor	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	TAY601350	A	\N	SEA
2174806	Cam	Thomas	Cam Thomas	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	THO063714	A	41	BUF
563723	Tiquan	Underwood	Tiquan Underwood	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	UND628560	A	\N	CAR
2175031	James	Vaughters	James Vaughters	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	VAU759522	A	\N	NE
1682065	Conner	Vernon	Conner Vernon	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	VER606614	A	\N	DET
1244316	Trevin	Wade	Trevin Wade	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244316.png	DB	26	WAD688464	A	31	NYG
1615618	Erik	Walden	Erik Walden	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1615618.png	LB	30	WAL028236	A	93	IND
2130699	Eric	Ward	Eric Ward	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WAR156620	A	\N	CIN
2174864	Josh	Watson	Josh Watson	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WAT439872	A	\N	DEN
2130328	Tony	Washington	Tony Washington	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	WAS566956	A	12	JAC
2114257	Greg	Wilson	Greg Wilson	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL711627	A	\N	DEN
1272242	Russell	Wilson	Russell Wilson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272242.png	QB	27	WIL777781	A	3	SEA
2059637	Darren	Woodard	Darren Woodard	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WOO201159	A	\N	ARI
1860783	Mehdi	Abdesmad	Mehdi Abdesmad	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ABD171953	A	\N	DRF
2176444	Corey	Acosta	Corey Acosta	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	24	ACO461434	A	\N	SF
519918	Cliff	Avril	Cliff Avril	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/519918.png	DL	30	AVR296636	A	56	SEA
1994904	Ben	Braunecker	Ben Braunecker	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BRA692421	A	\N	DRF
2009		Broncos	Broncos	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	328	\N	\N	DEN
2109		Broncos	Broncos	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	228	\N	\N	DEN
1995769	Beniquez	Brown	Beniquez Brown	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BRO268591	A	\N	DRF
395934	Josh	Brown	Josh Brown	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/395934.png	K	36	BRO502703	A	3	NYG
1964		Chiefs	Chiefs	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	503	\N	\N	KC
1860748	Jake	Coker	Jake Coker	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	COK428791	A	\N	DRF
409444	Thomas	DeCoud	Thomas DeCoud	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DEC677964	A	\N	CAR
410009	Justin	Durant	Justin Durant	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DUR116287	A	\N	ATL
2121		Eagles	Eagles	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	211	\N	\N	PHI
563631	Austin	Spitler	Austin Spitler	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SPI629420	A	\N	WAS
1749604	Marquis	Spruill	Marquis Spruill	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SPR791190	A	\N	ATL
1243716	Ricky	Stanzi	Ricky Stanzi	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	STA495069	A	\N	DET
1630840	Baker	Steinkuhler	Baker Steinkuhler	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	STE261904	A	\N	FA
1125485	Luke	Stocker	Luke Stocker	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125485.png	TE	27	STO088140	A	88	TB
1681960	Jamar	Taylor	Jamar Taylor	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1681960.png	DB	25	TAY294506	A	22	MIA
2174191	Randall	Telfer	Randall Telfer	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	TEF385627	A	\N	CLE
1880868	De'Anthony	Thomas	De'Anthony Thomas	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1880868.png	WR	23	THO095665	A	13	KC
1114965	Demaryius	Thomas	Demaryius Thomas	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1114965.png	WR	28	THO095855	A	88	DEN
1975895	Michael	Thomas	Michael Thomas	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975895.png	DB	26	THO308948	A	31	MIA
422172	Terrell	Thomas	Terrell Thomas	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	THO387318	A	\N	SEA
2124752	Jordan	Thompson	Jordan Thompson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	26	THO608955	A	82	DET
2130515	Zach	Thompson	Zach Thompson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	THO742595	IR	\N	BAL
2175122	Mike	Thornton	Mike Thornton	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	THO788709	A	\N	PIT
520021	Lawrence	Timmons	Lawrence Timmons	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/520021.png	LB	29	TIM589720	A	94	PIT
1117814	Scott	Tolzien	Scott Tolzien	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117814.png	QB	28	TOL825549	A	\N	IND
2141225	Jeremy	Towns	Jeremy Towns	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TOW368346	A	\N	PHI
2059179	Brynden	Trawick	Brynden Trawick	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059179.png	DB	26	TRA546413	A	\N	OAK
2060070	Justin	Tuggle	Justin Tuggle	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060070.png	LB	26	TUG633435	A	\N	CLE
2132685	Julius	Warmsley	Julius Warmsley	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	WAR431675	A	\N	MIA
563111	Willie	Young	Willie Young	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563111.png	LB	30	YOU682694	A	97	CHI
1891893	Isa	Abdul-Quddus	Isa Abdul-Quddus	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1891893.png	DB	26	ABD688172	A	\N	MIA
1996155	Alex	Balducci	Alex Balducci	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BAL241701	A	\N	DRF
1851206	Trevone	Boykin	Trevone Boykin	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	BOY620791	A	\N	DRF
1877247	Jacoby	Brissett	Jacoby Brissett	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	BRI516391	A	\N	DRF
1860746	Lamarcus	Brutus	Lamarcus Brutus	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRU819263	A	\N	DRF
1977		Buccaneers	Buccaneers	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	517	\N	\N	TB
1996158	DeForest	Buckner	DeForest Buckner	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BUC768760	A	\N	DRF
1852902	Taveze	Calhoun	Taveze Calhoun	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAL423015	A	\N	DRF
2024		Chargers	Chargers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	314	\N	\N	SD
2124		Chargers	Chargers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	214	\N	\N	SD
2079843	Alex	Collins	Alex Collins	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	COL479374	A	\N	DRF
2000872	Josh	Dawson	Josh Dawson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DAW581373	A	\N	DRF
2175112	Travell	Dixon	Travell Dixon	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	2016	DIX729086	A	\N	CAR
2000903	Kenyan	Drake	Kenyan Drake	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	DRA280410	A	\N	DRF
1977128	Rod	Streater	Rod Streater	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977128.png	WR	28	STR304414	A	\N	KC
1975894	Giorgio	Tavecchio	Giorgio Tavecchio	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975894.png	K	25	TAV164481	A	\N	OAK
2184573	Raymon	Taylor	Raymon Taylor	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	TAY617262	A	\N	IND
1737172	Randall	Telfer	Randall Telfer	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737172.png	TE	23	TEL116643	A	86	CLE
1697293	Manti	Te'o	Manti Te'o	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1697293.png	LB	25	TEO029662	A	50	SD
1692675	Phillip	Thomas	Phillip Thomas	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	27	THO319395	A	\N	BUF
2174121	Mykkele	Thompson	Mykkele Thompson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174121.png	DB	22	THO636131	IR	33	NYG
2130631	Ricky	Tjong-A-Tjoe	Ricky Tjong-A-Tjoe	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TJO444955	A	\N	SD
2078964	Max	Valles	Max Valles	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	21	VAL650439	A	54	BUF
235253	Mike	Vick	Mike Vick	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/235253.png	QB	35	VIC311467	A	2	PIT
1119272	Alterraun	Verner	Alterraun Verner	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1119272.png	DB	27	VER599864	A	21	TB
2174837	Brian	Vogler	Brian Vogler	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	VOG669650	A	\N	IND
559250	Mike	Wallace	Mike Wallace	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559250.png	WR	29	WAL468678	A	\N	BAL
1675042	Reggie	Walker	Reggie Walker	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WAL309457	A	\N	DEN
2174991	Terron	Ward	Terron Ward	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174991.png	RB	24	WAR275043	A	33	ATL
1737108	Jaylen	Watkins	Jaylen Watkins	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737108.png	DB	24	WAT230043	A	37	PHI
423207	Leon	Washington	Leon Washington	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WAS503628	A	\N	TEN
1824167	Sylvester	Williams	Sylvester Williams	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1824167.png	DL	27	WIL531095	A	92	DEN
2175027	Jason	Wilson	Jason Wilson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	REE000001	A	\N	DAL
1998197	Jameis	Winston	Jameis Winston	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1998197.png	QB	22	WIN623708	A	3	TB
1616817	Danny	Woodhead	Danny Woodhead	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616817.png	RB	31	WOO302348	A	39	SD
2025		49ers	49ers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	315	\N	\N	SF
2186554	Will	Brown	Will Brown	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRO748080	A	\N	TEN
2127		Buccaneers	Buccaneers	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	217	\N	\N	TB
2060733	De'Vondre	Campbell	De'Vondre Campbell	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	CAM227922	A	\N	DRF
2079670	Kenny	Clark	Kenny Clark	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CLA409251	A	\N	DRF
2079796	Pharoh	Cooper	Pharoh Cooper	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COO679646	A	\N	DRF
1958		Cowboys	Cowboys	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	527	\N	\N	DAL
1865417	David	Dean	David Dean	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DEA169106	A	\N	DRF
2015		Dolphins	Dolphins	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	304	\N	\N	MIA
2115		Dolphins	Dolphins	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	204	\N	\N	MIA
2041264	Jack	Doyle	Jack Doyle	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2041264.png	TE	25	DOY403968	A	84	IND
1737527	Kony	Ealy	Kony Ealy	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1737527.png	DL	24	EAL784006	A	94	CAR
492929	Dwan	Edwards	Dwan Edwards	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/492929.png	DL	34	EDW667014	A	92	CAR
1620799	Pat	Edwards	Pat Edwards	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	EDW629082	A	\N	DET
535241	Larry	English	Larry English	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/535241.png	DL	30	ENG571656	IR	57	TB
1824993	DeAndre	Elliott	DeAndre Elliott	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ELL278635	A	\N	DRF
2071630	Kendall	Fuller	Kendall Fuller	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FUL342614	A	\N	DRF
1999880	Jake	Ganus	Jake Ganus	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GAN779727	A	\N	DRF
2079275	Christian	Hackenberg	Christian Hackenberg	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	HAC346399	A	\N	DRF
2007634	Javon	Hargrave	Javon Hargrave	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HAR143881	A	\N	DRF
1996270	De'Vante	Harris	De'Vante Harris	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAR363986	A	\N	DRF
1754682	Marqueston	Huff	Marqueston Huff	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1754682.png	DB	24	HUF218767	A	28	TEN
1724952	Margus	Hunt	Margus Hunt	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1724952.png	DL	28	HUN320751	A	99	CIN
2058343	Justin	Staples	Justin Staples	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058343.png	LB	26	STA495813	A	57	TEN
421946	Jacob	Tamme	Jacob Tamme	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/421946.png	TE	31	TAM511848	A	83	ATL
517808	Aqib	Talib	Aqib Talib	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/517808.png	DB	30	TAL428789	A	21	DEN
2174805	A.J.	Tarpley	A.J. Tarpley	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	TAR428317	A	59	BUF
518136	Curtis	Taylor	Curtis Taylor	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	TAY137718	A	\N	ARI
2174863	Jordan	Taylor	Jordan Taylor	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	TAY439186	A	87	DEN
1114680	Tim	Tebow	Tim Tebow	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	TEB603856	A	\N	PHI
2174790	Robenson	Therezie	Robenson Therezie	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	THE622937	A	27	ATL
1691193	Logan	Thomas	Logan Thomas	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691193.png	QB	24	THO296280	A	5	MIA
2130341	D.J.	Tialavea	D.J. Tialavea	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	TIA385627	A	86	ATL
1737426	Lynden	Trail	Lynden Trail	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	TRA214132	A	\N	WAS
1737224	George	Uko	George Uko	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	UKO029662	A	\N	DEN
1273006	Shane	Vereen	Shane Vereen	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273006.png	RB	27	VER287449	A	34	NYG
2130422	Lawrence	Virgil	Lawrence Virgil	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	VIR265610	IR	96	NO
584113	Vance	Walker	Vance Walker	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/584113.png	DL	28	WAL347391	A	96	DEN
2132693	Jansen	Watson	Jansen Watson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WAT407729	A	\N	OAK
2060053	C.J.	Wilson	C.J. Wilson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WIL662886	A	\N	TB
1621707	Kion	Wilson	Kion Wilson	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL740292	A	\N	ARI
187381	John	Abraham	John Abraham	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ABR073003	IR	\N	ARI
1697774	Phillip	Adams	Phillip Adams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1697774.png	DB	27	ADA534252	A	20	ATL
2184575	Jude	Adjei-Barimah	Jude Adjei-Barimah	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	ADJ177981	A	38	TB
1871342	Michael	Caputo	Michael Caputo	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAP759395	A	\N	DRF
1951		Cardinals	Cardinals	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	512	\N	\N	ARI
1877146	Lloyd	Carrington	Lloyd Carrington	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAR496421	A	\N	DRF
2108		Cowboys	Cowboys	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	227	\N	\N	DAL
2001577	Brad	Craddock	Brad Craddock	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	CRA118921	A	\N	DRF
409254	Vernon	Davis	Vernon Davis	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/409254.png	TE	32	DAV785142	A	\N	WAS
2184574	Kai	De La Cruz	Kai De La Cruz	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DEL036890	A	\N	MIA
1226160	Mike	DeVito	Mike DeVito	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1226160.png	DL	31	DEV250058	A	70	KC
563992	Louis	Delmas	Louis Delmas	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563992.png	DB	28	DEL523629	IR	25	MIA
1977114	Marcus	Dowtin	Marcus Dowtin	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DOW805731	A	\N	SEA
1824553	Jahwan	Edwards	Jahwan Edwards	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	EDW513954	A	\N	MIA
1737092	Matt	Elam	Matt Elam	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737092.png	DB	24	ELA551596	IR	26	BAL
1108918	Fred	Evans	Fred Evans	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	EVA202016	A	\N	MIN
1996562	Ka'imi	Fairbairn	Ka'imi Fairbairn	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	FAI144264	A	\N	DRF
1877398	Keyarris	Garrett	Keyarris Garrett	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GAR480903	A	\N	DRF
2174933	Julian	Howsare	Julian Howsare	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	HOW805902	A	\N	NYJ
1891856	Robert	Hughes	Robert Hughes	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	26	HUG592048	A	\N	ARI
2175124	Adam	Humphries	Adam Humphries	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	22	HUM721507	A	\N	TB
1243592	Kendall	Hunter	Kendall Hunter	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243592.png	RB	27	HUN719158	IR	48	NO
2013		Jaguars	Jaguars	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	322	\N	\N	JAC
2059362	Adam	Thielen	Adam Thielen	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059362.png	WR	25	THI510348	A	19	MIN
1245247	Earl	Thomas	Earl Thomas	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245247.png	DB	26	THO124271	A	29	SEA
2174120	Cedric	Thompson	Cedric Thompson	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	THO498788	A	\N	NE
1273546	Chris	Thompson	Chris Thompson	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273546.png	RB	25	THO499404	A	25	WAS
1704674	Cedric	Thornton	Cedric Thornton	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1704674.png	DL	27	THO767983	A	\N	DAL
396100	Charles	Tillman	Charles Tillman	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/396100.png	DB	35	TIL389442	IR	31	CAR
1636732	Jordan	Todman	Jordan Todman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1636732.png	RB	26	TOD628353	A	\N	IND
1263572	Nick	Toon	Nick Toon	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1263572.png	WR	27	TOO505098	A	81	STL
1854818	Justin	Trattou	Justin Trattou	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1854818.png	DL	27	TRA432591	A	\N	MIN
1737455	Louis	Trinca-Pasat	Louis Trinca-Pasat	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	TRI245190	A	62	STL
1664486	Desmond	Trufant	Desmond Trufant	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664486.png	DB	25	TRU381665	A	21	ATL
559515	Frank	Zombo	Frank Zombo	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559515.png	LB	29	ZOM428791	A	51	KC
1824308	Ameer	Abdullah	Ameer Abdullah	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824308.png	RB	22	ABD647726	A	21	DET
2130621	Torrence	Allen	Torrence Allen	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130621.png	WR	25	ALL650766	IR	3	SD
1886795	James	Bradberry	James Bradberry	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRA092789	A	\N	DRF
1999382	Vernon	Butler	Vernon Butler	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BUT599965	A	\N	DRF
1851138	Devon	Cajuste	Devon Cajuste	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CAJ652601	A	\N	DRF
2101		Cardinals	Cardinals	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	212	\N	\N	ARI
2060621	Maliek	Collins	Maliek Collins	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COL633184	A	\N	DRF
1993152	Marshaun	Coprich	Marshaun Coprich	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	COP773056	A	\N	DRF
1999385	Kenneth	Dixon	Kenneth Dixon	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	DIX440855	A	\N	DRF
1965		Dolphins	Dolphins	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	504	\N	\N	MIA
1616289	Jo-Lonn	Dunbar	Jo-Lonn Dunbar	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DUN097263	A	\N	NO
2079673	Thomas	Duarte	Thomas Duarte	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	DUA426541	A	\N	DRF
1971913	Jeremy	Ebert	Jeremy Ebert	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	EBE679400	A	\N	ATL
410129	Trent	Edwards	Trent Edwards	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	EDW720778	A	\N	OAK
1886798	Darion	Griswold	Darion Griswold	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	GRI826107	A	\N	DRF
1868397	Joel	Heath	Joel Heath	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HEA727230	A	\N	DRF
2062183	Michael	Hill	Michael Hill	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HIL532525	A	\N	DAL
2080247	Johnny	Holton	Johnny Holton	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HOL773557	A	\N	DRF
1620126	Davon	House	Davon House	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620126.png	DB	26	HOU242346	A	31	JAC
1631100	Travis	Howard	Travis Howard	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631100.png	DB	26	HOW418663	IR	33	ATL
2091705	A.J.	Jenkins	A.J. Jenkins	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JEN103820	A	\N	KC
1724711	Cam	Johnson	Cam Johnson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1724711.png	LB	25	JOH088779	A	57	CLE
2186071	Dakorey	Johnson	Dakorey Johnson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JOH146223	A	\N	DAL
1632292	Dennis	Johnson	Dennis Johnson	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	JOH186619	A	\N	HOU
2227174	Tautvydas	Kieras	Tautvydas Kieras	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	2016	KIE520122	A	\N	KC
1886892	Jay	Lee	Jay Lee	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LEE382497	A	\N	DRF
2060774	Darron	Lee	Darron Lee	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LEE144802	A	\N	DRF
2131386	Roger	Lewis	Roger Lewis	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LEW642806	A	\N	DRF
1693102	Sean	Lissemore	Sean Lissemore	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1693102.png	DL	28	LIS569586	IR	98	SD
396021	Brandon	Lloyd	Brandon Lloyd	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LLO154116	A	\N	SF
1752635	Star	Lotulelei	Star Lotulelei	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1752635.png	DL	26	LOT652596	A	98	CAR
2000104	Ricardo	Louis	Ricardo Louis	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LOU665833	A	\N	DRF
2006432	Jordan	Lucas	Jordan Lucas	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LUC068819	A	\N	DRF
1999663	Paxton	Lynch	Paxton Lynch	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	LYN526034	A	\N	DRF
1123324	Jeremy	Maclin	Jeremy Maclin	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123324.png	WR	27	MAC710352	A	19	KC
2060777	Jalin	Marshall	Jalin Marshall	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MAR417066	A	\N	DRF
1995673	Cassanova	McKinzy	Cassanova McKinzy	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MCK677270	A	\N	DRF
1983762	Eric	Murray	Eric Murray	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MUR615618	A	\N	DRF
2005661	Romeo	Okwara	Romeo Okwara	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OKW059326	A	\N	DRF
2175026	Ray	Vinopal	Ray Vinopal	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	VIN522705	A	\N	PIT
2060059	Casey	Walker	Casey Walker	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060059.png	DL	26	WAL089842	A	63	DAL
1762370	Jimmie	Ward	Jimmie Ward	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762370.png	DB	24	WAR218386	A	25	SF
2129521	Avery	Williamson	Avery Williamson	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129521.png	LB	24	WIL577995	A	54	TEN
1971666	Tavon	Wilson	Tavon Wilson	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971666.png	DB	26	WIL791595	A	\N	DET
2125		49ers	49ers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	215	\N	\N	SF
518624	Robert	Ayers	Robert Ayers	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518624.png	DL	30	AYE577722	A	\N	TB
2131124	Devante	Bond	Devante Bond	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BON136909	A	\N	DRF
2061491	Devontae	Booker	Devontae Booker	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BOO019902	A	\N	DRF
1980		Browns	Browns	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	519	\N	\N	CLE
1998305	Deon	Bush	Deon Bush	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BUS216856	A	\N	DRF
1868388	Shilique	Calhoun	Shilique Calhoun	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CAL422827	A	\N	DRF
1752540	Kivon	Cartwright	Kivon Cartwright	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	CAR806161	A	\N	DRF
2114		Chiefs	Chiefs	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	203	\N	\N	KC
2012		Colts	Colts	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	302	\N	\N	IND
2061725	Kamalei	Correa	Kamalei Correa	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COR649558	A	\N	DRF
2008		Cowboys	Cowboys	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	327	\N	\N	DAL
2219915	Tyler	Davis	Tyler Davis	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	2016	DAV781635	A	\N	MIA
2135489	Todd	Davis	Todd Davis	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	DAV768914	A	51	DEN
2130827	Brandon	Dunn	Brandon Dunn	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	DUN512273	A	92	HOU
1277467	Marcus	Easley	Marcus Easley	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1277467.png	WR	28	EAS089979	A	81	BUF
2102		Falcons	Falcons	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	201	\N	\N	ATL
1892464	Blake	Frohnapfel	Blake Frohnapfel	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	FRO186366	A	\N	DRF
2130336	Ricky	Heimuli	Ricky Heimuli	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HEI424290	IR	\N	ATL
1888046	Temarrick	Hemingway	Temarrick Hemingway	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HEM189859	A	\N	DRF
413146	Chris	Houston	Chris Houston	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HOU439651	A	\N	CAR
2185869	DreQuan	Hoskey	DreQuan Hoskey	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HOS183645	A	\N	WAS
1877278	John	Jenkins	John Jenkins	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1877278.png	DL	26	JEN271683	A	92	NO
413740	Tim	Jennings	Tim Jennings	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JEN629013	A	\N	TB
2019		Jets	Jets	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	310	\N	\N	NYJ
1969		Jets	Jets	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	510	\N	\N	NYJ
1133439	Charles	Johnson	Charles Johnson	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1133439.png	DL	29	JOH106479	A	95	CAR
518847	Marcus	Thigpen	Marcus Thigpen	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	THI675348	A	\N	BUF
2174311	Dylan	Thompson	Dylan Thompson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	24	THO537623	A	13	SF
1971885	Taylor	Thompson	Taylor Thompson	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	THO702434	A	\N	TEN
2174836	John	Timu	John Timu	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	TIM755074	A	53	CHI
1685990	Levine	Toilolo	Levine Toilolo	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1685990.png	TE	24	TOI581408	A	80	ATL
1616887	Mike	Tolbert	Mike Tolbert	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1616887.png	RB	30	TOL160452	A	35	CAR
2130327	Zurlon	Tipton	Zurlon Tipton	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TIP645432	A	\N	IND
2175092	Abou	Toure	Abou Toure	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	25	TOU383726	A	\N	PIT
2174961	Justin	Tukes	Justin Tukes	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	TUK177981	A	\N	PHI
584170	T.J.	Ward	T.J. Ward	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/584170.png	DB	29	WAR274773	A	43	DEN
1632484	Emmanuel	Acho	Emmanuel Acho	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ACH665901	A	\N	PHI
1959		Broncos	Broncos	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	528	\N	\N	DEN
1860848	Kentrell	Brothers	Kentrell Brothers	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BRO204395	A	\N	DRF
2071570	Artie	Burns	Artie Burns	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BUR507944	A	\N	DRF
2001803	Leonte	Carroo	Leonte Carroo	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CAR559411	A	\N	DRF
1824409	Jeremy	Cash	Jeremy Cash	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAS270566	A	\N	DRF
1824390	Theiren	Cockran	Theiren Cockran	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COC469423	A	\N	DRF
2112		Colts	Colts	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	202	\N	\N	IND
1765589	Brandon	Doughty	Brandon Doughty	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	DOU162535	A	\N	DRF
1691609	Shaq	Evans	Shaq Evans	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691609.png	WR	25	EVA781924	A	\N	JAC
1850942	Tyler	Ervin	Tyler Ervin	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	ERV286234	A	\N	DRF
1877310	Josh	Forrest	Josh Forrest	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FOR496380	A	\N	DRF
1968		Giants	Giants	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	508	\N	\N	NYG
1850728	B.J.	Goodson	B.J. Goodson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GOO568123	A	\N	DRF
1998206	Adam	Gotsis	Adam Gotsis	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	GOT428790	A	\N	DRF
1853103	Tyler	Higbee	Tyler Higbee	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HIG033222	A	\N	DRF
396831	Cullen	Jenkins	Cullen Jenkins	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396831.png	DL	35	JEN173573	A	99	NYG
1262942	Jarvis	Jenkins	Jarvis Jenkins	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1262942.png	DL	27	JEN269130	A	\N	NYJ
1860773	Quinton	Jefferson	Quinton Jefferson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JEF350399	A	\N	DRF
552552	Derrick	Johnson	Derrick Johnson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/552552.png	LB	33	JOH202085	A	56	KC
1979430	Cordarro	Law	Cordarro Law	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979430.png	DL	27	LAW022612	A	99	SD
1242997	Greg	Little	Greg Little	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1242997.png	WR	26	LIT425789	A	\N	BUF
2001223	Cory	Littleton	Cory Littleton	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LIT776417	A	\N	DRF
1984182	Keith	Marshall	Keith Marshall	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	MAR429932	A	\N	DRF
2000859	Alex	McCalister	Alex McCalister	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MCC057244	A	\N	DRF
1998595	Paul	McRoberts	Paul McRoberts	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MCR584270	A	\N	DRF
2175147	Ryan	Mueller	Ryan Mueller	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	MUE507576	A	\N	PHI
2130322	Nnamdi	Obukwelu	Nnamdi Obukwelu	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OBU355964	A	\N	IND
2105		Panthers	Panthers	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	220	\N	\N	CAR
2174924	Eric	Patterson	Eric Patterson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	PAT443397	A	24	STL
2131655	Jarran	Reed	Jarran Reed	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	REE257783	A	\N	DRF
2061251	Rashard	Robinson	Rashard Robinson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ROB680485	A	\N	DRF
2174848	De'Ante	Saunders	De'Ante Saunders	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SAU673603	A	\N	CLE
1979386	Neiko	Thorpe	Neiko Thorpe	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979386.png	DB	26	THO820235	A	31	OAK
1674151	Greg	Toler	Greg Toler	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674151.png	DB	31	TOL296694	A	28	IND
2175406	Charles	Tuaau	Charles Tuaau	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	TUA039550	A	\N	MIA
1226373	Jason	Trusnik	Jason Trusnik	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1226373.png	LB	31	TRU694033	A	59	MIN
1665158	Matthew	Tucker	Matthew Tucker	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TUC250412	A	\N	PHI
1853419	LaQuan	Williams	LaQuan Williams	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL416260	A	\N	BAL
1245216	Sam	Acho	Sam Acho	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245216.png	LB	27	ACH678651	A	49	CHI
2062128	Akeem	Auguste	Akeem Auguste	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	AUG542470	A	\N	SEA
2130		Browns	Browns	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	219	\N	\N	CLE
2000846	Jonathan	Bullard	Jonathan Bullard	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BUL366378	A	\N	DRF
1833003	Kevin	Byard	Kevin Byard	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BYA251361	A	\N	DRF
2001251	Corey	Coleman	Corey Coleman	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COL232728	A	\N	DRF
2001500	Trevon	Coley	Trevon Coley	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COL378757	A	\N	DRF
1962		Colts	Colts	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	502	\N	\N	IND
2000766	Ken	Crawley	Ken Crawley	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CRA805255	A	\N	DRF
1984615	Sheldon	Day	Sheldon Day	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DAY392066	A	\N	DRF
1630773	Kip	Edwards	Kip Edwards	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	EDW568534	A	\N	TB
2001882	Willie	Henry	Willie Henry	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HEN713344	A	\N	DRF
2079848	Hunter	Henry	Hunter Henry	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HEN617052	A	\N	DRF
2067004	Austin	Hooper	Austin Hooper	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HOO570770	A	\N	DRF
2130318	Tyler	Hoover	Tyler Hoover	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HOO782501	A	\N	IND
2175101	Darius	Jennings	Darius Jennings	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	JEN465565	A	\N	CLE
1673492	Marcel	Jensen	Marcel Jensen	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1673492.png	TE	26	JEN740137	A	85	WAS
1115035	George	Johnson	George Johnson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1115035.png	DL	28	JOH261528	A	94	TB
492973	Greg	Jones	Greg Jones	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	JON348810	A	\N	NO
1824713	Cody	Kessler	Cody Kessler	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	KES701662	A	\N	DRF
2058360	Al	Lapuaho	Al Lapuaho	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LAP755595	A	\N	PIT
1621529	Jamari	Lattimore	Jamari Lattimore	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1621529.png	LB	27	LAT681499	A	54	NYJ
2110		Lions	Lions	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	229	\N	\N	DET
1984364	Kolby	Listenbee	Kolby Listenbee	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LIS582020	A	\N	DRF
563881	Curtis	Lofton	Curtis Lofton	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563881.png	LB	29	LOF267017	A	50	OAK
1893070	Jordan	Lomax	Jordan Lomax	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LOM688721	A	\N	DRF
1116474	Kyle	Love	Kyle Love	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1116474.png	DL	29	LOV026391	A	93	CAR
1631912	Andrew	Luck	Andrew Luck	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631912.png	QB	26	LUC524055	A	12	IND
1851173	Curt	Maggitt	Curt Maggitt	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MAG332205	A	\N	DRF
2186084	Joe	Okafor	Joe Okafor	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OKA218895	A	\N	PIT
1996808	Emmanuel	Ogbah	Emmanuel Ogbah	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OGB059326	A	\N	DRF
1737261	Leon	Orr	Leon Orr	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	ORR341795	A	93	OAK
1851262	Montese	Overton	Montese Overton	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	OVE488598	A	\N	DRF
2011		Packers	Packers	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	330	\N	\N	GB
2000862	Brian	Poole	Brian Poole	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	POO231564	A	\N	DRF
1854263	Michael	Preston	Michael Preston	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PRE581406	A	\N	MIA
1751796	Jay	Prosch	Jay Prosch	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1751796.png	RB	23	PRO476929	A	45	HOU
1971889	Korey	Toomer	Korey Toomer	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971889.png	LB	27	TOO356982	A	54	OAK
2175071	Will	Tye	Will Tye	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2175071.png	TE	24	TYE029662	A	45	NYG
2130423	Pierre	Warren	Pierre Warren	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WAR714904	A	\N	NO
1737170	Jordan	Zumwalt	Jordan Zumwalt	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737170.png	LB	24	ZUM711929	IR	56	PIT
492886	Jason	Babin	Jason Babin	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/492886.png	LB	35	BAB714004	A	58	ARI
235197	Drew	Brees	Drew Brees	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/235197.png	QB	37	BRE229498	A	9	NO
2030		Browns	Browns	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	319	\N	\N	CLE
2027		Buccaneers	Buccaneers	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	317	\N	\N	TB
2001905	Aaron	Burbridge	Aaron Burbridge	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BUR012358	A	\N	DRF
1850800	Juston	Burris	Juston Burris	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BUR608462	A	\N	DRF
1889915	Kyle	Carter	Kyle Carter	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	CAR687523	A	\N	DRF
2014		Chiefs	Chiefs	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	303	\N	\N	KC
1893068	Henry Krieger	Coble	Henry Krieger Coble	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	KRI288110	A	\N	DRF
1998285	Steven	Daniels	Steven Daniels	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DAN587968	A	\N	DRF
1825087	Josh	Doctson	Josh Doctson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DOC662393	A	\N	DRF
2002		Falcons	Falcons	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	301	\N	\N	ATL
1952		Falcons	Falcons	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	501	\N	\N	ATL
1884443	Travis	Feeney	Travis Feeney	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FEE576222	A	\N	DRF
2000164	Kavon	Frazier	Kavon Frazier	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FRA777401	A	\N	DRF
2118		Giants	Giants	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	208	\N	\N	NYG
2018		Giants	Giants	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	308	\N	\N	NYG
1996827	Johnathan	Gray	Johnathan Gray	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	GRA695426	A	\N	DRF
2007162	Deiondre'	Hall	Deiondre' Hall	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAL270555	A	\N	DRF
1245857	John	Hughes	John Hughes	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245857.png	DL	27	HUG532670	A	93	CLE
1695507	Montori	Hughes	Montori Hughes	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1695507.png	DL	25	HUG545058	A	79	NYG
2001257	Xavien	Howard	Xavien Howard	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HOW492325	A	\N	DRF
2130337	Allen	Hurns	Allen Hurns	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130337.png	WR	24	HUR436342	A	88	JAC
1760035	Martin	Ifedi	Martin Ifedi	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	IFE148317	A	\N	TB
2060137	Charles	James	Charles James	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060137.png	DB	25	JAM126088	IR	31	HOU
1824996	Cory	James	Cory James	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JAM147142	A	\N	DRF
1886730	Paul	James	Paul James	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	JAM420649	A	\N	DRF
1762313	Jeremy	Langford	Jeremy Langford	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762313.png	RB	24	LAN550009	A	33	CHI
2130818	Marcus	Lucas	Marcus Lucas	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	LUC080913	A	\N	CHI
2007569	John	Lunsford	John Lunsford	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	LUN646666	A	\N	DRF
2175067	Ashaad	Mabry	Ashaad Mabry	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	MAB422041	A	74	NO
2167526	Leon	Mackey	Leon Mackey	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MAC551889	A	\N	MIN
1850832	Luther	Maddy	Luther Maddy	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MAD499685	A	\N	DRF
559366	Kaluka	Maiava	Kaluka Maiava	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MAI059326	IR	\N	OAK
2167520	Chris	Manhertz	Chris Manhertz	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	MAN285874	A	86	NO
565222	Mario	Manningham	Mario Manningham	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MAN547041	IR	\N	NYG
2175683	Travis	Manning	Travis Manning	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAN000000	A	\N	NO
1737549	Kareem	Martin	Kareem Martin	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737549.png	DL	24	MAR665531	A	96	ARI
1996541	Blake	Martinez	Blake Martinez	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MAR776230	A	\N	DRF
1983609	Tyler	Matakevich	Tyler Matakevich	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MAT026118	A	\N	DRF
1984170	Antonio	Morrison	Antonio Morrison	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MOR564646	A	\N	DRF
2079896	Robert	Nkemdiche	Robert Nkemdiche	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	NKE415291	A	\N	DRF
1264791	Cheta	Ozougwu	Cheta Ozougwu	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OZO652601	A	\N	NO
1961		Packers	Packers	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	530	\N	\N	GB
2031		Patriots	Patriots	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	306	\N	\N	NE
1725141	Blidi	Wreh-Wilson	Blidi Wreh-Wilson	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1725141.png	DB	26	WRE233874	A	25	TEN
1825122	Teddy	Bridgewater	Teddy Bridgewater	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825122.png	QB	23	BRI129168	A	5	MIN
1984579	James	Burgess	James Burgess	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BUR081920	A	\N	DRF
2000050	Maurice	Canady	Maurice Canady	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAN149679	A	\N	DRF
2001		Cardinals	Cardinals	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	312	\N	\N	ARI
1974		Chargers	Chargers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	514	\N	\N	SD
1243171	Dom	DeCicco	Dom DeCicco	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DEC453043	A	\N	MIN
436547	Harry	Douglas	Harry Douglas	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/436547.png	WR	31	DOU372712	A	83	TEN
1850983	Kyler	Fackrell	Kyler Fackrell	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FAC355963	A	\N	DRF
2060685	Clayton	Fejedelem	Clayton Fejedelem	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FEJ177981	A	\N	DRF
2061110	Leonard	Floyd	Leonard Floyd	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FLO758060	A	\N	DRF
1825507	DeAndre	Houston-Carson	DeAndre Houston-Carson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HOU781909	A	\N	DRF
565760	Josh	Hull	Josh Hull	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HUL115822	A	\N	JAC
1824723	Brett	Hundley	Brett Hundley	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824723.png	QB	22	HUN005842	A	7	GB
2174880	Jimmie	Hunt	Jimmie Hunt	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HUN258289	IR	\N	GB
1751883	Carlos	Hyde	Carlos Hyde	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1751883.png	RB	24	HYD402541	IR	28	SF
2079677	Myles	Jack	Myles Jack	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JAC008463	A	\N	DRF
1860925	Branden	Jackson	Branden Jackson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JAC000003	A	\N	DRF
1983808	Jesse	James	Jesse James	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1983808.png	TE	21	JAM239797	A	81	PIT
2060116	Willie	Jefferson	Willie Jefferson	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060116.png	LB	25	JEF472449	A	\N	WAS
1137321	M.D.	Jennings	M.D. Jennings	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JEN573172	A	\N	TB
1996855	Derrick	Kindred	Derrick Kindred	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	KIN192537	A	\N	DRF
1983592	Raphael	Kirby	Raphael Kirby	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KIR092228	A	\N	DRF
1880836	Daniel	Lasco	Daniel Lasco	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	LAS076669	A	\N	DRF
2010		Lions	Lions	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	329	\N	\N	DET
2079762	Keanu	Neal	Keanu Neal	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	NEA222964	A	\N	DRF
1860759	Giorgio	Newberry	Giorgio Newberry	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	NEW132258	A	\N	DRF
1825054	Jared	Norris	Jared Norris	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	NOR463803	A	\N	DRF
1996859	Jaden	Oberkrom	Jaden Oberkrom	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	OBE669373	A	\N	DRF
2059273	Tristan	Okpalaugo	Tristan Okpalaugo	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059273.png	DL	26	OKP059326	A	\N	ARI
1998478	Drew	Ott	Drew Ott	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OTT011534	A	\N	DRF
1737115	Ronald	Powell	Ronald Powell	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	POW479487	A	\N	TB
1724829	De'Andre	Presley	De'Andre Presley	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PRE314613	A	\N	CAR
1631107	Terrelle	Pryor	Terrelle Pryor	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631107.png	WR	26	PRY474541	A	17	CLE
1117622	Andrew	Quarless	Andrew Quarless	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117622.png	TE	27	QUA373212	IR	81	GB
1973		Rams	Rams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	523	\N	\N	STL
1243006	Devon	Ramsay	Devon Ramsay	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	RAM180644	A	\N	FA
2003		Ravens	Ravens	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	325	\N	\N	BAL
1865647	Thomas	Rawls	Thomas Rawls	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1865647.png	RB	22	RAW440791	IR	34	SEA
2017		Saints	Saints	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	307	\N	\N	NO
559373	Mark	Sanchez	Mark Sanchez	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/559373.png	QB	29	SAN091667	A	\N	DEN
2129494	Tom	Savage	Tom Savage	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129494.png	QB	25	SAV166116	IR	3	HOU
1851127	Anthony	Sarao	Anthony Sarao	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SAR136909	A	\N	DRF
1974182	Pat	Schiller	Pat Schiller	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SCH171780	A	\N	STL
1877990	Steven	Scheu	Steven Scheu	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SCH136315	A	\N	DRF
1971916	Greg	Scruggs	Greg Scruggs	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971916.png	DL	25	SCR668150	A	90	CHI
1976		Seahawks	Seahawks	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	516	\N	\N	SEA
2010724	Tajae	Sharpe	Tajae Sharpe	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SHA495155	A	\N	DRF
1983799	Noah	Spence	Noah Spence	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SPE433467	A	\N	DRF
2175118	Ben	Edwards	Ben Edwards	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	EDW114134	A	83	NYG
1971		Eagles	Eagles	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	511	\N	\N	PHI
2021		Eagles	Eagles	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	311	\N	\N	PHI
1998180	Mario	Edwards	Mario Edwards	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	EDW608536	A	97	OAK
1691608	Tyler	Eifert	Tyler Eifert	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691608.png	TE	25	EIF131143	A	85	CIN
518503	Dannell	Ellerbe	Dannell Ellerbe	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518503.png	LB	30	ELL108165	A	59	NO
2130181	Jay	Elliott	Jay Elliott	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	ELL000000	A	91	GB
1684629	Kenrick	Ellis	Kenrick Ellis	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1684629.png	DL	28	ELL584769	A	72	MIN
1691546	Gavin	Escobar	Gavin Escobar	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691546.png	TE	25	ESC568293	A	89	DAL
1868396	Darien	Harris	Darien Harris	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAR360107	A	\N	DRF
2174785	Warren	Herring	Warren Herring	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HER565669	A	\N	ATL
2071919	Rashard	Higgins	Rashard Higgins	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HIG322107	A	\N	DRF
2174930	Akeem	Hunt	Akeem Hunt	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	HUN095764	A	33	HOU
1107218	Tarvaris	Jackson	Tarvaris Jackson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1107218.png	QB	32	JAC566507	A	7	SEA
1963		Jaguars	Jaguars	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	522	\N	\N	JAC
2042647	Stanley	Jean-Baptiste	Stanley Jean-Baptiste	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2042647.png	DB	25	JEA413603	A	\N	SEA
1630493	D.C.	Jefferson	D.C. Jefferson	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	JEF171398	IR	\N	CAR
413660	Mike	Jenkins	Mike Jenkins	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/413660.png	DB	31	JEN421854	A	24	TB
2042447	Austin	Johnson	Austin Johnson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	26	JOH064962	A	\N	NO
2119		Jets	Jets	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	210	\N	\N	NYJ
1137327	David	Johnson	David Johnson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137327.png	TE	28	JOH174852	A	88	SD
2174865	Isaiah	Johnson	Isaiah Johnson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	JOH321472	A	43	DET
1983783	Cardale	Jones	Cardale Jones	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	JON148555	A	\N	DRF
1686740	Austen	Lane	Austen Lane	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LAN213708	A	\N	CHI
1971901	Jeremy	Lane	Jeremy Lane	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971901.png	DB	25	LAN313217	A	20	SEA
2130162	Greg	Latta	Greg Latta	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LAT644910	IR	\N	DEN
2079214	Darius	Latham	Darius Latham	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LAT042617	A	\N	DRF
1983523	Shaq	Lawson	Shaq Lawson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LAW783964	A	\N	DRF
415246	Brian	Leonard	Brian Leonard	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	LEO380682	A	\N	NO
1960		Lions	Lions	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	529	\N	\N	DET
1630900	Corey	Liuget	Corey Liuget	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630900.png	DL	26	LIU237308	IR	94	SD
1664945	Bennie	Logan	Bennie Logan	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664945.png	DL	26	LOG113260	IR	96	PHI
2060459	Deon	Long	Deon Long	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	LON250134	A	\N	STL
2174945	Matt	Longacre	Matt Longacre	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	LON664460	A	96	STL
2061042	Antonio	Longino	Antonio Longino	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LON691077	A	\N	DRF
2175041	Cameron	Lynch	Cameron Lynch	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	LYN073558	A	50	STL
1759342	Jake	McGee	Jake McGee	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MCG169303	A	\N	DRF
2006767	Harlan	Miller	Harlan Miller	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIL281637	A	\N	DRF
1984271	Jalen	Mills	Jalen Mills	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIL688675	A	\N	DRF
1996576	Jordan	Payton	Jordan Payton	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PAY611897	A	\N	DRF
2071515	Jalen	Ramsey	Jalen Ramsey	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RAM435895	A	\N	DRF
1998998	Sheldon	Rankins	Sheldon Rankins	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	RAN676464	A	\N	DRF
2029		Redskins	Redskins	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	318	\N	\N	WAS
1996836	Hassan	Ridgeway	Hassan Ridgeway	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	RID299361	A	\N	DRF
565774	Mickey	Shuler	Mickey Shuler	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SHU338232	A	\N	ATL
2005665	Elijah	Shumate	Elijah Shumate	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SHU544491	A	\N	DRF
1890093	Deon	Simon	Deon Simon	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	SIM361296	A	\N	NYJ
1977801	Matt	Simms	Matt Simms	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977801.png	QB	27	SIM341997	A	\N	ATL
1687371	Armanti	Edwards	Armanti Edwards	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	EDW112351	A	\N	CHI
1880851	Ifo	Ekpre-Olomu	Ifo Ekpre-Olomu	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	EKP563610	A	\N	MIA
1620697	Lavar	Edwards	Lavar Edwards	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620697.png	DL	25	EDW579603	A	93	BUF
2060769	Ezekiel	Elliott	Ezekiel Elliott	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	ELL289284	A	\N	DRF
1685963	Zach	Ertz	Zach Ertz	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1685963.png	TE	25	ERT800920	A	86	PHI
2082827	Will	Fuller	Will Fuller	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	FUL503598	A	\N	DRF
1125969	Jerry	Hughes	Jerry Hughes	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125969.png	DL	27	HUG532081	A	55	BUF
517243	Brian	Hoyer	Brian Hoyer	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517243.png	QB	30	HOY440791	A	7	HOU
1675157	Ramon	Humber	Ramon Humber	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675157.png	LB	28	HUM029529	A	\N	NE
1737463	Justin	Hunter	Justin Hunter	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737463.png	WR	24	HUN711562	IR	15	TEN
1272768	Duke	Ihenacho	Duke Ihenacho	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272768.png	DB	26	IHE454705	IR	24	WAS
1892521	William	Jackson	William Jackson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JAC645516	A	\N	DRF
2113		Jaguars	Jaguars	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	222	\N	\N	JAC
1631834	LaMichael	James	LaMichael James	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	JAM321847	A	\N	MIA
1850829	Kyshoen	Jarrett	Kyshoen Jarrett	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	JAR259599	IR	30	WAS
2174922	Jimmy	Jean	Jimmy Jean	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JEA407191	A	\N	NE
1979671	Damaris	Johnson	Damaris Johnson	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979671.png	WR	26	JOH146736	A	14	TEN
2078989	Ufomba	Kamalu	Ufomba Kamalu	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	KAM105154	A	\N	DRF
1893066	Marshall	Koehn	Marshall Koehn	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	KOE369462	A	\N	DRF
2001812	Steve	Longa	Steve Longa	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LON653236	A	\N	DRF
1998499	Dean	Lowry	Dean Lowry	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	LOW617631	A	\N	DRF
2175102	Luke	Lundy	Luke Lundy	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	LUN504348	A	\N	CLE
2174981	Caushaud	Lyons	Caushaud Lyons	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	LYO183644	A	\N	PIT
1273023	Jeff	Maehl	Jeff Maehl	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MAE601871	A	\N	PHI
2174759	Devin	Mahina	Devin Mahina	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MAH386848	A	\N	WAS
1630362	Terrell	Manning	Terrell Manning	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630362.png	LB	25	MAN538256	A	\N	MIA
2175042	Keshaun	Malone	Keshaun Malone	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MAL685550	A	\N	STL
1825172	Justin	Manton	Justin Manton	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	MAN694782	A	\N	BAL
1996165	Byron	Marshall	Byron Marshall	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MAR374249	A	\N	DRF
2174971	Ronald	Martin	Ronald Martin	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MAR706350	A	\N	NYJ
1998160	Ross	Martin	Ross Martin	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	MAR708125	A	\N	DRF
2059197	Tobais	Palmer	Tobais Palmer	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	26	PAL677655	A	\N	PIT
2005		Panthers	Panthers	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	320	\N	\N	CAR
2131		Patriots	Patriots	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	206	\N	\N	NE
2123		Rams	Rams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	223	\N	\N	STL
1970		Raiders	Raiders	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	524	\N	\N	OAK
2130820	Darrin	Reaves	Darrin Reaves	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	22	REA660089	A	\N	KC
2178523	Justin	Shirk	Justin Shirk	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SHI647801	A	\N	IND
1664356	David	Sims	David Sims	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SIM678924	A	\N	IND
2174302	Justin	Sinz	Justin Sinz	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SIN826590	A	\N	IND
1886867	Jordan	Simone	Jordan Simone	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SIM381977	A	\N	DRF
1685984	Shayne	Skov	Shayne Skov	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	SKO692489	A	\N	SF
2174974	Tory	Slater	Tory Slater	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SLA451625	A	\N	CLE
2078901	DeAndre	Smelter	DeAndre Smelter	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2078901.png	WR	24	SME387261	A	15	SF
2129633	Kiero	Small	Kiero Small	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SMA680763	A	\N	BAL
1116949	Alfonso	Smith	Alfonso Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SMI031319	A	\N	SF
1687431	D.J.	Smith	D.J. Smith	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SMI161240	A	\N	CLE
1664780	Mike	James	Mike James	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664780.png	RB	24	JAM394061	A	\N	TB
517293	Tim	Jamison	Tim Jamison	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JAM809956	A	\N	HOU
1243265	Jaiquawn	Jarrett	Jaiquawn Jarrett	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243265.png	DB	26	JAR256007	A	37	NYJ
1737117	Tony	Jefferson	Tony Jefferson	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737117.png	DB	24	JEF423538	A	22	ARI
1983624	Karl	Joseph	Karl Joseph	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JOS219381	A	\N	DRF
1906454	Miles	Killebrew	Miles Killebrew	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	KIL283492	A	\N	DRF
1877196	Nick	Kwiatkoski	Nick Kwiatkoski	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KWI059326	A	\N	DRF
1691422	Eddie	Lacy	Eddie Lacy	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691422.png	RB	25	LAC702361	A	27	GB
1824833	Jarvis	Landry	Jarvis Landry	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824833.png	WR	23	LAN163029	A	14	MIA
1856487	Ricky	Lumpkin	Ricky Lumpkin	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1856487.png	DL	27	LUM488598	A	\N	IND
2176383	D.J.	Lynch	D.J. Lynch	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LYN081058	A	\N	NE
2132705	Ross	Madison	Ross Madison	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAD659403	A	\N	DEN
1723479	Brandon	Magee	Brandon Magee	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MAG121782	IR	\N	TB
2130303	Ben	Malena	Ben Malena	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	MAL115277	A	\N	DAL
493004	Eli	Manning	Eli Manning	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493004.png	QB	35	MAN473170	A	10	NYG
12531	Peyton	Manning	Peyton Manning	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	MAN515097	A	\N	DEN
1824921	Johnny	Manziel	Johnny Manziel	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824921.png	QB	23	MAN794607	A	2	CLE
1762335	Rishard	Matthews	Rishard Matthews	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762335.png	WR	26	MAT576281	A	\N	TEN
2174193	Deiontrez	Mount	Deiontrez Mount	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	MOU408541	IR	53	TEN
1955		Panthers	Panthers	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	520	\N	\N	CAR
1984950	Breshad	Perriman	Breshad Perriman	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1984950.png	WR	22	PER440170	IR	18	BAL
1996809	Kevin	Peterson	Kevin Peterson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PET345726	A	\N	DRF
1893147	D.J.	Pettway	D.J. Pettway	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PET742114	A	\N	DRF
2174958	Travis	Raciti	Travis Raciti	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	RAC290361	A	\N	PHI
2103		Ravens	Ravens	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	225	\N	\N	BAL
2063849	Caesar	Rayford	Caesar Rayford	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	RAY622592	A	\N	MIN
1995792	Will	Redmond	Will Redmond	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RED713301	A	\N	DRF
1113453	D'Aundre	Reed	D'Aundre Reed	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	REE000000	A	\N	MIA
1737133	Eric	Reid	Eric Reid	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737133.png	DB	24	REI265485	A	35	SF
2001697	Keenan	Reynolds	Keenan Reynolds	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	REY406954	A	\N	DRF
2139947	James	Sample	James Sample	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2139947.png	DB	23	SAM117282	A	23	JAC
1996785	Zack	Sanchez	Zack Sanchez	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SAN094981	A	\N	DRF
517948	Jamarca	Sanford	Jamarca Sanford	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517948.png	DB	30	SAN663248	A	33	NO
2132690	Cairo	Santos	Cairo Santos	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132690.png	K	24	SAN796299	A	5	KC
1985100	Wes	Saxton	Wes Saxton	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	22	SAX645435	A	\N	NYJ
1631190	Matt	Shaughnessy	Matt Shaughnessy	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SHA517783	A	\N	ARI
1996519	Kevon	Seymour	Kevon Seymour	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SEY438361	A	\N	DRF
1691435	Ed	Stinson	Ed Stinson	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691435.png	DL	26	STI638331	A	91	ARI
2032		Texans	Texans	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	332	\N	\N	HOU
565792	Mike	Thomas	Mike Thomas	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	THO309019	A	\N	HOU
1226369	Pierre	Thomas	Pierre Thomas	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1226369.png	RB	31	THO085535	A	39	WAS
2132789	Ian	Thompson	Ian Thompson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	THO607853	A	\N	TB
1692182	Adrian	Tracy	Adrian Tracy	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TRA091822	A	\N	ARI
2176128	Ify	Umodu	Ify Umodu	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	OMO148317	A	\N	CHI
1983691	Vincent	Valentine	Vincent Valentine	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	VAL459261	A	\N	DRF
424363	Usama	Young	Usama Young	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	YOU614589	IR	\N	OAK
1850730	Grady	Jarrett	Grady Jarrett	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1850730.png	DL	22	JAR254349	A	97	ATL
1737095	Jackson	Jeffcoat	Jackson Jeffcoat	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737095.png	LB	25	JEF043661	A	53	WAS
1759770	Anthony	Jefferson	Anthony Jefferson	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	JEF096477	IR	46	CHI
1631804	Marvin	Jones	Marvin Jones	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631804.png	WR	26	JON601697	A	\N	DET
559816	Erik	Lorig	Erik Lorig	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	LOR636435	A	\N	NO
504639	Marshawn	Lynch	Marshawn Lynch	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/504639.png	RB	29	LYN442976	A	24	SEA
1692203	Khalil	Mack	Khalil Mack	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1692203.png	LB	25	MAC325122	A	52	OAK
1824836	Terrence	Magee	Terrence Magee	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	MAG263157	A	\N	BAL
1130556	Steve	Maneri	Steve Maneri	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MAN167706	A	\N	NYJ
1737484	Sean	Mannion	Sean Mannion	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737484.png	QB	23	MAN548238	A	14	STL
1664896	Stansly	Maponga	Stansly Maponga	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664896.png	DL	25	MAP697687	A	71	NYG
1273854	Ryan	Mathews	Ryan Mathews	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273854.png	RB	28	MAT190010	A	24	PHI
2111		Packers	Packers	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	230	\N	\N	GB
1851283	DeVante	Parker	DeVante Parker	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851283.png	WR	23	PAR226609	A	11	MIA
2082728	A'Shawn	Robinson	A'Shawn Robinson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROB367960	A	\N	DRF
1996786	Sterling	Shepard	Sterling Shepard	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SHE495754	A	\N	DRF
1250925	Lawrence	Sidbury	Lawrence Sidbury	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SID063890	A	\N	TB
1664625	John	Simon	John Simon	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664625.png	LB	25	SIM369947	A	51	HOU
1998297	Justin	Simmons	Justin Simmons	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SIM248681	A	\N	DRF
1691483	Charles	Sims	Charles Sims	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691483.png	RB	25	SIM669037	A	34	TB
2174973	Alex	Singleton	Alex Singleton	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	SIN350252	A	\N	MIN
2175073	Phillip	Sims	Phillip Sims	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	23	SIM787024	A	\N	SEA
1675034	Dan	Skuta	Dan Skuta	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675034.png	LB	29	SKU704190	A	55	JAC
1906463	LeShaun	Sims	LeShaun Sims	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SIM767909	A	\N	DRF
1852913	Darius	Slay	Darius Slay	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1852913.png	DB	25	SLA746089	A	23	DET
2174947	Tyler	Slavin	Tyler Slavin	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	SLA742910	A	\N	SEA
1675742	Antone	Smith	Antone Smith	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SMI040756	A	\N	CHI
2060554	Wendell	Smallwood	Wendell Smallwood	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SMA757345	A	\N	DRF
2001529	D'Joun	Smith	D'Joun Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	SMI206823	A	30	IND
1125777	D'Anthony	Smith	D'Anthony Smith	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125777.png	DL	27	SMI158630	A	70	CHI
1737565	Derron	Smith	Derron Smith	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	SMI201550	A	40	CIN
2022		Steelers	Steelers	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	313	\N	\N	PIT
1701728	Shamar	Stephen	Shamar Stephen	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1701728.png	DL	25	STE351003	A	93	MIN
2001862	Nate	Sudfeld	Nate Sudfeld	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	SUD197144	A	\N	DRF
1877293	Damian	Swann	Damian Swann	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	SWA314232	IR	27	NO
2174925	Vince	Taylor	Vince Taylor	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TAY769812	A	\N	NE
1272413	J.T.	Thomas	J.T. Thomas	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272413.png	LB	27	THO212048	A	55	NYG
2130728	Robert	Thomas	Robert Thomas	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	THO342128	A	96	MIA
1244404	Walter	Thurmond	Walter Thurmond	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244404.png	DB	28	THU670009	A	26	PHI
2195596	Ross	Travis	Ross Travis	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	TRA542942	A	\N	KC
1787727	Isaiah	Trufant	Isaiah Trufant	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	TRU385183	IR	\N	CLE
2059182	Jeff	Tuel	Jeff Tuel	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	TUE385627	A	\N	JAC
2175403	Dezmond	Johnson	Dezmond Johnson	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	JOH202384	A	\N	CIN
1274365	Jeron	Johnson	Jeron Johnson	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1274365.png	DB	27	JOH344009	A	20	WAS
1996832	Cayleb	Jones	Cayleb Jones	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JON150943	A	\N	DRF
2000909	Cyrus	Jones	Cyrus Jones	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JON188523	A	\N	DRF
2217545	Matt	Judon	Matt Judon	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JUD442977	A	\N	DRF
1984683	Demarcus	Lawrence	Demarcus Lawrence	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1984683.png	DL	23	LAW282118	A	90	DAL
1996488	Kenny	Lawler	Kenny Lawler	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LAW151948	A	\N	DRF
2001515	Cre'von	LeBlanc	Cre'von LeBlanc	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LEB612957	A	\N	DRF
515627	Chris	Long	Chris Long	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/515627.png	DL	31	LON213256	A	\N	NE
1700711	Derrick	Lott	Derrick Lott	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	LOT289903	A	\N	TB
2175351	Johnny	Lowdermilk	Johnny Lowdermilk	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	LOW060965	A	\N	MIN
1107208	Danieal	Manning	Danieal Manning	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAN468179	A	\N	HOU
1983759	KJ	Maye	KJ Maye	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MAY290229	A	\N	DRF
2174860	Chuka	Ndulue	Chuka Ndulue	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	NDU385627	A	\N	SD
2078920	Yannick	Ngakoue	Yannick Ngakoue	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	NGA355963	A	\N	DRF
1851902	Victor	Ochi	Victor Ochi	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OCH296635	A	\N	DRF
1981		Patriots	Patriots	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	506	\N	\N	NE
1983792	Tyvis	Powell	Tyvis Powell	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	POW569823	A	\N	DRF
1824864	Dak	Prescott	Dak Prescott	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	PRE285723	A	\N	DRF
2120		Raiders	Raiders	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	224	\N	\N	OAK
1273191	Chris	Rainey	Chris Rainey	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	RAI599645	A	\N	IND
1632495	Kheeston	Randall	Kheeston Randall	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	RAN083057	A	\N	MIN
1953		Ravens	Ravens	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	525	\N	\N	BAL
2129		Redskins	Redskins	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	218	\N	\N	WAS
1616792	Marcel	Reece	Marcel Reece	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616792.png	RB	30	REE092680	A	45	OAK
564943	Emmanuel	Sanders	Emmanuel Sanders	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/564943.png	WR	29	SAN295989	A	10	DEN
2175068	Stephon	Sanders	Stephon Sanders	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SAN480429	A	\N	NO
1243816	Dane	Sanzenbacher	Dane Sanzenbacher	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SAN812129	IR	\N	CIN
493050	Matt	Schaub	Matt Schaub	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493050.png	QB	34	SCH085186	A	\N	ATL
2174854	Tim	Scott	Tim Scott	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	SCO706639	A	\N	CLE
1880803	Nelson	Spruce	Nelson Spruce	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SPR770940	A	\N	DRF
1824434	Joel	Stave	Joel Stave	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	STA818236	A	\N	DRF
1972		Steelers	Steelers	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	513	\N	\N	PIT
1982		Texans	Texans	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	532	\N	\N	HOU
2174900	Jake	Waters	Jake Waters	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	WAT095233	A	\N	SEA
1996374	Stephen	Weatherly	Stephen Weatherly	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WEA206755	A	\N	DRF
235258	Adrian	Wilson	Adrian Wilson	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WIL647976	A	\N	ARI
2071871	De'Runnya	Wilson	De'Runnya Wilson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL674240	A	\N	DRF
2130214	Kerry	Wynn	Kerry Wynn	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	WYN134240	A	72	NYG
1850735	Charone	Peake	Charone Peake	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PEA150300	A	\N	DRF
437049	Aaron	Ross	Aaron Ross	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ROS527992	A	\N	CLE
1116667	Sam	Shields	Sam Shields	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116667.png	DB	28	SHI229142	A	37	GB
1762121	Blake	Sims	Blake Sims	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	SIM662445	A	\N	FA
1700507	Dion	Sims	Dion Sims	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700507.png	TE	25	SIM687398	A	80	MIA
2175000	Gannon	Sinclair	Gannon Sinclair	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	SIN021087	A	84	CHI
2153683	Taylor	Sloat	Taylor Sloat	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SLO281988	A	\N	TB
1630791	Aldon	Smith	Aldon Smith	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630791.png	LB	26	SMI029386	A	99	OAK
1117478	Andre	Smith	Andre Smith	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SMI038086	A	\N	CLE
2129523	Chris	Smith	Chris Smith	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129523.png	DL	24	SMI141571	A	98	JAC
1823894	Malcolm	Smith	Malcolm Smith	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1823894.png	LB	26	SMI501156	A	53	OAK
1674149	Frank	Summers	Frank Summers	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SUM438237	A	\N	BUF
2061099	Kelvin	Taylor	Kelvin Taylor	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TAY467820	A	\N	DRF
2028		Titans	Titans	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	331	\N	\N	TEN
2132924	Fitzgerald	Toussaint	Fitzgerald Toussaint	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132924.png	RB	25	TOU424291	A	33	PIT
2008832	Jordie	Tripp	Jordie Tripp	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2008832.png	LB	25	TRI579306	A	58	JAC
1754432	Will	Tukuafu	Will Tukuafu	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1754432.png	RB	32	TUK652601	A	46	SEA
2175079	Andrew	Turzilli	Andrew Turzilli	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	TUR828576	A	86	TEN
1907522	Carson	Wentz	Carson Wentz	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	WEN615770	A	\N	DRF
1852875	Brandon	Wilds	Brandon Wilds	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WIL047556	A	\N	DRF
1889927	Anthony	Zettel	Anthony Zettel	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ZET629687	A	\N	DRF
1620490	Stephen	Paea	Stephen Paea	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620490.png	DL	27	PAE059326	IR	90	WAS
1878019	Cody	Prewitt	Cody Prewitt	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PRE797704	A	\N	TEN
2005662	C.J.	Prosise	C.J. Prosise	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	PRO494028	A	\N	DRF
1737112	Kelcy	Quarles	Kelcy Quarles	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	QUA361520	A	64	IND
2000915	Reggie	Ragland	Reggie Ragland	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	RAG263360	A	\N	DRF
2023		Rams	Rams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	323	\N	\N	STL
1737645	Joseph	Randle	Joseph Randle	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	RAN334748	A	\N	DAL
2186286	Joe	Rankin	Joe Rankin	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RAN650839	A	\N	CLE
1983526	D.J.	Reader	D.J. Reader	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	REA148116	A	\N	DRF
2174813	Terry	Redden	Terry Redden	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	RED323673	A	\N	CAR
1860912	Cedric	Reed	Cedric Reed	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	REE159531	A	\N	BUF
1979		Redskins	Redskins	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	518	\N	\N	WAS
2005514	Alonzo	Russell	Alonzo Russell	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	RUS329543	A	\N	DRF
2117		Saints	Saints	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	207	\N	\N	NO
1130512	Ricky	Sapp	Ricky Sapp	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SAP613372	A	\N	WAS
556987	Orlando	Scandrick	Orlando Scandrick	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/556987.png	DB	29	SCA335249	IR	32	DAL
1836169	Ross	Scheuerman	Ross Scheuerman	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	SCH139023	A	\N	PHI
2001175	Joe	Schobert	Joe Schobert	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SCH425646	A	\N	DRF
2126		Seahawks	Seahawks	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	216	\N	\N	SEA
2026		Seahawks	Seahawks	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	316	\N	\N	SEA
2139452	Hunter	Sharp	Hunter Sharp	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SHA408400	A	\N	DRF
2132467	Jean	Sifrin	Jean Sifrin	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SIF563610	A	\N	IND
2131610	A.J.	Stamps	A.J. Stamps	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	STA319474	A	\N	DRF
1996789	Eric	Striker	Eric Striker	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	STR441079	A	\N	DRF
1998915	Ron	Thompson	Ron Thompson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	THO680129	A	\N	DRF
422573	Justin	Tuck	Justin Tuck	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/422573.png	DL	33	TUC056287	A	91	OAK
2116		Vikings	Vikings	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	205	\N	\N	MIN
1983803	Adolphus	Washington	Adolphus Washington	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WAS013310	A	\N	DRF
1996291	Trey	Williams	Trey Williams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996291.png	RB	23	WIL547628	A	40	IND
1823916	Jimmy	Wilson	Jimmy Wilson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1823916.png	DB	29	WIL726328	A	\N	KC
1632251	Tyler	Wilson	Tyler Wilson	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	WIL719543	A	\N	CIN
1893731	Josh	Woodrum	Josh Woodrum	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	WOO399845	A	\N	DRF
2058172	Paul	Worrilow	Paul Worrilow	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058172.png	LB	25	WOR678863	A	55	ATL
1824820	Gabe	Wright	Gabe Wright	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	WRI269524	A	90	DET
1999651	Cam	Worthy	Cam Worthy	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	WOR725394	IR	\N	BAL
2020		Raiders	Raiders	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	324	\N	\N	OAK
419238	Jay	Ratliff	Jay Ratliff	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	RAT586227	A	\N	CHI
1823871	Mistral	Raymond	Mistral Raymond	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RAY749301	IR	\N	MIN
2130726	Silas	Redd	Silas Redd	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130726.png	RB	24	RED271501	IR	32	WAS
1691412	Jordan	Reed	Jordan Reed	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691412.png	TE	25	REE302847	A	86	WAS
2220224	Xavier	Rush	Xavier Rush	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	RUS152755	A	\N	PHI
2005664	KeiVarae	Russell	KeiVarae Russell	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RUS585854	A	\N	DRF
1967		Saints	Saints	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	507	\N	\N	NO
420095	Matt	Ryan	Matt Ryan	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/420095.png	QB	30	RYA238179	A	2	ATL
2060504	Beau	Sandland	Beau Sandland	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SAN584247	A	\N	DRF
1695553	Lanear	Sampson	Lanear Sampson	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SAM354761	A	\N	PIT
1977081	J.K.	Schaffer	J.K. Schaffer	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SCH034304	A	\N	CIN
2130628	Alvin	Scioneaux	Alvin Scioneaux	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SCI569642	IR	\N	SD
565191	O'Brien	Schofield	O'Brien Schofield	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565191.png	LB	29	SCH441573	A	50	ATL
1620544	Caleb	Sturgis	Caleb Sturgis	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620544.png	K	26	STU678742	A	6	PHI
2174899	Todd	Thomas	Todd Thomas	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	THO390999	A	\N	JAC
1893212	Stephon	Tuitt	Stephon Tuitt	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1893212.png	DL	22	TUI753113	A	91	PIT
2016		Vikings	Vikings	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	305	\N	\N	MIN
1851134	Antwaun	Woods	Antwaun Woods	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WOO412059	A	\N	DRF
1116455	Al	Woods	Al Woods	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116455.png	DL	29	WOO401988	A	96	TEN
1273280	Weslye	Saunders	Weslye Saunders	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SAU771570	A	\N	IND
1836063	Rashawn	Scott	Rashawn Scott	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SCO594648	A	\N	DRF
1860932	Delvon	Simmons	Delvon Simmons	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SIM165991	A	\N	DRF
1615610	Matthew	Slater	Matthew Slater	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1615610.png	WR	30	SLA420538	A	18	NE
1763479	Yawin	Smallwood	Yawin Smallwood	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SMA759621	IR	\N	TEN
552642	Alex	Smith	Alex Smith	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/552642.png	QB	31	SMI031126	A	11	KC
493063	Antonio	Smith	Antonio Smith	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/493063.png	DL	34	SMI040937	A	90	DEN
1759833	Chris	Smith	Chris Smith	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SMI000010	A	\N	FA
1272964	Kerry	Taylor	Kerry Taylor	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	TAY482700	A	\N	DEN
2130192	Eric	Thomas	Eric Thomas	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	THO160643	A	\N	BUF
1974183	Peyton	Thompson	Peyton Thompson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1974183.png	DB	25	THO641056	A	\N	JAC
1978		Titans	Titans	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	531	\N	\N	TEN
1759992	Eric	Tomlinson	Eric Tomlinson	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	TOM677561	A	\N	HOU
1621669	Torell	Troup	Torell Troup	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TRO525485	A	\N	OAK
2079899	Laquon	Treadwell	Laquon Treadwell	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	TRE425415	A	\N	DRF
1244482	Christian	Tupou	Christian Tupou	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TUP540553	A	\N	CLE
1632082	DeAngelo	Tyson	DeAngelo Tyson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632082.png	DL	26	TYS379291	A	\N	SEA
1998222	D.J.	White	D.J. White	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WHI151510	A	\N	DRF
1892488	Bryce	Williams	Bryce Williams	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	WIL170839	A	\N	DRF
1277508	Lawrence	Wilson	Lawrence Wilson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL744745	A	\N	STL
1687803	Buster	Skrine	Buster Skrine	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1687803.png	DB	26	SKR296636	A	41	NYJ
2130850	Nathan	Slaughter	Nathan Slaughter	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SLA680034	A	\N	ARI
1971906	Brad	Smelley	Brad Smelley	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SME373761	A	\N	STL
421093	Alex	Smith	Alex Smith	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/421093.png	TE	33	SMI029398	A	83	WAS
421137	Brad	Smith	Brad Smith	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SMI080910	A	\N	PHI
1856292	Brandon	Smith	Brandon Smith	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SMI082523	A	\N	DAL
2082844	Jaylon	Smith	Jaylon Smith	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SMI390302	A	\N	DRF
1996790	Charles	Tapper	Charles Tapper	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TAP517782	A	\N	DRF
1825226	Darian	Thompson	Darian Thompson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	THO507692	A	\N	DRF
1272825	Robert	Turbin	Robert Turbin	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272825.png	RB	26	TUR019059	A	\N	IND
1871322	Nick	Vannett	Nick Vannett	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	VAN643509	A	\N	DRF
1853342	Darrin	Walls	Darrin Walls	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1853342.png	DB	27	WEL363873	A	\N	DET
2131681	D'haquille	Williams	D'haquille Williams	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL236356	A	\N	DRF
559569	Sean	Smith	Sean Smith	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559569.png	DB	28	SMI707600	A	\N	OAK
1243338	Tyrod	Taylor	Tyrod Taylor	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243338.png	QB	26	TAY764868	A	5	BUF
1631925	Chase	Thomas	Chase Thomas	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	THO086092	A	\N	SF
2132		Texans	Texans	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	232	\N	\N	HOU
2129590	Jemea	Thomas	Jemea Thomas	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	THO222894	A	\N	TEN
1682114	Shamarko	Thomas	Shamarko Thomas	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1682114.png	DB	25	THO379701	A	29	PIT
1868409	Lawrence	Thomas	Lawrence Thomas	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	THO287945	A	\N	DRF
2129434	Khyri	Thornton	Khyri Thornton	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129434.png	DL	26	THO786923	A	67	DET
2128		Titans	Titans	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	231	\N	\N	TEN
1632103	Danny	Trevathan	Danny Trevathan	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632103.png	LB	26	TRE789054	A	\N	CHI
1985374	Justin	Tucker	Justin Tucker	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1985374.png	K	26	TUC224643	A	9	BAL
432041	Stephen	Tulloch	Stephen Tulloch	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/432041.png	LB	31	TUL394525	A	55	DET
1966		Vikings	Vikings	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	505	\N	\N	MIN
2066935	Daryl	Worley	Daryl Worley	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WOR616410	A	\N	DRF
1860806	Connor	Wujciak	Connor Wujciak	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WUJ129154	A	\N	DRF
2122		Steelers	Steelers	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	213	\N	\N	PIT
1679838	Nate	Stupar	Nate Stupar	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679838.png	LB	28	STU617455	A	\N	NO
1767273	Davis	Tull	Davis Tull	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	TUL136073	IR	55	NO
2136528	Jihad	Ward	Jihad Ward	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WAR208421	A	\N	DRF
1860934	DeAndre	Washington	DeAndre Washington	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WAS000000	A	\N	DRF
1621331	Derek	Wolfe	Derek Wolfe	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1621331.png	DL	26	WOL309455	A	95	DEN
1632252	Jarius	Wright	Jarius Wright	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632252.png	WR	26	WRI391959	A	17	MIN
2174771	Dylan	Wynn	Dylan Wynn	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	WYN131053	A	\N	CLE
1248070	Jarius	Wynn	Jarius Wynn	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1248070.png	DL	29	WYN131922	IR	92	BUF
1632499	Aaron	Williams	Aaron Williams	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632499.png	DB	25	WIL132538	IR	23	BUF
1620620	Shaq	Wilson	Shaq Wilson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL778171	A	\N	FA
1621330	George	Winn	George Winn	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1621330.png	RB	25	WIN446439	A	\N	DET
1983734	Joe	Bolden	Joe Bolden	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BOL203028	A	\N	DRF
1630952	Jewel	Hampton	Jewel Hampton	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAM728248	A	\N	SF
2057478	Duron	Harmon	Duron Harmon	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057478.png	DB	25	HAR195786	A	30	NE
2174929	Brad	Harrah	Brad Harrah	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HAR300672	A	\N	NYG
1245905	Dwayne	Harris	Dwayne Harris	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245905.png	WR	28	HAR376781	A	17	NYG
1665334	Trevardo	Williams	Trevardo Williams	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL547603	A	\N	WAS
2057661	Luke	Willson	Luke Willson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057661.png	TE	26	WIL747168	A	82	SEA
1765998	Marquess	Wilson	Marquess Wilson	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1765998.png	WR	23	WIL750668	IR	10	CHI
1823918	T.J.	Yates	T.J. Yates	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1823918.png	QB	28	YAT420091	IR	6	HOU
2130424	Ty	Zimmerman	Ty Zimmerman	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ZIM579972	A	\N	NO
1673231	Tori	Gurley	Tori Gurley	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GUR281188	A	\N	BUF
1679796	Ra'Shede	Hageman	Ra'Shede Hageman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679796.png	DL	25	HAG152196	A	77	ATL
1740196	Chad	Hall	Chad Hall	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAL192825	A	\N	JAC
1767568	Clayton	Geathers	Clayton Geathers	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1767568.png	DB	23	GEA405166	A	42	IND
2171890	Shayon	Green	Shayon Green	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GRE359465	A	\N	PIT
565750	Shonn	Greene	Shonn Greene	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	GRE510029	A	\N	TEN
437045	Michael	Griffin	Michael Griffin	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/437045.png	DB	31	GRI232017	A	\N	MIN
1891873	Marcus	Harris	Marcus Harris	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	26	HAR474270	IR	\N	NYG
1615897	Garrett	Hartley	Garrett Hartley	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	HAR717525	IR	\N	PIT
2129496	Taylor	Hart	Taylor Hart	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129496.png	DL	25	HAR704616	A	97	PHI
12429	Matt	Hasselbeck	Matt Hasselbeck	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/12429.png	QB	40	HAS536799	A	8	IND
2063120	Charles	Hawkins	Charles Hawkins	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAW246055	A	\N	NO
1116041	Joe	Hawley	Joe Hawley	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116041.png		27	HAW653722	A	68	TB
1701990	T.J.	Heath	T.J. Heath	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HEA785202	A	\N	NYG
2130626	Javontee	Herndon	Javontee Herndon	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130626.png	WR	24	HER391702	A	81	SD
2130137	Ryan	Hewitt	Ryan Hewitt	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130137.png	TE	25	HEW205087	A	89	CIN
1737362	Troy	Hill	Troy Hill	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	HIL626234	A	32	STL
1664293	Gerald	Hodges	Gerald Hodges	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664293.png	LB	25	HOD406062	A	51	SF
413021	Santonio	Holmes	Santonio Holmes	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HOL657297	A	\N	CHI
2167530	Kenneth	Horsley	Kenneth Horsley	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HOR530334	A	\N	CAR
1123374	Lamarr	Houston	Lamarr Houston	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123374.png	DL	28	HOU561271	A	99	CHI
2175146	Gordon	Hill	Gordon Hill	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HIL272661	IR	\N	SD
2174943	Jay	Hughes	Jay Hughes	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HUG529426	A	\N	STL
1675009	Phillip	Hunt	Phillip Hunt	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675009.png	DL	30	HUN330205	A	97	NO
2130513	Kerry	Hyder	Kerry Hyder	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	HYD440791	A	61	DET
2211213	Jamel	Johnson	Jamel Johnson	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	JOH321936	A	\N	GB
518691	Rashad	Johnson	Rashad Johnson	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518691.png	DB	30	JOH590218	A	\N	TEN
1115105	Arthur	Jones	Arthur Jones	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1115105.png	DL	29	JON069762	IR	97	IND
1220258	Jacoby	Jones	Jacoby Jones	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JON404038	A	\N	PIT
409879	Maurice	Jones-Drew	Maurice Jones-Drew	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	DRE527472	A	\N	OAK
1823837	Frank	Kearse	Frank Kearse	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1823837.png	DL	27	KEA506049	A	\N	NE
1243851	Ryan	Kerrigan	Ryan Kerrigan	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243851.png	LB	27	KER630730	A	91	WAS
2175086	Terry	Williams	Terry Williams	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	WIL535538	A	79	CHI
1877298	Ramik	Wilson	Ramik Wilson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1877298.png	LB	23	WIL774827	A	53	KC
1723467	Earl	Wolff	Earl Wolff	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1723467.png	DB	26	WOL371346	A	\N	JAC
1977150	Adrian	Hamilton	Adrian Hamilton	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAM183590	A	\N	SD
412179	Parys	Haralson	Parys Haralson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAR002868	A	\N	NO
1766833	Josh	Harper	Josh Harper	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAR267323	A	\N	OAK
412243	Roman	Harper	Roman Harper	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/412243.png	DB	33	HAR293034	A	41	CAR
1244521	Mike	Harris	Mike Harris	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAR492318	A	\N	NYG
2130610	Ethan	Hemer	Ethan Hemer	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HEM097715	A	\N	PIT
1824413	Jeff	Heuerman	Jeff Heuerman	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	HEU190731	IR	82	DEN
1984260	Jeremy	Hill	Jeremy Hill	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1984260.png	RB	23	HIL407410	A	32	CIN
1851481	Zack	Hodges	Zack Hodges	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	HOD495185	A	\N	STL
1631886	Wes	Horton	Wes Horton	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1631886.png	DL	26	HOR723162	A	\N	CAR
1737474	Josh	Huff	Josh Huff	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737474.png	WR	24	HUF194666	A	13	PHI
1984262	Danielle	Hunter	Danielle Hunter	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1984262.png	DL	21	HUN529240	A	99	MIN
1621494	George	Iloka	George Iloka	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1621494.png	DB	26	ILO355964	A	43	CIN
413503	D'Qwell	Jackson	D'Qwell Jackson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/413503.png	LB	32	JAC137677	A	52	IND
2130124	Tramain	Jacobs	Tramain Jacobs	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	JAC772270	A	25	NYG
1620095	James-Michael	Johnson	James-Michael Johnson	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620095.png	LB	26	JOH337662	A	56	MIA
1630686	Leonard	Johnson	Leonard Johnson	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JOH399760	A	\N	NE
2057700	Rufus	Johnson	Rufus Johnson	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057700.png	DL	25	JOH656721	A	70	NE
1633486	Trumaine	Johnson	Trumaine Johnson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633486.png	DB	26	JOH708747	A	22	STL
1674123	Brad	Jones	Brad Jones	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JON100075	A	\N	PHI
1823836	Colin	Jones	Colin Jones	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1823836.png	DB	28	JON179862	A	42	CAR
431446	James	Jones	James Jones	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/431446.png	WR	32	JON404681	A	89	GB
2130265	Ryan	Jones	Ryan Jones	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JON714896	A	\N	NYG
2175094	Harold	Jones-Quartey	Harold Jones-Quartey	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	JON826879	A	29	CHI
1683145	Kyle	Juszczyk	Kyle Juszczyk	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1683145.png	RB	24	JUS812736	A	44	BAL
1631987	Jermaine	Kearse	Jermaine Kearse	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631987.png	WR	26	KEA511674	A	15	SEA
1246019	Eric	Kettani	Eric Kettani	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	KET323003	A	\N	NE
1630750	Collin	Klein	Collin Klein	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	KLE228860	A	\N	FA
1620562	Tavarres	King	Tavarres King	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620562.png	WR	25	KIN640376	A	\N	NYG
1893200	Ben	Koyack	Ben Koyack	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1893200.png	TE	23	KOY563976	A	48	JAC
2174931	Matt	LaCosse	Matt LaCosse	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	LAC529668	A	81	NYG
2186072	Sammuel	Lamur	Sammuel Lamur	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	LAM805930	A	\N	TB
2175135	Jake	Heaps	Jake Heaps	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	HEA308918	A	\N	NYJ
1979980	Cooper	Helfet	Cooper Helfet	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979980.png	TE	26	HEL203312	A	84	SEA
1243115	Mark	Herzlich	Mark Herzlich	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243115.png	LB	28	HER828785	A	94	NYG
1759912	Jordan	Hicks	Jordan Hicks	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759912.png	LB	23	HIC516259	IR	58	PHI
2175134	Austin	Hill	Austin Hill	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HIL087132	A	\N	NE
303369	Shaun	Hill	Shaun Hill	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/303369.png	QB	36	HIL586565	A	13	MIN
2129465	Anthony	Hitchens	Anthony Hitchens	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129465.png	LB	23	HIT298611	A	59	DAL
2130123	Derrick	Hopkins	Derrick Hopkins	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HOP163422	A	\N	ATL
1724424	Andre	Holmes	Andre Holmes	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1724424.png	WR	27	HOL497389	A	18	OAK
1737472	Adrian	Hubbard	Adrian Hubbard	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HUB163574	A	\N	GB
1226212	James	Ihedigbo	James Ihedigbo	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1226212.png	DB	32	IHE148317	A	32	DET
1684461	Josh	Jarboe	Josh Jarboe	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JAR022246	A	\N	FA
1707322	B.W.	Webb	B.W. Webb	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1707322.png	DB	25	WEB094382	A	38	TEN
1272538	Brandon	Weeden	Brandon Weeden	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272538.png	QB	32	WEE221487	A	5	HOU
1737758	Bjoern	Werner	Bjoern Werner	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WER300216	A	\N	IND
1977174	Griff	Whalen	Griff Whalen	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977174.png	WR	26	WHA108810	IR	\N	MIA
1752475	Kevin	White	Kevin White	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	WHI315683	A	\N	ARI
1630570	Tahir	Whitehead	Tahir Whitehead	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630570.png	LB	26	WHI627494	A	59	DET
1976197	Ian	Wild	Ian Wild	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WIL035339	A	\N	PIT
2174847	DeShawn	Williams	DeShawn Williams	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	WIL234247	A	69	CIN
1674089	Jason	Williams	Jason Williams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL349409	A	\N	TB
423832	Kyle	Williams	Kyle Williams	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423832.png	DL	32	WIL415284	IR	95	BUF
1115064	Mike	Williams	Mike Williams	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL446858	IR	\N	BUF
1700724	Shawn	Williams	Shawn Williams	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700724.png	DB	24	WIL522311	A	36	CIN
423645	Tramon	Williams	Tramon Williams	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423645.png	DB	33	WIL545231	A	22	CLE
2175115	Austin	Willis	Austin Willis	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	WIL596927	A	\N	DET
565129	Corey	Wootton	Corey Wootton	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565129.png	DL	28	WOO826932	IR	99	DET
1632385	Kendall	Wright	Kendall Wright	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632385.png	WR	26	WRI486076	A	13	TEN
2001149	Roberto	Aguayo	Roberto Aguayo	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	AGU080357	A	\N	DRF
405385	Lorenzo	Alexander	Lorenzo Alexander	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/405385.png	LB	32	ALE509608	A	56	OAK
1737526	Ricardo	Allen	Ricardo Allen	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737526.png	DB	24	ALL641418	A	37	ATL
2131172	Geronimo	Allison	Geronimo Allison	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ALL713965	A	\N	DRF
516500	James	Anderson	James Anderson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/516500.png	LB	32	AND291445	A	42	NO
516968	Danny	Amendola	Danny Amendola	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/516968.png	WR	30	AME359918	A	80	NE
2130837	Jason	Ankrah	Jason Ankrah	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ANK401791	A	\N	TEN
1691326	Dri	Archer	Dri Archer	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691326.png	RB	24	ARC425790	A	\N	NYJ
1626550	Anthony	Armstrong	Anthony Armstrong	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ARM101839	A	\N	CLE
2006		Bears	Bears	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	321	\N	\N	CHI
1956		Bears	Bears	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	521	\N	\N	CHI
2060762	Vonn	Bell	Vonn Bell	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BEL626378	A	\N	DRF
1957		Bengals	Bengals	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	526	\N	\N	CIN
1631176	Bradie	Ewing	Bradie Ewing	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	EWI444955	IR	\N	JAC
1902		Falcons	Falcons	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	401	\N	\N	ATL
1913		Jaguars	Jaguars	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	422	\N	\N	JAC
1919		Jets	Jets	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	410	\N	\N	NYJ
1905		Panthers	Panthers	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	420	\N	\N	CAR
1931		Patriots	Patriots	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	406	\N	\N	NE
1903		Ravens	Ravens	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	425	\N	\N	BAL
1929		Redskins	Redskins	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	418	\N	\N	WAS
1922		Steelers	Steelers	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	413	\N	\N	PIT
2175085	Lee	Ward	Lee Ward	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WAR233194	A	\N	CAR
2130282	Corey	Washington	Corey Washington	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	WAS174012	A	\N	DET
2129631	Brandon	Watts	Brandon Watts	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129631.png	LB	25	WAT686542	A	\N	MIN
2186070	Diaheem	Watkins	Diaheem Watkins	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WAT188906	A	\N	PHI
1123545	Joe	Webb	Joe Webb	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1123545.png	QB	29	WEB212765	A	14	CAR
1691322	Kerwynn	Williams	Kerwynn Williams	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691322.png	RB	24	WIL407286	A	33	ARI
1113505	Kyle	Williams	Kyle Williams	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1113505.png	WR	27	WIL415195	IR	15	DEN
1679915	Steve	Williams	Steve Williams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679915.png	DB	25	WIL529278	A	23	SD
2130295	Evan	Wilson	Evan Wilson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	WIL687395	A	\N	OAK
2175151	Demetrius	Wilson	Demetrius Wilson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL672740	A	\N	SD
2175046	Milton	Williams	Milton Williams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL454359	A	\N	OAK
396134	Jason	Witten	Jason Witten	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396134.png	TE	33	WIT559021	A	82	DAL
1905948	Isaako	Aaitui	Isaako Aaitui	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	AAI622937	A	\N	TEN
1674108	Spencer	Adkins	Spencer Adkins	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ADK695150	A	\N	NYG
1620589	Antonio	Allen	Antonio Allen	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620589.png	DB	27	ALL057341	IR	\N	HOU
2087700	Mackensie	Alexander	Mackensie Alexander	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ALE511379	A	\N	DRF
2129632	Beau	Allen	Beau Allen	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129632.png	DL	24	ALL062169	A	94	PHI
1687456	Cortez	Allen	Cortez Allen	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1687456.png	DB	27	ALL091746	A	28	PIT
1688633	Robert	Alford	Robert Alford	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1688633.png	DB	27	ALF657259	A	23	ATL
1886746	Robby	Anderson	Robby Anderson	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	AND460305	A	\N	DRF
2060759	Eli	Apple	Eli Apple	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	APP195645	A	\N	DRF
1877266	Sterling	Bailey	Sterling Bailey	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BAI629414	A	\N	DRF
1906		Bears	Bears	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	421	\N	\N	CHI
1909		Broncos	Broncos	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	428	\N	\N	DEN
1920		Raiders	Raiders	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	424	\N	\N	OAK
1917		Saints	Saints	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	407	\N	\N	NO
1926		Seahawks	Seahawks	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	416	\N	\N	SEA
2185867	Arthur	Williams	Arthur Williams	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL144587	A	\N	NYJ
2174807	Erik	Williams	Erik Williams	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WIL270192	A	\N	DET
1758984	Teddy	Williams	Teddy Williams	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1758984.png	DB	27	WIL532429	A	21	CAR
2057702	Vince	Williams	Vince Williams	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057702.png	LB	26	WIL554116	A	98	PIT
1274059	C.J.	Wilson	C.J. Wilson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WIL662986	A	\N	DET
2060755	Damien	Wilson	Damien Wilson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	WIL664778	A	57	DAL
12841	Charles	Woodson	Charles Woodson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/12841.png	DB	39	WOO661523	A	24	OAK
2210433	Justin	Worley	Justin Worley	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	WOR632160	A	\N	CHI
2211800	J.J.	Worton	J.J. Worton	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WOR780192	A	\N	NE
1273864	Devon	Wylie	Devon Wylie	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WYL521323	A	\N	ATL
1984224	T.J.	Yeldon	T.J. Yeldon	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1984224.png	RB	22	YEL060186	A	24	JAC
2130853	Chris	Young	Chris Young	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	YOU147391	A	\N	NO
2175050	Jamal	Young	Jamal Young	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	YOU253170	A	\N	TB
2185129	Tyrequek	Zimmerman	Tyrequek Zimmerman	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	ZIM587472	A	\N	ARI
1631138	Michael	Zordich	Michael Zordich	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	ZOR206895	A	\N	NO
1971893	Greg	Zuerlein	Greg Zuerlein	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971893.png	K	28	ZUE563610	A	4	STL
405198	Husain	Abdullah	Husain Abdullah	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/405198.png	DB	30	ABD660476	A	39	KC
1996508	Nelson	Agholor	Nelson Agholor	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996508.png	WR	22	AGH474619	A	17	PHI
2130145	Ray	Agnew	Ray Agnew	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	AGN409291	A	\N	WAS
1245530	Seyi	Ajirotutu	Seyi Ajirotutu	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245530.png	WR	28	AJI563610	A	16	PHI
2074424	Daniel	Adongo	Daniel Adongo	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ADO444955	A	\N	IND
1853727	Kamar	Aiken	Kamar Aiken	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1853727.png	WR	26	AIK246111	A	11	BAL
1850872	Jimmy	Bean	Jimmy Bean	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BEA337135	A	\N	DRF
1998364	V'Angelo	Bentley	V'Angelo Bentley	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BEN717655	A	\N	DRF
2079903	Andrew	Billings	Andrew Billings	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BIL462662	A	\N	DRF
1840610	Ronald	Blair	Ronald Blair	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BLA547928	A	\N	DRF
1930		Browns	Browns	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	419	\N	\N	CLE
1927		Buccaneers	Buccaneers	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	417	\N	\N	TB
1901		Cardinals	Cardinals	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	412	\N	\N	ARI
1914		Chiefs	Chiefs	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	403	\N	\N	KC
1912		Colts	Colts	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	402	\N	\N	IND
1908		Cowboys	Cowboys	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	427	\N	\N	DAL
1915		Dolphins	Dolphins	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	404	\N	\N	MIA
1921		Eagles	Eagles	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	411	\N	\N	PHI
1918		Giants	Giants	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	408	\N	\N	NYG
1932		Texans	Texans	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	432	\N	\N	HOU
1928		Titans	Titans	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	431	\N	\N	TEN
1916		Vikings	Vikings	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	405	\N	\N	MIN
556990	Kyle	Wilson	Kyle Wilson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/556990.png	DB	28	WIL741575	A	24	NO
2175087	Kyle	Woestmann	Kyle Woestmann	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WOE584484	A	\N	CHI
2141268	Thomas	Wolfe	Thomas Wolfe	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WOL336413	A	\N	WAS
1754280	Robert	Woods	Robert Woods	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1754280.png	WR	24	WOO561144	IR	10	BUF
1273493	K.J.	Wright	K.J. Wright	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273493.png	LB	26	WRI487649	A	50	SEA
2175032	Chandler	Worthy	Chandler Worthy	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	22	WOR713638	A	\N	NYJ
1117679	Shareece	Wright	Shareece Wright	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117679.png	DB	29	WRI652077	A	35	BAL
1631028	Jerel	Worthy	Jerel Worthy	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631028.png	DL	25	WOR723988	A	69	BUF
2174792	Shane	Wynn	Shane Wynn	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	WYN154641	A	\N	NO
2058319	John	Youboty	John Youboty	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	YOU010928	A	\N	DEN
2130166	Lou	Young	Lou Young	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	YOU379662	A	\N	CAR
1675666	Darrel	Young	Darrel Young	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675666.png	RB	29	YOU170298	A	36	WAS
2129558	Kenneth	Acker	Kenneth Acker	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129558.png	DB	24	ACK184395	A	20	SF
1893167	Davante	Adams	Davante Adams	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1893167.png	WR	23	ADA218591	A	17	GB
1632223	Joe	Adams	Joe Adams	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ADA404040	A	\N	HOU
1977069	Kendrick	Adams	Kendrick Adams	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ADA476163	A	\N	TEN
1243552	Frank	Alexander	Frank Alexander	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1243552.png	DL	26	ALE244551	IR	90	CAR
1984250	Kwon	Alexander	Kwon Alexander	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1984250.png	LB	21	ALE492854	A	58	TB
2174119	D.J.	Alexander	D.J. Alexander	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	ALE227676	A	57	KC
1114610	Miles	Austin	Miles Austin	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	AUS467198	A	\N	PHI
405859	Jonathan	Babineaux	Jonathan Babineaux	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/405859.png	DL	34	BAB717191	A	95	ATL
1616157	Patrick	Bailey	Patrick Bailey	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BAI568176	A	\N	TEN
1975942	Brandon	Barden	Brandon Barden	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BAR114932	A	\N	DAL
432990	Alan	Ball	Alan Ball	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/432990.png	DB	31	BAL451070	A	24	CHI
522814	Gary	Barnidge	Gary Barnidge	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/522814.png	TE	30	BAR521746	A	82	CLE
1616907	Connor	Barth	Connor Barth	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616907.png	K	29	BAR722763	A	10	TB
2130273	Zach	Bauman	Zach Bauman	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BAU476491	A	\N	ARI
1765885	Vic	Beasley	Vic Beasley	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1765885.png	LB	23	BEA528276	A	44	ATL
1751828	Le'Veon	Bell	Le'Veon Bell	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1751828.png	RB	24	BEL474300	IR	26	PIT
1907		Bengals	Bengals	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	426	\N	\N	CIN
1114977	Aaron	Berry	Aaron Berry	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BER457560	A	\N	CLE
1674110	E.J.	Biggers	E.J. Biggers	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674110.png	DB	28	BIG377790	A	\N	NE
1954		Bills	Bills	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TQB	0	509	\N	\N	BUF
1904		Bills	Bills	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	409	\N	\N	BUF
2104		Bills	Bills	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	D	0	209	\N	\N	BUF
2174828	Qumain	Black	Qumain Black	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BLA147075	A	\N	CHI
1620455	LeGarrette	Blount	LeGarrette Blount	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620455.png	RB	29	BLO626278	IR	29	NE
1924		Chargers	Chargers	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	414	\N	\N	SD
1272548	Martez	Wilson	Martez Wilson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL750803	A	\N	DAL
1274395	Billy	Winn	Billy Winn	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1274395.png	DL	26	WIN429360	A	99	IND
1697297	Cierre	Wood	Cierre Wood	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1697297.png	RB	25	WOO018213	IR	41	BUF
424119	LaMarr	Woodley	LaMarr Woodley	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/424119.png	LB	31	WOO324724	A	56	ARI
563636	Doug	Worthington	Doug Worthington	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	28	WOR709813	A	92	STL
1631293	Khalid	Wooten	Khalid Wooten	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WOO791691	A	\N	TEN
2129669	James	Wright	James Wright	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129669.png	WR	24	WRI346086	IR	\N	CIN
1824823	Odell	Beckham	Odell Beckham Jr.	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824823.png	WR	23	BEC573252	A	13	NYG
2174793	Darius	Allen	Darius Allen	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ALL097402	A	\N	BAL
1737246	David	Amerson	David Amerson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737246.png	DB	24	AME404790	A	29	OAK
2174827	Jonathan	Anderson	Jonathan Anderson	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	AND315434	A	58	CHI
1631797	Marc	Anthony	Marc Anthony	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ANT155143	A	\N	PHI
2059226	Brian	Arnfelt	Brian Arnfelt	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ARN351871	A	\N	PIT
2130873	George	Atkinson	George Atkinson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	ATK679343	A	\N	OAK
2174949	Rasheed	Bailey	Rasheed Bailey	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	22	BAI572997	A	\N	JAC
1682170	Montee	Ball	Montee Ball	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BAL572158	A	\N	NE
1631827	Kenjon	Barner	Kenjon Barner	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631827.png	RB	26	BAR236353	A	34	PHI
556598	Connor	Barwin	Connor Barwin	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/556598.png	LB	29	BAR811942	A	98	PHI
2130596	Alex	Bayer	Alex Bayer	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BAY012833	A	\N	SD
1979372	Josh	Bellamy	Josh Bellamy	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979372.png	WR	26	BEL647328	A	11	CHI
1272540	Arrelious	Benn	Arrelious Benn	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272540.png	WR	27	BEN192067	IR	12	JAC
556546	Chase	Blackburn	Chase Blackburn	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BLA203310	IR	\N	CAR
2053495	Brandon	Bogotay	Brandon Bogotay	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	BOG533943	A	\N	TB
1749743	Anthony	Boone	Anthony Boone	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	BOO270860	A	\N	DET
2175304	Andrew	Bose	Andrew Bose	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BOS198481	A	\N	BAL
1619548	Da'Quan	Bowers	Da'Quan Bowers	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619548.png	DL	26	BOW198049	A	\N	TB
1673383	Josh	Boyce	Josh Boyce	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1673383.png	WR	24	BOY049190	A	82	IND
1632063	Brandon	Boykin	Brandon Boykin	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1632063.png	DB	25	BOY585357	A	\N	CAR
1664406	Michael	Brockers	Michael Brockers	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664406.png	DL	25	BRO060116	A	90	STL
2129411	Jay	Bromley	Jay Bromley	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129411.png	DL	23	BRO106058	A	96	NYG
584399	Andre	Brown	Andre Brown	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BRO251022	A	\N	HOU
1762348	DaRon	Brown	DaRon Brown	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BRO367442	A	\N	FA
2057701	Justin	Brown	Justin Brown	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BRO502709	A	\N	BUF
1979408	Omar	Brown	Omar Brown	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRO567585	A	\N	BAL
1630321	Zach	Brown	Zach Brown	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630321.png	LB	26	BRO777933	A	\N	BUF
1272524	Dez	Bryant	Dez Bryant	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272524.png	WR	27	BRY336027	IR	88	DAL
1664548	Michael	Buchanan	Michael Buchanan	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BUC168012	A	\N	BUF
395935	Nate	Burleson	Nate Burleson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BUR338629	A	\N	CLE
1114773	Jarrett	Bush	Jarrett Bush	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BUS248356	A	\N	GB
1117848	Donald	Butler	Donald Butler	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117848.png	LB	27	BUT163080	A	56	SD
2174809	Damiere	Byrd	Damiere Byrd	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	BYR127180	A	\N	CAR
1682076	Michael	Campanaro	Michael Campanaro	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1682076.png	WR	25	CAM169006	IR	15	BAL
1759997	Shane	Carden	Shane Carden	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	CAR047283	A	\N	CHI
1117460	Roc	Carmichael	Roc Carmichael	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAR225329	A	\N	ARI
1631879	Jurrell	Casey	Jurrell Casey	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631879.png	DL	26	CAS224804	A	99	TEN
1137271	Alex	Carrington	Alex Carrington	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137271.png	DL	28	CAR486635	A	98	BUF
1824648	Donald	Celiscar	Donald Celiscar	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CEL395705	A	\N	IND
1263300	Prince	Amukamara	Prince Amukamara	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1263300.png	DB	26	AMU221488	A	\N	JAC
2130220	Blake	Annen	Blake Annen	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	ANN355964	A	\N	BUF
1616686	Kyle	Arrington	Kyle Arrington	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616686.png	DB	29	ARR196395	A	24	BAL
1664860	Stedman	Bailey	Stedman Bailey	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664860.png	WR	25	BAI627251	A	12	STL
2008680	Neiron	Ball	Neiron Ball	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2008680.png	LB	23	BAL573596	IR	58	OAK
433287	Antwan	Barnes	Antwan Barnes	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BAR240987	A	\N	NYJ
2058153	Joplo	Bartu	Joplo Bartu	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058153.png	LB	26	BAR806381	A	54	JAC
2175055	DeVante	Bausby	DeVante Bausby	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	BAU711100	A	\N	CHI
1860744	Kelvin	Benjamin	Kelvin Benjamin	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1860744.png	WR	25	BEN150538	IR	13	CAR
2129670	Ken	Bishop	Ken Bishop	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BIS508098	A	\N	DAL
1691443	Emory	Blake	Emory Blake	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BLA574783	A	\N	STL
2060187	Kenneth	Boatright	Kenneth Boatright	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	26	BOA572176	A	79	DAL
1759439	Detrick	Bonner	Detrick Bonner	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BON534989	A	\N	FA
2129491	Tre	Boston	Tre Boston	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2129491.png	DB	23	BOS695248	A	33	CAR
563113	Zack	Bowman	Zack Bowman	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BOW667924	A	\N	MIA
1117305	Allen	Bradford	Allen Bradford	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BRA094086	A	\N	ATL
1737685	Tyler	Bray	Tyler Bray	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737685.png	QB	24	BRA780271	A	9	KC
1116495	Jasper	Brinkley	Jasper Brinkley	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116495.png	LB	30	BRI423678	A	53	NYG
2063144	Malcolm	Bronson	Malcolm Bronson	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRO106944	A	\N	CHI
1975909	Derrius	Brooks	Derrius Brooks	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRO122494	A	\N	TB
2130838	Austin	Brown	Austin Brown	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	BRO268017	IR	\N	NO
1971914	Bryce	Brown	Bryce Brown	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971914.png	RB	24	BRO279039	A	36	SEA
2130274	Jonathan	Brown	Jonathan Brown	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BRO499880	A	\N	DAL
1273898	Vincent	Brown	Vincent Brown	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BRO744480	A	\N	SD
1737158	Martavis	Bryant	Martavis Bryant	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737158.png	WR	24	BRY499889	A	10	PIT
2059192	Marcus	Burley	Marcus Burley	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059192.png	DB	25	BUR347280	A	28	SEA
537434	Darius	Butler	Darius Butler	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/537434.png	DB	30	BUT141928	A	20	IND
2174829	Bryce	Callahan	Bryce Callahan	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	CAL520433	A	\N	CHI
1273666	Tank	Carder	Tank Carder	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273666.png	LB	27	CAR049002	A	59	CLE
1977121	Derek	Carrier	Derek Carrier	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977121.png	TE	25	CAR471726	IR	89	WAS
504438	Antoine	Cason	Antoine Cason	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAS362242	A	\N	BAL
1273325	Josh	Chapman	Josh Chapman	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CHA430904	A	\N	IND
1621175	DeVonte	Christopher	DeVonte Christopher	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CHR616935	A	\N	FA
563861	Keenan	Clayton	Keenan Clayton	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	CLA797667	A	\N	ARI
1632199	Terrence	Cody	Terrence Cody	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COD600327	A	\N	BAL
2062168	Lanier	Coleman	Lanier Coleman	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COL296853	A	\N	TEN
1632065	Sanders	Commings	Sanders Commings	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	COM283885	A	\N	KC
1984220	Amari	Cooper	Amari Cooper	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1984220.png	WR	21	COO487703	A	89	OAK
2189224	Jeremy	Crayton	Jeremy Crayton	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CRA815386	A	\N	SEA
2060063	Andy	Cruse	Andy Cruse	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CRU567222	A	\N	MIN
1665140	A.J.	Klein	A.J. Klein	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1665140.png	LB	24	KLE216860	A	56	CAR
1880820	C.J.	Anderson	C.J. Anderson	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1880820.png	RB	25	AND163330	A	22	DEN
1979709	Joe	Anderson	Joe Anderson	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979709.png	WR	27	AND315508	A	\N	NYJ
1765317	Ezekiel	Ansah	Ezekiel Ansah	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1765317.png	DL	26	ANS059326	A	94	DET
1693202	R.J.	Archer	R.J. Archer	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	ARC562720	A	\N	SEA
2061215	Cameron	Artis-Payne	Cameron Artis-Payne	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2061215.png	RB	25	ART387205	A	34	CAR
1116984	Brandon	Bair	Brandon Bair	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116984.png	DL	31	BAI742577	A	93	PHI
2122782	Marcus	Ball	Marcus Ball	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	28	BAL539082	A	\N	CAR
1664140	Matt	Barkley	Matt Barkley	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664140.png	QB	25	BAR192558	A	9	ARI
1759765	Anthony	Barr	Anthony Barr	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759765.png	LB	24	BAR550678	A	55	MIN
1888780	David	Bass	David Bass	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1888780.png	DL	25	BAS384856	A	51	TEN
2176103	Camaron	Beard	Camaron Beard	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BEA354939	A	\N	JAC
1244136	Nick	Bellore	Nick Bellore	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244136.png	LB	26	BEL740458	A	50	SF
2189885	Michael	Bennett	Michael Bennett	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	BEN384016	A	\N	CIN
424656	Antoine	Bethea	Antoine Bethea	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/424656.png	DB	31	BET074391	IR	41	SF
559522	Desmond	Bishop	Desmond Bishop	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559522.png	LB	31	BIS247164	IR	\N	WAS
2174872	Bernard	Blake	Bernard Blake	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BLA573836	A	\N	GB
2174873	Javess	Blue	Javess Blue	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BLU308410	IR	\N	GB
1701956	Alan	Bonner	Alan Bonner	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BON589081	A	\N	HOU
1985606	Brandon	Bostick	Brandon Bostick	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1985606.png	TE	26	BOS614141	A	\N	NYJ
1117602	NaVorro	Bowman	NaVorro Bowman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117602.png	LB	27	BOW596979	A	53	SF
1117603	Brett	Brackett	Brett Brackett	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BRA068131	A	\N	TEN
2174297	Quan	Bray	Quan Bray	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	22	BRA778039	A	11	IND
1825182	Kyle	Brindza	Kyle Brindza	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	BRI391128	A	\N	NYJ
1114534	Ahmad	Brooks	Ahmad Brooks	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114534.png	LB	32	BRO114525	A	55	SF
1272852	Antonio	Brown	Antonio Brown	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272852.png	WR	27	BRO000001	A	84	PIT
2062132	Jaron	Brown	Jaron Brown	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062132.png	WR	26	BRO486303	A	13	ARI
1700701	Marlon	Brown	Marlon Brown	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700701.png	WR	24	BRO558667	IR	14	BAL
1989533	Keith	Browner	Keith Browner	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1989533.png	DL	28	BRO795294	A	\N	CHI
1116974	Corbin	Bryant	Corbin Bryant	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116974.png	DL	28	BRY320649	A	97	BUF
1691353	Vontaze	Burfict	Vontaze Burfict	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691353.png	LB	25	BUR044491	A	55	CIN
2174798	Clay	Burton	Clay Burton	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BUR759651	IR	\N	BUF
1145226	Crezdon	Butler	Crezdon Butler	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1145226.png	DB	28	BUT133826	A	29	DET
565145	Victor	Butler	Victor Butler	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BUT604556	A	\N	NYG
407741	Andre	Caldwell	Andre Caldwell	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/407741.png	WR	30	CAL102494	A	12	DEN
2058544	Jordan	Campbell	Jordan Campbell	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	CAM442489	IR	\N	WAS
2175106	Nordly	Capi	Nordly Capi	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	CAP202201	A	\N	BAL
407903	John	Carlson	John Carlson	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	CAR142530	A	\N	ARI
1631651	T.J.	Carrie	T.J. Carrie	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631651.png	DB	25	CAR470432	A	38	OAK
1117831	David	Carter	David Carter	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CAR640974	A	\N	CHI
1245173	James	Casey	James Casey	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	CAS214381	A	\N	DEN
408126	Brent	Celek	Brent Celek	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408126.png	TE	31	CEL251361	A	87	PHI
1245221	John	Chiles	John Chiles	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CHI576868	A	\N	CHI
1889909	Adrian	Amos	Adrian Amos	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1889909.png	DB	22	AMO568523	A	38	CHI
1850724	Stephone	Anthony	Stephone Anthony	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1850724.png	LB	23	ANT204590	A	50	NO
2059631	Ray-Ray	Armstrong	Ray-Ray Armstrong	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059631.png	LB	25	ARM649391	A	54	SF
1114813	Geno	Atkins	Geno Atkins	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114813.png	DL	28	ATK216644	A	97	CIN
1977108	Chase	Baker	Chase Baker	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BAK093226	A	\N	MIN
1246159	Ramses	Barden	Ramses Barden	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BAR116168	A	\N	JAC
1979719	Ben	Bass	Ben Bass	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BAS362284	A	\N	NE
1665096	Steve	Beauharnais	Steve Beauharnais	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BEA758553	A	\N	SF
1979352	Brenton	Bersin	Brenton Bersin	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1979352.png	WR	25	BER717297	A	\N	CAR
2059205	Brandan	Bishop	Brandan Bishop	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BIS240736	A	\N	PHI
2129554	Alfred	Blue	Alfred Blue	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129554.png	RB	24	BLU106654	A	28	HOU
1664202	Jon	Bostic	Jon Bostic	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664202.png	LB	24	BOS528427	A	58	NE
1888149	Nick	Boyle	Nick Boyle	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1888149.png	TE	23	BOY688993	A	82	BAL
2167527	Delvin	Breaux	Delvin Breaux	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	26	BRE019807	A	40	NO
1857700	Mason	Brodine	Mason Brodine	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BRO086802	A	\N	NE
2174996	Cariel	Brooks	Cariel Brooks	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	BRO119059	A	35	ARI
1619684	Arthur	Brown	Arthur Brown	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619684.png	LB	25	BRO267826	A	59	BAL
2174213	Da'Ron	Brown	Da'Ron Brown	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174213.png	WR	24	BRO367295	A	4	KC
2129438	John	Brown	John Brown	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129438.png	WR	26	BRO494712	A	12	ARI
1760014	Preston	Brown	Preston Brown	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1760014.png	LB	23	BRO581187	A	52	BUF
1978056	Mike	Brown	Mike Brown	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BRO564694	A	\N	CAR
407354	Brandon	Browner	Brandon Browner	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/407354.png	DB	31	BRO784737	A	39	NO
1674452	Desmond	Bryant	Desmond Bryant	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674452.png	DL	30	BRY332277	A	92	CLE
312308	Matt	Bryant	Matt Bryant	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/312308.png	K	40	BRY500226	IR	3	ATL
2174977	Quayshawne	Buckley	Quayshawne Buckley	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BUC686367	A	\N	TB
1679738	Rex	Burkhead	Rex Burkhead	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679738.png	RB	25	BUR268979	A	33	CIN
1977120	Kaelin	Burnett	Kaelin Burnett	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BUR413863	A	\N	TEN
1759407	Michael	Burton	Michael Burton	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759407.png	RB	24	BUR792195	A	46	DET
2130223	Trey	Burton	Trey Burton	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	BUR817654	A	47	PHI
2190506	Deon	Butler	Deon Butler	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BUT160455	A	\N	DET
2130816	Carrington	Byndom	Carrington Byndom	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	BYN230717	A	\N	ARI
1264599	Josh	Bynes	Josh Bynes	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1264599.png	LB	26	BYN301611	A	57	DET
1244459	Jordan	Cameron	Jordan Cameron	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244459.png	TE	27	CAM107841	A	84	MIA
520548	Calais	Campbell	Calais Campbell	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/520548.png	DL	29	CAM208843	A	93	ARI
1679724	Anthony	Cantele	Anthony Cantele	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	CAN749892	A	\N	FA
564870	Nolan	Carroll	Nolan Carroll	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564870.png	DB	29	CAR542658	IR	23	PHI
565189	Jonathan	Casillas	Jonathan Casillas	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565189.png	LB	28	CAS320904	A	52	NYG
2130622	Brelan	Chancellor	Brelan Chancellor	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CHA182802	A	\N	PIT
2185207	Jawon	Chisholm	Jawon Chisholm	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	CHI761708	A	\N	PIT
2130701	Steven	Clarke	Steven Clarke	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	CLA662711	A	\N	TEN
396781	Chris	Clemons	Chris Clemons	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396781.png	DL	34	CLE390635	A	\N	SEA
2174976	Quinton	Alston	Quinton Alston	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ALS627185	A	\N	TB
1852846	Rory	Anderson	Rory Anderson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	AND490104	IR	48	SF
2130156	Kenny	Anunike	Kenny Anunike	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	ANU431455	A	91	DEN
2175136	Chi Chi	Ariguzo	Chi Chi Ariguzo	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ARI438985	IR	\N	SD
1661883	Chris	Baker	Chris Baker	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1661883.png	DL	28	BAK000000	A	92	WAS
2129673	Lonnie	Ballentine	Lonnie Ballentine	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129673.png	DB	22	BAL694018	A	39	HOU
2174932	Deion	Barnes	Deion Barnes	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	BAR270183	A	50	NYJ
1688607	Sam	Barrington	Sam Barrington	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1688607.png	LB	25	BAR653446	IR	58	GB
2058326	Daren	Bates	Daren Bates	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058326.png	LB	25	BAT105064	A	\N	OAK
1737101	Blake	Bell	Blake Bell	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737101.png	TE	24	BEL227990	A	84	SF
1749692	Deion	Belue	Deion Belue	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BEL808821	IR	\N	JAC
2081439	Bryan	Bennett	Bryan Bennett	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	BEN267918	A	\N	IND
2129519	Bene'	Benwikere	Bene' Benwikere	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2129519.png	DB	24	BEN780726	IR	25	CAR
2174794	Brennen	Beyer	Brennen Beyer	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	BEY165981	A	45	BAL
1630860	Justin	Blackmon	Justin Blackmon	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630860.png	WR	26	BLA284263	A	14	JAC
2174306	Issac	Blakeney	Issac Blakeney	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	BLA609497	A	\N	PIT
1664304	Tommy	Bohanon	Tommy Bohanon	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664304.png	RB	25	BOH116572	A	40	NYJ
2130222	Kadron	Boone	Kadron Boone	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BOO423941	A	\N	IND
2175139	Cameron	Botticelli	Cameron Botticelli	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BOT717170	A	\N	SD
2130298	Chris	Boyd	Chris Boyd	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BOY068876	A	\N	DAL
1737364	Carl	Bradford	Carl Bradford	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737364.png	LB	23	BRA096195	A	54	GB
1262880	Andre	Branch	Andre Branch	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1262880.png	DL	26	BRA489900	A	\N	MIA
1776297	Brandon	Bridge	Brandon Bridge	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	BRI113475	A	\N	FA
1115016	Kenny	Britt	Kenny Britt	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1115016.png	WR	27	BRI708216	A	18	STL
1244951	Ron	Brooks	Ron Brooks	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244951.png	DB	27	BRO167445	A	\N	PHI
2174987	Chris	Brown	Chris Brown	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BRO340618	A	\N	ATL
1760013	Dominique	Brown	Dominique Brown	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BRO395301	A	\N	PIT
1975977	Kourtnei	Brown	Kourtnei Brown	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975977.png	LB	27	BRO521714	A	59	TB
1977789	Sammy	Brown	Sammy Brown	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BRO677994	A	\N	STL
2189225	Ernst	Brun	Ernst Brun	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BRU494667	A	\N	WAS
2175098	Paul	Browning	Paul Browning	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BRO821739	A	\N	CAR
559795	Red	Bryant	Red Bryant	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559795.png	DL	31	BRY512697	A	71	ARI
1632503	Randy	Bullock	Randy Bullock	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632503.png	K	26	BUL521842	A	8	NYJ
1242890	Morgan	Burnett	Morgan Burnett	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1242890.png	DB	27	BUR426788	A	42	GB
1682044	Ross	Cockrell	Ross Cockrell	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1682044.png	DB	24	COC494116	A	31	PIT
1737368	Brandon	Coleman	Brandon Coleman	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737368.png	WR	23	COL222062	A	16	NO
2175407	Henry	Coley	Henry Coley	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	COL367862	A	\N	NO
408644	Marques	Colston	Marques Colston	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408644.png	WR	32	COL777594	A	12	NO
408657	Jon	Condo	Jon Condo	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	34	CON072674	A	59	OAK
2130870	Travis	Coons	Travis Coons	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130870.png	K	24	COO469595	A	6	CLE
1672686	Quinton	Coples	Quinton Coples	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	COP568656	A	\N	MIA
559518	Tyson	Alualu	Tyson Alualu	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559518.png	DL	28	ALU059326	A	93	JAC
405598	Derek	Anderson	Derek Anderson	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/405598.png	QB	32	AND180512	A	3	CAR
2174857	Zaire	Anderson	Zaire Anderson	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	AND625465	A	47	DEN
2155508	Ike	Ariguzo	Ike Ariguzo	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	ARI447235	A	\N	GB
2175105	Richard	Ash	Richard Ash	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	ASH105787	A	94	JAC
1700485	Edwin	Baker	Edwin Baker	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BAK183603	A	\N	NO
2056912	Chris	Banjo	Chris Banjo	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2056912.png	DB	26	BAN373418	A	32	GB
1689515	T.J.	Barnes	T.J. Barnes	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1689515.png	DL	25	BAR378535	A	90	BUF
1632196	Mark	Barron	Mark Barron	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632196.png	DB	26	BAR661788	A	26	STL
2175414	Houston	Bates	Houston Bates	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	BAT213644	A	96	WAS
2167529	Braylon	Bell	Braylon Bell	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BEL248346	A	\N	WAS
1674982	Marcus	Benard	Marcus Benard	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BEN002075	A	\N	ARI
564954	Martellus	Bennett	Martellus Bennett	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564954.png	TE	29	BEN379259	IR	\N	NE
1273375	Jake	Bequette	Jake Bequette	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BEQ652601	A	\N	NE
2060571	E.J.	Bibbs	E.J. Bibbs	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	BIB074624	A	88	CLE
2214703	Craig	Bills	Craig Bills	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BIL684372	A	\N	PHI
406601	Will	Blackmon	Will Blackmon	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/406601.png	DB	31	BLA309190	A	41	WAS
1979710	Matt	Blanchard	Matt Blanchard	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979710.png	QB	27	BLA620023	A	\N	CHI
1632301	Brandon	Bolden	Brandon Bolden	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632301.png	RB	26	BOL167697	A	38	NE
1672907	Zach	Boren	Zach Boren	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BOR399715	A	\N	TEN
2060061	A.J.	Bouye	A.J. Bouye	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060061.png	DB	24	BOU651714	A	34	HOU
1664359	Josh	Boyd	Josh Boyd	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664359.png	DL	26	BOY228358	A	93	GB
1123599	Sam	Bradford	Sam Bradford	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123599.png	QB	28	BRA101548	A	7	PHI
537432	Tyvon	Branch	Tyvon Branch	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/537432.png	DB	29	BRA530779	A	\N	ARI
2132704	Courtney	Bridget	Courtney Bridget	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRI128262	A	\N	WAS
1737623	Terrence	Brooks	Terrence Brooks	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737623.png	DB	25	BRO172431	A	31	BAL
1123366	Chykie	Brown	Chykie Brown	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123366.png	DB	29	BRO350727	A	\N	CIN
1251085	Donald	Brown	Donald Brown	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1251085.png	RB	28	BRO398445	A	\N	NE
2175090	Mack	Brown	Mack Brown	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	BRO549550	A	\N	WAS
1246078	Sergio	Brown	Sergio Brown	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1246078.png	DB	27	BRO686791	A	38	JAC
563638	David	Bruton	David Bruton	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563638.png	DB	28	BRU814462	A	\N	WAS
1737409	Deone	Bucannon	Deone Bucannon	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737409.png	DB	23	BUC019775	A	20	ARI
1737264	Max	Bullough	Max Bullough	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	BUL613806	A	\N	HOU
1273899	Miles	Burris	Miles Burris	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BUR610522	A	\N	OAK
436526	Michael	Bush	Michael Bush	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BUS277873	A	\N	ARI
1732362	Rafael	Bush	Rafael Bush	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1732362.png	DB	28	BUS288052	A	\N	DET
2191265	James	Butler	James Butler	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BUT249498	A	\N	GB
2130120	Jeremy	Butler	Jeremy Butler	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130120.png	WR	24	BUT279308	A	17	BAL
1977791	Jake	Byrne	Jake Byrne	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BYR668150	A	\N	SD
1979389	LaRon	Byrd	LaRon Byrd	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979389.png	WR	26	BYR472969	A	\N	WAS
1971904	D.J.	Campbell	D.J. Campbell	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAM230172	A	\N	ARI
1752624	Dres	Anderson	Dres Anderson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	AND196249	IR	6	SF
1737505	Henry	Anderson	Henry Anderson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	AND287484	IR	96	IND
1776334	Antonio	Andrews	Antonio Andrews	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1776334.png	RB	23	AND650999	A	26	TEN
563049	Pat	Angerer	Pat Angerer	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ANG284961	A	\N	ATL
2176101	Boris	Anyama	Boris Anyama	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ANY059326	A	\N	ATL
2174894	Thurston	Armbrister	Thurston Armbrister	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	ARM002678	A	57	JAC
1245626	Matt	Asiata	Matt Asiata	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245626.png	RB	28	ASI070576	A	44	MIN
1823779	David	Ausberry	David Ausberry	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	AUS013223	A	\N	DET
1689664	Dan	Bailey	Dan Bailey	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1689664.png	K	28	BAI159977	A	5	DAL
1977807	Sean	Baker	Sean Baker	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977807.png	DB	27	BAK600332	A	\N	CLE
1700836	Johnthan	Banks	Johnthan Banks	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700836.png	DB	26	BAN557808	A	27	TB
1737543	Tavaris	Barnes	Tavaris Barnes	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	BAR378720	A	90	NO
1679954	Lamin	Barrow	Lamin Barrow	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679954.png	LB	25	BAR662715	A	57	CHI
1979402	Phil	Bates	Phil Bates	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BAT137358	A	\N	DAL
1662348	Joique	Bell	Joique Bell	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BEL389385	A	\N	DET
1768610	Tim	Benford	Tim Benford	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BEN038932	A	\N	PIT
559771	Michael	Bennett	Michael Bennett	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559771.png	DL	30	BEN384391	A	72	SEA
2129524	Nat	Berhe	Nat Berhe	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129524.png	DB	24	BER174739	IR	29	NYG
1971894	Christo	Bilukidi	Christo Bilukidi	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971894.png	DL	26	BIL787320	A	\N	WAS
1824792	Angelo	Blackson	Angelo Blackson	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	BLA326665	A	95	TEN
1706328	Rob	Blanchflower	Rob Blanchflower	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1706328.png	TE	25	BLA646699	IR	\N	PIT
1272943	Omar	Bolden	Omar Bolden	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272943.png	DB	27	BOL253786	IR	\N	CHI
1682171	Chris	Borland	Chris Borland	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BOR432476	A	\N	SF
406856	Dwayne	Bowe	Dwayne Bowe	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/406856.png	WR	31	BOW091822	A	80	CLE
1691126	Tajh	Boyd	Tajh Boyd	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	BOY341422	A	\N	PIT
1619567	Nigel	Bradham	Nigel Bradham	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619567.png	LB	26	BRA104685	A	\N	PHI
2167521	Tevrin	Brandon	Tevrin Brandon	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRA567310	A	\N	DEN
1723149	Corey	Broomfield	Corey Broomfield	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRO188853	A	\N	FA
2130815	Corey	Brown	Corey Brown	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2130815.png	WR	24	BRO356941	A	10	CAR
558835	Everette	Brown	Everette Brown	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BRO430930	A	\N	CLE
1852916	Malcolm	Brown	Malcolm Brown	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	22	BRO550137	A	39	STL
1116715	Stevie	Brown	Stevie Brown	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116715.png	DB	28	BRO695002	A	\N	KC
1888777	Armonty	Bryant	Armonty Bryant	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1888777.png	DL	25	BRY213205	A	95	CLE
1700840	Chad	Bumphis	Chad Bumphis	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BUM664809	A	\N	JAC
2130157	Isaiah	Burse	Isaiah Burse	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130157.png	WR	24	BUR697033	A	\N	SD
407605	Reggie	Bush	Reggie Bush	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/407605.png	RB	31	BUS294963	IR	23	SF
2132696	Malcolm	Butler	Malcolm Butler	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132696.png	DB	26	BUT474917	A	21	NE
1978283	Travaris	Cadet	Travaris Cadet	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1978283.png	RB	27	CAD290360	A	38	NO
2174307	DiAndre	Campbell	DiAndre Campbell	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	CAM229208	A	19	SF
2130692	Ri'Shard	Anderson	Ri'Shard Anderson	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	AND453551	A	\N	TEN
1631205	Ryan	Aplin	Ryan Aplin	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	APL296636	A	\N	FA
1244486	Doug	Baldwin	Doug Baldwin	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244486.png	WR	27	BAL312014	A	89	SEA
1751258	Vick	Ballard	Vick Ballard	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1751258.png	RB	25	BAL687264	IR	\N	NO
1977070	Joe	Banyard	Joe Banyard	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977070.png	RB	27	BAN826315	A	29	JAC
2130146	Calvin	Barnett	Calvin Barnett	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BAR402263	A	\N	MIA
2130832	Shaquil	Barrett	Shaquil Barrett	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	BAR645290	A	48	DEN
2185205	Brad	Bars	Brad Bars	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	BAR710913	A	66	NYG
406163	Jackie	Battle	Jackie Battle	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BAT414006	A	\N	TEN
1977145	Cole	Beasley	Cole Beasley	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977145.png	WR	26	BEA434808	A	11	DAL
565185	Travis	Beckum	Travis Beckum	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BEC798709	A	\N	NO
1751721	Kenny	Bell	Kenny Bell	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1751721.png	WR	24	BEL431552	IR	80	TB
1871307	Michael	Bennett	Michael Bennett	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1871307.png	DL	23	BEN000000	A	96	JAC
1737248	Giovani	Bernard	Giovani Bernard	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737248.png	RB	24	BER219765	A	25	CIN
1599752	Kroy	Biermann	Kroy Biermann	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1599752.png	LB	30	BIE712400	A	71	ATL
1245841	Armon	Binns	Armon Binns	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BIN720265	A	\N	KC
1977072	Antwon	Blake	Antwon Blake	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977072.png	DB	25	BLA573701	A	\N	TEN
1633101	Robert	Blanton	Robert Blanton	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633101.png	DB	26	BLA768016	A	\N	BUF
395928	Anquan	Boldin	Anquan Boldin	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/395928.png	WR	35	BOL283010	A	81	SF
1749727	Blake	Bortles	Blake Bortles	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1749727.png	QB	23	BOR650964	A	5	JAC
1114616	Stephen	Bowen	Stephen Bowen	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114616.png	DL	32	BOW129829	A	72	NYJ
1620456	John	Boyett	John Boyett	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BOY533092	A	\N	DEN
520778	Ahmad	Bradshaw	Ahmad Bradshaw	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/520778.png	RB	30	BRA254359	IR	44	IND
2132787	Cameron	Brate	Cameron Brate	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	BRA663798	A	84	TB
395932	Lance	Briggs	Lance Briggs	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	BRI150227	IR	\N	CHI
556966	Richie	Brockel	Richie Brockel	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BRO057770	A	\N	CAR
2133815	Demitrius	Bronson	Demitrius Bronson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BRO106752	A	\N	MIA
2130266	Deon	Broomfield	Deon Broomfield	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRO189439	A	\N	BUF
1255303	Curtis	Brown	Curtis Brown	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BRO360893	A	\N	NYJ
1114272	Jalil	Brown	Jalil Brown	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114272.png	DB	28	BRO477381	IR	31	IND
1996819	Malcom	Brown	Malcom Brown	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996819.png	DL	22	BRO550723	A	90	NE
437033	Tarell	Brown	Tarell Brown	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/437033.png	DB	31	BRO695589	IR	25	NE
2129674	Christian	Bryant	Christian Bryant	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129674.png	DB	24	BRY311774	A	37	STL
407492	Brodrick	Bunkley	Brodrick Bunkley	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BUN459212	A	\N	NO
1245634	Brandon	Burton	Brandon Burton	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	BUR756355	A	\N	IND
2057699	Brice	Butler	Brice Butler	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057699.png	WR	26	BUT116760	A	19	DAL
1242891	Mario	Butler	Mario Butler	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1242891.png	DB	27	BUT482645	A	39	BUF
1665224	Kenny	Cain	Kenny Cain	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	CAI484505	A	\N	FA
2058306	Larry	Black	Larry Black	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BLA097800	A	\N	CIN
2174808	Brian	Blechen	Brian Blechen	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	BLE153561	A	\N	CAR
2129634	Ted	Bolser	Ted Bolser	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	BOL580591	A	\N	ARI
1724930	Chris	Boswell	Chris Boswell	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1724930.png	K	25	BOS743355	A	9	PIT
1630575	Jarrett	Boykin	Jarrett Boykin	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630575.png	WR	26	BOY612580	A	\N	BUF
517273	Alan	Branch	Alan Branch	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517273.png	DL	31	BRA489251	A	97	NE
2058546	Michael	Brooks	Michael Brooks	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	BRO155618	A	\N	SEA
2175305	Daniel	Brown	Daniel Brown	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	BRO365389	A	83	BAL
407235	Ronnie	Brown	Ronnie Brown	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	BRO662745	A	\N	SD
1243556	Ryan	Broyles	Ryan Broyles	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BRO829985	A	\N	DET
1620826	Dan	Buckner	Dan Buckner	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	BUC766135	A	\N	ARI
1971896	Josh	Bush	Josh Bush	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1971896.png	DB	27	BUS252052	A	20	DEN
565629	Jairus	Byrd	Jairus Byrd	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565629.png	DB	29	BYR449897	A	31	NO
1737451	Ibraheim	Campbell	Ibraheim Campbell	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	CAM362155	A	30	CLE
1823794	Tommie	Campbell	Tommie Campbell	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAM665710	A	\N	JAC
407848	Chris	Canty	Chris Canty	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/407848.png	DL	33	CAN796581	IR	99	BAL
1278276	Don	Carey	Don Carey	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1278276.png	DB	29	CAR080364	A	26	DET
582248	Brandon	Carr	Brandon Carr	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/582248.png	DB	29	CAR347675	A	39	DAL
1242983	Bruce	Carter	Bruce Carter	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1242983.png	LB	28	CAR624497	A	\N	NYJ
1674227	Tony	Carter	Tony Carter	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674227.png	DB	29	CAR748336	A	\N	NO
2167523	Duron	Carter	Duron Carter	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CAR646894	A	\N	IND
2057698	Mike	Catapano	Mike Catapano	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057698.png	DL	25	CAT095568	IR	53	NYJ
1130628	Kam	Chancellor	Kam Chancellor	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1130628.png	DB	28	CHA185370	A	31	SEA
1116461	Jamar	Chaney	Jamar Chaney	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	CHA365118	A	\N	OAK
2060277	Stefan	Charles	Stefan Charles	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060277.png	DL	27	CHA600649	A	\N	DET
1860808	Anthony	Chickillo	Anthony Chickillo	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	CHI218488	A	56	PIT
1679957	Morris	Claiborne	Morris Claiborne	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679957.png	DB	26	CLA042651	A	24	DAL
1700342	Will	Clarke	Will Clarke	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700342.png	DL	24	CLA663918	A	93	CIN
2131924	Kaelin	Clay	Kaelin Clay	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2131924.png	WR	24	CLA726206	A	81	BAL
1852852	Jadeveon	Clowney	Jadeveon Clowney	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1852852.png	LB	23	CLO535848	A	90	HOU
2174202	Reshard	Cliett	Reshard Cliett	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	CLI024906	IR	58	HOU
1679894	Deandre	Coleman	Deandre Coleman	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	COL240969	A	62	MIA
492915	Jerricho	Cotchery	Jerricho Cotchery	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/492915.png	WR	33	COT027891	A	82	CAR
1125838	Michael	Crabtree	Michael Crabtree	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125838.png	WR	28	CRA111040	A	15	OAK
2190658	C.J.	Conway	C.J. Conway	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CON756588	A	\N	NYG
1749120	Da'Mon	Cromartie-Smith	Da'Mon Cromartie-Smith	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CRO168870	A	\N	WAS
2058327	Benny	Cunningham	Benny Cunningham	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058327.png	RB	25	CUN106619	A	36	STL
1974197	Matt	Daniels	Matt Daniels	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1974197.png	DB	26	DAN574879	A	30	SD
1265479	Austin	Davis	Austin Davis	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1265479.png	QB	26	DAV164136	A	7	CLE
2130121	Jace	Davis	Jace Davis	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DAV413380	A	\N	HOU
1824669	Ka'Deem	Carey	Ka'Deem Carey	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824669.png	RB	23	CAR085888	A	25	CHI
1680466	Ryan	Carrethers	Ryan Carrethers	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1680466.png	DL	25	CAR468784	A	92	SD
1621066	Chris	Carter	Chris Carter	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1621066.png	LB	27	CAR629400	A	56	BAL
2062158	Chance	Casey	Chance Casey	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062158.png	DB	25	CAS191947	A	\N	JAC
1692064	Mike	Caussin	Mike Caussin	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	CAU606774	IR	\N	WAS
1114912	Shaun	Chapas	Shaun Chapas	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	CHA387434	A	\N	BAL
2064706	Arceto	Clark	Arceto Clark	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CLA158169	A	\N	SEA
1115762	Adrian	Clayborn	Adrian Clayborn	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1115762.png	DL	27	CLA770145	A	99	ATL
1824794	Sammie	Coates	Sammie Coates	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824794.png	WR	23	COA667430	A	14	PIT
1272213	Audie	Cole	Audie Cole	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272213.png	LB	26	COL084915	A	57	MIN
396784	Colin	Cole	Colin Cole	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COL089722	A	\N	CAR
1117486	Kurt	Coleman	Kurt Coleman	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1117486.png	DB	27	COL295827	A	20	CAR
1824775	Justin	Coleman	Justin Coleman	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	COL294032	A	22	NE
1616845	Jed	Collins	Jed Collins	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	COL612676	A	\N	DAL
1974196	Matt	Conrath	Matt Conrath	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CON677425	A	\N	PIT
584314	Jared	Cook	Jared Cook	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/584314.png	TE	29	COO152641	A	\N	GB
2059172	Brandon	Copeland	Brandon Copeland	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059172.png	LB	24	COP276485	A	95	DET
1272574	Kirk	Cousins	Kirk Cousins	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272574.png	QB	27	COU709400	A	8	WAS
2057740	Michael	Cox	Michael Cox	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	COX518362	IR	\N	NYG
1631114	Jack	Crawford	Jack Crawford	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631114.png	DL	27	CRA660264	A	58	DAL
1619870	Jared	Crick	Jared Crick	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1619870.png	DL	26	CRI129618	A	\N	DEN
2130148	Isaiah	Crowell	Isaiah Crowell	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130148.png	RB	23	CRO734794	A	34	CLE
1686001	Justice	Cunningham	Justice Cunningham	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1686001.png	TE	25	CUN505098	A	\N	STL
2174810	Darious	Cummings	Darious Cummings	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CUM493473	A	\N	CAR
1125961	Andy	Dalton	Andy Dalton	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125961.png	QB	28	DAL659900	A	14	CIN
492918	Karlos	Dansby	Karlos Dansby	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/492918.png	LB	34	DAN762622	A	\N	CIN
1977792	A.J.	Davis	A.J. Davis	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DAV115245	A	\N	NO
2058187	Cody	Davis	Cody Davis	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058187.png	DB	26	DAV247918	A	38	STL
504530	Fred	Davis	Fred Davis	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	DAV318170	A	\N	NE
2000079	Mike	Davis	Mike Davis	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2000079.png	RB	23	DAV567050	A	22	SF
1824516	Will	Davis	Will Davis	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824516.png	DB	25	DAV000000	IR	39	BAL
2174795	DeAndre	Carter	DeAndre Carter	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174795.png	WR	23	CAR641798	A	\N	NE
1616704	Dan	Carpenter	Dan Carpenter	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616704.png	K	30	CAR297693	A	2	BUF
552470	Matt	Cassel	Matt Cassel	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/552470.png	QB	33	CAS541133	A	\N	TEN
2190611	Justin	Cherocci	Justin Cherocci	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	CHE270674	A	\N	DET
520636	Patrick	Chung	Patrick Chung	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/520636.png	DB	28	CHU126772	A	23	NE
2174782	Marquez	Clark	Marquez Clark	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CLA456292	A	\N	ATL
1851166	Cameron	Clear	Cameron Clear	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	CLE001411	IR	\N	IND
2175107	Kasey	Closs	Kasey Closs	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CLO139687	A	\N	JAC
565753	Chase	Coffman	Chase Coffman	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565753.png	TE	29	COF681801	A	86	SEA
408538	Trent	Cole	Trent Cole	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408538.png	LB	33	COL192262	A	58	IND
2001839	Tevin	Coleman	Tevin Coleman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2001839.png	RB	22	COL359050	A	26	ATL
2174874	Ricky	Collins	Ricky Collins	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COL678045	A	\N	GB
1737542	Aaron	Colvin	Aaron Colvin	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737542.png	DB	24	COL801093	A	22	JAC
1272978	Chris	Conte	Chris Conte	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272978.png	DB	27	CON690403	A	23	TB
2175056	Kenny	Cook	Kenny Cook	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	COO177054	A	\N	KC
2175108	Desmond	Cooper	Desmond Cooper	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	COO532592	A	\N	JAC
2130334	Damian	Copeland	Damian Copeland	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COP323982	IR	\N	JAC
1109735	Blake	Costanzo	Blake Costanzo	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	COS403812	IR	\N	SF
1892122	Christian	Covington	Christian Covington	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1892122.png	DL	22	COV546722	A	95	HOU
1133568	Perrish	Cox	Perrish Cox	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1133568.png	DB	29	COX570416	A	29	TEN
2175047	Nigel	Crawford	Nigel Crawford	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CRA717880	A	\N	PIT
1620389	Juron	Criner	Juron Criner	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CRI230716	A	\N	NYG
2174895	Eric	Crume	Eric Crume	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	CRU214722	A	\N	CAR
303245	Billy	Cundiff	Billy Cundiff	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	CUN046143	A	\N	BUF
2174926	Justin	Currie	Justin Currie	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	CUR343634	IR	36	NYG
2059181	Jordan	Dangerfield	Jordan Dangerfield	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059181.png	DB	25	DAN230284	A	42	PIT
2174876	Tavarus	Dantzler	Tavarus Dantzler	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DAN765454	A	\N	GB
1824992	Aaron	Davis	Aaron Davis	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DAV114567	A	\N	FA
2174308	Darius	Davis	Darius Davis	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DAV253218	A	\N	SF
1977073	Ryan	Davis	Ryan Davis	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977073.png	DL	27	DAV727624	A	59	JAC
1664819	Derek	Carr	Derek Carr	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664819.png	QB	25	CAR358797	A	4	OAK
2130275	Glenn	Carson	Glenn Carson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	CAR572098	A	\N	NYG
1891819	Jalil	Carter	Jalil Carter	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAR657452	A	\N	MIN
564952	Jamaal	Charles	Jamaal Charles	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564952.png	RB	29	CHA561428	A	25	KC
1128845	Barry	Church	Barry Church	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1128845.png	DB	28	CHU137380	A	42	DAL
1246081	Jimmy	Clausen	Jimmy Clausen	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1246081.png	QB	28	CLA709185	A	2	BAL
1971915	Toney	Clemons	Toney Clemons	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CLE535163	A	\N	CAR
1789457	Tyler	Clutts	Tyler Clutts	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1789457.png	RB	31	CLU606964	A	44	DAL
1977109	Derrick	Coleman	Derrick Coleman	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977109.png	RB	25	COL248659	A	40	SEA
1723146	Jamie	Collins	Jamie Collins	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1723146.png	LB	26	COL607549	A	91	NE
408510	Barry	Cofield	Barry Cofield	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408510.png	DL	32	COF756192	A	98	NYG
1116854	John	Conner	John Conner	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	CON466815	A	\N	BUF
2130676	Gannon	Conway	Gannon Conway	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	CON779136	A	\N	IND
1114590	Riley	Cooper	Riley Cooper	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COO700168	A	\N	PHI
2175310	Brandon	Cottom	Brandon Cottom	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	COT225171	A	\N	SEA
1700843	Fletcher	Cox	Fletcher Cox	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700843.png	DL	25	COX200177	A	91	PHI
1675830	Tom	Crabtree	Tom Crabtree	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	CRA116294	A	\N	NO
553367	Josh	Cribbs	Josh Cribbs	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CRI120494	A	\N	IND
2011030	John	Crockett	John Crockett	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	CRO028230	A	38	GB
408969	Mason	Crosby	Mason Crosby	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408969.png	K	31	CRO369933	A	2	GB
2186555	Tony	Creecy	Tony Creecy	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	CRE073540	A	\N	NE
2184473	A.J.	Cruz	A.J. Cruz	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	CRU779150	A	\N	MIA
1263432	B.J.	Cunningham	B.J. Cunningham	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CUN118244	A	\N	CHI
2175721	Zach	D'Orazio	Zach D'Orazio	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DOR115801	A	\N	NE
2174908	Robertson	Daniel	Robertson Daniel	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	DAN318388	A	31	GB
1998179	Ronald	Darby	Ronald Darby	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	DAR187187	A	28	BUF
2123000	Akeem	Davis	Akeem Davis	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	26	DAV115525	A	35	IND
2174962	Dominique	Davis	Dominique Davis	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	DAV264675	A	\N	KC
2174192	Darius	Kilgo	Darius Kilgo	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	KIL211314	A	98	DEN
1123604	Quinton	Carter	Quinton Carter	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CAR716706	IR	\N	DEN
2130276	Chandler	Catanzaro	Chandler Catanzaro	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130276.png	K	25	CAT093927	A	7	ARI
1707633	Orson	Charles	Orson Charles	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	CHA591533	A	\N	NO
2174940	Imoan	Claiborne	Imoan Claiborne	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CLA042007	A	\N	TB
1265552	Charles	Clay	Charles Clay	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1265552.png	TE	27	CLA715825	IR	85	BUF
2130634	Asante	Cleveland	Asante Cleveland	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	CLE667867	A	45	SD
1243307	Danny	Coale	Danny Coale	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COA396604	A	\N	PIT
1615560	Landon	Cohen	Landon Cohen	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COH427244	A	\N	SEA
2176102	Jasper	Coleman	Jasper Coleman	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COL293520	A	\N	HOU
2059269	Jasper	Collins	Jasper Collins	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COL609258	A	\N	CIN
564361	Kavell	Conner	Kavell Conner	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564361.png	LB	29	CON472381	A	53	SD
567291	Chris	Cook	Chris Cook	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	COO033097	A	\N	SF
2130169	Jerome	Couplin	Jerome Couplin	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	COU263360	IR	21	PHI
2061271	Justin	Cox	Justin Cox	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	COX440040	A	\N	KC
1737608	Corey	Crawford	Corey Crawford	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	CRA549968	A	95	WAS
1749673	Scott	Crichton	Scott Crichton	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1749673.png	DL	24	CRI125056	IR	95	MIN
1850749	Jamison	Crowder	Jamison Crowder	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1850749.png	WR	22	CRO697273	A	80	WAS
1273272	Chris	Culliver	Chris Culliver	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273272.png	DB	27	CUL399285	IR	29	WAS
2141500	Jerome	Cunningham	Jerome Cunningham	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	CUN460050	A	\N	NYG
1114592	Jermaine	Cunningham	Jermaine Cunningham	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	CUN454050	A	\N	NYJ
2220222	Anthony	Dable	Anthony Dable	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	2016	DAB359918	A	8	NYG
1226150	Craig	Dahl	Craig Dahl	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1226150.png	DB	30	DAH251407	A	43	NYG
1115107	Jeff	Cumberland	Jeff Cumberland	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1115107.png	TE	28	CUM139116	A	\N	SD
1692684	Mike	Daniels	Mike Daniels	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1692684.png	DL	26	DAN575730	A	76	GB
409170	Owen	Daniels	Owen Daniels	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/409170.png	TE	33	DAN576298	A	81	DEN
1769263	Lavonte	David	Lavonte David	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1769263.png	LB	26	DAV026654	A	54	TB
1759544	Carl	Davis	Carl Davis	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759544.png	DL	24	DAV212103	A	94	BAL
2174773	Dyshawn	Davis	Dyshawn Davis	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DAV288804	A	\N	WAS
518156	Glenn	Dorsey	Glenn Dorsey	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518156.png	DL	30	DOR587585	A	90	SF
408416	Kellen	Clemens	Kellen Clemens	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408416.png	QB	32	CLE075538	A	10	SD
2130333	Terrance	Cobb	Terrance Cobb	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	COB516549	A	\N	OAK
2130300	Davon	Coleman	Davon Coleman	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	COL238405	A	\N	TB
1824824	Jalen	Collins	Jalen Collins	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824824.png	DB	23	COL604473	A	32	ATL
2130347	Kain	Colter	Kain Colter	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COL784431	A	\N	MIN
1864646	Kevin	Cone	Kevin Cone	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	CON128909	A	\N	MIA
1880880	Brandin	Cooks	Brandin Cooks	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1880880.png	WR	22	COO299382	A	10	NO
1752358	J.C.	Copeland	J.C. Copeland	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	COP357445	A	\N	NYJ
2130348	Rakim	Cox	Rakim Cox	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	COX600242	A	77	CAR
408961	Antonio	Cromartie	Antonio Cromartie	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/408961.png	DB	31	CRO161100	A	31	NYJ
1684073	Victor	Cruz	Victor Cruz	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1684073.png	WR	29	CRU827288	IR	80	NYG
559359	Brian	Cushing	Brian Cushing	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559359.png	LB	29	CUS363261	A	56	HOU
1888751	DaVaris	Daniels	DaVaris Daniels	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	DAN529377	A	\N	NE
2175052	Erick	Dargan	Erick Dargan	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	DAR323358	A	\N	CIN
1762125	Chris	Davis	Chris Davis	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	DAV233017	A	43	SF
1630220	Andre	Ellington	Andre Ellington	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630220.png	RB	27	ELL164917	A	38	ARI
502303	Chris	Clemons	Chris Clemons	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/502303.png	DB	30	CLE388197	A	29	ARI
1632091	Randall	Cobb	Randall Cobb	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632091.png	WR	25	COB365535	A	18	GB
1117418	Nate	Collins	Nate Collins	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	COL653265	A	\N	CHI
1824756	Chris	Conley	Chris Conley	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824756.png	WR	23	CON236839	A	17	KC
2057741	Marcus	Cooper	Marcus Cooper	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057741.png	DB	26	COO646420	A	31	KC
2174875	Adrian	Coxson	Adrian Coxson	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	COX813852	A	\N	GB
395940	Chris	Crocker	Chris Crocker	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	CRO017900	A	\N	MIN
1983550	Yannik	Cudjoe-Virgil	Yannik Cudjoe-Virgil	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	CUD181942	A	40	TEN
409102	Jay	Cutler	Jay Cutler	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/409102.png	QB	32	CUT288111	A	6	CHI
2057736	B.J.	Daniels	B.J. Daniels	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057736.png	WR	26	DAN454194	A	2	HOU
1632200	Marcell	Dareus	Marcell Dareus	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632200.png	DL	26	DAR319542	A	99	BUF
2060064	Ja'Gared	Davis	Ja'Gared Davis	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	DAV419590	A	\N	KC
559806	Jim	Dray	Jim Dray	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559806.png	TE	29	DRA632509	A	\N	BUF
2175113	SaQwan	Edwards	SaQwan Edwards	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	EDW656739	A	30	OAK
1244464	Rhett	Ellison	Rhett Ellison	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244464.png	TE	27	ELL717708	IR	85	MIN
2129594	Quincy	Enunwa	Quincy Enunwa	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129594.png	WR	23	ENU444955	A	81	NYJ
1691182	Antone	Exum	Antone Exum	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691182.png	DB	25	EXU415291	IR	32	MIN
1749732	Cody	Fajardo	Cody Fajardo	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	FAJ059326	A	\N	OAK
2175062	Kaleb	Eulls	Kaleb Eulls	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	EUL385627	A	71	NO
2143086	T.J.	Fatinikun	T.J. Fatinikun	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	FAT296636	A	96	TB
1700545	David	Fales	David Fales	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700545.png	QB	25	FAL381917	A	12	CHI
2175099	Landon	Feichter	Landon Feichter	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FEI403290	A	\N	CLE
547043	Ryan	Fitzpatrick	Ryan Fitzpatrick	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/547043.png	QB	33	FIT792915	A	14	NYJ
1737102	Sharrif	Floyd	Sharrif Floyd	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737102.png	DL	24	FLO781132	A	73	MIN
1978518	Marcus	Forston	Marcus Forston	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	FOR564719	A	\N	STL
2174825	Andrew	Franks	Andrew Franks	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	23	FRA570164	A	3	MIA
1737456	Alani	Fua	Alani Fua	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	FUA016162	A	59	ARI
2135490	Andrew	Furney	Andrew Furney	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	FUR511098	A	\N	NYJ
1823812	Anthony	Gaitor	Anthony Gaitor	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GAI804192	A	\N	ARI
1693538	Ben	Garland	Ben Garland	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1693538.png	DL	28	GAR218169	A	\N	ATL
1273273	Clifton	Geathers	Clifton Geathers	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273273.png	DL	28	GEA408541	IR	96	PIT
1262941	Marcus	Gilchrist	Marcus Gilchrist	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1262941.png	DB	27	GIL211032	A	21	NYJ
1996128	Markus	Golden	Markus Golden	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996128.png	LB	25	GOL067489	A	44	ARI
1664260	Malliciah	Goodman	Malliciah Goodman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664260.png	DL	26	GOO462374	A	93	ATL
1781663	Tyrone	Crawford	Tyrone Crawford	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1781663.png	DL	26	CRA756298	A	98	DAL
2062181	Marcus	Cromartie	Marcus Cromartie	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062181.png	DB	25	CRO165702	A	47	SF
1635785	Vinny	Curry	Vinny Curry	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1635785.png	DL	27	CUR528238	A	75	PHI
565754	Chase	Daniel	Chase Daniel	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565754.png	QB	29	DAN230354	A	\N	PHI
2130677	Orleans	Darkwa	Orleans Darkwa	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130677.png	RB	24	DAR352254	A	26	NYG
1974167	Dominique	Davis	Dominique Davis	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	WIL000005	A	\N	IND
1766149	Kyle	Emanuel	Kyle Emanuel	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	EMA412803	A	51	SD
1824909	Mike	Evans	Mike Evans	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824909.png	WR	22	EVA534309	A	13	TB
1979376	Jean	Fanor	Jean Fanor	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FAN299931	A	\N	GB
1979377	Chandler	Fenner	Chandler Fenner	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FEN537220	A	\N	NYG
2175039	Isiah	Ferguson	Isiah Ferguson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	FER138429	IR	2	STL
2132692	Bojay	Filimoeatu	Bojay Filimoeatu	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FIL542920	A	\N	OAK
1633108	Darius	Fleming	Darius Fleming	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633108.png	LB	26	FLE172731	A	53	NE
502628	Bradley	Fletcher	Bradley Fletcher	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FLE563606	A	\N	NE
1852882	Trey	Flowers	Trey Flowers	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	FLO572739	A	98	NE
2133918	Kevin	Fogg	Kevin Fogg	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FOG223809	A	\N	PIT
410721	Nick	Folk	Nick Folk	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/410721.png	K	31	FOL378021	IR	2	NYJ
1114251	Jacoby	Ford	Jacoby Ford	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	FOR220896	A	\N	TEN
2130161	Bennie	Fowler	Bennie Fowler	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2130161.png	WR	24	FOW140514	A	16	DEN
2130350	Donte	Foster	Donte Foster	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	FOS208731	A	\N	DAL
1616730	Jerrell	Freeman	Jerrell Freeman	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616730.png	LB	29	FRE182967	A	\N	CHI
2059270	A.J.	Francis	A.J. Francis	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059270.png	DL	25	FRA068603	A	99	SEA
2175117	Isaac	Fruechte	Isaac Fruechte	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	FRU155733	A	15	MIN
2164712	Cameron	Fuller	Cameron Fuller	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FUL164818	A	\N	SF
1123282	Josh	Freeman	Josh Freeman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123282.png	QB	28	FRE183259	A	5	IND
2174784	Beau	Gardner	Beau Gardner	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	26	GAR154515	IR	\N	ATL
2130204	Shamiel	Gary	Shamiel Gary	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	GAR769446	A	27	MIA
1695497	Clyde	Gates	Clyde Gates	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GAT196555	A	\N	DAL
2059368	Ka'Lial	Glaud	Ka'Lial Glaud	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059368.png	LB	25	GLA723599	A	47	DAL
2129584	Demetri	Goodson	Demetri Goodson	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129584.png	DB	26	GOO571007	A	39	GB
411598	Bruce	Gradkowski	Bruce Gradkowski	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/411598.png	QB	33	GRA068015	IR	5	PIT
2130623	Alden	Darby	Alden Darby	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	DAR052577	A	\N	NO
1852857	Bruce	Ellington	Bruce Ellington	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1852857.png	WR	24	ELL164961	A	10	SF
1708343	Josh	Evans	Josh Evans	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1708343.png	DB	24	EVA426405	A	26	JAC
1724820	Nick	Fairley	Nick Fairley	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1724820.png	DL	28	FAI539049	A	\N	NO
1633106	Joseph	Fauria	Joseph Fauria	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	FAU446292	A	\N	NE
1664152	Anthony	Fera	Anthony Fera	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	FER003844	A	\N	FA
492934	Larry	Fitzgerald	Larry Fitzgerald	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/492934.png	WR	32	FIT437493	A	11	ARI
1725000	J.T.	Floyd	J.T. Floyd	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FLO740219	A	\N	FA
1674117	Moise	Fokou	Moise Fokou	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FOK474619	A	\N	CLE
2060082	Glenn	Foster	Glenn Foster	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	FOS326910	A	\N	NO
1975984	Jerry	Franklin	Jerry Franklin	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975984.png	LB	28	FRA465746	A	\N	DET
1272310	Jonathan	Freeny	Jonathan Freeny	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272310.png	LB	26	FRE425974	A	55	NE
2132670	Taylor	Gabriel	Taylor Gabriel	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132670.png	WR	25	GAB627060	A	18	CLE
2129624	Ben	Gardner	Ben Gardner	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129624.png	DL	24	GAR154652	A	\N	SD
396811	Antonio	Gates	Antonio Gates	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396811.png	TE	35	GAT194627	A	85	SD
492944	Robert	Geathers	Robert Geathers	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	GEA429541	A	\N	CIN
1691404	Mike	Gillislee	Mike Gillislee	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691404.png	RB	25	GIL667064	A	35	BUF
1979368	Brittan	Golden	Brittan Golden	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979368.png	WR	27	GOL049597	A	10	ARI
1762145	Antwan	Goodley	Antwan Goodley	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	GOO275616	A	\N	SEA
1675593	Danny	Gorrer	Danny Gorrer	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GOR775873	IR	\N	BAL
1691166	Jimmy	Graham	Jimmy Graham	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691166.png	TE	29	GRA207087	A	88	SEA
1680086	Ryan	Grant	Ryan Grant	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1680086.png	WR	25	GRA498062	A	14	WAS
1737770	Garrett	Grayson	Garrett Grayson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737770.png	QB	24	GRA824508	A	18	NO
411757	Chad	Greenway	Chad Greenway	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/411757.png	LB	33	GRE573953	A	52	MIN
396159	Rex	Grossman	Rex Grossman	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	GRO597298	A	\N	ATL
2174942	Jacob	Hagen	Jacob Hagen	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	HAG214970	A	\N	PIT
1700779	Cobi	Hamilton	Cobi Hamilton	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1700779.png	WR	25	HAM196632	A	\N	CAR
1682906	Clay	Harbor	Clay Harbor	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1682906.png	TE	28	HAR007683	A	\N	NE
2174878	Alonzo	Harris	Alonzo Harris	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAR337123	A	\N	BAL
2175064	R.J.	Harris	R.J. Harris	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	HAR521380	A	\N	NO
2175739	Tajh	Hasson	Tajh Hasson	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAS588886	A	\N	WAS
504721	Erin	Henderson	Erin Henderson	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/504721.png	LB	29	HEN060724	A	58	NYJ
1221719	Adam	Hayward	Adam Hayward	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1221719.png	LB	31	HAY776168	IR	55	WAS
2129593	IK	Enemkpali	IK Enemkpali	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129593.png	DL	24	ENE415291	A	75	BUF
2190610	Anthony	Ezeakunne	Anthony Ezeakunne	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	EZE043713	A	\N	CLE
2129679	Terrence	Fede	Terrence Fede	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129679.png	DL	24	FED152616	A	78	MIA
1992387	Tevita	Finau	Tevita Finau	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	FIN043163	A	\N	HOU
493614	Malcom	Floyd	Malcom Floyd	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493614.png	WR	34	FLO762180	A	80	SD
1679964	Michael	Ford	Michael Ford	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	FOR296028	A	\N	ATL
2169763	Josh	Francis	Josh Francis	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FRA161491	A	\N	GB
1117728	Sione	Fua	Sione Fua	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	FUA043162	A	\N	DEN
2130267	Jimmy	Gaines	Jimmy Gaines	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GAI360344	A	\N	BUF
2059358	Kendall	Gaskins	Kendall Gaskins	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059358.png	RB	25	GAS217909	A	30	SF
1707637	Chris	Givens	Chris Givens	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1707637.png	WR	26	GIV226674	A	\N	PHI
1672771	Najee	Goode	Najee Goode	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1672771.png	LB	26	GOO217526	A	\N	PHI
553121	Robbie	Gould	Robbie Gould	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/553121.png	K	34	GOU258729	A	9	CHI
1633112	Jonas	Gray	Jonas Gray	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633112.png	RB	25	GRA699244	A	34	JAC
2073825	Justin	Green	Justin Green	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GRE297630	A	\N	NE
2132897	Kevin	Greene	Kevin Greene	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	25	GRE481323	A	\N	TEN
1130615	Virgil	Green	Virgil Green	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1130615.png	TE	27	GRE378950	A	85	DEN
1620788	Robert	Griffin III	Robert Griffin III	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620788.png	QB	26	GRI283140	A	\N	CLE
1979730	Jonathan	Grimes	Jonathan Grimes	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979730.png	RB	26	GRI692120	A	41	HOU
2186229	Justin	Halley	Justin Halley	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAL640921	A	\N	NYG
2175044	Jacoby	Hale	Jacoby Hale	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAL055450	A	\N	OAK
1272731	Chandler	Harnish	Chandler Harnish	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	HAR235589	A	\N	ARI
1252130	DuJuan	Harris	DuJuan Harris	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1252130.png	RB	27	HAR370058	A	32	SF
1630429	Montel	Harris	Montel Harris	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAR492827	A	\N	FA
2060278	Travis	Harvey	Travis Harvey	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAR824696	A	\N	ARI
1803774	Andrew	Hawkins	Andrew Hawkins	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1803774.png	WR	30	HAW100447	IR	16	CLE
558845	Geno	Hayes	Geno Hayes	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAY163825	A	\N	JAC
2079780	Braylon	Heard	Braylon Heard	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HEA311945	A	\N	PIT
1272498	Roy	Helu	Roy Helu	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272498.png	RB	27	HEL697773	A	26	OAK
1703283	Brandon	Hepburn	Brandon Hepburn	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1703283.png	LB	26	HEP404791	A	\N	PHI
1697064	Justin	Ellis	Justin Ellis	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1697064.png	DL	25	ELL559046	A	78	OAK
2053740	Darren	Fells	Darren Fells	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2053740.png	TE	29	FEL484741	A	85	ARI
1737104	C.J.	Fiedorowicz	C.J. Fiedorowicz	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737104.png	TE	24	FIE077391	A	87	HOU
2130413	Timothy	Flanders	Timothy Flanders	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	FLA544670	A	\N	CLE
2130224	David	Fluellen	David Fluellen	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	FLU088852	A	31	TEN
1979669	Chase	Ford	Chase Ford	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979669.png	TE	25	FOR078468	IR	48	BAL
2129495	Khairi	Fortt	Khairi Fortt	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FOR728327	A	\N	JAC
2130427	Dan	Fox	Dan Fox	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FOX121620	A	\N	NYG
1620862	Terrence	Frederick	Terrence Frederick	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FRE054906	A	\N	NO
1971890	Isaiah	Frey	Isaiah Frey	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971890.png	DB	26	FRE773651	A	\N	PIT
1759440	Kyle	Fuller	Kyle Fuller	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759440.png	DB	24	FUL352996	A	23	CHI
558839	Graham	Gano	Graham Gano	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/558839.png	K	29	GAN308500	A	9	CAR
1760229	Jimmy	Garoppolo	Jimmy Garoppolo	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1760229.png	QB	24	GAR363225	A	10	NE
2062123	Linden	Gaydosh	Linden Gaydosh	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	GAY510348	A	\N	CAR
1114035	David	Gettis	David Gettis	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GET190022	A	\N	TB
431159	Wallace	Gilberry	Wallace Gilberry	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/431159.png	DL	31	GIL005973	A	\N	DET
1891877	David	Gilreath	David Gilreath	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GIL804953	A	\N	SEA
1630353	Mike	Glennon	Mike Glennon	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630353.png	QB	26	GLE711694	A	8	TB
1878007	Senquez	Golson	Senquez Golson	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	GOL688554	IR	27	PIT
559468	Josh	Gordy	Josh Gordy	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GOR410841	IR	\N	NYG
1221208	Corey	Graham	Corey Graham	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1221208.png	DB	30	GRA137223	A	20	BUF
1985839	Isaiah	Green	Isaiah Green	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GRE243423	A	\N	PIT
2001231	Shaq	Green-Thompson	Shaq Green-Thompson	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	21	GRE573058	A	54	CAR
1114698	Steve	Gregory	Steve Gregory	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GRE791295	A	\N	KC
2130777	Xavier	Grimble	Xavier Grimble	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	GRI662547	A	\N	PIT
2189223	Nick	Haag	Nick Haag	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAA138429	A	\N	IND
558843	Letroy	Guion	Letroy Guion	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/558843.png	DL	28	GUI723790	A	98	GB
1977149	Saalim	Hakim	Saalim Hakim	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAK319681	A	\N	KC
412717	Devin	Hester	Devin Hester	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/412717.png	WR	33	HES267217	A	17	ATL
2175410	Mylan	Hicks	Mylan Hicks	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HIC621690	A	\N	SF
2186082	Bronson	Hill	Bronson Hill	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	HIL102508	A	\N	CIN
1679651	Stephen	Hill	Stephen Hill	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1679651.png	WR	24	HIL590866	IR	87	CAR
2129635	Zach	Hocker	Zach Hocker	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129635.png	K	24	HOC440041	A	\N	CIN
2174910	Gabe	Holmes	Gabe Holmes	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	25	HOL550382	A	\N	OAK
1664151	Dustin	Hopkins	Dustin Hopkins	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664151.png	K	25	HOP165345	A	3	WAS
1620558	Justin	Houston	Justin Houston	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620558.png	LB	27	HOU476949	A	50	KC
1249642	Jerome	Felton	Jerome Felton	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1249642.png	RB	29	FEL642097	A	42	BUF
2130875	Carlos	Fields	Carlos Fields	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	FIE471195	A	59	WAS
1244492	Coby	Fleener	Coby Fleener	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244492.png	TE	27	FLE075352	A	\N	NO
2129582	Marquis	Flowers	Marquis Flowers	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129582.png	LB	24	FLO457902	IR	53	CIN
410711	Matt	Flynn	Matt Flynn	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/410711.png	QB	30	FLY487098	A	3	NO
518611	Arian	Foster	Arian Foster	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518611.png	RB	29	FOS107404	IR	\N	HOU
1691451	Dee	Ford	Dee Ford	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691451.png	LB	25	FOR113166	A	55	KC
2175411	Deshon	Foxx	Deshon Foxx	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	FOX816440	A	\N	SEA
1824135	Devonta	Freeman	Devonta Freeman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824135.png	RB	24	FRE174508	A	24	ATL
2175143	Eric	Frohnapfel	Eric Frohnapfel	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	FRO193116	A	\N	SD
583087	Pierre	Garcon	Pierre Garcon	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/583087.png	WR	29	GAR115573	A	88	WAS
2059363	Terence	Garvin	Terence Garvin	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059363.png	LB	25	GAR734578	A	\N	WAS
1691183	James	Gayle	James Gayle	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	GAY640464	IR	\N	WAS
1243072	Brandon	Ghee	Brandon Ghee	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GHE250267	A	\N	CIN
2136090	Garrett	Gilbert	Garrett Gilbert	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	24	GIL072219	A	\N	OAK
502737	Ted	Ginn	Ted Ginn	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/502737.png	WR	30	GIN403290	A	19	CAR
2130094	Tay	Glover-Wright	Tay Glover-Wright	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	GLO715400	A	39	IND
411568	Frank	Gore	Frank Gore	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/411568.png	RB	32	GOR411171	A	23	IND
565190	Garrett	Graham	Garrett Graham	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565190.png	TE	29	GRA193306	A	88	HOU
1971891	Jonte	Green	Jonte Green	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GRE291586	A	\N	ARI
2174849	Deontay	Greenberry	Deontay Greenberry	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GRE402125	A	\N	SEA
1123667	Jermaine	Gresham	Jermaine Gresham	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123667.png	TE	27	GRE819895	A	\N	ARI
1111117	Brent	Grimes	Brent Grimes	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1111117.png	DB	32	GRI674817	A	\N	TB
1983587	LaDarius	Gunter	LaDarius Gunter	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	GUN709450	A	36	GB
1851216	Chris	Hackett	Chris Hackett	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	HAC494322	A	\N	OAK
2129728	Andre	Hal	Andre Hal	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129728.png	DB	23	HAL004143	A	29	HOU
412010	Leon	Hall	Leon Hall	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/412010.png	DB	31	HAL432931	A	29	CIN
1892593	Rannell	Hall	Rannell Hall	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	HAL477156	A	85	CLE
2174850	Ray	Hamilton	Ray Hamilton	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HAM301278	IR	\N	WAS
1759559	Johnathan	Hankins	Johnathan Hankins	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759559.png	DL	24	HAN169902	IR	95	NYG
1620463	Chris	Harper	Chris Harper	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAR246346	A	\N	NYG
1977111	Bobby	Felder	Bobby Felder	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FEL097654	IR	\N	BUF
524093	Tony	Fiammetta	Tony Fiammetta	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	FIA415290	A	\N	CHI
1250697	Joe	Flacco	Joe Flacco	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1250697.png	QB	31	FLA009602	IR	5	BAL
1691648	Dane	Fletcher	Dane Fletcher	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691648.png	LB	29	FLE595388	A	52	NE
2174783	Terrell	Floyd	Terrell Floyd	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FLO782780	A	\N	ATL
302022	Larry	Foote	Larry Foote	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	FOO434041	A	\N	ARI
1976201	L.J.	Fort	L.J. Fort	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1976201.png	LB	26	FOR595356	A	54	PIT
2173899	Dante	Fowler	Dante Fowler	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	21	FOW382785	IR	56	JAC
1123662	Dominique	Franks	Dominique Franks	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FRA586643	A	\N	BAL
2129625	Nate	Freese	Nate Freese	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	FRE436108	A	\N	DET
1763065	Corey	Fuller	Corey Fuller	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1763065.png	WR	25	FUL171366	A	10	DET
1130539	Junior	Galette	Junior Galette	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1130539.png	LB	28	GAL199835	IR	58	WAS
2175058	Manasseh	Garner	Manasseh Garner	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	GAR305827	A	\N	DEN
2062356	Jordan	Gay	Jordan Gay	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062356.png	K	26	GAY293234	A	\N	BUF
2129525	Jeremiah	George	Jeremiah George	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129525.png	LB	24	GEO278961	A	52	TB
1752548	Crockett	Gillmore	Crockett Gillmore	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1752548.png	TE	24	GIL677592	A	80	BAL
2174953	Andrew	Gleichert	Andrew Gleichert	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	GLE084286	A	\N	PHI
1998182	Eddie	Goldman	Eddie Goldman	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	GOL216018	IR	91	CHI
1128771	Mike	Goodson	Mike Goodson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	GOO579925	A	\N	NYJ
1632236	Chris	Gragg	Chris Gragg	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632236.png	TE	25	GRA108585	A	89	BUF
1630354	T.J.	Graham	T.J. Graham	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GRA263708	A	\N	NO
1673207	A.J.	Green	A.J. Green	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1673207.png	WR	27	GRE034604	A	18	CIN
1971884	Chris	Greenwood	Chris Greenwood	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GRE578129	A	\N	NE
2058361	Ryan	Griffin	Ryan Griffin	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	26	GRI294351	A	4	TB
411852	Quentin	Groves	Quentin Groves	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GRO760958	A	\N	BUF
1632093	Winston	Guy	Winston Guy	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632093.png	DB	25	GUY486616	A	27	IND
1762147	Bryce	Hager	Bryce Hager	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	HAG352499	A	54	STL
492951	DeAngelo	Hall	DeAngelo Hall	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/492951.png	DB	32	HAL268837	A	23	WAS
1689628	Dominique	Hamilton	Dominique Hamilton	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HAM218436	A	\N	NYG
1737353	Victor	Hampton	Victor Hampton	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAM784439	A	\N	BAL
2061036	Marcus	Hardison	Marcus Hardison	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	HAR063200	A	91	CIN
2000059	Eli	Harold	Eli Harold	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	HAR236127	A	58	SF
2174921	Chris	Harper	Chris Harper	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	22	HAR246884	A	\N	NE
1865418	Anthony	Harris	Anthony Harris	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	HAR339211	A	41	MIN
2219916	Erik	Harris	Erik Harris	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	26	HAR385656	A	\N	NO
433568	Daniel	Fells	Daniel Fells	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/433568.png	TE	32	FEL481366	A	85	NYG
395957	Drayton	Florence	Drayton Florence	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FLO150404	A	\N	DET
1117835	Kai	Forbath	Kai Forbath	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117835.png	K	28	FOR007550	A	5	NO
559687	Matt	Forte	Matt Forte	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559687.png	RB	30	FOR645404	A	\N	NYJ
1737130	Jalston	Fowler	Jalston Fowler	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737130.png	RB	25	FOW442542	A	45	TEN
2174941	Terrence	Franks	Terrence Franks	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	FRA604770	A	\N	STL
1245850	Drew	Frey	Drew Frey	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	FRE725538	A	\N	FA
1620863	Jeff	Fuller	Jeff Fuller	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620863.png	WR	25	FUL257424	A	\N	SEA
2129680	Jeremy	Gallon	Jeremy Gallon	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GAL653898	A	\N	OAK
2061073	Tyler	Gaffney	Tyler Gaffney	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2061073.png	RB	24	GAF629685	IR	\N	NE
2175040	Montell	Garner	Montell Garner	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	GAR306356	A	\N	PIT
436527	William	Gay	William Gay	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/436527.png	DB	31	GAY400290	A	22	PIT
1116328	Brandon	Gibson	Brandon Gibson	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116328.png	WR	28	GIB479862	IR	13	NE
1117729	Toby	Gerhart	Toby Gerhart	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117729.png	RB	29	GER353982	A	21	JAC
1664166	Stephon	Gilmore	Stephon Gilmore	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664166.png	DB	25	GIL801084	IR	24	BUF
1999909	Jacoby	Glenn	Jacoby Glenn	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	GLE220216	A	\N	CHI
504810	Dashon	Goldson	Dashon Goldson	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/504810.png	DB	31	GOL350563	A	38	WAS
2133167	C.J.	Goodwin	C.J. Goodwin	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2133167.png	WR	26	GOO627559	A	\N	ATL
2130212	Thomas	Gordon	Thomas Gordon	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	GOR388204	A	\N	NYG
1116725	Brandon	Graham	Brandon Graham	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116725.png	LB	28	GRA130064	A	55	PHI
1691536	Alex	Green	Alex Green	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	GRE037487	A	\N	NYJ
1616291	BenJarvus	Green-Ellis	BenJarvus Green-Ellis	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	GRE532094	A	\N	CIN
2057695	Ryan	Griffin	Ryan Griffin	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057695.png	TE	26	GRI293499	A	84	HOU
2060630	Randy	Gregory	Randy Gregory	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060630.png	DL	23	GRE789590	A	94	DAL
1679925	Obum	Gwacham	Obum Gwacham	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	GWA118654	A	58	NO
2220223	Aaron	Grymes	Aaron Grymes	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	GRY542472	A	\N	PHI
2175088	Jarred	Haggins	Jarred Haggins	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAG593034	A	\N	DET
2174909	Jimmy	Hall	Jimmy Hall	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	HAL355824	A	43	OAK
2060092	Jakar	Hamilton	Jakar Hamilton	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HAM230188	A	\N	DAL
1616740	Caleb	Hanie	Caleb Hanie	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	HAN149018	A	\N	DAL
2175133	Sage	Harold	Sage Harold	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAR236933	A	\N	WAS
1630457	Brandon	Harris	Brandon Harris	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630457.png	DB	26	HAR348139	IR	32	TEN
2057696	Jeremy	Harris	Jeremy Harris	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057696.png	DB	24	HAR440596	A	37	WAS
1977116	Damon	Harrison	Damon Harrison	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977116.png	DL	27	HAR576467	A	98	NYG
1689522	Demetrius	Hartsfield	Demetrius Hartsfield	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAR746426	A	\N	FA
1633109	Michael	Floyd	Michael Floyd	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633109.png	WR	26	FLO764240	A	15	ARI
1631750	Nick	Foles	Nick Foles	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631750.png	QB	27	FOL058566	A	5	STL
520386	Justin	Forsett	Justin Forsett	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/520386.png	RB	30	FOR530271	IR	29	BAL
1273119	Mason	Foster	Mason Foster	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273119.png	LB	27	FOS493155	A	54	WAS
2174920	Devin	Gardner	Devin Gardner	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GAR168463	A	\N	PIT
2130278	Bruce	Gaston	Bruce Gaston	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	GAS671214	A	76	CHI
1762586	Kwame	Geathers	Kwame Geathers	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	GEA422907	A	\N	CIN
2063143	Alex	Gillett	Alex Gillett	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	GIL443348	A	\N	GB
2130817	Adarius	Glanton	Adarius Glanton	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	GLA303597	A	53	TB
1975971	Robert	Golden	Robert Golden	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975971.png	DB	25	GOL069318	A	21	PIT
1679504	Zaviar	Gooden	Zaviar Gooden	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	GOO270260	IR	\N	TEN
289074	Shayne	Graham	Shayne Graham	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/289074.png	K	38	GRA217171	A	6	ATL
1689564	Dwayne	Gratz	Dwayne Gratz	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1689564.png	DB	26	GRA546508	A	27	JAC
1631427	Ladarius	Green	Ladarius Green	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631427.png	TE	25	GRE298482	IR	\N	PIT
1724998	Khaseem	Greene	Khaseem Greene	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1724998.png	LB	27	GRE491619	A	\N	DET
1244469	Everson	Griffen	Everson Griffen	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244469.png	DL	28	GRI051627	A	97	MIN
1689563	Cory	Grissom	Cory Grissom	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	GRI825685	A	\N	MIA
2000877	Todd	Gurley	Todd Gurley	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2000877.png	RB	21	GUR000000	A	30	STL
2130414	Spencer	Hadley	Spencer Hadley	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAD698900	A	\N	OAK
412000	Tamba	Hali	Tamba Hali	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/412000.png	LB	32	HAL131362	A	91	KC
1749652	Connor	Halliday	Connor Halliday	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	HAL641981	A	\N	WAS
2130415	Je'Ron	Hamm	Je'Ron Hamm	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	HAM420043	A	83	SF
2175100	Kevin	Haplea	Kevin Haplea	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HAP547429	A	\N	CLE
2174997	Trevor	Harman	Trevor Harman	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HAR186373	A	\N	ARI
2175119	Kenneth	Harper	Kenneth Harper	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HAR268399	A	\N	NYG
2130269	Bryan	Johnson	Bryan Johnson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JOH084754	A	\N	NYJ
1760290	David	Johnson	David Johnson	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1760290.png	RB	24	JOH174477	A	31	ARI
2009218	Gus	Johnson	Gus Johnson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	22	JOH292540	A	\N	ATL
2130683	Kamal	Johnson	Kamal Johnson	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	JOH398786	A	\N	WAS
1691419	Nico	Johnson	Nico Johnson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691419.png	LB	25	JOH553177	A	48	NYG
2175074	Toby	Johnson	Toby Johnson	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	JOH690098	A	\N	MIN
1664267	Abry	Jones	Abry Jones	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664267.png	DL	24	JON026672	A	95	JAC
1631308	Chris	Jones	Chris Jones	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631308.png	DL	25	JON162611	A	94	NE
2057734	Don	Jones	Don Jones	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057734.png	DB	25	JON236068	A	35	CLE
2059194	Paul	Hazel	Paul Hazel	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HAZ159741	A	\N	NYG
2175145	Brock	Hekking	Brock Hekking	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	HEK428791	IR	59	SD
1766015	Robert	Herron	Robert Herron	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1766015.png	WR	23	HER722941	A	\N	MIA
1679965	Akiem	Hicks	Akiem Hicks	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679965.png	DL	26	HIC340209	A	\N	CHI
2188909	Alfy	Hill	Alfy Hill	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HIL072671	A	\N	PHI
1661886	Sammie Lee	Hill	Sammie Lee Hill	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1661886.png	DL	29	HIL580262	A	94	TEN
1673733	T.Y.	Hilton	T.Y. Hilton	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1673733.png	WR	26	HIL775449	A	13	IND
1664197	DeVonte	Holloman	DeVonte Holloman	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HOL293630	A	\N	DAL
1737078	DeAndre	Hopkins	DeAndre Hopkins	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737078.png	WR	23	HOP161500	A	10	HOU
1664683	Demontre	Hurst	Demontre Hurst	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664683.png	DB	25	HUR492348	A	30	CHI
1752580	Joey	Iosefa	Joey Iosefa	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1752580.png	RB	24	IOS177981	A	47	NE
1325808	Josh	Johnson	Josh Johnson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1325808.png	QB	29	JOH000001	A	8	BUF
584099	Michael	Johnson	Michael Johnson	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/584099.png	DL	29	JOH000003	A	90	CIN
1737190	Storm	Johnson	Storm Johnson	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	JOH676033	A	\N	JAC
1975914	Brandon	Joiner	Brandon Joiner	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JOI196395	A	\N	CIN
1971327	Chandler	Jones	Chandler Jones	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971327.png	DL	26	JON157557	A	\N	ARI
1977170	Dominique	Jones	Dominique Jones	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	28	JON235920	A	\N	MIA
2130208	Justin	Jones	Justin Jones	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	JON518491	A	\N	NE
1868398	Taiwan	Jones	Taiwan Jones	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	JON731462	A	\N	NYJ
518581	Johnathan	Joseph	Johnathan Joseph	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518581.png	DB	31	JOS218478	A	24	HOU
1265317	Linval	Joseph	Linval Joseph	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1265317.png	DL	27	JOS231117	A	98	MIN
565116	Mike	Kafka	Mike Kafka	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	KAF629229	IR	\N	CIN
2130228	Wade	Keliikipi	Wade Keliikipi	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	KEL023993	A	\N	PHI
2169687	Jarryd	Hayne	Jarryd Hayne	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	28	HAY563587	A	\N	SF
1824277	Ben	Heeney	Ben Heeney	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824277.png	LB	23	HEE290361	A	51	OAK
2174826	Neville	Hewitt	Neville Hewitt	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	HEW203041	A	46	MIA
1852886	Alonzo	Highsmith	Alonzo Highsmith	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HIG605342	A	\N	WAS
1824828	Kenny	Hilliard	Kenny Hilliard	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824828.png	RB	24	HIL719831	A	38	HOU
2174879	Lavon	Hooks	Lavon Hooks	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	HOO430120	A	\N	PIT
1737231	Mike	Hull	Mike Hull	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	HUL213269	A	45	MIA
1975964	David	Hunter	David Hunter	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HUN534304	A	\N	NO
1243179	Henry	Hynoski	Henry Hynoski	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HYN548472	A	\N	NYG
1623793	Mark	Ingram	Mark Ingram	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1623793.png	RB	26	ING656964	IR	22	NO
559554	DeSean	Jackson	DeSean Jackson	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559554.png	WR	29	JAC127681	A	11	WAS
1620532	Jaye	Howard	Jaye Howard	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620532.png	DL	27	HOW219505	A	96	KC
2057732	Kemal	Ishmael	Kemal Ishmael	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057732.png	DB	24	ISH415291	A	36	ATL
1759355	Kevin	Johnson	Kevin Johnson	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759355.png	DB	23	JOH000010	A	30	HOU
2129623	Randell	Johnson	Randell Johnson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129623.png	LB	25	JOH588257	A	58	BUF
2176386	Tony	Johnson	Tony Johnson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JOH693690	A	\N	NYG
1893141	Christion	Jones	Christion Jones	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	JON162440	A	\N	MIA
2130612	Howard	Jones	Howard Jones	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	26	JON402377	A	95	TB
2000854	Matt	Jones	Matt Jones	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2000854.png	RB	23	JON603219	A	31	WAS
1226231	Akeem	Jordan	Akeem Jordan	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JOR014362	IR	\N	WAS
2058542	Vernon	Kearney	Vernon Kearney	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	KEA388035	A	\N	OAK
1737773	Eric	Kendricks	Eric Kendricks	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737773.png	LB	24	KEN101224	A	54	MIN
2174980	Josh	Keyes	Josh Keyes	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	KEY195824	A	57	TB
414639	Mathias	Kiwanuka	Mathias Kiwanuka	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	KIW059326	IR	\N	NYG
553458	John	Kuhn	John Kuhn	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/553458.png	RB	33	KUH609436	A	30	GB
1723147	Jordan	Kovacs	Jordan Kovacs	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1723147.png	DB	25	KOV243683	A	\N	KC
2013049	Josh	Lambo	Josh Lambo	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	25	LAM580646	A	2	SD
1252360	Kendall	Langford	Kendall Langford	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1252360.png	DL	30	LAN553691	A	90	IND
1737318	Corey	Lemonier	Corey Lemonier	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737318.png	LB	24	LEM719640	A	96	SF
192741	Mike	Leach	Mike Leach	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	39	LEA091822	A	82	ARI
1741676	Anthony	Levine	Anthony Levine	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1741676.png	DB	29	LEV588156	A	41	BAL
563624	James	Laurinaitis	James Laurinaitis	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563624.png	LB	29	LAU629310	A	\N	NO
1243803	Dan	Herron	Dan Herron	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243803.png	RB	27	HER657540	A	36	IND
412694	Will	Herring	Will Herring	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	HER566051	A	\N	STL
564881	Darrius	Heyward-Bey	Darrius Heyward-Bey	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564881.png	WR	29	HEY279111	A	88	PIT
1673258	Dont'a	Hightower	Dont'a Hightower	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1673258.png	LB	26	HIG766208	A	54	NE
2175429	Keshawn	Hill	Keshawn Hill	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HIL476445	A	\N	KC
523458	Peyton	Hillis	Peyton Hillis	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	HIL734134	IR	\N	NYG
558554	Trindon	Holliday	Trindon Holliday	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	HOL241722	A	\N	OAK
1115114	Michael	Hoomanawanui	Michael Hoomanawanui	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	27	HOO548774	A	84	NO
1691186	Jayron	Hosley	Jayron Hosley	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691186.png	DB	25	HOS233600	A	28	NYG
1631911	Delano	Howell	Delano Howell	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	HOW538133	A	\N	IND
2197503	Kendrick	Ings	Kendrick Ings	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ING700275	A	\N	DET
1272286	Chris	Ivory	Chris Ivory	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272286.png	RB	28	IVO253611	A	\N	JAC
1971879	Matt	Johnson	Matt Johnson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JOH501139	A	\N	DAL
1116922	Steve	Johnson	Steve Johnson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116922.png	WR	29	JOH673533	A	11	SD
2174866	Vernon	Johnson	Vernon Johnson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JOH724907	A	\N	DET
1763472	Byron	Jones	Byron Jones	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1763472.png	DB	23	JON141600	A	31	DAL
1631952	Datone	Jones	Datone Jones	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631952.png	DL	25	JON213438	A	95	GB
520951	Jason	Jones	Jason Jones	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/520951.png	DL	29	JON448884	A	91	DET
2130417	Seantavius	Jones	Seantavius Jones	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JON717404	A	\N	PHI
1631835	Dion	Jordan	Dion Jordan	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631835.png	DL	26	JOR165083	A	95	MIA
1631836	Josh	Kaddu	Josh Kaddu	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KAD161787	A	\N	MIN
1244499	Thomas	Keiser	Thomas Keiser	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KEI386830	A	\N	ARI
1631805	Mychal	Kendricks	Mychal Kendricks	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631805.png	LB	25	KEN104336	A	95	PHI
2132677	Gregory	Hickman	Gregory Hickman	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	HIC266969	A	\N	MIN
1891895	Mike	Higgins	Mike Higgins	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	HIG255263	A	\N	MIN
1277423	Tim	Hightower	Tim Hightower	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1277423.png	RB	29	HIG784208	A	\N	NO
1975972	Ikponmwosa	Igbinosun	Ikponmwosa Igbinosun	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	IGB296636	A	\N	JAC
1272256	Dontrelle	Inman	Dontrelle Inman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272256.png	WR	27	INM264861	A	15	SD
1128770	Jerrod	Johnson	Jerrod Johnson	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1128770.png	QB	27	JOH344163	A	\N	BAL
2185524	Michael	Johnson	Michael Johnson	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JOH522043	A	\N	PHI
1692698	Steven	Johnson	Steven Johnson	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1692698.png	LB	28	JOH674515	A	\N	PIT
1963572	Will	Johnson	Will Johnson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1963572.png	RB	27	JOH750031	A	\N	NYG
2130150	Chandler	Jones	Chandler Jones	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JON157260	A	\N	TB
1700783	DeQuinta	Jones	DeQuinta Jones	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	JON233391	A	\N	FA
1623794	Julio	Jones	Julio Jones	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1623794.png	WR	27	JON640315	A	11	ATL
1737352	T.J.	Jones	T.J. Jones	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737352.png	WR	23	JON731255	A	13	DET
2130338	Reggie	Jordan	Reggie Jordan	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	JOR534156	IR	\N	JAC
1130583	Colin	Kaepernick	Colin Kaepernick	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1130583.png	QB	28	KAE371576	IR	7	SF
1725130	Travis	Kelce	Travis Kelce	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1725130.png	TE	26	KEL012458	A	87	KC
2008672	Devon	Kennard	Devon Kennard	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2008672.png	LB	24	KEN208338	IR	59	NYG
2058556	Jeremy	Kimbrough	Jeremy Kimbrough	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KIM478612	A	\N	WAS
2174817	Andrae	Kirk	Andrae Kirk	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KIR253552	A	\N	ARI
2191633	Dan	Light	Dan Light	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	LIG423549	A	\N	DEN
1620606	Melvin	Ingram	Melvin Ingram	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620606.png	LB	26	ING667979	A	54	SD
2174190	Malcolm	Johnson	Malcolm Johnson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174190.png	TE	23	JOH486021	A	42	CLE
556927	Tyrell	Johnson	Tyrell Johnson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JOH488871	A	\N	ATL
1765939	DaQuan	Jones	DaQuan Jones	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1765939.png	DL	24	JON200565	A	90	TEN
1114935	Reshad	Jones	Reshad Jones	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114935.png	DB	28	JON653665	A	20	MIA
1272992	Cameron	Jordan	Cameron Jordan	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272992.png	DL	26	JOR051224	A	94	NO
302066	Brett	Keisel	Brett Keisel	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	KEI384586	IR	\N	PIT
1763150	Hau'oli	Kikaha	Hau'oli Kikaha	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1763150.png	LB	23	KIK059326	A	44	NO
559908	Terrance	Knighton	Terrance Knighton	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559908.png	DL	29	KNI770772	A	\N	NE
558591	Brandon	LaFell	Brandon LaFell	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/558591.png	WR	29	LAF184731	A	\N	CIN
2130684	Ryan	Lankford	Ryan Lankford	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LAN674148	A	\N	IND
1664424	Cameron	Lawrence	Cameron Lawrence	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664424.png	LB	25	LAW268562	A	\N	DAL
2060060	Josh	Lenz	Josh Lenz	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060060.png	WR	25	LEN801846	A	\N	HOU
1975956	Lance	Lewis	Lance Lewis	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LEW369851	A	\N	NO
1762314	Tony	Lippett	Tony Lippett	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1762314.png	WR	23	LIP247808	A	36	MIA
1681620	Ricardo	Lockette	Ricardo Lockette	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1681620.png	WR	29	LOC426540	IR	83	SEA
2132975	Al	Louis-Jean	Al Louis-Jean	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	LOU680177	A	\N	WAS
1825187	Aaron	Lynch	Aaron Lynch	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825187.png	LB	23	LYN062058	A	59	SF
1767384	Craig	Mager	Craig Mager	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MAG263579	A	29	SD
1672655	EJ	Manuel	EJ Manuel	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1672655.png	QB	26	MAN738705	A	3	BUF
415972	Richard	Marshall	Richard Marshall	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAR452561	A	\N	SD
1684466	Jonathan	Massaquoi	Jonathan Massaquoi	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1684466.png	DL	27	MAS316324	A	\N	KC
1851278	Lorenzo	Mauldin	Lorenzo Mauldin	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851278.png	LB	23	MAU464116	A	55	NYJ
1825152	Bobby	McCain	Bobby McCain	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825152.png	DB	22	MCC050868	A	28	MIA
563691	Devin	McCourty	Devin McCourty	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563691.png	DB	28	MCC600258	A	32	NE
1674132	Clinton	McDonald	Clinton McDonald	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674132.png	DL	29	MCD242013	IR	98	TB
395998	Jarret	Johnson	Jarret Johnson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	JOH338168	A	\N	SD
2058176	Shelton	Johnson	Shelton Johnson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JOH669962	A	\N	TB
2059218	Brandon	Jones	Brandon Jones	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JON101217	A	\N	KC
1697215	Jarvis	Jones	Jarvis Jones	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1697215.png	LB	26	JON448832	A	95	PIT
2174761	Tony	Jones	Tony Jones	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	JON765721	A	\N	WAS
1737136	Lamarcus	Joyner	Lamarcus Joyner	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737136.png	DB	25	JOY464866	A	20	STL
1137118	Case	Keenum	Case Keenum	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1137118.png	QB	28	KEE690598	A	17	STL
2130863	Zach	Kerr	Zach Kerr	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130863.png	DL	25	KER593782	A	94	IND
2174902	Nigel	King	Nigel King	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	KIN586351	A	\N	SF
1115776	Karl	Klug	Karl Klug	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1115776.png	DL	28	KLU289611	A	97	TEN
1272223	Markus	Kuhn	Markus Kuhn	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272223.png	DL	29	KUH617029	IR	\N	NE
2174818	Paul	Lasike	Paul Lasike	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	25	LAS230970	A	\N	CHI
1979717	Emmanuel	Lamur	Emmanuel Lamur	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979717.png	LB	26	LAM793930	IR	\N	MIN
1851123	Marqise	Lee	Marqise Lee	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851123.png	WR	24	LEE466282	A	11	JAC
1632211	Robert	Lester	Robert Lester	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LES696435	A	\N	CAR
1273908	Ryan	Lindley	Ryan Lindley	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	LIN208832	A	\N	IND
1117861	Jake	Locker	Jake Locker	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	LOC066933	IR	\N	TEN
1889936	John	Lotulelei	John Lotulelei	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1889936.png	LB	24	LOT643596	A	55	OAK
2174916	Gavin	Lutman	Gavin Lutman	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LUT354079	IR	\N	TEN
1616767	Elbert	Mack	Elbert Mack	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAC290385	A	\N	HOU
2174787	Derrick	Malone	Derrick Malone	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MAL652285	A	\N	ATL
1633497	Marc	Mariani	Marc Mariani	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633497.png	WR	28	MAR136781	A	80	CHI
1691361	Cameron	Marshall	Cameron Marshall	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691361.png	RB	24	MAR374915	A	\N	SEA
2060078	Josh	Martin	Josh Martin	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060078.png	LB	24	MAR661981	A	95	NYJ
2167522	Danny	Mason	Danny Mason	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	MAS070172	A	\N	CHI
2079732	Vince	Mayle	Vince Mayle	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2079732.png	WR	24	MAY564719	A	\N	DAL
1892110	Joey	Mbu	Joey Mbu	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	MBU029662	A	74	ATL
1691424	AJ	McCarron	AJ McCarron	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691424.png	QB	25	MCC176481	A	5	CIN
1274370	Shea	McClellin	Shea McClellin	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1274370.png	LB	26	MCC384106	A	\N	NE
584648	Colt	McCoy	Colt McCoy	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/584648.png	QB	29	MCC603149	A	16	WAS
1631780	Keelan	Johnson	Keelan Johnson	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JOH398790	A	\N	SEA
1631101	Orhian	Johnson	Orhian Johnson	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	JOH567387	A	\N	ARI
1109438	Tom	Johnson	Tom Johnson	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1109438.png	DL	31	JOH692182	A	92	MIN
494520	Tommy	Kelly	Tommy Kelly	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	KEL742155	A	\N	ARI
1273689	Jeremy	Kerley	Jeremy Kerley	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273689.png	WR	27	KER321479	A	\N	DET
1137501	Shiloh	Keo	Shiloh Keo	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1137501.png	DB	28	KEO029662	A	33	DEN
2174923	Brandon	King	Brandon King	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	KIN317519	A	36	NE
1619921	David	King	David King	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619921.png	DL	26	KIN371626	A	93	KC
1759547	Chris	Kirksey	Chris Kirksey	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759547.png	LB	23	KIR636906	A	58	CLE
1979416	Ishmaa'ily	Kitchen	Ishmaa'ily Kitchen	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979416.png	DL	27	KIT017528	A	70	NE
2175125	Jared	Koster	Jared Koster	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	KOS465792	A	\N	TB
1245656	Paul	Kruger	Paul Kruger	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245656.png	LB	30	KRU476709	A	99	CLE
1123488	Jameson	Konz	Jameson Konz	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	KON725546	A	\N	DEN
1679691	Luke	Kuechly	Luke Kuechly	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1679691.png	LB	24	KUE289610	A	59	CAR
2174915	Jordan	Leslie	Jordan Leslie	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	LES225858	A	\N	ATL
504720	Keenan	Lewis	Keenan Lewis	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/504720.png	DB	29	LEW426086	A	21	NO
1996892	Chris	Lewis-Harris	Chris Lewis-Harris	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996892.png	DB	27	LEW814204	A	37	CIN
2130139	Colin	Lockett	Colin Lockett	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	LOC092808	A	\N	WAS
1664835	Travis	Long	Travis Long	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	LON634156	IR	57	PHI
2174812	Steve	Miller	Steve Miller	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MIL572012	A	\N	CAR
1737187	Isaiah	Lewis	Isaiah Lewis	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LEW372284	A	\N	PIT
1664753	Dion	Lewis	Dion Lewis	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664753.png	RB	25	LEW218646	IR	33	NE
2175060	Justin	March-Lillard	Justin March-Lillard	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	MAR051467	IR	59	KC
426135	Brandon	Marshall	Brandon Marshall	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/426135.png	WR	32	MAR370922	A	15	NYJ
2174819	Gabe	Martin	Gabe Martin	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MAR601640	A	\N	ARI
1974206	Matthew	Masifilo	Matthew Masifilo	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1974206.png	DL	26	MAS017789	A	\N	CAR
1245862	Ricardo	Mathews	Ricardo Mathews	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245862.png	DL	28	MAT188704	A	\N	PIT
2130174	Jacob	Maxwell	Jacob Maxwell	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	25	MAX484931	A	\N	BUF
2060181	Miguel	Maysonet	Miguel Maysonet	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	MAY824290	A	\N	BUF
1273592	Bryan	McCann	Bryan McCann	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MCC135648	A	\N	WAS
1759816	Jordan	Matthews	Jordan Matthews	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759816.png	WR	23	MAT569663	A	81	PHI
1125328	Albert	McClellan	Albert McClellan	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125328.png	LB	29	MCC379498	A	50	BAL
1117497	Anthony	McCoy	Anthony McCoy	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117497.png	TE	28	MCC601593	IR	85	SEA
2130100	Kimario	McFadden	Kimario McFadden	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	MCF123424	A	33	TB
1664157	T.J.	McDonald	T.J. McDonald	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664157.png	DB	25	MCD574475	IR	25	STL
1632053	Lerentee	McCray	Lerentee McCray	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1632053.png	LB	25	MCC670331	A	55	DEN
1979404	Sean	McGrath	Sean McGrath	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979404.png	TE	28	MCG631892	A	84	SD
1675182	Steve	McLendon	Steve McLendon	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1675182.png	DL	30	MCL589496	A	\N	NYJ
1243697	Marvin	McNutt	Marvin McNutt	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MCN825303	A	\N	WAS
1630318	Davin	Meggett	Davin Meggett	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	MEG284361	A	\N	IND
2129683	Trey	Millard	Trey Millard	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129683.png	RB	24	MIL100731	A	\N	KC
2062166	Junior	Mertile	Junior Mertile	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MER779694	A	\N	HOU
564012	Chris	Maragos	Chris Maragos	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564012.png	DB	29	MAR003549	A	42	PHI
1620099	Brandon	Marshall	Brandon Marshall	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1620099.png	LB	26	MAR373584	A	54	DEN
2060085	Eric	Martin	Eric Martin	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060085.png	LB	24	MAR588329	A	52	NE
2130097	Freddie	Martino	Freddie Martino	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	MAR789763	A	80	PHI
2130098	Jeff	Mathews	Jeff Mathews	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	MAT565954	A	\N	ARI
1620608	Cliff	Matthews	Cliff Matthews	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620608.png	DL	26	MAT000000	A	\N	TB
1242845	Byron	Maxwell	Byron Maxwell	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1242845.png	DB	28	MAX395038	A	\N	MIA
1117496	Taylor	Mays	Taylor Mays	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117496.png	DB	28	MAY817996	A	\N	OAK
1673297	Onterio	McCalebb	Onterio McCalebb	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MCC056058	IR	\N	CIN
1279455	Terrell	McClain	Terrell McClain	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1279455.png	DL	27	MCC342970	A	97	DAL
493008	Luke	McCown	Luke McCown	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493008.png	QB	34	MCC600925	IR	\N	NO
2130244	L.J.	McCray	L.J. McCray	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	MCC668551	IR	31	SF
416389	Ray	McDonald	Ray McDonald	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MCD529425	A	\N	CHI
565805	Darren	McFadden	Darren McFadden	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/565805.png	RB	28	MCF084974	A	20	DAL
2062479	Matt	McGloin	Matt McGloin	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062479.png	QB	26	MCG491502	A	14	OAK
1244474	Joe	McKnight	Joe McKnight	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	MCK725816	A	\N	KC
1123581	Mike	McNeill	Mike McNeill	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MCN667282	A	\N	CAR
2057629	Jonathan	Meeks	Jonathan Meeks	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057629.png	DB	26	MEE559720	A	\N	BUF
2135486	Jamil	Merrell	Jamil Merrell	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MER448537	A	\N	CHI
2174811	Arthur	Miley	Arthur Miley	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	MIL071790	IR	71	CAR
1123389	Roy	Miller	Roy Miller	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123389.png	DL	28	MIL544398	A	97	JAC
1674135	Zach	Miller	Zach Miller	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674135.png	TE	31	MIL625333	A	86	CHI
1737423	Cassius	Marsh	Cassius Marsh	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737423.png	DL	23	MAR336314	A	91	SEA
519666	Sherrod	Martin	Sherrod Martin	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	31	MAR731251	A	36	CHI
1123294	Jeron	Mastrud	Jeron Mastrud	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MAS806653	A	\N	CHI
1856325	Chris	Matthews	Chris Matthews	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1856325.png	WR	26	MAT534950	A	84	BAL
1685973	Josh	Mauro	Josh Mauro	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	MAU640520	A	97	ARI
2175307	Desmond	Martin	Desmond Martin	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	MAR568629	A	\N	DET
2130230	Frances	Mays	Frances Mays	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MAY746680	A	\N	PHI
1674130	Brice	McCain	Brice McCain	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674130.png	DB	29	MCC051906	A	\N	TEN
1277484	Robert	McClain	Robert McClain	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1277484.png	DB	27	MCC340374	A	27	CAR
1674131	Jason	McCourty	Jason McCourty	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674131.png	DB	28	MCC600480	IR	30	TEN
1687410	Demetrius	McCray	Demetrius McCray	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1687410.png	DB	24	MCC241537	A	35	JAC
2130191	Dewey	McDonald	Dewey McDonald	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	MCD252575	A	35	OAK
1664227	Bradley	McDougald	Bradley McDougald	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664227.png	DB	25	MCD722675	A	30	TB
1825051	Keith	McGill	Keith McGill	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825051.png	DB	27	MCG395307	A	39	OAK
1852910	Benardrick	McKinney	Benardrick McKinney	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1852910.png	LB	23	MCK465206	A	55	HOU
2054733	Josh	McNary	Josh McNary	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	28	MCN283529	IR	57	IND
416670	Brandon	Mebane	Brandon Mebane	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/416670.png	DL	31	MEB059326	A	\N	SD
2175051	Sam	Meredith	Sam Meredith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MER167815	A	\N	ATL
1684101	Jeromy	Miles	Jeromy Miles	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIL052567	A	\N	NYG
1691169	Lamar	Miller	Lamar Miller	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691169.png	RB	24	MIL373118	A	\N	HOU
1620879	Von	Miller	Von Miller	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1620879.png	LB	27	MIL597654	A	58	DEN
1272264	Chase	Minnifield	Chase Minnifield	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIN433338	IR	\N	WAS
1113447	Earl	Mitchell	Earl Mitchell	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1113447.png	DL	28	MIT240204	A	90	MIA
1263827	Curtis	Marsh	Curtis Marsh	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAR338977	A	\N	DEN
1274369	Doug	Martin	Doug Martin	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1274369.png	RB	27	MAR578568	A	22	TB
2210396	Derrick	Mathews	Derrick Mathews	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	MAT146210	A	\N	WAS
396030	Rashean	Mathis	Rashean Mathis	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MAT211558	IR	\N	DET
1620022	Michael	Mauti	Michael Mauti	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1620022.png	LB	26	MAU699924	A	56	NO
504552	Clay	Matthews	Clay Matthews	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/504552.png	LB	29	MAT538690	A	52	GB
518626	Jerod	Mayo	Jerod Mayo	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518626.png	LB	30	MAY651489	IR	51	NE
1253163	Joe	Mays	Joe Mays	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1253163.png	LB	30	MAY781637	A	57	SD
2132695	Chris	McCain	Chris McCain	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132695.png	LB	24	MCC052035	A	58	MIA
1116624	Colin	McCarthy	Colin McCarthy	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MCC182839	A	\N	TEN
1273352	Rolando	McClain	Rolando McClain	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273352.png	LB	26	MCC342321	A	55	DAL
2219917	Dexter	McCoil	Dexter McCoil	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	MCC468194	A	\N	SD
2174216	Dexter	McDonald	Dexter McDonald	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	MCD254825	A	21	OAK
2129437	Dexter	McDougle	Dexter McDougle	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	MCD753329	A	23	NYJ
1664265	Brandon	McGee	Brandon McGee	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664265.png	DB	25	MCG092837	A	\N	DAL
2175354	T.Y.	McGill	T.Y. McGill	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	MCG421261	A	67	IND
2129436	Jerick	McKinnon	Jerick McKinnon	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129436.png	RB	23	MCK656764	A	31	MIN
2156675	Douglas	McNeil	Douglas McNeil	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	27	MCN436913	A	\N	SEA
2130126	Jamie	Meder	Jamie Meder	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	MED121550	A	98	CLE
416743	Brandon	Meriweather	Brandon Meriweather	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/416743.png	DB	32	MER280467	A	\N	NYG
1737090	Rontez	Miles	Rontez Miles	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737090.png	DB	27	MIL070511	A	45	NYJ
2174859	Matt	Miller	Matt Miller	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MIL423109	A	\N	DEN
2175084	Wesley	Miller	Wesley Miller	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIL601852	A	\N	BUF
1664390	Kevin	Minter	Kevin Minter	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664390.png	LB	25	MIN670026	A	51	ARI
2174300	Josh	Mitchell	Josh Mitchell	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIT374229	IR	\N	IND
2053375	Ifeanyi	Momah	Ifeanyi Momah	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2053375.png	TE	26	MOM027685	IR	80	ARI
2079873	Nick	Marshall	Nick Marshall	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MAR442504	A	41	JAC
1630985	Mike	Martin	Mike Martin	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630985.png	DL	25	MAR682391	A	93	TEN
1824805	Tre	Mason	Tre Mason	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824805.png	RB	22	MAS269896	A	27	STL
396031	Robert	Mathis	Robert Mathis	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396031.png	LB	35	MAT227229	A	98	IND
559367	Rey	Maualuga	Rey Maualuga	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559367.png	LB	29	MAU059326	A	58	CIN
2062194	Benson	Mayowa	Benson Mayowa	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062194.png	DL	24	MAY692147	A	\N	DAL
1975886	Rodney	McLeod	Rodney McLeod	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975886.png	DB	25	MCL709174	A	\N	PHI
1852422	Caleb	McSurdy	Caleb McSurdy	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MCS476199	A	\N	CLE
2174833	Cameron	Meredith	Cameron Meredith	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	MER114236	A	81	CHI
2058548	Steven	Miller	Steven Miller	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	MIL572526	A	\N	DET
2174221	Brian	Mihalik	Brian Mihalik	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	MIH578472	A	\N	PIT
1632348	Charles	Mitchell	Charles Mitchell	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIT153011	A	\N	DEN
1851149	Ty	Montgomery	Ty Montgomery	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851149.png	WR	23	MON677133	IR	88	GB
2129589	Zach	Moore	Zach Moore	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129589.png	DL	25	MOO773925	A	91	MIN
1272387	Ryan	Mundy	Ryan Mundy	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272387.png	DB	31	MUN327528	IR	21	CHI
1664237	Zeke	Motta	Zeke Motta	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MOT727654	A	\N	ATL
2174964	Tyler	Murphy	Tyler Murphy	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	MUR524808	A	\N	MIA
1737335	Rajion	Neal	Rajion Neal	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	NEA308982	A	\N	PIT
2061065	Steven	Nelson	Steven Nelson	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	NEL716852	A	20	KC
2174209	Taurean	Nixon	Taurean Nixon	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	NIX736272	A	\N	DEN
1860760	Nick	O'Leary	Nick O'Leary	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860760.png	TE	23	OLE052702	A	\N	BUF
1737114	Alec	Ogletree	Alec Ogletree	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737114.png	LB	24	OGL654714	IR	52	STL
1702983	Fendi	Onobun	Fendi Onobun	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	ONO088990	A	\N	JAC
566620	Chris	Owens	Chris Owens	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/566620.png	DB	29	OWE276284	IR	30	NO
396173	Carson	Palmer	Carson Palmer	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396173.png	QB	36	PAL249055	A	3	ARI
2132784	Tyler	Patmon	Tyler Patmon	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	PAT162966	A	29	MIA
1272502	Niles	Paul	Niles Paul	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272502.png	TE	26	PAU265785	IR	84	WAS
1971909	Jeris	Pendleton	Jeris Pendleton	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PEN193416	A	\N	IND
2132668	Senorise	Perry	Senorise Perry	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	PER713312	IR	32	CHI
1274378	Austin	Pettis	Austin Pettis	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PET735733	A	\N	SD
493029	Shaun	Phillips	Shaun Phillips	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	PHI651521	A	\N	IND
1264207	Casey	Matthews	Casey Matthews	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1264207.png	LB	27	MAT534119	IR	59	MIN
2174117	David	Mayo	David Mayo	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	MAY646134	A	55	CAR
430374	Trumaine	McBride	Trumaine McBride	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/430374.png	DB	30	MCB000000	A	38	NYG
2174956	B.J.	McBryde	B.J. McBryde	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	MCB749157	A	\N	GB
1996571	Ellis	McCarthy	Ellis McCarthy	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MCC184267	A	\N	MIA
2060175	Jake	McDonough	Jake McDonough	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MCD603937	A	\N	WAS
519682	Leodis	McKelvin	Leodis McKelvin	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/519682.png	DB	30	MCK224554	A	\N	PHI
2058320	Brandon	McManus	Brandon McManus	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2058320.png	K	24	MCM226186	A	8	DEN
431497	Robert	Meachem	Robert Meachem	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MEA057501	A	\N	NO
1117841	Garrett	McIntyre	Garrett McIntyre	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MCI600092	A	\N	NYJ
2174888	Mike	McFarland	Mike McFarland	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	MCF587954	IR	\N	IND
2059243	Rashaan	Melvin	Rashaan Melvin	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059243.png	DB	26	MEL736889	A	\N	NE
2057662	Steven	Means	Steven Means	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2057662.png	DL	25	MEA790667	A	51	PHI
1664450	Zach	Mettenberger	Zach Mettenberger	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664450.png	QB	24	MET532718	A	7	TEN
2175104	Mike	Meyer	Mike Meyer	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	MEY403404	A	\N	TEN
1117703	Gabe	Miller	Gabe Miller	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MIL273414	A	\N	WAS
2132898	Horace	Miller	Horace Miller	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MIL298452	A	\N	OAK
502400	Jeremy	Mincey	Jeremy Mincey	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/502400.png	DL	32	MIN067178	A	92	DAL
1277487	Kashif	Moore	Kashif Moore	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1277487.png	WR	27	MOO445899	A	\N	KC
2174905	Kendall	Montgomery	Kendall Montgomery	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MON591625	A	\N	MIA
1685895	Tracy	Moore	Tracy Moore	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOO723580	A	\N	FA
559572	Cameron	Morrah	Cameron Morrah	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MOR302981	A	\N	DEN
1975962	Dale	Moss	Dale Moss	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOS527558	A	\N	CHI
1616777	Matthew	Mulligan	Matthew Mulligan	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616777.png	TE	31	MUL568545	A	\N	DET
1664257	Aaron	Murray	Aaron Murray	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664257.png	QB	25	MUR545474	A	7	KC
2175075	Connor	Neighbors	Connor Neighbors	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	NEI157116	IR	\N	TEN
1737333	Tyrann	Mathieu	Tyrann Mathieu	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737333.png	DB	23	MAT195206	IR	32	ARI
563731	Josh	Mauga	Josh Mauga	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563731.png	LB	28	MAU217290	A	90	KC
1825510	Tre	McBride	Tre McBride	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825510.png	WR	23	MCB743359	A	16	TEN
1751547	Anthony	McCloud	Anthony McCloud	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	MCC415769	A	\N	ARI
2175416	Dasman	McCullum	Dasman McCullum	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MCC788195	A	\N	PHI
1963573	Marshall	McFadden	Marshall McFadden	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MCF180074	A	\N	STL
1117294	Sherrick	McManis	Sherrick McManis	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117294.png	DB	28	MCM219811	A	27	CHI
1674133	Henry	Melton	Henry Melton	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674133.png	DL	29	MEL597141	A	90	TB
1114232	Bruce	Miller	Bruce Miller	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114232.png	RB	28	MIL152896	A	49	SF
2175083	Jonathon	Mincy	Jonathon Mincy	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	MIN076392	A	\N	ATL
1272677	Dontay	Moch	Dontay Moch	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	MOC188520	A	\N	ATL
1274372	Kellen	Moore	Kellen Moore	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1274372.png	QB	26	MOO446260	A	17	DAL
518150	William	Moore	William Moore	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518150.png	DB	30	MOO749400	IR	25	ATL
1254119	Alfred	Morris	Alfred Morris	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1254119.png	RB	27	MOR317547	A	\N	DAL
2058551	Darryl	Morris	Darryl Morris	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058551.png	DB	25	MOR336351	A	\N	NYJ
235234	Santana	Moss	Santana Moss	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOS717844	A	\N	WAS
1634499	David	Nelson	David Nelson	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1634499.png	WR	29	NEL270110	IR	15	PIT
2174122	J.J.	Nelson	J.J. Nelson	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174122.png	WR	23	NEL488051	A	14	ARI
1114237	Jamar	Newsome	Jamar Newsome	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	NEW570307	A	\N	DAL
1737206	Louis	Nix	Louis Nix	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737206.png	DL	24	NIX500878	A	\N	NYG
2174841	Tom	Obarski	Tom Obarski	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	23	OBA428791	A	5	NYG
1979428	Adewale	Ojomo	Adewale Ojomo	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	OJO415291	A	\N	DAL
2132786	Branden	Oliver	Branden Oliver	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132786.png	RB	24	OLI200892	IR	43	SD
418071	Kyle	Orton	Kyle Orton	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	ORT716150	A	\N	BUF
2174788	Jordan	Ozerities	Jordan Ozerities	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	OZE563610	A	\N	ATL
1707280	Eric	Page	Eric Page	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PAG335632	A	\N	TB
1975893	Nathan	Palmer	Nathan Palmer	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975893.png	WR	26	PAL540163	A	\N	CHI
1116555	Preston	Parker	Preston Parker	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PAR365365	A	\N	NYG
518905	Bear	Pascoe	Bear Pascoe	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/518905.png	TE	30	PAS179616	A	80	DET
1854817	Spencer	Paysinger	Spencer Paysinger	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1854817.png	LB	27	PAY528965	A	42	MIA
302214	Julius	Peppers	Julius Peppers	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/302214.png	LB	36	PEP422041	A	56	GB
567376	Dimitri	Patterson	Dimitri Patterson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PAT410630	A	\N	NYJ
1619959	Whitney	Mercilus	Whitney Mercilus	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1619959.png	LB	25	MER110570	A	59	HOU
417185	Lance	Moore	Lance Moore	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/417185.png	WR	32	MOO468248	A	16	DET
1114939	Knowshon	Moreno	Knowshon Moreno	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	MOR120768	IR	\N	MIA
2176384	Jimmay	Mundine	Jimmay Mundine	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MUN303378	A	\N	NE
2174957	Raheem	Mostert	Raheem Mostert	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	MOS822492	A	41	CLE
1637004	Latavius	Murray	Latavius Murray	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1637004.png	RB	26	MUR671046	A	28	OAK
1630528	Ryan	Nassib	Ryan Nassib	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630528.png	QB	26	NAS738294	A	12	NYG
417623	Jordy	Nelson	Jordy Nelson	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/417623.png	WR	30	NEL517441	IR	87	GB
2129522	Jonathan	Newsome	Jonathan Newsome	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	NEW576048	A	\N	IND
2175103	Rodman	Noel	Rodman Noel	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	NOE384033	A	\N	CLE
2130879	Jake	Murphy	Jake Murphy	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	MUR253604	A	\N	DEN
1691428	Kevin	Norwood	Kevin Norwood	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1691428.png	WR	26	NOR783931	A	81	CAR
1851299	Rakeem	Nunez-Roches	Rakeem Nunez-Roches	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851299.png	DL	22	NUN250521	A	99	KC
1117620	Jared	Odrick	Jared Odrick	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117620.png	DL	28	ODR296636	A	75	JAC
418207	Jordan	Palmer	Jordan Palmer	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	PAL445384	A	\N	TEN
2175148	Brian	Parker	Brian Parker	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	PAR158626	A	82	KC
2130130	A.J.	Pataiali'i	A.J. Pataiali'i	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PAT002162	A	\N	NE
2130184	Mike	Pennel	Mike Pennel	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	PEN320554	A	64	GB
1860814	Denzel	Perryman	Denzel Perryman	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860814.png	LB	23	PER775462	A	52	SD
1686061	Bryce	Petty	Bryce Petty	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1686061.png	QB	24	PET742461	A	9	NYJ
1692882	Jason	Pierre-Paul	Jason Pierre-Paul	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1692882.png	DL	27	PIE587019	A	90	NYG
1620623	Tauren	Poole	Tauren Poole	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	POO772692	A	\N	BAL
1113524	Jerraud	Powers	Jerraud Powers	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1113524.png	DB	28	POW641973	A	25	ARI
1851284	Calvin	Pryor	Calvin Pryor	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851284.png	DB	23	PRY432541	A	25	NYJ
2175030	Jermauria	Rasco	Jermauria Rasco	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	RAS096558	IR	\N	TB
396068	Cory	Redding	Cory Redding	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396068.png	DL	35	RED327892	IR	90	ARI
2175732	Mike	Reilly	Mike Reilly	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	REI000001	A	\N	ARI
1759757	Ed	Reynolds	Ed Reynolds	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759757.png	DB	24	REY187178	A	30	PHI
2174123	Shaquille	Riddick	Shaquille Riddick	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	RID186261	A	47	ARI
584448	Marlon	Moore	Marlon Moore	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/584448.png	WR	28	MOO542785	A	15	CLE
2130866	James	Morris	James Morris	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	MOR405012	IR	46	NYG
1116692	Captain	Munnerlyn	Captain Munnerlyn	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116692.png	DB	28	MUN472912	A	24	MIN
2169640	Jason	Myers	Jason Myers	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2169640.png	K	24	MYE407253	A	2	JAC
2129672	Corey	Nelson	Corey Nelson	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	NEL185148	A	52	DEN
396171	Terence	Newman	Terence Newman	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396171.png	DB	37	NEW475575	A	23	MIN
2171885	Efe	Obada	Efe Obada	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	OBA109286	A	\N	KC
1117438	Kevin	Ogletree	Kevin Ogletree	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	OGL674214	A	\N	NYG
1979720	Jamize	Olawale	Jamize Olawale	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979720.png	RB	26	OLA706032	A	49	OAK
2130129	Zach	Orr	Zach Orr	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	ORR579972	A	54	BAL
517030	Brian	Orakpo	Brian Orakpo	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517030.png	LB	29	ORA355963	A	98	TEN
2174222	Ryan	Murphy	Ryan Murphy	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	MUR521537	A	\N	DEN
1664777	Brock	Osweiler	Brock Osweiler	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664777.png	QB	25	OSW479182	A	\N	HOU
1631918	Chris	Owusu	Chris Owusu	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631918.png	WR	26	OWU580524	A	17	NYJ
1891920	Ron	Parker	Ron Parker	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1891920.png	DB	28	PAR396955	A	38	KC
1632296	Patrick	Peterson	Patrick Peterson	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632296.png	DB	25	PET415511	A	21	ARI
1886787	Jordan	Phillips	Jordan Phillips	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1886787.png	DL	23	PHI386190	A	97	MIA
2133817	Adrian	Phillips	Adrian Phillips	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	PHI113173	A	\N	SD
1713803	Bernard	Pierce	Bernard Pierce	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1713803.png	RB	24	PIE198196	IR	30	JAC
1616784	Ropati	Pitoitua	Ropati Pitoitua	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1616784.png	DL	31	PIT082722	A	92	TEN
2175097	Kalafitoni	Pole	Kalafitoni Pole	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	POL061254	A	\N	CIN
1854826	Julian	Posey	Julian Posey	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	POS189366	A	\N	MIN
2174998	Damond	Powell	Damond Powell	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	POW227556	A	\N	ARI
2130231	Quron	Pratt	Quron Pratt	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PRA662174	A	\N	PHI
1757874	Josh	Price-Brent	Josh Price-Brent	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PRI340298	A	\N	DAL
1244818	Denarius	Moore	Denarius Moore	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOO260498	A	\N	BUF
1857552	Sterling	Moore	Sterling Moore	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1857552.png	DB	26	MOO695945	A	\N	BUF
1854198	Mike	Morgan	Mike Morgan	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1854198.png	LB	28	MOR196620	A	57	SEA
2174804	Merrill	Noel	Merrill Noel	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	NOE380048	A	46	BUF
1675023	Tom	Nelson	Tom Nelson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	NEL791436	A	\N	BAL
1737142	Owa	Odighizuwa	Owa Odighizuwa	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737142.png	DL	24	ODI237308	A	58	NYG
2062570	Toben	Opurum	Toben Opurum	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062570.png	RB	25	OPU563610	A	45	NO
396172	Calvin	Pace	Calvin Pace	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396172.png	LB	35	PAC125680	A	97	NYJ
1630444	Chris	Pantale	Chris Pantale	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630444.png	TE	26	PAN772358	A	\N	PHI
1274084	Johnny	Patrick	Johnny Patrick	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PAT267050	A	\N	NYJ
1621321	Isaiah	Pead	Isaiah Pead	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1621321.png	RB	26	PEA141066	A	\N	MIA
2175095	Brandon	Person	Brandon Person	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	PER814307	IR	34	ARI
1117442	John	Phillips	John Phillips	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1117442.png	TE	28	PHI371340	A	\N	SD
2130614	Roy	Philon	Roy Philon	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	PHI679898	A	\N	PIT
2174911	Terrell	Pinson	Terrell Pinson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	PIN811060	A	\N	OAK
1974204	Quinton	Pointer	Quinton Pointer	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	POI643935	A	\N	BAL
1273888	Kealoha	Pilares	Kealoha Pilares	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PIL077839	A	\N	CAR
2175313	David	Porter	David Porter	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	POR123657	A	\N	DAL
1631106	DeVier	Posey	DeVier Posey	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1631106.png	WR	26	POS172116	A	\N	DEN
1632077	Bacarri	Rambo	Bacarri Rambo	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632077.png	DB	25	RAM021629	A	30	BUF
1704754	Trevor	Reilly	Trevor Reilly	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1704754.png	LB	28	REI575845	A	57	NYJ
2130187	Chase	Rettig	Chase Rettig	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	RET329378	A	\N	SD
2174959	Denzel	Rice	Denzel Rice	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	RIC078760	A	35	PHI
2175077	Curtis	Riley	Curtis Riley	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	RIL123071	IR	\N	TEN
2008709	Jared	Roberts	Jared Roberts	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	ROB214533	A	\N	FA
2132680	Luther	Robinson	Luther Robinson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROB621927	A	\N	SD
1737204	Da'Rick	Rogers	Da'Rick Rogers	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ROG197172	A	\N	KC
1737651	Damontre	Moore	Damontre Moore	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737651.png	DL	23	MOO240793	A	57	MIA
1695590	Sio	Moore	Sio Moore	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1695590.png	LB	25	MOO679741	A	51	IND
502839	Josh	Morgan	Josh Morgan	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	MOR166910	A	\N	NO
1893206	Troy	Niklas	Troy Niklas	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1893206.png	TE	23	NIK372127	A	87	ARI
2174148	James	O'Shaughnessy	James O'Shaughnessy	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174148.png	TE	24	OSH126429	IR	80	KC
2130128	Deji	Olatoye	Deji Olatoye	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	OLA612618	A	29	DAL
1109761	Montell	Owens	Montell Owens	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	OWE687716	A	\N	CHI
4056	Michael	Palmer	Michael Palmer	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	PAL529610	A	\N	PIT
2174789	Damian	Parms	Damian Parms	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	PAR609316	A	\N	ATL
559624	Logan	Paulsen	Logan Paulsen	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/559624.png	TE	29	PAU641214	IR	82	WAS
2175405	Earnest	Pettway	Earnest Pettway	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	PET742143	A	\N	KC
1999943	Darius	Philon	Darius Philon	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	22	PHI676018	A	93	SD
2129596	Eric	Pinkins	Eric Pinkins	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129596.png	DB	24	PIN585405	A	47	SEA
2175663	Taylor	Pontius	Taylor Pontius	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	PON721799	A	\N	IND
1109832	Matt	Prater	Matt Prater	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1109832.png	K	31	PRA143616	A	5	DET
1762520	Keith	Pough	Keith Pough	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	POU424323	A	\N	CLE
1760327	MyCole	Pruitt	MyCole Pruitt	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	PRU720361	A	83	MIN
1243129	B.J.	Raji	B.J. Raji	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243129.png	DL	29	RAJ346075	A	90	GB
2174917	Blake	Renaud	Blake Renaud	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	REN037162	A	\N	MIN
1665147	Xavier	Rhodes	Xavier Rhodes	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1665147.png	DB	25	RHO552860	A	29	MIN
1691430	Trent	Richardson	Trent Richardson	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	RIC635450	A	\N	OAK
2174201	Aaron	Ripkowski	Aaron Ripkowski	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174201.png	RB	23	RIP355963	A	22	GB
1975975	Adrian	Robinson	Adrian Robinson	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ROB365259	A	\N	WAS
2062161	Ryan	Robinson	Ryan Robinson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062161.png	DL	25	ROB693604	IR	\N	SEA
419829	Carlos	Rogers	Carlos Rogers	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ROG136198	IR	\N	OAK
2138343	LaKendrick	Ross	LaKendrick Ross	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROS622912	A	\N	WAS
1737695	Ace	Sanders	Ace Sanders	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SAN498310	A	\N	JAC
1243133	Damik	Scafe	Damik Scafe	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SCA038387	A	\N	SD
2130153	Connor	Shaw	Connor Shaw	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130153.png	QB	24	SHA535087	IR	9	CLE
1871319	Devin	Smith	Devin Smith	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1871319.png	WR	24	SMI202710	IR	19	NYJ
1226278	Matt	Moore	Matt Moore	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1226278.png	QB	31	MOO551757	A	8	MIA
564167	Reggie	Nelson	Reggie Nelson	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/564167.png	DB	32	NEL617002	A	\N	OAK
1273186	Cam	Newton	Cam Newton	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1273186.png	QB	26	NEW693984	A	1	CAR
1731084	Josh	Norman	Josh Norman	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1731084.png	DB	28	NOR291905	A	24	CAR
517027	Chris	Ogbonnaya	Chris Ogbonnaya	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	OGB488119	A	\N	NYG
2060067	Earl	Okine	Earl Okine	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060067.png	DL	26	OKI444955	A	\N	IND
1825042	Nate	Orchard	Nate Orchard	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825042.png	LB	23	ORC266972	A	44	CLE
1631401	Ryan	Otten	Ryan Otten	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	OTT156430	A	\N	MIN
2130627	Tenny	Palepoi	Tenny Palepoi	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	PAL145300	IR	\N	SD
2130323	Cody	Parkey	Cody Parkey	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130323.png	K	24	PAR471800	IR	1	PHI
1824467	Quinton	Patton	Quinton Patton	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824467.png	WR	25	PAT721549	A	11	SF
517256	Domata	Peko	Domata Peko	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517256.png	DL	31	PEK461869	A	94	CIN
517620	Brandon	Pettigrew	Brandon Pettigrew	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517620.png	TE	31	PET710823	IR	87	DET
558593	Kenny	Phillips	Kenny Phillips	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PHI403361	A	\N	NO
2174867	Casey	Pierce	Casey Pierce	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	PIE225306	A	86	DET
1273811	Dennis	Pitta	Dennis Pitta	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273811.png	TE	30	PIT086233	IR	88	BAL
1631997	Chris	Polk	Chris Polk	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631997.png	RB	26	POL125846	A	22	HOU
431421	Paul	Posluszny	Paul Posluszny	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/431421.png	LB	31	POS250036	A	51	JAC
2130280	Justin	Renfrow	Justin Renfrow	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	26	REN364499	A	\N	SF
2129682	Tevin	Reese	Tevin Reese	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	REE693836	A	\N	CIN
1123426	Naaman	Roosevelt	Naaman Roosevelt	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ROO670292	A	\N	BUF
516510	Eddie	Royal	Eddie Royal	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/516510.png	WR	29	ROY461040	A	19	CHI
2174762	Tyler	Rutenbeck	Tyler Rutenbeck	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	RUT069215	A	\N	IND
1664646	Mohamed	Sanu	Mohamed Sanu	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664646.png	WR	26	SAN808447	A	\N	ATL
2132691	Kona	Schwenke	Kona Schwenke	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	SCH826036	A	53	SEA
2130177	Mohammed	Seisay	Mohammed Seisay	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	SEI726068	IR	39	SEA
1243192	Jabaal	Sheard	Jabaal Sheard	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243192.png	DL	26	SHE039759	A	93	NE
1631164	Kawann	Short	Kawann Short	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1631164.png	DL	27	SHO598059	A	99	CAR
2132673	Robert	Nelson	Robert Nelson	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	26	NEL659964	A	\N	HOU
1244979	Drake	Nevis	Drake Nevis	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	NEV687714	A	\N	CAR
1825096	Mark	Nzeocha	Mark Nzeocha	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825096.png	LB	26	NZE474619	A	53	DAL
2058557	Emmanuel	Ogbuehi	Emmanuel Ogbuehi	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	OGB672851	A	\N	TB
418053	Dan	Orlovsky	Dan Orlovsky	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/418053.png	QB	32	ORL645634	A	8	DET
1682492	Nathan	Overbay	Nathan Overbay	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	OVE139116	A	\N	BAL
2059239	Lawrence	Okoye	Lawrence Okoye	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059239.png	DL	24	OKO565722	A	\N	NYJ
1911		Packers	Packers	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	430	\N	\N	GB
1675025	Ashlee	Palmer	Ashlee Palmer	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	PAL226555	A	\N	NYG
1979384	Terrance	Parks	Terrance Parks	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PAR560979	IR	\N	ATL
2008606	Garry	Peters	Garry Peters	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	PET147944	A	\N	CAR
2174890	Dan	Pettinato	Dan Pettinato	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	PET732459	A	96	HOU
2175308	Shakim	Phillips	Shakim Phillips	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	PHI650542	A	19	PIT
2175350	Markus	Pierce-Brewster	Markus Pierce-Brewster	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	PIE470487	A	\N	TEN
1273027	Eddie	Pleasant	Eddie Pleasant	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273027.png	DB	27	PLE221488	A	35	HOU
418839	Bernard	Pollard	Bernard Pollard	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	POL249174	A	\N	TEN
1971903	John	Potter	John Potter	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	POT320982	A	\N	MIA
2030193	Ty	Powell	Ty Powell	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2030193.png	LB	27	POW566907	IR	57	BUF
1974181	Micanor	Regis	Micanor Regis	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	REG464064	A	\N	CAR
2130163	Bryn	Renner	Bryn Renner	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	26	REN575545	A	\N	SD
502506	Darius	Reynaud	Darius Reynaud	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	REY044528	A	\N	CHI
1867912	Chase	Reynolds	Chase Reynolds	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1867912.png	RB	28	REY105214	A	34	STL
1243206	Ray	Rice	Ray Rice	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	RIC154451	A	\N	BAL
1851154	Jordan	Richards	Jordan Richards	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1851154.png	DB	23	RIC293560	A	37	NE
1860858	Sheldon	Richardson	Sheldon Richardson	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860858.png	DL	25	RIC621130	A	91	NYJ
2174820	Jaxon	Shipley	Jaxon Shipley	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	SHI382672	A	16	ARI
517568	Adrian	Peterson	Adrian Peterson	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/517568.png	RB	31	PET260705	A	28	MIN
530198	Jason	Phillips	Jason Phillips	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	PHI282255	A	\N	PHI
2130285	Dashaun	Phillips	Dashaun Phillips	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	PHI141090	A	35	WAS
235235	Ryan	Pickett	Ryan Pickett	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	PIC781272	A	\N	HOU
2174881	Larry	Pinkard	Larry Pinkard	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	PIN536597	IR	\N	OAK
2129497	Kevin	Pierre-Louis	Kevin Pierre-Louis	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129497.png	LB	24	PIE584019	A	58	SEA
1717351	Dontari	Poe	Dontari Poe	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1717351.png	DL	25	POE346076	A	92	KC
396174	Troy	Polamalu	Troy Polamalu	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	POL041872	A	\N	PIT
1665010	Sean	Porter	Sean Porter	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1665010.png	LB	25	POR596464	A	\N	JAC
516942	Tracy	Porter	Tracy Porter	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/516942.png	DB	29	POR632160	A	21	CHI
1923		Rams	Rams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DST	0	423	\N	\N	STL
1630333	Kevin	Reddick	Kevin Reddick	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630333.png	LB	26	RED324517	A	51	BUF
1632787	David	Reed	David Reed	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	REE177257	A	\N	SF
2058162	Adam	Replogle	Adam Replogle	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058162.png	DL	25	REP456819	A	67	ATL
2174983	Michael	Reynolds	Michael Reynolds	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	REY465213	A	\N	TB
1765923	Paul	Richardson	Paul Richardson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1765923.png	WR	23	RIC593564	IR	10	SEA
1369410	Keith	Rivers	Keith Rivers	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	RIV576418	A	\N	DAL
2175076	Cody	Riggs	Cody Riggs	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	RIG409288	A	37	TEN
1737252	Nickell	Robey	Nickell Robey	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737252.png	DB	24	ROB361684	A	37	BUF
2062165	Khiry	Robinson	Khiry Robinson	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2062165.png	RB	26	ROB601698	IR	\N	NYJ
493043	Ben	Roethlisberger	Ben Roethlisberger	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493043.png	QB	34	ROE750381	A	7	PIT
1759561	Bradley	Roby	Bradley Roby	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1759561.png	DB	23	ROB805749	A	29	DEN
1752983	Caraun	Reid	Caraun Reid	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1752983.png	DL	24	REI254895	A	97	DET
396886	Tony	Romo	Tony Romo	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396886.png	QB	35	ROM787981	IR	9	DAL
1117625	Evan	Royster	Evan Royster	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	ROY668900	A	\N	ATL
420098	DeMeco	Ryans	DeMeco Ryans	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/420098.png	LB	31	RYA781990	A	59	PHI
1824731	Austin	Seferian-Jenkins	Austin Seferian-Jenkins	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824731.png	TE	23	SEF177981	A	87	TB
558597	Darryl	Sharpton	Darryl Sharpton	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SHA507578	A	\N	ARI
2060184	Russell	Shepard	Russell Shepard	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060184.png	WR	25	SHE495616	A	89	TB
1979420	Bobby	Rainey	Bobby Rainey	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1979420.png	RB	28	RAI596457	A	43	TB
1679976	Rueben	Randle	Rueben Randle	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679976.png	WR	24	RAN372502	A	\N	PHI
1630445	Kaleb	Ramsey	Kaleb Ramsey	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630445.png	DL	26	RAM446199	A	68	SF
2174842	Floyd	Raven	Floyd Raven	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	RAV405166	A	\N	CIN
1860857	Shane	Ray	Shane Ray	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1860857.png	DL	22	RAY427102	A	56	DEN
2129559	Keith	Reaser	Keith Reaser	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129559.png	DB	24	REA540220	A	27	SF
2185866	Trovon	Reed	Trovon Reed	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	REE451499	A	\N	SEA
2130103	Bernard	Reedy	Bernard Reedy	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	REE475386	A	\N	TB
1243710	Allen	Reisner	Allen Reisner	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243710.png	TE	27	REI694855	IR	85	BAL
1114353	Vincent	Rey	Vincent Rey	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114353.png	LB	28	REY000813	A	57	CIN
2130420	Micajah	Reynolds	Micajah Reynolds	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	26	REY457713	A	79	BAL
1971912	Daryl	Richardson	Daryl Richardson	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971912.png	RB	25	RIC404335	A	\N	PIT
1244982	Stevan	Ridley	Stevan Ridley	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1244982.png	RB	27	RID658759	A	\N	DET
2059632	Gerald	Rivers	Gerald Rivers	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059632.png	DL	25	RIV471935	A	\N	HOU
2174219	Darryl	Roberts	Darryl Roberts	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	ROB127520	IR	28	NE
1878033	Josh	Robinson	Josh Robinson	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	ROB590769	A	\N	IND
1273600	Aldrick	Robinson	Aldrick Robinson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273600.png	WR	27	ROB365875	A	\N	ATL
563628	Brian	Robiskie	Brian Robiskie	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ROB738302	A	\N	TEN
2175110	Daniel	Rodriguez	Daniel Rodriguez	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ROD651526	A	\N	STL
2058543	Jumal	Rolle	Jumal Rolle	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058543.png	DB	25	ROL522710	A	38	BAL
1891890	Jeremy	Ross	Jeremy Ross	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1891890.png	WR	28	ROS612742	A	13	NYJ
2133141	Kyle	Sebetic	Kyle Sebetic	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SEB303860	A	\N	SF
2174301	Al-Hajj	Shabazz	Al-Hajj Shabazz	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	SHA011978	A	\N	PIT
1977775	DeShawn	Shead	DeShawn Shead	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977775.png	DB	27	SHE023560	A	35	SEA
419996	Frostee	Rucker	Frostee Rucker	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/419996.png	DL	32	RUC531571	A	92	ARI
563629	Anderson	Russell	Anderson Russell	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	RUS335570	A	\N	CAR
2061049	Damarious	Randall	Damarious Randall	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2061049.png	DB	23	RAN052747	A	23	GB
2174982	Josh	Reese	Josh Reese	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	REE643471	A	\N	PHI
1631919	Konrad	Reuland	Konrad Reuland	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631919.png	TE	29	REU128541	A	\N	BAL
1679934	Rashaad	Reynolds	Rashaad Reynolds	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	REY594534	A	\N	JAC
493041	Philip	Rivers	Philip Rivers	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493041.png	QB	34	RIV651634	A	17	SD
1226322	Nick	Roach	Nick Roach	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ROA296634	IR	\N	OAK
2130880	Seth	Roberts	Seth Roberts	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130880.png	WR	25	ROB240370	A	10	OAK
1244767	Travian	Robertson	Travian Robertson	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROB360273	A	\N	WAS
1889923	Allen	Robinson	Allen Robinson	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1889923.png	WR	22	ROB365982	A	15	JAC
1245244	Keenan	Robinson	Keenan Robinson	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245244.png	LB	26	ROB591898	A	\N	NYG
419760	Brian	Robison	Brian Robison	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/419760.png	DL	32	ROB744810	A	96	MIN
1781543	Evan	Rodriguez	Evan Rodriguez	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	ROD652182	A	\N	TB
2139524	Quinten	Rollins	Quinten Rollins	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2139524.png	DB	23	ROL752507	A	24	GB
1633122	Kyle	Rudolph	Kyle Rudolph	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1633122.png	TE	26	RUD559717	A	82	MIN
2174853	Joel	Ross	Joel Ross	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	ROS616344	A	\N	TB
1243007	Da'Norris	Searcy	Da'Norris Searcy	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243007.png	DB	27	SEA484041	A	21	TEN
1265468	Harrison	Smith	Harrison Smith	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1265468.png	DB	27	SMI317465	A	22	MIN
2130290	Keith	Smith	Keith Smith	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	SMI449995	A	\N	DAL
1688453	Quanterus	Smith	Quanterus Smith	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1688453.png	DL	26	SMI577680	A	\N	JAC
1907299	Tye	Smith	Tye Smith	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	22	SMI766180	A	21	SEA
519916	Paul	Soliai	Paul Soliai	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/519916.png	DL	32	SOL128376	IR	\N	CAR
2175069	Harold	Spears	Harold Spears	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	24	SPE080556	A	\N	BAL
1631841	Mychal	Rivera	Mychal Rivera	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631841.png	TE	25	RIV251717	A	81	OAK
2174898	Matt	Robinson	Matt Robinson	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	ROB638731	A	\N	JAC
1723480	Trenton	Robinson	Trenton Robinson	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1723480.png	DB	26	ROB710202	A	\N	CAR
419764	Courtney	Roby	Courtney Roby	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	ROB808006	A	\N	ATL
2174966	Eli	Rogers	Eli Rogers	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	ROG291905	IR	\N	PIT
558697	George	Selvie	George Selvie	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/558697.png	DL	29	SEL776645	A	93	NYG
502821	Kory	Sheets	Kory Sheets	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SHE161581	A	\N	OAK
2175149	Dreamius	Smith	Dreamius Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	SMI230948	A	36	SD
2130106	Jerome	Smith	Jerome Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SMI403680	A	\N	ATL
1760028	Marcus	Smith	Marcus Smith	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1760028.png	LB	24	SMI000009	A	90	PHI
235157	Steve	Smith	Steve Smith	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/235157.png	WR	36	SMI733120	IR	89	BAL
1854555	Vic	So'oto	Vic So'oto	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SOO622937	A	\N	PIT
2130197	Daniel	Sorensen	Daniel Sorensen	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	26	SOR181831	A	49	KC
421377	Anthony	Spencer	Anthony Spencer	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SPE434038	IR	\N	NO
419723	Michael	Robinson	Michael Robinson	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	ROB656044	A	\N	SEA
419780	Aaron	Rodgers	Aaron Rodgers	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/419780.png	QB	32	ROD339293	A	12	GB
1683916	Justin	Rogers	Justin Rogers	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ROG442915	A	\N	WAS
1630792	Jacquies	Smith	Jacquies Smith	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630792.png	DL	26	SMI344730	A	56	TB
2189587	L.T.	Smith	L.T. Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SMI496190	A	\N	IND
493064	Daryl	Smith	Daryl Smith	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493064.png	LB	34	SMI164720	A	\N	TB
1696992	Rodney	Smith	Rodney Smith	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1696992.png	WR	26	SMI664731	A	14	DAL
2061139	Za'Darius	Smith	Za'Darius Smith	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2061139.png	LB	23	SMI827086	A	90	BAL
493071	Isaac	Sopoaga	Isaac Sopoaga	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SOP474619	A	\N	ARI
2079858	Martrell	Spaight	Martrell Spaight	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2079858.png	LB	22	SPA202019	A	50	WAS
1127382	C.J.	Spiller	C.J. Spiller	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1127382.png	RB	28	SPI347627	IR	28	NO
1824332	Daimion	Stafford	Daimion Stafford	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824332.png	DB	25	STA130953	A	39	TEN
1123431	James	Starks	James Starks	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123431.png	RB	30	STA583786	A	44	GB
421599	Craig	Stevens	Craig Stevens	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/421599.png	TE	31	STE601672	A	88	TEN
1737295	Kenny	Stills	Kenny Stills	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737295.png	WR	23	STI417540	A	10	MIA
1630465	Tommy	Streeter	Tommy Streeter	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	STR314972	A	\N	MIA
1116562	Patrick	Robinson	Patrick Robinson	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116562.png	DB	28	ROB671457	A	\N	IND
1664230	Craig	Roh	Craig Roh	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	ROH033222	A	\N	CAR
2058553	Lowell	Rose	Lowell Rose	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	ROS266202	A	\N	SD
1620412	Matt	Scott	Matt Scott	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	SCO518526	A	\N	CIN
1114290	Jimmy	Smith	Jimmy Smith	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114290.png	DB	27	SMI000006	A	22	BAL
2130686	Garrison	Smith	Garrison Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	SMI281735	A	65	SF
1971917	Michael	Smith	Michael Smith	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SMI000008	A	\N	NYJ
1737420	Telvin	Smith	Telvin Smith	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737420.png	LB	24	SMI739018	A	50	JAC
1679913	Isi	Sofele	Isi Sofele	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	SOF249173	A	\N	FA
2129410	Dezmen	Southward	Dezmen Southward	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129410.png	DB	25	SOU683214	A	\N	IND
2174198	Evan	Spencer	Evan Spencer	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174198.png	WR	22	SPE465518	A	85	TB
1114549	Micheal	Spurlock	Micheal Spurlock	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SPU401416	A	\N	CHI
421487	Drew	Stanton	Drew Stanton	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/421487.png	QB	31	STA482517	A	5	ARI
2129560	Jimmy	Staten	Jimmy Staten	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129560.png	DL	24	STA699522	A	\N	KC
1870668	Brandon	Stephens	Brandon Stephens	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	STE353550	A	\N	CLE
1979673	Jeremy	Stewart	Jeremy Stewart	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	STE768859	A	\N	DEN
2176385	Logan	Stokes	Logan Stokes	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	STO262725	A	\N	SD
1615611	Andy	Studebaker	Andy Studebaker	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	30	STU396828	A	59	IND
1979370	Chris	Summers	Chris Summers	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SUM363096	A	\N	BUF
2174207	Geoff	Swaim	Geoff Swaim	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	22	SWA047065	A	87	DAL
1632002	Alameda	Ta'amu	Alameda Ta'amu	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632002.png	DL	25	TAA403291	A	\N	KC
2130257	Richard	Rodgers	Richard Rodgers	7,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130257.png	TE	24	ROD493098	A	82	GB
2138835	Damond	Smith	Damond Smith	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SMI154280	A	\N	ARI
1700358	Geno	Smith	Geno Smith	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700358.png	QB	25	SMI269700	A	7	NYJ
235243	Justin	Smith	Justin Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SMI446020	A	\N	SF
1852914	Preston	Smith	Preston Smith	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1852914.png	DL	23	SMI576520	A	94	WAS
2132896	Kevin	Smith	Kevin Smith	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132896.png	WR	24	SMI000013	A	\N	SEA
1242961	Torrey	Smith	Torrey Smith	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1242961.png	WR	27	SMI344958	A	82	SF
2129671	Will	Smith	Will Smith	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SMI803880	A	\N	DAL
2174204	Kristjan	Sokoli	Kristjan Sokoli	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	SOK402541	A	67	SEA
1683844	Ryan	Spadola	Ryan Spadola	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1683844.png	WR	25	SPA065094	A	\N	DET
421347	Matt	Spaeth	Matt Spaeth	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/421347.png	TE	32	SPA090352	A	89	PIT
519785	Kory	Sperry	Kory Sperry	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	SPE752333	A	\N	MIN
1664982	Zac	Stacy	Zac Stacy	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664982.png	RB	25	STA100025	IR	30	NYJ
2130428	Jordan	Stanton	Jordan Stanton	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	STA490568	A	\N	NYG
2174212	Neal	Sterling	Neal Sterling	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174212.png	WR	24	STE550563	A	87	JAC
565654	Jonathan	Stewart	Jonathan Stewart	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/565654.png	RB	29	STE770966	A	28	CAR
1272618	Devon	Still	Devon Still	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272618.png	DL	26	STI382761	A	\N	HOU
1631109	Jake	Stoneburner	Jake Stoneburner	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631109.png	TE	26	STO477591	A	\N	MIA
1632218	Damion	Square	Damion Square	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632218.png	DL	27	SQU055371	A	71	SD
1674148	Ryan	Succop	Ryan Succop	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1674148.png	K	29	SUC073828	A	4	TEN
1664373	D.J.	Swearinger	D.J. Swearinger	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664373.png	DB	24	SWE023150	A	36	ARI
1891868	Phillip	Tanner	Phillip Tanner	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TAN700798	A	\N	SF
1273654	Ryan	Tannehill	Ryan Tannehill	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273654.png	QB	27	TAN298716	A	17	MIA
1675037	Will	Ta'ufo'ou	Will Ta'ufo'ou	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TAU121126	A	\N	JAC
396097	Ike	Taylor	Ike Taylor	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	TAY288150	A	\N	PIT
2059201	Steven	Terrell	Steven Terrell	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059201.png	DB	25	TER431038	A	23	SEA
2060100	Devin	Smith	Devin Smith	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SMI203580	A	\N	PIT
2176561	Jeret	Smith	Jeret Smith	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	SMI397015	A	\N	KC
2174303	Robert	Smith	Robert Smith	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SMI648875	A	\N	SEA
493067	Will	Smith	Will Smith	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SMI805040	A	\N	NE
1264797	Scott	Solomon	Scott Solomon	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1264797.png	LB	27	SOL374773	A	54	CLE
1691215	Akeem	Spence	Akeem Spence	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691215.png	DL	24	SPE410844	IR	97	TB
2175150	Cole	Stoudt	Cole Stoudt	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	0	STO679382	A	\N	SD
2008687	Vinnie	Sunseri	Vinnie Sunseri	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2008687.png	DB	24	SUN545070	IR	43	NO
1697207	Dax	Swanson	Dax Swanson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SWA385854	A	\N	MIA
2175070	Jack	Tabb	Jack Tabb	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	TAB064660	IR	\N	NO
1716248	Andy	Tanner	Andy Tanner	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	TAN392788	IR	\N	NO
563223	Brandon	Tate	Brandon Tate	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563223.png	WR	28	TAT140853	A	19	CIN
1632125	Devin	Taylor	Devin Taylor	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632125.png	DL	26	TAY170559	A	98	DET
1823899	Ryan	Taylor	Ryan Taylor	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	TAY682237	A	\N	KC
396106	Osi	Umenyiora	Osi Umenyiora	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	UME444955	A	\N	ATL
1245039	Cassius	Vaughn	Cassius Vaughn	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1245039.png	DB	28	VAU411454	A	20	SD
422868	Kevin	Vickerson	Kevin Vickerson	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	VIC647685	A	\N	NYJ
1631476	Bobby	Wagner	Bobby Wagner	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631476.png	LB	25	WAG236381	A	54	SEA
1683110	Bryan	Walters	Bryan Walters	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1683110.png	WR	28	WAL666831	A	81	JAC
553478	Nate	Washington	Nate Washington	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/553478.png	WR	32	WAS587797	A	\N	NE
1868411	Trae	Waynes	Trae Waynes	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1868411.png	DB	23	WAY469703	A	26	MIN
2175412	Derrick	Wells	Derrick Wells	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WEL362178	A	\N	TB
1971886	Corey	White	Corey White	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1971886.png	DB	25	WHI144737	A	\N	BUF
423565	Donte	Whitner	Donte Whitner	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423565.png	DB	30	WHI720119	A	31	CLE
1630571	Muhammad	Wilkerson	Muhammad Wilkerson	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1630571.png	DL	26	WIL078350	A	96	NYJ
1675237	Chris	Williams	Chris Williams	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL185352	A	\N	CHI
2130358	Dominique	Williams	Dominique Williams	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	25	WIL237023	A	\N	NYJ
1824786	Jesse	Williams	Jesse Williams	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	DAV000006	A	\N	SEA
2130105	Jacques	Smith	Jacques Smith	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SMI344654	A	\N	ATL
2174975	Rod	Smith	Rod Smith	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	SMI664332	A	45	DAL
2164713	Varmah	Sonie	Varmah Sonie	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SON474617	A	\N	CLE
1114679	Brandon	Spikes	Brandon Spikes	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SPI225805	A	\N	NE
1977082	Julian	Stanford	Julian Stanford	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1977082.png	LB	25	STA425500	A	\N	NYJ
1700261	Devin	Street	Devin Street	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700261.png	WR	25	STR305294	A	15	DAL
563145	Ndamukong	Suh	Ndamukong Suh	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/563145.png	DL	29	SUH046142	A	93	MIA
1759801	J.T.	Surratt	J.T. Surratt	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SUR550109	A	\N	FA
2129490	Lorenzo	Taliaferro	Lorenzo Taliaferro	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129490.png	RB	24	TAL413914	IR	34	BAL
1685988	Stepfan	Taylor	Stepfan Taylor	9,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1685988.png	RB	24	TAY700784	A	30	ARI
1265470	Golden	Tate	Golden Tate	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1265470.png	WR	27	TAT245022	A	15	DET
1632220	Courtney	Upshaw	Courtney Upshaw	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632220.png	LB	26	UPS271236	A	\N	ATL
2059224	Joe	Vellano	Joe Vellano	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059224.png	DL	27	VEL283236	A	\N	NE
553202	Raymond	Ventrone	Raymond Ventrone	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	VEN263361	A	\N	SF
2130292	Dustin	Vaughan	Dustin Vaughan	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	25	VAU093530	A	\N	PIT
1752529	Kyle	Van Noy	Kyle Van Noy	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1752529.png	LB	25	VAN656604	A	53	DET
1850743	Sammy	Watkins	Sammy Watkins	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1850743.png	WR	22	WAT270480	A	14	BUF
1258118	Lardarius	Webb	Lardarius Webb	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1258118.png	DB	30	WEB229214	A	21	BAL
1829652	Terrance	West	Terrance West	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1829652.png	RB	25	WES395447	A	27	BAL
2060115	Myles	White	Myles White	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060115.png	WR	26	WHI370946	A	\N	NYG
2028294	James	Wilder Jr.	James Wilder Jr.	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	23	WIL045793	A	\N	BUF
1254859	Damian	Williams	Damian Williams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL206430	A	\N	STL
2132716	K'Waun	Williams	K'Waun Williams	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2132716.png	DB	24	WIL415204	A	36	CLE
2175352	Tyrell	Williams	Tyrell Williams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2175352.png	WR	24	WIL547641	A	16	SD
2009342	Nick	Williams	Nick Williams	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2009342.png	DL	26	WIL456378	A	98	KC
493743	George	Wilson	George Wilson	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WIL694602	A	\N	TEN
423993	Kamerion	Wimbley	Kamerion Wimbley	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIM511051	A	\N	TEN
1703700	Jared	Smith	Jared Smith	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1703700.png	DL	26	SMI389991	A	\N	ATL
1276441	Lee	Smith	Lee Smith	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1276441.png	TE	28	SMI480240	A	86	OAK
2130232	Carey	Spear	Carey Spear	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	0	SPE024112	A	\N	CLE
2130155	Willie	Snead	Willie Snead	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130155.png	WR	23	SNE115447	A	83	NO
1782643	Brad	Sorensen	Brad Sorensen	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1782643.png	QB	28	SOR181019	A	\N	SD
1114942	Matthew	Stafford	Matthew Stafford	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1114942.png	QB	28	STA134157	A	9	DET
1125335	C.J.	Spillman	C.J. Spillman	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SPI428038	A	\N	DAL
2175121	Cameron	Stingily	Cameron Stingily	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	STI575031	A	\N	PIT
2061051	Jaelen	Strong	Jaelen Strong	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2061051.png	WR	22	STR627834	A	11	HOU
396177	Terrell	Suggs	Terrell Suggs	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396177.png	LB	33	SUG467201	IR	55	BAL
1979429	Julian	Talley	Julian Talley	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	TAL603770	A	\N	NYG
2130326	Erik	Swoope	Erik Swoope	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130326.png	TE	23	SWO288111	A	86	IND
421962	Darryl	Tapp	Darryl Tapp	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/421962.png	DL	31	TAP504282	A	52	DET
1851128	J.R.	Tavai	J.R. Tavai	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	22	TAV054826	IR	41	TEN
2129295	Alejandro	Villanueva	Alejandro Villanueva	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	27	VIL407040	A	78	PIT
2062148	Ty	Walker	Ty Walker	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WAL337408	A	\N	MIN
2008834	Zack	Wagenmann	Zack Wagenmann	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	WAG045827	IR	94	ARI
1632083	Blair	Walsh	Blair Walsh	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632083.png	K	26	WAL585473	A	3	MIN
423138	DeMarcus	Ware	DeMarcus Ware	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/423138.png	LB	33	WAR350675	A	94	DEN
2130293	L'Damian	Washington	L'Damian Washington	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WAS496729	A	\N	KC
1116572	Dekoda	Watson	Dekoda Watson	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116572.png	LB	28	WAT325980	A	52	NE
235256	Reggie	Wayne	Reggie Wayne	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WAY456953	A	\N	NE
1853223	Raymond	Webber	Raymond Webber	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	WEB323774	A	\N	MIA
493735	Wes	Welker	Wes Welker	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493735.png	WR	34	WEL219433	A	19	STL
2130607	Ethan	Westbrooks	Ethan Westbrooks	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	WES590634	A	93	STL
423455	Philip	Wheeler	Philip Wheeler	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423455.png	LB	31	WHE450744	A	51	ATL
2130189	Ryan	White	Ryan White	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WHI492263	A	\N	GB
1977158	Eddie	Whitley	Eddie Whitley	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WHI668171	A	\N	ARI
493113	Vince	Wilfork	Vince Wilfork	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493113.png	DL	34	WIL059369	A	75	HOU
2174868	Kevin	Snyder	Kevin Snyder	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	23	SNY378898	IR	45	NE
2174869	Brian	Suite	Brian Suite	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SUI622937	A	\N	DET
1116094	Stevenson	Sylvester	Stevenson Sylvester	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	SYL419791	A	\N	BUF
1115784	Jeff	Tarpinian	Jeff Tarpinian	9,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	TAR421567	A	\N	HOU
1679885	Taimi	Tutogi	Taimi Tutogi	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	TUT237308	A	\N	FA
559296	Harvey	Unga	Harvey Unga	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	UNG134179	A	\N	JAC
2165243	Uani'	Unga	Uani' Unga	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	28	UNG153679	A	47	NYG
1272299	DeMarcus	Van Dyke	DeMarcus Van Dyke	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1272299.png	DB	27	VAN491891	A	\N	ATL
1675229	Josh	Vaughan	Josh Vaughan	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	VAU175116	A	\N	ATL
1977084	Matt	Veldman	Matt Veldman	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	VEL129802	A	\N	WAS
1688630	Kayvon	Webster	Kayvon Webster	7,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1688630.png	DB	25	WEB712587	A	36	DEN
2185206	Jarrod	West	Jarrod West	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WES274091	A	\N	PIT
1673334	Chris	White	Chris White	4,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WHI137459	A	\N	NE
423552	Charlie	Whitehurst	Charlie Whitehurst	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423552.png	QB	33	WHI646241	IR	6	IND
2175114	Gary	Wilkins	Gary Wilkins	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WIL087457	A	\N	OAK
1273405	D.J.	Williams	D.J. Williams	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	WIL210447	A	\N	WAS
2185523	Ed	Williams	Ed Williams	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	25	WIL248029	A	5	GB
2176428	Kasen	Williams	Kasen Williams	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	WIL398812	A	18	SEA
2174870	Rasheed	Williams	Rasheed Williams	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WIL474708	A	\N	DET
2175001	Xavier	Williams	Xavier Williams	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	WIL577675	A	94	ARI
1751764	Julian	Wilson	Julian Wilson	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	WIL737677	IR	\N	BAL
1695627	Walter	Stewart	Walter Stewart	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	STE817566	A	\N	FA
2130668	Chaz	Sutton	Chaz Sutton	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	SUT321048	A	\N	TEN
1675223	Ronald	Talley	Ronald Talley	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	TAL662347	A	\N	NYJ
1697015	Brent	Urban	Brent Urban	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1697015.png	DL	24	URB226521	A	96	BAL
1696744	Ross	Ventrone	Ross Ventrone	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1696744.png	DB	29	VEN270111	A	35	PIT
1392	Adam	Vinatieri	Adam Vinatieri	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1392.png	K	43	VIN196019	A	4	IND
1688226	Anthony	Walters	Anthony Walters	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WAL664834	A	\N	ARI
1109396	Delanie	Walker	Delanie Walker	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1109396.png	TE	31	WAL115671	A	82	TEN
1632084	Cornelius	Washington	Cornelius Washington	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632084.png	DL	26	WAS177649	IR	94	CHI
2130606	Trey	Watts	Trey Watts	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	25	WAT728732	A	42	STL
2174814	Brandon	Wegher	Brandon Wegher	5,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	25	WEG651250	A	32	CAR
2130294	Chris	Whaley	Chris Whaley	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	25	WHA369337	A	64	DAL
1679844	Melvin	White	Melvin White	5,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679844.png	DB	25	WHI367166	A	\N	MIN
1245251	Fozzy	Whittaker	Fozzy Whittaker	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/1245251.png	RB	27	WHI731103	A	43	CAR
1788852	Brandon	Williams	Brandon Williams	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1788852.png	DL	27	WIL156757	A	98	BAL
1265473	Ian	Williams	Ian Williams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1265473.png	DL	26	WIL317445	A	93	SF
427356	Mario	Williams	Mario Williams	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/427356.png	DL	31	WIL431243	A	\N	MIA
1632384	Terrance	Williams	Terrance Williams	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632384.png	WR	26	WIL532584	A	83	DAL
2130201	Albert	Wilson	Albert Wilson	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130201.png	WR	23	WIL648299	A	12	KC
423912	Josh	Wilson	Josh Wilson	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423912.png	DB	31	WIL737475	IR	30	DET
2058171	Ronnie	Wingo	Ronnie Wingo	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WIN294553	A	\N	ATL
2141267	Glenn	Winston	Glenn Winston	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2141267.png	RB	26	WIN622925	A	28	CLE
519042	Wesley	Woodyard	Wesley Woodyard	4,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/519042.png	LB	29	WOO711929	A	59	TEN
1243344	Jason	Worilds	Jason Worilds	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WOR401085	A	\N	PIT
1273200	Major	Wright	Major Wright	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1273200.png	DB	27	WRI556864	IR	31	TB
2059251	Timothy	Wright	Timothy Wright	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059251.png	TE	26	WRI708932	A	83	DET
1825662	Zach	Zenner	Zach Zenner	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1825662.png	RB	24	ZEN513046	IR	34	DET
567087	Darrell	Stuckey	Darrell Stuckey	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/567087.png	DB	28	STU152615	IR	25	SD
2059223	Zach	Sudfeld	Zach Sudfeld	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2059223.png	TE	26	SUD207644	IR	44	NYJ
2130164	Jordan	Sullen	Jordan Sullen	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	SUL099202	A	\N	PIT
1691366	Will	Sutton	Will Sutton	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1691366.png	DL	24	SUT768626	A	93	CHI
2174304	Junior	Sylvestre	Junior Sylvestre	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	24	SYL431041	IR	44	IND
2174118	C.J.	Uzomah	C.J. Uzomah	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2174118.png	TE	23	UZO415291	A	87	CIN
2132676	Chase	Vaughn	Chase Vaughn	7,18	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	VAU412349	A	\N	DEN
2129492	Brock	Vereen	Brock Vereen	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129492.png	DB	23	VER271236	A	\N	NE
1853417	Josh	Victorian	Josh Victorian	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	VIC754503	A	\N	NYG
422972	Cameron	Wake	Cameron Wake	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/422972.png	DL	34	WAK155481	IR	91	MIA
1978286	Tyrunn	Walker	Tyrunn Walker	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1978286.png	DL	26	WAL339405	IR	93	DET
2175080	Lucas	Vincent	Lucas Vincent	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	VIN402000	A	\N	TEN
1752070	L.T.	Walton	L.T. Walton	11,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	24	WAL808582	A	96	PIT
1737109	Spencer	Ware	Spencer Ware	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1737109.png	RB	24	WAR371408	A	32	KC
1759740	Tony	Washington	Tony Washington	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759740.png	LB	23	WAS778332	A	\N	HOU
1123346	Sean	Weatherspoon	Sean Weatherspoon	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1123346.png	LB	28	WEA447811	A	\N	ATL
2129493	Larry	Webster	Larry Webster	5,18	https://auth.cbssports.com/images/football/nfl/players/170x170/2129493.png	DL	26	WEB730183	A	\N	CAR
2174194	Matt	Wells	Matt Wells	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	WEL487826	A	\N	STL
1737194	DeAndrew	White	DeAndrew White	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	24	WHI152624	A	18	SF
1824819	Jermaine	Whitehead	Jermaine Whitehead	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	WHI621475	A	40	BAL
1243088	Kyle	Wilber	Kyle Wilber	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1243088.png	LB	26	WIL000201	A	51	DAL
2129668	Lavelle	Westbrooks	Lavelle Westbrooks	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WES622201	A	\N	BUF
1263087	Joe	Young	Joe Young	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	LEF333270	A	\N	JAC
1975981	Brian	Tyms	Brian Tyms	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1975981.png	WR	27	TYM593274	IR	84	IND
1116194	Mitch	Unrein	Mitch Unrein	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1116194.png	DL	29	UNR404041	A	98	CHI
1995486	Tyler	Varga	Tyler Varga	10,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	22	VAR306732	IR	38	IND
2063168	Justin	Veltung	Justin Veltung	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	VEL701381	A	\N	STL
1824936	Jason	Verrett	Jason Verrett	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1824936.png	DB	24	VER625072	A	22	SD
2175049	Triston	Wade	Triston Wade	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WAD699714	A	\N	SEA
2174088	Tray	Walker	Tray Walker	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	23	WAL336010	A	25	BAL
2174934	Davon	Walls	Davon Walls	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WAL541051	A	\N	NYJ
1705702	Trabis	Ward	Trabis Ward	\N	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WAR277115	A	\N	FA
1125985	Daryl	Washington	Daryl Washington	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WAS186798	A	\N	ARI
493107	Benjamin	Watson	Benjamin Watson	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/493107.png	TE	35	WAT311042	A	\N	BAL
2175417	Daryl	Waud	Daryl Waud	8,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	0	WAU148049	A	\N	WAS
1891902	Martell	Webb	Martell Webb	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	0	WEB247304	A	\N	CLE
2174846	Mark	Weisman	Mark Weisman	7,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WEI702542	A	\N	CIN
1679942	Markus	Wheaton	Markus Wheaton	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1679942.png	WR	25	WHE120992	A	11	PIT
523103	Roddy	White	Roddy White	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/523103.png	WR	34	WHI472686	A	84	ATL
2130343	Marcus	Whitfield	Marcus Whitfield	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WHI656533	A	\N	JAC
2174815	Matt	Wile	Matt Wile	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	K	23	WIL048868	A	\N	DAL
1749684	Andre	Williams	Andre Williams	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1749684.png	RB	23	WIL142264	A	44	NYG
2130689	Damien	Williams	Damien Williams	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130689.png	RB	24	WIL206455	A	34	MIA
2175404	Tevin	Westbrook	Tevin Westbrook	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	23	WES571720	A	81	TB
2117662	Fred	Williams	Fred Williams	9,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	27	WIL286809	A	\N	KC
1860763	Karlos	Williams	Karlos Williams	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1860763.png	RB	22	WIL398250	A	29	BUF
1996523	Leonard	Williams	Leonard Williams	5,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1996523.png	DL	21	WIL426411	A	92	NYJ
2058366	Nick	Williams	Nick Williams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2058366.png	WR	25	WIL456782	A	15	ATL
2130632	Colton	Underwood	Colton Underwood	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130632.png	LB	24	UND194145	IR	\N	OAK
1664330	Kenny	Vaccaro	Kenny Vaccaro	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1664330.png	DB	25	VAC079101	A	32	NO
2082385	Rahsaan	Vaughn	Rahsaan Vaughn	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	VAU489379	A	\N	OAK
2174906	Zach	Vigil	Zach Vigil	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	25	VIG259556	A	49	MIA
2174845	Terrell	Watson	Terrell Watson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	22	WAT549840	A	\N	CLE
1116872	Ross	Weaver	Ross Weaver	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	0	WEA681123	A	\N	ARI
423334	Eric	Weddle	Eric Weddle	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423334.png	DB	31	WED290361	IR	\N	BAL
2129581	Keith	Wenning	Keith Wenning	7,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2129581.png	QB	25	WEN494391	A	\N	CIN
1675234	Jamaal	Westerman	Jamaal Westerman	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WES656246	A	\N	CLE
2174856	Lucky	Whitehead	Lucky Whitehead	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	23	WHI623090	A	13	DAL
1705885	J.J.	Wilcox	J.J. Wilcox	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1705885.png	DB	25	WIL027463	A	27	DAL
423706	Cary	Williams	Cary Williams	8,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423706.png	DB	31	WIL179853	A	40	WAS
2060284	Jonathan	Willard	Jonathan Willard	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL112277	A	\N	TEN
423714	DeAngelo	Williams	DeAngelo Williams	11,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/423714.png	RB	32	WIL221323	A	34	PIT
1823911	Jacquian	Williams	Jacquian Williams	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL326694	IR	\N	NYG
396181	Kevin	Williams	Kevin Williams	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/396181.png	DL	35	WIL407346	A	93	NO
1759592	James	White	James White	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1759592.png	RB	24	WHI242257	A	28	NE
1630601	Ryan	Williams	Ryan Williams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WIL000004	A	\N	DAL
1704209	Tourek	Williams	Tourek Williams	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1704209.png	LB	24	WIL544726	IR	58	SD
1979703	Mike	Willie	Mike Willie	9,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL593206	A	\N	BAL
1664188	David	Wilson	David Wilson	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	0	WIL672509	A	\N	NYG
1632222	Michael	Williams	Michael Williams	4,18,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1632222.png	TE	25	WIL438730	A	\N	NE
1631198	J.J.	Watt	J.J. Watt	9,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1631198.png	DL	27	WAT579210	A	99	HOU
1221953	Eric	Weems	Eric Weems	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1221953.png	WR	30	WEE551877	A	14	ATL
2130200	Charcandrick	West	Charcandrick West	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2130200.png	RB	24	WES136836	A	35	KC
1244509	Ryan	Whalen	Ryan Whalen	5,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WHA262526	A	\N	MIN
2060558	Kevin	White	Kevin White	7,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060558.png	WR	23	WHI315610	A	13	CHI
2130141	Nikita	Whitlock	Nikita Whitlock	11,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	RB	24	WHI683244	A	49	NYG
1927580	Michael	Wilhoite	Michael Wilhoite	10,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1927580.png	LB	29	WIL062802	IR	57	SF
2219937	Joel	Wilkinson	Joel Wilkinson	9,18,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	24	WIL100894	A	\N	ARI
2058173	Brandon	Williams	Brandon Williams	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	TE	28	WIL156886	IR	\N	MIA
493116	D.J.	Williams	D.J. Williams	7,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL237478	IR	\N	CHI
1125522	Dan	Williams	Dan Williams	6,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1125522.png	DL	28	WIL206531	A	90	OAK
1675850	Isaiah	Williams	Isaiah Williams	4,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	WR	0	WIL319061	A	\N	TEN
1700569	Duke	Williams	Duke Williams	8,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1700569.png	DB	25	WIL438575	A	27	BUF
2174935	Jordan	Williams	Jordan Williams	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DL	23	WIL384566	A	\N	MIA
2130851	Marcus	Williams	Marcus Williams	5,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	DB	25	WIL430732	A	20	NYJ
1983769	Maxx	Williams	Maxx Williams	9,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1983769.png	TE	21	WIL436992	A	87	BAL
1998196	P.J.	Williams	P.J. Williams	11,18,19,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/1998196.png	DB	22	WIL407276	IR	36	NO
2219913	Ryan	Williams	Ryan Williams	7,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	QB	2016	WIL514738	A	\N	GB
2004		Bills	Bills	8,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	ST	0	309	\N	\N	BUF
2176111	Chase	Williams	Chase Williams	6,18,19,20,21	https://auth.cbssports.com/images/players/unknown-player-170x170.png	LB	0	WIL185196	A	\N	OAK
2060074	Demetrius	Harris	Demetrius Harris	9,20,21	https://auth.cbssports.com/images/football/nfl/players/170x170/2060074.png	TE	24	HAR363756	A	84	KC
\.


--
-- Data for Name: dk; Type: TABLE DATA; Schema: public; Owner: Raymond
--

COPY dk ("position", name, salary, gameinfo, avgpointspergame, week, year) FROM stdin;
QB	Peyton Manning	10100	Ind@Den 08:30PM ET	27.325	1	2015
QB	Drew Brees	9700	NO@Atl 01:00PM ET	24.297	1	2015
RB	Jamaal Charles	9000	Ten@KC 01:00PM ET	24.8	1	2015
QB	Matthew Stafford	9000	NYG@Det 07:10PM ET	19.994	1	2015
QB	Aaron Rodgers	8800	GB@Sea 08:30PM ET	19.962	1	2015
WR	Calvin Johnson	8800	NYG@Det 07:10PM ET	23.229	1	2015
QB	Nick Foles	8500	Jax@Phi 01:00PM ET	20.417	1	2015
QB	Cam Newton	8400	Car@TB 04:25PM ET	19.573	1	2015
RB	LeSean McCoy	8400	Jax@Phi 01:00PM ET	22.047	1	2015
RB	Matt Forte	8300	Buf@Chi 01:00PM ET	22.144	1	2015
QB	Andrew Luck	8300	Ind@Den 08:30PM ET	20.641	1	2015
QB	Robert Griffin III	8200	Was@Hou 01:00PM ET	18.309	1	2015
WR	Demaryius Thomas	8100	Ind@Den 08:30PM ET	22.032	1	2015
QB	Colin Kaepernick	8000	SF@Dal 04:25PM ET	18.243	1	2015
WR	Josh Gordon	8000	Cle@Pit 01:00PM ET	23.957	1	2015
QB	Matt Ryan	7900	NO@Atl 01:00PM ET	18.131	1	2015
RB	Adrian Peterson	7900	Min@StL 01:00PM ET	17.907	1	2015
QB	Jay Cutler	7800	Buf@Chi 01:00PM ET	17.24	1	2015
WR	Dez Bryant	7800	SF@Dal 04:25PM ET	19.088	1	2015
QB	Tom Brady	7700	NE@Mia 01:00PM ET	17.396	1	2015
QB	Russell Wilson	7700	GB@Sea 08:30PM ET	17.334	1	2015
WR	Julio Jones	7700	NO@Atl 01:00PM ET	23.94	1	2015
WR	Brandon Marshall	7600	Buf@Chi 01:00PM ET	20.219	1	2015
QB	Ben Roethlisberger	7600	Cle@Pit 01:00PM ET	18.365	1	2015
QB	Carson Palmer	7500	SD@Ari 10:20PM ET	16.079	1	2015
QB	Tony Romo	7500	SF@Dal 04:25PM ET	18.928	1	2015
WR	Antonio Brown	7500	Cle@Pit 01:00PM ET	20.619	1	2015
QB	Eli Manning	7400	NYG@Det 07:10PM ET	13.02	1	2015
QB	Philip Rivers	7400	SD@Ari 10:20PM ET	19.034	1	2015
WR	A.J. Green	7400	Cin@Bal 01:00PM ET	19.471	1	2015
QB	Michael Vick	7300	Oak@NYJ 01:00PM ET	15.571	1	2015
RB	Arian Foster	7200	Was@Hou 01:00PM ET	14.313	1	2015
TE	Jimmy Graham	7200	NO@Atl 01:00PM ET	18.372	1	2015
QB	Ryan Tannehill	7200	NE@Mia 01:00PM ET	17.208	1	2015
WR	Alshon Jeffery	7100	Buf@Chi 01:00PM ET	18.725	1	2015
QB	Alex Smith	7000	Ten@KC 01:00PM ET	18.94	1	2015
RB	Montee Ball	7000	Ind@Den 08:30PM ET	6.868	1	2015
WR	Jordy Nelson	6800	GB@Sea 08:30PM ET	17.565	1	2015
RB	Marshawn Lynch	6800	GB@Sea 08:30PM ET	18.179	1	2015
QB	Andy Dalton	6800	Cin@Bal 01:00PM ET	20.352	1	2015
RB	DeMarco Murray	6800	SF@Dal 04:25PM ET	19.15	1	2015
WR	Keenan Allen	6800	SD@Ari 10:20PM ET	16.229	1	2015
QB	Joe Flacco	6700	Cin@Bal 01:00PM ET	15.036	1	2015
WR	Larry Fitzgerald	6700	SD@Ari 10:20PM ET	15.2	1	2015
TE	Rob Gronkowski	6700	NE@Mia 01:00PM ET	18.743	1	2015
RB	Le'Veon Bell	6700	Cle@Pit 01:00PM ET	16.992	1	2015
WR	Andre Johnson	6600	Was@Hou 01:00PM ET	18.606	1	2015
QB	Sam Bradford	6600	Min@StL 01:00PM ET	18.083	1	2015
RB	Giovani Bernard	6600	Cin@Bal 01:00PM ET	14.218	1	2015
RB	Reggie Bush	6500	NYG@Det 07:10PM ET	18.229	1	2015
WR	Pierre Garcon	6500	Was@Hou 01:00PM ET	18.344	1	2015
WR	Randall Cobb	6500	GB@Sea 08:30PM ET	17.043	1	2015
RB	Eddie Lacy	6500	GB@Sea 08:30PM ET	16.644	1	2015
QB	Geno Smith	6500	Oak@NYJ 01:00PM ET	13.921	1	2015
RB	Zac Stacy	6400	Min@StL 01:00PM ET	14.029	1	2015
WR	Victor Cruz	6300	NYG@Det 07:10PM ET	14.843	1	2015
RB	Doug Martin	6300	Car@TB 04:25PM ET	12.033	1	2015
QB	Johnny Manziel	6300	Cle@Pit 01:00PM ET	0	1	2015
RB	C.J. Spiller	6200	Buf@Chi 01:00PM ET	11.187	1	2015
QB	Jake Locker	6100	Ten@KC 01:00PM ET	15.391	1	2015
WR	Michael Crabtree	6100	SF@Dal 04:25PM ET	11.838	1	2015
RB	Shane Vereen	6100	NE@Mia 01:00PM ET	16.01	1	2015
QB	Brian Hoyer	6000	Cle@Pit 01:00PM ET	15.4	1	2015
WR	Michael Floyd	6000	SD@Ari 10:20PM ET	12.819	1	2015
RB	Andre Ellington	6000	SD@Ari 10:20PM ET	11.22	1	2015
WR	Marques Colston	5900	NO@Atl 01:00PM ET	14.312	1	2015
WR	Roddy White	5900	NO@Atl 01:00PM ET	11.221	1	2015
WR	Vincent Jackson	5900	Car@TB 04:25PM ET	15.9	1	2015
WR	Percy Harvin	5900	GB@Sea 08:30PM ET	6.9	1	2015
QB	EJ Manuel	5900	Buf@Chi 01:00PM ET	14.548	1	2015
WR	Wes Welker	5800	Ind@Den 08:30PM ET	15.738	1	2015
RB	Alfred Morris	5800	Was@Hou 01:00PM ET	11.956	1	2015
TE	Julius Thomas	5800	Ind@Den 08:30PM ET	15.153	1	2015
WR	Cordarrelle Patterson	5800	Min@StL 01:00PM ET	10.294	1	2015
WR	Jeremy Maclin	5700	Jax@Phi 01:00PM ET	0	1	2015
WR	Kendall Wright	5700	Ten@KC 01:00PM ET	13.744	1	2015
RB	Bishop Sankey	5700	Ten@KC 01:00PM ET	0	1	2015
QB	Josh McCown	5600	Car@TB 04:25PM ET	18.383	1	2015
QB	Matt Cassel	5600	Min@StL 01:00PM ET	13.442	1	2015
RB	Toby Gerhart	5600	Jax@Phi 01:00PM ET	4.364	1	2015
WR	Mike Wallace	5600	NE@Mia 01:00PM ET	13.206	1	2015
WR	T.Y. Hilton	5500	Ind@Den 08:30PM ET	16.867	1	2015
RB	Joique Bell	5500	NYG@Det 07:10PM ET	13.731	1	2015
QB	Teddy Bridgewater	5500	Min@StL 01:00PM ET	0	1	2015
QB	Ryan Fitzpatrick	5400	Was@Hou 01:00PM ET	17.06	1	2015
WR	DeSean Jackson	5400	Was@Hou 01:00PM ET	17.218	1	2015
WR	Torrey Smith	5400	Cin@Bal 01:00PM ET	13.113	1	2015
RB	Ryan Mathews	5400	SD@Ari 10:20PM ET	13.356	1	2015
QB	Matt Schaub	5300	Oak@NYJ 01:00PM ET	12.58	1	2015
WR	Julian Edelman	5300	NE@Mia 01:00PM ET	16.833	1	2015
RB	Trent Richardson	5300	Ind@Den 08:30PM ET	8.056	1	2015
RB	Frank Gore	5200	SF@Dal 04:25PM ET	12.205	1	2015
QB	Chad Henne	5200	Jax@Phi 01:00PM ET	12.289	1	2015
WR	Eric Decker	5200	Oak@NYJ 01:00PM ET	16.574	1	2015
RB	Ben Tate	5200	Cle@Pit 01:00PM ET	10.721	1	2015
WR	Reggie Wayne	5100	Ind@Den 08:30PM ET	15.114	1	2015
RB	Chris Johnson	5100	Oak@NYJ 01:00PM ET	15.513	1	2015
WR	Golden Tate	5100	NYG@Det 07:10PM ET	10.895	1	2015
QB	Matt Hasselbeck	5000	Ind@Den 08:30PM ET	1.333	1	2015
QB	Jason Campbell	5000	Cin@Bal 01:00PM ET	14.256	1	2015
QB	Charlie Whitehurst	5000	Ten@KC 01:00PM ET	-0.25	1	2015
QB	Tarvaris Jackson	5000	GB@Sea 08:30PM ET	3.228	1	2015
QB	Derek Anderson	5000	Car@TB 04:25PM ET	0	1	2015
QB	Bruce Gradkowski	5000	Cle@Pit 01:00PM ET	0	1	2015
QB	Dan Orlovsky	5000	NYG@Det 07:10PM ET	0	1	2015
QB	Kellen Clemens	5000	SD@Ari 10:20PM ET	9.632	1	2015
QB	Shaun Hill	5000	Min@StL 01:00PM ET	-0.1	1	2015
QB	Dennis Dixon	5000	Buf@Chi 01:00PM ET	0	1	2015
QB	Trent Edwards	5000	Oak@NYJ 01:00PM ET	0	1	2015
QB	Matt Moore	5000	NE@Mia 01:00PM ET	0.12	1	2015
RB	Pierre Thomas	5000	NO@Atl 01:00PM ET	13.263	1	2015
QB	Drew Stanton	5000	SD@Ari 10:20PM ET	0	1	2015
QB	Jordan Palmer	5000	Buf@Chi 01:00PM ET	0	1	2015
QB	Matt Flynn	5000	GB@Sea 08:30PM ET	11.883	1	2015
QB	Tyler Thigpen	5000	Cle@Pit 01:00PM ET	0	1	2015
RB	Steven Jackson	5000	NO@Atl 01:00PM ET	12.367	1	2015
QB	Luke McCown	5000	NO@Atl 01:00PM ET	-0.022	1	2015
QB	Caleb Hanie	5000	SF@Dal 04:25PM ET	0	1	2015
QB	Curtis Painter	5000	NYG@Det 07:10PM ET	0.027	1	2015
RB	Rashad Jennings	5000	NYG@Det 07:10PM ET	12.033	1	2015
QB	Josh Johnson	5000	SF@Dal 04:25PM ET	1	1	2015
QB	Mike Kafka	5000	Car@TB 04:25PM ET	0	1	2015
QB	Joe Webb	5000	Car@TB 04:25PM ET	0.519	1	2015
QB	Mark Sanchez	5000	Jax@Phi 01:00PM ET	0	1	2015
QB	Chase Daniel	5000	Ten@KC 01:00PM ET	3.624	1	2015
QB	Colt McCoy	5000	Was@Hou 01:00PM ET	-0.02	1	2015
QB	T.J. Yates	5000	NO@Atl 01:00PM ET	0.84	1	2015
QB	Christian Ponder	5000	Min@StL 01:00PM ET	13.336	1	2015
QB	Scott Tolzien	5000	GB@Sea 08:30PM ET	14.06	1	2015
QB	Ricky Stanzi	5000	Jax@Phi 01:00PM ET	0	1	2015
QB	Thad Lewis	5000	Buf@Chi 01:00PM ET	10.813	1	2015
QB	Jimmy Clausen	5000	Buf@Chi 01:00PM ET	0	1	2015
QB	Pat Devlin	5000	NE@Mia 01:00PM ET	0	1	2015
QB	Kellen Moore	5000	NYG@Det 07:10PM ET	0	1	2015
QB	Brandon Weeden	5000	SF@Dal 04:25PM ET	12.955	1	2015
TE	Jordan Cameron	5000	Cle@Pit 01:00PM ET	14.647	1	2015
QB	G.J. Kinne	5000	Jax@Phi 01:00PM ET	0	1	2015
QB	Tyrod Taylor	5000	Cin@Bal 01:00PM ET	2.36	1	2015
QB	Chandler Harnish	5000	Ind@Den 08:30PM ET	0	1	2015
QB	Austin Davis	5000	Min@StL 01:00PM ET	0	1	2015
QB	Ryan Lindley	5000	SD@Ari 10:20PM ET	0	1	2015
QB	Kirk Cousins	5000	Was@Hou 01:00PM ET	9.112	1	2015
QB	Matt Simms	5000	Oak@NYJ 01:00PM ET	3.98	1	2015
QB	Landry Jones	5000	Cle@Pit 01:00PM ET	0	1	2015
QB	Brendon Kay	5000	Cle@Pit 01:00PM ET	0	1	2015
QB	Matt Scott	5000	Cin@Bal 01:00PM ET	0	1	2015
QB	Terrelle Pryor	5000	GB@Sea 08:30PM ET	14.775	1	2015
QB	Blaine Gabbert	5000	SF@Dal 04:25PM ET	6.48	1	2015
QB	Sean Renfree	5000	NO@Atl 01:00PM ET	0	1	2015
QB	Mike Glennon	5000	Car@TB 04:25PM ET	13.155	1	2015
QB	B.J. Daniels	5000	GB@Sea 08:30PM ET	0	1	2015
QB	Ryan Nassib	5000	NYG@Det 07:10PM ET	0	1	2015
QB	Ryan Griffin	5000	NO@Atl 01:00PM ET	0	1	2015
QB	Zac Dysert	5000	Ind@Den 08:30PM ET	0	1	2015
QB	Matt McGloin	5000	Oak@NYJ 01:00PM ET	12.511	1	2015
QB	Matt Barkley	5000	Jax@Phi 01:00PM ET	2.267	1	2015
QB	Zach Mettenberger	5000	Ten@KC 01:00PM ET	0	1	2015
QB	Aaron Murray	5000	Ten@KC 01:00PM ET	0	1	2015
QB	Derek Carr	5000	Oak@NYJ 01:00PM ET	0	1	2015
QB	Brock Osweiler	5000	Ind@Den 08:30PM ET	1	1	2015
QB	Bryn Renner	5000	Ind@Den 08:30PM ET	0	1	2015
WR	Rueben Randle	5000	NYG@Det 07:10PM ET	8.756	1	2015
QB	Seth Lobato	5000	NE@Mia 01:00PM ET	0	1	2015
QB	Tajh Boyd	5000	Oak@NYJ 01:00PM ET	0	1	2015
QB	Jeff Tuel	5000	Buf@Chi 01:00PM ET	7.53	1	2015
QB	Garrett Gilbert	5000	Min@StL 01:00PM ET	0	1	2015
QB	Logan Thomas	5000	SD@Ari 10:20PM ET	0	1	2015
QB	AJ McCarron	5000	Cin@Bal 01:00PM ET	0	1	2015
QB	Tom Savage	5000	Was@Hou 01:00PM ET	0	1	2015
QB	Brad Sorensen	5000	SD@Ari 10:20PM ET	0	1	2015
QB	David Fales	5000	Buf@Chi 01:00PM ET	0	1	2015
QB	Stephen Morris	5000	Jax@Phi 01:00PM ET	0	1	2015
QB	Keith Wenning	5000	Cin@Bal 01:00PM ET	0	1	2015
QB	Chase Rettig	5000	GB@Sea 08:30PM ET	0	1	2015
QB	Tyler Bray	5000	Ten@KC 01:00PM ET	0	1	2015
QB	Connor Shaw	5000	Cle@Pit 01:00PM ET	0	1	2015
QB	James Franklin	5000	NYG@Det 07:10PM ET	0	1	2015
QB	Logan Kilgore	5000	NO@Atl 01:00PM ET	0	1	2015
QB	Jimmy Garoppolo	5000	NE@Mia 01:00PM ET	0	1	2015
QB	Blake Bortles	5000	Jax@Phi 01:00PM ET	0	1	2015
WR	Sammy Watkins	5000	Buf@Chi 01:00PM ET	0	1	2015
QB	Alex Tanney	5000	Car@TB 04:25PM ET	0	1	2015
QB	Matt Blanchard	5000	Car@TB 04:25PM ET	0	1	2015
QB	Dustin Vaughan	5000	SF@Dal 04:25PM ET	0	1	2015
WR	Dwayne Bowe	4900	Ten@KC 01:00PM ET	11.644	1	2015
RB	Stevan Ridley	4900	NE@Mia 01:00PM ET	9.713	1	2015
WR	Cecil Shorts III	4900	Jax@Phi 01:00PM ET	12.669	1	2015
TE	Vernon Davis	4800	SF@Dal 04:25PM ET	13.3	1	2015
WR	Emmanuel Sanders	4800	Ind@Den 08:30PM ET	11.344	1	2015
RB	Lamar Miller	4800	NE@Mia 01:00PM ET	7.994	1	2015
RB	Darren Sproles	4700	Jax@Phi 01:00PM ET	11.576	1	2015
WR	Anquan Boldin	4700	SF@Dal 04:25PM ET	15.774	1	2015
TE	Greg Olsen	4700	Car@TB 04:25PM ET	11.771	1	2015
WR	Terrance Williams	4600	SF@Dal 04:25PM ET	9.438	1	2015
TE	Jordan Reed	4600	Was@Hou 01:00PM ET	13.078	1	2015
RB	Bernard Pierce	4600	Cin@Bal 01:00PM ET	5.375	1	2015
RB	Terrance West	4600	Cle@Pit 01:00PM ET	0	1	2015
WR	Greg Jennings	4500	Min@StL 01:00PM ET	11.693	1	2015
TE	Jason Witten	4500	SF@Dal 04:25PM ET	13.444	1	2015
RB	Maurice Jones-Drew	4500	Oak@NYJ 01:00PM ET	12.735	1	2015
WR	Andrew Hawkins	4500	Cle@Pit 01:00PM ET	4.133	1	2015
WR	Brian Hartline	4500	NE@Mia 01:00PM ET	12.975	1	2015
WR	DeAndre Hopkins	4500	Was@Hou 01:00PM ET	9.138	1	2015
WR	Mike Evans	4500	Car@TB 04:25PM ET	0	1	2015
WR	Brandin Cooks	4500	NO@Atl 01:00PM ET	0	1	2015
DST	Seahawks	4400	GB@Sea 08:30PM ET	12.474	1	2015
WR	James Jones	4400	Oak@NYJ 01:00PM ET	11.18	1	2015
TE	Dennis Pitta	4400	Cin@Bal 01:00PM ET	10.725	1	2015
WR	Hakeem Nicks	4400	Ind@Den 08:30PM ET	10.307	1	2015
WR	Riley Cooper	4400	Jax@Phi 01:00PM ET	12.476	1	2015
RB	Fred Jackson	4400	Buf@Chi 01:00PM ET	14.856	1	2015
RB	Jeremy Hill	4400	Cin@Bal 01:00PM ET	0	1	2015
RB	DeAngelo Williams	4300	Car@TB 04:25PM ET	10.619	1	2015
RB	Chris Ivory	4300	Oak@NYJ 01:00PM ET	7.553	1	2015
TE	Kyle Rudolph	4300	Min@StL 01:00PM ET	9.913	1	2015
WR	Tavon Austin	4300	Min@StL 01:00PM ET	10.377	1	2015
RB	Mark Ingram	4200	NO@Atl 01:00PM ET	6.746	1	2015
RB	Lance Dunbar	4200	SF@Dal 04:25PM ET	2.989	1	2015
WR	Kelvin Benjamin	4200	Car@TB 04:25PM ET	0	1	2015
RB	Shonn Greene	4100	Ten@KC 01:00PM ET	5.764	1	2015
RB	Darren McFadden	4100	Oak@NYJ 01:00PM ET	10.234	1	2015
WR	Marvin Jones	4100	Cin@Bal 01:00PM ET	12.688	1	2015
TE	Zach Ertz	4100	Jax@Phi 01:00PM ET	6.947	1	2015
RB	Christine Michael	4100	GB@Sea 08:30PM ET	1.975	1	2015
WR	Aaron Dobson	4100	NE@Mia 01:00PM ET	9.323	1	2015
WR	Kenny Stills	4100	NO@Atl 01:00PM ET	7.594	1	2015
RB	Devonta Freeman	4100	NO@Atl 01:00PM ET	0	1	2015
DST	Chiefs	4000	Ten@KC 01:00PM ET	12.471	1	2015
WR	Steve Smith Sr.	4000	Cin@Bal 01:00PM ET	11.244	1	2015
TE	Heath Miller	4000	Cle@Pit 01:00PM ET	8.736	1	2015
WR	Danny Amendola	4000	NE@Mia 01:00PM ET	10.65	1	2015
RB	Knowshon Moreno	4000	NE@Mia 01:00PM ET	17.516	1	2015
WR	Kenny Britt	4000	Min@StL 01:00PM ET	1.717	1	2015
RB	Knile Davis	4000	Ten@KC 01:00PM ET	5.924	1	2015
WR	Robert Woods	4000	Buf@Chi 01:00PM ET	8.593	1	2015
RB	C.J. Anderson	4000	Ind@Den 08:30PM ET	1.183	1	2015
DST	Jets	3900	Oak@NYJ 01:00PM ET	6.188	1	2015
DST	Steelers	3900	Cle@Pit 01:00PM ET	7.313	1	2015
RB	Ahmad Bradshaw	3900	Ind@Den 08:30PM ET	13.933	1	2015
RB	James Starks	3900	GB@Sea 08:30PM ET	7.171	1	2015
WR	Stevie Johnson	3900	SF@Dal 04:25PM ET	11.225	1	2015
WR	Jerrel Jernigan	3900	NYG@Det 07:10PM ET	5.64	1	2015
TE	Charles Clay	3900	NE@Mia 01:00PM ET	11.963	1	2015
RB	Carlos Hyde	3900	SF@Dal 04:25PM ET	0	1	2015
WR	Donte Moncrief	3900	Ind@Den 08:30PM ET	0	1	2015
DST	Ravens	3800	Cin@Bal 01:00PM ET	7.813	1	2015
TE	Martellus Bennett	3800	Buf@Chi 01:00PM ET	10.619	1	2015
RB	LeGarrette Blount	3800	Cle@Pit 01:00PM ET	9.456	1	2015
RB	Charles Sims	3800	Car@TB 04:25PM ET	0	1	2015
WR	Justin Hunter	3800	Ten@KC 01:00PM ET	5.957	1	2015
WR	Kenbrell Thompkins	3800	NE@Mia 01:00PM ET	8.123	1	2015
WR	Jordan Matthews	3800	Jax@Phi 01:00PM ET	0	1	2015
DST	49ers	3700	SF@Dal 04:25PM ET	10.105	1	2015
WR	Harry Douglas	3700	NO@Atl 01:00PM ET	13.169	1	2015
RB	Danny Woodhead	3700	SD@Ari 10:20PM ET	13.728	1	2015
TE	Coby Fleener	3700	Ind@Den 08:30PM ET	9.6	1	2015
RB	Roy Helu Jr.	3700	Was@Hou 01:00PM ET	6.719	1	2015
RB	Jordan Todman	3700	Jax@Phi 01:00PM ET	4.513	1	2015
RB	Andre Williams	3700	NYG@Det 07:10PM ET	0	1	2015
WR	Odell Beckham Jr.	3700	NYG@Det 07:10PM ET	0	1	2015
RB	Tre Mason	3700	Min@StL 01:00PM ET	0	1	2015
RB	Ka'Deem Carey	3700	Buf@Chi 01:00PM ET	0	1	2015
DST	Patriots	3600	NE@Mia 01:00PM ET	8.778	1	2015
WR	Miles Austin	3600	Cle@Pit 01:00PM ET	4.4	1	2015
WR	Jerricho Cotchery	3600	Car@TB 04:25PM ET	10.544	1	2015
TE	Tyler Eifert	3600	Cin@Bal 01:00PM ET	5.969	1	2015
DST	Bears	3500	Buf@Chi 01:00PM ET	7.375	1	2015
DST	Packers	3500	GB@Sea 08:30PM ET	6.647	1	2015
TE	Owen Daniels	3500	Cin@Bal 01:00PM ET	13.44	1	2015
TE	Marcedes Lewis	3500	Jax@Phi 01:00PM ET	7.627	1	2015
TE	Scott Chandler	3500	Buf@Chi 01:00PM ET	8.094	1	2015
TE	Brent Celek	3500	Jax@Phi 01:00PM ET	7.106	1	2015
TE	Jared Cook	3500	Min@StL 01:00PM ET	9.381	1	2015
WR	Mike Thomas	3500	Was@Hou 01:00PM ET	0	1	2015
WR	Doug Baldwin	3500	GB@Sea 08:30PM ET	10.558	1	2015
WR	Stephen Hill	3500	Oak@NYJ 01:00PM ET	5.517	1	2015
RB	David Wilson	3500	NYG@Det 07:10PM ET	4.28	1	2015
WR	Marqise Lee	3500	Jax@Phi 01:00PM ET	0	1	2015
DST	Rams	3400	Min@StL 01:00PM ET	10.063	1	2015
WR	Devin Hester	3400	NO@Atl 01:00PM ET	0.313	1	2015
TE	Antonio Gates	3400	SD@Ari 10:20PM ET	10.928	1	2015
WR	Jerome Simpson	3400	Min@StL 01:00PM ET	8.288	1	2015
TE	Jermaine Gresham	3400	Cin@Bal 01:00PM ET	8.813	1	2015
WR	Mike Williams	3400	Buf@Chi 01:00PM ET	9.267	1	2015
WR	Nick Toon	3400	NO@Atl 01:00PM ET	1.35	1	2015
TE	Mychal Rivera	3400	Oak@NYJ 01:00PM ET	6.419	1	2015
WR	Jarrett Boykin	3400	GB@Sea 08:30PM ET	8.3	1	2015
WR	Rod Streater	3400	Oak@NYJ 01:00PM ET	11.031	1	2015
DST	Lions	3300	NYG@Det 07:10PM ET	7	1	2015
DST	Panthers	3300	Car@TB 04:25PM ET	11.529	1	2015
WR	Lance Moore	3300	Cle@Pit 01:00PM ET	7.253	1	2015
RB	BenJarvus Green-Ellis	3300	Cin@Bal 01:00PM ET	7.412	1	2015
TE	Delanie Walker	3300	Ten@KC 01:00PM ET	10.207	1	2015
WR	Malcom Floyd	3300	SD@Ari 10:20PM ET	11.95	1	2015
RB	Jonathan Stewart	3300	Car@TB 04:25PM ET	4.9	1	2015
WR	Brandon Gibson	3300	NE@Mia 01:00PM ET	11.514	1	2015
TE	Jeff Cumberland	3300	Oak@NYJ 01:00PM ET	5.987	1	2015
WR	Brian Quick	3300	Min@StL 01:00PM ET	3.763	1	2015
WR	Denarius Moore	3300	Oak@NYJ 01:00PM ET	11.423	1	2015
WR	Jermaine Kearse	3300	GB@Sea 08:30PM ET	5.895	1	2015
TE	Dwayne Allen	3300	Ind@Den 08:30PM ET	9	1	2015
WR	Markus Wheaton	3300	Cle@Pit 01:00PM ET	1.033	1	2015
TE	C.J. Fiedorowicz	3300	Was@Hou 01:00PM ET	0	1	2015
WR	Andre Holmes	3300	Oak@NYJ 01:00PM ET	7.71	1	2015
TE	Austin Seferian-Jenkins	3300	Car@TB 04:25PM ET	0	1	2015
TE	Eric Ebron	3300	NYG@Det 07:10PM ET	0	1	2015
DST	Bengals	3200	Cin@Bal 01:00PM ET	10.412	1	2015
DST	Giants	3200	NYG@Det 07:10PM ET	7.563	1	2015
DST	Eagles	3200	Jax@Phi 01:00PM ET	7.588	1	2015
TE	Benjamin Watson	3200	NO@Atl 01:00PM ET	3.429	1	2015
TE	Brandon Pettigrew	3200	NYG@Det 07:10PM ET	6.686	1	2015
TE	Ed Dickson	3200	Car@TB 04:25PM ET	3.644	1	2015
RB	Donald Brown	3200	SD@Ari 10:20PM ET	10.217	1	2015
TE	Garrett Graham	3200	Was@Hou 01:00PM ET	10.423	1	2015
WR	Tiquan Underwood	3200	Car@TB 04:25PM ET	7.917	1	2015
WR	Brandon LaFell	3200	NE@Mia 01:00PM ET	9.071	1	2015
TE	Rob Housler	3200	SD@Ari 10:20PM ET	6.954	1	2015
TE	Jake Ballard	3200	SD@Ari 10:20PM ET	3.313	1	2015
TE	Adrien Robinson	3200	NYG@Det 07:10PM ET	0	1	2015
TE	Travis Kelce	3200	Ten@KC 01:00PM ET	0	1	2015
RB	Jacquizz Rodgers	3200	NO@Atl 01:00PM ET	9.487	1	2015
TE	Ladarius Green	3200	SD@Ari 10:20PM ET	4.872	1	2015
TE	Joseph Fauria	3200	NYG@Det 07:10PM ET	5.169	1	2015
WR	Mohamed Sanu	3200	Cin@Bal 01:00PM ET	6.376	1	2015
TE	Gavin Escobar	3200	SF@Dal 04:25PM ET	2.15	1	2015
TE	Jace Amaro	3200	Oak@NYJ 01:00PM ET	0	1	2015
WR	Allen Robinson	3200	Jax@Phi 01:00PM ET	0	1	2015
TE	Troy Niklas	3200	SD@Ari 10:20PM ET	0	1	2015
DST	Bills	3100	Buf@Chi 01:00PM ET	9.063	1	2015
DST	Texans	3100	Was@Hou 01:00PM ET	4.5	1	2015
DST	Broncos	3100	Ind@Den 08:30PM ET	7	1	2015
DST	Cardinals	3100	SD@Ari 10:20PM ET	10.625	1	2015
DST	Buccaneers	3100	Car@TB 04:25PM ET	8	1	2015
TE	Anthony Fasano	3100	Ten@KC 01:00PM ET	6.36	1	2015
TE	Zach Miller	3100	GB@Sea 08:30PM ET	6.547	1	2015
TE	Brandon Myers	3100	Car@TB 04:25PM ET	7.7	1	2015
TE	Lance Kendricks	3100	Min@StL 01:00PM ET	5.453	1	2015
TE	Michael Hoomanawanui	3100	NE@Mia 01:00PM ET	2.567	1	2015
WR	Ryan Broyles	3100	NYG@Det 07:10PM ET	2.75	1	2015
WR	Austin Pettis	3100	Min@StL 01:00PM ET	6.369	1	2015
WR	Jeremy Kerley	3100	Oak@NYJ 01:00PM ET	9.533	1	2015
TE	Zach Sudfeld	3100	Oak@NYJ 01:00PM ET	0.807	1	2015
TE	Vance McDonald	3100	SF@Dal 04:25PM ET	1.233	1	2015
RB	Bryce Brown	3100	Buf@Chi 01:00PM ET	3.812	1	2015
RB	Theo Riddick	3100	NYG@Det 07:10PM ET	1.079	1	2015
TE	Arthur Lynch	3100	NE@Mia 01:00PM ET	0	1	2015
TE	Colt Lyerla	3100	GB@Sea 08:30PM ET	0	1	2015
WR	Martavis Bryant	3100	Cle@Pit 01:00PM ET	0	1	2015
DST	Titans	3000	Ten@KC 01:00PM ET	7.375	1	2015
DST	Raiders	3000	Oak@NYJ 01:00PM ET	6.5	1	2015
DST	Dolphins	3000	NE@Mia 01:00PM ET	7.75	1	2015
DST	Chargers	3000	SD@Ari 10:20PM ET	6.222	1	2015
TE	Mike Leach	3000	SD@Ari 10:20PM ET	0	1	2015
WR	Santana Moss	3000	Was@Hou 01:00PM ET	6.231	1	2015
TE	Alex Smith	3000	Cin@Bal 01:00PM ET	0.638	1	2015
WR	Micheal Spurlock	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	Courtney Roby	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Brad Smith	3000	Jax@Phi 01:00PM ET	0.7	1	2015
TE	Daniel Fells	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Nate Burleson	3000	Cle@Pit 01:00PM ET	10.367	1	2015
WR	Brandon Lloyd	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Derek Hagan	3000	Ten@KC 01:00PM ET	0	1	2015
TE	Craig Stevens	3000	Ten@KC 01:00PM ET	0.167	1	2015
TE	Dante Rosario	3000	Buf@Chi 01:00PM ET	0.153	1	2015
RB	Leon Washington	3000	Ten@KC 01:00PM ET	0.014	1	2015
WR	Jason Avant	3000	Car@TB 04:25PM ET	5.929	1	2015
TE	Clark Harris	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Jackie Battle	3000	Ten@KC 01:00PM ET	1.738	1	2015
RB	Montell Owens	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Nate Washington	3000	Ten@KC 01:00PM ET	11.056	1	2015
WR	Andre Caldwell	3000	Ind@Den 08:30PM ET	3.121	1	2015
TE	Jacob Tamme	3000	Ind@Den 08:30PM ET	3.037	1	2015
WR	Robert Meachem	3000	NO@Atl 01:00PM ET	4.365	1	2015
RB	Tyler Clutts	3000	SF@Dal 04:25PM ET	0.05	1	2015
WR	Donnie Avery	3000	Ten@KC 01:00PM ET	7.594	1	2015
TE	John Carlson	3000	SD@Ari 10:20PM ET	5.492	1	2015
WR	Kassim Osgood	3000	SF@Dal 04:25PM ET	0.159	1	2015
WR	Matthew Slater	3000	NE@Mia 01:00PM ET	0	1	2015
RB	Frank Summers	3000	Buf@Chi 01:00PM ET	2.1	1	2015
TE	Zach Miller	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	Josh Morgan	3000	Buf@Chi 01:00PM ET	3.05	1	2015
WR	Eddie Royal	3000	SD@Ari 10:20PM ET	9.906	1	2015
WR	Ted Ginn Jr.	3000	SD@Ari 10:20PM ET	8.347	1	2015
RB	Jalen Parmele	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Jerome Felton	3000	Min@StL 01:00PM ET	0.746	1	2015
WR	Anthony Armstrong	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Andre Brown	3000	Was@Hou 01:00PM ET	12.813	1	2015
TE	Kellen Davis	3000	NYG@Det 07:10PM ET	0.718	1	2015
RB	Marcus Thigpen	3000	NE@Mia 01:00PM ET	1.594	1	2015
TE	Darren Fells	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Kory Sheets	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Lavelle Hawkins	3000	Car@TB 04:25PM ET	0	1	2015
TE	Bear Pascoe	3000	NO@Atl 01:00PM ET	1.269	1	2015
TE	Kevin Brock	3000	Cin@Bal 01:00PM ET	1.65	1	2015
RB	Peyton Hillis	3000	NYG@Det 07:10PM ET	8.329	1	2015
RB	Cedric Peerman	3000	Cin@Bal 01:00PM ET	0.1	1	2015
RB	Chris Ogbonnaya	3000	Cle@Pit 01:00PM ET	7.269	1	2015
RB	Mike Tolbert	3000	Car@TB 04:25PM ET	7.382	1	2015
RB	Justin Forsett	3000	Cin@Bal 01:00PM ET	2.922	1	2015
RB	Tony Fiammetta	3000	Buf@Chi 01:00PM ET	0.606	1	2015
RB	Jed Collins	3000	NYG@Det 07:10PM ET	1.661	1	2015
TE	Gary Barnidge	3000	Cle@Pit 01:00PM ET	2.356	1	2015
RB	Will Ta'ufo'ou	3000	Jax@Phi 01:00PM ET	0.881	1	2015
WR	Darrius Heyward-Bey	3000	Cle@Pit 01:00PM ET	4.147	1	2015
TE	Tony Moeaki	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	Stephen Williams	3000	NE@Mia 01:00PM ET	0	1	2015
RB	John Kuhn	3000	GB@Sea 08:30PM ET	2.481	1	2015
WR	Seyi Ajirotutu	3000	SD@Ari 10:20PM ET	0.963	1	2015
WR	Marlon Moore	3000	Cle@Pit 01:00PM ET	1.007	1	2015
TE	Jameson Konz	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Brandon Tate	3000	Cin@Bal 01:00PM ET	0.094	1	2015
TE	David Johnson	3000	SD@Ari 10:20PM ET	2.2	1	2015
TE	Nathan Overbay	3000	Cin@Bal 01:00PM ET	0	1	2015
TE	Mike Caussin	3000	Was@Hou 01:00PM ET	0	1	2015
RB	Darrel Young	3000	Was@Hou 01:00PM ET	3.015	1	2015
TE	Clay Harbor	3000	Jax@Phi 01:00PM ET	4.013	1	2015
RB	Josh Vaughan	3000	NO@Atl 01:00PM ET	0.1	1	2015
WR	Louis Murphy Jr.	3000	Car@TB 04:25PM ET	1.05	1	2015
WR	David Nelson	3000	Oak@NYJ 01:00PM ET	7.525	1	2015
WR	David Gettis	3000	Car@TB 04:25PM ET	0	1	2015
RB	Antone Smith	3000	NO@Atl 01:00PM ET	1.967	1	2015
WR	Brian Robiskie	3000	Ten@KC 01:00PM ET	0	1	2015
RB	Alfonso Smith	3000	SF@Dal 04:25PM ET	1.7	1	2015
TE	Chad Rempel	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	Chris Williams	3000	Buf@Chi 01:00PM ET	0	1	2015
TE	Logan Paulsen	3000	Was@Hou 01:00PM ET	4.419	1	2015
RB	Jorvorskie Lane	3000	Car@TB 04:25PM ET	0	1	2015
TE	Andrew DePaola	3000	Car@TB 04:25PM ET	0	1	2015
TE	Zach Potter	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Jordan Norwood	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Mario Manningham	3000	NYG@Det 07:10PM ET	2.917	1	2015
RB	John Conner	3000	NYG@Det 07:10PM ET	0.7	1	2015
WR	Trindon Holliday	3000	NYG@Det 07:10PM ET	0.594	1	2015
TE	Richie Brockel	3000	Car@TB 04:25PM ET	0.076	1	2015
WR	Isaiah Williams	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Kevin Ogletree	3000	NYG@Det 07:10PM ET	3.744	1	2015
RB	Collin Mooney	3000	Ten@KC 01:00PM ET	0.767	1	2015
TE	David Ausberry	3000	Oak@NYJ 01:00PM ET	0	1	2015
RB	Stanley Havili	3000	Ind@Den 08:30PM ET	2.593	1	2015
TE	Dorin Dickerson	3000	Ten@KC 01:00PM ET	1.08	1	2015
WR	Kyle Williams	3000	Ten@KC 01:00PM ET	2.56	1	2015
TE	Jim Dray	3000	Cle@Pit 01:00PM ET	3.656	1	2015
RB	Erik Lorig	3000	NO@Atl 01:00PM ET	1.047	1	2015
TE	Virgil Green	3000	Ind@Den 08:30PM ET	0.811	1	2015
TE	John Phillips	3000	SD@Ari 10:20PM ET	0.467	1	2015
RB	Shaun Draughn	3000	Buf@Chi 01:00PM ET	0.067	1	2015
TE	Ryan Taylor	3000	GB@Sea 08:30PM ET	0.6	1	2015
TE	Richard Gordon	3000	Ten@KC 01:00PM ET	0.65	1	2015
WR	Preston Parker	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Greg Salas	3000	Oak@NYJ 01:00PM ET	2.788	1	2015
WR	Jacoby Ford	3000	Oak@NYJ 01:00PM ET	1.671	1	2015
RB	Marcel Reece	3000	Oak@NYJ 01:00PM ET	7.174	1	2015
TE	Cameron Morrah	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Dwayne Harris	3000	SF@Dal 04:25PM ET	2.662	1	2015
RB	Bruce Miller	3000	SF@Dal 04:25PM ET	3.614	1	2015
WR	Jamar Newsome	3000	SF@Dal 04:25PM ET	0	1	2015
TE	Kyle Nelson	3000	SF@Dal 04:25PM ET	0	1	2015
TE	Matthew Mulligan	3000	Buf@Chi 01:00PM ET	0.659	1	2015
WR	Marc Mariani	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Bryan Walters	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Andre Roberts	3000	Was@Hou 01:00PM ET	6.381	1	2015
WR	Jacoby Jones	3000	Cin@Bal 01:00PM ET	8.625	1	2015
RB	Anthony Dixon	3000	Buf@Chi 01:00PM ET	1.668	1	2015
WR	Jeremy Ross	3000	NYG@Det 07:10PM ET	2.592	1	2015
RB	Shaun Chapas	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Chase Reynolds	3000	Min@StL 01:00PM ET	0	1	2015
WR	Naaman Roosevelt	3000	Buf@Chi 01:00PM ET	0	1	2015
TE	Mike McNeill	3000	Car@TB 04:25PM ET	0.107	1	2015
WR	Joseph Morgan	3000	NO@Atl 01:00PM ET	0	1	2015
TE	Jeron Mastrud	3000	Buf@Chi 01:00PM ET	0.925	1	2015
WR	Armanti Edwards	3000	Buf@Chi 01:00PM ET	0.333	1	2015
WR	Damian Williams	3000	NE@Mia 01:00PM ET	3.18	1	2015
TE	Lee Smith	3000	Buf@Chi 01:00PM ET	0.8	1	2015
TE	Luke Stocker	3000	Car@TB 04:25PM ET	0	1	2015
RB	Dexter McCluster	3000	Ten@KC 01:00PM ET	8.819	1	2015
WR	Weston Dressler	3000	Ten@KC 01:00PM ET	0	1	2015
TE	Andrew Quarless	3000	GB@Sea 08:30PM ET	4.529	1	2015
RB	James Develin	3000	NE@Mia 01:00PM ET	0.956	1	2015
WR	Kerry Taylor	3000	Jax@Phi 01:00PM ET	5.09	1	2015
RB	Evan Royster	3000	Was@Hou 01:00PM ET	0.13	1	2015
TE	Brandon Barden	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Dezmon Briscoe	3000	SF@Dal 04:25PM ET	0	1	2015
RB	Fozzy Whittaker	3000	Car@TB 04:25PM ET	4.029	1	2015
WR	Arrelious Benn	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Marcus Easley	3000	Buf@Chi 01:00PM ET	0.206	1	2015
WR	Kealoha Pilares	3000	Car@TB 04:25PM ET	0	1	2015
TE	James Casey	3000	Jax@Phi 01:00PM ET	0.381	1	2015
WR	Armon Binns	3000	NE@Mia 01:00PM ET	0	1	2015
RB	Matt Asiata	3000	Min@StL 01:00PM ET	3.9	1	2015
WR	Marcus Harris	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Charles Johnson	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	DuJuan Harris	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Deonte Thompson	3000	Cin@Bal 01:00PM ET	2.8	1	2015
RB	Kendall Hunter	3000	SF@Dal 04:25PM ET	3.2	1	2015
RB	Daniel Herron	3000	Ind@Den 08:30PM ET	1.667	1	2015
WR	Phil Bates	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Dane Sanzenbacher	3000	Cin@Bal 01:00PM ET	1.21	1	2015
RB	Henry Hynoski	3000	NYG@Det 07:10PM ET	0.5	1	2015
WR	Leonard Hankerson	3000	Was@Hou 01:00PM ET	8.55	1	2015
WR	Fred Williams	3000	Ten@KC 01:00PM ET	0	1	2015
RB	Jonathan Dwyer	3000	SD@Ari 10:20PM ET	2.273	1	2015
WR	Michael Campbell	3000	Oak@NYJ 01:00PM ET	0	1	2015
TE	Rhett Ellison	3000	Min@StL 01:00PM ET	1.555	1	2015
WR	Ronald Johnson	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Ifeanyi Momah	3000	Jax@Phi 01:00PM ET	0	1	2015
RB	Joe McKnight	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Marvin McNutt	3000	Car@TB 04:25PM ET	0	1	2015
WR	Reggie Dunn	3000	SD@Ari 10:20PM ET	0	1	2015
WR	David Gilreath	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Dontrelle Inman	3000	SD@Ari 10:20PM ET	0	1	2015
WR	Danny Coale	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Dallas Walker	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Greg Little	3000	Oak@NYJ 01:00PM ET	6.406	1	2015
RB	Jeremy Stewart	3000	Oak@NYJ 01:00PM ET	0.677	1	2015
WR	Ryan Whalen	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Joe Banyard	3000	Min@StL 01:00PM ET	0.7	1	2015
WR	Kamar Aiken	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Robert Hughes	3000	SD@Ari 10:20PM ET	1.6	1	2015
RB	Travaris Cadet	3000	NO@Atl 01:00PM ET	0.567	1	2015
WR	Kashif Moore	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Anthony Sherman	3000	Ten@KC 01:00PM ET	2.759	1	2015
WR	Julian Talley	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Mike Brown	3000	Jax@Phi 01:00PM ET	8.236	1	2015
WR	Aldrick Robinson	3000	Was@Hou 01:00PM ET	4.131	1	2015
WR	Jeff Maehl	3000	Jax@Phi 01:00PM ET	0.982	1	2015
RB	Will Tukuafu	3000	SF@Dal 04:25PM ET	0.24	1	2015
WR	Vincent Brown	3000	SD@Ari 10:20PM ET	5.4	1	2015
RB	Robert Turbin	3000	GB@Sea 08:30PM ET	2.384	1	2015
WR	Drew Davis	3000	NO@Atl 01:00PM ET	2.85	1	2015
TE	Larry Donnell	3000	NYG@Det 07:10PM ET	0.381	1	2015
RB	Bilal Powell	3000	Oak@NYJ 01:00PM ET	8.944	1	2015
TE	Garrett Celek	3000	SF@Dal 04:25PM ET	0.493	1	2015
WR	B.J. Cunningham	3000	Jax@Phi 01:00PM ET	0	1	2015
RB	Will Johnson	3000	Cle@Pit 01:00PM ET	1.131	1	2015
TE	Raymond Webber	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Kevin Elliott	3000	Buf@Chi 01:00PM ET	0	1	2015
RB	Bobby Rainey	3000	Car@TB 04:25PM ET	7.325	1	2015
RB	Evan Rodriguez	3000	Buf@Chi 01:00PM ET	0	1	2015
TE	Niles Paul	3000	Was@Hou 01:00PM ET	0.607	1	2015
WR	Derek Moye	3000	Cle@Pit 01:00PM ET	1.429	1	2015
TE	Weslye Saunders	3000	Ind@Den 08:30PM ET	1.229	1	2015
WR	Devon Wylie	3000	SF@Dal 04:25PM ET	-0.5	1	2015
TE	D.J. Williams	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Toney Clemons	3000	Car@TB 04:25PM ET	0	1	2015
WR	Junior Hemingway	3000	Ten@KC 01:00PM ET	2.441	1	2015
RB	Patrick DiMarco	3000	NO@Atl 01:00PM ET	0.921	1	2015
TE	Andre Hardy	3000	SD@Ari 10:20PM ET	0	1	2015
TE	David Paulson	3000	Cle@Pit 01:00PM ET	0.95	1	2015
WR	Kevin Dorsey	3000	GB@Sea 08:30PM ET	0	1	2015
RB	Mikel Leshoure	3000	NYG@Det 07:10PM ET	0.3	1	2015
TE	Jake Byrne	3000	SD@Ari 10:20PM ET	0	1	2015
TE	James Hanna	3000	SF@Dal 04:25PM ET	1.206	1	2015
TE	Jake Stoneburner	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Dan Buckner	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Isaiah Pead	3000	Min@StL 01:00PM ET	2.29	1	2015
RB	George Winn	3000	NYG@Det 07:10PM ET	0	1	2015
RB	Cyrus Gray	3000	Ten@KC 01:00PM ET	0.853	1	2015
WR	Juron Criner	3000	Oak@NYJ 01:00PM ET	6.2	1	2015
WR	Tavarres King	3000	Car@TB 04:25PM ET	0	1	2015
RB	Tauren Poole	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Austin Johnson	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Patrick Edwards	3000	NYG@Det 07:10PM ET	2.4	1	2015
WR	Chris Harper	3000	GB@Sea 08:30PM ET	0	1	2015
RB	Chris Polk	3000	Jax@Phi 01:00PM ET	2.369	1	2015
WR	Tim Benford	3000	SF@Dal 04:25PM ET	0	1	2015
RB	LaMichael James	3000	SF@Dal 04:25PM ET	0.731	1	2015
WR	Brice Butler	3000	Oak@NYJ 01:00PM ET	1.93	1	2015
RB	Demitrius Bronson	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Jabin Sambrano	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Lanear Sampson	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Colin Anderson	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Cole Beasley	3000	SF@Dal 04:25PM ET	6.271	1	2015
RB	Zach Line	3000	Min@StL 01:00PM ET	0.6	1	2015
TE	Taylor Thompson	3000	Ten@KC 01:00PM ET	0.644	1	2015
TE	Gerell Robinson	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Chris Owusu	3000	Car@TB 04:25PM ET	2.711	1	2015
WR	Griff Whalen	3000	Ind@Den 08:30PM ET	7.109	1	2015
WR	Brenton Bersin	3000	Car@TB 04:25PM ET	0	1	2015
RB	Derrick Coleman	3000	GB@Sea 08:30PM ET	1.367	1	2015
TE	Cory Harkey	3000	Min@StL 01:00PM ET	2.42	1	2015
RB	Jonathan Grimes	3000	Was@Hou 01:00PM ET	12.95	1	2015
TE	Chris Pantale	3000	Oak@NYJ 01:00PM ET	0	1	2015
RB	Bradie Ewing	3000	Jax@Phi 01:00PM ET	2.45	1	2015
RB	Jeff Demps	3000	Car@TB 04:25PM ET	3.25	1	2015
WR	Frankie Hammond Jr.	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Jerrell Jackson	3000	Ten@KC 01:00PM ET	0	1	2015
WR	David Reed	3000	SF@Dal 04:25PM ET	0.171	1	2015
TE	Beau Brinkley	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Jaron Brown	3000	SD@Ari 10:20PM ET	1.938	1	2015
RB	Ryan Williams	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Chris Givens	3000	Min@StL 01:00PM ET	5.856	1	2015
WR	Jeremy Ebert	3000	NO@Atl 01:00PM ET	0.8	1	2015
WR	Tandon Doss	3000	Jax@Phi 01:00PM ET	3.633	1	2015
WR	A.J. Jenkins	3000	Ten@KC 01:00PM ET	1.488	1	2015
RB	Jewel Hampton	3000	SF@Dal 04:25PM ET	0	1	2015
RB	Michael Cox	3000	NYG@Det 07:10PM ET	0.607	1	2015
TE	Kevin McDermott	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Keshawn Martin	3000	Was@Hou 01:00PM ET	4.019	1	2015
WR	Myles White	3000	GB@Sea 08:30PM ET	2.229	1	2015
WR	DeVier Posey	3000	Was@Hou 01:00PM ET	2.179	1	2015
WR	Lamaar Thomas	3000	Jax@Phi 01:00PM ET	2.55	1	2015
RB	Davin Meggett	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Travis Benjamin	3000	Cle@Pit 01:00PM ET	3.125	1	2015
TE	D.C. Jefferson	3000	Car@TB 04:25PM ET	0	1	2015
WR	Gerrard Sheppard	3000	GB@Sea 08:30PM ET	0	1	2015
TE	Ryan Griffin	3000	Was@Hou 01:00PM ET	3.293	1	2015
TE	Chris Gragg	3000	Buf@Chi 01:00PM ET	1.811	1	2015
RB	Dennis Johnson	3000	Was@Hou 01:00PM ET	3.863	1	2015
WR	Jarius Wright	3000	Min@StL 01:00PM ET	5.463	1	2015
RB	Brandon Bolden	3000	NE@Mia 01:00PM ET	5.807	1	2015
WR	Arceto Clark	3000	GB@Sea 08:30PM ET	0	1	2015
RB	Ina Liaina	3000	GB@Sea 08:30PM ET	0	1	2015
TE	Ryan Otten	3000	SD@Ari 10:20PM ET	0	1	2015
WR	Chris Summers	3000	Buf@Chi 01:00PM ET	0	1	2015
TE	Brandon Hartson	3000	Buf@Chi 01:00PM ET	0	1	2015
TE	Luke Willson	3000	GB@Sea 08:30PM ET	2.995	1	2015
RB	Latavius Murray	3000	Oak@NYJ 01:00PM ET	0	1	2015
TE	Jason Schepler	3000	Ten@KC 01:00PM ET	0	1	2015
TE	Jordan Thompson	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Andy Cruse	3000	Min@StL 01:00PM ET	0	1	2015
RB	Jonas Gray	3000	NE@Mia 01:00PM ET	0	1	2015
TE	Josh Hill	3000	NO@Atl 01:00PM ET	1.3	1	2015
RB	Michael Zordich	3000	Car@TB 04:25PM ET	0	1	2015
WR	John Brown	3000	SD@Ari 10:20PM ET	0	1	2015
TE	Jack Doyle	3000	Ind@Den 08:30PM ET	0.506	1	2015
WR	Will Murphy	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Russell Shepard	3000	Car@TB 04:25PM ET	0	1	2015
WR	Morrell Presley	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Tori Gurley	3000	Buf@Chi 01:00PM ET	0.833	1	2015
RB	Zach Boren	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Dion Lewis	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Zurlon Tipton	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Josh Boyce	3000	NE@Mia 01:00PM ET	2.344	1	2015
RB	Waymon James	3000	Ten@KC 01:00PM ET	0	1	2015
RB	Matthew Tucker	3000	Jax@Phi 01:00PM ET	0	1	2015
RB	Mike James	3000	Car@TB 04:25PM ET	5.653	1	2015
RB	Alvester Alexander	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Marcel Jensen	3000	Jax@Phi 01:00PM ET	0	1	2015
RB	Tommy Bohanon	3000	Oak@NYJ 01:00PM ET	1.506	1	2015
RB	Toben Opurum	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Ryan Grant	3000	Was@Hou 01:00PM ET	0	1	2015
RB	Derrick Strozier	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Wilson Van Hooser	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Micah Hatfield	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Rex Burkhead	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Michael Ford	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	Geraldo Boldewijn	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Julian Jones	3000	NO@Atl 01:00PM ET	0	1	2015
RB	Benny Cunningham	3000	Min@StL 01:00PM ET	3.429	1	2015
WR	Derrick Johnson	3000	NE@Mia 01:00PM ET	0	1	2015
TE	Justin Perillo	3000	GB@Sea 08:30PM ET	0	1	2015
TE	Nick Kasa	3000	Oak@NYJ 01:00PM ET	0.494	1	2015
WR	Jaz Reynolds	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Mike Campanaro	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Josh Harris	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Jacob Pedersen	3000	NO@Atl 01:00PM ET	0	1	2015
TE	Brian Wozniak	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Damian Copeland	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Tyrone Walker	3000	Min@StL 01:00PM ET	0	1	2015
RB	Kyle Juszczyk	3000	Cin@Bal 01:00PM ET	0	1	2015
WR	Erik Lora	3000	Min@StL 01:00PM ET	0	1	2015
WR	Alec Lemon	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Skye Dawson	3000	Car@TB 04:25PM ET	0.671	1	2015
WR	Ryan Spadola	3000	NE@Mia 01:00PM ET	0	1	2015
TE	James Oboh	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Ryan Hewitt	3000	Cin@Bal 01:00PM ET	0	1	2015
TE	Jordan Najvar	3000	SF@Dal 04:25PM ET	0	1	2015
RB	Stepfan Taylor	3000	SD@Ari 10:20PM ET	1.663	1	2015
TE	Levine Toilolo	3000	NO@Atl 01:00PM ET	1.781	1	2015
WR	Tevin Reese	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Jerodis Williams	3000	Ind@Den 08:30PM ET	0	1	2015
TE	Justice Cunningham	3000	Min@StL 01:00PM ET	1.4	1	2015
WR	Andrew Peacock	3000	NYG@Det 07:10PM ET	0	1	2015
RB	Ray Agnew	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Justin Jones	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Reese Wiggins	3000	NYG@Det 07:10PM ET	0	1	2015
RB	Branden Oliver	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Dri Archer	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Chris Coyle	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Marquise Goodwin	3000	Buf@Chi 01:00PM ET	5.3	1	2015
WR	Cody Hoffman	3000	Was@Hou 01:00PM ET	0	1	2015
RB	Alex Green	3000	Oak@NYJ 01:00PM ET	0.485	1	2015
RB	Kerwynn Williams	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Jordan Lynch	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	Rico Richardson	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Emory Blake	3000	Min@StL 01:00PM ET	0	1	2015
WR	Steve Hull	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Kevin Norwood	3000	GB@Sea 08:30PM ET	0	1	2015
RB	Mike Gillislee	3000	NE@Mia 01:00PM ET	0.7	1	2015
TE	Ted Bolser	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Quintin Payton	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Shaq Evans	3000	Oak@NYJ 01:00PM ET	0	1	2015
RB	Jordan Hall	3000	Cle@Pit 01:00PM ET	0	1	2015
WR	Randall Carroll	3000	GB@Sea 08:30PM ET	0	1	2015
RB	Damien Thigpen	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Tim Flanders	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Tramaine Thompson	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Rodney Smith	3000	Min@StL 01:00PM ET	0	1	2015
RB	Chris Thompson	3000	Was@Hou 01:00PM ET	0	1	2015
RB	Cierre Wood	3000	Cin@Bal 01:00PM ET	0.3	1	2015
RB	Trey Watts	3000	Min@StL 01:00PM ET	0	1	2015
TE	Alex Bayer	3000	Min@StL 01:00PM ET	0	1	2015
WR	Alex Gillett	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Eric Page	3000	Car@TB 04:25PM ET	0.638	1	2015
WR	Jerry Rice Jr.	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Josh Lenz	3000	Ind@Den 08:30PM ET	0	1	2015
WR	T.J. Moe	3000	Min@StL 01:00PM ET	0	1	2015
WR	L'Damian Washington	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Ricardo Lockette	3000	GB@Sea 08:30PM ET	1.445	1	2015
WR	EZ Nwachukwu	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Jeremy Gallon	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Denard Robinson	3000	Jax@Phi 01:00PM ET	0.288	1	2015
RB	Fitzgerald Toussaint	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Edwin Baker	3000	Cle@Pit 01:00PM ET	14.267	1	2015
WR	Bennie Fowler	3000	Ind@Den 08:30PM ET	0	1	2015
TE	Dion Sims	3000	NE@Mia 01:00PM ET	1.013	1	2015
WR	Jared Abbrederis	3000	GB@Sea 08:30PM ET	0	1	2015
TE	Blake Annen	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Nick Williams	3000	Was@Hou 01:00PM ET	1.3	1	2015
WR	Devin Street	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Mark Harrison	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Quron Pratt	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Cobi Hamilton	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Ronnie Wingo	3000	Buf@Chi 01:00PM ET	0.8	1	2015
WR	Marlon Brown	3000	Cin@Bal 01:00PM ET	10.371	1	2015
WR	Rantavious Wooten	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Chris Matthews	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Chad Bumphis	3000	Jax@Phi 01:00PM ET	0	1	2015
RB	LaDarius Perkins	3000	GB@Sea 08:30PM ET	0	1	2015
RB	Ronnie Hillman	3000	Ind@Den 08:30PM ET	5.07	1	2015
WR	Colin Lockett	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Chad Young	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Chandler Jones	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Brandon Bostick	3000	GB@Sea 08:30PM ET	2.273	1	2015
WR	Kamar Jorden	3000	Min@StL 01:00PM ET	0	1	2015
WR	Stedman Bailey	3000	Min@StL 01:00PM ET	3.056	1	2015
WR	Jace Davis	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Daniel Thomas	3000	NE@Mia 01:00PM ET	6.727	1	2015
WR	Justin Brown	3000	Cle@Pit 01:00PM ET	0	1	2015
WR	Greg Hardin	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Travis Harvey	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Justin Veltung	3000	Min@StL 01:00PM ET	0	1	2015
TE	Ian Thompson	3000	Car@TB 04:25PM ET	0	1	2015
RB	Karl Williams	3000	Oak@NYJ 01:00PM ET	0	1	2015
RB	Miguel Maysonet	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Rob Blanchflower	3000	Cle@Pit 01:00PM ET	0	1	2015
WR	Charles Hawkins	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Cole Stanford	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Kevin Ozier	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Stephen Campbell	3000	Was@Hou 01:00PM ET	0	1	2015
RB	Alfred Blue	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Kadron Boone	3000	Jax@Phi 01:00PM ET	0	1	2015
RB	J.C. Copeland	3000	SF@Dal 04:25PM ET	0	1	2015
TE	Nic Jacobs	3000	NO@Atl 01:00PM ET	0	1	2015
RB	Spencer Ware	3000	GB@Sea 08:30PM ET	0.5	1	2015
WR	James Wright	3000	Cin@Bal 01:00PM ET	0	1	2015
TE	Brandon Williams	3000	Car@TB 04:25PM ET	0	1	2015
WR	Quincy Enunwa	3000	Oak@NYJ 01:00PM ET	0	1	2015
TE	Cooper Helfet	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Allen Hurns	3000	Jax@Phi 01:00PM ET	0	1	2015
RB	Storm Johnson	3000	Jax@Phi 01:00PM ET	0	1	2015
TE	Crockett Gillmore	3000	Cin@Bal 01:00PM ET	0	1	2015
TE	Trey Burton	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Solomon Patton	3000	Car@TB 04:25PM ET	0	1	2015
RB	Rajion Neal	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Da'Rick Rogers	3000	Ind@Den 08:30PM ET	7.686	1	2015
RB	Brennan Clay	3000	Ind@Den 08:30PM ET	0	1	2015
RB	Roy Finch	3000	NE@Mia 01:00PM ET	0	1	2015
RB	Trey Millard	3000	SF@Dal 04:25PM ET	0	1	2015
RB	Vick Ballard	3000	Ind@Den 08:30PM ET	6.8	1	2015
TE	Eric Waters	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Silas Redd	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Ryan Lankford	3000	Ind@Den 08:30PM ET	0	1	2015
RB	Jay Prosch	3000	Was@Hou 01:00PM ET	0	1	2015
WR	TJ Jones	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Anthony McClung	3000	Was@Hou 01:00PM ET	0	1	2015
TE	Je'Ron Hamm	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Darryl Surgent	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Kain Colter	3000	Min@StL 01:00PM ET	0	1	2015
WR	Rashad Lawrence	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Andy Tanner	3000	NO@Atl 01:00PM ET	0	1	2015
RB	Orleans Darkwa	3000	NE@Mia 01:00PM ET	0	1	2015
RB	Roosevelt Nix	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Greg Jenkins	3000	Oak@NYJ 01:00PM ET	-0.167	1	2015
RB	Joseph Randle	3000	SF@Dal 04:25PM ET	3.269	1	2015
RB	Tim Cornett	3000	Was@Hou 01:00PM ET	0	1	2015
TE	Erik Swoope	3000	Ind@Den 08:30PM ET	0	1	2015
RB	Zach Bauman	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Marcus Lattimore	3000	SF@Dal 04:25PM ET	0	1	2015
TE	Cameron Brate	3000	Car@TB 04:25PM ET	0	1	2015
TE	Tyler Ott	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Ace Sanders	3000	Jax@Phi 01:00PM ET	7.376	1	2015
WR	Mike Willie	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Henry Josey	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Marcus Lucas	3000	Car@TB 04:25PM ET	0	1	2015
RB	Chad Abram	3000	NYG@Det 07:10PM ET	0	1	2015
TE	Chase Ford	3000	Min@StL 01:00PM ET	2.7	1	2015
RB	Maurice Hagens	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Josh Bellamy	3000	Buf@Chi 01:00PM ET	0	1	2015
RB	Senorise Perry	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	Jordan Harris	3000	Min@StL 01:00PM ET	0	1	2015
TE	Emmanuel Ogbuehi	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Lache Seastrunk	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Julian Horton	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Javontee Herndon	3000	SD@Ari 10:20PM ET	0	1	2015
WR	Chris Boyd	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Jonathan Krause	3000	Cle@Pit 01:00PM ET	0	1	2015
WR	Josh Huff	3000	Jax@Phi 01:00PM ET	0	1	2015
TE	Xavier Grimble	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Mike Davis	3000	Oak@NYJ 01:00PM ET	0	1	2015
RB	Juwan Thompson	3000	Ind@Den 08:30PM ET	0	1	2015
TE	Asante Cleveland	3000	SF@Dal 04:25PM ET	0	1	2015
RB	James White	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Brandon Coleman	3000	NO@Atl 01:00PM ET	0	1	2015
RB	Ben Malena	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Eric Thomas	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Albert Wilson	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Matt Hazel	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Rishard Matthews	3000	NE@Mia 01:00PM ET	6.3	1	2015
WR	Kevin Smith	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Clyde Gates	3000	Oak@NYJ 01:00PM ET	4.033	1	2015
WR	Corey Fuller	3000	NYG@Det 07:10PM ET	0	1	2015
TE	Michael Higgins	3000	Min@StL 01:00PM ET	0	1	2015
RB	Jerome Smith	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Jeremy Johnson	3000	Cin@Bal 01:00PM ET	0	1	2015
WR	Bernard Reedy	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Brelan Chancellor	3000	SD@Ari 10:20PM ET	0	1	2015
RB	Jamize Olawale	3000	Oak@NYJ 01:00PM ET	0.869	1	2015
RB	Kadeem Jones	3000	Min@StL 01:00PM ET	0	1	2015
WR	Marquess Wilson	3000	Buf@Chi 01:00PM ET	0.33	1	2015
WR	Paul Richardson	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Robert Herron	3000	Car@TB 04:25PM ET	0	1	2015
TE	Harold Hoskins	3000	NE@Mia 01:00PM ET	0	1	2015
WR	Donte Foster	3000	Min@StL 01:00PM ET	0	1	2015
WR	Isaiah Burse	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Jalen Saunders	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Freddie Martino	3000	NO@Atl 01:00PM ET	0	1	2015
TE	Rashaun Allen	3000	GB@Sea 08:30PM ET	0	1	2015
RB	Jerick McKinnon	3000	Min@StL 01:00PM ET	0	1	2015
WR	Brian Tyms	3000	NE@Mia 01:00PM ET	0.457	1	2015
RB	Antonio Andrews	3000	Ten@KC 01:00PM ET	0	1	2015
TE	Scott Simonson	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Chris Hogan	3000	Buf@Chi 01:00PM ET	1.144	1	2015
WR	Saalim Hakim	3000	Oak@NYJ 01:00PM ET	-0.1	1	2015
WR	Philly Brown	3000	Car@TB 04:25PM ET	0	1	2015
TE	Jake Murphy	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Michael Preston	3000	Ten@KC 01:00PM ET	2.957	1	2015
WR	Jarvis Landry	3000	NE@Mia 01:00PM ET	0	1	2015
RB	Kapri Bibbs	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Willie Snead	3000	Cle@Pit 01:00PM ET	0	1	2015
TE	Anthony Denham	3000	Was@Hou 01:00PM ET	0	1	2015
TE	A.C. Leonard	3000	Min@StL 01:00PM ET	0	1	2015
WR	Quinton Patton	3000	SF@Dal 04:25PM ET	1.489	1	2015
WR	Travis Labhart	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Dale Moss	3000	Buf@Chi 01:00PM ET	0	1	2015
RB	Steven Miller	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Greg Wilson	3000	Ind@Den 08:30PM ET	0	1	2015
TE	Demetrius Harris	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Austin Franklin	3000	Min@StL 01:00PM ET	0	1	2015
WR	Bruce Ellington	3000	SF@Dal 04:25PM ET	0	1	2015
TE	Mason Brodine	3000	Min@StL 01:00PM ET	0	1	2015
RB	James Wilder Jr.	3000	Cin@Bal 01:00PM ET	0	1	2015
RB	Isaiah Crowell	3000	Cle@Pit 01:00PM ET	0	1	2015
WR	Chuck Jacobs	3000	SF@Dal 04:25PM ET	0	1	2015
WR	Jeremy Butler	3000	Cin@Bal 01:00PM ET	0	1	2015
TE	Richard Rodgers	3000	GB@Sea 08:30PM ET	0	1	2015
RB	De'Anthony Thomas	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Rahsaan Vaughn	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Cody Latimer	3000	Ind@Den 08:30PM ET	0	1	2015
RB	Darrin Reaves	3000	Car@TB 04:25PM ET	0	1	2015
RB	George Atkinson III	3000	Oak@NYJ 01:00PM ET	0	1	2015
WR	Davante Adams	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Lee Doss	3000	Was@Hou 01:00PM ET	0	1	2015
WR	Brittan Golden	3000	SD@Ari 10:20PM ET	3.52	1	2015
RB	Daryl Richardson	3000	Oak@NYJ 01:00PM ET	5.95	1	2015
TE	Phillip Supernaw	3000	Cin@Bal 01:00PM ET	0	1	2015
TE	Bryce Davis	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Marion Grice	3000	SD@Ari 10:20PM ET	0	1	2015
WR	Deon Anthony	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Derel Walker	3000	Ten@KC 01:00PM ET	0	1	2015
TE	Derek Carrier	3000	SF@Dal 04:25PM ET	0	1	2015
TE	Dominique Jones	3000	Buf@Chi 01:00PM ET	0.675	1	2015
WR	Lacoltan Bester	3000	Was@Hou 01:00PM ET	0	1	2015
RB	Damien Williams	3000	NE@Mia 01:00PM ET	0	1	2015
TE	Brian Leonhardt	3000	Oak@NYJ 01:00PM ET	0	1	2015
RB	Lorenzo Taliaferro	3000	Cin@Bal 01:00PM ET	0	1	2015
WR	Seth Roberts	3000	Oak@NYJ 01:00PM ET	0	1	2015
RB	Cameron White	3000	Ind@Den 08:30PM ET	0	1	2015
RB	Michael Hill	3000	GB@Sea 08:30PM ET	0.825	1	2015
WR	Adam Thielen	3000	Min@StL 01:00PM ET	0	1	2015
RB	Khiry Robinson	3000	NO@Atl 01:00PM ET	3.908	1	2015
TE	Adam Schiltz	3000	Ten@KC 01:00PM ET	0	1	2015
WR	Taylor Gabriel	3000	Cle@Pit 01:00PM ET	0	1	2015
RB	Charcandrick West	3000	Ten@KC 01:00PM ET	0	1	2015
RB	Terrance Cobb	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Corey Washington	3000	NYG@Det 07:10PM ET	0	1	2015
WR	Jeff Janis	3000	GB@Sea 08:30PM ET	0	1	2015
WR	Torrence Allen	3000	SD@Ari 10:20PM ET	0	1	2015
TE	Reggie Jordan	3000	Jax@Phi 01:00PM ET	0	1	2015
TE	Michael Flacco	3000	SD@Ari 10:20PM ET	0	1	2015
WR	Greg Moore	3000	Ind@Den 08:30PM ET	0	1	2015
WR	Seantavius Jones	3000	NO@Atl 01:00PM ET	0	1	2015
WR	Nathan Slaughter	3000	Jax@Phi 01:00PM ET	0	1	2015
WR	Caleb Holley	3000	Buf@Chi 01:00PM ET	0	1	2015
WR	C.J. Goodwin	3000	Cle@Pit 01:00PM ET	0	1	2015
DST	Falcons	2900	NO@Atl 01:00PM ET	5.25	1	2015
DST	Redskins	2900	Was@Hou 01:00PM ET	6.375	1	2015
DST	Browns	2800	Cle@Pit 01:00PM ET	7.125	1	2015
DST	Saints	2800	NO@Atl 01:00PM ET	6.333	1	2015
DST	Vikings	2700	Min@StL 01:00PM ET	5.625	1	2015
DST	Colts	2600	Ind@Den 08:30PM ET	7.667	1	2015
DST	Cowboys	1900	SF@Dal 04:25PM ET	7.688	1	2015
DST	Jaguars	1500	Jax@Phi 01:00PM ET	5.438	1	2015
\.


--
-- Data for Name: espn_projections; Type: TABLE DATA; Schema: public; Owner: Raymond
--

COPY espn_projections (espn_id, name, p_c, p_a, p_yds, p_tds, p_ints, r_avg, r_tds, r_yds, rec_catches, rec_yds, rec_tds, total, team, "position", status, dk_total, week, year) FROM stdin;
17704	Cameron Artis-Payne	0	0	0	0	0	3.9	0.1	15.6	0.3	2.8	0	1.5	CAR	RB		2.7399999999999998	1	2015
17136	Dontrelle Inman	0	0	0	0	0	0	0	0	0.8	12.1	0.1	1.4	SD	WR		2.61	1	2015
17475	Clive Walford	0	0	0	0	0	0	0	0	0.9	10.9	0.1	1.3	OAK	TE		2.5900000000000003	1	2015
10521	Matt Spaeth	0	0	0	0	0	0	0	0	0.8	9.1	0.1	0.4	PIT	TE		2.31	1	2015
14880	Kirk Cousins	23.3	37.3	272	1.2	1.2	1.3	0	5	0	0	0	12.3	WSH	QB		14.98	1	2015
11467	Justin Forsett	0	0	0	0	0	13.8	0.3	57.7	3.9	30	0.1	9.4	BAL	RB	P	15.07	1	2015
11128	Matt Moore	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	MIA	QB		0.052000000000000005	1	2015
12471	Chase Daniel	0.1	0.2	1.1	0	0	0	0	0	0	0	0	0	PHI	QB		0.044000000000000004	1	2015
15168	Case Keenum	0.1	0.2	1.1	0	0	0	0	0	0	0	0	0	STL	QB		0.044000000000000004	1	2015
10467	Dwayne Bowe	0	0	0	0	0	0	0	0	0	0	0	0	CLE	WR		0	1	2015
13203	C.J. Spiller	0	0	0	0	0	0	0	0	0	0	0	0	NO	RB	P	0	1	2015
17370	Freddie Martino	0	0	0	0	0	0	0	0	0	0	0	0	PHI	WR		0	1	2015
17450	Randall Telfer	0	0	0	0	0	0	0	0	0	0	0	0	CLE	TE		0	1	2015
17527	Dylan Thompson	0	0	0	0	0	0	0	0	0	0	0	0	SF	QB		0	1	2015
17578	David Cobb	0	0	0	0	0	0	0	0	0	0	0	0	TEN	RB		0	1	2015
17649	Kennard Backman	0	0	0	0	0	0	0	0	0	0	0	0	GB	TE		0	1	2015
15349	Cole Beasley	0	0	0	0	0	0	0	0	3.1	37.5	0.3	4.6	DAL	WR		8.649999999999999	1	2015
14021	Leonard Hankerson	0	0	0	0	0	0	0	0	3.1	39.3	0.2	4.4	BUF	WR		8.23	1	2015
60017	Patriots	0	0	0	0	0	0	0	0	0	0	0	4.3		DST		4.3	1	2015
15072	Marvin Jones	0	0	0	0	0	0.2	0	1	2.7	35.6	0.2	4.3	DET	WR		7.5600000000000005	1	2015
60008	Lions	0	0	0	0	0	0	0	0	0	0	0	6.5		DST		6.5	1	2015
16794	Bishop Sankey	0	0	0	0	0	11.7	0.3	46.7	1.2	9.4	0	5.8	TEN	RB		8.61	1	2015
14221	Doug Baldwin	0	0	0	0	0	0.1	0	0.9	3.7	49.9	0.3	5.6	SEA	WR		10.580000000000002	1	2015
16016	Kenny Stills	0	0	0	0	0	0.3	0	1.7	3	43	0.2	5.3	MIA	WR		8.67	1	2015
16026	Quinton Patton	0	0	0	0	0	0.4	0	2.7	1.1	15.4	0.1	1.4	SF	WR		3.5100000000000002	1	2015
14360	Matt Asiata	0	0	0	0	0	1.3	0.1	4.9	1	7.2	0	0.6	MIN	RB		2.8099999999999996	1	2015
12705	Brandon Myers	0	0	0	0	0	0	0	0	0.9	9.8	0.1	0.4	TB	TE		2.4800000000000004	1	2015
60029	Panthers	0	0	0	0	0	0	0	0	0	0	0	12.4		DST		12.4	1	2015
11278	Matt Forte	0	0	0	0	0	16	0	68	4	40	0	9.9	NYJ	RB		14.8	1	2015
3609	Josh McCown	16.6	29.1	187.1	0.7	0.9	2.8	0.1	13.9	0	0	0	9	CLE	QB	P	11.373999999999999	1	2015
14037	Ryan Mallett	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	BAL	QB		0.048	1	2015
12497	Arian Foster	0	0	0	0	0	0	0	0	0	0	0	0	HOU	RB	Q	0	1	2015
17392	Justin Perillo	0	0	0	0	0	0	0	0	0	0	0	0	GB	TE		0	1	2015
17533	Sean Mannion	0	0	0	0	0	0	0	0	0	0	0	0	STL	QB		0	1	2015
17683	Todd Gurley	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	1	2015
17695	Matt Jones	0	0	0	0	0	6.4	0.2	20.6	2.2	17.7	0.1	4.4	WSH	RB	P	7.83	1	2015
10453	Ted Ginn Jr.	0	0	0	0	0	0.3	0	2	2.2	30.9	0.2	4.3	CAR	WR		6.69	1	2015
12570	Darrius Heyward-Bey	0	0	0	0	0	0	0	0	1.9	25.1	0.2	2.9	PIT	WR		5.61	1	2015
14083	Bruce Miller	0	0	0	0	0	0.5	0	1.7	0.7	6	0	0.3	SF	RB		1.4699999999999998	1	2015
12524	Marcus Thigpen	0	0	0	0	0	0	0	0	0	0	0	0.3	BUF	RB		0.3	1	2015
10147	Miles Austin	0	0	0	0	0	0	0	0	0.4	5.4	0	0.2	PHI	WR		0.9400000000000001	1	2015
17137	Jeff Janis	0	0	0	0	0	0	0	0	0.3	4.6	0	0.2	GB	WR		0.76	1	2015
60001	Falcons	0	0	0	0	0	0	0	0	0	0	0	0.7		DST		0.7	1	2015
14223	Armon Binns	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	1	2015
14904	Ladarius Green	0	0	0	0	0	0	0	0	0	0	0	0	PIT	TE	P	0	1	2015
15189	Nathan Palmer	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	1	2015
5209	Tony Romo	20.6	31.1	263.6	2.3	0.6	1.7	0.1	4.5	0	0	0	17.6	DAL	QB	Q	20.194	1	2015
4459	Carson Palmer	22.4	34.6	264.7	1.9	0.9	1.5	0.1	2.5	0	0	0	16.2	ARI	QB	P	18.138	1	2015
13982	Julio Jones	0	0	0	0	0	0	0	1	8	99	1	14.9	ATL	WR		24	1	2015
16724	Blake Bortles	21.2	33.4	244.9	1.2	0.9	3.8	0.1	22.9	0	0	0	14.1	JAX	QB		16.586000000000002	1	2015
15397	Saalim Hakim	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	1	2015
15855	Christine Michael	0	0	0	0	0	0	0	0	0	0	0	0	SEA	RB		0	1	2015
15991	Justice Cunningham	0	0	0	0	0	0	0	0	0	0	0	0	STL	TE		0	1	2015
16140	Ryan Griffin	0	0	0	0	0	0	0	0	0	0	0	0	TB	QB		0	1	2015
16418	Zach Sudfeld	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	TE	Q	0	1	2015
17736	John Crockett	0	0	0	0	0	0	0	0	0	0	0	0	GB	RB		0	1	2015
17753	E.J. Bibbs	0	0	0	0	0	0	0	0	0	0	0	0	CLE	TE		0	1	2015
14918	Jarius Wright	0	0	0	0	0	0.3	0	1.7	1.9	24.4	0.1	2.8	MIN	WR		5.109999999999999	1	2015
16945	De'Anthony Thomas	0	0	0	0	0	0.9	0	5.7	1.8	13.6	0.1	2.1	KC	RB		4.33	1	2015
13371	Garrett Graham	0	0	0	0	0	0	0	0	1.8	20	0.1	1.8	HOU	TE		4.4	1	2015
17734	Chris Matthews	0	0	0	0	0	0	0	0	0.8	11.6	0.1	1.5	BAL	WR		2.56	1	2015
14167	Taiwan Jones	0	0	0	0	0	2.6	0.1	10	0.8	6.4	0	1.4	OAK	RB		3.0400000000000005	1	2015
14890	LaMike James	0	0	0	0	0	1.8	0	7.3	0.8	6.8	0	0.4	MIA	RB		2.21	1	2015
13587	Chris Ivory	0	0	0	0	0	16.8	0.4	73.5	1.7	12.3	0.1	10.4	JAX	RB		13.28	1	2015
8475	Vincent Jackson	0	0	0	0	0	0	0	0	4.7	73.3	0.4	9.2	TB	WR	P	14.430000000000001	1	2015
8559	Dan Orlovsky	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	DET	QB		0.052000000000000005	1	2015
5362	Antonio Gates	0	0	0	0	0	0	0	0	0	0	0	0	SD	TE		0	1	2015
13213	LeGarrette Blount	0	0	0	0	0	0	0	0	0	0	0	0	NE	RB		0	1	2015
17483	Neal Sterling	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR		0	1	2015
17613	Brett Hundley	0	0	0	0	0	0	0	0	0	0	0	0	GB	QB		0	1	2015
14054	Kyle Rudolph	0	0	0	0	0	0	0	0	3.2	35.1	0.3	4.5	MIN	TE		8.510000000000002	1	2015
14129	Bilal Powell	0	0	0	0	0	7	0.2	28.4	2.5	19.7	0.1	4.3	NYJ	RB		9.11	1	2015
17181	Frankie Hammond	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	1	2015
17210	Glenn Winston	0	0	0	0	0	0	0	0	0	0	0	0	CLE	RB		0	1	2015
13224	Marcus Easley	0	0	0	0	0	0	0	0	0.2	2.1	0	0.1	BUF	WR	Q	0.41000000000000003	1	2015
15360	Griff Whalen	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	MIA	WR		0.24	1	2015
17564	C.J. Uzomah	0	0	0	0	0	0	0	0	0.1	1.2	0	0	CIN	TE		0.22	1	2015
8627	Derek Anderson	0.1	0.2	1.2	0	0	0.2	0	0.5	0	0	0	0	CAR	QB		0.098	1	2015
14198	Dion Lewis	0	0	0	0	0	9.9	0.3	42	2.5	20.8	0.1	8.6	NE	RB	Q	11.18	1	2015
17678	T.J. Yeldon	0	0	0	0	0	12.8	0.4	55.1	1.8	14.6	0.1	8.3	JAX	RB		11.770000000000001	1	2015
60009	Packers	0	0	0	0	0	0	0	0	0	0	0	8		DST		8	1	2015
2148	Sebastian Janikowski	0	0	0	0	0	0	0	0	0	0	0	7.8	OAK	K		7.8	1	2015
13414	Anthony McCoy	0	0	0	0	0	0	0	0	0	0	0	0	SEA	TE		0	1	2015
14080	Ricky Stanzi	0	0	0	0	0	0	0	0	0	0	0	0	NYG	QB		0	1	2015
14594	McLeod Bethel-Thompson	0	0	0	0	0	0	0	0	0	0	0	0	MIA	QB		0	1	2015
16797	Troy Niklas	0	0	0	0	0	0	0	0	0.5	5.4	0.1	0.3	ARI	TE		1.6400000000000001	1	2015
17444	MyCole Pruitt	0	0	0	0	0	0	0	0	0.6	7.4	0	0.3	MIN	TE		1.3399999999999999	1	2015
15564	Brittan Golden	0	0	0	0	0	0	0	0	0	0	0	0.3	ARI	WR		0.3	1	2015
15971	Rex Burkhead	0	0	0	0	0	0.3	0	1.2	0.7	5.7	0	0.2	CIN	RB		1.3900000000000001	1	2015
14906	James Hanna	0	0	0	0	0	0	0	0	0.2	2.4	0	0.2	DAL	TE		0.44	1	2015
12597	Brandon Tate	0	0	0	0	0	0	0	0	0.1	1.2	0	0.1	CIN	WR		0.22	1	2015
17507	Keith Mumphery	0	0	0	0	0	0	0	0	0.2	2.4	0	0.1	HOU	WR		0.44	1	2015
15966	Chris Thompson	0	0	0	0	0	1	0	3.6	0.6	4.8	0	0.2	WSH	RB	P	1.44	1	2015
17135	Devin Street	0	0	0	0	0	0	0	0	0.2	2.9	0	0.1	DAL	WR		0.49	1	2015
9400	Lance Moore	0	0	0	0	0	0	0	0	0.2	2.8	0	0.1	DET	WR		0.48	1	2015
16323	MarQueis Gray	0	0	0	0	0	0	0	0	0.5	6.2	0.1	0.3	BUF	TE		1.7200000000000002	1	2015
17186	Austin Johnson	0	0	0	0	0	0.2	0	0.7	0.6	5.3	0	0.3	NO	RB		1.2	1	2015
11392	Kellen Davis	0	0	0	0	0	0	0	0	0.6	6.3	0	0.2	NYJ	TE		1.23	1	2015
17607	Darren Waller	0	0	0	0	0	0	0	0	0.4	5.5	0	0.2	BAL	WR		0.9500000000000001	1	2015
17516	Jalston Fowler	0	0	0	0	0	0.5	0	1.5	0.2	2.1	0	0.2	TEN	RB		0.56	1	2015
14215	Lee Smith	0	0	0	0	0	0	0	0	0.5	5.3	0	0.2	OAK	TE		1.03	1	2015
15003	Rhett Ellison	0	0	0	0	0	0	0	0	0.2	2.5	0	0.1	MIN	TE	Q	0.45	1	2015
13536	Joique Bell	0	0	0	0	0	10.8	0.3	41.2	1.6	12.3	0	7	DET	RB		8.75	1	2015
16836	Jared Abbrederis	0	0	0	0	0	0	0	0	0	0	0	0	GB	WR		0	1	2015
17141	Kapri Bibbs	0	0	0	0	0	0	0	0	0	0	0	0	DEN	RB		0	1	2015
17183	Trey Watts	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	1	2015
17212	Ray Agnew	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB		0	1	2015
17759	Jerome Cunningham	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	NYG	TE		0.24	1	2015
17189	Demetrius Harris	0	0	0	0	0	0	0	0	0.1	1.2	0	0	KC	TE		0.22	1	2015
14878	Brandon Weeden	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	HOU	QB		0.048	1	2015
11122	Matt Prater	0	0	0	0	0	0	0	0	0	0	0	8.3	DET	K		8.3	1	2015
10636	Mason Crosby	0	0	0	0	0	0	0	0	0	0	0	8.1	GB	K		8.1	1	2015
15873	Markus Wheaton	0	0	0	0	0	0.3	0	2.2	4.4	61.2	0.3	7.9	PIT	WR		12.54	1	2015
13945	Tyler Clutts	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB		0	1	2015
14204	Julius Thomas	0	0	0	0	0	0	0	0	0	0	0	0	JAX	TE		0	1	2015
14894	Robert Turbin	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	1	2015
15130	Collin Mooney	0	0	0	0	0	0	0	0	0	0	0	0	ATL	RB		0	1	2015
2580	Drew Brees	26.8	41.3	325.6	1.9	0.9	1.6	0.1	2.5	0	0	0	18.6	NO	QB	P	23.574000000000005	1	2015
13197	Sam Bradford	21.9	35.3	255.3	1.9	0.7	2.6	0.1	7.5	0	0	0	16.2	PHI	QB		18.462	1	2015
15848	Eddie Lacy	0	0	0	0	0	18	1	82	3	19	0	14.8	GB	RB		19.099999999999998	1	2015
12483	Matthew Stafford	22.9	37.1	265.5	1.4	0.8	2.3	0.1	7.2	0	0	0	14.1	DET	QB		16.74	1	2015
15407	Matt Simms	0	0	0	0	0	0	0	0	0	0	0	0	ATL	QB		0	1	2015
15948	Matt Barkley	0	0	0	0	0	0	0	0	0	0	0	0	ARI	QB		0	1	2015
16330	Joseph Fauria	0	0	0	0	0	0	0	0	0	0	0	0	ARI	TE		0	1	2015
17751	Bennie Fowler	0	0	0	0	0	0	0	0	0	0	0	0	DEN	WR		0	1	2015
16946	Bruce Ellington	0	0	0	0	0	0.1	0	0.9	1.8	21.2	0.1	2.5	SF	WR		4.609999999999999	1	2015
10195	Fred Jackson	0	0	0	0	0	4.8	0.1	19.8	0.8	6.8	0	2	SEA	RB		4.06	1	2015
13228	Jermaine Gresham	0	0	0	0	0	0	0	0	1.6	16.7	0.1	1.9	ARI	TE		3.87	1	2015
15974	Dion Sims	0	0	0	0	0	0	0	0	1.4	15.1	0.1	1.7	MIA	TE		3.5100000000000002	1	2015
16779	Josh Huff	0	0	0	0	0	0	0	0	0.8	10.1	0.1	1.6	PHI	WR		2.41	1	2015
15807	Cordarrelle Patterson	0	0	0	0	0	0.5	0	3.4	0.9	11.1	0.1	1.6	MIN	WR		2.9499999999999997	1	2015
14193	Jacquizz Rodgers	0	0	0	0	0	2.6	0.1	10.7	0.7	5.9	0	1.5	CHI	RB		2.96	1	2015
13212	Boobie Dixon	0	0	0	0	0	1.7	0.1	6.7	0.3	2.5	0	0.5	BUF	RB		1.82	1	2015
14053	Randall Cobb	0	0	0	0	0	1	0	7	6	79	1	13	GB	WR	P	20.6	1	2015
15893	Andre Ellington	0	0	0	0	0	12	0.4	50.2	3.1	28.8	0.1	9.9	ARI	RB		14.000000000000002	1	2015
16731	Brandin Cooks	0	0	0	0	0	0.7	0	4.6	5.8	79	0.4	9.3	NO	WR		16.560000000000002	1	2015
16763	Jordan Matthews	0	0	0	0	0	0	0	0	4.9	61.4	0.5	8.8	PHI	WR		14.04	1	2015
8644	Matt Cassel	0.1	0.2	1	0	0	0	0	0	0	0	0	0	TEN	QB		0.04	1	2015
8544	Darren Sproles	0	0	0	0	0	3.2	0.1	15.1	2.8	23.2	0.1	4.3	PHI	RB		7.83	1	2015
60030	Jaguars	0	0	0	0	0	0	0	0	0	0	0	6.4		DST		6.4	1	2015
10522	James Jones	0	0	0	0	0	0	0	0	2.7	40.4	0.3	5.8	GB	WR		8.54	1	2015
13204	Ryan Mathews	0	0	0	0	0	8.9	0.2	40.5	0.8	6.3	0	5.5	PHI	RB	P	6.68	1	2015
13226	Andre Roberts	0	0	0	0	0	0.1	0	0.8	2.5	31.3	0.2	4.1	WSH	WR	P	6.91	1	2015
17442	David Johnson	0	0	0	0	0	4.4	0.2	19.2	1.9	15.5	0.1	3.5	ARI	RB		7.17	1	2015
12550	John Phillips	0	0	0	0	0	0	0	0	1.8	21.2	0.2	3.1	SD	TE		5.12	1	2015
17755	Lucky Whitehead	0	0	0	0	0	0	0	0	0.1	1.5	0	0.3	DAL	WR		0.25	1	2015
17745	Jarryd Hayne	0	0	0	0	0	0.7	0	2.7	0.1	0.9	0	0.3	SF	RB		0.45999999999999996	1	2015
14518	Shaun Draughn	0	0	0	0	0	1.5	0	5.4	0	0	0	0.3	SF	RB	P	0.54	1	2015
17384	Charcandrick West	0	0	0	0	0	1.1	0	4.4	0.1	1	0	0.2	KC	RB		0.64	1	2015
14289	Kyle Miller	0	0	0	0	0	0	0	0	0.3	4	0	0.2	SD	TE		0.7	1	2015
17356	Nic Jacobs	0	0	0	0	0	0	0	0	0.4	5.1	0	0.2	JAX	TE		0.91	1	2015
12510	Jorvorskie Lane	0	0	0	0	0	0.3	0	0.8	0.4	3.2	0	0.2	TB	RB		0.8	1	2015
15226	Cory Harkey	0	0	0	0	0	0	0	0	0.5	4	0	0.1	STL	TE		0.9	1	2015
15204	Garrett Celek	0	0	0	0	0	0	0	0	0.2	2.3	0	0.1	SF	TE		0.43	1	2015
16838	Tyler Gaffney	0	0	0	0	0	0	0	0	0	0	0	0	NE	RB	P	0	1	2015
17146	Marcus Harris	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	1	2015
17193	James Wright	0	0	0	0	0	0	0	0	0	0	0	0	CIN	WR		0	1	2015
17342	Fitzgerald Toussaint	0	0	0	0	0	0	0	0	0	0	0	0	PIT	RB		0	1	2015
17195	Trey Burton	0	0	0	0	0	0	0	0	0.1	1.3	0	0.1	PHI	TE		0.23	1	2015
17757	Nikita Whitlock	0	0	0	0	0	0.2	0	0.8	0	0	0	0.1	NYG	RB		0.08	1	2015
11708	Matthew Mulligan	0	0	0	0	0	0	0	0	0.1	1	0	0	DET	TE		0.2	1	2015
13198	Jimmy Clausen	0.1	0.2	1.2	0	0	0.1	0	0.4	0	0	0	0	BAL	QB		0.088	1	2015
10487	Drew Stanton	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	ARI	QB		0.048	1	2015
15058	Blair Walsh	0	0	0	0	0	0	0	0	0	0	0	8.4	MIN	K		8.4	1	2015
17747	Andrew Franks	0	0	0	0	0	0	0	0	0	0	0	8.3	MIA	K		8.3	1	2015
2622	Steve Smith Sr.	0	0	0	0	0	0	0	0	4.7	62.4	0.4	8	BAL	WR	Q	13.340000000000002	1	2015
17580	Ameer Abdullah	0	0	0	0	0	9.6	0.2	43.1	3.6	29.3	0.1	7.8	DET	RB		12.64	1	2015
13353	Dorin Dickerson	0	0	0	0	0	0	0	0	0	0	0	0	TEN	TE	P	0	1	2015
14159	Kendall Hunter	0	0	0	0	0	0	0	0	0	0	0	0	NO	RB	P	0	1	2015
14882	Kellen Moore	0	0	0	0	0	0	0	0	0	0	0	0	DAL	QB		0	1	2015
15092	Bryce Brown	0	0	0	0	0	0	0	0	0	0	0	0	SEA	RB		0	1	2015
2330	Tom Brady	25.8	38.3	282.7	2.2	0.5	2.1	0.1	2.6	0	0	0	19.4	NE	QB		20.468000000000004	1	2015
14874	Andrew Luck	23	39	263	2	1	4	0	17	0	0	0	16.5	IND	QB		19.22	1	2015
16728	Teddy Bridgewater	20	32.3	238.4	1.3	0.8	3.7	0.2	19.9	0	0	0	14.3	MIN	QB		17.126	1	2015
16040	C.J. Anderson	0	0	0	0	0	16	1	66	3	26	0	13.9	DEN	RB		18.2	1	2015
15653	Jamize Olawale	0	0	0	0	0	0	0	0	0	0	0	0	OAK	RB		0	1	2015
15940	Chris Gragg	0	0	0	0	0	0	0	0	0	0	0	0	BUF	TE		0	1	2015
16024	Kerwynn Williams	0	0	0	0	0	0	0	0	0	0	0	0	ARI	RB		0	1	2015
17713	Geoff Swaim	0	0	0	0	0	0	0	0	0	0	0	0	DAL	TE		0	1	2015
17748	Brandon Wegher	0	0	0	0	0	0	0	0	0	0	0	0	CAR	RB		0	1	2015
14403	Andre Holmes	0	0	0	0	0	0	0	0	1.4	21	0.1	2.5	OAK	WR		4.1	1	2015
17611	Ty Montgomery	0	0	0	0	0	0.4	0	2.6	1.3	16.9	0.1	2	GB	WR	Q	3.85	1	2015
17148	Michael Campanaro	0	0	0	0	0	0.2	0	1.5	1.6	17.8	0.1	1.9	BAL	WR	P	4.130000000000001	1	2015
16791	Donte Moncrief	0	0	0	0	0	0.2	0	1.6	1.3	17.3	0.1	1.7	IND	WR		3.79	1	2015
17542	Justin Hardy	0	0	0	0	0	0	0	0	1.6	18.7	0.1	1.6	ATL	WR		4.07	1	2015
15501	Bobby Rainey	0	0	0	0	0	2.5	0.1	10.1	0.5	4.1	0	1.6	TB	RB		2.5200000000000005	1	2015
16014	Corey Fuller	0	0	0	0	0	0.2	0	1.6	1	15.4	0.1	1.5	DET	WR		3.3000000000000003	1	2015
13662	Bryan Walters	0	0	0	0	0	0	0	0	0.8	10.8	0	1.3	JAX	WR		1.8800000000000001	1	2015
14888	Isaiah Pead	0	0	0	0	0	2.6	0.1	9.2	0.5	3.9	0	0.4	MIA	RB		2.41	1	2015
16757	Derek Carr	23	37.5	264.6	1.1	0.8	2.1	0.1	7	0	0	0	12.6	OAK	QB		15.484	1	2015
14912	Alshon Jeffery	0	0	0	0	0	0.5	0	3.1	5.5	76.5	0.6	10.5	CHI	WR		17.060000000000002	1	2015
13295	Emmanuel Sanders	0	0	0	0	0	0.6	0	3.7	5.1	71.7	0.4	9.7	DEN	WR		15.040000000000001	1	2015
17133	Isaiah Crowell	0	0	0	0	0	17.9	0.4	71.4	1	8	0	9.2	CLE	RB		11.340000000000002	1	2015
14921	Nick Toon	0	0	0	0	0	0	0	0	0	0	0	0	STL	WR		0	1	2015
15299	G.J. Kinne	0	0	0	0	0	0	0	0	0	0	0	0	PHI	QB		0	1	2015
5536	Ben Roethlisberger	25.6	39.2	302.9	1.8	0.9	2.1	0	6.2	0	0	0	17.1	PIT	QB		22.036	1	2015
14881	Russell Wilson	18	28	221	2	1	6	0	39	0	0	0	16.5	SEA	QB		19.74	1	2015
11252	Joe Flacco	23.9	37.5	266.1	1.7	0.9	2.4	0.1	5.2	0	0	0	15.1	BAL	QB	Q	17.664	1	2015
14012	Andy Dalton	19.6	30.3	225.3	1.4	0.9	3.2	0.1	10.4	0	0	0	14.3	CIN	QB		15.351999999999999	1	2015
8664	Ryan Fitzpatrick	21.1	33.7	239.1	1.4	1.1	3.8	0.1	15.8	0	0	0	13.6	NYJ	QB		16.244	1	2015
15775	Ifeanyi Momah	0	0	0	0	0	0	0	0	0	0	0	0	ARI	WR	P	0	1	2015
15864	Geno Smith	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	QB		0	1	2015
15996	Zac Stacy	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	RB	P	0	1	2015
16210	Chris Pantale	0	0	0	0	0	0	0	0	0	0	0	0	CHI	TE		0	1	2015
16488	George Winn	0	0	0	0	0	0	0	0	0	0	0	0	DET	RB		0	1	2015
17738	Seantavius Jones	0	0	0	0	0	0	0	0	0	0	0	0	NO	WR		0	1	2015
17760	Matt Hazel	0	0	0	0	0	0	0	0	0	0	0	0	MIA	WR		0	1	2015
17184	Willie Snead	0	0	0	0	0	0	0	0	1.9	26.4	0.1	2.7	NO	WR		5.139999999999999	1	2015
17138	Damien Williams	0	0	0	0	0	4.4	0.1	17	0.8	6.7	0	2	MIA	RB		3.7699999999999996	1	2015
13440	Andrew Quarless	0	0	0	0	0	0	0	0	1.2	13.1	0.1	1.9	GB	TE	P	3.11	1	2015
14085	Virgil Green	0	0	0	0	0	0	0	0	1.3	12.4	0.1	1.8	DEN	TE		3.14	1	2015
14151	Jeremy Kerley	0	0	0	0	0	0.3	0	1.7	1.1	13.9	0.1	1.6	DET	WR		3.2600000000000002	1	2015
17505	Jeremy Langford	0	0	0	0	0	3.6	0.1	14.7	0.1	1.1	0	1.5	CHI	RB		2.2800000000000002	1	2015
17600	Jamison Crowder	0	0	0	0	0	0	0	0	1.6	18.2	0.1	1.4	WSH	WR		4.02	1	2015
15078	Chris Givens	0	0	0	0	0	0	0	0	1	13	0.1	1.4	PHI	WR		2.9	1	2015
16504	Jack Doyle	0	0	0	0	0	0	0	0	0.7	7.8	0.1	0.5	IND	TE		2.08	1	2015
13229	Rob Gronkowski	0	0	0	0	0	0	0	0	6	71	1	13	NE	TE		19.1	1	2015
13981	Mark Ingram	0	0	0	0	0	14.3	0.5	60	2.8	21.6	0.1	11.4	NO	RB	P	14.56	1	2015
60004	Bengals	0	0	0	0	0	0	0	0	0	0	0	10		DST		10	1	2015
15818	Keenan Allen	0	0	0	0	0	0	0	0	5.3	72.2	0.4	9.3	SD	WR	P	14.92	1	2015
14885	Doug Martin	0	0	0	0	0	15.5	0.3	61.5	1.5	12	0	9.1	TB	RB		10.649999999999999	1	2015
9635	Kellen Clemens	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	SD	QB		0.052000000000000005	1	2015
15894	Sean Renfree	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	ATL	QB		0.052000000000000005	1	2015
16814	Zach Mettenberger	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	TEN	QB	P	0.052000000000000005	1	2015
9667	Charlie Whitehurst	0	0	0	0	0	0	0	0	0	0	0	0	IND	QB	P	0	1	2015
12586	Hakeem Nicks	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	1	2015
17354	Asante Cleveland	0	0	0	0	0	0	0	0	0	0	0	0	SD	TE		0	1	2015
17433	Andre Debose	0	0	0	0	0	0	0	0	0	0	0	0	OAK	WR		0	1	2015
17490	Blake Bell	0	0	0	0	0	0	0	0	0	0	0	0	SF	TE		0	1	2015
17562	Sammie Coates	0	0	0	0	0	0	0	0	0	0	0	0	PIT	WR		0	1	2015
17626	Aaron Ripkowski	0	0	0	0	0	0	0	0	0	0	0	0	GB	RB		0	1	2015
16775	Crockett Gillmore	0	0	0	0	0	0	0	0	3	33.8	0.3	4.7	BAL	TE	Q	8.18	1	2015
14895	Ronnie Hillman	0	0	0	0	0	6.4	0.2	24.3	1.2	10.4	0.1	4.5	DEN	RB		6.470000000000001	1	2015
9592	Vernon Davis	0	0	0	0	0	0	0	0	2.8	37.1	0.2	4.4	DEN	TE		7.71	1	2015
14024	Greg Little	0	0	0	0	0	0	0	0	0	0	0	0	BUF	WR		0	1	2015
14156	Niles Paul	0	0	0	0	0	0	0	0	0	0	0	0	WSH	TE	P	0	1	2015
14875	Robert Griffin	0	0	0	0	0	0	0	0	0	0	0	0	CLE	QB		0	1	2015
15041	Daniel Herron	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	1	2015
15364	Jonas Gray	0	0	0	0	0	0	0	0	0	0	0	0	JAX	RB		0	1	2015
8439	Aaron Rodgers	22	34	269	2	1	3	0	16	0	0	0	16.5	GB	QB	P	19.36	1	2015
17610	Marcus Mariota	20.7	32.7	236.7	1.3	1	5.5	0.2	33.1	0	0	0	15.7	TEN	QB		18.177999999999997	1	2015
10456	Marshawn Lynch	0	0	0	0	0	18	1	81	2	15	0	14.8	SEA	RB		17.6	1	2015
10447	Calvin Johnson	0	0	0	0	0	0	0	0	6	82	1	14	DET	WR		20.2	1	2015
15503	Deonte Thompson	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	1	2015
15839	Marquise Goodwin	0	0	0	0	0	0	0	0	0	0	0	0	BUF	WR	P	0	1	2015
15952	Mike Gillislee	0	0	0	0	0	0	0	0	0	0	0	0	BUF	RB		0	1	2015
16074	Michael Hill*	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB	IR	0	1	2015
16381	Kenbrell Thompkins	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	WR		0	1	2015
17730	Duron Carter	0	0	0	0	0	0	0	0	0	0	0	0	IND	WR		0	1	2015
17752	Corey Grant	0	0	0	0	0	0	0	0	0	0	0	0	JAX	RB		0	1	2015
15836	Gavin Escobar	0	0	0	0	0	0	0	0	1	12.6	0.2	2.3	DAL	TE	Q	3.46	1	2015
14922	Mohamed Sanu	0	0	0	0	0	0.6	0	4.1	1.8	20	0.2	2	ATL	WR		5.41	1	2015
17187	Darren Fells	0	0	0	0	0	0	0	0	1.2	13.8	0.1	1.8	ARI	TE		3.18	1	2015
14889	Chris Polk	0	0	0	0	0	4.2	0.1	19.6	0.5	4	0	1.7	HOU	RB		3.4600000000000004	1	2015
15403	Derek Carrier	0	0	0	0	0	0	0	0	1.5	16.9	0.1	1.6	WSH	TE	P	3.79	1	2015
14009	Rob Housler	0	0	0	0	0	0	0	0	1.6	18.3	0.1	1.5	CHI	TE		4.03	1	2015
17638	Chris Conley	0	0	0	0	0	0	0	0	1	11.9	0.1	1.4	KC	WR		2.79	1	2015
9530	John Kuhn	0	0	0	0	0	1	0.1	3	0.4	3.1	0	0.5	GB	RB		1.6100000000000003	1	2015
14100	Dwayne Harris	0	0	0	0	0	0	0	0	0.4	4.8	0	0.4	NYG	WR		0.88	1	2015
13983	A.J. Green	0	0	0	0	0	0	0	2	5	76	1	12.9	CIN	WR		18.8	1	2015
14877	Nick Foles	19.2	31.2	218.2	1.1	0.8	2.1	0.1	9.4	0	0	0	10.7	STL	QB		13.867999999999999	1	2015
60025	49ers	0	0	0	0	0	0	0	0	0	0	0	9.8		DST		9.8	1	2015
1440	Phil Dawson	0	0	0	0	0	0	0	0	0	0	0	9.3	SF	K		9.3	1	2015
12649	Julian Edelman	0	0	0	0	0	0.5	0	3.6	6.4	67.6	0.5	9	NE	WR	P	16.52	1	2015
5615	Matt Schaub	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	ATL	QB		0.052000000000000005	1	2015
11291	Chad Henne	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	JAX	QB		0.048	1	2015
16593	Matt McGloin	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	OAK	QB		0.052000000000000005	1	2015
12536	Chase Coffman	0	0	0	0	0	0	0	0	0	0	0	0	SEA	TE		0	1	2015
13211	Toby Gerhart	0	0	0	0	0	0	0	0	0	0	0	0	JAX	RB		0	1	2015
16002	Kyle Juszczyk	0	0	0	0	0	0	0	0	1.4	11.5	0.1	1.4	BAL	RB		3.15	1	2015
17709	Jaelen Strong	0	0	0	0	0	0	0	0	0.9	12	0.1	1.4	HOU	WR		2.7	1	2015
12535	James Casey	0	0	0	0	0	0	0	0	0.8	9.2	0.1	0.5	DEN	TE		2.3200000000000003	1	2015
11717	Marcel Reece	0	0	0	0	0	0.7	0	2.2	1	9.7	0	0.4	OAK	RB	SSPD	2.19	1	2015
12477	Brian Hoyer	19.6	33.6	240.6	1.2	1	2.3	0.1	3.4	0	0	0	11.8	HOU	QB		14.363999999999999	1	2015
11247	Jonathan Stewart	0	0	0	0	0	16.5	0.3	72.4	1.6	11.5	0	10.1	CAR	RB		11.79	1	2015
16055	Benjamin Cunningham	0	0	0	0	0	13.8	0.3	53.5	3.2	26	0.1	9.3	STL	RB		13.549999999999999	1	2015
15920	Latavius Murray	0	0	0	0	0	15.2	0.3	63.8	1.9	14.9	0	9.2	OAK	RB		11.57	1	2015
1575	Matt Hasselbeck	0.1	0.2	1.4	0	0	0	0	0	0	0	0	0	IND	QB		0.055999999999999994	1	2015
15837	Mike Glennon	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	TB	QB		0.052000000000000005	1	2015
17434	Bryce Petty	0.1	0.2	1.1	0	0	0	0	0	0	0	0	0	NYJ	QB		0.044000000000000004	1	2015
9643	Devin Hester	0	0	0	0	0	0	0	0	0	0	0	0	ATL	WR	Q	0	1	2015
12576	Brandon LaFell	0	0	0	0	0	0	0	0	0	0	0	0	CIN	WR		0	1	2015
13218	Arrelious Benn	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR	P	0	1	2015
17419	Kevin Smith	0	0	0	0	0	0	0	0	0	0	0	0	SEA	WR		0	1	2015
17488	Kenny Bell	0	0	0	0	0	0	0	0	0	0	0	0	TB	WR	P	0	1	2015
17561	Quan Bray	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	1	2015
17617	Kenny Hilliard	0	0	0	0	0	0	0	0	0	0	0	0	HOU	RB		0	1	2015
15835	Zach Ertz	0	0	0	0	0	0	0	0	2.7	33.7	0.3	4.7	PHI	TE		7.87	1	2015
12569	Percy Harvin	0	0	0	0	0	1.1	0	7.4	3.2	34.7	0.2	4.5	BUF	WR	Q	8.61	1	2015
12585	Louis Murphy	0	0	0	0	0	0	0	0	2.4	35.3	0.2	4.4	TB	WR	Q	7.13	1	2015
15369	Lance Dunbar	0	0	0	0	0	3.5	0.1	15.2	2.3	20.2	0.1	4.3	DAL	RB	Q	7.039999999999999	1	2015
13207	Dexter McCluster	0	0	0	0	0	2.9	0.1	11.1	2.5	23	0.1	4.2	TEN	WR		7.109999999999999	1	2015
15899	B.J. Daniels	0	0	0	0	0	0	0	0	0	0	0	0	HOU	WR		0	1	2015
15998	Josh Boyce	0	0	0	0	0	0	0	0	0	0	0	0	NE	WR		0	1	2015
16236	Matthew Tucker*	0	0	0	0	0	0	0	0	0	0	0	0	PHI	RB	O	0	1	2015
16493	Jake Stoneburner	0	0	0	0	0	0	0	0	0	0	0	0	MIA	TE		0	1	2015
17740	Gus Johnson	0	0	0	0	0	0	0	0	0	0	0	0	ATL	RB		0	1	2015
17761	Seth Roberts	0	0	0	0	0	0	0	0	0	0	0	0	OAK	WR		0	1	2015
14192	Roy Helu	0	0	0	0	0	3.5	0.1	13.9	2.2	17.6	0.1	2.7	OAK	RB	P	6.550000000000001	1	2015
17605	Karlos Williams	0	0	0	0	0	4.3	0.1	17.4	0.6	5	0	2	BUF	RB	P	3.44	1	2015
13214	James Starks	0	0	0	0	0	4.1	0.1	18.6	1.1	8.7	0.1	1.8	GB	RB		5.029999999999999	1	2015
16007	Chris Harper	0	0	0	0	0	0	0	0	1	12.7	0.1	1.5	NYG	WR		2.87	1	2015
13272	Ed Dickson	0	0	0	0	0	0	0	0	1.1	13.1	0.1	1.4	CAR	TE		3.0100000000000002	1	2015
16227	Russell Shepard	0	0	0	0	0	0	0	0	0.9	12.5	0.1	1.4	TB	WR		2.75	1	2015
10517	Jacoby Jones	0	0	0	0	0	0	0	0	0.2	3	0	0.5	PIT	WR		0.5	1	2015
14163	Tyrod Taylor	17.4	29.4	194.5	1.3	1	5	0.2	24.9	0	0	0	13	BUF	QB		15.670000000000002	1	2015
14886	Lamar Miller	0	0	0	0	0	16.1	0.5	72.5	2.5	19.5	0.1	11.2	HOU	RB		15.299999999999999	1	2015
13232	Jimmy Graham	0	0	0	0	0	0	0	0	4	48	1	9.9	SEA	TE	Q	14.8	1	2015
17689	Tevin Coleman	0	0	0	0	0	13.9	0.5	56	1.8	14.5	0.1	9.3	ATL	RB	P	12.45	1	2015
60015	Dolphins	0	0	0	0	0	0	0	0	0	0	0	9.1		DST		9.1	1	2015
9650	Tarvaris Jackson	0.1	0.1	1.1	0	0	0	0	0	0	0	0	0	SEA	QB		0.044000000000000004	1	2015
15891	Ryan Nassib	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	NYG	QB		0.052000000000000005	1	2015
16810	AJ McCarron	0.1	0.1	1	0	0	0	0	0	0	0	0	0	CIN	QB		0.04	1	2015
9780	Bruce Gradkowski	0	0	0	0	0	0	0	0	0	0	0	0	PIT	QB		0	1	2015
12874	Kevin Brock*	0	0	0	0	0	0	0	0	0	0	0	0	NO	TE	IR	0	1	2015
17355	Rashad Ross	0	0	0	0	0	0	0	0	0	0	0	0	WSH	WR		0	1	2015
17445	DiAndre Campbell	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR		0	1	2015
17498	Dres Anderson	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR		0	1	2015
17572	Garrett Grayson	0	0	0	0	0	0	0	0	0	0	0	0	NO	QB		0	1	2015
17645	Ben Koyack	0	0	0	0	0	0	0	0	0	0	0	0	JAX	TE		0	1	2015
14145	Charles Clay	0	0	0	0	0	0	0	0	3.2	34.9	0.3	4.7	BUF	TE	P	8.489999999999998	1	2015
17619	Tyler Lockett	0	0	0	0	0	0.1	0	0.9	2.4	34.2	0.2	4.5	SEA	WR		7.11	1	2015
15834	Aaron Dobson	0	0	0	0	0	0	0	0	2.2	30.4	0.2	4.4	NE	WR	P	6.44	1	2015
11258	Chris Johnson	0	0	0	0	0	7.3	0.2	33.1	0.8	6.5	0	4.3	ARI	RB		5.96	1	2015
15786	Tavon Austin	0	0	0	0	0	1.6	0	11.7	2.5	26.2	0.1	4.2	STL	WR		6.890000000000001	1	2015
17763	Chris Harper	0	0	0	0	0	0	0	0	0.7	9.8	0.1	0.4	NE	WR		2.2800000000000002	1	2015
60026	Seahawks	0	0	0	0	0	0	0	0	0	0	0	12.4		DST		12.4	1	2015
16777	Carlos Hyde	0	0	0	0	0	16.3	0.6	68.5	1.4	11.3	0	10.5	SF	RB	P	12.98	1	2015
8479	Frank Gore	0	0	0	0	0	15.2	0.3	57.3	2.7	21.9	0.1	9.5	IND	RB	P	13.02	1	2015
17134	Alfred Blue	0	0	0	0	0	16.7	0.3	68.8	1.5	12.2	0	9.2	HOU	RB		11.4	1	2015
4333	Matt Bryant	0	0	0	0	0	0	0	0	0	0	0	8.8	ATL	K		8.8	1	2015
4260	Shaun Hill	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	MIN	QB		0.048	1	2015
11280	Jerome Simpson	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR		0	1	2015
17383	RaShaun Allen	0	0	0	0	0	0	0	0	0	0	0	0	NO	TE		0	1	2015
17482	Da'Ron Brown	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	1	2015
17553	Dezmin Lewis	0	0	0	0	0	0	0	0	0	0	0	0	BUF	WR		0	1	2015
17604	Nick O'Leary	0	0	0	0	0	0	0	0	0	0	0	0	BUF	TE		0	1	2015
9588	Reggie Bush	0	0	0	0	0	4.6	0.1	20.2	2.5	21.5	0.1	5	SF	RB	Q	7.869999999999999	1	2015
15788	Tyler Eifert	0	0	0	0	0	0	0	0	3.3	39.1	0.3	4.5	CIN	TE		9.01	1	2015
14184	Cecil Shorts III	0	0	0	0	0	0.1	0	0.9	2.8	35.5	0.2	4.4	HOU	WR		7.64	1	2015
17616	Javorius Allen	0	0	0	0	0	5.8	0.2	22.7	1.7	13.2	0.1	4.3	BAL	RB		7.09	1	2015
15951	Denard Robinson	0	0	0	0	0	6.4	0.1	26.6	2.4	16.6	0.1	4.2	JAX	RB		7.92	1	2015
12601	Mike Wallace	0	0	0	0	0	0	0	0	3.4	51.8	0.3	6.7	BAL	WR		10.379999999999999	1	2015
15478	Brandon Bolden	0	0	0	0	0	8.5	0.5	36	0.9	6.8	0	6	NE	RB		8.18	1	2015
11788	Danny Woodhead	0	0	0	0	0	4.6	0.1	19	3.7	35.3	0.2	5.7	SD	RB		10.93	1	2015
60006	Cowboys	0	0	0	0	0	0	0	0	0	0	0	5.4		DST		5.4	1	2015
16913	James White	0	0	0	0	0	6.9	0.3	26.4	0.9	6.9	0	3.9	NE	RB		6.03	1	2015
13958	Andrew Hawkins	0	0	0	0	0	0.3	0	2	3.3	33.3	0.1	3.6	CLE	WR	Q	7.43	1	2015
16889	Andre Williams	0	0	0	0	0	6.2	0.2	26.8	0.6	4.6	0	3.2	NYG	RB		4.94	1	2015
17682	Devin Funchess	0	0	0	0	0	0	0	0	2.1	26.4	0.2	3.1	CAR	WR		5.94	1	2015
17398	Connor Shaw	0	0	0	0	0	0	0	0	0	0	0	0	CLE	QB	P	0	1	2015
17466	Gerald Christian	0	0	0	0	0	0	0	0	0	0	0	0	ARI	TE		0	1	2015
17549	Chris Manhertz	0	0	0	0	0	0	0	0	0	0	0	0	NO	TE		0	1	2015
17584	Devin Smith	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	WR	Q	0	1	2015
17690	Jesse James	0	0	0	0	0	0	0	0	0	0	0	0	PIT	TE		0	1	2015
60022	Cardinals	0	0	0	0	0	0	0	0	0	0	0	4.6		DST		4.6	1	2015
11373	Jacob Tamme	0	0	0	0	0	0	0	0	3	32.8	0.2	4.4	ATL	TE		7.4799999999999995	1	2015
17665	Dorial Green-Beckham	0	0	0	0	0	0	0	0	2.2	31.1	0.2	4.3	TEN	WR		6.510000000000001	1	2015
16732	Eric Ebron	0	0	0	0	0	0	0	0	3	33.8	0.2	4.2	DET	TE		7.58	1	2015
10475	Greg Olsen	0	0	0	0	0	0	0	0	4.3	53.1	0.3	6.7	CAR	TE		11.41	1	2015
15878	Terrance Williams	0	0	0	0	0	0	0	0	2.7	44.2	0.4	6.3	DAL	WR		9.52	1	2015
14017	Shane Vereen	0	0	0	0	0	4.9	0.2	22	3.1	29	0.1	5.8	NYG	RB		10	1	2015
16737	Mike Evans	0	0	0	0	0	0	0	0	2.8	42	0.2	5.4	TB	WR		8.2	1	2015
9307	Nate Washington	0	0	0	0	0	0	0	0	3.1	46.7	0.2	5.3	HOU	WR		8.969999999999999	1	2015
17601	Rashad Greene	0	0	0	0	0	0	0	0	2.6	32.1	0.2	4.1	JAX	WR		7.010000000000001	1	2015
15822	Stedman Bailey	0	0	0	0	0	0	0	0	2.3	30.1	0.1	3.8	STL	WR	Q	5.91	1	2015
10605	Brent Celek	0	0	0	0	0	0	0	0	2.2	24.9	0.2	3.5	PHI	TE		5.89	1	2015
9614	Marcedes Lewis	0	0	0	0	0	0	0	0	2.5	28.2	0.2	3.2	JAX	TE		6.5200000000000005	1	2015
14820	Chase Reynolds	0	0	0	0	0	6.8	0.2	23.9	0.8	6.8	0	3	STL	RB		5.069999999999999	1	2015
13387	Michael Hoomanawanui	0	0	0	0	0	0	0	0	0.5	5.8	0.1	0.3	NO	TE		1.6800000000000002	1	2015
16783	Terrance West	0	0	0	0	0	1.7	0	6.8	0.1	1	0	0.3	BAL	RB		0.8799999999999999	1	2015
11364	Gary Barnidge	0	0	0	0	0	0	0	0	0.8	9.8	0	0.3	CLE	TE		1.7800000000000002	1	2015
9695	Jason Avant	0	0	0	0	0	0	0	0	0.6	7.3	0	0.2	KC	WR		1.33	1	2015
9639	Anthony Fasano	0	0	0	0	0	0	0	0	0.4	4.9	0	0.2	TEN	TE		0.8900000000000001	1	2015
14402	Chris Hogan	0	0	0	0	0	0	0	0	0.4	5.2	0	0.2	NE	WR		0.92	1	2015
14135	Anthony Sherman	0	0	0	0	0	0.1	0	0.4	0.5	4.1	0	0.2	KC	RB		0.95	1	2015
12549	Brandon Pettigrew	0	0	0	0	0	0	0	0	0.3	3.8	0	0.2	DET	TE	Q	0.6799999999999999	1	2015
17519	DeAndrew White	0	0	0	0	0	0	0	0	0	0	0	0.1	SF	WR		0.1	1	2015
15351	Derrick Coleman	0	0	0	0	0	0	0	0	0.2	1.8	0	0.1	SEA	RB		0.38	1	2015
16581	Marlon Brown	0	0	0	0	0	0	0	0	2.4	30.3	0.2	4.3	BAL	WR		6.63	1	2015
16530	Khiry Robinson	0	0	0	0	0	5.5	0.2	24.3	1.3	10.3	0	4.2	NYJ	RB		5.960000000000001	1	2015
11439	Pierre Garcon	0	0	0	0	0	0	0	0	4.6	58.7	0.3	6.6	WSH	WR		12.27	1	2015
60007	Broncos	0	0	0	0	0	0	0	0	0	0	0	6.2		DST		6.2	1	2015
17145	Brandon Coleman	0	0	0	0	0	0	0	0	2.9	47.7	0.3	5.7	NO	WR		9.469999999999999	1	2015
17163	Allen Hurns	0	0	0	0	0	0	0	0	3.6	47.7	0.2	5.4	JAX	WR	P	9.57	1	2015
12568	Brian Hartline	0	0	0	0	0	0	0	0	2.9	38.8	0.2	3.9	CLE	WR	P	7.9799999999999995	1	2015
14900	Coby Fleener	0	0	0	0	0	0	0	0	2.4	28	0.3	3.7	NO	TE		6.999999999999999	1	2015
16143	Josh Hill	0	0	0	0	0	0	0	0	2.2	23.2	0.2	3.4	NO	TE		5.72	1	2015
15880	Robert Woods	0	0	0	0	0	0	0	0	2.2	28.7	0.2	3.1	BUF	WR		6.2700000000000005	1	2015
13225	Riley Cooper	0	0	0	0	0	0	0	0	1.4	20.3	0.2	2.9	PHI	WR		4.630000000000001	1	2015
15230	Chris Owusu	0	0	0	0	0	0	0	0	0.6	7.4	0	0.3	NYJ	WR	P	1.3399999999999999	1	2015
17737	Terrence Magee	0	0	0	0	0	1.9	0.1	6.5	0	0	0	0.3	BAL	RB		1.25	1	2015
15887	Ryan Griffin	0	0	0	0	0	0	0	0	0.6	6.9	0	0.3	HOU	TE		1.29	1	2015
11383	Tim Hightower	0	0	0	0	0	0.2	0	0.9	0.9	7.7	0	0.2	NO	RB		1.76	1	2015
11319	Craig Stevens	0	0	0	0	0	0	0	0	0.4	4.9	0	0.2	TEN	TE		0.8900000000000001	1	2015
17167	Corey Brown	0	0	0	0	0	0.6	0	4	2.6	37.9	0.2	4.2	CAR	WR	P	7.99	1	2015
11238	Darren McFadden	0	0	0	0	0	10.2	0.4	42.6	1.1	9.9	0.1	6.6	DAL	RB		9.35	1	2015
60014	Rams	0	0	0	0	0	0	0	0	0	0	0	6		DST		6	1	2015
16793	Cody Latimer	0	0	0	0	0	0	0	0	3.1	41.5	0.3	5.8	DEN	WR		9.05	1	2015
14189	Jordan Cameron	0	0	0	0	0	0	0	0	3.2	40	0.4	5.1	MIA	TE		9.600000000000001	1	2015
60033	Ravens	0	0	0	0	0	0	0	0	0	0	0	3.8		DST		3.8	1	2015
11658	Mike Tolbert	0	0	0	0	0	3	0.1	11.4	1.6	11.6	0.1	3.2	CAR	RB		5.1	1	2015
16285	Timothy Wright	0	0	0	0	0	0	0	0	0.7	7.7	0.1	0.3	DET	TE		2.0700000000000003	1	2015
60024	Chargers	0	0	0	0	0	0	0	0	0	0	0	6.6		DST		6.6	1	2015
15707	Larry Donnell	0	0	0	0	0	0	0	0	3.7	44.6	0.4	6.2	NYG	TE		10.56	1	2015
14583	Kamar Aiken	0	0	0	0	0	0	0	0	3.3	43.3	0.3	5.7	BAL	WR		9.43	1	2015
15860	Jordan Reed	0	0	0	0	0	0	0	0	3.9	40.4	0.2	5.4	WSH	TE		9.14	1	2015
16749	Charles Sims	0	0	0	0	0	5.1	0.1	19.9	2.5	20.8	0.1	3.9	TB	RB		7.77	1	2015
60018	Saints	0	0	0	0	0	0	0	0	0	0	0	3.7		DST		3.7	1	2015
60023	Steelers	0	0	0	0	0	0	0	0	0	0	0	3.3		DST		3.3	1	2015
60013	Raiders	0	0	0	0	0	0	0	0	0	0	0	3.1		DST		3.1	1	2015
17182	Albert Wilson	0	0	0	0	0	0	0	0	2.5	29.3	0.2	2.9	KC	WR		6.63	1	2015
17744	Terron Ward	0	0	0	0	0	1.1	0	4.2	0.3	2.6	0	0.3	ATL	RB		0.98	1	2015
17441	James O'Shaughnessy	0	0	0	0	0	0	0	0	0.7	7.5	0.1	0.3	KC	TE	P	2.05	1	2015
15981	Stepfan Taylor	0	0	0	0	0	1	0	4.3	0.1	1.1	0	0.3	ARI	RB		0.64	1	2015
17739	Thomas Rawls	0	0	0	0	0	1.4	0	5.9	0.1	0.9	0	0.2	SEA	RB	P	0.78	1	2015
15555	Joshua Bellamy	0	0	0	0	0	0	0	0	0.4	5.5	0	0.2	CHI	WR		0.9500000000000001	1	2015
17650	Tyler Kroft	0	0	0	0	0	0	0	0	0.4	4.9	0	0.2	CIN	TE		0.8900000000000001	1	2015
14280	Ricardo Lockette	0	0	0	0	0	0	0	0	0.3	4.7	0	0.2	SEA	WR	P	0.77	1	2015
17700	Mike Davis	0	0	0	0	0	0.8	0	3.2	0.1	1	0	0.1	SF	RB		0.52	1	2015
17502	Michael Burton	0	0	0	0	0	0.2	0	0.8	0.1	1.1	0	0.1	DET	RB		0.29000000000000004	1	2015
12567	Brandon Gibson	0	0	0	0	0	0	0	0	0	0	0	0	NE	WR	P	0	1	2015
17411	Bryn Renner	0	0	0	0	0	0	0	0	0	0	0	0	BAL	QB		0	1	2015
17530	Kaelin Clay	0	0	0	0	0	0	0	0	0	0	0	0	BAL	WR		0	1	2015
17583	Jeff Heuerman	0	0	0	0	0	0	0	0	0	0	0	0	DEN	TE		0	1	2015
9684	Owen Daniels	0	0	0	0	0	0	0	0	3.2	36.6	0.3	5	DEN	TE		8.66	1	2015
5557	Benjamin Watson	0	0	0	0	0	0	0	0	2.7	31.1	0.2	4.4	BAL	TE		7.010000000000001	1	2015
16795	Austin Seferian-Jenkins	0	0	0	0	0	0	0	0	2.9	35.8	0.2	4.3	TB	TE		7.68	1	2015
11318	Harry Douglas	0	0	0	0	0	0	0	0	3.1	38.8	0.2	4.3	TEN	WR		8.18	1	2015
17658	Duke Johnson Jr.	0	0	0	0	0	8.1	0.2	33.8	2.7	21.8	0.1	6.5	CLE	RB		10.06	1	2015
14908	Michael Floyd	0	0	0	0	0	0	0	0	2.8	40.2	0.3	5.9	ARI	WR		8.620000000000001	1	2015
9761	Delanie Walker	0	0	0	0	0	0	0	0	4.1	46.8	0.3	5.5	TEN	TE		10.579999999999998	1	2015
60012	Chiefs	0	0	0	0	0	0	0	0	0	0	0	5.1		DST		5.1	1	2015
16786	Richard Rodgers	0	0	0	0	0	0	0	0	2.4	27.4	0.3	3.7	GB	TE		6.9399999999999995	1	2015
10572	Scott Chandler*	0	0	0	0	0	0	0	0	1.8	20.9	0.2	3.1	NE	TE	O	5.09	1	2015
17756	Adam Humphries	0	0	0	0	0	0	0	0	0.2	2.5	0	0.3	TB	WR		0.45	1	2015
17653	Tyler Varga	0	0	0	0	0	1.4	0	5	0.4	3.4	0	0.3	IND	RB	P	1.24	1	2015
13097	Darrel Young	0	0	0	0	0	0.5	0	1.5	0.6	4.9	0	0.3	WSH	RB		1.24	1	2015
17478	Geremy Davis	0	0	0	0	0	0	0	0	0.5	6.3	0	0.2	NYG	WR		1.13	1	2015
13357	Jim Dray	0	0	0	0	0	0	0	0	0.6	6.6	0	0.2	BUF	TE		1.2599999999999998	1	2015
11369	Jerome Felton	0	0	0	0	0	0.3	0	0.9	0.3	2.7	0	0.2	BUF	RB		0.66	1	2015
11331	Andre Caldwell	0	0	0	0	0	0	0	0	0.2	3.1	0	0.2	DEN	WR		0.51	1	2015
13616	Marlon Moore	0	0	0	0	0	0.1	0	1	0.1	1.9	0	0.1	CLE	WR		0.39	1	2015
5633	Jerricho Cotchery	0	0	0	0	0	0	0	0	2.7	35.3	0.2	4	CAR	WR		7.430000000000001	1	2015
14434	Konrad Reuland	0	0	0	0	0	0	0	0	0	0	0	0	BAL	TE		0	1	2015
15252	Brian Tyms	0	0	0	0	0	0	0	0	0	0	0	0	NE	WR	P	0	1	2015
5526	Eli Manning	24.7	38.7	303	1.9	1	1	0	1.8	0	0	0	17.1	NYG	QB		21.9	1	2015
9597	Jay Cutler	23.2	37	268.7	1.8	1.3	2.8	0.1	12.5	0	0	0	15.4	CHI	QB		18.498	1	2015
13215	Dez Bryant	0	0	0	0	0	0	0	0	5	83	1	13.9	DAL	WR	P	19.3	1	2015
17677	Amari Cooper	0	0	0	0	0	0.2	0	1.5	5	67.2	0.3	7.7	OAK	WR		13.670000000000002	2	2015
11283	DeSean Jackson	0	0	0	0	0	0.2	0	1.5	4.4	66.5	0.3	7.5	WSH	WR		13	2	2015
17438	Jason Myers	0	0	0	0	0	0	0	0	0	0	0	7.4	JAX	K		7.4	2	2015
6016	Malcom Floyd	0	0	0	0	0	0	0	0	3.1	53.6	0.4	7.2	SD	WR		10.860000000000001	2	2015
11276	Eddie Royal	0	0	0	0	0	0	0	0	4.5	53.8	0.3	7	CHI	WR		11.68	2	2015
11439	Pierre Garcon	0	0	0	0	0	0	0	0	4.6	58.7	0.3	6.6	WSH	WR		12.27	2	2015
60007	Broncos	0	0	0	0	0	0	0	0	0	0	0	6.2		DST		6.2	2	2015
16793	Cody Latimer	0	0	0	0	0	0	0	0	3.1	41.5	0.3	5.8	DEN	WR		9.05	2	2015
13204	Ryan Mathews	0	0	0	0	0	8.9	0.2	40.5	0.8	6.3	0	5.5	PHI	RB	P	6.68	2	2015
14189	Jordan Cameron	0	0	0	0	0	0	0	0	3.2	40	0.4	5.1	MIA	TE		9.600000000000001	2	2015
11128	Matt Moore	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	MIA	QB		0.052000000000000005	2	2015
5631	Luke McCown	0.1	0.2	1.4	0	0	0	0	0	0	0	0	0	NO	QB		0.055999999999999994	2	2015
15168	Case Keenum	0.1	0.2	1.1	0	0	0	0	0	0	0	0	0	STL	QB		0.044000000000000004	2	2015
10467	Dwayne Bowe	0	0	0	0	0	0	0	0	0	0	0	0	CLE	WR		0	2	2015
13203	C.J. Spiller	0	0	0	0	0	0	0	0	0	0	0	0	NO	RB	P	0	2	2015
14918	Jarius Wright	0	0	0	0	0	0.3	0	1.7	1.9	24.4	0.1	2.8	MIN	WR		5.109999999999999	2	2015
16945	De'Anthony Thomas	0	0	0	0	0	0.9	0	5.7	1.8	13.6	0.1	2.1	KC	RB		4.33	2	2015
16172	Jaron Brown	0	0	0	0	0	0	0	0	1.5	18.4	0.2	1.9	ARI	WR		4.54	2	2015
14007	Lance Kendricks	0	0	0	0	0	0	0	0	1.8	19.5	0.1	1.8	STL	TE		4.35	2	2015
16755	Dri Archer	0	0	0	0	0	2	0	7.6	1.8	14.6	0.1	1.7	NYJ	RB		4.619999999999999	2	2015
17718	DeAndre Smelter	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR	P	0	2	2015
17752	Corey Grant	0	0	0	0	0	0	0	0	0	0	0	0	JAX	RB		0	2	2015
15994	Theo Riddick	0	0	0	0	0	1	0	3.7	1.7	14.7	0.1	1.5	DET	RB		4.140000000000001	2	2015
15705	Josh Gordon	0	0	0	0	0	0	0	0	0	0	0	0	CLE	WR	SSPD	0	1	2015
15904	Landry Jones	0	0	0	0	0	0	0	0	0	0	0	0	PIT	QB		0	1	2015
16003	Alan Bonner	0	0	0	0	0	0	0	0	0	0	0	0	HOU	WR		0	1	2015
16252	Tyler Bray	0	0	0	0	0	0	0	0	0	0	0	0	KC	QB		0	1	2015
17703	Kevin White	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	1	2015
17742	Malcolm Brown	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	1	2015
17762	Tyrell Williams	0	0	0	0	0	0	0	0	0	0	0	0	SD	WR		0	1	2015
13376	Clay Harbor	0	0	0	0	0	0	0	0	1.8	20.5	0.1	2.7	NE	TE		4.449999999999999	1	2015
15980	Levine Toilolo	0	0	0	0	0	0	0	0	1.3	13.4	0.2	2	ATL	TE		3.8400000000000003	1	2015
17660	Maxx Williams	0	0	0	0	0	0	0	0	1.5	16.9	0.1	1.8	BAL	TE		3.79	1	2015
17143	Branden Oliver	0	0	0	0	0	3.1	0.1	12.2	0.5	4.5	0	1.6	SD	RB	P	2.7700000000000005	1	2015
13272	Ed Dickson	0	0	0	0	0	0	0	0	1.1	13.1	0.1	1.4	CAR	TE		3.0100000000000002	2	2015
14100	Dwayne Harris	0	0	0	0	0	0	0	0	0.4	4.8	0	0.4	NYG	WR		0.88	2	2015
60022	Cardinals	0	0	0	0	0	0	0	0	0	0	0	4.6		DST		4.6	2	2015
14184	Cecil Shorts III	0	0	0	0	0	0.1	0	0.9	2.8	35.5	0.2	4.4	HOU	WR		7.64	2	2015
15369	Lance Dunbar	0	0	0	0	0	3.5	0.1	15.2	2.3	20.2	0.1	4.3	DAL	RB	Q	7.039999999999999	2	2015
13207	Dexter McCluster	0	0	0	0	0	2.9	0.1	11.1	2.5	23	0.1	4.2	TEN	WR		7.109999999999999	2	2015
16784	Tre Mason	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	2	2015
16886	Martavis Bryant	0	0	0	0	0	0	0	0	0	0	0	0	PIT	WR	SSPD	0	2	2015
17147	Arthur Lynch	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	TE	SSPD	0	2	2015
17198	Damian Copeland	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR		0	2	2015
17398	Connor Shaw	0	0	0	0	0	0	0	0	0	0	0	0	CLE	QB	P	0	2	2015
17483	Neal Sterling	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR		0	2	2015
17578	David Cobb	0	0	0	0	0	0	0	0	0	0	0	0	TEN	RB		0	2	2015
17683	Todd Gurley	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	2	2015
15503	Deonte Thompson	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	2	2015
15864	Geno Smith	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	QB		0	2	2015
15991	Justice Cunningham	0	0	0	0	0	0	0	0	0	0	0	0	STL	TE		0	2	2015
15755	Fozzy Whittaker	0	0	0	0	0	1	0	3.7	0.2	1.9	0	0.2	CAR	RB		0.76	1	2015
14099	Luke Stocker	0	0	0	0	0	0	0	0	0.3	3.6	0	0.1	TB	TE		0.6599999999999999	1	2015
17192	TJ Jones	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	DET	WR		0.24	1	2015
14909	Kendall Wright	0	0	0	0	0	0.2	0	1.6	4.8	53.2	0.2	6.4	TEN	WR		11.48	1	2015
11674	Danny Amendola	0	0	0	0	0	0	0	0	4.3	43.6	0.3	5.8	NE	WR		10.46	1	2015
12556	Kenny Britt	0	0	0	0	0	0	0	0	3.3	49.7	0.3	5.6	STL	WR		10.07	1	2015
5528	Larry Fitzgerald	0	0	0	0	0	0	0	0	4.5	56.5	0.4	7.7	ARI	WR		12.55	2	2015
10621	Nick Folk	0	0	0	0	0	0	0	0	0	0	0	7.5	NYJ	K	P	7.5	2	2015
11295	Martellus Bennett	0	0	0	0	0	0	0	0	4.7	50.4	0.4	7.4	NE	TE		12.14	2	2015
11458	Stevie Johnson	0	0	0	0	0	0	0	0	4.1	54.8	0.4	7.1	SD	WR	P	11.979999999999999	2	2015
9638	Greg Jennings	0	0	0	0	0	0	0	0	3.7	51.5	0.3	6.9	MIA	WR		10.650000000000002	2	2015
4527	Jason Witten	0	0	0	0	0	0	0	0	3.8	44.8	0.4	6.5	DAL	TE		10.68	2	2015
15707	Larry Donnell	0	0	0	0	0	0	0	0	3.7	44.6	0.4	6.2	NYG	TE		10.56	2	2015
17145	Brandon Coleman	0	0	0	0	0	0	0	0	2.9	47.7	0.3	5.7	NO	WR		9.469999999999999	2	2015
16737	Mike Evans	0	0	0	0	0	0	0	0	2.8	42	0.2	5.4	TB	WR		8.2	2	2015
1575	Matt Hasselbeck	0.1	0.2	1.4	0	0	0	0	0	0	0	0	0	IND	QB		0.055999999999999994	2	2015
4260	Shaun Hill	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	MIN	QB		0.048	2	2015
13199	Colt McCoy	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	WSH	QB		0.052000000000000005	2	2015
11270	Jordy Nelson	0	0	0	0	0	0	0	0	0	0	0	0	GB	WR	P	0	2	2015
13211	Toby Gerhart	0	0	0	0	0	0	0	0	0	0	0	0	JAX	RB		0	2	2015
17184	Willie Snead	0	0	0	0	0	0	0	0	1.9	26.4	0.1	2.7	NO	WR		5.139999999999999	2	2015
60019	Giants	0	0	0	0	0	0	0	0	0	0	0	2.1		DST		2.1	2	2015
17140	Juwan Thompson	0	0	0	0	0	2.9	0.1	11.7	0.3	2.4	0	1.9	DEN	RB		2.3099999999999996	2	2015
17660	Maxx Williams	0	0	0	0	0	0	0	0	1.5	16.9	0.1	1.8	BAL	TE		3.79	2	2015
12506	David Johnson	0	0	0	0	0	0	0	0	1.2	13.4	0.1	1.7	SD	TE		3.14	2	2015
17730	Duron Carter	0	0	0	0	0	0	0	0	0	0	0	0	IND	WR		0	2	2015
17753	E.J. Bibbs	0	0	0	0	0	0	0	0	0	0	0	0	CLE	TE		0	2	2015
17505	Jeremy Langford	0	0	0	0	0	3.6	0.1	14.7	0.1	1.1	0	1.5	CHI	RB		2.2800000000000002	2	2015
17638	Chris Conley	0	0	0	0	0	0	0	0	1	11.9	0.1	1.4	KC	WR		2.79	2	2015
17763	Chris Harper	0	0	0	0	0	0	0	0	0.7	9.8	0.1	0.4	NE	WR		2.2800000000000002	2	2015
14054	Kyle Rudolph	0	0	0	0	0	0	0	0	3.2	35.1	0.3	4.5	MIN	TE		8.510000000000002	2	2015
12585	Louis Murphy	0	0	0	0	0	0	0	0	2.4	35.3	0.2	4.4	TB	WR	Q	7.13	2	2015
16581	Marlon Brown	0	0	0	0	0	0	0	0	2.4	30.3	0.2	4.3	BAL	WR		6.63	2	2015
16792	Jace Amaro	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	TE	P	0	2	2015
17141	Kapri Bibbs	0	0	0	0	0	0	0	0	0	0	0	0	DEN	RB		0	2	2015
17197	Jeremy Butler	0	0	0	0	0	0	0	0	0	0	0	0	BAL	WR		0	2	2015
17346	Julian Talley	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	2	2015
17411	Bryn Renner	0	0	0	0	0	0	0	0	0	0	0	0	BAL	QB		0	2	2015
17553	Dezmin Lewis	0	0	0	0	0	0	0	0	0	0	0	0	BUF	WR		0	2	2015
17613	Brett Hundley	0	0	0	0	0	0	0	0	0	0	0	0	GB	QB		0	2	2015
15688	Brandon Bostick	0	0	0	0	0	0	0	0	0	0	0	0	MIN	TE		0	2	2015
15904	Landry Jones	0	0	0	0	0	0	0	0	0	0	0	0	PIT	QB		0	2	2015
16003	Alan Bonner	0	0	0	0	0	0	0	0	0	0	0	0	HOU	WR		0	2	2015
16252	Tyler Bray	0	0	0	0	0	0	0	0	0	0	0	0	KC	QB		0	2	2015
17195	Trey Burton	0	0	0	0	0	0	0	0	0.1	1.3	0	0.1	PHI	TE		0.23	2	2015
12482	Mark Sanchez	0.1	0.2	1.3	0	0	0.2	0	0.5	0	0	0	0.1	DEN	QB		0.10200000000000001	2	2015
15647	Chase Ford	0	0	0	0	0	0	0	0	0.1	1.2	0	0	BAL	TE		0.22	2	2015
14851	Terrelle Pryor	0	0	0	0	0	0.3	0	2	0	2	0	0	CLE	WR		0.4	2	2015
13357	Jim Dray	0	0	0	0	0	0	0	0	0.6	6.6	0	0.2	BUF	TE		1.2599999999999998	2	2015
12587	Jordan Norwood	0	0	0	0	0	0	0	0	0.2	3.2	0	0.2	DEN	WR		0.52	2	2015
15755	Fozzy Whittaker	0	0	0	0	0	1	0	3.7	0.2	1.9	0	0.2	CAR	RB		0.76	2	2015
14099	Luke Stocker	0	0	0	0	0	0	0	0	0.3	3.6	0	0.1	TB	TE		0.6599999999999999	2	2015
15003	Rhett Ellison	0	0	0	0	0	0	0	0	0.2	2.5	0	0.1	MIN	TE	Q	0.45	2	2015
13387	Michael Hoomanawanui	0	0	0	0	0	0	0	0	0.5	5.8	0.1	0.3	NO	TE		1.6800000000000002	2	2015
16323	MarQueis Gray	0	0	0	0	0	0	0	0	0.5	6.2	0.1	0.3	BUF	TE		1.7200000000000002	2	2015
14891	Bernard Pierce	0	0	0	0	0	1.4	0	5.4	0.5	4.2	0	0.2	JAX	RB	P	1.46	2	2015
15555	Joshua Bellamy	0	0	0	0	0	0	0	0	0.4	5.5	0	0.2	CHI	WR		0.9500000000000001	2	2015
12537	Jared Cook	0	0	0	0	0	0	0	0	3.4	40	0.2	5.3	GB	TE		8.600000000000001	1	2015
15923	Mychal Rivera	0	0	0	0	0	0	0	0	3	34.8	0.2	4	OAK	TE		7.68	1	2015
14901	Dwayne Allen	0	0	0	0	0	0	0	0	2.3	24.7	0.2	3.4	IND	TE		5.97	1	2015
16913	James White	0	0	0	0	0	6.9	0.3	26.4	0.9	6.9	0	3.9	NE	RB		6.03	2	2015
17442	David Johnson	0	0	0	0	0	4.4	0.2	19.2	1.9	15.5	0.1	3.5	ARI	RB		7.17	2	2015
13555	Jeff Cumberland	0	0	0	0	0	0	0	0	2.3	28.5	0.2	3.2	NYJ	TE		6.3500000000000005	2	2015
15795	DeAndre Hopkins	0	0	0	0	0	0	0	0	5.1	71.6	0.3	8.6	HOU	WR		14.059999999999999	2	2015
14911	Rueben Randle	0	0	0	0	0	0	0	0	4.7	67.9	0.4	8.3	PHI	WR		13.890000000000002	2	2015
14322	Dan Bailey	0	0	0	0	0	0	0	0	0	0	0	8	DAL	K		8	2	2015
2580	Drew Brees	26.8	41.3	325.6	1.9	0.9	1.6	0.1	2.5	0	0	0	18.6	NO	QB	P	23.574000000000005	2	2015
14891	Bernard Pierce	0	0	0	0	0	1.4	0	5.4	0.5	4.2	0	0.2	JAX	RB	P	1.46	1	2015
14186	Jordan Todman	0	0	0	0	0	0.7	0	2.5	0.6	4.8	0	0.2	IND	RB		1.33	1	2015
12587	Jordan Norwood	0	0	0	0	0	0	0	0	0.2	3.2	0	0.2	DEN	WR		0.52	1	2015
14332	Patrick DiMarco	0	0	0	0	0	0	0	0	0.4	3.9	0	0.2	ATL	RB		0.79	1	2015
17567	Nick Boyle	0	0	0	0	0	0	0	0	0.2	2.8	0	0.1	BAL	TE	SSPD	0.48	1	2015
17486	Marcus Murphy	0	0	0	0	0	1.4	0	5.5	2.7	22.4	0.1	2.8	NO	RB	P	6.09	1	2015
17173	Cody Parkey	0	0	0	0	0	0	0	0	0	0	0	7.7	PHI	K	P	7.7	2	2015
14816	Kai Forbath	0	0	0	0	0	0	0	0	0	0	0	7.3	NO	K		7.3	2	2015
16944	Devonta Freeman	0	0	0	0	0	8.5	0.3	32.7	2.4	23.5	0.1	7.1	ATL	RB		10.42	2	2015
60011	Colts	0	0	0	0	0	0	0	0	0	0	0	6.7		DST		6.7	2	2015
60014	Rams	0	0	0	0	0	0	0	0	0	0	0	6		DST		6	2	2015
17163	Allen Hurns	0	0	0	0	0	0	0	0	3.6	47.7	0.2	5.4	JAX	WR	P	9.57	2	2015
8559	Dan Orlovsky	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	DET	QB		0.052000000000000005	2	2015
12567	Brandon Gibson	0	0	0	0	0	0	0	0	0	0	0	0	NE	WR	P	0	2	2015
14870	Will Johnson	0	0	0	0	0	3.7	0.1	12	1.1	9.9	0.1	2.1	PIT	RB		4.49	2	2015
13214	James Starks	0	0	0	0	0	4.1	0.1	18.6	1.1	8.7	0.1	1.8	GB	RB		5.029999999999999	2	2015
17715	Vince Mayle	0	0	0	0	0	0	0	0	0	0	0	0	DAL	WR		0	2	2015
60003	Bears	0	0	0	0	0	0	0	0	0	0	0	-1.2		DST		0	2	2015
15501	Bobby Rainey	0	0	0	0	0	2.5	0.1	10.1	0.5	4.1	0	1.6	TB	RB		2.5200000000000005	2	2015
14009	Rob Housler	0	0	0	0	0	0	0	0	1.6	18.3	0.1	1.5	CHI	TE		4.03	2	2015
17709	Jaelen Strong	0	0	0	0	0	0	0	0	0.9	12	0.1	1.4	HOU	WR		2.7	2	2015
9530	John Kuhn	0	0	0	0	0	1	0.1	3	0.4	3.1	0	0.5	GB	RB		1.6100000000000003	2	2015
14890	LaMike James	0	0	0	0	0	1.8	0	7.3	0.8	6.8	0	0.4	MIA	RB		2.21	2	2015
60021	Eagles	0	0	0	0	0	0	0	0	0	0	0	4.6		DST		4.6	2	2015
11373	Jacob Tamme	0	0	0	0	0	0	0	0	3	32.8	0.2	4.4	ATL	TE		7.4799999999999995	2	2015
8544	Darren Sproles	0	0	0	0	0	3.2	0.1	15.1	2.8	23.2	0.1	4.3	PHI	RB		7.83	2	2015
17167	Corey Brown	0	0	0	0	0	0.6	0	4	2.6	37.9	0.2	4.2	CAR	WR	P	7.99	2	2015
16809	Garrett Gilbert	0	0	0	0	0	0	0	0	0	0	0	0	DET	QB		0	2	2015
17150	Tevin Reese	0	0	0	0	0	0	0	0	0	0	0	0	CIN	WR		0	2	2015
17354	Asante Cleveland	0	0	0	0	0	0	0	0	0	0	0	0	SD	TE		0	2	2015
17488	Kenny Bell	0	0	0	0	0	0	0	0	0	0	0	0	TB	WR	P	0	2	2015
17617	Kenny Hilliard	0	0	0	0	0	0	0	0	0	0	0	0	HOU	RB		0	2	2015
15775	Ifeanyi Momah	0	0	0	0	0	0	0	0	0	0	0	0	ARI	WR	P	0	2	2015
16015	Brad Sorensen	0	0	0	0	0	0	0	0	0	0	0	0	SD	QB		0	2	2015
15968	Mike James	0	0	0	0	0	0.5	0	2	0	0	0	0.1	TB	RB		0.2	2	2015
12489	Donald Brown	0	0	0	0	0	0.2	0	0.9	0.1	1.1	0	0.1	NE	RB		0.30000000000000004	2	2015
15921	Kenjon Barner	0	0	0	0	0	0.1	0	0.6	0	0	0	0	PHI	RB		0.06	2	2015
17356	Nic Jacobs	0	0	0	0	0	0	0	0	0.4	5.1	0	0.2	JAX	TE		0.91	2	2015
17567	Nick Boyle	0	0	0	0	0	0	0	0	0.2	2.8	0	0.1	BAL	TE	SSPD	0.48	2	2015
15230	Chris Owusu	0	0	0	0	0	0	0	0	0.6	7.4	0	0.3	NYJ	WR	P	1.3399999999999999	2	2015
15981	Stepfan Taylor	0	0	0	0	0	1	0	4.3	0.1	1.1	0	0.3	ARI	RB		0.64	2	2015
17607	Darren Waller	0	0	0	0	0	0	0	0	0.4	5.5	0	0.2	BAL	WR		0.9500000000000001	2	2015
60033	Ravens	0	0	0	0	0	0	0	0	0	0	0	3.8		DST		3.8	2	2015
14900	Coby Fleener	0	0	0	0	0	0	0	0	2.4	28	0.3	3.7	NO	TE		6.999999999999999	2	2015
60023	Steelers	0	0	0	0	0	0	0	0	0	0	0	3.3		DST		3.3	2	2015
17682	Devin Funchess	0	0	0	0	0	0	0	0	2.1	26.4	0.2	3.1	CAR	WR		5.94	2	2015
17182	Albert Wilson	0	0	0	0	0	0	0	0	2.5	29.3	0.2	2.9	KC	WR		6.63	2	2015
15091	Randy Bullock	0	0	0	0	0	0	0	0	0	0	0	8.5	NYJ	K		8.5	2	2015
17747	Andrew Franks	0	0	0	0	0	0	0	0	0	0	0	8.3	MIA	K		8.3	2	2015
9838	Marques Colston	0	0	0	0	0	0	0	0	4	60.1	0.3	8	NO	WR		11.809999999999999	2	2015
16799	Allen Robinson	0	0	0	0	0	0	0	0	5	66.4	0.3	7.8	JAX	WR		13.440000000000001	2	2015
5536	Ben Roethlisberger	25.6	39.2	302.9	1.8	0.9	2.1	0	6.2	0	0	0	17.1	PIT	QB		22.036	2	2015
13934	Antonio Brown	0	0	0	0	0	0	0	2	8	107	1	16.1	PIT	WR		27.9	2	2015
12514	LeSean McCoy	0	0	0	0	0	18	1	83	2	16	0	14.8	BUF	RB		17.900000000000002	2	2015
13216	Demaryius Thomas	0	0	0	0	0	0	0	0	7	83	1	14	DEN	WR		21.3	2	2015
14163	Tyrod Taylor	17.4	29.4	194.5	1.3	1	5	0.2	24.9	0	0	0	13	BUF	QB		15.670000000000002	2	2015
13981	Mark Ingram	0	0	0	0	0	14.3	0.5	60	2.8	21.6	0.1	11.4	NO	RB	P	14.56	2	2015
15893	Andre Ellington	0	0	0	0	0	12	0.4	50.2	3.1	28.8	0.1	9.9	ARI	RB		14.000000000000002	2	2015
17689	Tevin Coleman	0	0	0	0	0	13.9	0.5	56	1.8	14.5	0.1	9.3	ATL	RB	P	12.45	2	2015
60015	Dolphins	0	0	0	0	0	0	0	0	0	0	0	9.1		DST		9.1	2	2015
13940	James Develin	0	0	0	0	0	0	0	0	0	0	0	0	NE	RB		0	2	2015
14204	Julius Thomas	0	0	0	0	0	0	0	0	0	0	0	0	JAX	TE		0	2	2015
14882	Kellen Moore	0	0	0	0	0	0	0	0	0	0	0	0	DAL	QB		0	2	2015
15041	Daniel Herron	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	2	2015
11270	Jordy Nelson	0	0	0	0	0	0	0	0	0	0	0	0	GB	WR	P	0	1	2015
17382	Blake Annen	0	0	0	0	0	0	0	0	0	0	0	0	BUF	TE		0	1	2015
17455	Trevor Siemian	0	0	0	0	0	0	0	0	0	0	0	0	DEN	QB		0	1	2015
17556	Jay Ajayi	0	0	0	0	0	0	0	0	0	0	0	0	MIA	RB		0	1	2015
60020	Jets	0	0	0	0	0	0	0	0	0	0	0	7.6		DST		7.6	2	2015
16804	John Brown	0	0	0	0	0	0.3	0	1.7	4.1	57.6	0.4	7.3	ARI	WR		12.43	2	2015
15847	Travis Kelce	0	0	0	0	0	0	0	0	4.8	51.5	0.4	7	KC	TE		12.35	2	2015
60008	Lions	0	0	0	0	0	0	0	0	0	0	0	6.5		DST		6.5	2	2015
11674	Danny Amendola	0	0	0	0	0	0	0	0	4.3	43.6	0.3	5.8	NE	WR		10.46	2	2015
9761	Delanie Walker	0	0	0	0	0	0	0	0	4.1	46.8	0.3	5.5	TEN	TE		10.579999999999998	2	2015
9307	Nate Washington	0	0	0	0	0	0	0	0	3.1	46.7	0.2	5.3	HOU	WR		8.969999999999999	2	2015
9650	Tarvaris Jackson	0.1	0.1	1.1	0	0	0	0	0	0	0	0	0	SEA	QB		0.044000000000000004	2	2015
14037	Ryan Mallett	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	BAL	QB		0.048	2	2015
9667	Charlie Whitehurst	0	0	0	0	0	0	0	0	0	0	0	0	IND	QB	P	0	2	2015
12874	Kevin Brock*	0	0	0	0	0	0	0	0	0	0	0	0	NO	TE	IR	0	2	2015
16946	Bruce Ellington	0	0	0	0	0	0.1	0	0.9	1.8	21.2	0.1	2.5	SF	WR		4.609999999999999	2	2015
15980	Levine Toilolo	0	0	0	0	0	0	0	0	1.3	13.4	0.2	2	ATL	TE		3.8400000000000003	2	2015
14085	Virgil Green	0	0	0	0	0	0	0	0	1.3	12.4	0.1	1.8	DEN	TE		3.14	2	2015
17542	Justin Hardy	0	0	0	0	0	0	0	0	1.6	18.7	0.1	1.6	ATL	WR		4.07	2	2015
17740	Gus Johnson	0	0	0	0	0	0	0	0	0	0	0	0	ATL	RB		0	2	2015
15807	Cordarrelle Patterson	0	0	0	0	0	0.5	0	3.4	0.9	11.1	0.1	1.6	MIN	WR		2.9499999999999997	2	2015
14193	Jacquizz Rodgers	0	0	0	0	0	2.6	0.1	10.7	0.7	5.9	0	1.5	CHI	RB		2.96	2	2015
14167	Taiwan Jones	0	0	0	0	0	2.6	0.1	10	0.8	6.4	0	1.4	OAK	RB		3.0400000000000005	2	2015
13212	Boobie Dixon	0	0	0	0	0	1.7	0.1	6.7	0.3	2.5	0	0.5	BUF	RB		1.82	2	2015
12705	Brandon Myers	0	0	0	0	0	0	0	0	0.9	9.8	0.1	0.4	TB	TE		2.4800000000000004	2	2015
15349	Cole Beasley	0	0	0	0	0	0	0	0	3.1	37.5	0.3	4.6	DAL	WR		8.649999999999999	2	2015
17695	Matt Jones	0	0	0	0	0	6.4	0.2	20.6	2.2	17.7	0.1	4.4	WSH	RB	P	7.83	2	2015
17616	Javorius Allen	0	0	0	0	0	5.8	0.2	22.7	1.7	13.2	0.1	4.3	BAL	RB		7.09	2	2015
15951	Denard Robinson	0	0	0	0	0	6.4	0.1	26.6	2.4	16.6	0.1	4.2	JAX	RB		7.92	2	2015
16787	Marqise Lee	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR		0	2	2015
17021	Aaron Murray	0	0	0	0	0	0	0	0	0	0	0	0	KC	QB		0	2	2015
17196	Alex Bayer	0	0	0	0	0	0	0	0	0	0	0	0	STL	TE		0	2	2015
17353	Marcel Jensen	0	0	0	0	0	0	0	0	0	0	0	0	WSH	TE		0	2	2015
17466	Gerald Christian	0	0	0	0	0	0	0	0	0	0	0	0	ARI	TE		0	2	2015
17549	Chris Manhertz	0	0	0	0	0	0	0	0	0	0	0	0	NO	TE		0	2	2015
17604	Nick O'Leary	0	0	0	0	0	0	0	0	0	0	0	0	BUF	TE		0	2	2015
15653	Jamize Olawale	0	0	0	0	0	0	0	0	0	0	0	0	OAK	RB		0	2	2015
15899	B.J. Daniels	0	0	0	0	0	0	0	0	0	0	0	0	HOU	WR		0	2	2015
15998	Josh Boyce	0	0	0	0	0	0	0	0	0	0	0	0	IND	WR		0	2	2015
16236	Matthew Tucker*	0	0	0	0	0	0	0	0	0	0	0	0	PHI	RB	O	0	2	2015
17741	Zach Zenner	0	0	0	0	0	0.5	0	2	0	0	0	0.1	DET	RB	P	0.2	2	2015
13524	Seyi Ajirotutu	0	0	0	0	0	0	0	0	0.1	1.3	0	0.1	PHI	WR		0.23	2	2015
17524	Malcolm Johnson	0	0	0	0	0	0	0	0	0.2	1.8	0	0	CLE	TE		0.38	2	2015
2549	Mike Vick	0.1	0.2	1.3	0	0	0.1	0	0.4	0	0	0	0	PIT	QB		0.092	2	2015
15966	Chris Thompson	0	0	0	0	0	1	0	3.6	0.6	4.8	0	0.2	WSH	RB	P	1.44	2	2015
12510	Jorvorskie Lane	0	0	0	0	0	0.3	0	0.8	0.4	3.2	0	0.2	TB	RB		0.8	2	2015
17192	TJ Jones	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	DET	WR		0.24	2	2015
15102	Rishard Matthews	0	0	0	0	0	0	0	0	0.7	9.2	0.1	0.3	TEN	WR		2.2199999999999998	2	2015
16783	Terrance West	0	0	0	0	0	1.7	0	6.8	0.1	1	0	0.3	BAL	RB		0.8799999999999999	2	2015
15853	Vance McDonald	0	0	0	0	0	0	0	0	0.6	6.9	0	0.2	SF	TE		1.29	2	2015
14186	Jordan Todman	0	0	0	0	0	0.7	0	2.5	0.6	4.8	0	0.2	IND	RB		1.33	2	2015
12568	Brian Hartline	0	0	0	0	0	0	0	0	2.9	38.8	0.2	3.9	CLE	WR	P	7.9799999999999995	2	2015
16786	Richard Rodgers	0	0	0	0	0	0	0	0	2.4	27.4	0.3	3.7	GB	TE		6.9399999999999995	2	2015
16143	Josh Hill	0	0	0	0	0	0	0	0	2.2	23.2	0.2	3.4	NO	TE		5.72	2	2015
60013	Raiders	0	0	0	0	0	0	0	0	0	0	0	3.1		DST		3.1	2	2015
13225	Riley Cooper	0	0	0	0	0	0	0	0	1.4	20.3	0.2	2.9	PHI	WR		4.630000000000001	2	2015
9704	Stephen Gostkowski	0	0	0	0	0	0	0	0	0	0	0	8.5	NE	K		8.5	2	2015
15009	Alfred Morris	0	0	0	0	0	13.8	0.4	52.1	1.4	11.1	0	8.3	DAL	RB		10.12	2	2015
4680	Josh Brown	0	0	0	0	0	0	0	0	0	0	0	8.1	NYG	K		8.1	2	2015
17674	Breshad Perriman	0	0	0	0	0	0	0	0	0	0	0	0	BAL	WR	Q	0	1	2015
60021	Eagles	0	0	0	0	0	0	0	0	0	0	0	4.6		DST		4.6	1	2015
12503	Rashad Jennings	0	0	0	0	0	12.3	0.4	52.7	1.2	9.2	0	7.8	NYG	RB		9.790000000000001	2	2015
14876	Ryan Tannehill	23.5	35.9	277.1	1.7	0.8	3.3	0.1	16.2	0	0	0	17.5	MIA	QB		19.304000000000002	2	2015
4459	Carson Palmer	22.4	34.6	264.7	1.9	0.9	1.5	0.1	2.5	0	0	0	16.2	ARI	QB	P	18.138	2	2015
13982	Julio Jones	0	0	0	0	0	0	0	1	8	99	1	14.9	ATL	WR		24	2	2015
16724	Blake Bortles	21.2	33.4	244.9	1.2	0.9	3.8	0.1	22.9	0	0	0	14.1	JAX	QB		16.586000000000002	2	2015
8664	Ryan Fitzpatrick	21.1	33.7	239.1	1.4	1.1	3.8	0.1	15.8	0	0	0	13.6	NYJ	QB		16.244	2	2015
17172	Cairo Santos	0	0	0	0	0	0	0	0	0	0	0	7.6	KC	K		7.6	2	2015
9354	Robbie Gould	0	0	0	0	0	0	0	0	0	0	0	7.4	CHI	K		7.4	2	2015
16725	Sammy Watkins	0	0	0	0	0	0.3	0	1.9	4.1	56.2	0.4	7.3	BUF	WR		12.31	2	2015
60034	Texans	0	0	0	0	0	0	0	0	0	0	0	7.1		DST		7.1	2	2015
13271	Eric Decker	0	0	0	0	0	0	0	0	4.6	58.5	0.3	6.7	NYJ	WR		12.25	2	2015
17658	Duke Johnson Jr.	0	0	0	0	0	8.1	0.2	33.8	2.7	21.8	0.1	6.5	CLE	RB		10.06	2	2015
14908	Michael Floyd	0	0	0	0	0	0	0	0	2.8	40.2	0.3	5.9	ARI	WR		8.620000000000001	2	2015
11788	Danny Woodhead	0	0	0	0	0	4.6	0.1	19	3.7	35.3	0.2	5.7	SD	RB		10.93	2	2015
15860	Jordan Reed	0	0	0	0	0	0	0	0	3.9	40.4	0.2	5.4	WSH	TE		9.14	2	2015
15837	Mike Glennon	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	TB	QB		0.052000000000000005	2	2015
11291	Chad Henne	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	JAX	QB		0.048	2	2015
11280	Jerome Simpson	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR		0	2	2015
16782	Jerick McKinnon	0	0	0	0	0	3.8	0.1	17	1.4	11	0	2.7	MIN	RB		4.8	2	2015
17611	Ty Montgomery	0	0	0	0	0	0.4	0	2.6	1.3	16.9	0.1	2	GB	WR	Q	3.85	2	2015
13228	Jermaine Gresham	0	0	0	0	0	0	0	0	1.6	16.7	0.1	1.9	ARI	TE		3.87	2	2015
16791	Donte Moncrief	0	0	0	0	0	0.2	0	1.6	1.3	17.3	0.1	1.7	IND	WR		3.79	2	2015
17748	Brandon Wegher	0	0	0	0	0	0	0	0	0	0	0	0	CAR	RB		0	2	2015
17760	Matt Hazel	0	0	0	0	0	0	0	0	0	0	0	0	MIA	WR		0	2	2015
15403	Derek Carrier	0	0	0	0	0	0	0	0	1.5	16.9	0.1	1.6	WSH	TE	P	3.79	2	2015
16014	Corey Fuller	0	0	0	0	0	0.2	0	1.6	1	15.4	0.1	1.5	DET	WR		3.3000000000000003	2	2015
15078	Chris Givens	0	0	0	0	0	0	0	0	1	13	0.1	1.4	PHI	WR		2.9	2	2015
12535	James Casey	0	0	0	0	0	0	0	0	0.8	9.2	0.1	0.5	DEN	TE		2.3200000000000003	2	2015
11717	Marcel Reece	0	0	0	0	0	0.7	0	2.2	1	9.7	0	0.4	OAK	RB	SSPD	2.19	2	2015
15788	Tyler Eifert	0	0	0	0	0	0	0	0	3.3	39.1	0.3	4.5	CIN	TE		9.01	2	2015
9592	Vernon Davis	0	0	0	0	0	0	0	0	2.8	37.1	0.2	4.4	DEN	TE		7.71	2	2015
16781	Paul Richardson	0	0	0	0	0	0	0	0	0	0	0	0	SEA	WR	P	0	2	2015
17146	Marcus Harris	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	2	2015
17342	Fitzgerald Toussaint	0	0	0	0	0	0	0	0	0	0	0	0	PIT	RB		0	2	2015
17482	Da'Ron Brown	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	2	2015
17583	Jeff Heuerman	0	0	0	0	0	0	0	0	0	0	0	0	DEN	TE		0	2	2015
15705	Josh Gordon	0	0	0	0	0	0	0	0	0	0	0	0	CLE	WR	SSPD	0	2	2015
15996	Zac Stacy	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	RB	P	0	2	2015
16493	Jake Stoneburner	0	0	0	0	0	0	0	0	0	0	0	0	MIA	TE		0	2	2015
10770	Eric Weems	0	0	0	0	0	0	0	0	0.1	1.7	0	0.1	ATL	WR		0.27	2	2015
17749	Alonzo Harris	0	0	0	0	0	0.3	0	1	0	0	0	0	GB	RB		0.1	2	2015
14878	Brandon Weeden	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	HOU	QB		0.048	2	2015
15971	Rex Burkhead	0	0	0	0	0	0.3	0	1.2	0.7	5.7	0	0.2	CIN	RB		1.3900000000000001	2	2015
14332	Patrick DiMarco	0	0	0	0	0	0	0	0	0.4	3.9	0	0.2	ATL	RB		0.79	2	2015
17135	Devin Street	0	0	0	0	0	0	0	0	0.2	2.9	0	0.1	DAL	WR		0.49	2	2015
16285	Timothy Wright	0	0	0	0	0	0	0	0	0.7	7.7	0.1	0.3	DET	TE		2.0700000000000003	2	2015
17737	Terrence Magee	0	0	0	0	0	1.9	0.1	6.5	0	0	0	0.3	BAL	RB		1.25	2	2015
15564	Brittan Golden	0	0	0	0	0	0	0	0	0	0	0	0.3	ARI	WR		0.3	2	2015
10147	Miles Austin	0	0	0	0	0	0	0	0	0.4	5.4	0	0.2	PHI	WR		0.9400000000000001	2	2015
15632	Jonathan Grimes	0	0	0	0	0	5.6	0.1	23.3	1.7	14.1	0	3.9	HOU	RB		6.04	2	2015
60018	Saints	0	0	0	0	0	0	0	0	0	0	0	3.7		DST		3.7	2	2015
16889	Andre Williams	0	0	0	0	0	6.2	0.2	26.8	0.6	4.6	0	3.2	NYG	RB		4.94	2	2015
17593	DeVante Parker	0	0	0	0	0	0	0	0	2	27.8	0.2	3.1	MIA	WR		5.98	2	2015
17486	Marcus Murphy	0	0	0	0	0	1.4	0	5.5	2.7	22.4	0.1	2.8	NO	RB	P	6.09	2	2015
9705	Brandon Marshall	0	0	0	0	0	0	0	0	5	60.6	0.4	8.4	NYJ	WR		13.46	2	2015
60005	Browns	0	0	0	0	0	0	0	0	0	0	0	8.3		DST		8.3	2	2015
2622	Steve Smith Sr.	0	0	0	0	0	0	0	0	4.7	62.4	0.4	8	BAL	WR	Q	13.340000000000002	2	2015
8461	Mike Nugent	0	0	0	0	0	0	0	0	0	0	0	7.8	CIN	K		7.8	2	2015
2330	Tom Brady	25.8	38.3	282.7	2.2	0.5	2.1	0.1	2.6	0	0	0	19.4	NE	QB		20.468000000000004	2	2015
14874	Andrew Luck	23	39	263	2	1	4	0	17	0	0	0	16.5	IND	QB		19.22	2	2015
16728	Teddy Bridgewater	20	32.3	238.4	1.3	0.8	3.7	0.2	19.9	0	0	0	14.3	MIN	QB		17.126	2	2015
16757	Derek Carr	23	37.5	264.6	1.1	0.8	2.1	0.1	7	0	0	0	12.6	OAK	QB		15.484	2	2015
13295	Emmanuel Sanders	0	0	0	0	0	0.6	0	3.7	5.1	71.7	0.4	9.7	DEN	WR		15.040000000000001	2	2015
17586	Melvin Gordon	0	0	0	0	0	15.3	0.3	64.2	1.6	12.7	0.1	8.9	SD	RB	Q	11.689999999999998	2	2015
13231	Dennis Pitta	0	0	0	0	0	0	0	0	0	0	0	0	BAL	TE	Q	0	2	2015
14028	Stevan Ridley	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	RB		0	2	2015
14894	Robert Turbin	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	2	2015
15187	Austin Davis	0	0	0	0	0	0	0	0	0	0	0	0	CLE	QB		0	2	2015
17171	Brandon McManus	0	0	0	0	0	0	0	0	0	0	0	7.6	DEN	K		7.6	2	2015
16800	Davante Adams	0	0	0	0	0	0	0	0	4	57.7	0.4	7.4	GB	WR	P	12.17	2	2015
17668	Nelson Agholor	0	0	0	0	0	0	0	0	4.1	55.9	0.4	7.3	PHI	WR		12.09	2	2015
17743	Kyle Brindza	0	0	0	0	0	0	0	0	0	0	0	7	NYJ	K	D	7	2	2015
10475	Greg Olsen	0	0	0	0	0	0	0	0	4.3	53.1	0.3	6.7	CAR	TE		11.41	2	2015
60030	Jaguars	0	0	0	0	0	0	0	0	0	0	0	6.4		DST		6.4	2	2015
10522	James Jones	0	0	0	0	0	0	0	0	2.7	40.4	0.3	5.8	GB	WR		8.54	2	2015
15428	Jermaine Kearse	0	0	0	0	0	0	0	0	2.7	40.5	0.3	5.7	SEA	WR		8.55	2	2015
60006	Cowboys	0	0	0	0	0	0	0	0	0	0	0	5.4		DST		5.4	2	2015
15894	Sean Renfree	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	ATL	QB		0.052000000000000005	2	2015
17434	Bryce Petty	0.1	0.2	1.1	0	0	0	0	0	0	0	0	0	NYJ	QB		0.044000000000000004	2	2015
5362	Antonio Gates	0	0	0	0	0	0	0	0	0	0	0	0	SD	TE		0	2	2015
12576	Brandon LaFell	0	0	0	0	0	0	0	0	0	0	0	0	CIN	WR		0	2	2015
15391	Rod Streater	0	0	0	0	0	0	0	0	1.9	25.8	0.1	2.6	KC	WR		5.08	2	2015
14922	Mohamed Sanu	0	0	0	0	0	0.6	0	4.1	1.8	20	0.2	2	ATL	WR		5.41	2	2015
17148	Michael Campanaro	0	0	0	0	0	0.2	0	1.5	1.6	17.8	0.1	1.9	BAL	WR	P	4.130000000000001	2	2015
14889	Chris Polk	0	0	0	0	0	4.2	0.1	19.6	0.5	4	0	1.7	HOU	RB		3.4600000000000004	2	2015
17703	Kevin White	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	2	2015
17750	Cameron Meredith	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	2	2015
17761	Seth Roberts	0	0	0	0	0	0	0	0	0	0	0	0	OAK	WR		0	2	2015
17143	Branden Oliver	0	0	0	0	0	3.1	0.1	12.2	0.5	4.5	0	1.6	SD	RB	P	2.7700000000000005	2	2015
16766	C.J. Fiedorowicz	0	0	0	0	0	0	0	0	1.1	11.9	0.1	1.4	HOU	TE		2.89	2	2015
16227	Russell Shepard	0	0	0	0	0	0	0	0	0.9	12.5	0.1	1.4	TB	WR		2.75	2	2015
16504	Jack Doyle	0	0	0	0	0	0	0	0	0.7	7.8	0.1	0.5	IND	TE		2.08	2	2015
9588	Reggie Bush	0	0	0	0	0	4.6	0.1	20.2	2.5	21.5	0.1	5	SF	RB	Q	7.869999999999999	2	2015
12569	Percy Harvin	0	0	0	0	0	1.1	0	7.4	3.2	34.7	0.2	4.5	BUF	WR	Q	8.61	2	2015
15834	Aaron Dobson	0	0	0	0	0	0	0	0	2.2	30.4	0.2	4.4	NE	WR	P	6.44	2	2015
11258	Chris Johnson	0	0	0	0	0	7.3	0.2	33.1	0.8	6.5	0	4.3	ARI	RB		5.96	2	2015
16530	Khiry Robinson	0	0	0	0	0	5.5	0.2	24.3	1.3	10.3	0	4.2	NYJ	RB		5.960000000000001	2	2015
16813	Logan Thomas	0	0	0	0	0	0	0	0	0	0	0	0	MIA	QB		0	2	2015
16915	Keith Wenning	0	0	0	0	0	0	0	0	0	0	0	0	CIN	QB		0	2	2015
17154	Brenton Bersin	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR		0	2	2015
17200	Jonathan Krause	0	0	0	0	0	0	0	0	0	0	0	0	PHI	WR		0	2	2015
17355	Rashad Ross	0	0	0	0	0	0	0	0	0	0	0	0	WSH	WR		0	2	2015
17419	Kevin Smith	0	0	0	0	0	0	0	0	0	0	0	0	SEA	WR		0	2	2015
17490	Blake Bell	0	0	0	0	0	0	0	0	0	0	0	0	SF	TE		0	2	2015
17556	Jay Ajayi	0	0	0	0	0	0	0	0	0	0	0	0	MIA	RB		0	2	2015
17626	Aaron Ripkowski	0	0	0	0	0	0	0	0	0	0	0	0	GB	RB		0	2	2015
15383	Dominique Jones	0	0	0	0	0	0	0	0	0	0	0	0	MIA	TE		0	2	2015
15803	EJ Manuel	0	0	0	0	0	0	0	0	0	0	0	0	BUF	QB		0	2	2015
15935	Zac Dysert	0	0	0	0	0	0	0	0	0	0	0	0	MIA	QB		0	2	2015
16020	Spencer Ware	0	0	0	0	0	0	0	0	0	0	0	0	KC	RB		0	2	2015
16330	Joseph Fauria	0	0	0	0	0	0	0	0	0	0	0	0	ARI	TE		0	2	2015
13224	Marcus Easley	0	0	0	0	0	0	0	0	0.2	2.1	0	0.1	BUF	WR	Q	0.41000000000000003	2	2015
15896	Brice Butler	0	0	0	0	0	0	0	0	0.2	2.8	0	0.1	DAL	WR		0.48	2	2015
15670	Cooper Helfet	0	0	0	0	0	0	0	0	0.1	1	0	0.1	SEA	TE		0.2	2	2015
17717	Mario Alford	0	0	0	0	0	0	0	0	0.1	1.2	0	0	CIN	WR		0.22	2	2015
12519	Cedric Peerman	0	0	0	0	0	0.2	0	0.6	0	0	0	0	CIN	RB		0.06	2	2015
13409	Marc Mariani	0	0	0	0	0	0	0	0	0.1	1.4	0	0.2	CHI	WR		0.24	2	2015
14135	Anthony Sherman	0	0	0	0	0	0.1	0	0.4	0.5	4.1	0	0.2	KC	RB		0.95	2	2015
12549	Brandon Pettigrew	0	0	0	0	0	0	0	0	0.3	3.8	0	0.2	DET	TE	Q	0.6799999999999999	2	2015
15226	Cory Harkey	0	0	0	0	0	0	0	0	0.5	4	0	0.1	STL	TE		0.9	2	2015
17502	Michael Burton	0	0	0	0	0	0.2	0	0.8	0.1	1.1	0	0.1	DET	RB		0.29000000000000004	2	2015
17744	Terron Ward	0	0	0	0	0	1.1	0	4.2	0.3	2.6	0	0.3	ATL	RB		0.98	2	2015
17653	Tyler Varga	0	0	0	0	0	1.4	0	5	0.4	3.4	0	0.3	IND	RB	P	1.24	2	2015
17186	Austin Johnson	0	0	0	0	0	0.2	0	0.7	0.6	5.3	0	0.3	NO	RB		1.2	2	2015
11392	Kellen Davis	0	0	0	0	0	0	0	0	0.6	6.3	0	0.2	NYJ	TE		1.23	2	2015
9639	Anthony Fasano	0	0	0	0	0	0	0	0	0.4	4.9	0	0.2	TEN	TE		0.8900000000000001	2	2015
16749	Charles Sims	0	0	0	0	0	5.1	0.1	19.9	2.5	20.8	0.1	3.9	TB	RB		7.77	2	2015
13958	Andrew Hawkins	0	0	0	0	0	0.3	0	2	3.3	33.3	0.1	3.6	CLE	WR	Q	7.43	2	2015
16023	Marquess Wilson	0	0	0	0	0	0	0	0	2.3	28.5	0.2	3.2	CHI	WR	P	6.3500000000000005	2	2015
12550	John Phillips	0	0	0	0	0	0	0	0	1.8	21.2	0.2	3.1	SD	TE		5.12	2	2015
14924	T.Y. Hilton	0	0	0	0	0	0.2	0	1.6	4.8	65.5	0.4	8.4	IND	WR		13.91	2	2015
11923	Steven Hauschka	0	0	0	0	0	0	0	0	0	0	0	8.2	SEA	K		8.2	2	2015
60009	Packers	0	0	0	0	0	0	0	0	0	0	0	8		DST		8	2	2015
14001	Colin Kaepernick	19.8	31	244	1.1	0.4	6.6	0.2	40.6	0	0	0	17.1	SF	QB	P	19.02	2	2015
10452	Adrian Peterson	0	0	0	0	0	18	1	82	3	26	0	15.7	MIN	RB		19.8	2	2015
8416	Alex Smith	21.1	34	225.9	1.3	0.6	3.4	0.1	17.5	0	0	0	14.4	KC	QB		15.986	2	2015
17746	Josh Lambo	0	0	0	0	0	0	0	0	0	0	0	7.6	SD	K		7.6	2	2015
4512	Anquan Boldin	0	0	0	0	0	0	0	0	4.9	60.2	0.2	7.4	SF	WR		12.120000000000001	2	2015
60016	Vikings	0	0	0	0	0	0	0	0	0	0	0	7.2		DST		7.2	2	2015
8444	Heath Miller	0	0	0	0	0	0	0	0	4.6	51.3	0.4	7	PIT	TE		12.13	2	2015
12601	Mike Wallace	0	0	0	0	0	0	0	0	3.4	51.8	0.3	6.7	BAL	WR		10.379999999999999	2	2015
14909	Kendall Wright	0	0	0	0	0	0.2	0	1.6	4.8	53.2	0.2	6.4	TEN	WR		11.48	2	2015
60028	Redskins	0	0	0	0	0	0	0	0	0	0	0	5.8		DST		5.8	2	2015
12556	Kenny Britt	0	0	0	0	0	0	0	0	3.3	49.7	0.3	5.6	STL	WR		10.07	2	2015
12563	Michael Crabtree	0	0	0	0	0	0	0	0	4.2	49.5	0.2	5.3	OAK	WR		10.350000000000001	2	2015
15891	Ryan Nassib	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	NYG	QB		0.052000000000000005	2	2015
16814	Zach Mettenberger	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	TEN	QB	P	0.052000000000000005	2	2015
9643	Devin Hester	0	0	0	0	0	0	0	0	0	0	0	0	ATL	WR	Q	0	2	2015
12586	Hakeem Nicks	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	2	2015
14403	Andre Holmes	0	0	0	0	0	0	0	0	1.4	21	0.1	2.5	OAK	WR		4.1	2	2015
17605	Karlos Williams	0	0	0	0	0	4.3	0.1	17.4	0.6	5	0	2	BUF	RB	P	3.44	2	2015
17187	Darren Fells	0	0	0	0	0	0	0	0	1.2	13.8	0.1	1.8	ARI	TE		3.18	2	2015
15845	Justin Hunter	0	0	0	0	0	0	0	0	1.3	19.4	0.1	1.7	TEN	WR	P	3.8400000000000003	2	2015
17713	Geoff Swaim	0	0	0	0	0	0	0	0	0	0	0	0	DAL	TE		0	2	2015
17751	Bennie Fowler	0	0	0	0	0	0	0	0	0	0	0	0	DEN	WR		0	2	2015
17762	Tyrell Williams	0	0	0	0	0	0	0	0	0	0	0	0	SD	WR		0	2	2015
17704	Cameron Artis-Payne	0	0	0	0	0	3.9	0.1	15.6	0.3	2.8	0	1.5	CAR	RB		2.7399999999999998	2	2015
16026	Quinton Patton	0	0	0	0	0	0.4	0	2.7	1.1	15.4	0.1	1.4	SF	WR		3.5100000000000002	2	2015
17475	Clive Walford	0	0	0	0	0	0	0	0	0.9	10.9	0.1	1.3	OAK	TE		2.5900000000000003	2	2015
10517	Jacoby Jones	0	0	0	0	0	0	0	0	0.2	3	0	0.5	PIT	WR		0.5	2	2015
9684	Owen Daniels	0	0	0	0	0	0	0	0	3.2	36.6	0.3	5	DEN	TE		8.66	2	2015
14895	Ronnie Hillman	0	0	0	0	0	6.4	0.2	24.3	1.2	10.4	0.1	4.5	DEN	RB		6.470000000000001	2	2015
60017	Patriots	0	0	0	0	0	0	0	0	0	0	0	4.3		DST		4.3	2	2015
15072	Marvin Jones	0	0	0	0	0	0.2	0	1	2.7	35.6	0.2	4.3	DET	WR		7.5600000000000005	2	2015
15786	Tavon Austin	0	0	0	0	0	1.6	0	11.7	2.5	26.2	0.1	4.2	STL	WR		6.890000000000001	2	2015
16821	David Fales	0	0	0	0	0	0	0	0	0	0	0	0	CHI	QB		0	2	2015
16928	Jalen Saunders	0	0	0	0	0	0	0	0	0	0	0	0	NE	WR	SSPD	0	2	2015
17181	Frankie Hammond	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	2	2015
17208	Orleans Darkwa	0	0	0	0	0	0	0	0	0	0	0	0	NYG	RB		0	2	2015
17370	Freddie Martino	0	0	0	0	0	0	0	0	0	0	0	0	PHI	WR		0	2	2015
17433	Andre Debose	0	0	0	0	0	0	0	0	0	0	0	0	OAK	WR		0	2	2015
17498	Dres Anderson	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR		0	2	2015
17561	Quan Bray	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	2	2015
17645	Ben Koyack	0	0	0	0	0	0	0	0	0	0	0	0	JAX	TE		0	2	2015
15397	Saalim Hakim	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	2	2015
15825	Le'Veon Bell	0	0	0	0	0	0	0	0	0	0	0	0	PIT	RB	Q	0	2	2015
15940	Chris Gragg	0	0	0	0	0	0	0	0	0	0	0	0	BUF	TE		0	2	2015
16024	Kerwynn Williams	0	0	0	0	0	0	0	0	0	0	0	0	ARI	RB		0	2	2015
16373	Rodney Smith	0	0	0	0	0	0	0	0	0	0	0	0	DAL	WR		0	2	2015
17179	Ryan Grant	0	0	0	0	0	0	0	0	0.2	2.9	0	0.1	WSH	WR		0.49	2	2015
17757	Nikita Whitlock	0	0	0	0	0	0.2	0	0.8	0	0	0	0.1	NYG	RB		0.1	2	2015
17189	Demetrius Harris	0	0	0	0	0	0	0	0	0.1	1.2	0	0	KC	TE		0.22	2	2015
13987	Blaine Gabbert	0.1	0.2	1.3	0	0	0.1	0	0.4	0	0	0	0	SF	QB		0.092	2	2015
16760	Jimmy Garoppolo	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	NE	QB		0.048	2	2015
17650	Tyler Kroft	0	0	0	0	0	0	0	0	0.4	4.9	0	0.2	CIN	TE		0.8900000000000001	2	2015
15973	Tommy Bohanon	0	0	0	0	0	0.5	0	1.7	0.2	2.1	0	0.2	NYJ	RB		0.5800000000000001	2	2015
14916	Keshawn Martin	0	0	0	0	0	0	0	0	0.1	1.2	0	0.2	NE	WR		0.22	2	2015
17519	DeAndrew White	0	0	0	0	0	0	0	0	0	0	0	0.1	SF	WR		0.1	2	2015
15204	Garrett Celek	0	0	0	0	0	0	0	0	0.2	2.3	0	0.1	SF	TE		0.43	2	2015
17755	Lucky Whitehead	0	0	0	0	0	0	0	0	0.1	1.5	0	0.3	DAL	WR		0.3	2	2015
17745	Jarryd Hayne	0	0	0	0	0	0.7	0	2.7	0.1	0.9	0	0.3	SF	RB		0.45999999999999996	2	2015
11364	Gary Barnidge	0	0	0	0	0	0	0	0	0.8	9.8	0	0.3	CLE	TE		1.7800000000000002	2	2015
9695	Jason Avant	0	0	0	0	0	0	0	0	0.6	7.3	0	0.2	KC	WR		1.33	2	2015
13226	Andre Roberts	0	0	0	0	0	0.1	0	0.8	2.5	31.3	0.2	4.1	WSH	WR	P	6.91	2	2015
15822	Stedman Bailey	0	0	0	0	0	0	0	0	2.3	30.1	0.1	3.8	STL	WR	Q	5.91	2	2015
10572	Scott Chandler*	0	0	0	0	0	0	0	0	1.8	20.9	0.2	3.1	NE	TE	O	5.09	2	2015
14198	Dion Lewis	0	0	0	0	0	9.9	0.3	42	2.5	20.8	0.1	8.6	NE	RB	Q	11.18	2	2015
15683	Justin Tucker	0	0	0	0	0	0	0	0	0	0	0	8.3	BAL	K		8.3	2	2015
14993	Greg Zuerlein	0	0	0	0	0	0	0	0	0	0	0	8.1	STL	K		8.1	2	2015
15873	Markus Wheaton	0	0	0	0	0	0.3	0	2.2	4.4	61.2	0.3	7.9	PIT	WR		12.54	2	2015
14032	Torrey Smith	0	0	0	0	0	0	0	0	3.7	61.5	0.3	7.7	SF	WR		11.650000000000002	2	2015
5209	Tony Romo	20.6	31.1	263.6	2.3	0.6	1.7	0.1	4.5	0	0	0	17.6	DAL	QB	Q	20.194	2	2015
9597	Jay Cutler	23.2	37	268.7	1.8	1.3	2.8	0.1	12.5	0	0	0	15.4	CHI	QB		18.498	2	2015
10447	Calvin Johnson	0	0	0	0	0	0	0	0	6	82	1	14	DET	WR		20.2	2	2015
11688	Dan Carpenter	0	0	0	0	0	0	0	0	0	0	0	7.6	BUF	K		7.6	2	2015
60002	Bills	0	0	0	0	0	0	0	0	0	0	0	7.4		DST		7.4	2	2015
16790	Jarvis Landry	0	0	0	0	0	0.3	0	1.7	5.5	58.7	0.3	7.2	MIA	WR		13.34	2	2015
13536	Joique Bell	0	0	0	0	0	10.8	0.3	41.2	1.6	12.3	0	7	DET	RB		8.75	2	2015
11238	Darren McFadden	0	0	0	0	0	10.2	0.4	42.6	1.1	9.9	0.1	6.6	DAL	RB		9.35	2	2015
15878	Terrance Williams	0	0	0	0	0	0	0	0	2.7	44.2	0.4	6.3	DAL	WR		9.52	2	2015
16794	Bishop Sankey	0	0	0	0	0	11.7	0.3	46.7	1.2	9.4	0	5.8	TEN	RB		8.61	2	2015
14221	Doug Baldwin	0	0	0	0	0	0.1	0	0.9	3.7	49.9	0.3	5.6	SEA	WR		10.580000000000002	2	2015
12537	Jared Cook	0	0	0	0	0	0	0	0	3.4	40	0.2	5.3	GB	TE		8.600000000000001	2	2015
9635	Kellen Clemens	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	SD	QB		0.052000000000000005	2	2015
8644	Matt Cassel	0.1	0.2	1	0	0	0	0	0	0	0	0	0	TEN	QB		0.04	2	2015
16593	Matt McGloin	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	OAK	QB		0.052000000000000005	2	2015
12497	Arian Foster	0	0	0	0	0	0	0	0	0	0	0	0	HOU	RB	Q	0	2	2015
13213	LeGarrette Blount	0	0	0	0	0	0	0	0	0	0	0	0	NE	RB		0	2	2015
14192	Roy Helu	0	0	0	0	0	3.5	0.1	13.9	2.2	17.6	0.1	2.7	OAK	RB	P	6.550000000000001	2	2015
17138	Damien Williams	0	0	0	0	0	4.4	0.1	17	0.8	6.7	0	2	MIA	RB		3.7699999999999996	2	2015
13440	Andrew Quarless	0	0	0	0	0	0	0	0	1.2	13.1	0.1	1.9	GB	TE	P	3.11	2	2015
13371	Garrett Graham	0	0	0	0	0	0	0	0	1.8	20	0.1	1.8	HOU	TE		4.4	2	2015
14151	Jeremy Kerley	0	0	0	0	0	0.3	0	1.7	1.1	13.9	0.1	1.6	DET	WR		3.2600000000000002	2	2015
17736	John Crockett	0	0	0	0	0	0	0	0	0	0	0	0	GB	RB		0	2	2015
16736	Johnny Manziel	0.1	0.1	0.9	0	0	0	0	0	0	0	0	0	CLE	QB		0.036000000000000004	2	2015
10287	Daniel Fells	0	0	0	0	0	0	0	0	1	11.3	0.1	1.5	NYG	TE	Q	2.73	2	2015
17136	Dontrelle Inman	0	0	0	0	0	0	0	0	0.8	12.1	0.1	1.4	SD	WR		2.61	2	2015
13662	Bryan Walters	0	0	0	0	0	0	0	0	0.8	10.8	0	1.3	JAX	WR		1.8800000000000001	2	2015
10521	Matt Spaeth	0	0	0	0	0	0	0	0	0.8	9.1	0.1	0.4	PIT	TE		2.31	2	2015
15835	Zach Ertz	0	0	0	0	0	0	0	0	2.7	33.7	0.3	4.7	PHI	TE		7.87	2	2015
17619	Tyler Lockett	0	0	0	0	0	0.1	0	0.9	2.4	34.2	0.2	4.5	SEA	WR		7.11	2	2015
16795	Austin Seferian-Jenkins	0	0	0	0	0	0	0	0	2.9	35.8	0.2	4.3	TB	TE		7.68	2	2015
11318	Harry Douglas	0	0	0	0	0	0	0	0	3.1	38.8	0.2	4.3	TEN	WR		8.18	2	2015
16496	Cierre Wood	0	0	0	0	0	0	0	0	0	0	0	0	BUF	RB	Q	0	2	2015
16836	Jared Abbrederis	0	0	0	0	0	0	0	0	0	0	0	0	GB	WR		0	2	2015
16942	Ka'Deem Carey	0	0	0	0	0	0	0	0	0	0	0	0	CHI	RB		0	2	2015
17183	Trey Watts	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	2	2015
17209	Senorise Perry	0	0	0	0	0	0	0	0	0	0	0	0	CHI	RB	P	0	2	2015
17382	Blake Annen	0	0	0	0	0	0	0	0	0	0	0	0	BUF	TE		0	2	2015
17445	DiAndre Campbell	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR		0	2	2015
17527	Dylan Thompson	0	0	0	0	0	0	0	0	0	0	0	0	SF	QB		0	2	2015
17562	Sammie Coates	0	0	0	0	0	0	0	0	0	0	0	0	PIT	WR		0	2	2015
17649	Kennard Backman	0	0	0	0	0	0	0	0	0	0	0	0	GB	TE		0	2	2015
15407	Matt Simms	0	0	0	0	0	0	0	0	0	0	0	0	ATL	QB		0	2	2015
15839	Marquise Goodwin	0	0	0	0	0	0	0	0	0	0	0	0	BUF	WR	P	0	2	2015
15948	Matt Barkley	0	0	0	0	0	0	0	0	0	0	0	0	ARI	QB		0	2	2015
16066	Kendall Gaskins	0	0	0	0	0	0	0	0	0	0	0	0	SF	RB		0	2	2015
16381	Kenbrell Thompkins	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	WR		0	2	2015
17178	Adam Thielen	0	0	0	0	0	0	0	0	0.2	2.5	0	0.1	MIN	WR		0.45	2	2015
14879	Brock Osweiler	0.1	0.2	1.3	0	0	0.1	0	0.4	0	0	0	0.1	HOU	QB		0.1	2	2015
11708	Matthew Mulligan	0	0	0	0	0	0	0	0	0.1	1	0	0	DET	TE		0.2	2	2015
13198	Jimmy Clausen	0.1	0.2	1.2	0	0	0.1	0	0.4	0	0	0	0	BAL	QB		0.088	2	2015
10487	Drew Stanton	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	ARI	QB		0.048	2	2015
17137	Jeff Janis	0	0	0	0	0	0	0	0	0.3	4.6	0	0.2	GB	WR		0.76	2	2015
11369	Jerome Felton	0	0	0	0	0	0.3	0	0.9	0.3	2.7	0	0.2	BUF	RB		0.66	2	2015
11331	Andre Caldwell	0	0	0	0	0	0	0	0	0.2	3.1	0	0.2	DEN	WR		0.51	2	2015
17170	Ryan Hewitt	0	0	0	0	0	0	0	0	0.3	2.9	0	0.1	CIN	TE		0.59	2	2015
15351	Derrick Coleman	0	0	0	0	0	0	0	0	0.2	1.8	0	0.1	SEA	RB		0.38	2	2015
16797	Troy Niklas	0	0	0	0	0	0	0	0	0.5	5.4	0.1	0.3	ARI	TE		1.6400000000000001	2	2015
17444	MyCole Pruitt	0	0	0	0	0	0	0	0	0.6	7.4	0	0.3	MIN	TE		1.3399999999999999	2	2015
15887	Ryan Griffin	0	0	0	0	0	0	0	0	0.6	6.9	0	0.3	HOU	TE		1.29	2	2015
11383	Tim Hightower	0	0	0	0	0	0.2	0	0.9	0.9	7.7	0	0.2	NO	RB		1.76	2	2015
17601	Rashad Greene	0	0	0	0	0	0	0	0	2.6	32.1	0.2	4.1	JAX	WR		7.010000000000001	2	2015
15832	Knile Davis	0	0	0	0	0	5.4	0.2	21.7	1	7	0	3.6	KC	RB		5.07	2	2015
11658	Mike Tolbert	0	0	0	0	0	3	0.1	11.4	1.6	11.6	0.1	3.2	CAR	RB		5.1	2	2015
60027	Buccaneers	0	0	0	0	0	0	0	0	0	0	0	8.6		DST		8.6	2	2015
11122	Matt Prater	0	0	0	0	0	0	0	0	0	0	0	8.3	DET	K		8.3	2	2015
10636	Mason Crosby	0	0	0	0	0	0	0	0	0	0	0	8.1	GB	K		8.1	2	2015
17169	Chandler Catanzaro	0	0	0	0	0	0	0	0	0	0	0	7.9	ARI	K		7.9	2	2015
12460	Graham Gano	0	0	0	0	0	0	0	0	0	0	0	7.8	CAR	K		7.8	2	2015
11237	Matt Ryan	26.2	39.5	309.8	1.9	0.8	1.9	0.1	5.8	0	0	0	18.2	ATL	QB		23.372	2	2015
5529	Philip Rivers	22.8	35.3	286.7	1.8	0.9	1.9	0	4.3	0	0	0	16.3	SD	QB		18.198	2	2015
13217	Golden Tate	0	0	0	0	0	0.2	0	1.6	5.1	62.8	0.2	7.5	DET	WR		12.739999999999998	2	2015
15826	Giovani Bernard	0	0	0	0	0	8.4	0.3	33.2	3.5	27.6	0.1	7.3	CIN	RB	P	11.98	2	2015
17157	Travis Coons	0	0	0	0	0	0	0	0	0	0	0	7.1	CLE	K		7.1	2	2015
15885	Charles Johnson	0	0	0	0	0	0	0	0	3.8	55.7	0.3	6.9	MIN	WR		11.170000000000002	2	2015
15478	Brandon Bolden	0	0	0	0	0	8.5	0.5	36	0.9	6.8	0	6	NE	RB		8.18	2	2015
14583	Kamar Aiken	0	0	0	0	0	0	0	0	3.3	43.3	0.3	5.7	BAL	WR		9.43	2	2015
16016	Kenny Stills	0	0	0	0	0	0.3	0	1.7	3	43	0.2	5.3	MIA	WR		8.67	2	2015
5615	Matt Schaub	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	ATL	QB		0.052000000000000005	2	2015
16810	AJ McCarron	0.1	0.1	1	0	0	0	0	0	0	0	0	0	CIN	QB		0.04	2	2015
12536	Chase Coffman	0	0	0	0	0	0	0	0	0	0	0	0	SEA	TE		0	2	2015
13218	Arrelious Benn	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR	P	0	2	2015
13376	Clay Harbor	0	0	0	0	0	0	0	0	1.8	20.5	0.1	2.7	NE	TE		4.449999999999999	2	2015
10195	Fred Jackson	0	0	0	0	0	4.8	0.1	19.8	0.8	6.8	0	2	SEA	RB		4.06	2	2015
15974	Dion Sims	0	0	0	0	0	0	0	0	1.4	15.1	0.1	1.7	MIA	TE		3.5100000000000002	2	2015
17742	Malcolm Brown	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	2	2015
17734	Chris Matthews	0	0	0	0	0	0	0	0	0.8	11.6	0.1	1.5	BAL	WR		2.56	2	2015
17600	Jamison Crowder	0	0	0	0	0	0	0	0	1.6	18.2	0.1	1.4	WSH	WR		4.02	2	2015
14360	Matt Asiata	0	0	0	0	0	1.3	0.1	4.9	1	7.2	0	0.6	MIN	RB		2.8099999999999996	2	2015
16775	Crockett Gillmore	0	0	0	0	0	0	0	0	3	33.8	0.3	4.7	BAL	TE	Q	8.18	2	2015
14021	Leonard Hankerson	0	0	0	0	0	0	0	0	3.1	39.3	0.2	4.4	BUF	WR		8.23	2	2015
14129	Bilal Powell	0	0	0	0	0	7	0.2	28.4	2.5	19.7	0.1	4.3	NYJ	RB		9.11	2	2015
10453	Ted Ginn Jr.	0	0	0	0	0	0.3	0	2	2.2	30.9	0.2	4.3	CAR	WR		6.69	2	2015
16569	Myles White	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	2	2015
16838	Tyler Gaffney	0	0	0	0	0	0	0	0	0	0	0	0	NE	RB	P	0	2	2015
16943	Tom Savage	0	0	0	0	0	0	0	0	0	0	0	0	HOU	QB	P	0	2	2015
17193	James Wright	0	0	0	0	0	0	0	0	0	0	0	0	CIN	WR		0	2	2015
17210	Glenn Winston	0	0	0	0	0	0	0	0	0	0	0	0	CLE	RB		0	2	2015
17383	RaShaun Allen	0	0	0	0	0	0	0	0	0	0	0	0	NO	TE		0	2	2015
17450	Randall Telfer	0	0	0	0	0	0	0	0	0	0	0	0	CLE	TE		0	2	2015
17530	Kaelin Clay	0	0	0	0	0	0	0	0	0	0	0	0	BAL	WR		0	2	2015
17572	Garrett Grayson	0	0	0	0	0	0	0	0	0	0	0	0	NO	QB		0	2	2015
17674	Breshad Perriman	0	0	0	0	0	0	0	0	0	0	0	0	BAL	WR	Q	0	2	2015
15457	Travaris Cadet	0	0	0	0	0	0	0	0	0	0	0	0	NO	RB		0	2	2015
15855	Christine Michael	0	0	0	0	0	0	0	0	0	0	0	0	SEA	RB		0	2	2015
15952	Mike Gillislee	0	0	0	0	0	0	0	0	0	0	0	0	BUF	RB		0	2	2015
16074	Michael Hill*	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB	IR	0	2	2015
16418	Zach Sudfeld	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	TE	Q	0	2	2015
17676	Stefon Diggs	0	0	0	0	0	0	0	0	0.2	2.5	0	0.1	MIN	WR		0.45	2	2015
15360	Griff Whalen	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	MIA	WR		0.24	2	2015
16313	Brandon Williams	0	0	0	0	0	0	0	0	0.1	1.1	0	0	MIA	TE		0.21000000000000002	2	2015
16366	Zach Line	0	0	0	0	0	0	0	0	0.1	1	0	0	MIN	RB		0.2	2	2015
14353	Scott Tolzien	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	IND	QB		0.052000000000000005	2	2015
17516	Jalston Fowler	0	0	0	0	0	0.5	0	1.5	0.2	2.1	0	0.2	TEN	RB		0.56	2	2015
14906	James Hanna	0	0	0	0	0	0	0	0	0.2	2.4	0	0.2	DAL	TE		0.44	2	2015
12597	Brandon Tate	0	0	0	0	0	0	0	0	0.1	1.2	0	0.1	CIN	WR		0.22	2	2015
13616	Marlon Moore	0	0	0	0	0	0.1	0	1	0.1	1.9	0	0.1	CLE	WR		0.39	2	2015
16121	Luke Willson	0	0	0	0	0	0	0	0	0.7	8	0.1	0.4	SEA	TE		2.1	2	2015
14083	Bruce Miller	0	0	0	0	0	0.5	0	1.7	0.7	6	0	0.3	SF	RB		1.4699999999999998	2	2015
10482	Zach Miller	0	0	0	0	0	0	0	0	0.5	5.4	0	0.3	SEA	TE		1.04	2	2015
13097	Darrel Young	0	0	0	0	0	0.5	0	1.5	0.6	4.9	0	0.3	WSH	RB		1.24	2	2015
17478	Geremy Davis	0	0	0	0	0	0	0	0	0.5	6.3	0	0.2	NYG	WR		1.13	2	2015
17758	Quincy Enunwa	0	0	0	0	0	0	0	0	2.5	30.9	0.2	4	NYJ	WR		6.79	2	2015
17177	Taylor Gabriel	0	0	0	0	0	0.1	0	1	2.2	30.2	0.1	3.7	CLE	WR		5.92	2	2015
14901	Dwayne Allen	0	0	0	0	0	0	0	0	2.3	24.7	0.2	3.4	IND	TE		5.97	2	2015
15880	Robert Woods	0	0	0	0	0	0	0	0	2.2	28.7	0.2	3.1	BUF	WR		6.2700000000000005	2	2015
12570	Darrius Heyward-Bey	0	0	0	0	0	0	0	0	1.9	25.1	0.2	2.9	PIT	WR		5.61	2	2015
9613	DeAngelo Williams	0	0	0	0	0	14.3	0.2	63.3	2	14.8	0.1	8.6	PIT	RB		11.610000000000001	2	2015
8442	Roddy White	0	0	0	0	0	0	0	0	4.9	61.8	0.4	8.2	ATL	WR		13.48	2	2015
1097	Adam Vinatieri	0	0	0	0	0	0	0	0	0	0	0	8	IND	K		8	2	2015
2148	Sebastian Janikowski	0	0	0	0	0	0	0	0	0	0	0	7.8	OAK	K		7.8	2	2015
8439	Aaron Rodgers	22	34	269	2	1	3	0	16	0	0	0	16.5	GB	QB	P	19.36	2	2015
17610	Marcus Mariota	20.7	32.7	236.7	1.3	1	5.5	0.2	33.1	0	0	0	15.7	TEN	QB		18.177999999999997	2	2015
10456	Marshawn Lynch	0	0	0	0	0	18	1	81	2	15	0	14.8	SEA	RB		17.6	2	2015
16040	C.J. Anderson	0	0	0	0	0	16	1	66	3	26	0	13.9	DEN	RB		18.2	2	2015
14886	Lamar Miller	0	0	0	0	0	16.1	0.5	72.5	2.5	19.5	0.1	11.2	HOU	RB		15.299999999999999	2	2015
11278	Matt Forte	0	0	0	0	0	16	0	68	4	40	0	9.9	NYJ	RB		14.8	2	2015
3609	Josh McCown	16.6	29.1	187.1	0.7	0.9	2.8	0.1	13.9	0	0	0	9	CLE	QB	P	11.373999999999999	2	2015
14024	Greg Little	0	0	0	0	0	0	0	0	0	0	0	0	BUF	WR		0	2	2015
4461	Andre Johnson	0	0	0	0	0	0	0	0	4.3	53.5	0.4	7.6	IND	WR		12.049999999999999	2	2015
12731	Ryan Succop	0	0	0	0	0	0	0	0	0	0	0	7.2	TEN	K		7.2	2	2015
60024	Chargers	0	0	0	0	0	0	0	0	0	0	0	6.6		DST		6.6	2	2015
14017	Shane Vereen	0	0	0	0	0	4.9	0.2	22	3.1	29	0.1	5.8	NYG	RB		10	2	2015
60012	Chiefs	0	0	0	0	0	0	0	0	0	0	0	5.1		DST		5.1	2	2015
12471	Chase Daniel	0.1	0.2	1.1	0	0	0	0	0	0	0	0	0	PHI	QB		0.044000000000000004	2	2015
9780	Bruce Gradkowski	0	0	0	0	0	0	0	0	0	0	0	0	PIT	QB		0	2	2015
15836	Gavin Escobar	0	0	0	0	0	0	0	0	1	12.6	0.2	2.3	DAL	TE	Q	3.46	2	2015
17149	Antonio Andrews	0	0	0	0	0	4	0.2	14.6	0.2	2	0	1.9	TEN	RB		3.0600000000000005	2	2015
15062	Travis Benjamin	0	0	0	0	0	0.3	0	2	1.3	17.4	0.1	1.8	SD	WR		3.84	2	2015
16779	Josh Huff	0	0	0	0	0	0	0	0	0.8	10.1	0.1	1.6	PHI	WR		2.41	2	2015
17738	Seantavius Jones	0	0	0	0	0	0	0	0	0	0	0	0	NO	WR		0	2	2015
16007	Chris Harper	0	0	0	0	0	0	0	0	1	12.7	0.1	1.5	NYG	WR		2.87	2	2015
16002	Kyle Juszczyk	0	0	0	0	0	0	0	0	1.4	11.5	0.1	1.4	BAL	RB		3.15	2	2015
60001	Falcons	0	0	0	0	0	0	0	0	0	0	0	0.7		DST		0.7	2	2015
14888	Isaiah Pead	0	0	0	0	0	2.6	0.1	9.2	0.5	3.9	0	0.4	MIA	RB		2.41	2	2015
14145	Charles Clay	0	0	0	0	0	0	0	0	3.2	34.9	0.3	4.7	BUF	TE	P	8.489999999999998	2	2015
5557	Benjamin Watson	0	0	0	0	0	0	0	0	2.7	31.1	0.2	4.4	BAL	TE		7.010000000000001	2	2015
17665	Dorial Green-Beckham	0	0	0	0	0	0	0	0	2.2	31.1	0.2	4.3	TEN	WR		6.510000000000001	2	2015
16732	Eric Ebron	0	0	0	0	0	0	0	0	3	33.8	0.2	4.2	DET	TE		7.58	2	2015
16730	Kelvin Benjamin	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR	P	0	2	2015
16841	Kevin Norwood	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR		0	2	2015
16950	Lorenzo Taliaferro	0	0	0	0	0	0	0	0	0	0	0	0	BAL	RB		0	2	2015
17194	Isaiah Burse	0	0	0	0	0	0	0	0	0	0	0	0	SD	WR		0	2	2015
17212	Ray Agnew	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB		0	2	2015
17392	Justin Perillo	0	0	0	0	0	0	0	0	0	0	0	0	GB	TE		0	2	2015
17455	Trevor Siemian	0	0	0	0	0	0	0	0	0	0	0	0	DEN	QB		0	2	2015
17533	Sean Mannion	0	0	0	0	0	0	0	0	0	0	0	0	STL	QB		0	2	2015
17584	Devin Smith	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	WR	Q	0	2	2015
17690	Jesse James	0	0	0	0	0	0	0	0	0	0	0	0	PIT	TE		0	2	2015
15910	Michael Williams	0	0	0	0	0	0	0	0	0	0	0	0	NE	TE		0	2	2015
16210	Chris Pantale	0	0	0	0	0	0	0	0	0	0	0	0	CHI	TE		0	2	2015
17754	Khari Lee	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	CHI	TE		0.24	2	2015
17550	Tre McBride	0	0	0	0	0	0	0	0	0.1	1.3	0	0	TEN	WR		0.23	2	2015
14289	Kyle Miller	0	0	0	0	0	0	0	0	0.3	4	0	0.2	SD	TE		0.7	2	2015
14215	Lee Smith	0	0	0	0	0	0	0	0	0.5	5.3	0	0.2	OAK	TE		1.03	2	2015
9400	Lance Moore	0	0	0	0	0	0	0	0	0.2	2.8	0	0.1	DET	WR		0.48	2	2015
17441	James O'Shaughnessy	0	0	0	0	0	0	0	0	0.7	7.5	0.1	0.3	KC	TE	P	2.05	2	2015
17739	Thomas Rawls	0	0	0	0	0	1.4	0	5.9	0.1	0.9	0	0.2	SEA	RB	P	0.78	2	2015
15923	Mychal Rivera	0	0	0	0	0	0	0	0	3	34.8	0.2	4	OAK	TE		7.68	2	2015
17643	Phillip Dorsett	0	0	0	0	0	0.2	0	1.6	2.4	29.2	0.2	3.4	IND	WR		6.680000000000001	2	2015
14820	Chase Reynolds	0	0	0	0	0	6.8	0.2	23.9	0.8	6.8	0	3	STL	RB		5.069999999999999	2	2015
15058	Blair Walsh	0	0	0	0	0	0	0	0	0	0	0	8.4	MIN	K		8.4	2	2015
60010	Titans	0	0	0	0	0	0	0	0	0	0	0	8.2		DST		8.2	2	2015
17580	Ameer Abdullah	0	0	0	0	0	9.6	0.2	43.1	3.6	29.3	0.1	7.8	DET	RB		12.64	2	2015
5526	Eli Manning	24.7	38.7	303	1.9	1	1	0	1.8	0	0	0	17.1	NYG	QB		21.9	2	2015
13197	Sam Bradford	21.9	35.3	255.3	1.9	0.7	2.6	0.1	7.5	0	0	0	16.2	PHI	QB		18.462	2	2015
15848	Eddie Lacy	0	0	0	0	0	18	1	82	3	19	0	14.8	GB	RB		19.099999999999998	2	2015
12483	Matthew Stafford	22.9	37.1	265.5	1.4	0.8	2.3	0.1	7.2	0	0	0	14.1	DET	QB		16.74	2	2015
13229	Rob Gronkowski	0	0	0	0	0	0	0	0	6	71	1	13	NE	TE		19.1	2	2015
12477	Brian Hoyer	19.6	33.6	240.6	1.2	1	2.3	0.1	3.4	0	0	0	11.8	HOU	QB		14.363999999999999	2	2015
13232	Jimmy Graham	0	0	0	0	0	0	0	0	4	48	1	9.9	SEA	TE	Q	14.8	2	2015
15818	Keenan Allen	0	0	0	0	0	0	0	0	5.3	72.2	0.4	9.3	SD	WR	P	14.92	2	2015
14885	Doug Martin	0	0	0	0	0	15.5	0.3	61.5	1.5	12	0	9.1	TB	RB		10.649999999999999	2	2015
13621	David Nelson	0	0	0	0	0	0	0	0	0	0	0	0	PIT	WR		0	2	2015
14159	Kendall Hunter	0	0	0	0	0	0	0	0	0	0	0	0	NO	RB	P	0	2	2015
14875	Robert Griffin	0	0	0	0	0	0	0	0	0	0	0	0	CLE	QB		0	2	2015
14921	Nick Toon	0	0	0	0	0	0	0	0	0	0	0	0	STL	WR		0	2	2015
15364	Jonas Gray	0	0	0	0	0	0	0	0	0	0	0	0	JAX	RB		0	2	2015
16140	Ryan Griffin	0	0	0	0	0	0	0	0	0	0	0	0	TB	QB		0	2	2015
16488	George Winn	0	0	0	0	0	0	0	0	0	0	0	0	DET	RB		0	2	2015
17759	Jerome Cunningham	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	NYG	TE		0.24	2	2015
17191	Jay Prosch	0	0	0	0	0	0.1	0	0.5	0.1	1	0	0.1	HOU	RB		0.25	2	2015
17564	C.J. Uzomah	0	0	0	0	0	0	0	0	0.1	1.2	0	0	CIN	TE		0.22	2	2015
8627	Derek Anderson	0.1	0.2	1.2	0	0	0.2	0	0.5	0	0	0	0	CAR	QB		0.098	2	2015
11319	Craig Stevens	0	0	0	0	0	0	0	0	0.4	4.9	0	0.2	TEN	TE		0.8900000000000001	2	2015
14402	Chris Hogan	0	0	0	0	0	0	0	0	0.4	5.2	0	0.2	NE	WR		0.92	2	2015
14280	Ricardo Lockette	0	0	0	0	0	0	0	0	0.3	4.7	0	0.2	SEA	WR	P	0.77	2	2015
17700	Mike Davis	0	0	0	0	0	0.8	0	3.2	0.1	1	0	0.1	SF	RB		0.52	2	2015
17507	Keith Mumphery	0	0	0	0	0	0	0	0	0.2	2.4	0	0.1	HOU	WR		0.44	2	2015
17756	Adam Humphries	0	0	0	0	0	0	0	0	0.2	2.5	0	0.3	TB	WR		0.45	2	2015
17514	J.J. Nelson	0	0	0	0	0	0	0	0	0.4	5.4	0	0.3	ARI	WR		0.9400000000000001	2	2015
12524	Marcus Thigpen	0	0	0	0	0	0	0	0	0	0	0	0.3	BUF	RB		0.3	2	2015
14518	Shaun Draughn	0	0	0	0	0	1.5	0	5.4	0	0	0	0.3	SF	RB	P	0.54	2	2015
17384	Charcandrick West	0	0	0	0	0	1.1	0	4.4	0.1	1	0	0.2	KC	RB		0.64	2	2015
5633	Jerricho Cotchery	0	0	0	0	0	0	0	0	2.7	35.3	0.2	4	CAR	WR		7.430000000000001	2	2015
10605	Brent Celek	0	0	0	0	0	0	0	0	2.2	24.9	0.2	3.5	PHI	TE		5.89	2	2015
9614	Marcedes Lewis	0	0	0	0	0	0	0	0	2.5	28.2	0.2	3.2	JAX	TE		6.5200000000000005	2	2015
17622	Josh Robinson	0	0	0	0	0	5	0.1	19.1	1.4	11.5	0.1	3	IND	RB		5.66	2	2015
5662	Josh Scobee	0	0	0	0	0	0	0	0	0	0	0	8.4	NO	K		8.4	2	2015
17678	T.J. Yeldon	0	0	0	0	0	12.8	0.4	55.1	1.8	14.6	0.1	8.3	JAX	RB		11.770000000000001	2	2015
16916	Zach Hocker	0	0	0	0	0	0	0	0	0	0	0	8	STL	K		8	2	2015
12579	Jeremy Maclin	0	0	0	0	0	0	0	0	5	64.1	0.3	7.8	KC	WR		13.21	2	2015
1428	Peyton Manning	25	38	298	2	1	2	0	0	0	0	0	16.8	DEN	QB		18.92	2	2015
13994	Cam Newton	18.8	30.8	224.6	1.2	0.7	6.7	0.2	36.1	0	0	0	15.4	CAR	QB		17.894	2	2015
16803	Jeremy Hill	0	0	0	0	0	19	1	85	1	11	0	14.6	CIN	RB		16.6	2	2015
13215	Dez Bryant	0	0	0	0	0	0	0	0	5	83	1	13.9	DAL	WR	P	19.3	2	2015
13983	A.J. Green	0	0	0	0	0	0	0	2	5	76	1	12.9	CIN	WR		18.8	2	2015
14912	Alshon Jeffery	0	0	0	0	0	0.5	0	3.1	5.5	76.5	0.6	10.5	CHI	WR		17.060000000000002	2	2015
60025	49ers	0	0	0	0	0	0	0	0	0	0	0	9.8		DST		9.8	2	2015
17133	Isaiah Crowell	0	0	0	0	0	17.9	0.4	71.4	1	8	0	9.2	CLE	RB		11.340000000000002	2	2015
13945	Tyler Clutts	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB		0	2	2015
14223	Armon Binns	0	0	0	0	0	0	0	0	0	0	0	0	KC	WR		0	2	2015
14904	Ladarius Green	0	0	0	0	0	0	0	0	0	0	0	0	PIT	TE	P	0	2	2015
14881	Russell Wilson	18	28	221	2	1	6	0	39	0	0	0	16.5	SEA	QB		19.74	2	2015
11252	Joe Flacco	23.9	37.5	266.1	1.7	0.9	2.4	0.1	5.2	0	0	0	15.1	BAL	QB	Q	17.664	2	2015
14012	Andy Dalton	19.6	30.3	225.3	1.4	0.9	3.2	0.1	10.4	0	0	0	14.3	CIN	QB		15.351999999999999	2	2015
14005	DeMarco Murray	0	0	0	0	0	15	1	72	2	13	0	13.8	TEN	RB		16.5	2	2015
60026	Seahawks	0	0	0	0	0	0	0	0	0	0	0	12.4		DST		12.4	2	2015
13587	Chris Ivory	0	0	0	0	0	16.8	0.4	73.5	1.7	12.3	0.1	10.4	JAX	RB		13.28	2	2015
8479	Frank Gore	0	0	0	0	0	15.2	0.3	57.3	2.7	21.9	0.1	9.5	IND	RB	P	13.02	2	2015
17134	Alfred Blue	0	0	0	0	0	16.7	0.3	68.8	1.5	12.2	0	9.2	HOU	RB		11.4	2	2015
4333	Matt Bryant	0	0	0	0	0	0	0	0	0	0	0	8.8	ATL	K		8.8	2	2015
13414	Anthony McCoy	0	0	0	0	0	0	0	0	0	0	0	0	SEA	TE		0	2	2015
14051	Ryan Williams	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB	P	0	2	2015
14434	Konrad Reuland	0	0	0	0	0	0	0	0	0	0	0	0	BAL	TE		0	2	2015
14907	Justin Blackmon	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR	SSPD	0	2	2015
15252	Brian Tyms	0	0	0	0	0	0	0	0	0	0	0	0	IND	WR		0	2	2015
16733	Odell Beckham Jr.	0	0	0	0	0	1	0	3	7	91	1	15	NYG	WR		22.4	2	2015
17399	Jameis Winston	19.9	33.4	253.4	1.4	1.1	3	0.1	11.5	0	0	0	14.3	TB	QB		16.386000000000003	2	2015
11307	Jamaal Charles	0	0	0	0	0	15	1	69	3	29	0	13.7	KC	RB	Q	18.8	2	2015
60029	Panthers	0	0	0	0	0	0	0	0	0	0	0	12.4		DST		12.4	2	2015
11247	Jonathan Stewart	0	0	0	0	0	16.5	0.3	72.4	1.6	11.5	0	10.1	CAR	RB		11.79	2	2015
11467	Justin Forsett	0	0	0	0	0	13.8	0.3	57.7	3.9	30	0.1	9.4	BAL	RB	P	15.07	2	2015
8475	Vincent Jackson	0	0	0	0	0	0	0	0	4.7	73.3	0.4	9.2	TB	WR	P	14.430000000000001	2	2015
16763	Jordan Matthews	0	0	0	0	0	0	0	0	4.9	61.4	0.5	8.8	PHI	WR		14.04	2	2015
13484	Joe Webb	0	0	0	0	0	0	0	0	0	0	0	0	CAR	QB		0	2	2015
14080	Ricky Stanzi	0	0	0	0	0	0	0	0	0	0	0	0	NYG	QB		0	2	2015
14500	Jeremy Ross	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	WR		0	2	2015
14910	Stephen Hill	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR	Q	0	2	2015
15257	Phillip Supernaw	0	0	0	0	0	0	0	0	0	0	0	0	TEN	TE		0	2	2015
14053	Randall Cobb	0	0	0	0	0	1	0	7	6	79	1	13	GB	WR	P	20.6	2	2015
16777	Carlos Hyde	0	0	0	0	0	16.3	0.6	68.5	1.4	11.3	0	10.5	SF	RB	P	12.98	2	2015
16731	Brandin Cooks	0	0	0	0	0	0.7	0	4.6	5.8	79	0.4	9.3	NO	WR		16.560000000000002	2	2015
12649	Julian Edelman	0	0	0	0	0	0.5	0	3.6	6.4	67.6	0.5	9	NE	WR	P	16.52	2	2015
13397	Mike Kafka*	0	0	0	0	0	0	0	0	0	0	0	0	MIN	QB	IR	0	2	2015
14433	Allen Reisner	0	0	0	0	0	0	0	0	0	0	0	0	BAL	TE		0	2	2015
15189	Nathan Palmer	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	2	2015
14880	Kirk Cousins	23.3	37.3	272	1.2	1.2	1.3	0	5	0	0	0	12.3	WSH	QB		14.98	2	2015
60004	Bengals	0	0	0	0	0	0	0	0	0	0	0	10		DST		10	2	2015
16055	Benjamin Cunningham	0	0	0	0	0	13.8	0.3	53.5	3.2	26	0.1	9.3	STL	RB		13.549999999999999	2	2015
15920	Latavius Murray	0	0	0	0	0	15.2	0.3	63.8	1.9	14.9	0	9.2	OAK	RB		11.57	2	2015
13553	Victor Cruz	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR	Q	0	2	2015
14156	Niles Paul	0	0	0	0	0	0	0	0	0	0	0	0	WSH	TE	P	0	2	2015
14594	McLeod Bethel-Thompson	0	0	0	0	0	0	0	0	0	0	0	0	MIA	QB		0	2	2015
14914	Brian Quick	0	0	0	0	0	0	0	0	0	0	0	0	STL	WR		0	2	2015
15299	G.J. Kinne	0	0	0	0	0	0	0	0	0	0	0	0	PHI	QB		0	2	2015
14877	Nick Foles	19.2	31.2	218.2	1.1	0.8	2.1	0.1	9.4	0	0	0	10.7	STL	QB		13.867999999999999	2	2015
1440	Phil Dawson	0	0	0	0	0	0	0	0	0	0	0	9.3	SF	K		9.3	2	2015
13353	Dorin Dickerson	0	0	0	0	0	0	0	0	0	0	0	0	TEN	TE	P	0	2	2015
14211	Richard Gordon	0	0	0	0	0	0	0	0	0	0	0	0	DEN	TE		0	2	2015
15092	Bryce Brown	0	0	0	0	0	0	0	0	0	0	0	0	SEA	RB		0	2	2015
14898	Vick Ballard	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	2	2015
15130	Collin Mooney	0	0	0	0	0	0	0	0	0	0	0	0	ATL	RB		0	2	2015
17758	Quincy Enunwa	0	0	0	0	0	0	0	0	2.5	30.9	0.2	4	NYJ	WR		6.79	1	2015
17177	Taylor Gabriel	0	0	0	0	0	0.1	0	1	2.2	30.2	0.1	3.7	CLE	WR		5.92	1	2015
17643	Phillip Dorsett	0	0	0	0	0	0.2	0	1.6	2.4	29.2	0.2	3.4	IND	WR		6.680000000000001	1	2015
13555	Jeff Cumberland	0	0	0	0	0	0	0	0	2.3	28.5	0.2	3.2	NYJ	TE		6.3500000000000005	1	2015
17622	Josh Robinson	0	0	0	0	0	5	0.1	19.1	1.4	11.5	0.1	3	IND	RB		5.66	1	2015
15102	Rishard Matthews	0	0	0	0	0	0	0	0	0.7	9.2	0.1	0.3	TEN	WR		2.2199999999999998	1	2015
10482	Zach Miller	0	0	0	0	0	0	0	0	0.5	5.4	0	0.3	SEA	TE		1.04	1	2015
17677	Amari Cooper	0	0	0	0	0	0.2	0	1.5	5	67.2	0.3	7.7	OAK	WR		13.670000000000002	1	2015
11283	DeSean Jackson	0	0	0	0	0	0.2	0	1.5	4.4	66.5	0.3	7.5	WSH	WR		13	1	2015
60002	Bills	0	0	0	0	0	0	0	0	0	0	0	7.4		DST		7.4	1	2015
12731	Ryan Succop	0	0	0	0	0	0	0	0	0	0	0	7.2	TEN	K		7.2	1	2015
16730	Kelvin Benjamin	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR	P	0	1	2015
5528	Larry Fitzgerald	0	0	0	0	0	0	0	0	4.5	56.5	0.4	7.7	ARI	WR		12.55	1	2015
10621	Nick Folk	0	0	0	0	0	0	0	0	0	0	0	7.5	NYJ	K	P	7.5	1	2015
17438	Jason Myers	0	0	0	0	0	0	0	0	0	0	0	7.4	JAX	K		7.4	1	2015
6016	Malcom Floyd	0	0	0	0	0	0	0	0	3.1	53.6	0.4	7.2	SD	WR		10.860000000000001	1	2015
15847	Travis Kelce	0	0	0	0	0	0	0	0	4.8	51.5	0.4	7	KC	TE		12.35	1	2015
16781	Paul Richardson	0	0	0	0	0	0	0	0	0	0	0	0	SEA	WR	P	0	1	2015
17173	Cody Parkey	0	0	0	0	0	0	0	0	0	0	0	7.7	PHI	K	P	7.7	1	2015
14816	Kai Forbath	0	0	0	0	0	0	0	0	0	0	0	7.3	NO	K		7.3	1	2015
15885	Charles Johnson	0	0	0	0	0	0	0	0	3.8	55.7	0.3	6.9	MIN	WR		11.170000000000002	1	2015
16886	Martavis Bryant	0	0	0	0	0	0	0	0	0	0	0	0	PIT	WR	SSPD	0	1	2015
17200	Jonathan Krause	0	0	0	0	0	0	0	0	0	0	0	0	PHI	WR		0	1	2015
17741	Zach Zenner	0	0	0	0	0	0.5	0	2	0	0	0	0.1	DET	RB	P	0.2	1	2015
15670	Cooper Helfet	0	0	0	0	0	0	0	0	0.1	1	0	0.1	SEA	TE		0.2	1	2015
17717	Mario Alford	0	0	0	0	0	0	0	0	0.1	1.2	0	0	CIN	WR		0.22	1	2015
12519	Cedric Peerman	0	0	0	0	0	0.2	0	0.6	0	0	0	0	CIN	RB		0.06	1	2015
9704	Stephen Gostkowski	0	0	0	0	0	0	0	0	0	0	0	8.5	NE	K		8.5	1	2015
15009	Alfred Morris	0	0	0	0	0	13.8	0.4	52.1	1.4	11.1	0	8.3	DAL	RB		10.12	1	2015
4680	Josh Brown	0	0	0	0	0	0	0	0	0	0	0	8.1	NYG	K		8.1	1	2015
16799	Allen Robinson	0	0	0	0	0	0	0	0	5	66.4	0.3	7.8	JAX	WR		13.440000000000001	1	2015
13940	James Develin	0	0	0	0	0	0	0	0	0	0	0	0	NE	RB		0	1	2015
60020	Jets	0	0	0	0	0	0	0	0	0	0	0	7.6		DST		7.6	1	2015
15826	Giovani Bernard	0	0	0	0	0	8.4	0.3	33.2	3.5	27.6	0.1	7.3	CIN	RB	P	11.98	1	2015
17157	Travis Coons	0	0	0	0	0	0	0	0	0	0	0	7.1	CLE	K		7.1	1	2015
9638	Greg Jennings	0	0	0	0	0	0	0	0	3.7	51.5	0.3	6.9	MIA	WR		10.650000000000002	1	2015
16787	Marqise Lee	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR		0	1	2015
16950	Lorenzo Taliaferro	0	0	0	0	0	0	0	0	0	0	0	0	BAL	RB		0	1	2015
17198	Damian Copeland	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR		0	1	2015
17676	Stefon Diggs	0	0	0	0	0	0	0	0	0.2	2.5	0	0.1	MIN	WR		0.45	1	2015
17191	Jay Prosch	0	0	0	0	0	0.1	0	0.5	0.1	1	0	0.1	HOU	RB		0.25	1	2015
15647	Chase Ford	0	0	0	0	0	0	0	0	0.1	1.2	0	0	BAL	TE		0.22	1	2015
14851	Terrelle Pryor	0	0	0	0	0	0.3	0	2	0	2	0	0	CLE	WR		0.4	1	2015
15795	DeAndre Hopkins	0	0	0	0	0	0	0	0	5.1	71.6	0.3	8.6	HOU	WR		14.059999999999999	1	2015
9705	Brandon Marshall	0	0	0	0	0	0	0	0	5	60.6	0.4	8.4	NYJ	WR		13.46	1	2015
11923	Steven Hauschka	0	0	0	0	0	0	0	0	0	0	0	8.2	SEA	K		8.2	1	2015
1097	Adam Vinatieri	0	0	0	0	0	0	0	0	0	0	0	8	IND	K		8	1	2015
8461	Mike Nugent	0	0	0	0	0	0	0	0	0	0	0	7.8	CIN	K		7.8	1	2015
13484	Joe Webb	0	0	0	0	0	0	0	0	0	0	0	0	CAR	QB		0	1	2015
16733	Odell Beckham	0	0	0	0	0	1	0	3	7	91	1	15	NYG	WR		22.4	1	2015
14211	Richard Gordon	0	0	0	0	0	0	0	0	0	0	0	0	DEN	TE		0	1	2015
14898	Vick Ballard	0	0	0	0	0	0	0	0	0	0	0	0	IND	RB		0	1	2015
15187	Austin Davis	0	0	0	0	0	0	0	0	0	0	0	0	CLE	QB		0	1	2015
11237	Matt Ryan	26.2	39.5	309.8	1.9	0.8	1.9	0.1	5.8	0	0	0	18.2	ATL	QB		23.372	1	2015
5529	Philip Rivers	22.8	35.3	286.7	1.8	0.9	1.9	0	4.3	0	0	0	16.3	SD	QB		18.198	1	2015
17399	Jameis Winston	19.9	33.4	253.4	1.4	1.1	3	0.1	11.5	0	0	0	14.3	TB	QB		16.386000000000003	1	2015
15383	Dominique Jones	0	0	0	0	0	0	0	0	0	0	0	0	DEN	TE		0	1	2015
15825	Le'Veon Bell	0	0	0	0	0	0	0	0	0	0	0	0	PIT	RB	Q	0	1	2015
16066	Kendall Gaskins	0	0	0	0	0	0	0	0	0	0	0	0	SF	RB		0	1	2015
17718	DeAndre Smelter	0	0	0	0	0	0	0	0	0	0	0	0	SF	WR	P	0	1	2015
15391	Rod Streater	0	0	0	0	0	0	0	0	1.9	25.8	0.1	2.6	KC	WR		5.08	1	2015
16172	Jaron Brown	0	0	0	0	0	0	0	0	1.5	18.4	0.2	1.9	ARI	WR		4.54	1	2015
15845	Justin Hunter	0	0	0	0	0	0	0	0	1.3	19.4	0.1	1.7	TEN	WR	P	3.8400000000000003	1	2015
10287	Daniel Fells	0	0	0	0	0	0	0	0	1	11.3	0.1	1.5	NYG	TE	Q	2.73	1	2015
16766	C.J. Fiedorowicz	0	0	0	0	0	0	0	0	1.1	11.9	0.1	1.4	HOU	TE		2.89	1	2015
17172	Cairo Santos	0	0	0	0	0	0	0	0	0	0	0	7.6	KC	K		7.6	1	2015
9354	Robbie Gould	0	0	0	0	0	0	0	0	0	0	0	7.4	CHI	K		7.4	1	2015
16804	John Brown	0	0	0	0	0	0.3	0	1.7	4.1	57.6	0.4	7.3	ARI	WR		12.43	1	2015
16944	Devonta Freeman	0	0	0	0	0	8.5	0.3	32.7	2.4	23.5	0.1	7.1	ATL	RB		10.42	1	2015
60011	Colts	0	0	0	0	0	0	0	0	0	0	0	6.7		DST		6.7	1	2015
16809	Garrett Gilbert	0	0	0	0	0	0	0	0	0	0	0	0	DET	QB		0	1	2015
16915	Keith Wenning	0	0	0	0	0	0	0	0	0	0	0	0	CIN	QB		0	1	2015
17150	Tevin Reese	0	0	0	0	0	0	0	0	0	0	0	0	CIN	WR		0	1	2015
17208	Orleans Darkwa	0	0	0	0	0	0	0	0	0	0	0	0	NYG	RB		0	1	2015
17353	Marcel Jensen	0	0	0	0	0	0	0	0	0	0	0	0	WSH	TE		0	1	2015
17754	Khari Lee	0	0	0	0	0	0	0	0	0.1	1.4	0	0.1	CHI	TE		0.24	1	2015
12482	Mark Sanchez	0.1	0.2	1.3	0	0	0.2	0	0.5	0	0	0	0.1	DEN	QB		0.10200000000000001	1	2015
17524	Malcolm Johnson	0	0	0	0	0	0	0	0	0.2	1.8	0	0	CLE	TE		0.38	1	2015
2549	Mike Vick	0.1	0.2	1.3	0	0	0.1	0	0.4	0	0	0	0	PIT	QB		0.092	1	2015
60027	Buccaneers	0	0	0	0	0	0	0	0	0	0	0	8.6		DST		8.6	1	2015
14924	T.Y. Hilton	0	0	0	0	0	0.2	0	1.6	4.8	65.5	0.4	8.4	IND	WR		13.91	1	2015
8442	Roddy White	0	0	0	0	0	0	0	0	4.9	61.8	0.4	8.2	ATL	WR		13.48	1	2015
14322	Dan Bailey	0	0	0	0	0	0	0	0	0	0	0	8	DAL	K		8	1	2015
12460	Graham Gano	0	0	0	0	0	0	0	0	0	0	0	7.8	CAR	K		7.8	1	2015
13553	Victor Cruz	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR	Q	0	1	2015
17171	Brandon McManus	0	0	0	0	0	0	0	0	0	0	0	7.6	DEN	K		7.6	1	2015
16800	Davante Adams	0	0	0	0	0	0	0	0	4	57.7	0.4	7.4	GB	WR	P	12.17	1	2015
16725	Sammy Watkins	0	0	0	0	0	0.3	0	1.9	4.1	56.2	0.4	7.3	BUF	WR		12.31	1	2015
60034	Texans	0	0	0	0	0	0	0	0	0	0	0	7.1		DST		7.1	1	2015
13271	Eric Decker	0	0	0	0	0	0	0	0	4.6	58.5	0.3	6.7	NYJ	WR		12.25	1	2015
16813	Logan Thomas	0	0	0	0	0	0	0	0	0	0	0	0	MIA	QB		0	1	2015
16928	Jalen Saunders	0	0	0	0	0	0	0	0	0	0	0	0	NE	WR	SSPD	0	1	2015
17154	Brenton Bersin	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR		0	1	2015
17209	Senorise Perry	0	0	0	0	0	0	0	0	0	0	0	0	CHI	RB	P	0	1	2015
15968	Mike James	0	0	0	0	0	0.5	0	2	0	0	0	0.1	TB	RB		0.2	1	2015
15896	Brice Butler	0	0	0	0	0	0	0	0	0.2	2.8	0	0.1	DAL	WR		0.48	1	2015
13987	Blaine Gabbert	0.1	0.2	1.3	0	0	0.1	0	0.4	0	0	0	0	SF	QB		0.092	1	2015
15091	Randy Bullock	0	0	0	0	0	0	0	0	0	0	0	8.5	NYJ	K		8.5	1	2015
9838	Marques Colston	0	0	0	0	0	0	0	0	4	60.1	0.3	8	NO	WR		11.809999999999999	1	2015
13231	Dennis Pitta	0	0	0	0	0	0	0	0	0	0	0	0	BAL	TE	Q	0	1	2015
14028	Stevan Ridley	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	RB		0	1	2015
14910	Stephen Hill	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR	Q	0	1	2015
14001	Colin Kaepernick	19.8	31	244	1.1	0.4	6.6	0.2	40.6	0	0	0	17.1	SF	QB	P	19.02	1	2015
13994	Cam Newton	18.8	30.8	224.6	1.2	0.7	6.7	0.2	36.1	0	0	0	15.4	CAR	QB		17.894	1	2015
8416	Alex Smith	21.1	34	225.9	1.3	0.6	3.4	0.1	17.5	0	0	0	14.4	KC	QB		15.986	1	2015
11307	Jamaal Charles	0	0	0	0	0	15	1	69	3	29	0	13.7	KC	RB	Q	18.8	1	2015
15803	EJ Manuel	0	0	0	0	0	0	0	0	0	0	0	0	BUF	QB		0	1	2015
15910	Michael Williams	0	0	0	0	0	0	0	0	0	0	0	0	NE	TE		0	1	2015
16015	Brad Sorensen	0	0	0	0	0	0	0	0	0	0	0	0	SD	QB		0	1	2015
17715	Vince Mayle	0	0	0	0	0	0	0	0	0	0	0	0	DAL	WR		0	1	2015
16736	Johnny Manziel	0.1	0.1	0.9	0	0	0	0	0	0	0	0	0	CLE	QB		0.036000000000000004	1	2015
16782	Jerick McKinnon	0	0	0	0	0	3.8	0.1	17	1.4	11	0	2.7	MIN	RB		4.8	1	2015
17149	Antonio Andrews	0	0	0	0	0	4	0.2	14.6	0.2	2	0	1.9	TEN	RB		3.0600000000000005	1	2015
14007	Lance Kendricks	0	0	0	0	0	0	0	0	1.8	19.5	0.1	1.8	STL	TE		4.35	1	2015
16755	Dri Archer	0	0	0	0	0	2	0	7.6	1.8	14.6	0.1	1.7	NYJ	RB		4.619999999999999	1	2015
17746	Josh Lambo	0	0	0	0	0	0	0	0	0	0	0	7.6	SD	K		7.6	1	2015
4512	Anquan Boldin	0	0	0	0	0	0	0	0	4.9	60.2	0.2	7.4	SF	WR		12.120000000000001	1	2015
17668	Nelson Agholor	0	0	0	0	0	0	0	0	4.1	55.9	0.4	7.3	PHI	WR		12.09	1	2015
17743	Kyle Brindza	0	0	0	0	0	0	0	0	0	0	0	7	NYJ	K	D	7	1	2015
16496	Cierre Wood	0	0	0	0	0	0	0	0	0	0	0	0	BUF	RB	Q	0	1	2015
16821	David Fales	0	0	0	0	0	0	0	0	0	0	0	0	CHI	QB		0	1	2015
16942	Ka'Deem Carey	0	0	0	0	0	0	0	0	0	0	0	0	CHI	RB		0	1	2015
11688	Dan Carpenter	0	0	0	0	0	0	0	0	0	0	0	7.6	BUF	K		7.6	1	2015
11295	Martellus Bennett	0	0	0	0	0	0	0	0	4.7	50.4	0.4	7.4	NE	TE		12.14	1	2015
11458	Stevie Johnson	0	0	0	0	0	0	0	0	4.1	54.8	0.4	7.1	SD	WR	P	11.979999999999999	1	2015
11276	Eddie Royal	0	0	0	0	0	0	0	0	4.5	53.8	0.3	7	CHI	WR		11.68	1	2015
16784	Tre Mason	0	0	0	0	0	0	0	0	0	0	0	0	STL	RB		0	1	2015
16841	Kevin Norwood	0	0	0	0	0	0	0	0	0	0	0	0	CAR	WR		0	1	2015
17147	Arthur Lynch	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	TE	SSPD	0	1	2015
17194	Isaiah Burse	0	0	0	0	0	0	0	0	0	0	0	0	SD	WR		0	1	2015
17346	Julian Talley	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	1	2015
10770	Eric Weems	0	0	0	0	0	0	0	0	0.1	1.7	0	0.1	ATL	WR		0.27	1	2015
13524	Seyi Ajirotutu	0	0	0	0	0	0	0	0	0.1	1.3	0	0.1	PHI	WR		0.23	1	2015
17749	Alonzo Harris	0	0	0	0	0	0.3	0	1	0	0	0	0	GB	RB		0.1	1	2015
16760	Jimmy Garoppolo	0.1	0.2	1.2	0	0	0	0	0	0	0	0	0	NE	QB		0.048	1	2015
14911	Rueben Randle	0	0	0	0	0	0	0	0	4.7	67.9	0.4	8.3	PHI	WR		13.890000000000002	1	2015
60010	Titans	0	0	0	0	0	0	0	0	0	0	0	8.2		DST		8.2	1	2015
17169	Chandler Catanzaro	0	0	0	0	0	0	0	0	0	0	0	7.9	ARI	K		7.9	1	2015
14032	Torrey Smith	0	0	0	0	0	0	0	0	3.7	61.5	0.3	7.7	SF	WR		11.650000000000002	1	2015
13217	Golden Tate	0	0	0	0	0	0.2	0	1.6	5.1	62.8	0.2	7.5	DET	WR		12.739999999999998	1	2015
4461	Andre Johnson	0	0	0	0	0	0	0	0	4.3	53.5	0.4	7.6	IND	WR		12.049999999999999	1	2015
60016	Vikings	0	0	0	0	0	0	0	0	0	0	0	7.2		DST		7.2	1	2015
16790	Jarvis Landry	0	0	0	0	0	0.3	0	1.7	5.5	58.7	0.3	7.2	MIA	WR		13.34	1	2015
8444	Heath Miller	0	0	0	0	0	0	0	0	4.6	51.3	0.4	7	PIT	TE		12.13	1	2015
16569	Myles White	0	0	0	0	0	0	0	0	0	0	0	0	NYG	WR		0	1	2015
16792	Jace Amaro	0	0	0	0	0	0	0	0	0	0	0	0	NYJ	TE	P	0	1	2015
16943	Tom Savage	0	0	0	0	0	0	0	0	0	0	0	0	HOU	QB	P	0	1	2015
17021	Aaron Murray	0	0	0	0	0	0	0	0	0	0	0	0	KC	QB		0	1	2015
17196	Alex Bayer	0	0	0	0	0	0	0	0	0	0	0	0	STL	TE		0	1	2015
17197	Jeremy Butler	0	0	0	0	0	0	0	0	0	0	0	0	BAL	WR		0	1	2015
17179	Ryan Grant	0	0	0	0	0	0	0	0	0.2	2.9	0	0.1	WSH	WR		0.49	1	2015
17178	Adam Thielen	0	0	0	0	0	0	0	0	0.2	2.5	0	0.1	MIN	WR		0.45	1	2015
14879	Brock Osweiler	0.1	0.2	1.3	0	0	0.1	0	0.4	0	0	0	0.1	HOU	QB		0.092	1	2015
12489	Donald Brown	0	0	0	0	0	0.2	0	0.9	0.1	1.1	0	0.1	NE	RB		0.30000000000000004	1	2015
16313	Brandon Williams	0	0	0	0	0	0	0	0	0.1	1.1	0	0	MIA	TE		0.21000000000000002	1	2015
17550	Tre McBride	0	0	0	0	0	0	0	0	0.1	1.3	0	0	TEN	WR		0.23	1	2015
16366	Zach Line	0	0	0	0	0	0	0	0	0.1	1	0	0	MIN	RB		0.2	1	2015
15921	Kenjon Barner	0	0	0	0	0	0.1	0	0.6	0	0	0	0	PHI	RB		0.06	1	2015
14353	Scott Tolzien	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	IND	QB		0.052000000000000005	1	2015
9613	DeAngelo Williams	0	0	0	0	0	14.3	0.2	63.3	2	14.8	0.1	8.6	PIT	RB	P	11.610000000000001	1	2015
5662	Josh Scobee	0	0	0	0	0	0	0	0	0	0	0	8.4	NO	K		8.4	1	2015
15683	Justin Tucker	0	0	0	0	0	0	0	0	0	0	0	8.3	BAL	K		8.3	1	2015
60005	Browns	0	0	0	0	0	0	0	0	0	0	0	8.3		DST		8.3	1	2015
14993	Greg Zuerlein	0	0	0	0	0	0	0	0	0	0	0	8.1	STL	K		8.1	1	2015
16916	Zach Hocker	0	0	0	0	0	0	0	0	0	0	0	8	STL	K		8	1	2015
12503	Rashad Jennings	0	0	0	0	0	12.3	0.4	52.7	1.2	9.2	0	7.8	NYG	RB		9.790000000000001	1	2015
12579	Jeremy Maclin	0	0	0	0	0	0	0	0	5	64.1	0.3	7.8	KC	WR		13.21	1	2015
13397	Mike Kafka*	0	0	0	0	0	0	0	0	0	0	0	0	MIN	QB	IR	0	1	2015
14051	Ryan Williams	0	0	0	0	0	0	0	0	0	0	0	0	DAL	RB	P	0	1	2015
14500	Jeremy Ross	0	0	0	0	0	0	0	0	0	0	0	0	OAK	WR		0	1	2015
14914	Brian Quick	0	0	0	0	0	0	0	0	0	0	0	0	STL	WR		0	1	2015
15257	Phillip Supernaw	0	0	0	0	0	0	0	0	0	0	0	0	TEN	TE		0	1	2015
1428	Peyton Manning	25	38	298	2	1	2	0	0	0	0	0	16.8	DEN	QB		18.92	1	2015
13934	Antonio Brown	0	0	0	0	0	0	0	2	8	107	1	16.1	PIT	WR		27.9	1	2015
12514	LeSean McCoy	0	0	0	0	0	18	1	83	2	16	0	14.8	BUF	RB		17.900000000000002	1	2015
13216	Demaryius Thomas	0	0	0	0	0	0	0	0	7	83	1	14	DEN	WR		21.3	1	2015
15457	Travaris Cadet	0	0	0	0	0	0	0	0	0	0	0	0	NO	RB		0	1	2015
15935	Zac Dysert	0	0	0	0	0	0	0	0	0	0	0	0	MIA	QB		0	1	2015
16020	Spencer Ware	0	0	0	0	0	0	0	0	0	0	0	0	KC	RB		0	1	2015
16373	Rodney Smith	0	0	0	0	0	0	0	0	0	0	0	0	DAL	WR		0	1	2015
17750	Cameron Meredith	0	0	0	0	0	0	0	0	0	0	0	0	CHI	WR		0	1	2015
60003	Bears	0	0	0	0	0	0	0	0	0	0	0	-1.2		DST		-1.2	1	2015
14870	Will Johnson	0	0	0	0	0	3.7	0.1	12	1.1	9.9	0.1	2.1	PIT	RB		4.49	1	2015
60019	Giants	0	0	0	0	0	0	0	0	0	0	0	2.1		DST		2.1	1	2015
17140	Juwan Thompson	0	0	0	0	0	2.9	0.1	11.7	0.3	2.4	0	1.9	DEN	RB		2.3099999999999996	1	2015
15062	Travis Benjamin	0	0	0	0	0	0.3	0	2	1.3	17.4	0.1	1.8	SD	WR		3.84	1	2015
12506	David Johnson	0	0	0	0	0	0	0	0	1.2	13.4	0.1	1.7	SD	TE		3.14	1	2015
15994	Theo Riddick	0	0	0	0	0	1	0	3.7	1.7	14.7	0.1	1.5	DET	RB		4.140000000000001	1	2015
13621	David Nelson	0	0	0	0	0	0	0	0	0	0	0	0	PIT	WR		0	1	2015
14433	Allen Reisner	0	0	0	0	0	0	0	0	0	0	0	0	BAL	TE		0	1	2015
14907	Justin Blackmon	0	0	0	0	0	0	0	0	0	0	0	0	JAX	WR	SSPD	0	1	2015
14876	Ryan Tannehill	23.5	35.9	277.1	1.7	0.8	3.3	0.1	16.2	0	0	0	17.5	MIA	QB		19.304000000000002	1	2015
10452	Adrian Peterson	0	0	0	0	0	18	1	82	3	26	0	15.7	MIN	RB		19.8	1	2015
16803	Jeremy Hill	0	0	0	0	0	19	1	85	1	11	0	14.6	CIN	RB		16.6	1	2015
14005	DeMarco Murray	0	0	0	0	0	15	1	72	2	13	0	13.8	TEN	RB		16.5	1	2015
15688	Brandon Bostick	0	0	0	0	0	0	0	0	0	0	0	0	MIN	TE		0	1	2015
17586	Melvin Gordon	0	0	0	0	0	15.3	0.3	64.2	1.6	12.7	0.1	8.9	SD	RB	Q	11.689999999999998	1	2015
5631	Luke McCown	0.1	0.2	1.4	0	0	0	0	0	0	0	0	0	NO	QB		0.055999999999999994	1	2015
13199	Colt McCoy	0.1	0.2	1.3	0	0	0	0	0	0	0	0	0	WSH	QB		0.052000000000000005	1	2015
4527	Jason Witten	0	0	0	0	0	0	0	0	3.8	44.8	0.4	6.5	DAL	TE		10.68	1	2015
60028	Redskins	0	0	0	0	0	0	0	0	0	0	0	5.8		DST		5.8	1	2015
15428	Jermaine Kearse	0	0	0	0	0	0	0	0	2.7	40.5	0.3	5.7	SEA	WR		8.55	1	2015
12563	Michael Crabtree	0	0	0	0	0	0	0	0	4.2	49.5	0.2	5.3	OAK	WR		10.350000000000001	1	2015
15632	Jonathan Grimes	0	0	0	0	0	5.6	0.1	23.3	1.7	14.1	0	3.9	HOU	RB		6.04	1	2015
15832	Knile Davis	0	0	0	0	0	5.4	0.2	21.7	1	7	0	3.6	KC	RB		5.07	1	2015
16023	Marquess Wilson	0	0	0	0	0	0	0	0	2.3	28.5	0.2	3.2	CHI	WR	P	6.3500000000000005	1	2015
17593	DeVante Parker	0	0	0	0	0	0	0	0	2	27.8	0.2	3.1	MIA	WR		5.98	1	2015
16121	Luke Willson	0	0	0	0	0	0	0	0	0.7	8	0.1	0.4	SEA	TE		2.1	1	2015
17514	J.J. Nelson	0	0	0	0	0	0	0	0	0.4	5.4	0	0.3	ARI	WR		0.9400000000000001	1	2015
15853	Vance McDonald	0	0	0	0	0	0	0	0	0.6	6.9	0	0.2	SF	TE		1.29	1	2015
13409	Marc Mariani	0	0	0	0	0	0	0	0	0.1	1.4	0	0.2	CHI	WR		0.24	1	2015
15973	Tommy Bohanon	0	0	0	0	0	0.5	0	1.7	0.2	2.1	0	0.2	NYJ	RB		0.5800000000000001	1	2015
14916	Keshawn Martin	0	0	0	0	0	0	0	0	0.1	1.2	0	0.2	NE	WR		0.22	1	2015
17170	Ryan Hewitt	0	0	0	0	0	0	0	0	0.3	2.9	0	0.1	CIN	TE		0.59	1	2015
\.


--
-- Data for Name: ffn_players; Type: TABLE DATA; Schema: public; Owner: Raymond
--

COPY ffn_players (ffn_id, active, fname, lname, full_name, team, "position", height, weight, college, jersey, dob) FROM stdin;
1	f	Erik	Ainge	Erik Ainge	NYJ	QB				0	0000-00-00
2	t	Derek	Anderson	Derek Anderson	CAR	QB				3	1969-12-31
3	f	Matt	Baker	Matt Baker	BUF	QB	6-2	217	North Carolina	0	0000-00-00
5	f	Brett	Basanez	Brett Basanez	CHI	QB	6-1	208	Northwestern	14	0000-00-00
4	f	Richard	Bartel	Richard Bartel	ARI	QB	6-4	230	Tarleton State	2	1983-02-03
6	f	Charlie	Batch	Charlie Batch	PIT	QB	6-2	216	Eastern Michigan	16	1974-12-05
7	f	John	Beck	John Beck	HOU	QB	6-2	215	Brigham Young	12	1981-08-21
8	f	Brock	Berlin	Brock Berlin	DET	QB	6-1	215	Miami (Fla.)	0	0000-00-00
9	f	Kyle	Boller	Kyle Boller	OAK	QB	6-3	220	California	7	0000-00-00
10	f	Brooks	Bollinger	Brooks Bollinger	DET	QB	6-1	205	Wisconsin	0	0000-00-00
12	f	Todd	Bouman	Todd Bouman	JAC	QB	6-2	236	St. Cloud State	4	1972-08-01
13	t	Tom	Brady	Tom Brady	NE	QB				12	1969-12-31
14	t	Drew	Brees	Drew Brees	NO	QB				9	1969-12-31
15	f	Colt	Brennan	Colt Brennan	OAK	QB	6-3	215	Hawaii	5	0000-00-00
16	f	Alex	Brink	Alex Brink	HOU	QB	6-2	208	Washington State	0	0000-00-00
17	f	Brian	Brohm	Brian Brohm	BUF	QB	6-3	223	Louisville	4	0000-00-00
18	f	Mark	Brunell	Mark Brunell	NYJ	QB	6-1	215	Washington	8	0000-00-00
19	f	Marc	Bulger	Marc Bulger	BAL	QB	6-3	208	West Virginia	10	0000-00-00
20	t	Jason	Campbell	Jason Campbell	CIN	QB	6-5	235	Auburn	17	0000-00-00
21	f	David	Carr	David Carr	NYG	QB	6-3	212	Fresno State	8	0000-00-00
22	t	Matt	Cassel	Matt Cassel	DAL	QB				16	1969-12-31
23	t	Kellen	Clemens	Kellen Clemens	SD	QB				10	1969-12-31
24	f	Kerry	Collins	Kerry Collins	IND	QB	6-5	247	Penn State	5	0000-00-00
25	f	Todd	Collins	Todd Collins	CHI	QB	6-4	223	Michigan	10	0000-00-00
26	f	Brodie	Croyle	Brodie Croyle	ARI	QB	6-2	206	Alabama	12	1983-02-06
27	f	Daunte	Culpepper	Daunte Culpepper	DET	QB	6-4	260	Central Florida	0	0000-00-00
28	t	Jay	Cutler	Jay Cutler	CHI	QB				6	1969-12-31
29	f	Jake	Delhomme	Jake Delhomme	HOU	QB	6-2	215	Louisiana-Lafayette	17	0000-00-00
30	t	Dennis	Dixon	Dennis Dixon	BUF	QB	6-3	215	Oregon	6	0000-00-00
31	f	Ken	Dorsey	Ken Dorsey	CLE	QB				0	0000-00-00
32	t	Trent	Edwards	Trent Edwards	OAK	QB	6-4	230	Stanford	5	0000-00-00
33	f	A.J.	Feeley	A.J. Feeley	STL	QB	6-3	216	Oregon	4	0000-00-00
34	t	Ryan	Fitzpatrick	Ryan Fitzpatrick	NYJ	QB				14	1969-12-31
35	t	Joe	Flacco	Joe Flacco	BAL	QB				5	1969-12-31
36	t	Matt	Flynn	Matt Flynn	NO	QB				5	1969-12-31
37	f	Gus	Frerotte	Gus Frerotte	MIN	QB	6-3	233	Tulsa	0	0000-00-00
38	f	Charlie	Frye	Charlie Frye	OAK	QB	6-4	220	Akron	3	0000-00-00
39	f	Jeff	Garcia	Jeff Garcia	HOU	QB	6-1	205	San Jose State	0	0000-00-00
40	t	David	Garrard	David Garrard	NYJ	QB	6-1	236	East Carolina	4	0000-00-00
41	t	Bruce	Gradkowski	Bruce Gradkowski	PIT	QB				5	1969-12-31
42	f	Quinn	Gray	Quinn Gray	KC	QB	6-3	254	Florida A&M	0	0000-00-00
43	f	Trent	Green	Trent Green	STL	QB	6-3	208	Indiana	0	0000-00-00
44	f	Brian	Griese	Brian Griese	TB	QB	6-3	214	Michigan	0	0000-00-00
45	f	Rex	Grossman	Rex Grossman	ATL	QB	6-1	225	Florida	8	1980-08-23
46	f	Matt	Gutierrez	Matt Gutierrez	WAS	QB	6-4	230	Idaho State	0	1984-06-09
47	f	Darrell	Hackney	Darrell Hackney	DEN	QB	6-0	248	Alabama-Birmingham	0	0000-00-00
48	f	Gibran	Hamdan	Gibran Hamdan	BUF	QB	6-4	220	Indiana	0	0000-00-00
49	t	Caleb	Hanie	Caleb Hanie	DAL	QB	6-2	235	Colorado State	7	0000-00-00
50	f	Joey	Harrington	Joey Harrington	NO	QB	6-4	210	Oregon	0	0000-00-00
51	t	Matt	Hasselbeck	Matt Hasselbeck	IND	QB				8	1969-12-31
52	t	Chad	Henne	Chad Henne	JAC	QB				7	1969-12-31
53	f	Drew	Henson	Drew Henson	DET	QB	6-4	235	Michigan	0	0000-00-00
54	t	Shaun	Hill	Shaun Hill	MIN	QB				13	1969-12-31
55	f	Damon	Huard	Damon Huard	SF	QB	6-3	218	Washington	0	0000-00-00
56	t	Tarvaris	Jackson	Tarvaris Jackson	SEA	QB				7	1969-12-31
57	f	Brad	Johnson	Brad Johnson	DAL	QB	6-5	235	Florida State	0	0000-00-00
58	t	Josh	Johnson	Josh Johnson	BUF	QB				8	1969-12-31
59	t	Jon	Kitna	Jon Kitna	DAL	QB	6-2	220	Central Washington	3	0000-00-00
60	t	Kevin	Kolb	Kevin Kolb	BUF	QB	6-3	218	Houston	4	0000-00-00
61	f	Byron	Leftwich	Byron Leftwich	PIT	QB	6-5	250	Marshall	4	1980-01-14
62	f	Matt	Leinart	Matt Leinart	OAK	QB	6-5	225	USC	7	1983-05-11
63	f	Cleo	Lemon	Cleo Lemon	BAL	QB	6-2	215	Arkansas State	0	0000-00-00
64	f	J.P.	Losman	J.P. Losman	SEA	QB	6-2	217	Tulane	7	0000-00-00
65	t	Eli	Manning	Eli Manning	NYG	QB				10	1969-12-31
66	t	Peyton	Manning	Peyton Manning	DEN	QB				18	1969-12-31
67	f	Ingle	Martin	Ingle Martin	KC	QB	6-2	220	Furman	0	0000-00-00
68	f	Jamie	Martin	Jamie Martin	SF	QB	6-2	205	Weber State	0	0000-00-00
69	t	Josh	McCown	Josh McCown	CLE	QB				13	1969-12-31
70	t	Luke	McCown	Luke McCown	NO	QB				7	1969-12-31
71	f	Donovan	McNabb	Donovan McNabb	MIN	QB	6-2	240	Syracuse	5	0000-00-00
72	t	Matt	Moore	Matt Moore	MIA	QB				8	1969-12-31
73	f	Kevin	O'Connell	Kevin O'Connell	SD	QB	6-5	225	San Diego State	8	1985-05-25
74	f	J.T.	O'Sullivan	J.T. O'Sullivan	OAK	QB	6-2	232	California-Davis	13	0000-00-00
84	f	Brett	Ratliff	Brett Ratliff	TB	QB	6-4	224	Utah	7	1985-08-08
92	f	JaMarcus	Russell	JaMarcus Russell	OAK	QB	6-6	260	LSU	0	1985-08-09
101	t	Brian	St. Pierre	Brian St. Pierre	CAR	QB	6-3	224	Boston College	6	0000-00-00
111	f	Andre	Woodson	Andre Woodson	WAS	QB	6-4	230	Kentucky	0	0000-00-00
122	f	Darian	Barnes	Darian Barnes	NO	RB	6-2	240	Hampton	0	0000-00-00
132	t	Ahmad	Bradshaw	Ahmad Bradshaw	IND	RB				44	1969-12-31
142	f	Aveion	Cason	Aveion Cason	DET	RB	5-10	215	Illinois State	0	0000-00-00
159	f	Carey	Davis	Carey Davis	WAS	RB	5-10	225	Illinois	0	0000-00-00
169	f	Heath	Evans	Heath Evans	NO	RB	6-0	250	Auburn	44	0000-00-00
180	t	Frank	Gore	Frank Gore	IND	RB				23	1969-12-31
190	f	Lynell	Hamilton	Lynell Hamilton	NO	RB	6-0	235	San Diego State	30	0000-00-00
207	f	Edgerrin	James	Edgerrin James	SEA	RB	6-0	219	Miami (Fla.)	0	0000-00-00
218	f	LaMont	Jordan	LaMont Jordan	DEN	RB	5-10	242	Maryland	0	0000-00-00
228	f	Stefan	Logan	Stefan Logan	DET	RB	5-6	180	South Dakota	11	1981-06-02
245	f	Maurice	Morris	Maurice Morris	DET	RB	5-11	216	Oregon	28	0000-00-00
256	f	Alvin	Pearman	Alvin Pearman	TEN	RB	5-10	196	Virginia	35	0000-00-00
266	f	Louis	Rankin	Louis Rankin	OAK	RB	6-1	207	Washington	40	1985-05-04
283	t	Jason	Snelling	Jason Snelling	ATL	RB	5-11	234	Virginia	44	0000-00-00
294	t	Mike	Tolbert	Mike Tolbert	CAR	RB				35	1969-12-31
304	t	Leon	Washington	Leon Washington	TEN	RB	5-8	192	Florida State	29	1982-08-29
321	f	Aundrae	Allison	Aundrae Allison	TB	WR	6-0	198	East Carolina	88	1984-06-25
332	f	Gary	Banks	Gary Banks	SD	WR	6-0	198	Troy	16	0000-00-00
342	f	Marty	Booker	Marty Booker	ATL	WR	6-0	205	Louisiana-Monroe	0	0000-00-00
359	f	Greg	Camarillo	Greg Camarillo	NO	WR	6-2	200	Stanford	17	1982-04-18
370	f	Laveranues	Coles	Laveranues Coles	NYJ	WR	5-11	200	Florida State	87	0000-00-00
380	f	Chris	Davis	Chris Davis	CIN	WR	5-10	181	Florida State	0	0000-00-00
397	f	Jayson	Foster	Jayson Foster	DEN	WR	5-7	175	Georgia Southern	0	0000-00-00
408	f	Skyler	Green	Skyler Green	NO	WR	5-9	190	LSU	0	0000-00-00
418	f	Marvin	Harrison	Marvin Harrison	IND	WR	6-0	185	Syracuse	0	1972-08-25
435	f	Paul	Hubbard	Paul Hubbard	BUF	WR	6-2	225	Wisconsin	17	1985-06-12
443	f	Nate	Jackson	Nate Jackson	CLE	WR	6-3	235	Menlo College	0	0000-00-00
453	f	Bryant	Johnson	Bryant Johnson	HOU	WR	6-3	212	Penn State	89	0000-00-00
463	f	Nate	Jones	Nate Jones	STL	WR	6-2	195	Texas	0	0000-00-00
471	f	Greg	Lewis	Greg Lewis	MIN	WR	6-0	185	Illinois	17	0000-00-00
481	f	Ruvell	Martin	Ruvell Martin	BUF	WR	6-4	214	Saginaw Valley State	82	1982-08-10
490	t	Robert	Meachem	Robert Meachem	NO	WR	6-2	215	Tennessee	0	0000-00-00
500	f	Legedu	Naanee	Legedu Naanee	MIA	WR	6-2	215	Boise State	19	1983-09-16
508	f	Roscoe	Parrish	Roscoe Parrish	TB	WR	5-9	175	Miami (Fla.)	12	1982-07-16
518	f	Darius	Reynaud	Darius Reynaud	TEN	RB	5-9	201	West Virginia	25	1984-12-29
527	f	Micah	Rucker	Micah Rucker	NYG	WR	6-6	221	Eastern Illinois	0	0000-00-00
536	t	Jerome	Simpson	Jerome Simpson	SF	WR				14	1969-12-31
546	f	John	Standeford	John Standeford	CIN	WR	6-4	200	Purdue	0	1982-04-15
553	f	Limas	Sweed	Limas Sweed	PIT	WR	6-4	220	Texas	80	1984-12-25
563	f	Bobby	Wade	Bobby Wade	WAS	WR	5-10	195	Arizona	19	0000-00-00
571	t	Nate	Washington	Nate Washington	HOU	WR				85	1969-12-31
581	f	Brandon	Williams	Brandon Williams	PIT	WR	5-11	170	Wisconsin	0	0000-00-00
589	f	Troy	Williamson	Troy Williamson	JAC	WR	6-1	203	South Carolina	84	0000-00-00
599	f	Chris	Baker	Chris Baker	SEA	TE	6-3	261	Michigan State	86	0000-00-00
608	f	Eric	Butler	Eric Butler	OAK	TE	6-2	263	Mississippi State	0	0000-00-00
618	f	Chris	Cooley	Chris Cooley	WAS	TE	6-3	243	Utah State	47	0000-00-00
626	t	Vernon	Davis	Vernon Davis	DEN	TE				85	1969-12-31
636	f	Derek	Fine	Derek Fine	HOU	TE	6-3	247	Kansas	82	0000-00-00
644	f	John	Gilmore	John Gilmore	NO	TE	6-5	257	Penn State	89	0000-00-00
654	f	Will	Heller	Will Heller	DET	TE	6-6	275	Georgia Tech	89	1981-02-28
663	f	Joe	Klopfenstein	Joe Klopfenstein	BUF	TE	6-5	262	Colorado	87	0000-00-00
673	f	Randy	McMichael	Randy McMichael	SD	TE	6-3	248	Georgia	81	1979-06-28
686	f	John	Owens	John Owens	OAK	TE	6-3	255	Notre Dame	82	0000-00-00
697	t	Dante	Rosario	Dante Rosario	CHI	TE				88	1969-12-31
707	f	Dezmond	Sherrod	Dezmond Sherrod	HOU	TE	6-2	250	Mississippi State	0	0000-00-00
720	f	Tony	Stewart	Tony Stewart	OAK	TE	6-5	260	Penn State	86	0000-00-00
729	f	Aaron	Walker	Aaron Walker	CLE	TE	6-6	260	Florida	0	0000-00-00
739	t	Connor	Barth	Connor Barth	TB	K				1	1969-12-31
751	f	Nick	Folk	Nick Folk	NYJ	K				2	1969-12-31
766	f	Taylor	Mehlhaff	Taylor Mehlhaff	MIN	K	5-10	184	Wisconsin	0	0000-00-00
777	t	Adam	Vinatieri	Adam Vinatieri	IND	K				4	1969-12-31
787	f	Nate	Davis	Nate Davis	IND	QB	6-1	226	Ball State	8	1987-05-05
797	f	Andre	Brown	Andre Brown	HOU	RB	6-0	227	North Carolina State	33	1986-12-15
807	f	Jeremi	Johnson	Jeremi Johnson	CIN	RB	5-11	275	Western Kentucky	0	0000-00-00
821	f	Eddie	Williams	Eddie Williams	WAS	RB	6-1	249	Idaho	43	1987-08-22
832	t	Julian	Edelman	Julian Edelman	NE	WR				11	1969-12-31
842	f	Juaquin	Iglesias	Juaquin Iglesias	HOU	WR	6-0	205	Oklahoma	19	1987-08-22
75	t	Dan	Orlovsky	Dan Orlovsky	DET	QB				8	1969-12-31
86	t	Philip	Rivers	Philip Rivers	SD	QB				17	1969-12-31
105	t	Michael	Vick	Michael Vick	PIT	QB				2	1969-12-31
120	f	Lance	Ball	Lance Ball	DEN	RB	5-9	224	Maryland	35	1985-06-19
130	f	Lorenzo	Booker	Lorenzo Booker	MIN	RB	5-10	201	Florida State	27	1984-06-14
139	t	Reggie	Bush	Reggie Bush	SF	RB				23	1969-12-31
149	f	Patrick	Cobbs	Patrick Cobbs	NO	RB	5-8	205	North Texas	38	1983-01-31
158	f	Kenneth	Darby	Kenneth Darby	STL	RB	5-10	219	Alabama	34	0000-00-00
168	f	Allen	Ervin	Allen Ervin	DET	RB	5-10	224	Lambuth	0	0000-00-00
178	f	Quinton	Ganther	Quinton Ganther	BUF	RB	5-9	220	Utah	25	0000-00-00
187	f	Ahmard	Hall	Ahmard Hall	TEN	RB	5-11	241	Texas	45	1979-11-13
197	f	Jacob	Hester	Jacob Hester	DEN	RB	5-11	225	LSU	22	0000-00-00
206	t	Brandon	Jacobs	Brandon Jacobs	NYG	RB	6-4	264	Southern Illinois	34	0000-00-00
216	f	Thomas	Jones	Thomas Jones	KC	RB	5-10	212	Virginia	20	0000-00-00
225	t	Brian	Leonard	Brian Leonard	TB	RB	6-1	225	Rutgers	30	0000-00-00
235	t	Darren	McFadden	Darren McFadden	DAL	RB				20	1969-12-31
244	f	Mewelde	Moore	Mewelde Moore	IND	RB	5-11	209	Tulane	26	1982-07-24
254	t	Jalen	Parmele	Jalen Parmele	CLE	RB				35	1969-12-31
263	f	Lousaka	Polite	Lousaka Polite	ATL	RB	6-0	245	Pittsburgh	45	1981-09-14
273	f	Gary	Russell	Gary Russell	OAK	RB	5-11	215	Minnesota	0	0000-00-00
282	f	Terrelle	Smith	Terrelle Smith	DET	RB	6-0	246	Arizona State	0	0000-00-00
292	t	Pierre	Thomas	Pierre Thomas	WAS	RB				23	1969-12-31
301	f	Derrick	Ward	Derrick Ward	HOU	RB	5-11	240	Ottawa (Kans.)	32	0000-00-00
311	f	Ricky	Williams	Ricky Williams	BAL	RB	5-10	230	Texas	34	0000-00-00
320	f	Jake	Allen	Jake Allen	CLE	WR	6-4	196	Mississippi College	85	0000-00-00
330	t	Donnie	Avery	Donnie Avery	KC	WR	5-11	200	Houston	17	1984-06-12
339	f	Taye	Biddle	Taye Biddle	MIN	WR	6-1	185	Mississippi	15	0000-00-00
349	f	Reggie	Brown	Reggie Brown	TB	WR	6-1	197	Georgia	15	0000-00-00
358	t	Andre	Caldwell	Andre Caldwell	DEN	WR				12	1969-12-31
368	f	David	Clowney	David Clowney	BUF	WR	6-0	188	Virginia Tech	17	1985-07-07
377	f	Kevin	Curtis	Kevin Curtis	TEN	WR	6-0	186	Utah State	17	1978-07-17
387	f	Braylon	Edwards	Braylon Edwards	NYJ	WR	6-3	214	Michigan	17	0000-00-00
396	f	Marquis	Floyd	Marquis Floyd	SEA	WR	6-0	190	West Georgia	0	0000-00-00
406	t	Ted	Ginn Jr.	Ted Ginn Jr.	CAR	WR				19	1969-12-31
415	f	Chris	Hannon	Chris Hannon	BAL	WR	6-3	205	Tennessee	0	0000-00-00
425	t	Devin	Hester	Devin Hester	ATL	WR				17	1969-12-31
434	f	T.J.	Houshmandzadeh	T.J. Houshmandzadeh	OAK	WR	6-2	203	Oregon State	84	0000-00-00
446	f	Mike	Jefferson	Mike Jefferson	DAL	WR	6-1	206	Montana State	0	0000-00-00
461	f	Mark	Jones	Mark Jones	TEN	WR	5-9	185	Tennessee	0	0000-00-00
479	t	Mario	Manningham	Mario Manningham	NYG	WR	5-11	185	Michigan	86	1986-05-25
498	f	Sinorice	Moss	Sinorice Moss	PHI	WR	5-8	185	Miami (Fla.)	83	1983-12-28
510	f	Logan	Payne	Logan Payne	BAL	WR	6-2	205	Minnesota	18	1985-01-21
520	f	Brandon	Rideau	Brandon Rideau	CHI	WR	6-3	198	Kansas	0	0000-00-00
533	f	Edell	Shepherd	Edell Shepherd	DEN	WR	6-1	175	San Jose State	0	0000-00-00
543	t	Micheal	Spurlock	Micheal Spurlock	CHI	WR	5-11	210	Mississippi	18	0000-00-00
554	f	Courtney	Taylor	Courtney Taylor	SEA	WR	6-1	205	Auburn	0	0000-00-00
564	t	Delanie	Walker	Delanie Walker	TEN	TE				82	1969-12-31
578	t	Roddy	White	Roddy White	ATL	WR				84	1969-12-31
590	f	Matt	Willis	Matt Willis	DET	WR	6-0	190	UCLA	12	0000-00-00
600	t	Gary	Barnidge	Gary Barnidge	CLE	TE				82	1969-12-31
615	t	Dallas	Clark	Dallas Clark	BAL	TE	6-3	252	Iowa	87	0000-00-00
627	f	Tyson	DeVree	Tyson DeVree	IND	TE	6-6	245	Colorado	0	1984-11-12
637	f	Jermichael	Finley	Jermichael Finley	GB	TE	6-5	247	Texas	88	1987-03-26
652	f	Todd	Heap	Todd Heap	ARI	TE	6-5	252	Arizona State	86	1980-03-16
664	f	Scott	Kuhn	Scott Kuhn	BAL	TE				0	0000-00-00
680	t	Matthew	Mulligan	Matthew Mulligan	BUF	TE				82	1969-12-31
690	f	Jason	Pociask	Jason Pociask	DAL	TE	6-3	259	Wisconsin	0	1983-02-09
704	f	Matt	Schobel	Matt Schobel	PHI	TE	6-5	247	Texas Christian	0	0000-00-00
715	f	Stephen	Spach	Stephen Spach	STL	TE	6-4	260	Fresno State	81	0000-00-00
734	f	George	Wrighster	George Wrighster	NYG	TE	6-3	265	Oregon	0	0000-00-00
748	t	Phil	Dawson	Phil Dawson	SF	K				9	1969-12-31
758	t	Steven	Hauschka	Steven Hauschka	SEA	K				4	1969-12-31
772	f	Jeff	Reed	Jeff Reed	SEA	K	5-11	225	North Carolina	3	1979-04-09
788	t	Josh	Freeman	Josh Freeman	MIA	QB				5	1969-12-31
799	f	Glen	Coffee	Glen Coffee	SF	RB	6-0	209	Alabama	29	0000-00-00
809	f	Dan	Kreider	Dan Kreider	ARI	RB	5-11	250	New Hampshire	0	0000-00-00
822	f	Javarris	Williams	Javarris Williams	DAL	RB	5-10	223	Tennessee State	30	1986-04-08
833	f	Dominique	Edison	Dominique Edison	DAL	WR	6-2	201	Stephen F. Austin St.	0	1986-07-16
843	f	Manuel	Johnson	Manuel Johnson	DAL	WR	5-11	200	Oklahoma	15	1986-10-14
858	t	Brandon	Tate	Brandon Tate	CIN	WR				19	1969-12-31
869	f	Marquez	Branson	Marquez Branson	ATL	TE	6-3	248	Central Arkansas	0	1987-02-14
879	f	Lance	Louis	Lance Louis	CHI	TE	6-3	305	San Diego State	60	1985-04-24
893	t	Ryan	Succop	Ryan Succop	TEN	K				4	1969-12-31
76	t	Kyle	Orton	Kyle Orton	BUF	QB	6-4	228	Purdue	0	1982-11-14
85	f	Chris	Redman	Chris Redman	ATL	QB	6-3	225	Louisville	8	1977-07-07
95	f	D.J.	Shockley	D.J. Shockley	ATL	QB	6-0	218	Georgia	0	0000-00-00
103	t	Tyler	Thigpen	Tyler Thigpen	CLE	QB	6-1	216	Coastal Carolina	4	1984-04-14
113	t	Vince	Young	Vince Young	GB	QB	6-5	232	Texas	13	0000-00-00
121	f	Marion	Barber	Marion Barber	CHI	RB	5-11	218	Minnesota	24	1983-06-10
131	f	Cory	Boyd	Cory Boyd	DEN	RB	6-1	213	South Carolina	0	0000-00-00
141	f	Rock	Cartwright	Rock Cartwright	SF	RB	5-8	215	Kansas State	28	1979-12-03
150	f	Alonzo	Coleman	Alonzo Coleman	DAL	RB	5-9	210	Hampton	0	0000-00-00
160	f	Jason	Davis	Jason Davis	NYJ	RB	5-10	242	Illinois	43	0000-00-00
170	f	Jonathan	Evans	Jonathan Evans	WAS	RB	6-1	245	Baylor	0	0000-00-00
179	f	Nick	Goings	Nick Goings	CAR	RB	6-0	225	Pittsburgh	0	0000-00-00
189	f	Bruce	Hall	Bruce Hall	BUF	RB	5-11	205	Mississippi	36	1985-03-18
198	f	Maurice	Hicks	Maurice Hicks	MIN	RB	5-11	205	North Carolina A&T	0	0000-00-00
208	t	Chris	Johnson	Chris Johnson	ARI	RB				23	1969-12-31
217	f	Maurice	Jones-Drew	Maurice Jones-Drew	OAK	RB	5-7	210	UCLA	21	1985-03-23
227	f	Rafael	Little	Rafael Little	TEN	RB	5-9	195	Kentucky	0	0000-00-00
236	t	Willis	McGahee	Willis McGahee	CLE	RB	6-0	235	Miami (Fla.)	26	0000-00-00
246	f	Sammy	Morris	Sammy Morris	DAL	RB	6-0	220	Texas Tech	34	0000-00-00
255	f	Allen	Patrick	Allen Patrick	STL	RB	6-1	196	Oklahoma	0	0000-00-00
265	f	Ryan	Powdrell	Ryan Powdrell	SEA	RB	5-11	254	USC	0	0000-00-00
274	f	Cecil	Sapp	Cecil Sapp	HOU	RB	5-11	236	Colorado State	0	0000-00-00
284	f	Olaniyi	Sobomehin	Olaniyi Sobomehin	NO	RB	6-1	230	Portland State	0	0000-00-00
293	f	Marcus	Thomas	Marcus Thomas	DEN	RB	6-0	215	Texas-El Paso	0	0000-00-00
303	f	Chauncey	Washington	Chauncey Washington	STL	RB	5-11	222	USC	33	0000-00-00
312	f	Garrett	Wolfe	Garrett Wolfe	CHI	RB	5-7	185	Northern Illinois	25	0000-00-00
322	f	Anthony	Alridge	Anthony Alridge	WAS	WR	5-9	185	Houston	0	0000-00-00
331	f	Dallas	Baker	Dallas Baker	PIT	WR	6-3	206	Florida	0	0000-00-00
341	t	Anquan	Boldin	Anquan Boldin	SF	WR				81	1969-12-31
350	f	Travis	Brown	Travis Brown	STL	WR				0	0000-00-00
360	f	Kelly	Campbell	Kelly Campbell	TB	WR	5-10	175	Georgia Tech	0	0000-00-00
369	f	Keary	Colbert	Keary Colbert	KC	WR	6-1	205	USC	84	0000-00-00
379	f	Andre	Davis	Andre Davis	HOU	WR	6-1	200	Virginia Tech	11	0000-00-00
388	f	Devale	Ellis	Devale Ellis	CLE	WR	5-10	174	Hofstra	0	0000-00-00
398	f	Eric	Fowler	Eric Fowler	DET	WR	6-3	210	Grand Valley State	18	0000-00-00
407	f	Anthony	Gonzalez	Anthony Gonzalez	NE	WR	6-0	193	Ohio State	0	0000-00-00
417	f	Justin	Harper	Justin Harper	BAL	WR	6-3	226	Virginia Tech	82	1985-02-24
426	t	Johnnie Lee	Higgins	Johnnie Lee Higgins	PHI	WR	5-11	185	Texas-El Paso	15	0000-00-00
436	f	Felton	Huggins	Felton Huggins	BUF	WR	6-2	195	Southeastern Louisiana	15	1983-02-15
442	f	Dexter	Jackson	Dexter Jackson	NYJ	WR	5-9	182	Appalachian State	89	0000-00-00
452	t	Andre	Johnson	Andre Johnson	IND	WR				81	1969-12-31
462	f	Matt	Jones	Matt Jones	CIN	WR	6-6	222	Arkansas	0	0000-00-00
470	f	Ashley	Lelie	Ashley Lelie	KC	WR	6-3	195	Hawaii	0	0000-00-00
480	t	Brandon	Marshall	Brandon Marshall	NYJ	WR				15	1969-12-31
489	f	Billy	McMullen	Billy McMullen	DET	WR	6-4	215	Virginia	0	0000-00-00
499	f	Muhsin	Muhammad	Muhsin Muhammad	CAR	WR	6-2	215	Michigan State	0	0000-00-00
507	f	Terrell	Owens	Terrell Owens	SEA	WR	6-3	224	Tennessee-Chattanooga	10	0000-00-00
517	f	Josh	Reed	Josh Reed	SD	WR	5-10	210	LSU	82	0000-00-00
526	t	Eddie	Royal	Eddie Royal	CHI	WR				19	1969-12-31
535	f	Mark	Simmons	Mark Simmons	HOU	WR	5-10	185	Kansas	0	0000-00-00
545	f	Isaiah	Stanback	Isaiah Stanback	JAC	TE	6-2	228	Washington	86	0000-00-00
552	f	Brett	Swain	Brett Swain	SEA	WR	6-0	200	San Diego State	16	0000-00-00
562	f	Mario	Urrutia	Mario Urrutia	TB	WR	6-6	232	Louisville	0	0000-00-00
570	f	Kelley	Washington	Kelley Washington	SD	WR	6-3	217	Tennessee	87	1979-08-21
580	f	Ernest	Wilford	Ernest Wilford	JAC	WR	6-4	235	Virginia Tech	85	0000-00-00
588	f	Roy	Williams	Roy Williams	CHI	WR	6-3	215	Texas	11	0000-00-00
598	t	Billy	Bajema	Billy Bajema	BAL	TE	6-4	259	Oklahoma State	86	0000-00-00
607	f	Mark	Bruener	Mark Bruener	HOU	TE	6-4	253	Washington	0	0000-00-00
617	f	Daniel	Coats	Daniel Coats	NYG	TE	6-3	264	Brigham Young	85	1984-04-16
625	t	Kellen	Davis	Kellen Davis	NYJ	TE				47	1969-12-31
635	t	Daniel	Fells	Daniel Fells	NYG	TE				85	1969-12-31
643	t	Antonio	Gates	Antonio Gates	SD	TE				85	1969-12-31
653	f	Steve	Heiden	Steve Heiden	CLE	TE	6-5	270	South Dakota State	0	0000-00-00
662	f	Jimmy	Kleinsasser	Jimmy Kleinsasser	MIN	TE	6-3	272	North Dakota	40	0000-00-00
672	f	Michael	Matthews	Michael Matthews	IND	TE	6-4	270	Georgia Tech	82	1983-10-09
678	f	Fontel	Mines	Fontel Mines	CHI	TE	6-4	244	Virginia	0	0000-00-00
688	f	Justin	Peelle	Justin Peelle	SF	TE	6-4	251	Oregon	81	1979-03-15
696	f	Jeff	Robinson	Jeff Robinson	SEA	TE	6-4	250	Idaho	0	0000-00-00
706	f	Mike	Sellers	Mike Sellers	WAS	RB	6-3	272	Walla Walla CC WA	45	0000-00-00
713	f	L.J.	Smith	L.J. Smith	BAL	TE	6-3	245	Rutgers	0	0000-00-00
723	f	Quinn	Sypniewski	Quinn Sypniewski	BAL	TE	6-6	270	Colorado	0	0000-00-00
77	f	Jeff	Otis	Jeff Otis	STL	QB	6-2	208	Columbia	0	0000-00-00
93	t	Matt	Ryan	Matt Ryan	ATL	QB				2	1969-12-31
102	t	Drew	Stanton	Drew Stanton	ARI	QB				5	1969-12-31
112	f	Anthony	Wright	Anthony Wright	NYG	QB	6-1	211	South Carolina	0	0000-00-00
123	t	Jackie	Battle	Jackie Battle	TEN	RB	6-2	240	Houston	44	0000-00-00
140	f	Brian	Calhoun	Brian Calhoun	DET	RB	5-10	208	Wisconsin	0	0000-00-00
151	t	Jed	Collins	Jed Collins	DAL	RB	6-1	252	Washington State	45	1986-03-03
161	f	Clifton	Dawson	Clifton Dawson	HOU	RB	5-10	212	Harvard	0	0000-00-00
171	f	Justin	Fargas	Justin Fargas	DEN	RB	6-1	220	USC	25	0000-00-00
188	f	Andre	Hall	Andre Hall	HOU	RB	5-10	212	South Florida	0	0000-00-00
199	t	Tim	Hightower	Tim Hightower	NO	RB				34	1969-12-31
209	f	James	Johnson	James Johnson	PIT	RB	5-11	205	Kansas State	0	1984-09-06
226	f	Jamal	Lewis	Jamal Lewis	CLE	RB	5-11	245	Tennessee	0	0000-00-00
237	f	Sean	McHugh	Sean McHugh	PIT	TE	6-5	265	Penn State	49	0000-00-00
247	f	Lorenzo	Neal	Lorenzo Neal	BAL	RB	5-11	255	Fresno State	0	0000-00-00
264	f	Clinton	Portis	Clinton Portis	WAS	RB	5-11	219	Miami (Fla.)	26	0000-00-00
275	f	Dantrell	Savage	Dantrell Savage	CAR	RB	5-8	182	Oklahoma State	26	0000-00-00
285	t	Darren	Sproles	Darren Sproles	PHI	RB				43	1969-12-31
302	f	D.J.	Ware	D.J. Ware	TB	RB	6-0	225	Georgia	28	1985-02-18
313	t	Danny	Woodhead	Danny Woodhead	SD	RB				39	1969-12-31
323	t	Danny	Amendola	Danny Amendola	NE	WR				80	1969-12-31
340	f	Shaun	Bodiford	Shaun Bodiford	OAK	WR	5-11	186	Portland State	10	1982-05-04
351	f	Isaac	Bruce	Isaac Bruce	SF	WR	6-0	188	Memphis	0	0000-00-00
361	f	Drew	Carter	Drew Carter	OAK	WR	6-3	200	Ohio State	0	0000-00-00
378	f	Devard	Darling	Devard Darling	HOU	WR	6-1	213	Washington State	0	1982-04-16
389	f	Bobby	Engram	Bobby Engram	CLE	WR	5-10	192	Penn State	88	0000-00-00
399	f	Will	Franklin	Will Franklin	OAK	WR	6-0	210	Missouri	0	0000-00-00
416	f	James	Hardy	James Hardy	BAL	WR	6-5	220	Indiana	84	1985-12-24
427	f	Jason	Hill	Jason Hill	NYJ	WR	6-0	202	Washington State	0	0000-00-00
441	t	DeSean	Jackson	DeSean Jackson	WAS	WR				11	1969-12-31
451	t	Greg	Jennings	Greg Jennings	MIA	WR				85	1969-12-31
459	t	Jacoby	Jones	Jacoby Jones	PIT	WR				12	1969-12-31
469	f	Lance	Leggett	Lance Leggett	CLE	WR	6-3	205	Miami (Fla.)	0	0000-00-00
477	f	Chad	Lucas	Chad Lucas	STL	WR	6-1	201	Alabama State	0	0000-00-00
488	f	Kevin	McMahan	Kevin McMahan	CAR	WR	6-2	192	Maine	0	0000-00-00
496	f	Randy	Moss	Randy Moss	SF	WR	6-4	210	Marshall	84	0000-00-00
506	t	Kassim	Osgood	Kassim Osgood	SF	WR	6-5	220	San Diego State	14	0000-00-00
515	t	Antwaan	Randle El	Antwaan Randle El	PIT	WR	5-10	185	Indiana	82	0000-00-00
525	t	Courtney	Roby	Courtney Roby	ATL	WR	6-0	189	Indiana	15	0000-00-00
537	t	Matthew	Slater	Matthew Slater	NE	WR				18	1969-12-31
555	f	Travis	Taylor	Travis Taylor	DET	WR	6-1	210	Florida	0	0000-00-00
572	f	Todd	Watkins	Todd Watkins	NYG	WR	6-3	195	Brigham Young	19	1983-06-22
582	f	Chandler	Williams	Chandler Williams	KC	WR	5-11	176	Florida International	8	1985-08-09
597	f	Richard	Angulo	Richard Angulo	CHI	TE	6-8	260	Western New Mexico	89	0000-00-00
609	f	Dan	Campbell	Dan Campbell	NO	TE	6-5	265	Texas A&M	0	0000-00-00
619	f	Brad	Cottam	Brad Cottam	KC	TE	6-7	269	Tennessee	87	0000-00-00
634	t	Anthony	Fasano	Anthony Fasano	TEN	TE				80	1969-12-31
645	f	Tony	Gonzalez	Tony Gonzalez	ATL	TE	6-5	247	California	88	1976-02-27
655	f	Tory	Humphrey	Tory Humphrey	OAK	TE	6-2	257	Central Michigan	88	1983-01-20
670	f	Brandon	Manumaleuna	Brandon Manumaleuna	CHI	TE	6-2	295	Arizona	86	0000-00-00
681	f	Chad	Mustard	Chad Mustard	DEN	TE	6-6	277	North Dakota	0	0000-00-00
691	f	Marcus	Pollard	Marcus Pollard	ATL	TE	6-3	255	Bradley	0	0000-00-00
705	f	Derek	Schouman	Derek Schouman	NO	TE	6-2	223	Boise State	83	1985-03-11
722	f	Jonathan	Stupar	Jonathan Stupar	BAL	TE	6-3	254	Virginia	88	1984-07-24
733	t	Jason	Witten	Jason Witten	DAL	TE				82	1969-12-31
747	t	Mason	Crosby	Mason Crosby	GB	K				2	1969-12-31
757	t	Garrett	Hartley	Garrett Hartley	PIT	K				0	1969-12-31
771	f	Dave	Rayner	Dave Rayner	BUF	K	6-2	215	Michigan State	3	0000-00-00
780	f	P.J.	Pope	P.J. Pope	DEN	RB	5-9	205	Bowling Green State	0	0000-00-00
798	t	Donald	Brown	Donald Brown	SD	RB				34	1969-12-31
808	f	Quinn	Johnson	Quinn Johnson	TEN	RB	6-1	263	LSU	45	1986-09-30
823	t	Ramses	Barden	Ramses Barden	JAC	WR	6-6	224	Cal Poly-S.L.O.	9	0000-00-00
834	f	Brooks	Foster	Brooks Foster	MIA	WR	6-1	204	North Carolina	0	1986-04-09
844	f	Derek	Kinder	Derek Kinder	CHI	WR	6-1	202	Pittsburgh	0	0000-00-00
857	f	Nate	Swift	Nate Swift	JAC	WR	6-2	195	Nebraska	0	0000-00-00
868	f	Travis	Beckum	Travis Beckum	NYG	TE	6-3	234	Wisconsin	47	1987-01-24
878	t	David	Johnson	David Johnson	SD	TE				88	1969-12-31
892	f	Piotr	Czech	Piotr Czech	PIT	K	6-5	210	Wagner	0	0000-00-00
903	f	Alex	Mortensen	Alex Mortensen	TEN	QB	6-1	222	Arkansas	0	0000-00-00
913	f	Nehemiah	Broughton	Nehemiah Broughton	ARI	RB	5-11	255	The Citadel	32	0000-00-00
927	f	Marlon	Lucky	Marlon Lucky	CIN	RB	5-11	218	Nebraska	0	0000-00-00
938	f	Tyrell	Sutton	Tyrell Sutton	SEA	RB	5-8	213	Northwestern	30	1986-12-19
78	f	Tyler	Palko	Tyler Palko	KC	QB	6-1	215	Pittsburgh	4	0000-00-00
94	t	Matt	Schaub	Matt Schaub	BAL	QB				8	1969-12-31
104	f	Marques	Tuiasosopo	Marques Tuiasosopo	OAK	QB	6-1	220	Washington	0	0000-00-00
119	f	B.J.	Askew	B.J. Askew	TB	RB	6-3	233	Michigan	0	0000-00-00
129	f	DeCori	Birmingham	DeCori Birmingham	CAR	RB	5-10	210	Arkansas	0	0000-00-00
138	f	Michael	Bush	Michael Bush	ARI	RB	6-1	245	Louisville	29	0000-00-00
148	f	Thomas	Clayton	Thomas Clayton	ARI	RB	5-11	222	Kansas State	31	1984-04-26
157	f	P.J.	Daniels	P.J. Daniels	BAL	RB	5-10	210	Georgia Tech	0	0000-00-00
167	f	Kyle	Eckel	Kyle Eckel	DEN	RB	5-11	237	Navy	36	0000-00-00
177	f	Samkon	Gado	Samkon Gado	TEN	RB	5-10	227	Liberty	0	0000-00-00
186	f	Justin	Griffith	Justin Griffith	HOU	RB	6-0	230	Mississippi State	45	0000-00-00
196	f	Noah	Herron	Noah Herron	CLE	RB	5-11	220	Northwestern	0	0000-00-00
205	t	Steven	Jackson	Steven Jackson	NE	RB				39	1969-12-31
215	f	Kevin	Jones	Kevin Jones	CHI	RB	6-0	225	Virginia Tech	0	0000-00-00
224	t	Vonta	Leach	Vonta Leach	BAL	RB	6-0	260	East Carolina	44	0000-00-00
234	t	Le'Ron	McClain	Le'Ron McClain	SD	RB	6-0	260	Alabama	33	0000-00-00
243	f	Ryan	Moats	Ryan Moats	MIN	RB	5-8	210	Louisiana Tech	30	0000-00-00
253	f	Willie	Parker	Willie Parker	WAS	RB	5-10	212	North Carolina	39	0000-00-00
262	f	Michael	Pittman	Michael Pittman	DEN	RB	6-0	225	Fresno State	0	0000-00-00
272	f	J.D.	Runnels	J.D. Runnels	CIN	RB	5-11	237	Oklahoma	0	0000-00-00
281	f	Kolby	Smith	Kolby Smith	JAC	RB	5-11	219	Louisville	0	0000-00-00
291	f	Fred	Taylor	Fred Taylor	NE	RB	6-1	228	Florida	21	0000-00-00
300	f	Justin	Vincent	Justin Vincent	PIT	RB	5-10	219	LSU	28	0000-00-00
310	t	DeAngelo	Williams	DeAngelo Williams	PIT	RB				34	1969-12-31
319	f	Sam	Aiken	Sam Aiken	CLE	WR	6-2	220	North Carolina	85	0000-00-00
329	t	Jason	Avant	Jason Avant	KC	WR				81	1969-12-31
338	t	Davone	Bess	Davone Bess	CLE	WR	5-10	195	Hawaii	15	0000-00-00
348	f	John	Broussard	John Broussard	DET	WR	6-1	181	San Jose State	0	0000-00-00
357	f	Keenan	Burton	Keenan Burton	STL	WR	6-0	206	Kentucky	14	0000-00-00
367	f	Michael	Clayton	Michael Clayton	NYG	WR	6-4	215	LSU	83	1982-10-13
376	f	Ronald	Curry	Ronald Curry	STL	WR	6-2	210	North Carolina	0	0000-00-00
386	f	Biren	Ealy	Biren Ealy	GB	WR	6-3	207	Houston	0	0000-00-00
395	t	Malcom	Floyd	Malcom Floyd	SD	WR				80	1969-12-31
405	f	Samuel	Giguere	Samuel Giguere	NYG	WR	5-11	215	Unknown	0	1985-07-11
414	f	Cortez	Hankton	Cortez Hankton	TB	WR	6-0	200	Texas Southern	0	0000-00-00
424	f	Marcus	Henry	Marcus Henry	CAR	WR	6-4	212	Kansas	0	0000-00-00
433	f	Torry	Holt	Torry Holt	NE	WR	6-0	200	North Carolina State	84	0000-00-00
444	t	Vincent	Jackson	Vincent Jackson	TB	WR				83	1969-12-31
454	t	Calvin	Johnson	Calvin Johnson	DET	WR				81	1969-12-31
464	f	Onrea	Jones	Onrea Jones	CHI	WR	5-11	202	Hampton	17	1983-12-22
478	f	John	Madsen	John Madsen	CLE	TE	6-5	240	Utah	0	0000-00-00
491	f	Evan	Moore	Evan Moore	SEA	TE	6-6	250	Stanford	82	1985-01-03
501	f	Martin	Nance	Martin Nance	PIT	WR	6-3	212	Miami (Ohio)	0	0000-00-00
516	f	Paul	Raymond	Paul Raymond	CLE	WR	5-10	185	Brown	0	0000-00-00
534	f	Arman	Shields	Arman Shields	OAK	WR	6-1	195	Richmond	0	0000-00-00
544	f	Donte'	Stallworth	Donte' Stallworth	WAS	WR	6-0	200	Tennessee	14	0000-00-00
561	f	Jerheme	Urban	Jerheme Urban	KC	WR	6-3	207	Trinity (Tex.)	83	0000-00-00
579	f	Huey	Whittaker	Huey Whittaker	NYJ	WR	6-4	234	South Florida	0	0000-00-00
591	f	George	Wilson	George Wilson	BUF	WR	6-0	212	Arkansas	37	1981-03-14
601	f	Anthony	Becht	Anthony Becht	KC	TE	6-6	270	West Virginia	87	1977-08-08
616	f	Desmond	Clark	Desmond Clark	CHI	TE	6-3	249	Wake Forest	88	1977-04-20
628	f	James	Dearth	James Dearth	NE	TE	6-4	265	Tarleton State	58	1976-01-22
646	f	Daniel	Graham	Daniel Graham	NO	TE	6-3	257	Colorado	86	1978-11-16
656	f	Brian	Jennings	Brian Jennings	SF	TE	6-5	242	Arizona State	86	0000-00-00
671	f	David	Martin	David Martin	BUF	TE	6-4	264	Tennessee	80	0000-00-00
687	f	Ben	Patrick	Ben Patrick	ARI	TE	6-3	264	Delaware	89	0000-00-00
698	f	Robert	Royal	Robert Royal	CLE	TE	6-4	257	LSU	84	0000-00-00
708	f	Matt	Sherry	Matt Sherry	CIN	TE	6-4	250	Villanova	0	0000-00-00
721	f	Darrell	Strong	Darrell Strong	OAK	TE	6-5	265	Pittsburgh	0	0000-00-00
730	t	Benjamin	Watson	Benjamin Watson	NO	TE				82	1969-12-31
740	t	Rob	Bironas	Rob Bironas	TEN	K	6-0	208	Georgia Southern	2	0000-00-00
752	t	Stephen	Gostkowski	Stephen Gostkowski	NE	K				3	1969-12-31
767	f	Joe	Nedney	Joe Nedney	SF	K	6-5	220	San Jose State	6	0000-00-00
789	f	Stephen	McGee	Stephen McGee	HOU	QB	6-3	227	Texas A&M	3	0000-00-00
805	t	Rashad	Jennings	Rashad Jennings	NYG	RB				23	1969-12-31
816	f	Bernard	Scott	Bernard Scott	BAL	RB	5-10	195	Abilene Christian	34	1984-02-10
826	f	Freddie	Brown	Freddie Brown	MIN	WR	6-4	200	Utah	84	0000-00-00
840	t	Percy	Harvin	Percy Harvin	BUF	WR				18	1969-12-31
851	f	Marko	Mitchell	Marko Mitchell	MIN	WR	6-4	218	Nevada-Reno	19	0000-00-00
861	f	Patrick	Turner	Patrick Turner	NYJ	WR	6-5	220	USC	88	1987-05-19
875	f	Dan	Gronkowski	Dan Gronkowski	CLE	TE	6-5	255	Maryland	87	0000-00-00
886	f	Chris	O'Neill	Chris O'Neill	OAK	TE	6-3	250	Boise State	0	0000-00-00
79	t	Carson	Palmer	Carson Palmer	ARI	QB				3	1969-12-31
87	t	Aaron	Rodgers	Aaron Rodgers	GB	QB				12	1969-12-31
96	f	Chris	Simms	Chris Simms	TEN	QB	6-4	230	Texas	7	0000-00-00
106	f	Billy	Volek	Billy Volek	SD	QB	6-2	214	Fresno State	7	0000-00-00
114	f	Josh	Abrams	Josh Abrams	GB	RB				0	0000-00-00
124	f	Mike	Bell	Mike Bell	DET	RB	6-0	225	Arizona	22	1983-04-23
133	f	Chris	Brown	Chris Brown	HOU	RB	6-3	235	Colorado	0	0000-00-00
143	f	Tim	Castille	Tim Castille	KC	RB	5-11	238	Alabama	46	0000-00-00
152	f	Jameel	Cook	Jameel Cook	TB	RB	5-10	237	Illinois	0	0000-00-00
162	f	DeDe	Dorsey	DeDe Dorsey	DET	RB	5-11	210	Lindenwood	38	0000-00-00
172	f	Kevin	Faulk	Kevin Faulk	NE	RB	5-8	202	LSU	33	0000-00-00
181	f	Earnest	Graham	Earnest Graham	TB	RB	5-9	225	Florida	34	0000-00-00
191	f	Kay-Jay	Harris	Kay-Jay Harris	NYG	RB	6-0	240	West Virginia	0	0000-00-00
200	t	Lex	Hilliard	Lex Hilliard	NYJ	RB	5-11	235	Montana	36	0000-00-00
210	f	Larry	Johnson	Larry Johnson	MIA	RB	6-1	235	Penn State	23	0000-00-00
219	f	Mike	Karney	Mike Karney	STL	RB	5-11	260	Arizona State	44	0000-00-00
229	f	Kregg	Lumpkin	Kregg Lumpkin	SEA	RB	5-11	228	Georgia	20	1984-05-15
238	f	Corey	McIntyre	Corey McIntyre	BUF	RB	6-0	245	West Virginia	38	1979-01-25
248	f	Moran	Norris	Moran Norris	HOU	RB	6-1	250	Kansas	40	1978-06-16
257	f	Chris	Perry	Chris Perry	CIN	RB	6-0	224	Michigan	0	0000-00-00
267	t	Marcel	Reece	Marcel Reece	OAK	RB				45	1969-12-31
276	f	Owen	Schmitt	Owen Schmitt	OAK	RB	6-2	250	West Virginia	44	1985-02-13
286	f	Aaron	Stecker	Aaron Stecker	ATL	RB	5-10	213	Western Illinois	0	1975-11-13
295	f	LaDainian	Tomlinson	LaDainian Tomlinson	NYJ	RB	5-10	215	Texas Christian	21	1979-06-23
305	f	Kenny	Watson	Kenny Watson	CIN	RB	6-0	220	Penn State	0	0000-00-00
314	f	Dwayne	Wright	Dwayne Wright	PIT	RB	5-11	228	Fresno State	0	0000-00-00
324	f	David	Anderson	David Anderson	HOU	WR	5-10	193	Colorado State	89	1983-07-28
333	f	Hank	Baskett	Hank Baskett	MIN	WR	6-4	220	New Mexico	19	0000-00-00
343	t	Dwayne	Bowe	Dwayne Bowe	CLE	WR				80	1969-12-31
352	f	Antonio	Bryant	Antonio Bryant	CIN	WR	6-1	211	Pittsburgh	81	0000-00-00
362	f	Jason	Carter	Jason Carter	CAR	WR	6-0	205	Texas A&M	0	0000-00-00
371	t	Marques	Colston	Marques Colston	NO	WR				12	1969-12-31
381	f	Buster	Davis	Buster Davis	SD	WR	6-1	210	LSU	84	0000-00-00
390	f	Lee	Evans	Lee Evans	JAC	WR	6-0	210	Wisconsin	83	1981-03-11
400	f	Mike	Furrey	Mike Furrey	WAS	WR	6-0	192	Northern Iowa	85	0000-00-00
409	f	D.J.	Hackett	D.J. Hackett	WAS	WR	6-2	208	Colorado	0	0000-00-00
419	f	Mike	Hass	Mike Hass	SEA	WR	6-1	210	Oregon State	18	0000-00-00
428	f	Ike	Hilliard	Ike Hilliard	TB	WR	5-11	210	Florida	0	0000-00-00
437	f	Nate	Hughes	Nate Hughes	DET	WR	6-2	195	Alcorn State	86	0000-00-00
447	f	Darnell	Jenkins	Darnell Jenkins	NE	WR	5-10	191	Miami (Fla.)	10	1982-12-31
455	f	Jaymar	Johnson	Jaymar Johnson	ARI	WR	6-0	183	Jackson State	83	1984-07-10
465	f	Joe	Jurevicius	Joe Jurevicius	CLE	WR	6-5	230	Penn State	0	0000-00-00
473	f	Brandon	London	Brandon London	PIT	WR	6-4	210	Massachusetts	15	0000-00-00
484	f	Marcus	Maxwell	Marcus Maxwell	SEA	WR	6-3	210	Oregon	0	0000-00-00
492	f	Kenny	Moore	Kenny Moore	PIT	WR	5-11	195	Wake Forest	80	1985-02-19
502	t	Jordy	Nelson	Jordy Nelson	GB	WR				87	1969-12-31
511	f	Tab	Perry	Tab Perry	MIA	WR	6-3	215	UCLA	0	0000-00-00
521	f	Kevin	Robinson	Kevin Robinson	KC	WR	6-0	196	Utah State	0	0000-00-00
528	f	Cliff	Russell	Cliff Russell	DEN	WR	5-11	190	Utah	0	0000-00-00
538	t	Brad	Smith	Brad Smith	PHI	WR	6-2	213	Missouri	16	1983-12-12
547	f	Derek	Stanley	Derek Stanley	STL	WR	5-11	182	Wis.-Whitewater	0	0000-00-00
556	f	Devin	Thomas	Devin Thomas	DET	WR	6-2	221	Michigan State	11	0000-00-00
565	f	Javon	Walker	Javon Walker	MIN	WR	6-3	215	Florida State	0	0000-00-00
573	f	Reggie	Wayne	Reggie Wayne	NE	WR	6-0	203	Miami (FL)	15	1978-11-17
583	f	Demetrius	Williams	Demetrius Williams	JAC	WR	6-2	202	Oregon	88	1983-03-28
592	f	Travis	Wilson	Travis Wilson	DAL	WR	6-1	210	Oklahoma	0	0000-00-00
602	f	Martellus	Bennett	Martellus Bennett	CHI	TE				83	1969-12-31
610	f	Mark	Campbell	Mark Campbell	NO	TE	6-6	260	Michigan	0	0000-00-00
620	f	Alge	Crumpler	Alge Crumpler	NE	TE	6-2	275	North Carolina	82	0000-00-00
629	f	Jon	Dekker	Jon Dekker	PIT	TE	6-5	250	Princeton	0	0000-00-00
638	f	J.J.	Finley	J.J. Finley	CAR	TE	6-6	250	Oklahoma	49	0000-00-00
647	f	Aaron	Halterman	Aaron Halterman	MIA	TE	6-5	265	Indiana	0	0000-00-00
657	f	Darcy	Johnson	Darcy Johnson	STL	TE	6-5	252	Central Florida	87	0000-00-00
665	f	Nate	Lawrie	Nate Lawrie	SF	TE	6-6	255	Yale	0	1981-10-07
674	f	Billy	Miller	Billy Miller	NO	TE	6-3	252	USC	0	0000-00-00
682	f	Joe	Newton	Joe Newton	SEA	TE	6-7	258	Oregon State	0	0000-00-00
692	f	Leonard	Pope	Leonard Pope	CHI	TE	6-8	264	Georgia	88	0000-00-00
699	f	Martin	Rucker	Martin Rucker	KC	TE	6-5	251	Missouri	85	0000-00-00
709	t	Visanthe	Shiancoe	Visanthe Shiancoe	BAL	TE	6-4	250	Morgan State	81	0000-00-00
716	t	Matt	Spaeth	Matt Spaeth	PIT	TE				89	1969-12-31
725	f	John	Tereshinski	John Tereshinski	SEA	TE	6-3	245	Wake Forest	0	0000-00-00
735	f	Todd	Yoder	Todd Yoder	WAS	TE	6-4	251	Vanderbilt	0	0000-00-00
80	t	Jordan	Palmer	Jordan Palmer	TEN	QB	6-5	230	Texas-El Paso	5	0000-00-00
88	t	Ben	Roethlisberger	Ben Roethlisberger	PIT	QB				7	1969-12-31
97	t	Alex	Smith	Alex Smith	KC	QB				11	1969-12-31
107	t	Seneca	Wallace	Seneca Wallace	GB	QB	5-11	205	Iowa State	9	0000-00-00
115	f	Joseph	Addai	Joseph Addai	NE	RB	5-11	205	LSU	29	0000-00-00
125	f	Tatum	Bell	Tatum Bell	DEN	RB	5-11	213	Oklahoma State	0	0000-00-00
134	t	Ronnie	Brown	Ronnie Brown	SD	RB	6-0	223	Auburn	30	0000-00-00
144	f	Jehuu	Caulcrick	Jehuu Caulcrick	BUF	RB	6-0	250	Michigan State	30	1983-08-06
153	f	Anthony	Cotrone	Anthony Cotrone	JAC	RB				0	0000-00-00
163	f	Dominique	Dorsey	Dominique Dorsey	WAS	RB	5-7	175	Nevada-Las Vegas	0	0000-00-00
173	t	Jerome	Felton	Jerome Felton	BUF	RB				42	1969-12-31
182	f	Ryan	Grant	Ryan Grant	GB	RB	6-1	222	Notre Dame	25	1982-12-09
192	f	Jerome	Harrison	Jerome Harrison	DET	RB	5-9	205	Washington State	36	0000-00-00
201	t	Peyton	Hillis	Peyton Hillis	NYG	RB	6-2	250	Arkansas	33	0000-00-00
211	f	Rudi	Johnson	Rudi Johnson	DET	RB	5-10	225	Auburn	0	0000-00-00
220	t	John	Kuhn	John Kuhn	GB	RB				30	1969-12-31
230	t	Marshawn	Lynch	Marshawn Lynch	SEA	RB				24	1969-12-31
239	f	Jason	McKie	Jason McKie	BAL	RB	5-11	247	Temple	38	0000-00-00
249	f	Jerious	Norwood	Jerious Norwood	STL	RB	5-11	205	Mississippi State	34	0000-00-00
258	f	Adrian	Peterson	Adrian Peterson	SEA	RB	5-10	212	Georgia Southern	0	0000-00-00
268	f	Dominic	Rhodes	Dominic Rhodes	IND	RB	5-9	203	Midwestern State	30	0000-00-00
277	f	Chad	Simpson	Chad Simpson	WAS	RB	5-9	216	Morgan State	2	0000-00-00
287	t	Jonathan	Stewart	Jonathan Stewart	CAR	RB				28	1969-12-31
296	f	Ryan	Torain	Ryan Torain	NYG	RB	6-0	220	Arizona State	46	0000-00-00
306	f	Leonard	Weaver	Leonard Weaver	PHI	RB	6-0	250	Carson-Newman	43	0000-00-00
315	f	Jason	Wright	Jason Wright	ARI	RB	5-10	212	Northwestern	31	0000-00-00
325	t	Anthony	Armstrong	Anthony Armstrong	CLE	WR	5-10	185	West Texas A&M	86	0000-00-00
334	f	Arnaz	Battle	Arnaz Battle	PIT	WR	6-1	208	Notre Dame	81	0000-00-00
344	f	Mark	Bradford	Mark Bradford	SF	WR	6-2	205	Stanford	0	0000-00-00
353	f	Michael	Bumpus	Michael Bumpus	SEA	WR	5-11	194	Washington State	0	0000-00-00
363	f	Chris	Chambers	Chris Chambers	KC	WR	5-11	210	Wisconsin	84	0000-00-00
372	f	Terrance	Copper	Terrance Copper	KC	WR	6-0	207	East Carolina	15	0000-00-00
382	f	Rashied	Davis	Rashied Davis	CHI	WR	5-9	187	San Jose State	81	1979-07-24
391	f	Yamon	Figurs	Yamon Figurs	TEN	WR	5-11	185	Kansas State	13	1982-01-10
401	f	Jabar	Gaffney	Jabar Gaffney	MIA	WR	6-2	200	Florida	10	1980-12-01
410	t	Derek	Hagan	Derek Hagan	TEN	WR	6-2	210	Arizona State	80	0000-00-00
420	t	Lavelle	Hawkins	Lavelle Hawkins	TB	WR	5-11	194	California	15	0000-00-00
429	t	Domenik	Hixon	Domenik Hixon	CHI	WR	6-2	205	Akron	0	0000-00-00
438	f	Sam	Hurd	Sam Hurd	CHI	WR	6-3	200	Northern Illinois	81	0000-00-00
448	f	Justin	Jenkins	Justin Jenkins	BUF	WR	6-0	207	Mississippi State	0	0000-00-00
456	t	Stevie	Johnson	Stevie Johnson	SD	WR				11	1969-12-31
466	f	Malcolm	Kelly	Malcolm Kelly	WAS	WR	6-4	226	Oklahoma	12	1986-12-30
474	f	Lance	Long	Lance Long	DET	WR	5-11	186	Mississippi State	86	0000-00-00
485	f	Shaheer	McBride	Shaheer McBride	PHI	WR	6-2	205	Delaware State	0	0000-00-00
493	t	Lance	Moore	Lance Moore	DET	WR				16	1969-12-31
503	f	Dennis	Northcutt	Dennis Northcutt	DET	WR	5-11	172	Arizona	86	0000-00-00
512	f	Jerry	Porter	Jerry Porter	JAC	WR				0	0000-00-00
522	f	Koren	Robinson	Koren Robinson	SEA	WR	6-1	205	North Carolina State	0	0000-00-00
529	f	Lorne	Sam	Lorne Sam	GB	WR	6-3	220	Texas-El Paso	0	0000-00-00
539	f	Marcus	Smith	Marcus Smith	BAL	WR	6-1	225	New Mexico	11	1985-01-11
548	f	Syndric	Steptoe	Syndric Steptoe	CLE	WR	5-9	200	Arizona	10	0000-00-00
557	f	Craphonso	Thorpe	Craphonso Thorpe	TEN	WR	6-0	187	Florida State	0	0000-00-00
566	f	Mike	Sims-Walker	Mike Sims-Walker	STL	WR	6-2	212	Central Florida	10	0000-00-00
574	f	Jeff	Webb	Jeff Webb	KC	WR	6-2	211	San Diego State	0	0000-00-00
584	f	Edward	Williams	Edward Williams	CLE	WR	6-4	210	Lane	0	0000-00-00
593	f	D'Juan	Woods	D'Juan Woods	NO	WR	6-1	210	Oklahoma State	0	0000-00-00
603	f	Adam	Bergen	Adam Bergen	DEN	TE	6-4	267	Lehigh	84	0000-00-00
611	t	John	Carlson	John Carlson	ARI	TE	6-5	248	Notre Dame	89	0000-00-00
621	f	Tony	Curtis	Tony Curtis	WAS	TE	6-5	251	Portland State	88	1983-02-11
630	f	Darnell	Dinkins	Darnell Dinkins	NO	TE	6-4	260	Pittsburgh	0	0000-00-00
639	f	Casey	Fitzsimmons	Casey Fitzsimmons	DET	TE	6-4	260	Carroll (Mont.)	0	0000-00-00
648	f	Rodney	Hannah	Rodney Hannah	DAL	TE	6-6	258	Houston	0	0000-00-00
658	f	Kolo	Kapanui	Kolo Kapanui	CIN	TE	6-3	270	West Texas A&M	0	0000-00-00
666	f	Mike	Leach	Mike Leach	ARI	TE	6-2	235	William & Mary	82	1976-10-18
675	t	Heath	Miller	Heath Miller	PIT	TE				83	1969-12-31
683	f	Jake	Nordin	Jake Nordin	DET	TE	6-3	262	Northern Illinois	47	0000-00-00
693	f	Jeb	Putzier	Jeb Putzier	DEN	TE	6-4	256	Boise State	0	0000-00-00
700	f	Sean	Ryan	Sean Ryan	TEN	TE	6-5	260	Boston College	86	0000-00-00
710	f	Jeremy	Shockey	Jeremy Shockey	CAR	TE	6-5	251	Miami (Fla.)	80	0000-00-00
717	f	Brad	St. Louis	Brad St. Louis	CIN	TE	6-3	243	Missouri State	0	0000-00-00
743	t	Matt	Bryant	Matt Bryant	ATL	K				3	1969-12-31
81	f	Chad	Pennington	Chad Pennington	MIA	QB	6-3	225	Marshall	10	0000-00-00
89	t	Tony	Romo	Tony Romo	DAL	QB				9	1969-12-31
98	f	Paul	Smith	Paul Smith	JAC	QB	6-1	202	Tulsa	0	0000-00-00
108	f	Andrew	Walter	Andrew Walter	OAK	QB	6-6	230	Arizona State	0	0000-00-00
116	f	Charles	Ali	Charles Ali	ARI	RB	6-2	255	Arkansas-Pine Bluff	38	1984-08-23
126	f	Michael	Bennett	Michael Bennett	OAK	RB	5-9	205	Wisconsin	32	1978-08-13
135	f	Thomas	Brown	Thomas Brown	CLE	RB	5-8	203	Georgia	0	0000-00-00
145	t	Jamaal	Charles	Jamaal Charles	KC	RB				25	1969-12-31
154	f	Mike	Cox	Mike Cox	ATL	RB	6-0	252	Georgia Tech	42	1985-07-11
164	f	Reuben	Droughns	Reuben Droughns	NYG	RB	5-11	220	Oregon	0	0000-00-00
174	t	Justin	Forsett	Justin Forsett	BAL	RB				29	1969-12-31
183	f	Ahman	Green	Ahman Green	FA	RB	6-0	218	Nebraska	0	0000-00-00
193	f	Mike	Hart	Mike Hart	IND	RB	5-9	206	Michigan	32	0000-00-00
202	f	Brad	Hoover	Brad Hoover	CAR	RB	6-0	245	Western Carolina	0	0000-00-00
212	t	Felix	Jones	Felix Jones	PIT	RB	5-10	215	Arkansas	23	0000-00-00
221	f	Billy	Latsko	Billy Latsko	SD	RB	5-10	233	Florida	34	0000-00-00
231	f	Laurence	Maroney	Laurence Maroney	DEN	RB	5-11	220	Minnesota	26	0000-00-00
240	t	Rashard	Mendenhall	Rashard Mendenhall	ARI	RB	5-10	225	Illinois	28	0000-00-00
250	f	Oren	O'Neal	Oren O'Neal	OAK	RB	5-11	245	Arkansas State	0	0000-00-00
259	t	Adrian	Peterson	Adrian Peterson	MIN	RB				28	1969-12-31
269	f	Ray	Rice	Ray Rice	BAL	RB	5-8	206	Rutgers	27	1987-01-22
278	f	Steve	Slaton	Steve Slaton	MIA	RB	5-9	208	West Virginia	23	1986-01-04
288	f	Byron	Storer	Byron Storer	TB	RB	6-1	219	California	0	0000-00-00
297	f	Michael	Turner	Michael Turner	ATL	RB	5-10	247	Northern Illinois	33	1982-02-13
307	f	Brian	Westbrook	Brian Westbrook	SF	RB	5-10	203	Villanova	20	0000-00-00
316	f	DeShawn	Wynn	DeShawn Wynn	NO	RB	5-10	232	Florida	35	0000-00-00
326	f	Devin	Aromashodu	Devin Aromashodu	CHI	WR	6-2	201	Auburn	13	0000-00-00
335	f	Drew	Bennett	Drew Bennett	BAL	WR	6-5	196	UCLA	0	0000-00-00
345	f	Mark	Bradley	Mark Bradley	NO	WR	6-1	201	Oklahoma	16	0000-00-00
354	f	Rudy	Burgess	Rudy Burgess	CHI	WR	5-10	186	Arizona State	0	0000-00-00
364	f	Antonio	Chatman	Antonio Chatman	CIN	WR	5-8	185	Cincinnati	0	0000-00-00
373	t	Jerricho	Cotchery	Jerricho Cotchery	CAR	WR				82	1969-12-31
383	f	Early	Doucet	Early Doucet	ARI	WR	6-0	212	LSU	85	1985-10-28
392	f	Joel	Filani	Joel Filani	TB	WR	6-2	206	Texas Tech	0	0000-00-00
402	f	Justin	Gage	Justin Gage	TEN	WR	6-4	204	Missouri	12	1981-01-24
411	f	Marques	Hagans	Marques Hagans	WAS	WR	5-10	205	Virginia	0	0000-00-00
421	f	C.J.	Hawthorne	C.J. Hawthorne	BUF	WR	5-11	168	Hawaii	0	0000-00-00
430	f	Jonathan	Holland	Jonathan Holland	OAK	WR	6-1	195	Louisiana Tech	17	0000-00-00
439	f	Chad	Jackson	Chad Jackson	OAK	WR	6-1	223	Florida	17	1985-03-06
449	f	Michael	Jenkins	Michael Jenkins	NE	WR	6-4	214	Ohio State	10	0000-00-00
457	f	Brandon	Jones	Brandon Jones	BAL	WR	6-1	212	Oklahoma	0	1982-10-06
467	f	Jordan	Kent	Jordan Kent	STL	WR	6-4	219	Oregon	82	0000-00-00
475	f	Dane	Looker	Dane Looker	DET	WR	6-0	194	Washington	0	0000-00-00
486	f	Justin	McCareins	Justin McCareins	TEN	WR	6-2	215	Northern Illinois	0	0000-00-00
494	f	Sean	Morey	Sean Morey	SEA	WR	5-11	193	Brown	0	0000-00-00
504	t	Ben	Obomanu	Ben Obomanu	NYJ	WR	6-1	204	Auburn	15	0000-00-00
513	f	Maurice	Price	Maurice Price	TB	WR	6-1	197	Charleston Southern	0	1985-09-11
523	f	Laurent	Robinson	Laurent Robinson	JAC	WR	6-2	205	Illinois State	81	1985-05-20
530	f	P.K.	Sam	P.K. Sam	BUF	WR	6-3	210	Florida State	0	0000-00-00
540	f	Steve	Smith	Steve Smith	TB	WR	5-11	195	USC	12	1985-05-06
549	t	Brandon	Stokley	Brandon Stokley	BAL	WR	6-0	194	Louisiana-Lafayette	80	0000-00-00
558	f	James	Thrash	James Thrash	WAS	WR	6-0	204	Missouri Southern State	0	0000-00-00
567	t	Kevin	Walter	Kevin Walter	TEN	WR	6-3	218	Eastern Michigan	87	0000-00-00
575	t	Eric	Weems	Eric Weems	ATL	WR				14	1969-12-31
585	f	Harry	Williams	Harry Williams	HOU	WR	6-2	187	Tuskegee	0	0000-00-00
594	f	Wallace	Wright	Wallace Wright	TB	WR	6-1	197	North Carolina	12	1984-02-01
604	f	David	Binn	David Binn	SD	TE	6-3	228	California	50	1972-02-06
612	t	Brent	Celek	Brent Celek	PHI	TE				87	1969-12-31
622	t	Owen	Daniels	Owen Daniels	DEN	TE				81	1969-12-31
631	t	Joel	Dreessen	Joel Dreessen	DEN	TE	6-4	245	Colorado State	81	0000-00-00
640	f	Bubba	Franks	Bubba Franks	NYJ	TE	6-6	265	Miami (Fla.)	0	0000-00-00
649	t	Clark	Harris	Clark Harris	CIN	TE				46	1969-12-31
659	t	Dustin	Keller	Dustin Keller	MIA	TE	6-2	255	Purdue	81	0000-00-00
667	f	Donald	Lee	Donald Lee	CIN	TE	6-4	248	Mississippi State	86	1980-08-31
676	t	Zach	Miller	Zach Miller	SEA	TE	6-5	255	Arizona State	86	0000-00-00
684	t	Greg	Olsen	Greg Olsen	CAR	TE				88	1969-12-31
694	f	Jason	Rader	Jason Rader	ATL	TE	6-4	263	Marshall	0	0000-00-00
701	f	Tom	Santi	Tom Santi	IND	TE	6-3	250	Virginia	0	0000-00-00
711	f	Alex	Shor	Alex Shor	ARI	TE	6-8	255	Syracuse	0	0000-00-00
718	t	Craig	Stevens	Craig Stevens	TEN	TE				88	1969-12-31
727	f	Jerame	Tuman	Jerame Tuman	ARI	TE	6-4	253	Michigan	0	0000-00-00
737	f	Keith	Zinger	Keith Zinger	NYJ	TE	6-4	258	LSU	89	1984-10-09
745	t	Dan	Carpenter	Dan Carpenter	BUF	K				2	1969-12-31
82	t	Brady	Quinn	Brady Quinn	MIA	QB	6-3	235	Notre Dame	3	0000-00-00
90	f	Sage	Rosenfels	Sage Rosenfels	MIN	QB	6-4	225	Iowa State	18	1978-03-06
99	f	Troy	Smith	Troy Smith	PIT	QB	6-0	217	Ohio State	0	0000-00-00
109	f	Kurt	Warner	Kurt Warner	ARI	QB	6-2	214	Northern Iowa	0	1971-06-22
117	f	Deon	Anderson	Deon Anderson	MIA	RB	5-10	240	Connecticut	33	1983-01-27
127	f	Cedric	Benson	Cedric Benson	GB	RB	5-11	227	Texas	32	1982-12-28
136	f	Correll	Buckhalter	Correll Buckhalter	DEN	RB	6-0	223	Nebraska	28	0000-00-00
146	f	Jesse	Chatman	Jesse Chatman	NYJ	RB	5-8	225	Eastern Washington	0	0000-00-00
155	f	Casey	Cramer	Casey Cramer	TEN	RB	6-2	250	Dartmouth	0	0000-00-00
165	f	T.J.	Duckett	T.J. Duckett	SEA	RB	6-0	254	Michigan State	0	0000-00-00
175	t	Matt	Forte	Matt Forte	CHI	RB				22	1969-12-31
184	f	Justin	Green	Justin Green	ARI	RB	5-11	251	Montana	0	0000-00-00
194	f	Madison	Hedgecock	Madison Hedgecock	NYG	RB	6-3	266	North Carolina	39	0000-00-00
203	f	Brandon	Jackson	Brandon Jackson	CLE	RB	5-10	215	Nebraska	27	0000-00-00
213	t	Greg	Jones	Greg Jones	HOU	RB	6-1	251	Florida State	33	1981-05-09
222	f	Matt	Lawrence	Matt Lawrence	BAL	RB	6-1	209	Massachusetts	32	1985-05-05
232	f	Marcus	Mason	Marcus Mason	SD	RB	5-9	226	Youngstown State	29	0000-00-00
241	f	Garrett	Mills	Garrett Mills	NE	TE	6-1	235	Tulsa	86	1983-10-12
251	f	Xavier	Omon	Xavier Omon	DEN	RB	5-11	227	Northwest Missouri State	33	1985-02-15
260	f	Andrew	Pinnock	Andrew Pinnock	DEN	RB	5-10	250	South Carolina	0	0000-00-00
270	f	Tony	Richardson	Tony Richardson	NYJ	RB	6-1	240	Auburn	49	0000-00-00
279	f	Clifton	Smith	Clifton Smith	CLE	RB	5-9	190	Fresno State	28	0000-00-00
289	f	Naufahu	Tahi	Naufahu Tahi	MIN	RB	6-0	254	Brigham Young	38	0000-00-00
298	f	Lawrence	Vickers	Lawrence Vickers	DAL	RB	6-0	250	Colorado	47	0000-00-00
308	f	LenDale	White	LenDale White	DEN	RB	6-1	235	USC	25	1984-12-20
317	f	Albert	Young	Albert Young	JAC	RB	5-10	209	Iowa	34	1985-02-25
327	f	Adrian	Arrington	Adrian Arrington	NO	WR	6-3	192	Michigan	87	1985-11-07
336	t	Earl	Bennett	Earl Bennett	CLE	WR	6-0	206	Vanderbilt	0	0000-00-00
346	f	Deion	Branch	Deion Branch	NE	WR	5-9	195	Louisville	84	0000-00-00
355	f	Nate	Burleson	Nate Burleson	CLE	WR	6-0	198	Nevada	13	0000-00-00
365	f	Brian	Clark	Brian Clark	DET	WR	6-2	204	North Carolina State	87	0000-00-00
374	f	Patrick	Crayton	Patrick Crayton	SD	WR	6-0	205	Northwestern Oklahoma State	12	0000-00-00
384	t	Harry	Douglas	Harry Douglas	TEN	WR				83	1969-12-31
393	f	Brian	Finneran	Brian Finneran	ATL	WR	6-5	210	Villanova	86	0000-00-00
403	f	Joey	Galloway	Joey Galloway	WAS	WR	5-11	197	Ohio State	84	0000-00-00
412	f	Dante	Hall	Dante Hall	STL	WR	5-8	187	Texas A&M	0	0000-00-00
422	f	Devery	Henderson	Devery Henderson	WAS	WR	5-11	200	LSU	87	0000-00-00
431	t	Santonio	Holmes	Santonio Holmes	CHI	WR	5-11	192	Ohio State	14	0000-00-00
440	f	Darrell	Jackson	Darrell Jackson	DEN	WR	5-11	210	Florida	0	0000-00-00
450	f	Adam	Jennings	Adam Jennings	NYG	WR	5-9	180	Fresno State	0	0000-00-00
458	f	C.J.	Jones	C.J. Jones	DEN	WR	5-11	195	Iowa	0	0000-00-00
468	f	Ethan	Kilmer	Ethan Kilmer	MIA	WR	6-0	205	Penn State	0	0000-00-00
476	f	Todd	Lowber	Todd Lowber	MIA	WR	6-3	205	Ramapo	0	0000-00-00
487	f	Shaun	McDonald	Shaun McDonald	PIT	WR	5-10	183	Arizona State	0	0000-00-00
495	t	Josh	Morgan	Josh Morgan	NO	WR				17	1969-12-31
505	f	Chad	Johnson	Chad Johnson	MIA	WR	6-1	188	Oregon State	85	1969-12-31
514	f	Maurice	Purify	Maurice Purify	CIN	WR	6-3	222	Nebraska	14	0000-00-00
524	f	Ryne	Robinson	Ryne Robinson	CAR	WR				0	0000-00-00
531	f	Steve	Sanders	Steve Sanders	ARI	WR	6-3	205	Bowling Green State	0	0000-00-00
541	t	Steve	Smith	Steve Smith	BAL	WR				89	1969-12-31
550	f	Maurice	Stovall	Maurice Stovall	JAC	TE	6-5	220	Notre Dame	83	1985-02-21
559	f	Amani	Toomer	Amani Toomer	KC	WR	6-3	203	Michigan	0	0000-00-00
568	f	Hines	Ward	Hines Ward	PIT	WR	6-0	205	Georgia	86	1976-03-08
576	t	Wes	Welker	Wes Welker	STL	WR				83	1969-12-31
586	f	Paul	Williams	Paul Williams	HOU	WR	6-1	196	Fresno State	17	1983-12-02
595	f	Dominique	Zeigler	Dominique Zeigler	SF	WR	6-3	185	Baylor	17	1984-10-11
605	f	Kevin	Boss	Kevin Boss	KC	TE	6-6	255	Western Oregon	80	1984-01-11
613	t	Scott	Chandler	Scott Chandler	NE	TE				88	1969-12-31
623	f	Charles	Davis	Charles Davis	SD	TE	6-6	260	Purdue	0	1983-03-13
632	f	Jeff	Dugan	Jeff Dugan	MIN	TE	6-4	258	Maryland	83	1981-04-08
641	f	Thomas	Gafford	Thomas Gafford	KC	TE	6-2	250	Houston	43	0000-00-00
650	t	Ben	Hartsock	Ben Hartsock	CAR	TE	6-4	265	Ohio State	0	0000-00-00
660	f	Reggie	Kelly	Reggie Kelly	ATL	TE	6-4	257	Mississippi State	89	0000-00-00
668	t	Marcedes	Lewis	Marcedes Lewis	JAC	TE				89	1969-12-31
677	f	Martrez	Milner	Martrez Milner	NO	TE	6-4	260	Georgia	0	0000-00-00
685	f	Buck	Ortega	Buck Ortega	NO	TE	6-4	250	Miami (Fla.)	0	0000-00-00
695	f	Gijon	Robinson	Gijon Robinson	DET	TE	6-1	255	Missouri Western State	47	0000-00-00
702	f	Bo	Scaife	Bo Scaife	CIN	TE	6-3	249	Texas	83	1981-01-06
712	t	Alex	Smith	Alex Smith	WAS	TE				99	1969-12-31
719	f	Jerramy	Stevens	Jerramy Stevens	TB	TE	6-7	260	Washington	86	0000-00-00
728	f	Ben	Utecht	Ben Utecht	CIN	TE	6-6	245	Minnesota	0	0000-00-00
753	t	Robbie	Gould	Robbie Gould	CHI	K				9	1969-12-31
83	f	Patrick	Ramsey	Patrick Ramsey	MIN	QB	6-2	225	Tulane	9	0000-00-00
91	f	Jeff	Rowe	Jeff Rowe	NE	QB	6-5	221	Nevada-Reno	0	0000-00-00
100	f	Jim	Sorgi	Jim Sorgi	NYG	QB	6-5	196	Wisconsin	19	0000-00-00
110	t	Charlie	Whitehurst	Charlie Whitehurst	TEN	QB				12	1969-12-31
118	f	J.J.	Arrington	J.J. Arrington	PHI	RB	5-9	212	California	0	0000-00-00
128	f	Ladell	Betts	Ladell Betts	NO	RB	5-11	224	Iowa	46	0000-00-00
137	f	Eldra	Buckley	Eldra Buckley	PHI	RB	5-9	207	Tennessee-Chattanooga	34	1985-06-23
147	t	Tashard	Choice	Tashard Choice	BUF	RB	5-10	210	Georgia Tech	20	0000-00-00
156	f	Julius	Crosslin	Julius Crosslin	DAL	RB	5-11	245	Oklahoma State	0	0000-00-00
166	f	Warrick	Dunn	Warrick Dunn	TB	RB	5-9	187	Florida State	0	0000-00-00
176	f	DeShaun	Foster	DeShaun Foster	SF	RB	6-0	222	UCLA	0	0000-00-00
185	f	BenJarvus	Green-Ellis	BenJarvus Green-Ellis	CIN	RB	5-11	220	Mississippi	42	1985-07-02
195	f	Chris	Henry	Chris Henry	SEA	RB	5-11	234	Arizona	40	0000-00-00
204	t	Fred	Jackson	Fred Jackson	SEA	RB				22	1969-12-31
214	f	Julius	Jones	Julius Jones	NO	RB	5-10	208	Notre Dame	21	0000-00-00
223	f	Luke	Lawton	Luke Lawton	OAK	RB	6-0	240	McNeese State	44	0000-00-00
233	f	Deuce	McAllister	Deuce McAllister	NO	RB	6-1	232	Mississippi	0	0000-00-00
242	f	Travis	Minor	Travis Minor	STL	RB	5-10	203	Florida State	0	0000-00-00
252	t	Montell	Owens	Montell Owens	CHI	RB	5-10	225	Maine	24	1984-05-04
261	f	Antonio	Pittman	Antonio Pittman	STL	RB	5-11	195	Ohio State	0	0000-00-00
271	t	Michael	Robinson	Michael Robinson	SEA	RB	6-1	240	Penn State	26	0000-00-00
280	f	Kevin	Smith	Kevin Smith	DET	RB	6-1	217	Central Florida	30	0000-00-00
290	f	Chester	Taylor	Chester Taylor	ARI	RB	5-11	213	Toledo	29	0000-00-00
299	f	Chris	Vincent	Chris Vincent	ARI	RB	6-1	218	Oregon	0	0000-00-00
309	f	Cadillac	Williams	Cadillac Williams	STL	RB	5-11	218	Auburn	33	0000-00-00
318	f	Selvin	Young	Selvin Young	DEN	RB	5-11	215	Texas	0	0000-00-00
328	f	Miles	Austin	Miles Austin	PHI	WR				19	1969-12-31
337	f	Bernard	Berrian	Bernard Berrian	MIN	WR	6-1	185	Fresno State	87	0000-00-00
347	f	Steve	Breaston	Steve Breaston	NO	WR	6-0	189	Michigan	85	0000-00-00
356	f	Plaxico	Burress	Plaxico Burress	PIT	WR	6-5	232	Michigan State	80	0000-00-00
366	f	Mark	Clayton	Mark Clayton	STL	WR	5-10	190	Oklahoma	89	1982-07-02
375	t	Josh	Cribbs	Josh Cribbs	IND	WR	6-1	192	Kent State	0	0000-00-00
385	f	Donald	Driver	Donald Driver	GB	WR	6-0	194	Alcorn State	80	1975-02-02
394	t	Larry	Fitzgerald	Larry Fitzgerald	ARI	WR				11	1969-12-31
404	t	Pierre	Garcon	Pierre Garcon	WAS	WR				88	1969-12-31
413	f	Roy	Hall	Roy Hall	NO	WR	6-3	240	Ohio State	0	0000-00-00
423	f	Chris	Henry	Chris Henry	CIN	WR	6-4	200	West Virginia	0	1983-05-17
432	f	Glenn	Holt	Glenn Holt	DET	WR	6-1	193	Kentucky	0	0000-00-00
445	f	Dwayne	Jarrett	Dwayne Jarrett	CAR	WR	6-4	219	USC	80	0000-00-00
460	t	James	Jones	James Jones	GB	WR				89	1969-12-31
472	t	Brandon	Lloyd	Brandon Lloyd	SF	WR	6-0	200	Illinois	84	1981-07-05
482	f	Glenn	Martinez	Glenn Martinez	HOU	WR	6-1	190	Saginaw Valley State	0	0000-00-00
497	t	Santana	Moss	Santana Moss	WAS	WR	5-10	193	Miami (Fla.)	89	1979-06-01
509	f	David	Patten	David Patten	NE	WR	5-10	190	Western Carolina	0	0000-00-00
519	t	Sidney	Rice	Sidney Rice	SEA	WR	6-4	202	South Carolina	18	0000-00-00
532	f	Chaz	Schilens	Chaz Schilens	DET	WR	6-4	225	San Diego State	19	0000-00-00
542	f	Taj	Smith	Taj Smith	IND	WR	6-0	192	Syracuse	10	1983-09-30
551	f	Chansi	Stuckey	Chansi Stuckey	ARI	WR	6-0	196	Clemson	17	0000-00-00
560	f	David	Tyree	David Tyree	BAL	WR	6-0	206	Syracuse	0	0000-00-00
569	f	Paris	Warren	Paris Warren	NO	WR	6-0	213	Utah	0	0000-00-00
577	f	Ernie	Wheelwright	Ernie Wheelwright	BAL	WR	6-5	217	Minnesota	0	0000-00-00
587	f	Reggie	Williams	Reggie Williams	SEA	WR	6-4	212	Washington	0	0000-00-00
596	f	Courtney	Anderson	Courtney Anderson	HOU	TE	6-6	270	San Jose State	0	0000-00-00
606	f	Chris	Brown	Chris Brown	MIA	RB	6-0	239	Tennessee	0	0000-00-00
614	f	Brad	Cieslak	Brad Cieslak	CLE	TE	6-3	260	Northern Illinois	0	0000-00-00
624	t	Fred	Davis	Fred Davis	NE	TE	6-4	247	USC	83	0000-00-00
633	f	Greg	Estandia	Greg Estandia	CLE	TE	6-8	266	Nevada-Las Vegas	88	0000-00-00
642	f	Michael	Gaines	Michael Gaines	HOU	TE	6-2	267	Central Florida	0	0000-00-00
651	f	Joey	Haynos	Joey Haynos	TEN	TE	6-8	270	Maryland	44	1984-08-28
661	t	Jeff	King	Jeff King	ARI	TE	6-3	260	Virginia Tech	87	0000-00-00
669	f	Brad	Listorti	Brad Listorti	NE	TE	6-4	251	Massachusetts	0	0000-00-00
679	f	Anthony	Mix	Anthony Mix	TB	TE	6-5	243	Auburn	0	0000-00-00
689	f	Jamie	Petrowski	Jamie Petrowski	CAR	TE	6-4	262	Indiana State	86	0000-00-00
703	t	Tony	Scheffler	Tony Scheffler	DET	TE	6-5	255	Western Michigan	85	0000-00-00
714	f	Isaac	Smolko	Isaac Smolko	BAL	TE	6-5	258	Penn State	0	0000-00-00
724	t	Jacob	Tamme	Jacob Tamme	ATL	TE				83	1969-12-31
732	t	Kellen	Winslow	Kellen Winslow	NYJ	TE	6-4	240	Miami (Fla.)	81	0000-00-00
742	f	Kris	Brown	Kris Brown	DAL	K	5-11	215	Nebraska	2	0000-00-00
750	t	Jay	Feely	Jay Feely	CHI	K	5-10	208	Michigan	0	0000-00-00
760	f	Nate	Kaeding	Nate Kaeding	MIA	K	6-0	187	Iowa	10	0000-00-00
769	t	Matt	Prater	Matt Prater	DET	K				5	1969-12-31
726	f	David	Thomas	David Thomas	NO	TE	6-3	248	Texas	85	1983-07-05
736	f	Joe	Zelenka	Joe Zelenka	ATL	TE	6-3	260	Wake Forest	82	1976-03-09
744	f	John	Carney	John Carney	NO	K	5-11	185	Notre Dame	3	0000-00-00
754	t	Shayne	Graham	Shayne Graham	ATL	K				3	1969-12-31
763	f	Rhys	Lloyd	Rhys Lloyd	NYG	K	5-11	231	Minnesota	8	1982-06-05
774	f	Matt	Stover	Matt Stover	IND	K	5-11	180	Louisiana Tech	0	0000-00-00
784	f	Rhett	Bomar	Rhett Bomar	OAK	QB	6-2	225	Sam Houston State	0	0000-00-00
793	t	Matthew	Stafford	Matthew Stafford	DET	QB				9	1969-12-31
801	t	Tony	Fiammetta	Tony Fiammetta	CHI	RB	6-0	250	Syracuse	43	1986-08-22
811	f	Kestahn	Moore	Kestahn Moore	TEN	RB	5-10	214	Florida	0	1987-04-13
818	t	Frank	Summers	Frank Summers	BUF	RB	5-9	248	Nevada-Las Vegas	38	1985-09-06
828	f	Demetrius	Byrd	Demetrius Byrd	SD	WR				0	0000-00-00
836	t	Brandon	Gibson	Brandon Gibson	NE	WR				13	1969-12-31
846	f	Quinten	Lawrence	Quinten Lawrence	KC	WR	6-0	184	McNeese State	14	1984-09-21
853	t	Hakeem	Nicks	Hakeem Nicks	NYG	WR				14	1969-12-31
863	t	Mike	Wallace	Mike Wallace	MIN	WR				11	1969-12-31
871	t	Chase	Coffman	Chase Coffman	SEA	TE				85	1969-12-31
881	t	Cameron	Morrah	Cameron Morrah	DEN	TE	6-3	251	California	86	0000-00-00
888	t	Brandon	Pettigrew	Brandon Pettigrew	DET	TE				87	1969-12-31
898	f	Adam	DiMichele	Adam DiMichele	PHI	QB	6-1	202	Temple	0	0000-00-00
905	f	Mike	Reilly	Mike Reilly	SEA	QB	6-3	212	Central Washington	0	0000-00-00
915	f	Herb	Donaldson	Herb Donaldson	TEN	RB	5-11	221	Western Illinois	36	1985-12-13
922	f	Jerome	Johnson	Jerome Johnson	ARI	RB	6-1	258	Nevada-Reno	48	0000-00-00
932	f	Chris	Pressley	Chris Pressley	CIN	RB	5-11	260	Wisconsin	36	1986-08-08
940	t	Marcus	Thigpen	Marcus Thigpen	BUF	WR				11	1969-12-31
950	f	Jarrett	Byers	Jarrett Byers	STL	WR	5-10	191	Northeastern State (Okla.)	0	0000-00-00
958	f	Britt	Davis	Britt Davis	NE	WR	6-3	215	Northern Illinois	17	1986-04-23
967	f	JaRon	Harris	JaRon Harris	GB	WR	6-0	200	South Dakota State	0	0000-00-00
975	f	Aaron	Kelly	Aaron Kelly	ATL	WR	6-5	203	Clemson	0	0000-00-00
984	f	Darren	Mougey	Darren Mougey	ARI	WR	6-5	219	San Diego State	0	0000-00-00
992	f	Eric	Peterman	Eric Peterman	CHI	WR	6-1	202	Northwestern	0	0000-00-00
1002	f	Raymond	Ventrone	Raymond Ventrone	NE	WR	5-10	200	Villanova	41	1982-10-21
1010	f	J'Nathan	Bullock	J'Nathan Bullock	NYJ	TE	6-5	240	Cleveland State	0	0000-00-00
1020	f	Darius	Hill	Darius Hill	CIN	TE	6-7	250	Ball State	49	0000-00-00
1027	f	Kory	Sperry	Kory Sperry	ARI	TE	6-5	265	Colorado State	83	1985-04-10
1036	t	New	York Giants	New York Giants	NYG	DEF				0	0000-00-00
1046	t	Atlanta	Falcons	Atlanta Falcons	ATL	DEF				0	0000-00-00
1053	t	Miami	Dolphins	Miami Dolphins	MIA	DEF				0	0000-00-00
1063	t	Houston	Texans	Houston Texans	HOU	DEF				0	0000-00-00
1071	f	Dan	Curran	Dan Curran	SEA	RB	6-0	240	New Hampshire	0	0000-00-00
1081	f	Bradon	Godfrey	Bradon Godfrey	BAL	WR	6-3	197	Utah	0	0000-00-00
1090	f	Clarence	Denmark	Clarence Denmark	JAC	WR	5-11	185	Arkansas-Monticello	13	0000-00-00
1100	f	Chris	Taylor	Chris Taylor	NO	RB	6-0	224	Indiana	0	0000-00-00
1107	t	Zach	Potter	Zach Potter	HOU	TE	6-7	269	Nebraska	86	0000-00-00
1117	f	Tony	Pike	Tony Pike	CAR	QB	6-6	222	Cincinnati	16	1986-03-10
1124	f	Ryan	D'Imperio	Ryan D'Imperio	NYG	RB	6-3	240	Rutgers	49	1987-08-15
1134	f	Dimitri	Nance	Dimitri Nance	ATL	RB	5-10	219	Arizona State	30	1988-02-18
1141	t	Dezmon	Briscoe	Dezmon Briscoe	DAL	WR	6-2	210	Kansas	81	1989-08-31
1151	t	Jacoby	Ford	Jacoby Ford	TEN	WR				10	1969-12-31
1159	t	Brandon	LaFell	Brandon LaFell	NE	WR				19	1969-12-31
1169	f	Jordan	Shipley	Jordan Shipley	JAC	WR	6-0	188	Texas	15	0000-00-00
1177	f	Mike	Williams	Mike Williams	SEA	WR	6-5	235	USC	17	0000-00-00
1187	t	Jimmy	Graham	Jimmy Graham	SEA	TE				88	1969-12-31
1194	t	Tony	Moeaki	Tony Moeaki	ATL	TE				81	1969-12-31
1204	f	Garrett	Lindholm	Garrett Lindholm	STL	K	5-9	190	Tarleton State	1	0000-00-00
1212	f	Trevor	Harris	Trevor Harris	JAC	QB	6-2	223	Edinboro	0	0000-00-00
1222	f	Carlos	Brown	Carlos Brown	NYJ	RB	6-0	212	Michigan	0	0000-00-00
1229	f	Javarris	James	Javarris James	ARI	RB	6-0	208	Miami (Fla.)	45	0000-00-00
1239	f	Daniel	Porter	Daniel Porter	CAR	RB				0	0000-00-00
1247	f	Manase	Tonga	Manase Tonga	OAK	RB	5-11	245	Brigham Young	41	1984-02-28
1256	f	Freddie	Barnes	Freddie Barnes	CHI	WR	5-11	210	Bowling Green State	0	0000-00-00
1266	f	Bakari	Grant	Bakari Grant	SF	WR	6-3	195	California-Davis	13	0000-00-00
1274	f	Patrick	Honeycutt	Patrick Honeycutt	DEN	WR				0	0000-00-00
1283	f	Chris	McGaha	Chris McGaha	JAC	WR				0	0000-00-00
1291	f	Rod	Owens	Rod Owens	NE	WR	6-0	183	Florida State	0	0000-00-00
1301	f	Juamorris	Stewart	Juamorris Stewart	PIT	WR	6-3	198	Southern University	87	0000-00-00
1309	t	Stephen	Williams	Stephen Williams	MIA	WR	6-5	215	Toledo	0	0000-00-00
1319	f	Gerald	Harris	Gerald Harris	TEN	TE				0	0000-00-00
1327	f	Hunter	Lawrence	Hunter Lawrence	TB	K	6-0	187	Texas	0	0000-00-00
1337	f	Curtis	Steele	Curtis Steele	BAL	RB	6-0	200	Memphis	42	0000-00-00
1344	f	Richard	Goodman	Richard Goodman	SD	WR	6-0	192	Florida State	15	0000-00-00
1354	t	Bryan	Walters	Bryan Walters	JAC	WR				0	1969-12-31
731	f	Daniel	Wilcox	Daniel Wilcox	BAL	TE	6-1	250	Appalachian State	0	0000-00-00
741	t	Josh	Brown	Josh Brown	NYG	K				3	1969-12-31
749	f	Jason	Elam	Jason Elam	ATL	K	5-11	195	Hawaii	0	0000-00-00
759	t	Sebastian	Janikowski	Sebastian Janikowski	OAK	K				11	1969-12-31
768	t	Mike	Nugent	Mike Nugent	CIN	K				2	1969-12-31
778	f	Steven	Weatherford	Steven Weatherford	JAC	K	6-3	215	Illinois	9	0000-00-00
790	f	Keith	Null	Keith Null	CAR	QB	6-4	219	West Texas A&M	9	0000-00-00
796	f	Aaron	Brown	Aaron Brown	CIN	RB	6-1	200	Texas Christian	33	1985-10-10
806	f	Gartrell	Johnson	Gartrell Johnson	ATL	RB	5-10	219	Colorado State	27	1986-06-21
814	t	Cedric	Peerman	Cedric Peerman	CIN	RB				30	1969-12-31
824	f	Shawn	Bayes	Shawn Bayes	OAK	WR	5-10	180	Kent State	0	1987-07-08
831	f	Jarett	Dillard	Jarett Dillard	ARI	WR	5-11	190	Rice	16	0000-00-00
841	t	Darrius	Heyward-Bey	Darrius Heyward-Bey	PIT	WR				88	1969-12-31
849	f	Kenny	McKinley	Kenny McKinley	DEN	WR	6-0	183	South Carolina	11	0000-00-00
859	f	Lucas	Taylor	Lucas Taylor	DEN	WR				0	0000-00-00
866	f	Rodney	Wright	Rodney Wright	SF	WR	5-9	181	Fresno State	0	0000-00-00
876	f	Anthony	Hill	Anthony Hill	PHI	TE	6-6	277	North Carolina State	87	1985-01-02
884	f	Shawn	Nelson	Shawn Nelson	BUF	TE	6-5	240	Southern Mississippi	89	1985-10-05
894	f	Hunter	Cantwell	Hunter Cantwell	BAL	QB	6-4	236	Louisville	12	1985-12-30
901	t	Brian	Hoyer	Brian Hoyer	HOU	QB				7	1969-12-31
911	f	Brock	Bolen	Brock Bolen	CLE	RB	6-0	232	Louisville	38	0000-00-00
918	f	P.J.	Hill	P.J. Hill	NO	RB	5-10	218	Wisconsin	32	0000-00-00
928	f	Markus	Manson	Markus Manson	CAR	RB				0	0000-00-00
935	f	Tyler	Roehl	Tyler Roehl	SEA	RB				0	0000-00-00
945	f	Aubrey	Bell	Aubrey Bell	HOU	WR				0	0000-00-00
953	f	Tim	Carter	Tim Carter	STL	WR	6-0	185	Auburn	0	0000-00-00
963	f	Michael Ray	Garvin	Michael Ray Garvin	ARI	WR	5-8	182	Florida State	0	0000-00-00
970	f	Kole	Heckendorf	Kole Heckendorf	IND	WR	6-2	191	North Dakota State	19	0000-00-00
980	f	Marcus	Monk	Marcus Monk	CAR	WR	6-4	212	Arkansas	0	0000-00-00
987	t	Kevin	Ogletree	Kevin Ogletree	NYG	WR	6-1	195	Virginia	15	1987-08-05
997	f	Tommy	Saunders	Tommy Saunders	DET	WR				0	0000-00-00
1005	t	Isaiah	Williams	Isaiah Williams	TEN	WR	6-2	201	Maryland	11	0000-00-00
1015	f	Sean	Conover	Sean Conover	STL	TE	6-5	275	Bucknell	0	0000-00-00
1023	f	Travis	McCall	Travis McCall	BUF	TE	6-2	276	Alabama	0	0000-00-00
1033	f	Sam	Swank	Sam Swank	JAC	K	6-0	193	Wake Forest	0	1985-10-05
1040	t	Minnesota	Vikings	Minnesota Vikings	MIN	DEF				0	0000-00-00
1050	t	Green	Bay Packers	Green Bay Packers	GB	DEF				0	0000-00-00
1058	t	Washington	Redskins	Washington Redskins	WAS	DEF				0	0000-00-00
1067	t	St.	Louis Rams	St. Louis Rams	STL	DEF				0	0000-00-00
1076	f	Shane	Andrus	Shane Andrus	SF	K	5-10	190	Murray State	9	0000-00-00
1086	f	Sean	Walker	Sean Walker	STL	WR	6-0	178	Vanderbilt	0	0000-00-00
1094	f	Bobby	Sippio	Bobby Sippio	DET	WR	6-3	215	Western Kentucky	0	0000-00-00
1104	f	Edgar	Jones	Edgar Jones	WAS	TE	6-3	262	Southeast Missouri State	50	1984-12-01
1112	t	Jimmy	Clausen	Jimmy Clausen	BAL	QB				8	1969-12-31
1121	f	Tim	Tebow	Tim Tebow	PHI	QB	6-3	245	Florida	11	1987-08-14
1129	f	Deji	Karim	Deji Karim	HOU	RB	5-8	209	Southern Illinois	39	0000-00-00
1138	t	Ben	Tate	Ben Tate	PIT	RB	5-10	220	Auburn	34	1988-08-21
1146	t	Eric	Decker	Eric Decker	NYJ	WR				87	1969-12-31
1156	t	Chad	Hall	Chad Hall	JAC	WR	5-8	187	Air Force	14	0000-00-00
1164	t	Taylor	Price	Taylor Price	SEA	WR	6-1	195	Ohio U.	0	0000-00-00
1174	t	Joe	Webb	Joe Webb	CAR	QB				14	1969-12-31
1181	f	Nate	Byham	Nate Byham	TB	TE	6-4	264	Pittsburgh	82	1988-06-27
1191	f	Aaron	Hernandez	Aaron Hernandez	NE	TE	6-1	245	Florida	81	1989-11-06
1198	f	Colin	Peek	Colin Peek	ATL	TE				0	0000-00-00
1209	f	Jarrett	Brown	Jarrett Brown	CLE	QB	6-3	224	West Virginia	0	1987-09-21
1216	f	Dominic	Randolph	Dominic Randolph	NYG	QB	6-3	223	Holy Cross	0	0000-00-00
1225	f	Keithon	Flemming	Keithon Flemming	PHI	RB				0	0000-00-00
1235	f	Lonyae	Miller	Lonyae Miller	BAL	RB	6-0	216	Fresno State	0	0000-00-00
1242	t	Alfonso	Smith	Alfonso Smith	SF	RB	6-1	209	Kentucky	38	1987-01-23
1252	f	Chris	Zardas	Chris Zardas	PHI	RB				0	0000-00-00
1260	f	Duke	Calhoun	Duke Calhoun	OAK	WR	6-2	200	Memphis	14	0000-00-00
1270	f	Johnathan	Haggerty	Johnathan Haggerty	NE	WR	6-1	195	Southwestern Oklahoma State	83	0000-00-00
1278	f	Donald	Jones	Donald Jones	NE	WR	6-0	208	Youngstown State	19	0000-00-00
1287	f	Michael	Moore	Michael Moore	DET	WR	6-2	210	Georgia	17	0000-00-00
1295	f	Aaron	Rhea	Aaron Rhea	BUF	WR	6-1	202	Stephen F. Austin St.	0	0000-00-00
1305	f	Roberto	Wallace	Roberto Wallace	TEN	WR	6-4	222	San Diego State	16	0000-00-00
1314	f	Patrick	Devenny	Patrick Devenny	SEA	TE				0	0000-00-00
1323	f	Steve	Pfahler	Steve Pfahler	TEN	TE	6-4	254	Montana	0	0000-00-00
1331	f	Graham	Harrell	Graham Harrell	NYJ	QB	6-2	215	Texas Tech	8	0000-00-00
1340	f	Brandon	Banks	Brandon Banks	WAS	WR	5-7	153	Kansas State	16	1987-12-21
1350	f	Ernest	Smith	Ernest Smith	SD	WR				0	0000-00-00
1357	t	Richie	Brockel	Richie Brockel	CAR	TE				47	1969-12-31
1367	f	Andre'	Anderson	Andre' Anderson	BUF	RB	6-0	212	Tulane	0	0000-00-00
738	f	David	Akers	David Akers	DET	K	5-10	200	Louisville	2	1974-12-09
746	f	Brandon	Coutu	Brandon Coutu	JAC	K	5-10	195	Georgia	2	0000-00-00
756	f	Jason	Hanson	Jason Hanson	DET	K	6-0	190	Washington State	4	1970-06-17
765	f	Olindo	Mare	Olindo Mare	CHI	K	5-11	185	Syracuse	10	0000-00-00
776	f	Lawrence	Tynes	Lawrence Tynes	TB	K	6-1	194	Troy	1	0000-00-00
786	t	Chase	Daniel	Chase Daniel	KC	QB				10	1969-12-31
795	f	Pat	White	Pat White	WAS	QB	6-0	190	West Virginia	5	0000-00-00
803	t	Shonn	Greene	Shonn Greene	TEN	RB	5-11	233	Iowa	23	1985-08-21
813	t	Chris	Ogbonnaya	Chris Ogbonnaya	NYG	RB	6-0	225	Texas	47	1986-05-20
820	f	Beanie	Wells	Beanie Wells	ARI	RB	6-2	229	Ohio State	26	1988-08-07
830	t	Michael	Crabtree	Michael Crabtree	OAK	WR				15	1969-12-31
838	f	John	Halman	John Halman	WAS	WR				0	0000-00-00
848	f	Mohamed	Massaquoi	Mohamed Massaquoi	NYJ	WR	6-2	207	Georgia	13	0000-00-00
855	f	Travis	Shelton	Travis Shelton	DEN	WR	5-11	185	Temple	0	0000-00-00
865	f	Jaison	Williams	Jaison Williams	WAS	WR				0	0000-00-00
873	f	Davon	Drew	Davon Drew	BAL	TE	6-4	260	East Carolina	80	1985-12-09
883	f	John	Nalbone	John Nalbone	SEA	TE	6-4	255	Monmouth (N.J.)	0	0000-00-00
890	t	Richard	Quinn	Richard Quinn	NO	TE	6-4	264	North Carolina	85	0000-00-00
900	f	Sean	Glennon	Sean Glennon	MIN	QB				0	0000-00-00
907	f	Drew	Willy	Drew Willy	SD	QB	6-3	217	Buffalo	0	1986-11-13
917	f	Arian	Foster	Arian Foster	HOU	RB				23	1969-12-31
924	f	David	Kirtman	David Kirtman	SEA	RB	6-0	238	USC	0	0000-00-00
934	f	Bill	Rentmeester	Bill Rentmeester	SF	RB	6-0	247	Wisconsin	0	0000-00-00
942	f	Darius	Walker	Darius Walker	DEN	RB	5-11	205	Notre Dame	0	0000-00-00
952	f	Patrick	Carter	Patrick Carter	MIA	WR	6-3	215	Louisville	0	1985-02-06
960	f	Keith	Eloi	Keith Eloi	WAS	WR	5-10	190	Nebraska-Omaha	0	0000-00-00
969	f	Julian	Hawkins	Julian Hawkins	DAL	WR	6-2	215	Boise State	0	0000-00-00
977	f	Charly	Martin	Charly Martin	JAC	WR	6-1	212	West Texas A&M	85	0000-00-00
986	f	Terrence	Nunn	Terrence Nunn	TB	WR	6-0	195	Nebraska	83	0000-00-00
994	f	David	Richmond	David Richmond	CIN	WR	6-2	192	San Jose State	0	0000-00-00
1004	t	Chris	Williams	Chris Williams	CHI	WR	5-8	175	New Mexico State	82	0000-00-00
1012	f	Dominique	Byrd	Dominique Byrd	SEA	TE	6-3	255	USC	86	0000-00-00
1022	f	Tyler	Lorenzen	Tyler Lorenzen	NO	TE	6-5	234	Connecticut	0	1985-12-24
1029	f	Robbie	Dehaze	Robbie Dehaze	ATL	K				0	0000-00-00
1038	t	Baltimore	Ravens	Baltimore Ravens	BAL	DEF				0	0000-00-00
1048	t	Carolina	Panthers	Carolina Panthers	CAR	DEF				0	0000-00-00
1055	t	San	Francisco 49ers	San Francisco 49ers	SF	DEF				0	0000-00-00
1065	t	New	Orleans Saints	New Orleans Saints	NO	DEF				0	0000-00-00
1078	f	Derrick	Mason	Derrick Mason	HOU	WR	5-10	197	Michigan State	85	0000-00-00
1093	f	James	Robinson	James Robinson	CLE	WR	6-3	200	Butler Co. CC PA	0	0000-00-00
1103	f	Julius	Pruitt	Julius Pruitt	MIA	WR	6-2	212	Ouachita Baptist	83	0000-00-00
1115	f	Dan	LeFevour	Dan LeFevour	JAC	QB	6-3	230	Central Michigan	13	0000-00-00
1127	t	Toby	Gerhart	Toby Gerhart	JAC	RB				21	1969-12-31
1137	t	James	Starks	James Starks	GB	RB				44	1969-12-31
1149	t	Marcus	Easley	Marcus Easley	BUF	WR				81	1969-12-31
1163	t	Carlton	Mitchell	Carlton Mitchell	ATL	WR				19	1969-12-31
1173	f	Tim	Toone	Tim Toone	NO	WR	5-10	175	Weber State	19	0000-00-00
1185	f	Dedrick	Epps	Dedrick Epps	NYJ	TE	6-3	250	Miami (Fla.)	83	0000-00-00
1202	f	Todd	Carter	Todd Carter	KC	K	6-1	190	Grand Valley State	5	1986-06-07
1220	t	Joique	Bell	Joique Bell	DET	RB				35	1969-12-31
1233	f	Dominique	Lindsay	Dominique Lindsay	TEN	RB				0	0000-00-00
1245	f	Rendrick	Taylor	Rendrick Taylor	TB	RB	6-2	265	Clemson	0	1987-04-03
1264	f	Buddy	Farnham	Buddy Farnham	NE	WR	6-0	185	Brown	13	1987-05-22
1281	f	Scott	Long	Scott Long	SF	WR				0	0000-00-00
1299	f	Pat	Simonds	Pat Simonds	PHI	WR				0	0000-00-00
1313	t	Jeff	Cumberland	Jeff Cumberland	NYJ	TE				85	1969-12-31
1330	f	Brett	Swenson	Brett Swenson	IND	K				0	0000-00-00
1349	f	Jordan	Sisco	Jordan Sisco	IND	WR				0	0000-00-00
1361	f	Jared	Ballman	Jared Ballman	NYJ	K				0	0000-00-00
1371	t	Trindon	Holliday	Trindon Holliday	OAK	WR				16	1969-12-31
1384	f	Andy	Fantuz	Andy Fantuz	CHI	WR				0	0000-00-00
1396	t	Ryan	Mallett	Ryan Mallett	BAL	QB				15	1969-12-31
1406	f	Delone	Carter	Delone Carter	BAL	RB	5-9	232	Syracuse	34	0000-00-00
1419	f	Owen	Marecic	Owen Marecic	CLE	RB	6-0	245	Stanford	48	0000-00-00
1429	t	Jordan	Todman	Jordan Todman	PIT	RB				30	1969-12-31
1440	t	Clyde	Gates	Clyde Gates	DAL	WR				81	1969-12-31
1453	t	Kealoha	Pilares	Kealoha Pilares	CAR	WR	5-10	200	Hawaii	19	0000-00-00
1463	t	Virgil	Green	Virgil Green	DEN	TE				85	1969-12-31
1470	t	Ryan	Taylor	Ryan Taylor	KC	TE				82	1969-12-31
1481	f	Zack	Eskridge	Zack Eskridge	DAL	QB				0	0000-00-00
1490	t	Scott	Tolzien	Scott Tolzien	GB	QB				16	1969-12-31
1500	f	Lucas	Cox	Lucas Cox	ATL	RB				0	0000-00-00
1508	t	John	Griffin	John Griffin	NYJ	RB	5-11	208	Massachusetts	24	0000-00-00
1519	f	Richard	Medlin	Richard Medlin	ATL	RB	5-11	200	Fayetteville State	40	0000-00-00
755	f	Martin	Gramatica	Martin Gramatica	NO	K	5-8	170	Kansas State	0	0000-00-00
764	f	Ryan	Longwell	Ryan Longwell	SEA	K	6-0	200	California	0	0000-00-00
775	t	Shaun	Suisham	Shaun Suisham	PIT	K				6	1969-12-31
785	f	Tom	Brandstater	Tom Brandstater	STL	QB	6-5	222	Fresno State	5	0000-00-00
794	f	Mike	Teel	Mike Teel	CHI	QB	6-3	230	Rutgers	0	0000-00-00
802	t	Mike	Goodson	Mike Goodson	NYJ	RB	6-0	210	Texas A&M	23	0000-00-00
812	f	Knowshon	Moreno	Knowshon Moreno	MIA	RB	5-11	218	Georgia	28	1987-07-16
819	f	Fui	Vakapuna	Fui Vakapuna	NYJ	RB	6-0	257	Brigham Young	22	0000-00-00
829	f	Austin	Collie	Austin Collie	NE	WR	6-0	204	Brigham Young	6	1985-11-11
837	f	David	Grimes	David Grimes	KC	WR				0	0000-00-00
847	t	Jeremy	Maclin	Jeremy Maclin	KC	WR				19	1969-12-31
854	t	Brian	Robiskie	Brian Robiskie	TEN	WR	6-4	212	Ohio State	17	0000-00-00
864	f	Derrick	Williams	Derrick Williams	PIT	WR	5-11	197	Penn State	15	1986-07-06
872	t	Jared	Cook	Jared Cook	STL	TE				89	1969-12-31
882	t	Brandon	Myers	Brandon Myers	TB	TE				82	1969-12-31
889	t	John	Phillips	John Phillips	SD	TE				83	1969-12-31
899	f	Billy	Farris	Billy Farris	CIN	QB				0	0000-00-00
906	f	Danny	Southwick	Danny Southwick	OAK	QB				0	0000-00-00
916	f	Rodney	Ferguson	Rodney Ferguson	BUF	RB	6-0	234	New Mexico	44	0000-00-00
923	f	Anthony	Kimble	Anthony Kimble	NYJ	RB	6-0	215	Stanford	0	0000-00-00
933	f	Isaac	Redman	Isaac Redman	PIT	RB	6-0	230	Bowie State	33	1984-11-10
941	t	Josh	Vaughan	Josh Vaughan	ATL	RB	6-0	225	Richmond	30	1986-12-03
951	f	Greg	Carr	Greg Carr	SD	WR	6-5	217	Florida State	0	0000-00-00
959	f	Maurice	Dupree	Maurice Dupree	JAC	WR	5-10	168	Jacksonville State	0	0000-00-00
968	f	Kenneth	Harris	Kenneth Harris	DET	WR	6-3	215	Georgia	0	0000-00-00
976	f	Brennan	Marion	Brennan Marion	MIA	WR				0	0000-00-00
985	t	Jordan	Norwood	Jordan Norwood	DEN	WR				11	1969-12-31
993	f	Todd	Peterson	Todd Peterson	JAC	WR	6-4	215	Nebraska	0	0000-00-00
1003	f	Shun	White	Shun White	NE	WR	5-8	195	Navy	0	0000-00-00
1011	f	Carson	Butler	Carson Butler	NE	TE	6-4	260	Michigan	0	1987-08-21
1021	f	Jamar	Hunt	Jamar Hunt	DAL	TE				0	0000-00-00
1028	f	Nick	Walker	Nick Walker	MIN	TE				0	0000-00-00
1037	t	Pittsburgh	Steelers	Pittsburgh Steelers	PIT	DEF				0	0000-00-00
1047	t	Buffalo	Bills	Buffalo Bills	BUF	DEF				0	0000-00-00
1054	t	San	Diego Chargers	San Diego Chargers	SD	DEF				0	0000-00-00
1064	t	Kansas	City Chiefs	Kansas City Chiefs	KC	DEF				0	0000-00-00
1072	f	Justise	Hairston	Justise Hairston	BUF	RB				0	0000-00-00
1082	f	Rod	Harper	Rod Harper	PHI	WR	6-0	209	Murray State	0	1985-03-26
1091	f	Robert	Ferguson	Robert Ferguson	ATL	WR	6-1	219	Texas A&M	0	0000-00-00
1101	t	Billy	Cundiff	Billy Cundiff	CLE	K				8	1969-12-31
1108	f	Joel	Gamble	Joel Gamble	TEN	RB	6-2	260	Shippensburg	0	0000-00-00
1118	t	Zac	Robinson	Zac Robinson	CIN	QB	6-3	208	Oklahoma State	5	0000-00-00
1125	t	Anthony	Dixon	Anthony Dixon	BUF	RB				26	1969-12-31
1135	f	Charles	Scott	Charles Scott	NYG	RB	6-0	238	LSU	0	1988-08-08
1142	t	Antonio	Brown	Antonio Brown	PIT	WR				84	1969-12-31
1152	t	David	Gettis	David Gettis	TB	WR	6-3	220	Baylor	6	0000-00-00
1160	t	Marc	Mariani	Marc Mariani	CHI	WR				80	1969-12-31
1170	t	Golden	Tate	Golden Tate	DET	WR				15	1969-12-31
1178	f	Mike	Williams	Mike Williams	BUF	WR	6-2	212	Syracuse	19	1987-05-18
1188	t	Jermaine	Gresham	Jermaine Gresham	ARI	TE				84	1969-12-31
1195	f	Dennis	Morris	Dennis Morris	SD	TE	6-2	253	Louisiana Tech	0	1987-02-15
1205	f	Justin	Medlock	Justin Medlock	CAR	K	5-11	200	UCLA	6	0000-00-00
1213	f	Tim	Hiller	Tim Hiller	IND	QB	6-4	229	Western Michigan	0	0000-00-00
1223	f	Matt	Clapp	Matt Clapp	DET	RB	6-2	246	Oklahoma	0	1986-12-06
1230	f	Stafon	Johnson	Stafon Johnson	TEN	RB	5-11	222	USC	26	1988-02-06
1240	f	Quinn	Porter	Quinn Porter	STL	RB	6-0	205	Stillman	35	0000-00-00
1248	f	Keith	Toston	Keith Toston	JAC	RB	5-11	214	Oklahoma State	35	0000-00-00
1257	f	Chris	Bell	Chris Bell	NO	WR				0	0000-00-00
1267	f	Rashaun	Greer	Rashaun Greer	DAL	WR				0	0000-00-00
1275	f	Jeremy	Horne	Jeremy Horne	NYG	WR	6-2	193	Massachusetts	13	0000-00-00
1284	f	Brandon	McRae	Brandon McRae	STL	WR	6-3	208	Mississippi State	83	0000-00-00
1292	t	Preston	Parker	Preston Parker	NYG	WR				15	1969-12-31
1302	f	Roren	Thomas	Roren Thomas	JAC	WR	5-10	172	Lindenwood	0	0000-00-00
1310	f	Oliver	Young	Oliver Young	CAR	WR	6-2	196	South Carolina State	0	0000-00-00
1320	t	Jeron	Mastrud	Jeron Mastrud	CHI	TE	6-6	255	Kansas State	87	1987-12-17
1328	t	Nick	Novak	Nick Novak	HOU	K				9	1969-12-31
1338	t	Seyi	Ajirotutu	Seyi Ajirotutu	PHI	WR				16	1969-12-31
1345	f	Shay	Hodge	Shay Hodge	CIN	WR	6-1	209	Mississippi	17	0000-00-00
1355	f	Jeremy	Williams	Jeremy Williams	PHI	WR	6-0	206	Tulane	0	0000-00-00
1364	f	Harvey	Unga	Harvey Unga	CHI	RB	6-0	237	Brigham Young	45	0000-00-00
1374	f	Derrick	Townsel	Derrick Townsel	HOU	WR	5-9	175	Murray State	14	1988-07-12
1381	t	James	Develin	James Develin	NE	RB				46	1969-12-31
1391	t	Andy	Dalton	Andy Dalton	CIN	QB				14	1969-12-31
1400	t	Ricky	Stanzi	Ricky Stanzi	NYG	QB				2	1969-12-31
762	f	Rian	Lindell	Rian Lindell	TB	K	6-3	227	Washington State	4	0000-00-00
773	t	Josh	Scobee	Josh Scobee	PIT	K				8	1969-12-31
781	f	Reggie	Ball	Reggie Ball	DET	WR	5-11	195	Georgia Tech	0	0000-00-00
792	t	Mark	Sanchez	Mark Sanchez	PHI	QB				3	1969-12-31
800	f	James	Davis	James Davis	WAS	RB	5-11	218	Clemson	24	1986-01-01
810	t	LeSean	McCoy	LeSean McCoy	BUF	RB				25	1969-12-31
817	f	LaRod	Stephens-Howling	LaRod Stephens-Howling	PIT	RB	5-7	185	Pittsburgh	34	1987-04-26
827	f	Deon	Butler	Deon Butler	SD	WR	5-10	182	Penn State	3	0000-00-00
835	f	Horace	Gant	Horace Gant	STL	WR	6-3	214	St. Olaf	0	0000-00-00
845	f	Johnny	Knox	Johnny Knox	CHI	WR	6-0	185	Abilene Christian	13	1986-11-03
852	t	Louis	Murphy	Louis Murphy	TB	WR				18	1969-12-31
862	t	Tiquan	Underwood	Tiquan Underwood	CAR	WR	6-1	185	Rutgers	11	0000-00-00
870	t	James	Casey	James Casey	DEN	TE				80	1969-12-31
880	t	Zach	Miller	Zach Miller	CHI	TE				86	1969-12-31
887	t	Bear	Pascoe	Bear Pascoe	CHI	TE				87	1969-12-31
897	f	Chris	Crane	Chris Crane	IND	QB	6-4	236	Boston College	0	0000-00-00
904	f	Chris	Pizzotti	Chris Pizzotti	GB	QB	6-5	225	Harvard	0	0000-00-00
914	f	Omar	Cuff	Omar Cuff	NE	RB	5-10	195	Delaware	0	0000-00-00
921	f	Jeremiah	Johnson	Jeremiah Johnson	DEN	RB	5-9	210	Oregon	37	0000-00-00
931	f	Devin	Moore	Devin Moore	DET	RB	5-9	190	Wyoming	0	0000-00-00
939	t	Will	Ta'ufo'ou	Will Ta'ufo'ou	JAC	RB	5-11	247	California	45	1986-06-19
949	f	Justin	Brown	Justin Brown	ARI	WR				0	0000-00-00
957	f	Quan	Cosby	Quan Cosby	IND	WR	5-9	195	Texas	14	1982-12-23
966	f	Dudley	Guice	Dudley Guice	IND	WR	6-3	209	Northwestern State-Louisiana	13	0000-00-00
974	f	Michael	Jones	Michael Jones	ARI	WR	6-4	212	Arizona State	0	0000-00-00
983	f	Phillip	Morris	Phillip Morris	TEN	WR	6-3	175	South Carolina State	0	0000-00-00
991	f	Vinny	Perretta	Vinny Perretta	MIN	WR	5-9	186	Boise State	0	0000-00-00
1001	f	Rodgeriqus	Smith	Rodgeriqus Smith	SD	WR	6-0	200	Auburn	0	0000-00-00
1009	f	Jared	Bronson	Jared Bronson	MIA	TE				0	0000-00-00
1019	f	J.P.	Foschi	J.P. Foschi	CIN	TE	6-3	265	Georgia Tech	88	0000-00-00
1026	f	Jack	Simmons	Jack Simmons	NYJ	TE	6-4	246	Minnesota	0	0000-00-00
1035	f	Swayze	Waters	Swayze Waters	PIT	K	5-11	178	Alabama-Birmingham	1	1987-05-18
1045	t	Arizona	Cardinals	Arizona Cardinals	ARI	DEF				0	0000-00-00
1052	t	Jacksonville	Jaguars	Jacksonville Jaguars	JAC	DEF				0	0000-00-00
1062	t	Detroit	Lions	Detroit Lions	DET	DEF				0	0000-00-00
1070	f	Patrick	Pass	Patrick Pass	NE	RB	5-10	217	Georgia	0	0000-00-00
1080	f	Lee	Vickers	Lee Vickers	WAS	TE	6-6	275	North Alabama	88	0000-00-00
1088	f	Bobby	Williams	Bobby Williams	HOU	WR	6-2	194	North Alabama	0	0000-00-00
1099	f	Chris	Jennings	Chris Jennings	NYJ	RB	5-10	219	Arizona	34	1985-12-12
1106	f	Shane	Boyd	Shane Boyd	IND	QB	6-1	232	Kentucky	0	0000-00-00
1116	t	Colt	McCoy	Colt McCoy	WAS	QB				16	1969-12-31
1123	t	John	Conner	John Conner	BUF	RB				38	1969-12-31
1133	t	Joe	McKnight	Joe McKnight	KC	RB	5-11	205	USC	22	0000-00-00
1140	t	Arrelious	Benn	Arrelious Benn	PHI	WR				17	1969-12-31
1150	t	Armanti	Edwards	Armanti Edwards	CHI	WR	5-11	190	Appalachian State	80	1988-03-08
1158	t	Jameson	Konz	Jameson Konz	DEN	TE	6-3	234	Kent State	82	0000-00-00
1168	t	Emmanuel	Sanders	Emmanuel Sanders	DEN	WR				10	1969-12-31
1176	t	Kyle	Williams	Kyle Williams	DEN	WR	5-10	186	Arizona State	15	1988-07-19
1186	t	Garrett	Graham	Garrett Graham	HOU	TE				88	1969-12-31
1193	t	Anthony	McCoy	Anthony McCoy	SEA	TE				85	1969-12-31
1203	f	Connor	Hughes	Connor Hughes	DAL	K	5-10	172	Virginia	0	0000-00-00
1211	f	Max	Hall	Max Hall	ARI	QB	6-1	205	Brigham Young	6	1985-10-01
1221	t	LeGarrette	Blount	LeGarrette Blount	NE	RB				29	1969-12-31
1228	f	Rashawn	Jackson	Rashawn Jackson	CAR	RB	6-1	239	Virginia	38	1987-01-15
1238	f	Pat	Paschall	Pat Paschall	NE	RB				0	0000-00-00
1246	f	Kennedy	Tinsley	Kennedy Tinsley	STL	RB				0	0000-00-00
1255	f	Alric	Arnett	Alric Arnett	DET	WR	6-2	188	West Virginia	0	0000-00-00
1265	f	Shawn	Gore	Shawn Gore	GB	WR	6-0	200	Bishop's University	0	0000-00-00
1273	f	Jason	Harmon	Jason Harmon	JAC	WR				0	0000-00-00
1282	f	Dicky	Lyons	Dicky Lyons	DEN	WR				0	0000-00-00
1290	t	David	Nelson	David Nelson	PIT	WR				15	1969-12-31
1300	f	Ray	Small	Ray Small	IND	WR	5-11	180	Ohio State	0	0000-00-00
1308	f	Landis	Williams	Landis Williams	DEN	WR				0	0000-00-00
1318	f	Andrew	George	Andrew George	BUF	TE	6-4	247	Brigham Young	0	0000-00-00
1326	f	Delbert	Alvarado	Delbert Alvarado	DAL	K				0	0000-00-00
1336	f	Shawnbrey	McNeal	Shawnbrey McNeal	SD	RB	5-9	190	Southern Methodist	0	1988-10-17
1343	f	London	Crawford	London Crawford	HOU	WR	6-2	205	Arkansas	0	0000-00-00
1353	f	Marcel	Thompson	Marcel Thompson	SD	WR				0	0000-00-00
1362	f	Christian	Ducre	Christian Ducre	NO	RB				0	0000-00-00
1372	f	Zeke	Markshausen	Zeke Markshausen	KC	WR	5-11	185	Northwestern	5	1987-01-26
1379	f	Ryan	Perrilloux	Ryan Perrilloux	NYG	QB	6-3	204	Jacksonville State	17	0000-00-00
1389	f	Caz	Piurowski	Caz Piurowski	SEA	TE	6-7	271	Florida State	69	0000-00-00
1398	t	Cam	Newton	Cam Newton	CAR	QB				1	1969-12-31
1408	t	Charles	Clay	Charles Clay	BUF	TE				85	1969-12-31
779	f	Najeh	Davenport	Najeh Davenport	IND	RB	6-1	247	Miami (Fla.)	0	0000-00-00
791	t	Curtis	Painter	Curtis Painter	NYG	QB	6-4	230	Purdue	17	0000-00-00
804	f	Verron	Haynes	Verron Haynes	ATL	RB	5-9	222	Georgia	0	0000-00-00
815	f	Javon	Ringer	Javon Ringer	TEN	RB	5-9	213	Michigan State	21	1987-02-02
825	t	Kenny	Britt	Kenny Britt	STL	WR				18	1969-12-31
839	t	Brian	Hartline	Brian Hartline	CLE	WR				83	1969-12-31
850	f	Nick	Miller	Nick Miller	PHI	WR	5-9	180	Southern Utah	16	0000-00-00
860	t	Mike	Thomas	Mike Thomas	HOU	WR	5-8	198	Arizona	89	0000-00-00
874	f	Devin	Frischknecht	Devin Frischknecht	GB	TE	6-3	258	Washington State	0	0000-00-00
885	f	Jake	O'Connell	Jake O'Connell	DEN	TE	6-3	250	Miami (Ohio)	82	0000-00-00
895	f	Rudy	Carpenter	Rudy Carpenter	DAL	QB	6-2	215	Arizona State	11	1986-04-15
908	f	John	Parker Wilson	John Parker Wilson	PIT	QB	6-2	215	Alabama	4	0000-00-00
919	f	Kareem	Huggins	Kareem Huggins	TB	RB	5-9	198	Hofstra	32	0000-00-00
929	f	Michael	McLendon	Michael McLendon	JAC	RB				0	0000-00-00
943	f	Tyree	Barnes	Tyree Barnes	NE	WR				0	0000-00-00
955	f	Jeremy	Childs	Jeremy Childs	SD	WR				0	0000-00-00
965	f	Tyler	Grisham	Tyler Grisham	DEN	WR	5-11	180	Clemson	13	1987-06-11
979	f	Brett	McDermott	Brett McDermott	IND	WR	6-0	201	Holy Cross	0	0000-00-00
989	t	Greg	Orton	Greg Orton	NE	WR	6-3	199	Purdue	89	0000-00-00
999	f	Jamarko	Simmons	Jamarko Simmons	GB	WR				0	0000-00-00
1013	f	Tripp	Chandler	Tripp Chandler	SD	TE				0	0000-00-00
1024	f	Rob	Myers	Rob Myers	WAS	TE	6-4	239	Utah State	87	0000-00-00
1034	f	Danny	Urrego	Danny Urrego	BUF	K				0	0000-00-00
1041	t	New	England Patriots	New England Patriots	NE	DEF				0	0000-00-00
1051	t	Indianapolis	Colts	Indianapolis Colts	IND	DEF				0	0000-00-00
1059	t	Cincinnati	Bengals	Cincinnati Bengals	CIN	DEF				0	0000-00-00
1069	t	Eric	Kettani	Eric Kettani	KC	RB				0	1969-12-31
1077	f	Andy	Strickland	Andy Strickland	ATL	WR	6-0	197	Wofford	15	1987-09-02
1087	f	Thomas	White	Thomas White	BAL	WR				0	0000-00-00
1095	f	Reagan	Maui'a	Reagan Maui'a	ARI	RB	6-0	260	Hawaii	45	1984-07-06
1105	f	Spencer	Havner	Spencer Havner	GB	TE	6-3	250	UCLA	41	1983-02-02
1113	f	Jonathan	Crompton	Jonathan Crompton	WAS	QB	6-3	225	Tennessee	3	1987-07-25
1122	f	Jahvid	Best	Jahvid Best	DET	RB	5-10	199	California	44	0000-00-00
1130	f	Rolly	Lumbala	Rolly Lumbala	MIA	RB	6-2	238	Idaho	0	0000-00-00
1139	f	Terrence	Austin	Terrence Austin	DET	WR	5-11	171	UCLA	16	0000-00-00
1147	t	Dorin	Dickerson	Dorin Dickerson	TEN	TE	6-1	226	Pittsburgh	42	0000-00-00
1157	f	Brandyn	Harvey	Brandyn Harvey	ARI	WR	6-4	205	Villanova	0	1987-11-06
1165	t	David	Reed	David Reed	SF	WR	6-0	190	Utah	10	0000-00-00
1175	t	Damian	Williams	Damian Williams	STL	WR				10	1969-12-31
1182	t	Ed	Dickson	Ed Dickson	CAR	TE				84	1969-12-31
1192	t	Michael	Hoomanawanui	Michael Hoomanawanui	NO	TE				47	1969-12-31
1199	t	Dennis	Pitta	Dennis Pitta	BAL	TE				88	1969-12-31
1210	f	Joey	Elliott	Joey Elliott	PHI	QB				0	0000-00-00
1217	f	Noah	Shepard	Noah Shepard	GB	QB				0	0000-00-00
1226	f	Chris	Gronkowski	Chris Gronkowski	SD	RB	6-2	245	Arizona	44	0000-00-00
1236	f	Brandon	Minor	Brandon Minor	DEN	RB	6-1	203	Michigan	36	1988-07-24
1243	f	Ben	Stallings	Ben Stallings	JAC	RB				0	0000-00-00
1253	f	Damola	Adeniji	Damola Adeniji	OAK	WR				0	0000-00-00
1261	f	Blue	Cooper	Blue Cooper	PHI	WR				0	0000-00-00
1271	f	Vic	Hall	Vic Hall	NYJ	WR	5-9	185	Virginia	0	0000-00-00
1279	f	Kevin	Jurovich	Kevin Jurovich	SF	WR	6-0	188	San Jose State	14	1986-06-30
1288	f	Dion	Morton	Dion Morton	CLE	WR				0	0000-00-00
1296	f	Antonio	Robinson	Antonio Robinson	GB	WR				0	0000-00-00
1306	f	Chastin	West	Chastin West	DET	WR	6-1	217	Fresno State	15	1987-05-01
1315	f	Richard	Dickson	Richard Dickson	DET	TE	6-2	245	LSU	46	1987-11-17
1324	f	Scott	Sicko	Scott Sicko	DAL	TE	6-4	255	New Hampshire	0	0000-00-00
1332	f	Tyler	Sheehan	Tyler Sheehan	HOU	QB				0	0000-00-00
1341	f	Montez	Billings	Montez Billings	NO	WR	6-1	181	Auburn	0	1986-06-15
1351	f	Trey	Stross	Trey Stross	HOU	WR				0	0000-00-00
1358	f	DajLeon	Farr	DajLeon Farr	DAL	TE	6-5	256	Memphis	0	0000-00-00
1368	f	Dan	Klecko	Dan Klecko	ATL	RB	5-11	275	Temple	49	0000-00-00
1377	f	Kris	Wilson	Kris Wilson	BAL	TE	6-2	245	Pittsburgh	87	0000-00-00
1387	f	Joe	West	Joe West	STL	WR				0	0000-00-00
1394	t	Colin	Kaepernick	Colin Kaepernick	SF	QB				7	1969-12-31
1404	f	Baron	Batch	Baron Batch	PIT	RB	5-10	210	Texas Tech	20	0000-00-00
1413	t	Roy	Helu	Roy Helu	OAK	RB				26	1969-12-31
1422	t	Bilal	Powell	Bilal Powell	NYJ	RB				29	1969-12-31
1432	t	Ryan	Williams	Ryan Williams	DAL	RB	5-9	207	Virginia Tech	34	0000-00-00
1439	t	Kris	Durham	Kris Durham	OAK	WR				83	1969-12-31
1449	f	Scotty	McKnight	Scotty McKnight	NYJ	WR	5-11	185	Colorado	0	0000-00-00
1456	f	DeMarco	Sampson	DeMarco Sampson	BUF	WR	6-2	204	San Diego State	19	0000-00-00
1465	t	Rob	Housler	Rob Housler	CHI	TE				84	1969-12-31
1476	f	Ben	Chappell	Ben Chappell	WAS	QB				0	0000-00-00
1483	f	Mike	Hartline	Mike Hartline	NE	QB	6-6	210	Kentucky	0	0000-00-00
856	f	Sammie	Stroughter	Sammie Stroughter	TB	WR	5-10	189	Oregon State	18	1986-01-03
867	f	Robert	Agnone	Robert Agnone	ATL	TE	6-6	256	Delaware	0	0000-00-00
877	f	Cornelius	Ingram	Cornelius Ingram	DEN	TE	6-4	250	Florida	82	1985-06-10
891	f	David	Buehler	David Buehler	NYG	K	6-2	230	USC	19	0000-00-00
902	f	Kevin	McCabe	Kevin McCabe	PIT	QB				0	0000-00-00
912	f	Curtis	Brinkley	Curtis Brinkley	CHI	RB	5-9	208	Syracuse	30	0000-00-00
925	f	Keon	Lattimore	Keon Lattimore	SF	RB	5-11	222	Maryland	0	0000-00-00
936	t	Kory	Sheets	Kory Sheets	OAK	RB	5-11	208	Purdue	30	1985-03-31
946	f	Troy	Bergeron	Troy Bergeron	DAL	WR	6-2	195	Middle Tennessee State	0	0000-00-00
954	f	Jason	Chery	Jason Chery	GB	WR	5-10	185	Louisiana-Lafayette	18	0000-00-00
964	f	Cedric	Goodman	Cedric Goodman	PIT	WR				0	0000-00-00
978	f	John	Matthews	John Matthews	SF	WR	6-0	200	San Diego	0	0000-00-00
988	f	Robert	Ortiz	Robert Ortiz	NE	WR	6-1	188	San Diego State	0	0000-00-00
998	f	Trent	Shelton	Trent Shelton	WAS	WR	6-0	202	Baylor	0	0000-00-00
1006	f	Pat	Williams	Pat Williams	BAL	WR	6-1	204	Colorado	14	1986-01-13
1016	f	Tom	Crabtree	Tom Crabtree	NO	TE	6-4	245	Miami (Ohio)	84	0000-00-00
1030	f	Parker	Douglass	Parker Douglass	NYJ	K	5-8	170	South Dakota State	0	0000-00-00
1039	t	Dallas	Cowboys	Dallas Cowboys	DAL	DEF				0	0000-00-00
1049	t	Chicago	Bears	Chicago Bears	CHI	DEF				0	0000-00-00
1060	t	Cleveland	Browns	Cleveland Browns	CLE	DEF				0	0000-00-00
1073	f	Craig	Kobel	Craig Kobel	NYJ	RB	6-2	265	South Florida	0	0000-00-00
1083	f	Jesse	Holley	Jesse Holley	NE	WR	6-2	220	North Carolina	16	1984-01-08
1092	f	Chris	Francies	Chris Francies	SF	WR	6-1	193	Texas-El Paso	0	0000-00-00
1102	f	John David	Booty	John David Booty	HOU	QB	6-3	208	USC	10	0000-00-00
1114	t	Mike	Kafka	Mike Kafka	MIN	QB				4	1969-12-31
1126	t	Jonathan	Dwyer	Jonathan Dwyer	ARI	RB	5-11	229	Georgia Tech	20	1989-07-26
1136	t	C.J.	Spiller	C.J. Spiller	NO	RB				28	1969-12-31
1148	f	Charles	Dillon	Charles Dillon	GB	WR	6-0	202	Washington State	0	0000-00-00
1162	f	Kerry	Meier	Kerry Meier	ATL	WR	6-3	220	Kansas	80	0000-00-00
1172	t	Demaryius	Thomas	Demaryius Thomas	DEN	WR				88	1969-12-31
1184	f	Brody	Eldridge	Brody Eldridge	CHI	TE	6-5	265	Oklahoma	88	0000-00-00
1197	t	Michael	Palmer	Michael Palmer	PIT	TE	6-5	252	Clemson	82	1988-01-18
1208	t	R.J.	Archer	R.J. Archer	SEA	QB				6	1969-12-31
1219	f	Toney	Baker	Toney Baker	DEN	RB	5-10	225	North Carolina State	0	0000-00-00
1234	f	Jamie	McCoy	Jamie McCoy	PIT	TE	6-3	240	Texas A&M	82	0000-00-00
1251	f	DeMaundray	Woolridge	DeMaundray Woolridge	STL	RB				0	0000-00-00
1263	f	Dominique	Curry	Dominique Curry	CAR	TE	6-2	223	California (PA)	85	0000-00-00
1277	f	Brandon	James	Brandon James	IND	WR	5-7	176	Florida	12	0000-00-00
1294	f	Jared	Perry	Jared Perry	PHI	WR	6-1	178	Missouri	0	0000-00-00
1304	f	Verran	Tucker	Verran Tucker	KC	WR	6-1	204	California	15	1988-06-26
1317	f	Riar	Geer	Riar Geer	TEN	TE	6-4	250	Colorado	0	1986-12-19
1335	f	Jack	Corcoran	Jack Corcoran	SF	RB	6-1	230	Rutgers	0	1987-06-26
1348	f	Andre	Jones	Andre Jones	KC	WR				0	0000-00-00
1360	f	Cody	Slate	Cody Slate	KC	TE	6-4	229	Marshall	0	1987-08-14
1370	f	Bobby	Guillory	Bobby Guillory	SF	WR	5-10	180	Central Missouri State	0	0000-00-00
1385	t	Andrew	Hawkins	Andrew Hawkins	CLE	WR				16	1969-12-31
1397	t	Greg	McElroy	Greg McElroy	CIN	QB	6-2	225	Alabama	0	0000-00-00
1407	t	Shaun	Chapas	Shaun Chapas	BAL	RB	6-2	241	Georgia	40	0000-00-00
1424	t	Jacquizz	Rodgers	Jacquizz Rodgers	CHI	RB				35	1969-12-31
1442	t	Leonard	Hankerson	Leonard Hankerson	NE	WR				85	1969-12-31
1458	t	Torrey	Smith	Torrey Smith	SF	WR				82	1969-12-31
1472	t	D.J.	Williams	D.J. Williams	WAS	TE				0	1969-12-31
1485	f	Jordan	LaSecla	Jordan LaSecla	OAK	QB				0	0000-00-00
1495	f	Armando	Allen	Armando Allen	CHI	RB	5-8	190	Notre Dame	25	0000-00-00
1509	f	Nic	Grigsby	Nic Grigsby	MIA	RB				0	0000-00-00
1522	f	Richard	Murphy	Richard Murphy	JAC	RB	6-1	215	LSU	39	0000-00-00
1540	f	Frank	Warren	Frank Warren	DAL	RB				0	0000-00-00
1555	f	Dontavia	Bogan	Dontavia Bogan	SF	WR	6-1	188	South Florida	9	0000-00-00
1565	t	John	Chiles	John Chiles	CHI	WR				82	1969-12-31
1575	f	Mark	Dell	Mark Dell	DEN	WR	6-2	195	Michigan State	15	0000-00-00
1588	t	Vidal	Hazelton	Vidal Hazelton	NYJ	WR	6-2	209	Cincinnati	17	0000-00-00
1599	f	Dominique	Johnson	Dominique Johnson	MIN	WR				0	0000-00-00
1615	f	Adam	Mims	Adam Mims	PIT	WR				0	0000-00-00
1634	f	Courtney	Smith	Courtney Smith	NYJ	WR				0	0000-00-00
1651	f	Chris	Blohm	Chris Blohm	SF	TE				0	0000-00-00
1664	t	Michael	Higgins	Michael Higgins	MIN	TE	6-5	242	Nebraska-Omaha	47	0000-00-00
1682	t	Martell	Webb	Martell Webb	CLE	TE	6-3	276	Michigan	88	0000-00-00
1692	f	Adi	Kunalic	Adi Kunalic	CAR	K				0	0000-00-00
1754	f	Aaron	Corp	Aaron Corp	MIA	QB	6-4	195	Richmond	4	0000-00-00
1773	f	Matt	Roark	Matt Roark	NE	WR	6-5	220	Kentucky	13	0000-00-00
1785	t	Lance	Dunbar	Lance Dunbar	DAL	RB				25	1969-12-31
1796	t	Jake	Rogers	Jake Rogers	CLE	K	6-2	220	Cincinnati	0	0000-00-00
1809	t	Alfred	Morris	Alfred Morris	WAS	RB				46	1969-12-31
1820	t	Junior	Hemingway	Junior Hemingway	KC	WR				88	1969-12-31
896	f	Patrick	Cowan	Patrick Cowan	NO	QB				0	0000-00-00
910	f	Kyle	Bell	Kyle Bell	JAC	RB				0	0000-00-00
926	f	Jamall	Lee	Jamall Lee	CAR	RB	6-1	225	Bishop	0	0000-00-00
937	t	Antone	Smith	Antone Smith	CHI	RB				35	1969-12-31
947	f	Steven	Black	Steven Black	PIT	WR	6-3	213	Memphis	0	0000-00-00
961	f	Jason	English	Jason English	JAC	WR				0	0000-00-00
971	f	Amarri	Jackson	Amarri Jackson	TB	WR				0	0000-00-00
981	f	Nick	Moore	Nick Moore	STL	WR	6-2	186	Toledo	0	0000-00-00
995	f	Eron	Riley	Eron Riley	NYJ	WR	6-3	207	Duke	19	1987-08-05
1007	f	Roydell	Williams	Roydell Williams	WAS	WR	6-0	178	Tulane	87	0000-00-00
1017	f	Andrew	Davie	Andrew Davie	CAR	TE	6-6	275	Arkansas	0	0000-00-00
1031	t	Graham	Gano	Graham Gano	CAR	K				9	1969-12-31
1042	t	New	York Jets	New York Jets	NYJ	DEF				0	0000-00-00
1056	t	Seattle	Seahawks	Seattle Seahawks	SEA	DEF				0	0000-00-00
1066	t	Oakland	Raiders	Oakland Raiders	OAK	DEF				0	0000-00-00
1079	f	Richard	Owens	Richard Owens	NYJ	TE	6-4	273	Louisville	0	0000-00-00
1098	f	Tristan	Davis	Tristan Davis	WAS	RB	5-10	211	Auburn	38	0000-00-00
1110	f	Levi	Brown	Levi Brown	BUF	QB	6-4	225	Troy	10	1987-03-11
1120	f	Rusty	Smith	Rusty Smith	TEN	QB	6-5	226	Florida Atlantic	11	1987-01-28
1132	t	Ryan	Mathews	Ryan Mathews	PHI	RB				24	1969-12-31
1144	f	Tim	Buckley	Tim Buckley	ATL	WR	6-1	185	Alcorn State	16	1988-08-19
1154	f	S.J.	Green	S.J. Green	NYJ	WR				0	0000-00-00
1166	t	Andre	Roberts	Andre Roberts	WAS	WR				12	1969-12-31
1179	f	Ryan	Wolfe	Ryan Wolfe	ATL	WR	6-2	210	Nevada-Las Vegas	0	0000-00-00
1189	t	Rob	Gronkowski	Rob Gronkowski	NE	TE				87	1969-12-31
1200	t	Andrew	Quarless	Andrew Quarless	GB	TE				81	1969-12-31
1214	t	Thaddeus	Lewis	Thaddeus Lewis	PHI	QB				3	1969-12-31
1224	f	Cordera	Eason	Cordera Eason	CIN	RB	6-0	233	Mississippi	0	0000-00-00
1231	f	Tervaris	Johnson	Tervaris Johnson	KC	RB				0	0000-00-00
1241	f	William	Rose	William Rose	TEN	RB				0	0000-00-00
1249	f	Joe	Tronzo	Joe Tronzo	TEN	RB	5-11	242	Louisville	38	1987-06-10
1258	f	Chris	Brooks	Chris Brooks	IND	WR	6-2	215	Nebraska	83	1987-02-05
1268	f	Rich	Gunnell	Rich Gunnell	KC	WR	5-11	197	Boston College	0	0000-00-00
1276	f	Terrell	Hudgins	Terrell Hudgins	DAL	WR	6-2	225	Elon	0	0000-00-00
1285	f	Mico	McSwain	Mico McSwain	TEN	WR				0	0000-00-00
1293	f	Contrevious	Parks	Contrevious Parks	DET	WR	5-10	190	Stephen F. Austin St.	0	0000-00-00
1303	f	Kelton	Tindal	Kelton Tindal	MIN	WR				0	0000-00-00
1311	t	Jake	Ballard	Jake Ballard	ARI	TE	6-6	275	Ohio State	86	0000-00-00
1321	t	Nathan	Overbay	Nathan Overbay	BAL	TE	6-5	260	Eastern Washington	86	0000-00-00
1329	f	Aaron	Pettrey	Aaron Pettrey	CIN	K	6-2	199	Ohio State	4	1986-06-17
1339	f	Rodelin	Anthony	Rodelin Anthony	BAL	WR				0	0000-00-00
1346	f	Jordyn	Jackson	Jordyn Jackson	SD	WR				0	0000-00-00
1356	f	Michael	Allan	Michael Allan	SEA	TE	6-6	254	Whitworth	0	0000-00-00
1365	f	Eugene	Bright	Eugene Bright	PIT	TE	6-4	268	Purdue	87	1985-04-18
1375	f	Danario	Alexander	Danario Alexander	SD	WR	6-5	217	Missouri	84	1988-08-07
1382	f	Emmanuel	Arceneaux	Emmanuel Arceneaux	MIN	WR	6-2	215	Alcorn State	16	0000-00-00
1392	f	Nathan	Enderle	Nathan Enderle	SD	QB	6-4	240	Idaho	7	0000-00-00
1401	t	Tyrod	Taylor	Tyrod Taylor	BUF	QB				5	1969-12-31
1411	f	Jamie	Harper	Jamie Harper	TEN	RB	5-11	233	Clemson	23	0000-00-00
1423	t	Stevan	Ridley	Stevan Ridley	NYJ	RB				22	1969-12-31
1437	t	Randall	Cobb	Randall Cobb	GB	WR				18	1969-12-31
1447	t	Jeremy	Kerley	Jeremy Kerley	NYJ	WR				11	1969-12-31
1459	t	Ryan	Whalen	Ryan Whalen	MIN	WR				89	1969-12-31
1471	t	Julius	Thomas	Julius Thomas	JAC	TE				80	1969-12-31
1484	t	Jerrod	Johnson	Jerrod Johnson	CHI	QB	6-5	251	Texas A&M	0	0000-00-00
1494	f	Adam	Weber	Adam Weber	TB	QB	6-3	210	Minnesota	2	0000-00-00
1503	f	Anthony	Elzy	Anthony Elzy	BUF	RB				0	0000-00-00
1513	f	Bryson	Kelly	Bryson Kelly	OAK	RB				0	0000-00-00
1526	f	Alexander	Robinson	Alexander Robinson	MIN	RB				0	0000-00-00
1535	f	Brandon	Sullivan	Brandon Sullivan	SD	RB				0	0000-00-00
1544	f	Kenny	Younger	Kenny Younger	SD	RB				0	0000-00-00
1552	f	Doug	Beaumont	Doug Beaumont	ATL	WR				0	0000-00-00
1562	f	Adrian	Cannon	Adrian Cannon	SD	WR				0	0000-00-00
1572	f	Craig	Davis	Craig Davis	BUF	WR				0	0000-00-00
1580	f	D'Andre	Goodwin	D'Andre Goodwin	DEN	WR	5-11	188	Washington	11	0000-00-00
1590	t	Chris	Hogan	Chris Hogan	BUF	WR				15	1969-12-31
1603	f	James	Kirkendoll	James Kirkendoll	TEN	WR	5-10	186	Texas	19	0000-00-00
1614	f	Eddie	McGee	Eddie McGee	OAK	WR	6-3	210	Illinois	16	0000-00-00
1624	f	Josue	Paul	Josue Paul	KC	WR				0	0000-00-00
1631	t	Dane	Sanzenbacher	Dane Sanzenbacher	CIN	WR	5-11	184	Ohio State	11	0000-00-00
1641	f	Jamorris	Warren	Jamorris Warren	CAR	WR				0	0000-00-00
1649	f	Josh	Baker	Josh Baker	NYJ	RB	6-3	244	Northwest Missouri State	45	0000-00-00
1659	f	Evan	Frosch	Evan Frosch	CLE	TE				0	0000-00-00
1667	t	Kyle	Miller	Kyle Miller	SD	TE				86	1969-12-31
1676	f	Steve	Skelton	Steve Skelton	ARI	TE	6-5	250	Fordham	44	0000-00-00
1685	t	Dan	Bailey	Dan Bailey	DAL	K				5	1969-12-31
909	f	Kahlil	Bell	Kahlil Bell	NYJ	RB	5-11	219	UCLA	24	0000-00-00
920	f	Ian	Johnson	Ian Johnson	SF	RB	5-11	212	Boise State	0	1986-10-10
930	f	Walter	Mendenhall	Walter Mendenhall	CIN	RB	6-0	227	Illinois State	0	0000-00-00
944	f	Larry	Beavers	Larry Beavers	NO	WR	5-10	186	Wesleyan	0	1985-10-07
956	f	Dobson	Collins	Dobson Collins	PHI	WR	6-2	189	Gardner-Webb	11	0000-00-00
973	f	Khalil	Jones	Khalil Jones	GB	WR				0	0000-00-00
990	f	Samie	Parker	Samie Parker	OAK	WR	5-11	185	Oregon	0	0000-00-00
1000	f	Matt	Simon	Matt Simon	NO	WR	6-1	199	Northern Illinois	0	0000-00-00
1014	f	Colin	Cloherty	Colin Cloherty	ATL	TE	6-2	252	Brown	81	0000-00-00
1025	f	Ryan	Purvis	Ryan Purvis	NYG	TE	6-4	261	Boston College	83	1986-05-08
1044	t	Tennessee	Titans	Tennessee Titans	TEN	DEF				0	0000-00-00
1061	t	Denver	Broncos	Denver Broncos	DEN	DEF				0	0000-00-00
1075	f	Todd	Boeckman	Todd Boeckman	JAC	QB	6-5	244	Ohio State	0	0000-00-00
1085	f	Shaine	Smith	Shaine Smith	BUF	WR	6-2	189	Hofstra	0	0000-00-00
1096	f	Joe	Kowalewski	Joe Kowalewski	MIA	TE	6-4	250	Syracuse	0	0000-00-00
1109	t	Sam	Bradford	Sam Bradford	PHI	QB				7	1969-12-31
1119	f	John	Skelton	John Skelton	TEN	QB	6-6	250	Fordham	8	0000-00-00
1131	f	Martell	Mallett	Martell Mallett	NYG	RB	6-0	210	Arkansas-Pine Bluff	0	0000-00-00
1143	t	Dez	Bryant	Dez Bryant	DAL	WR				88	1969-12-31
1153	f	Mardy	Gilyard	Mardy Gilyard	KC	WR	6-1	187	Cincinnati	13	0000-00-00
1161	t	Dexter	McCluster	Dexter McCluster	TEN	RB				22	1969-12-31
1171	f	Larry	Taylor	Larry Taylor	NYJ	WR	5-6	177	Connecticut	15	0000-00-00
1183	t	Jim	Dray	Jim Dray	CLE	TE				81	1969-12-31
1196	t	Fendi	Onobun	Fendi Onobun	JAC	TE	6-6	260	Houston	48	1986-11-17
1206	f	Ricky	Schmitt	Ricky Schmitt	TEN	K	6-3	217	Shepherd	3	1985-08-17
1218	f	Jevan	Snead	Jevan Snead	TB	QB	6-3	219	Mississippi	0	0000-00-00
1232	f	Chad	Kackert	Chad Kackert	JAC	RB	5-9	199	New Hampshire	0	0000-00-00
1244	f	Demetrius	Taylor	Demetrius Taylor	PIT	RB				0	0000-00-00
1254	f	Bryan	Anderson	Bryan Anderson	NE	WR				0	0000-00-00
1262	t	Victor	Cruz	Victor Cruz	NYG	WR				80	1969-12-31
1272	f	Marquis	Hamilton	Marquis Hamilton	MIN	WR	6-3	222	Iowa State	0	0000-00-00
1286	t	Marlon	Moore	Marlon Moore	CLE	WR				15	1969-12-31
1298	f	Bobby	Sewall	Bobby Sewall	TB	WR	6-1	197	Brown	14	0000-00-00
1312	t	Mike	Caussin	Mike Caussin	WAS	TE	6-5	243	James Madison	40	1987-02-26
1322	t	Logan	Paulsen	Logan Paulsen	WAS	TE				82	1969-12-31
1334	f	Eric	Ward	Eric Ward	ATL	QB				0	0000-00-00
1347	f	Victor	James	Victor James	SEA	WR				0	0000-00-00
1359	f	Ryan	Moya	Ryan Moya	CLE	TE				0	0000-00-00
1369	f	Nyan	Boateng	Nyan Boateng	NYG	WR	6-1	204	California	0	0000-00-00
1383	f	Chris	Carter	Chris Carter	SEA	WR				0	0000-00-00
1395	t	Jake	Locker	Jake Locker	TEN	QB	6-3	223	Washington	10	0000-00-00
1405	f	Allen	Bradford	Allen Bradford	SEA	RB	5-11	235	USC	44	0000-00-00
1418	t	Dion	Lewis	Dion Lewis	NE	RB				33	1969-12-31
1428	t	Daniel	Thomas	Daniel Thomas	CHI	RB				37	1969-12-31
1436	f	Stephen	Burton	Stephen Burton	JAC	WR	6-1	224	West Texas A&M	15	0000-00-00
1446	t	Julio	Jones	Julio Jones	ATL	WR				11	1969-12-31
1457	t	Cecil	Shorts III	Cecil Shorts III	HOU	WR				18	1969-12-31
1466	t	Lance	Kendricks	Lance Kendricks	STL	TE				88	1969-12-31
1477	f	Ryan	Colburn	Ryan Colburn	NO	QB				0	0000-00-00
1491	f	Marc	Verica	Marc Verica	WAS	QB				0	0000-00-00
1504	f	Darren	Evans	Darren Evans	TEN	RB	6-0	232	Virginia Tech	36	0000-00-00
1514	f	Derrick	Locke	Derrick Locke	PHI	RB				0	0000-00-00
1527	f	Steven	Robinson	Steven Robinson	CIN	RB				0	0000-00-00
1536	f	Austin	Sylvester	Austin Sylvester	DEN	RB	6-1	248	Washington	40	0000-00-00
1545	f	Darvin	Adams	Darvin Adams	CAR	WR	6-2	195	Auburn	13	0000-00-00
1553	f	Tyler	Beiler	Tyler Beiler	PIT	WR	6-0	190	Bridgewater College	19	0000-00-00
1563	f	Jalil	Carter	Jalil Carter	STL	WR				0	0000-00-00
1573	t	Drew	Davis	Drew Davis	ATL	WR	6-1	205	Oregon	19	0000-00-00
1586	t	Marcus	Harris	Marcus Harris	NYG	WR	6-1	187	Murray State	18	0000-00-00
1597	f	Jared	Jenkins	Jared Jenkins	STL	WR				0	0000-00-00
1607	f	Ryan	Lincoln	Ryan Lincoln	MIN	WR				0	0000-00-00
1620	t	Jamar	Newsome	Jamar Newsome	DAL	WR	6-1	206	Central Florida	85	0000-00-00
1632	f	Cordarol	Scales	Cordarol Scales	NYJ	WR				0	0000-00-00
1645	t	Teddy	Williams	Teddy Williams	ARI	WR				29	1969-12-31
1655	f	DeMarco	Cosby	DeMarco Cosby	TEN	TE	6-3	240	Central Missouri State	86	0000-00-00
1668	f	Johdrick	Morris	Johdrick Morris	BAL	TE				0	0000-00-00
1680	f	Joe	Torchia	Joe Torchia	WAS	TE				0	0000-00-00
1690	f	Josh	Jasper	Josh Jasper	TB	K				0	0000-00-00
1710	f	John	Kasay	John Kasay	NO	K	5-10	210	Georgia	2	0000-00-00
1757	f	John	Potter	John Potter	WAS	K	6-1	219	Western Michigan	1	0000-00-00
1771	t	Spencer	Larsen	Spencer Larsen	TB	RB	6-2	245	Arizona	46	0000-00-00
1783	f	Jordan	White	Jordan White	NYJ	WR	6-0	215	Western Michigan	89	0000-00-00
1794	t	James	Hanna	James Hanna	DAL	TE				84	1969-12-31
1801	t	Nick	Foles	Nick Foles	STL	QB				5	1969-12-31
1812	f	Sam	Kirkland	Sam Kirkland	WAS	WR	6-0	190	Kent State	14	0000-00-00
948	f	D.J.	Boldin	D.J. Boldin	DET	WR	5-11	210	Wake Forest	0	0000-00-00
962	f	Ed	Gant	Ed Gant	TB	WR	6-3	200	North Alabama	15	1987-01-24
972	f	Taurus	Johnson	Taurus Johnson	WAS	WR	6-1	205	South Florida	0	0000-00-00
982	f	Shane	Morales	Shane Morales	ARI	WR				0	0000-00-00
996	f	Brandon	Robinson	Brandon Robinson	PHI	WR				0	0000-00-00
1008	t	Kevin	Brock	Kevin Brock	NO	TE				85	1969-12-31
1018	f	Travis	Dekker	Travis Dekker	GB	TE				0	0000-00-00
1032	f	Alex	Romero	Alex Romero	SF	K	5-9	180	Nicholls State	0	0000-00-00
1043	t	Philadelphia	Eagles	Philadelphia Eagles	PHI	DEF				0	0000-00-00
1057	t	Tampa	Bay Buccaneers	Tampa Bay Buccaneers	TB	DEF				0	0000-00-00
1074	f	Kareem	Brown	Kareem Brown	NYJ	TE	6-4	285	Miami (Fla.)	79	0000-00-00
1084	f	Willie	Reid	Willie Reid	DAL	WR	5-10	192	Florida State	0	0000-00-00
1097	f	Brett	Favre	Brett Favre	MIN	QB	6-2	222	Southern Mississippi	4	1969-10-10
1111	f	Sean	Canfield	Sean Canfield	NO	QB	6-4	223	Oregon State	4	1986-11-12
1128	f	Montario	Hardesty	Montario Hardesty	CLE	RB	6-0	225	Tennessee	20	1987-02-01
1145	t	Riley	Cooper	Riley Cooper	PHI	WR				14	1969-12-31
1155	f	Ryan	Grice-Mullen	Ryan Grice-Mullen	MIA	WR	5-11	180	Hawaii	0	0000-00-00
1167	f	Titus	Ryan	Titus Ryan	NYJ	WR	6-0	193	Concordia (AL)	86	0000-00-00
1180	f	Leroy	Banks	Leroy Banks	KC	TE	6-3	243	Southern Mississippi	0	0000-00-00
1190	t	Clay	Harbor	Clay Harbor	JAC	TE				86	1969-12-31
1201	t	Mickey	Shuler	Mickey Shuler	ATL	TE				82	1969-12-31
1215	f	Matt	Nichols	Matt Nichols	DAL	QB	6-2	222	Eastern Washington	0	0000-00-00
1227	t	Chris	Ivory	Chris Ivory	NYJ	RB				33	1969-12-31
1237	f	Chane	Moline	Chane Moline	OAK	RB	6-0	250	UCLA	0	0000-00-00
1250	f	Keiland	Williams	Keiland Williams	WAS	RB	5-11	230	LSU	25	0000-00-00
1259	f	Tim	Brown	Tim Brown	ARI	WR	5-8	165	Rutgers	0	0000-00-00
1269	f	Trent	Guy	Trent Guy	CAR	WR	5-8	171	Louisville	84	1987-08-22
1280	f	Max	Komar	Max Komar	CHI	WR	5-11	202	Idaho	83	0000-00-00
1289	f	Jeff	Moturi	Jeff Moturi	DAL	WR				0	0000-00-00
1297	t	Naaman	Roosevelt	Naaman Roosevelt	BUF	WR	6-0	200	Buffalo	13	0000-00-00
1307	f	Blair	White	Blair White	IND	WR	6-2	205	Michigan State	15	0000-00-00
1316	f	T.C.	Drake	T.C. Drake	KC	TE				0	0000-00-00
1325	f	Nick	Tow-Arnett	Nick Tow-Arnett	SEA	TE	6-3	248	Minnesota	0	0000-00-00
1333	f	Riley	Skinner	Riley Skinner	NYG	QB				0	0000-00-00
1342	f	Deryn	Bowser	Deryn Bowser	ARI	WR				0	0000-00-00
1352	t	Andy	Tanner	Andy Tanner	NO	WR	6-0	183	Midwestern State	14	1988-05-16
1366	f	Bill	Stull	Bill Stull	KC	QB	6-3	215	Pittsburgh	0	0000-00-00
1378	f	Chris	Greisen	Chris Greisen	DAL	QB				0	0000-00-00
1388	f	Rod	Windsor	Rod Windsor	CLE	WR	6-2	205	Western New Mexico	13	0000-00-00
1402	t	T.J.	Yates	T.J. Yates	HOU	QB				13	1969-12-31
1420	t	Bruce	Miller	Bruce Miller	SF	RB				49	1969-12-31
1430	t	Shane	Vereen	Shane Vereen	NYG	RB				34	1969-12-31
1441	t	A.J.	Green	A.J. Green	CIN	WR				18	1969-12-31
1454	t	Aldrick	Robinson	Aldrick Robinson	BAL	WR				13	1969-12-31
1473	f	Alex	Henery	Alex Henery	DET	K	6-1	177	Nebraska	6	0000-00-00
1486	f	Zac	Lee	Zac Lee	SEA	QB				0	0000-00-00
1496	t	Matt	Asiata	Matt Asiata	MIN	RB				44	1969-12-31
1510	f	Korey	Hall	Korey Hall	NO	RB	6-0	236	Boise State	35	0000-00-00
1528	f	Dean	Rogers	Dean Rogers	SD	RB				0	0000-00-00
1539	f	Vai	Taua	Vai Taua	SEA	RB				0	0000-00-00
1554	t	Armon	Binns	Armon Binns	KC	WR	6-3	211	Cincinnati	0	0000-00-00
1564	f	L.J.	Castile	L.J. Castile	CLE	WR				0	0000-00-00
1574	f	Dan	DePalma	Dan DePalma	SD	WR	5-11	192	West Chester	82	0000-00-00
1585	f	Jamel	Hamler	Jamel Hamler	PHI	WR	6-2	195	Fresno State	84	0000-00-00
1596	f	Sean	Jeffcoat	Sean Jeffcoat	ARI	WR				0	0000-00-00
1606	f	Detron	Lewis	Detron Lewis	TB	WR				0	0000-00-00
1613	f	Terrence	McCrae	Terrence McCrae	PIT	WR				0	0000-00-00
1623	f	Travon	Patterson	Travon Patterson	SD	WR				0	0000-00-00
1630	f	Jock	Sanders	Jock Sanders	TB	WR				0	0000-00-00
1640	f	Terrance	Turner	Terrance Turner	PHI	WR				0	0000-00-00
1648	f	Kyle	Adams	Kyle Adams	CHI	TE	6-4	255	Purdue	86	0000-00-00
1658	f	Collin	Franklin	Collin Franklin	TB	TE	6-5	252	Iowa State	85	0000-00-00
1666	t	Mike	McNeill	Mike McNeill	CAR	TE	6-4	240	Nebraska	0	0000-00-00
1675	f	A.J.	Simmons	A.J. Simmons	ATL	TE				0	0000-00-00
1684	f	Will	Yeatman	Will Yeatman	MIA	TE	6-6	270	Maryland	89	0000-00-00
1694	f	Nate	Whitaker	Nate Whitaker	MIN	K				0	0000-00-00
1701	f	Calvin	Russell	Calvin Russell	CIN	WR				0	0000-00-00
1756	t	T.J.	Graham	T.J. Graham	NO	WR				10	1969-12-31
1764	f	Jeff	Fuller	Jeff Fuller	MIA	WR	6-4	223	Texas A&M	14	0000-00-00
1775	t	G.J.	Kinne	G.J. Kinne	PHI	QB				4	1969-12-31
1790	t	Saalim	Hakim	Saalim Hakim	NYJ	WR				19	1969-12-31
1802	t	Bryce	Brown	Bryce Brown	SEA	RB				35	1969-12-31
1813	f	Beau	Reliford	Beau Reliford	WAS	TE	6-6	269	Florida State	87	0000-00-00
1828	f	Thomas	Mayo	Thomas Mayo	NYJ	WR	6-2	205	California (PA)	81	0000-00-00
1842	t	Chris	Owusu	Chris Owusu	NYJ	WR				17	1969-12-31
1853	t	Austin	Davis	Austin Davis	CLE	QB				7	1969-12-31
1363	f	Brit	Miller	Brit Miller	STL	RB	6-1	253	Illinois	49	1986-09-15
1373	f	Greg	Mathews	Greg Mathews	STL	WR	6-3	207	Michigan	0	1988-01-28
1380	t	Tyler	Clutts	Tyler Clutts	DAL	RB				44	1969-12-31
1390	f	Fabrizio	Scaccia	Fabrizio Scaccia	SF	K				0	0000-00-00
1399	t	Christian	Ponder	Christian Ponder	DEN	QB				9	1969-12-31
1409	f	Jay	Finley	Jay Finley	CIN	RB	5-11	203	Baylor	33	0000-00-00
1415	t	Mark	Ingram	Mark Ingram	NO	RB				22	1969-12-31
1426	f	Da'Rel	Scott	Da'Rel Scott	NYG	RB	5-11	210	Maryland	33	0000-00-00
1434	t	Jonathan	Baldwin	Jonathan Baldwin	SF	WR	6-4	230	Pittsburgh	84	0000-00-00
1444	t	Jerrel	Jernigan	Jerrel Jernigan	NYG	WR	5-8	189	Troy	12	0000-00-00
1451	t	Niles	Paul	Niles Paul	WAS	TE				84	1969-12-31
1461	t	Jordan	Cameron	Jordan Cameron	MIA	TE				84	1969-12-31
1468	t	Lee	Smith	Lee Smith	OAK	TE				86	1969-12-31
1479	t	Pat	Devlin	Pat Devlin	MIN	QB				7	1969-12-31
1488	f	Josh	Portis	Josh Portis	SEA	QB	6-3	211	California (PA)	2	0000-00-00
1498	f	John	Clay	John Clay	PIT	RB	6-1	233	Wisconsin	38	0000-00-00
1506	f	C.J.	Gable	C.J. Gable	DEN	RB				0	0000-00-00
1517	f	Ryan	Mahaffey	Ryan Mahaffey	BAL	RB				0	0000-00-00
1524	f	Zac	Pauga	Zac Pauga	HOU	RB				0	0000-00-00
1533	f	Chad	Spann	Chad Spann	NYJ	RB	5-9	198	Northern Illinois	34	0000-00-00
1542	f	Jonathan	Williams	Jonathan Williams	CIN	RB				0	0000-00-00
1550	f	Demario	Ballard	Demario Ballard	DET	WR				0	0000-00-00
1559	f	Brandon	Caleb	Brandon Caleb	PHI	WR				0	0000-00-00
1568	f	Travis	Cobb	Travis Cobb	CHI	WR				0	0000-00-00
1577	f	Jarred	Fayson	Jarred Fayson	NO	WR	6-0	214	Illinois	11	0000-00-00
1583	f	Eric	Greenwood	Eric Greenwood	PIT	WR				0	0000-00-00
1593	f	Joe	Horn	Joe Horn	IND	WR				0	0000-00-00
1601	f	Gerald	Jones	Gerald Jones	PHI	WR				0	0000-00-00
1610	t	Jeff	Maehl	Jeff Maehl	PHI	WR				88	1969-12-31
1617	f	Larrone	Moore	Larrone Moore	IND	WR				0	0000-00-00
1626	t	Michael	Preston	Michael Preston	MIA	WR				19	1969-12-31
1636	f	Owen	Spencer	Owen Spencer	CLE	WR	6-3	185	North Carolina State	17	0000-00-00
1643	t	LaQuan	Williams	LaQuan Williams	BAL	WR	6-0	195	Maryland	15	0000-00-00
1653	f	Vaughn	Charlton	Vaughn Charlton	PIT	TE				0	0000-00-00
1661	f	Charlie	Gantt	Charlie Gantt	KC	TE				0	0000-00-00
1671	t	Allen	Reisner	Allen Reisner	BAL	TE				85	1969-12-31
1678	f	Greg	Smith	Greg Smith	CAR	TE	6-4	255	Texas	85	0000-00-00
1688	f	Jacob	Harfman	Jacob Harfman	BAL	K				0	0000-00-00
1697	f	Caleb	King	Caleb King	MIN	RB	5-11	212	Georgia	38	0000-00-00
1728	f	Justin	Snow	Justin Snow	IND	TE	6-3	240	Baylor	48	0000-00-00
1760	f	Jerome	Messam	Jerome Messam	MIA	RB	6-4	250	Graceland	35	0000-00-00
1768	t	Michael	Egnew	Michael Egnew	PIT	TE				80	1969-12-31
1780	f	DaMarcus	Ganaway	DaMarcus Ganaway	NYJ	WR	6-2	185	Kentucky Wesleyan	87	0000-00-00
1788	t	Tim	Benford	Tim Benford	PIT	WR	5-11	198	Tennessee Tech	0	0000-00-00
1798	t	Henry	Hynoski	Henry Hynoski	NYG	RB				45	1969-12-31
1806	t	Robert	Griffin	Robert Griffin III	WAS	QB				10	1969-12-31
1816	t	Eric	Page	Eric Page	TB	WR	5-10	180	Toledo	17	0000-00-00
1825	t	Derek	Carrier	Derek Carrier	WAS	TE				89	1969-12-31
1837	f	Cameron	Bell	Cameron Bell	SF	RB	6-2	252	Northern Illinois	44	0000-00-00
1844	t	Brian	Tyms	Brian Tyms	NE	WR				84	1969-12-31
1855	f	Calvin	Middleton	Calvin Middleton	STL	RB	5-11	220	Jacksonville State	36	0000-00-00
1865	t	Greg	Zuerlein	Greg Zuerlein	STL	K				4	1969-12-31
1872	t	Marvin	Jones	Marvin Jones	CIN	WR				82	1969-12-31
1884	t	Josh	Cooper	Josh Cooper	CLE	WR	5-10	190	Oklahoma State	88	0000-00-00
1893	t	David	Paulson	David Paulson	SD	TE				81	1969-12-31
1905	f	Alex	Gottlieb	Alex Gottlieb	ARI	TE	6-4	255	William & Mary	80	0000-00-00
1912	f	Jon	Hoese	Jon Hoese	OAK	RB	6-2	248	Minnesota	40	0000-00-00
1924	t	Rhett	Ellison	Rhett Ellison	MIN	TE				85	1969-12-31
1931	t	Chandler	Harnish	Chandler Harnish	ARI	QB	6-2	222	Northern Illinois	0	0000-00-00
1944	t	Joe	Banyard	Joe Banyard	JAC	RB				23	1969-12-31
1955	f	Darryl	Whiting	Darryl Whiting	TEN	RB	6-3	235	Fordham	0	0000-00-00
1966	f	Michael	Calvin	Michael Calvin	ATL	WR	6-3	215	California	15	0000-00-00
1974	t	Travaris	Cadet	Travaris Cadet	SF	RB				39	1969-12-31
1985	f	Tyler	Shoemaker	Tyler Shoemaker	KC	WR	6-1	213	Boise State	14	0000-00-00
1992	f	Brandon	Collins	Brandon Collins	NYG	WR	5-11	180	Southeastern Louisiana	6	0000-00-00
2002	t	Chase	Ford	Chase Ford	BAL	TE				86	1969-12-31
2010	f	Tre'Shawn	Robinson	Tre'Shawn Robinson	OAK	RB	5-11	250	Idaho	0	0000-00-00
2020	f	Phillip	Payne	Phillip Payne	SD	WR	6-3	210	Nevada-Las Vegas	7	0000-00-00
2028	t	Cooper	Helfet	Cooper Helfet	SEA	TE				84	1969-12-31
2037	t	Deonte	Thompson	Deonte Thompson	CHI	WR				80	1969-12-31
2045	f	Joe	Anderson	Joe Anderson	CHI	WR	6-1	196	Texas Southern	19	0000-00-00
2054	f	Jason	Ford	Jason Ford	PIT	RB	5-10	251	Illinois	21	0000-00-00
2061	f	David	Legree	David Legree	IND	QB	6-4	249	Hampton	0	0000-00-00
2070	f	Wes	Kemp	Wes Kemp	CAR	WR	6-3	233	Missouri	0	0000-00-00
2078	f	Gino	Crump	Gino Crump	ARI	WR	6-2	210	Arizona	89	0000-00-00
2087	f	Michael	Avila	Michael Avila	CAR	WR	5-8	165	San Jose State	17	0000-00-00
1376	f	Joe	Reitz	Joe Reitz	IND	TE	6-7	320	Western Michigan	76	1985-08-24
1386	f	Wes	Lyons	Wes Lyons	PIT	WR	6-8	233	West Virginia	0	0000-00-00
1393	t	Blaine	Gabbert	Blaine Gabbert	SF	QB				2	1969-12-31
1403	f	Anthony	Allen	Anthony Allen	BAL	RB	6-1	223	Georgia Tech	35	0000-00-00
1412	t	Stanley	Havili	Stanley Havili	IND	RB	6-0	243	USC	39	0000-00-00
1421	t	DeMarco	Murray	DeMarco Murray	PHI	RB				29	1969-12-31
1431	f	Johnny	White	Johnny White	GB	RB	5-10	202	North Carolina	34	0000-00-00
1438	t	Tandon	Doss	Tandon Doss	JAC	WR				17	1969-12-31
1448	t	Greg	Little	Greg Little	CIN	WR				11	1969-12-31
1455	t	Greg	Salas	Greg Salas	DET	WR				18	1969-12-31
1464	f	Daniel	Hardy	Daniel Hardy	TB	TE	6-4	249	Idaho	88	0000-00-00
1475	t	McLeod	Bethel-Thompson	McLeod Bethel-Thompson	MIA	QB				16	1969-12-31
1482	f	Adam	Froman	Adam Froman	ATL	QB				0	0000-00-00
1492	f	Trevor	Vittatoe	Trevor Vittatoe	IND	QB	6-2	220	Texas-El Paso	0	0000-00-00
1501	f	Dan	Dierking	Dan Dierking	CHI	RB				0	0000-00-00
1511	t	DuJuan	Harris	DuJuan Harris	SF	RB				20	1969-12-31
1520	f	Thor	Merrow	Thor Merrow	ATL	RB				0	0000-00-00
1529	f	Brandon	Saine	Brandon Saine	GB	RB	5-11	220	Ohio State	33	0000-00-00
1537	f	Philip	Sylvester	Philip Sylvester	ATL	RB				0	0000-00-00
1546	t	Kris	Adams	Kris Adams	NYG	WR	6-3	194	Texas-El Paso	89	0000-00-00
1556	f	Diondre	Borel	Diondre Borel	TEN	WR	6-0	199	Utah State	19	0000-00-00
1561	t	Michael	Campbell	Michael Campbell	NYJ	WR	6-2	205	Temple	18	0000-00-00
1571	f	Daiveun	Curry-Chapman	Daiveun Curry-Chapman	ARI	WR				0	0000-00-00
1579	t	David	Gilreath	David Gilreath	SEA	WR	5-10	169	Wisconsin	6	0000-00-00
1589	f	Joseph	Hills	Joseph Hills	TEN	WR				0	0000-00-00
1595	t	Lestar	Jean	Lestar Jean	MIN	WR	6-3	202	Florida Atlantic	0	0000-00-00
1605	f	Lyle	Leong	Lyle Leong	DAL	WR				0	0000-00-00
1612	t	Chris	Matthews	Chris Matthews	SEA	WR				13	1969-12-31
1622	f	Juan	Nunez	Juan Nunez	CLE	WR				0	0000-00-00
1629	t	Jeremy	Ross	Jeremy Ross	BAL	WR				12	1969-12-31
1639	t	Terrence	Toliver	Terrence Toliver	CHI	WR	6-5	204	LSU	0	0000-00-00
1647	f	Terrell	Zachery	Terrell Zachery	BAL	WR				0	0000-00-00
1657	f	Harry	Flaherty	Harry Flaherty	NO	TE				0	0000-00-00
1665	f	Chris	Hopkins	Chris Hopkins	NYG	TE	6-5	277	Toledo	84	0000-00-00
1674	t	Weslye	Saunders	Weslye Saunders	IND	TE	6-5	261	South Carolina	47	0000-00-00
1683	f	Ryan	Winterswyk	Ryan Winterswyk	ATL	TE				0	0000-00-00
1693	f	Thomas	Weber	Thomas Weber	CIN	K	6-0	204	Arizona State	3	0000-00-00
1699	f	William	Powell	William Powell	ARI	RB	5-9	207	Kansas State	33	0000-00-00
1755	f	Chris	Douglas	Chris Douglas	BUF	RB	5-9	212	Missouri State	35	0000-00-00
1763	t	B.J.	Cunningham	B.J. Cunningham	PHI	WR	6-2	215	Michigan State	89	0000-00-00
1774	f	Brad	Herman	Brad Herman	NE	TE	6-5	255	Iowa	0	0000-00-00
1782	f	Royce	Pollard	Royce Pollard	NYJ	WR	6-1	175	Hawaii	18	0000-00-00
1793	f	George	Bryan	George Bryan	DAL	TE	6-5	265	North Carolina State	0	0000-00-00
1800	t	Adrien	Robinson	Adrien Robinson	NYG	TE				81	1969-12-31
1811	f	Brian	Hernandez	Brian Hernandez	WAS	WR	6-0	183	Utah	19	0000-00-00
1818	t	Cyrus	Gray	Cyrus Gray	KC	RB	5-10	206	Texas A&M	32	0000-00-00
1830	t	Andre	Hardy	Andre Hardy	ARI	TE	6-6	245	Cal State-Fullerton	80	0000-00-00
1839	t	LaMichael	James	LaMichael James	MIA	RB				27	1969-12-31
1851	f	Lavasier	Tuinei	Lavasier Tuinei	NE	WR	6-4	220	Oregon	19	0000-00-00
1858	f	Nick	Schwieger	Nick Schwieger	STL	RB	5-10	210	Dartmouth	35	0000-00-00
1868	f	Tyler	Hansen	Tyler Hansen	CIN	QB	6-1	225	Colorado	9	0000-00-00
1877	t	Brandon	Weeden	Brandon Weeden	HOU	QB				3	1969-12-31
1889	f	Mike	Adams	Mike Adams	PIT	WR	6-7	323	Ohio State	0	0000-00-00
1898	t	Kellen	Moore	Kellen Moore	DAL	QB				17	1969-12-31
1908	f	B.J.	Coleman	B.J. Coleman	GB	QB	6-3	231	Tennessee-Chattanooga	9	0000-00-00
1916	f	Cameron	Ford	Cameron Ford	GB	TE	6-4	255	Wake Forest	0	0000-00-00
1927	t	Keshawn	Martin	Keshawn Martin	NE	WR				82	1969-12-31
1938	t	Jabin	Sambrano	Jabin Sambrano	ATL	WR	5-11	190	Montana	8	0000-00-00
1951	f	D.J.	Hall	D.J. Hall	JAC	WR	6-1	312	Texas State	61	0000-00-00
1959	f	LaQuinton	Evans	LaQuinton Evans	TEN	WR	6-1	203	Southern University	16	0000-00-00
1969	f	James	Rodgers	James Rodgers	ATL	WR	5-7	188	Oregon State	18	0000-00-00
1979	t	Jake	Byrne	Jake Byrne	SD	TE	6-4	258	Wisconsin	81	0000-00-00
1988	f	Tyler	Urban	Tyler Urban	NE	TE	6-5	249	West Virginia	47	0000-00-00
1996	t	Chris	Polk	Chris Polk	HOU	RB				22	1969-12-31
2005	f	Taylor	Gentry	Taylor Gentry	KC	RB	6-2	250	North Carolina State	49	0000-00-00
2015	f	Michael	Hayes	Michael Hayes	SD	RB	5-8	200	Houston	34	0000-00-00
2023	f	Stanley	Arukwe	Stanley Arukwe	NYJ	WR	5-11	184	Troy	16	0000-00-00
2033	f	Brandon	Pendergrass	Brandon Pendergrass	BAL	RB	5-9	200	Wake Forest	0	0000-00-00
2039	f	Bruce	Figgins	Bruce Figgins	BAL	TE	6-4	278	Georgia	87	0000-00-00
2049	f	Brandon	Venson	Brandon Venson	CHI	TE	6-3	245	Illinois State	85	0000-00-00
2056	t	Davin	Meggett	Davin Meggett	IND	RB	5-8	219	Maryland	23	0000-00-00
2066	t	Tauren	Poole	Tauren Poole	PIT	RB	5-10	210	Tennessee	38	0000-00-00
2072	t	Chris	Givens	Chris Givens	NO	WR	6-2	203	Miami (Ohio)	0	0000-00-00
1410	t	Alex	Green	Alex Green	NYJ	RB	6-2	220	Hawaii	25	0000-00-00
1416	t	Taiwan	Jones	Taiwan Jones	OAK	RB				22	1969-12-31
1427	t	Anthony	Sherman	Anthony Sherman	KC	RB				42	1969-12-31
1435	t	Vincent	Brown	Vincent Brown	SD	WR				14	1969-12-31
1445	f	Ronald	Johnson	Ronald Johnson	PHI	WR	5-10	185	USC	80	0000-00-00
1452	t	Austin	Pettis	Austin Pettis	SD	WR				82	1969-12-31
1462	t	Richard	Gordon	Richard Gordon	BAL	TE				89	1969-12-31
1469	t	Luke	Stocker	Luke Stocker	TB	TE				88	1969-12-31
1480	f	Chris	Dieker	Chris Dieker	PIT	QB				0	0000-00-00
1489	f	Taylor	Potts	Taylor Potts	STL	QB				0	0000-00-00
1499	f	Graig	Cooper	Graig Cooper	PHI	RB	5-10	205	Miami (Fla.)	0	0000-00-00
1507	f	Kevin	Gidrey	Kevin Gidrey	WAS	RB				0	0000-00-00
1518	f	Jeremiah	Masoli	Jeremiah Masoli	SF	QB				0	0000-00-00
1525	t	Chase	Reynolds	Chase Reynolds	STL	RB				34	1969-12-31
1534	f	Van	Stumon	Van Stumon	STL	RB				0	0000-00-00
1543	t	Darrel	Young	Darrel Young	WAS	RB				36	1969-12-31
1551	f	Dominique	Barnes	Dominique Barnes	DET	WR				0	0000-00-00
1560	f	Cordario	Calvin	Cordario Calvin	PHI	WR				0	0000-00-00
1569	t	Kevin	Cone	Kevin Cone	MIA	WR				40	1969-12-31
1578	f	Michael	Galatas	Michael Galatas	NO	WR				0	0000-00-00
1584	t	Tori	Gurley	Tori Gurley	BUF	WR	6-4	230	South Carolina	17	0000-00-00
1594	t	Dontrelle	Inman	Dontrelle Inman	SD	WR				15	1969-12-31
1602	f	Cameron	Kenney	Cameron Kenney	DEN	WR	6-1	193	Oklahoma	19	0000-00-00
1611	f	Chris	Manno	Chris Manno	CAR	WR	5-10	185	Hofstra	0	0000-00-00
1618	t	Joseph	Morgan	Joseph Morgan	BAL	WR				13	1969-12-31
1627	f	Raymond	Radway	Raymond Radway	STL	WR	6-3	204	Abilene Christian	15	0000-00-00
1637	f	Giovanni	Stanley	Giovanni Stanley	SD	WR				0	0000-00-00
1644	f	Marshall	Williams	Marshall Williams	IND	WR				0	0000-00-00
1654	f	Colin	Cochart	Colin Cochart	DAL	TE	6-4	249	South Dakota State	86	0000-00-00
1662	f	Cameron	Graham	Cameron Graham	STL	TE	6-3	244	Louisville	81	0000-00-00
1672	t	Konrad	Reuland	Konrad Reuland	BAL	TE				86	1969-12-31
1679	f	Brandon	Taylor	Brandon Taylor	SD	TE	5-11	209	LSU	28	0000-00-00
1689	f	Chris	Hazley	Chris Hazley	BUF	K				0	0000-00-00
1698	t	Mikel	Leshoure	Mikel Leshoure	DET	RB	6-0	233	Illinois	25	0000-00-00
1731	f	Ovie	Mughelli	Ovie Mughelli	STL	RB	6-1	250	Wake Forest	34	0000-00-00
1761	t	Lamar	Miller	Lamar Miller	MIA	RB				26	1969-12-31
1769	t	Brandon	Bolden	Brandon Bolden	NE	RB				38	1969-12-31
1781	t	Stephen	Hill	Stephen Hill	CAR	WR	6-4	215	Georgia Tech	87	0000-00-00
1789	t	Danny	Coale	Danny Coale	PIT	WR	6-0	187	Virginia Tech	0	0000-00-00
1799	t	Rueben	Randle	Rueben Randle	NYG	WR				82	1969-12-31
1807	f	Antwon	Bailey	Antwon Bailey	WAS	RB	5-7	195	Syracuse	34	0000-00-00
1817	t	Gerell	Robinson	Gerell Robinson	MIA	TE				89	1969-12-31
1826	t	Brandon	Carswell	Brandon Carswell	SF	WR	6-1	201	USC	13	0000-00-00
1838	t	Jewel	Hampton	Jewel Hampton	SF	RB	5-9	210	Southern Illinois	33	0000-00-00
1845	t	Garrett	Celek	Garrett Celek	SF	TE				88	1969-12-31
1856	t	Isaiah	Pead	Isaiah Pead	PIT	RB				24	1969-12-31
1866	t	Bernard	Pierce	Bernard Pierce	JAC	RB				30	1969-12-31
1873	t	Kashif	Moore	Kashif Moore	PIT	WR	5-9	180	Connecticut	19	0000-00-00
1885	f	Bert	Reed	Bert Reed	PIT	WR	5-10	183	Florida State	0	0000-00-00
1895	t	Alshon	Jeffery	Alshon Jeffery	CHI	WR				17	1969-12-31
1906	f	Austin	Wells	Austin Wells	DET	TE	6-3	255	Northern Iowa	45	0000-00-00
1913	t	Dale	Moss	Dale Moss	CHI	WR	6-3	215	South Dakota State	13	0000-00-00
1925	t	Blair	Walsh	Blair Walsh	MIN	K				3	1969-12-31
1932	t	Andrew	Luck	Andrew Luck	IND	QB				12	1969-12-31
1947	t	Justin	Blackmon	Justin Blackmon	JAC	WR	6-1	210	Oklahoma State	14	0000-00-00
1957	f	Devin	Aguilar	Devin Aguilar	TEN	WR	5-11	200	Washington	81	0000-00-00
1967	f	Marcus	Jackson	Marcus Jackson	ATL	WR	6-1	195	Lamar	16	0000-00-00
1975	f	Malcolm	Beyah	Malcolm Beyah	NO	WR	5-11	182	Middle Tennessee State	0	0000-00-00
1986	f	Drake	Dunsmore	Drake Dunsmore	TB	TE	6-3	235	Northwestern	81	0000-00-00
1993	f	David	Douglas	David Douglas	TB	WR	6-1	205	Arizona	89	0000-00-00
2003	t	Emil	Igwenagu	Emil Igwenagu	DET	RB				44	1969-12-31
2011	f	Tray	Session	Tray Session	OAK	WR	6-2	195	Nevada	87	0000-00-00
2021	t	Michael	Willie	Michael Willie	BAL	WR	6-2	220	Arizona State	13	0000-00-00
2029	t	Sean	McGrath	Sean McGrath	SD	TE				46	1969-12-31
2038	f	Matt	Balasavage	Matt Balasavage	BAL	TE	6-4	250	Temple	89	0000-00-00
2046	f	Terriun	Crump	Terriun Crump	TB	WR	6-2	223	Western Illinois	13	0000-00-00
2055	t	Jonathan	Grimes	Jonathan Grimes	HOU	RB				41	1969-12-31
2062	f	Lee	Meisner	Lee Meisner	ATL	RB	6-0	245	Southern Colorado	43	0000-00-00
2071	f	Rico	Wallace	Rico Wallace	CAR	WR	6-3	215	Shenandoah (Va.)	4	0000-00-00
2079	t	Justin	Tucker	Justin Tucker	BAL	K				9	1969-12-31
2088	t	Lamont	Bryant	Lamont Bryant	TEN	WR	6-5	225	Morgan State	0	0000-00-00
2096	f	Dorson	Boyce	Dorson Boyce	WAS	RB	6-1	237	Washington	45	0000-00-00
2107	f	Andy	Brewer	Andy Brewer	GB	WR	6-3	215	Northwestern	17	0000-00-00
2116	f	Drew	Smith	Drew Smith	BUF	RB	6-2	225	Albany	40	0000-00-00
2125	t	Mike	Gillislee	Mike Gillislee	BUF	RB				23	1969-12-31
1414	t	Kendall	Hunter	Kendall Hunter	NO	RB				32	1969-12-31
1425	t	Evan	Royster	Evan Royster	WAS	RB				26	1969-12-31
1433	t	David	Ausberry	David Ausberry	OAK	TE	6-4	250	USC	86	0000-00-00
1443	t	Dwayne	Harris	Dwayne Harris	NYG	WR				17	1969-12-31
1450	t	Denarius	Moore	Denarius Moore	CIN	WR				17	1969-12-31
1460	f	Titus	Young	Titus Young	DET	WR	5-11	174	Boise State	16	0000-00-00
1467	t	Kyle	Rudolph	Kyle Rudolph	MIN	TE				82	1969-12-31
1478	f	Mike	Coughlin	Mike Coughlin	TB	QB				0	0000-00-00
1487	f	Joshua	Nesbitt	Joshua Nesbitt	BUF	QB				0	0000-00-00
1497	f	Damien	Berry	Damien Berry	BAL	RB	5-10	223	Miami (Fla.)	28	0000-00-00
1505	f	Mario	Fannin	Mario Fannin	DEN	RB	5-11	224	Auburn	42	0000-00-00
1516	f	Mossis	Madu	Mossis Madu	NYJ	RB	6-0	197	Oklahoma	41	0000-00-00
1523	f	Isaac	Odim	Isaac Odim	SD	RB				0	0000-00-00
1532	f	Seth	Smith	Seth Smith	SF	RB				0	0000-00-00
1541	f	Eddie	Wide	Eddie Wide	STL	RB	5-10	195	Utah	30	0000-00-00
1549	t	Doug	Baldwin	Doug Baldwin	SEA	WR				89	1969-12-31
1558	f	DeAndre	Brown	DeAndre Brown	PHI	WR				0	0000-00-00
1567	f	James	Cleveland	James Cleveland	DAL	WR				0	0000-00-00
1576	f	Greg	Ellingson	Greg Ellingson	TB	WR	6-3	197	Florida International	8	0000-00-00
1582	f	Steve	Goulet	Steve Goulet	OAK	WR				0	0000-00-00
1592	t	Andre	Holmes	Andre Holmes	OAK	WR				18	1969-12-31
1600	f	Derrick	Jones	Derrick Jones	OAK	WR	6-0	195	California (PA)	14	0000-00-00
1609	t	Ricardo	Lockette	Ricardo Lockette	SEA	WR				83	1969-12-31
1616	f	Hakeem	Moore	Hakeem Moore	BAL	WR				0	0000-00-00
1625	f	Tysson	Poots	Tysson Poots	DAL	WR				0	0000-00-00
1635	f	Shaky	Smithson	Shaky Smithson	GB	WR	5-11	202	Utah	16	0000-00-00
1642	t	Raymond	Webber	Raymond Webber	MIA	WR	6-2	237	Arkansas-Pine Bluff	1	0000-00-00
1652	t	Brett	Brackett	Brett Brackett	TEN	TE	6-5	246	Penn State	87	0000-00-00
1660	f	Tommy	Gallarda	Tommy Gallarda	ATL	TE	6-5	262	Boise State	87	0000-00-00
1670	f	Zack	Pianalto	Zack Pianalto	CAR	TE	6-4	250	North Carolina	82	0000-00-00
1677	t	Andre	Smith	Andre Smith	CLE	TE	6-5	270	Virginia Tech	87	0000-00-00
1687	t	Kai	Forbath	Kai Forbath	NO	K				2	1969-12-31
1696	f	Terrelle	Pryor	Terrelle Pryor	CLE	WR	6-4	223	Ohio State	87	0000-00-00
1720	t	Patrick	DiMarco	Patrick DiMarco	ATL	RB	6-1	234	South Carolina	42	0000-00-00
1759	t	Jonas	Gray	Jonas Gray	JAC	RB				35	1969-12-31
1767	f	Les	Brown	Les Brown	MIA	TE	6-4	245	Westminster (UT)	87	0000-00-00
1779	f	Royce	Adams	Royce Adams	NYJ	WR	6-0	190	Purdue	0	0000-00-00
1787	t	Cole	Beasley	Cole Beasley	DAL	WR				11	1969-12-31
1797	t	David	Wilson	David Wilson	NYG	RB	5-9	205	Virginia Tech	22	0000-00-00
1805	t	Kirk	Cousins	Kirk Cousins	WAS	QB				8	1969-12-31
1815	t	Ronnie	Hillman	Ronnie Hillman	DEN	RB				23	1969-12-31
1823	f	James	McCluskey	James McCluskey	OAK	RB	6-2	250	Boston College	42	0000-00-00
1836	t	Michael	Floyd	Michael Floyd	ARI	WR				15	1969-12-31
1843	t	Nathan	Palmer	Nathan Palmer	DEN	WR				13	1969-12-31
1854	f	Todd	Anderson	Todd Anderson	STL	RB	6-1	267	Michigan State	47	0000-00-00
1864	f	Deangelo	Peterson	Deangelo Peterson	DEN	TE	6-4	235	LSU	86	0000-00-00
1871	f	Justin	Hilton	Justin Hilton	TEN	WR	6-2	190	Indiana State	12	0000-00-00
1883	t	Travis	Benjamin	Travis Benjamin	CLE	WR				11	1969-12-31
1892	f	Marquis	Maze	Marquis Maze	PIT	WR	5-8	186	Alabama	3	0000-00-00
1904	f	Jared	Karstetter	Jared Karstetter	DET	WR	6-4	213	Washington State	0	0000-00-00
1911	f	Marc	Tyler	Marc Tyler	GB	RB	5-11	226	USC	26	0000-00-00
1923	t	Jarius	Wright	Jarius Wright	MIN	WR				17	1969-12-31
1930	t	Randy	Bullock	Randy Bullock	NYJ	K				4	1969-12-31
1942	t	Dominique	Jones	Dominique Jones	DEN	TE				86	1969-12-31
1954	t	Collin	Mooney	Collin Mooney	ATL	RB				39	1969-12-31
1965	t	Bradie	Ewing	Bradie Ewing	JAC	RB	5-11	243	Wisconsin	33	0000-00-00
1973	t	Joe	Adams	Joe Adams	HOU	WR	5-11	185	Arkansas	15	0000-00-00
1984	t	Erik	Lorig	Erik Lorig	NO	RB				41	1969-12-31
1991	f	Joe	Martinek	Joe Martinek	NYG	RB	6-0	225	Rutgers	38	0000-00-00
2001	f	Aaron	Pflugrad	Aaron Pflugrad	PHI	WR	5-10	182	Arizona State	0	0000-00-00
2009	f	Matt	Szymanski	Matt Szymanski	KC	K	6-1	196	Southern Methodist	0	0000-00-00
2019	f	Taylor	Embree	Taylor Embree	SD	WR	6-3	210	UCLA	84	0000-00-00
2027	t	Phil	Bates	Phil Bates	DAL	WR				86	1969-12-31
2036	f	Dorian	Graham	Dorian Graham	BAL	WR	5-9	188	Syracuse	13	0000-00-00
2044	t	Alvester	Alexander	Alvester Alexander	PIT	RB	5-11	204	Wyoming	39	0000-00-00
2053	f	Curenski	Gilleylen	Curenski Gilleylen	GB	WR	5-11	213	Nebraska	7	0000-00-00
2060	f	Logan	Brock	Logan Brock	HOU	TE	6-3	244	Texas Christian	0	0000-00-00
2069	t	Jared	Green	Jared Green	OAK	WR	6-1	186	Southern University	0	0000-00-00
2077	f	Kyle	Newhall-Caballero	Kyle Newhall-Caballero	OAK	QB	6-3	220	Brown	10	0000-00-00
2086	f	A.J.	Love	A.J. Love	MIN	WR	6-2	208	South Florida	87	0000-00-00
2094	f	Erik	Folk	Erik Folk	ATL	K	5-11	185	Washington	1	0000-00-00
2105	f	Dre	Muhammad	Dre Muhammad	OAK	WR	5-10	180	Indiana	15	0000-00-00
2114	t	EJ	Manuel	EJ Manuel	BUF	QB				3	1969-12-31
2123	t	Chris	Gragg	Chris Gragg	BUF	TE				89	1969-12-31
2130	t	Jasper	Collins	Jasper Collins	CIN	WR	5-11	188	Mount Union	13	0000-00-00
1493	f	Troy	Weatherhead	Troy Weatherhead	CLE	QB				0	0000-00-00
1502	t	Shaun	Draughn	Shaun Draughn	SF	RB				20	1969-12-31
1512	t	Robert	Hughes	Robert Hughes	ARI	RB				39	1969-12-31
1521	f	Emmanuel	Moody	Emmanuel Moody	BUF	RB				0	0000-00-00
1530	f	Walter	Sanders	Walter Sanders	BAL	RB				0	0000-00-00
1538	t	Phillip	Tanner	Phillip Tanner	SF	RB	5-10	208	Middle Tennessee	48	0000-00-00
1547	t	Kamar	Aiken	Kamar Aiken	BAL	WR				11	1969-12-31
1557	f	Rodney	Bradley	Rodney Bradley	BAL	WR	5-11	190	Hawaii	0	0000-00-00
1570	f	Landon	Cox	Landon Cox	TB	WR	6-3	220	Northern Illinois	89	0000-00-00
1587	f	Joe	Hastings	Joe Hastings	MIA	WR	6-0	185	Washburn	13	0000-00-00
1598	f	Bart	Johnson	Bart Johnson	CIN	WR				0	0000-00-00
1608	f	Phillip	Livas	Phillip Livas	BAL	WR	5-7	179	Louisiana Tech	0	0000-00-00
1619	f	O.J.	Murdock	O.J. Murdock	TEN	WR	5-11	200	Fort Hays State	15	0000-00-00
1628	f	Armand	Robinson	Armand Robinson	PIT	WR				0	0000-00-00
1638	t	Kerry	Taylor	Kerry Taylor	JAC	WR	6-0	200	Arizona State	13	0000-00-00
1650	f	Ed	Barham	Ed Barham	MIN	TE				0	0000-00-00
1663	f	Ben	Guidugli	Ben Guidugli	NYG	RB	6-1	248	Cincinnati	5	0000-00-00
1673	f	Draylen	Ross	Draylen Ross	CHI	TE				0	0000-00-00
1686	f	Wes	Byrum	Wes Byrum	SEA	K				0	0000-00-00
1713	f	Clint	Stitser	Clint Stitser	WAS	K				0	0000-00-00
1766	t	Derek	Moye	Derek Moye	PIT	WR	6-5	210	Penn State	14	0000-00-00
1777	f	Terrance	Ganaway	Terrance Ganaway	STL	RB	6-1	240	Baylor	42	0000-00-00
1792	f	Darrell	Scott	Darrell Scott	DAL	WR	6-2	225	South Florida	35	0000-00-00
1810	f	Darius	Hanks	Darius Hanks	WAS	WR	6-0	187	Alabama	17	0000-00-00
1821	t	Devon	Wylie	Devon Wylie	OAK	WR				13	1969-12-31
1834	t	Ryan	Lindley	Ryan Lindley	NE	QB				7	1969-12-31
1849	t	Robert	Turbin	Robert Turbin	DAL	RB				27	1969-12-31
1861	t	Brian	Quick	Brian Quick	STL	WR				83	1969-12-31
1874	f	Taveon	Rogers	Taveon Rogers	CIN	WR	5-11	200	New Mexico State	83	0000-00-00
1886	f	Jermaine	Saffold	Jermaine Saffold	CLE	WR	6-0	200	Missouri State	85	0000-00-00
1901	t	Ryan	Broyles	Ryan Broyles	DET	WR				84	1969-12-31
1914	f	Darius	Reynolds	Darius Reynolds	GB	WR	6-2	206	Iowa State	0	0000-00-00
1926	t	Case	Keenum	Case Keenum	STL	QB				17	1969-12-31
1941	t	Coby	Fleener	Coby Fleener	IND	TE				80	1969-12-31
1958	f	Chase	Deadder	Chase Deadder	TEN	WR	6-4	224	Sacramento State	82	0000-00-00
1968	f	Cody	Pearcy	Cody Pearcy	ATL	WR	5-11	160	Huntingdon College	0	0000-00-00
1982	t	Michael	Smith	Michael Smith	NYJ	RB	5-9	205	Utah State	36	0000-00-00
1994	t	Julian	Talley	Julian Talley	NYG	WR				19	1969-12-31
2004	f	Nate	Eachus	Nate Eachus	KC	RB	5-10	212	Colgate	45	0000-00-00
2012	f	Kyle	Efaw	Kyle Efaw	OAK	TE	6-4	250	Boise State	81	0000-00-00
2022	f	Jared	Crank	Jared Crank	ARI	RB	6-2	238	Purdue	38	0000-00-00
2030	f	John	Brantley	John Brantley	BAL	QB	6-3	218	Florida	9	0000-00-00
2040	t	Jourdan	Brooks	Jourdan Brooks	CLE	RB	6-0	237	Morgan State	0	0000-00-00
2050	f	Dave	Teggart	Dave Teggart	CHI	K	6-1	203	Connecticut	16	0000-00-00
2063	f	Kenny	Stafford	Kenny Stafford	MIA	WR	6-4	204	Toledo	88	0000-00-00
2073	f	Derek	Session	Derek Session	BUF	WR	6-1	225	Maine	15	0000-00-00
2083	t	Jorvorskie	Lane	Jorvorskie Lane	TB	RB				46	1969-12-31
2098	f	Zach	Brown	Zach Brown	BUF	RB	5-10	220	Pittsburgh	35	0000-00-00
2110	t	Jeff	Demps	Jeff Demps	IND	RB	5-7	191	Florida	35	0000-00-00
2120	f	Kevin	Norrell	Kevin Norrell	BUF	WR	5-10	200	Stony Brook	0	0000-00-00
2127	f	Rupert	Bryan	Rupert Bryan	MIA	RB	6-1	248	Florida International	48	0000-00-00
2137	f	Ben	Bartholomew	Ben Bartholomew	NE	RB	6-2	252	Tennessee	39	0000-00-00
2150	t	Ryan	Spadola	Ryan Spadola	ARI	WR				18	1969-12-31
2162	f	Paul	Freedman	Paul Freedman	DAL	TE	6-6	255	Virginia	88	0000-00-00
2172	t	Russell	Shepard	Russell Shepard	TB	WR				89	1969-12-31
2184	t	C.J.	Anderson	C.J. Anderson	DEN	RB				22	1969-12-31
2194	f	Braden	Wilson	Braden Wilson	KC	RB	6-4	256	Kansas State	40	0000-00-00
2204	t	Greg	Jenkins	Greg Jenkins	OAK	WR				0	1969-12-31
2217	f	Ben	Cotton	Ben Cotton	SD	TE	6-6	256	Nebraska	87	0000-00-00
2230	f	Ryan	Swope	Ryan Swope	ARI	WR	6-0	204	Texas A&M	19	0000-00-00
2247	t	Chris	Harper	Chris Harper	NYG	WR	6-1	228	Kansas State	0	0000-00-00
2260	f	Andrew	Helmick	Andrew Helmick	STL	WR	6-0	192	Lindenwood	84	0000-00-00
2270	t	Gerrard	Sheppard	Gerrard Sheppard	GB	WR	6-2	211	Towson	9	0000-00-00
2276	f	Tyrone	Goard	Tyrone Goard	CIN	WR	6-4	205	Eastern Kentucky	17	0000-00-00
2286	f	Travis	Tannahill	Travis Tannahill	CLE	TE	6-4	255	Kansas State	49	0000-00-00
2294	f	J.D.	Woods	J.D. Woods	PIT	WR	6-0	203	West Virginia	17	0000-00-00
2303	t	Steven	Miller	Steven Miller	DET	RB	5-7	172	Appalachian State	40	0000-00-00
2311	f	Matt	Brown	Matt Brown	GB	QB	6-3	225	Illinois State	1	0000-00-00
2321	t	Myles	White	Myles White	GB	WR				19	1969-12-31
2328	t	Cordarrelle	Patterson	Cordarrelle Patterson	MIN	WR				84	1969-12-31
2338	t	DeAndre	Hopkins	DeAndre Hopkins	HOU	WR				10	1969-12-31
2346	f	Rodrick	Rumble	Rodrick Rumble	IND	WR	6-2	207	Idaho State	11	0000-00-00
2356	t	Tobais	Palmer	Tobais Palmer	BUF	WR				17	1969-12-31
2363	f	Dontel	Watkins	Dontel Watkins	TEN	WR	6-2	194	Murray State	5	0000-00-00
1531	f	Armond	Smith	Armond Smith	CAR	RB	5-9	195	Union College (KY)	36	0000-00-00
1548	f	Isaac	Anderson	Isaac Anderson	WAS	WR				0	0000-00-00
1566	f	Curtis	Clay	Curtis Clay	DAL	WR				0	0000-00-00
1581	f	P.J.	Gore	P.J. Gore	ATL	WR				0	0000-00-00
1591	f	Jamere	Holland	Jamere Holland	CIN	WR				0	0000-00-00
1604	f	Jeremy	LaFrance	Jeremy LaFrance	PHI	WR				0	0000-00-00
1621	f	Aaron	Nichols	Aaron Nichols	ARI	WR				0	0000-00-00
1633	f	Brandon	Smith	Brandon Smith	SEA	WR				0	0000-00-00
1646	f	Jimmy	Young	Jimmy Young	PIT	WR	6-0	204	Texas Christian	14	0000-00-00
1656	f	Jose	Cruz	Jose Cruz	TB	TE				0	0000-00-00
1669	f	Schuylar	Oordt	Schuylar Oordt	STL	TE				0	0000-00-00
1681	f	Ryan	Travis	Ryan Travis	SEA	TE				0	0000-00-00
1691	f	Chris	Koepplin	Chris Koepplin	NE	K	6-3	215	Massachusetts	2	0000-00-00
1711	f	Neil	Rackers	Neil Rackers	WAS	K	6-1	207	Illinois	1	0000-00-00
1758	t	Ryan	Tannehill	Ryan Tannehill	MIA	QB				17	1969-12-31
1772	t	Jeremy	Ebert	Jeremy Ebert	ATL	WR	6-0	195	Northwestern	7	0000-00-00
1784	f	Hayden	Smith	Hayden Smith	NYJ	TE	6-6	255	Metro State	82	0000-00-00
1795	t	Andrew	Szczerba	Andrew Szczerba	ATL	TE	6-6	256	Penn State	85	0000-00-00
1808	f	Lennon	Creer	Lennon Creer	WAS	RB	6-1	216	Louisiana Tech	23	0000-00-00
1819	f	Shane	Bannon	Shane Bannon	KC	RB	6-3	245	Yale	38	0000-00-00
1831	t	Edwin	Baker	Edwin Baker	NO	RB				27	1969-12-31
1846	t	Giorgio	Tavecchio	Giorgio Tavecchio	OAK	K				6	1969-12-31
1857	t	Daryl	Richardson	Daryl Richardson	CLE	RB				35	1969-12-31
1867	t	Tommy	Streeter	Tommy Streeter	MIA	WR				6	1969-12-31
1880	t	Brad	Smelley	Brad Smelley	STL	TE				87	1969-12-31
1896	t	Evan	Rodriguez	Evan Rodriguez	TB	RB				81	1969-12-31
1907	t	Derek	Dimke	Derek Dimke	NO	K	6-0	180	Illinois	2	0000-00-00
1921	t	Greg	Childs	Greg Childs	MIN	WR	6-3	217	Arkansas	85	0000-00-00
1933	t	Vick	Ballard	Vick Ballard	IND	RB				26	1969-12-31
1948	t	Mike	Brown	Mike Brown	CAR	WR				12	1969-12-31
1962	t	Brandon	Barden	Brandon Barden	DAL	TE				80	1969-12-31
1977	f	Kevin	Hardy	Kevin Hardy	NYG	WR	6-0	182	The Citadel	15	0000-00-00
1987	t	Danny	Noble	Danny Noble	JAC	TE	6-5	248	Toledo	88	0000-00-00
1999	f	McKay	Jacobson	McKay Jacobson	PHI	WR	5-11	199	Brigham Young	17	0000-00-00
2008	f	Tim	Biere	Tim Biere	ATL	TE	6-4	260	Kansas	48	0000-00-00
2018	f	Paul	Cox	Paul Cox	PIT	WR	6-5	205	Mississippi Valley State	81	0000-00-00
2031	f	Chester	Stewart	Chester Stewart	BAL	QB	6-3	220	Temple	1	0000-00-00
2041	f	Adonis	Thomas	Adonis Thomas	CLE	RB	5-9	185	Toledo	35	0000-00-00
2051	f	Wallace	Miles	Wallace Miles	DET	WR	6-0	191	North Carolina A&T	15	0000-00-00
2064	f	Aron	White	Aron White	ATL	TE	6-4	239	Georgia	49	0000-00-00
2075	f	Tarren	Lloyd	Tarren Lloyd	NYJ	TE	6-7	258	Utah State	43	0000-00-00
2085	f	Patrick	Doyle	Patrick Doyle	SD	TE	6-4	245	San Diego	0	0000-00-00
2101	f	David	Little	David Little	DAL	WR	6-2	187	Midwestern State	10	0000-00-00
2111	t	Steve	Maneri	Steve Maneri	NYJ	TE				0	1969-12-31
2121	t	Da'Rick	Rogers	Da'Rick Rogers	KC	WR	6-3	215	Tennessee Tech	0	0000-00-00
2134	t	Caleb	Sturgis	Caleb Sturgis	PHI	K				9	1969-12-31
2146	t	Geno	Smith	Geno Smith	NYJ	QB				7	1969-12-31
2156	f	Kendial	Lawrence	Kendial Lawrence	DAL	RB	5-9	193	Missouri	28	0000-00-00
2168	t	Matt	Barkley	Matt Barkley	ARI	QB				9	1969-12-31
2180	t	Emmanuel	Ogbuehi	Emmanuel Ogbuehi	TB	RB	6-2	230	Georgia State	86	0000-00-00
2190	f	Lucas	Reed	Lucas Reed	DEN	TE	6-6	242	New Mexico	97	0000-00-00
2197	t	Demetrius	Harris	Demetrius Harris	KC	TE				84	1969-12-31
2207	f	Nick	Guess	Nick Guess	OAK	TE	6-2	250	Tennessee	60	0000-00-00
2216	f	Luke	Tasker	Luke Tasker	SD	WR	5-11	191	Cornell	2	0000-00-00
2226	t	Charles	Hawkins	Charles Hawkins	NO	WR	5-8	180	Southern University	18	0000-00-00
2233	t	B.J.	Daniels	B.J. Daniels	SEA	WR				11	1969-12-31
2243	t	Jason	Schepler	Jason Schepler	TEN	TE	6-2	274	Northern Illinois	89	0000-00-00
2252	f	Tim	Jenkins	Tim Jenkins	STL	QB	6-1	200	Fort Lewis	3	0000-00-00
2263	f	Brett	Baer	Brett Baer	STL	K	5-10	194	Louisiana-Lafayette	5	0000-00-00
2273	t	Giovani	Bernard	Giovani Bernard	CIN	RB				25	1969-12-31
2283	f	Keenan	Davis	Keenan Davis	MIA	WR	6-3	215	Iowa	3	0000-00-00
2296	f	Demetrius	Fields	Demetrius Fields	STL	WR	6-0	210	Northwestern	17	0000-00-00
2306	t	Cody	Wilson	Cody Wilson	DET	WR	5-9	191	Central Michigan	0	0000-00-00
2316	t	Sederrik	Cunningham	Sederrik Cunningham	GB	WR	5-11	188	Furman	0	0000-00-00
2330	t	Adam	Thielen	Adam Thielen	MIN	WR				19	1969-12-31
2341	f	Michael	Smith	Michael Smith	HOU	WR	6-0	201	Connecticut	86	0000-00-00
2351	t	Matt	Scott	Matt Scott	CIN	QB	6-2	210	Arizona	8	0000-00-00
2365	t	Maikon	Bonani	Maikon Bonani	TEN	K	5-10	183	South Florida	3	0000-00-00
2375	t	Levine	Toilolo	Levine Toilolo	ATL	TE				80	1969-12-31
2385	t	Ryan	Griffin	Ryan Griffin	TB	QB				4	1969-12-31
2399	f	Keith	Carlos	Keith Carlos	NYG	WR	6-0	199	Purdue	2	0000-00-00
2410	t	Jeremy	Kelley	Jeremy Kelley	CHI	WR				0	1969-12-31
2420	f	Marcus	Sales	Marcus Sales	ATL	WR	6-0	195	Syracuse	17	0000-00-00
2436	t	Matt	Hazel	Matt Hazel	MIA	WR				83	1969-12-31
2447	t	Justin	Jones	Justin Jones	NE	TE	6-8	275	East Carolina	88	0000-00-00
1695	f	Jeff	Wolfert	Jeff Wolfert	CLE	K	6-2	185	Missouri	7	0000-00-00
1732	f	Matt	Bosher	Matt Bosher	ATL	K	6-0	208	Miami (Fla.)	5	0000-00-00
1765	t	Rishard	Matthews	Rishard Matthews	MIA	WR				18	1969-12-31
1776	t	Matt	Simms	Matt Simms	BUF	QB				8	1969-12-31
1791	f	Donavon	Kemp	Donavon Kemp	SEA	WR	6-1	195	Texas-El Paso	1	0000-00-00
1804	t	Marvin	McNutt	Marvin McNutt	CAR	WR	6-2	225	Iowa	15	0000-00-00
1814	t	Brock	Osweiler	Brock Osweiler	DEN	QB				17	1969-12-31
1829	t	Rod	Streater	Rod Streater	OAK	WR				80	1969-12-31
1850	t	Jermaine	Kearse	Jermaine Kearse	SEA	WR				15	1969-12-31
1863	t	Cory	Harkey	Cory Harkey	STL	TE				46	1969-12-31
1876	t	Orson	Charles	Orson Charles	NO	TE				81	1969-12-31
1888	t	Chris	Rainey	Chris Rainey	IND	RB	5-9	180	Florida	25	0000-00-00
1902	f	Troy	Burrell	Troy Burrell	DET	WR	5-10	182	Wayne State (Mich.)	0	0000-00-00
1915	f	Marcus	Rivers	Marcus Rivers	BAL	WR	6-3	218	Buffalo	6	0000-00-00
1937	t	T.Y.	Hilton	T.Y. Hilton	IND	WR				13	1969-12-31
1950	f	Chris	Forcier	Chris Forcier	JAC	WR	6-3	200	Furman	0	0000-00-00
1964	t	Dominique	Davis	Dominique Davis	ATL	QB	6-3	210	East Carolina	4	0000-00-00
1983	f	Cody	Johnson	Cody Johnson	TB	RB	5-11	252	Texas	38	0000-00-00
2000	t	Damaris	Johnson	Damaris Johnson	TEN	WR				13	1969-12-31
2013	f	Eddy	Carmona	Eddy Carmona	OAK	K	5-10	203	Harding	3	0000-00-00
2026	f	Marc	Wilson	Marc Wilson	ARI	WR	5-11	186	St. Anselm	0	0000-00-00
2043	t	Matt	Blanchard	Matt Blanchard	GB	QB				6	1969-12-31
2059	f	Mario	Louis	Mario Louis	HOU	WR	6-0	195	Grambling State	84	0000-00-00
2074	f	Nick	Melillo	Nick Melillo	NE	TE	6-2	226	Kentucky	0	0000-00-00
2084	t	Alex	Tanney	Alex Tanney	TEN	QB				11	1969-12-31
2100	f	Ed	Wesley	Ed Wesley	DAL	RB	5-9	200	Texas Christian	35	0000-00-00
2112	t	Demarcus	Dobbs	Demarcus Dobbs	SF	TE				83	1969-12-31
2122	t	Robert	Woods	Robert Woods	BUF	WR				10	1969-12-31
2133	t	Dion	Sims	Dion Sims	MIA	TE				80	1969-12-31
2144	t	Zach	Sudfeld	Zach Sudfeld	NYJ	TE	6-7	260	Nevada	44	0000-00-00
2154	f	Brett	Maher	Brett Maher	DAL	K	6-0	185	Nebraska	2	0000-00-00
2161	t	Gavin	Escobar	Gavin Escobar	DAL	TE				89	1969-12-31
2171	t	Will	Murphy	Will Murphy	PHI	WR	6-2	193	Oregon	84	0000-00-00
2179	t	Nick	Williams	Nick Williams	ATL	WR				15	1969-12-31
2189	t	Lamaar	Thomas	Lamaar Thomas	JAC	WR	6-0	185	New Mexico	81	0000-00-00
2196	t	Rico	Richardson	Rico Richardson	TEN	WR				81	1969-12-31
2206	t	Conner	Vernon	Conner Vernon	CLE	WR	6-0	200	Duke	0	0000-00-00
2215	t	Keenan	Allen	Keenan Allen	SD	WR				13	1969-12-31
2225	f	Robert	Gill	Robert Gill	ARI	WR	5-10	180	Texas State	10	0000-00-00
2232	t	D.C.	Jefferson	D.C. Jefferson	CAR	TE	6-6	265	Rutgers	80	0000-00-00
2241	t	Quinton	Patton	Quinton Patton	SF	WR				11	1969-12-31
2256	f	C.J.	Akins	C.J. Akins	STL	WR	6-1	200	Angelo State	0	0000-00-00
2265	t	Kyle	Juszczyk	Kyle Juszczyk	BAL	RB				44	1969-12-31
2279	t	Tyler	Eifert	Tyler Eifert	CIN	TE				85	1969-12-31
2290	f	Curtis	McNeal	Curtis McNeal	PIT	RB	5-7	190	USC	39	0000-00-00
2300	t	Gabe	Miller	Gabe Miller	WAS	TE	6-3	250	Oregon State	0	0000-00-00
2314	f	Angelo	Pease	Angelo Pease	GB	RB	5-10	211	Kansas State	39	0000-00-00
2325	t	Jerodis	Williams	Jerodis Williams	DEN	RB	5-10	203	Furman	33	0000-00-00
2335	t	Zach	Boren	Zach Boren	TEN	RB				44	1969-12-31
2349	t	Brandon	McManus	Brandon McManus	DEN	K				8	1969-12-31
2360	t	Travis	Harvey	Travis Harvey	ARI	WR				0	1969-12-31
2378	t	Kenjon	Barner	Kenjon Barner	PHI	RB				34	1969-12-31
2394	f	Jheranie	Boyd	Jheranie Boyd	CIN	WR	6-2	190	North Carolina	15	0000-00-00
2411	f	Quentin	Sims	Quentin Sims	NE	WR	6-3	202	Tennessee-Martin	84	0000-00-00
2421	f	Nick	Provo	Nick Provo	BUF	TE	6-4	249	Syracuse	87	0000-00-00
2437	t	Jarvis	Landry	Jarvis Landry	MIA	WR				14	1969-12-31
2449	t	Tajh	Boyd	Tajh Boyd	PIT	QB				0	1969-12-31
2459	t	Chris	Boyd	Chris Boyd	DAL	WR	6-4	210	Vanderbilt	0	0000-00-00
2471	t	Jordan	Matthews	Jordan Matthews	PHI	WR				81	1969-12-31
2482	t	Rashad	Lawrence	Rashad Lawrence	JAC	WR				18	1969-12-31
2492	t	Cody	Latimer	Cody Latimer	DEN	WR				14	1969-12-31
2500	t	Fred	Williams	Fred Williams	KC	WR				83	1969-12-31
2510	t	Scott	Simonson	Scott Simonson	CAR	TE				80	1969-12-31
2524	t	Kelsey	Pope	Kelsey Pope	ARI	WR	6-0	202	Samford	0	0000-00-00
2536	t	Kevin	Norwood	Kevin Norwood	CAR	WR				81	1969-12-31
2546	t	Jamaine	Sherman	Jamaine Sherman	STL	WR	6-2	188	East Texas Baptist	0	0000-00-00
2559	t	Colin	Lockett	Colin Lockett	WAS	WR				15	1969-12-31
2570	t	Kenny	Shaw	Kenny Shaw	JAC	WR	5-11	180	Florida State	4	0000-00-00
2586	t	Andrew	Peacock	Andrew Peacock	DET	WR				11	1969-12-31
2595	t	Jared	Abbrederis	Jared Abbrederis	GB	WR				84	1969-12-31
2605	t	Donte	Foster	Donte Foster	MIN	WR				2	1969-12-31
2614	t	Chris	Coyle	Chris Coyle	HOU	TE	6-4	243	Arizona State	0	0000-00-00
2623	t	Eric	Thomas	Eric Thomas	IND	WR	6-1	225	Troy	81	0000-00-00
2632	t	Damian	Copeland	Damian Copeland	JAC	WR				0	1969-12-31
2639	t	Zach	Mettenberger	Zach Mettenberger	TEN	QB				7	1969-12-31
2648	t	David	Wright	David Wright	TEN	TE	6-4	251	Westminster (PA)	0	0000-00-00
1827	t	Juron	Criner	Juron Criner	NYG	WR				87	1969-12-31
1841	t	A.J.	Jenkins	A.J. Jenkins	DAL	WR				16	1969-12-31
1852	f	Carson	Wiggs	Carson Wiggs	SEA	K	6-1	222	Purdue	5	0000-00-00
1859	t	Chris	Givens	Chris Givens	BAL	WR				19	1969-12-31
1869	t	Daniel	Herron	Dan Herron	IND	RB				36	1969-12-31
1878	f	William	Green	William Green	CLE	RB	6-3	250	Florida	49	0000-00-00
1890	t	Toney	Clemons	Toney Clemons	CAR	WR	6-2	205	Colorado	18	0000-00-00
1899	f	James	Bryant	James Bryant	DET	RB	6-3	257	Louisville	40	0000-00-00
1909	f	Nick	Hill	Nick Hill	GB	QB	6-3	210	Southern Illinois	0	0000-00-00
1917	f	Eric	Lair	Eric Lair	GB	TE	6-2	238	Minnesota	45	0000-00-00
1928	t	DeVier	Posey	DeVier Posey	NYJ	WR				89	1969-12-31
1939	t	Griff	Whalen	Griff Whalen	IND	WR				17	1969-12-31
1952	t	Matt	Veldman	Matt Veldman	DET	TE	6-7	255	North Dakota State	85	0000-00-00
1960	f	D.J.	Woods	D.J. Woods	TEN	WR	5-11	180	Cincinnati	12	0000-00-00
1970	f	LaMark	Brown	LaMark Brown	MIN	WR	6-4	221	Minnesota State-Mankato	87	0000-00-00
1980	f	De'Anthony	Curtis	De'Anthony Curtis	TB	RB	5-9	211	Arkansas	39	0000-00-00
1989	f	Brian	Linthicum	Brian Linthicum	NYJ	TE	6-5	245	Michigan State	0	0000-00-00
1997	t	Jeremy	Stewart	Jeremy Stewart	DEN	RB				27	1969-12-31
2006	t	Josh	Bellamy	Josh Bellamy	CHI	WR				11	1969-12-31
2016	f	Mohamed	Marah	Mohamed Marah	SD	RB	6-0	240	San Jose State	35	0000-00-00
2024	t	LaRon	Byrd	LaRon Byrd	MIA	WR				81	1969-12-31
2034	t	Bobby	Rainey	Bobby Rainey	TB	RB				43	1969-12-31
2047	t	Brittan	Golden	Brittan Golden	ARI	WR				10	1969-12-31
2057	t	Jerrell	Jackson	Jerrell Jackson	KC	WR	6-0	192	Missouri	89	0000-00-00
2067	f	Lyndon	Rowells	Lyndon Rowells	CAR	RB	5-9	215	Humboldt State	38	0000-00-00
2080	t	Brandon	Bostick	Brandon Bostick	MIN	TE				81	1969-12-31
2092	t	Josh	Gordon	Josh Gordon	CLE	WR	6-3	225	Utah	12	0000-00-00
2103	t	Lance	Lewis	Lance Lewis	NO	WR				84	1969-12-31
2117	t	Kendall	Gaskins	Kendall Gaskins	SF	RB				72	1969-12-31
2128	t	Ina	Liaina	Ina Liaina	GB	RB	6-0	250	San Jose State	41	0000-00-00
2138	t	Josh	Boyce	Josh Boyce	NE	WR				82	1969-12-31
2151	f	K.J.	Stroud	K.J. Stroud	NYJ	WR	6-3	205	Bethune-Cookman	83	0000-00-00
2163	f	Spencer	Benton	Spencer Benton	DAL	K	6-1	187	Clemson	13	0000-00-00
2173	t	Zach	Ertz	Zach Ertz	PHI	TE				86	1969-12-31
2185	f	Montee	Ball	Montee Ball	DEN	RB	5-10	215	Wisconsin	28	0000-00-00
2199	t	Matt	McGloin	Matt McGloin	OAK	QB				14	1969-12-31
2212	t	Brad	Sorensen	Brad Sorensen	SD	QB				4	1969-12-31
2222	t	Stepfan	Taylor	Stepfan Taylor	ARI	RB				30	1969-12-31
2235	f	D.J.	Harper	D.J. Harper	SF	RB	5-9	211	Boise State	36	0000-00-00
2245	t	Spencer	Ware	Spencer Ware	KC	RB				30	1969-12-31
2254	t	Zac	Stacy	Zac Stacy	NYJ	RB				30	1969-12-31
2266	t	Marlon	Brown	Marlon Brown	BAL	WR				14	1969-12-31
2281	f	Perez	Ashford	Perez Ashford	SEA	WR	5-11	182	Northern Illinois	6	0000-00-00
2297	t	Josh	Lenz	Josh Lenz	CLE	WR				7	1969-12-31
2307	t	Joseph	Fauria	Joseph Fauria	ARI	TE				81	1969-12-31
2317	t	Kevin	Dorsey	Kevin Dorsey	NE	WR	6-1	207	Maryland	0	0000-00-00
2331	t	Colin	Anderson	Colin Anderson	NYJ	TE	6-4	240	Furman	89	0000-00-00
2342	t	Ryan	Griffin	Ryan Griffin	HOU	TE				84	1969-12-31
2352	f	De'Leon	Eskridge	De'Leon Eskridge	JAC	RB	5-11	215	San Jose State	39	0000-00-00
2366	f	Seth	Doege	Seth Doege	ATL	QB	6-1	197	Texas Tech	9	0000-00-00
2376	f	Jeremy	Shelley	Jeremy Shelley	ATL	K	5-10	165	Alabama	1	0000-00-00
2386	t	Khiry	Robinson	Khiry Robinson	NO	RB				29	1969-12-31
2400	f	Chase	Clement	Chase Clement	NYG	TE	6-5	265	LSU	47	0000-00-00
2412	f	Robby	Toma	Robby Toma	ARI	WR	5-9	185	Notre Dame	1	0000-00-00
2423	f	Justin	Wilson	Justin Wilson	GB	WR	6-1	175	Delaware State	7	0000-00-00
2438	t	Rantavious	Wooten	Rantavious Wooten	MIA	WR	5-9	185	Georgia	0	0000-00-00
2448	t	Asa	Watson	Asa Watson	NE	TE	6-3	240	North Carolina State	86	0000-00-00
2458	t	J.C.	Copeland	J.C. Copeland	DAL	RB	5-11	260	null	0	0000-00-00
2472	t	Quron	Pratt	Quron Pratt	PHI	WR				10	1969-12-31
2483	t	Ted	Bolser	Ted Bolser	ARI	TE	6-5	249	Indiana	0	0000-00-00
2493	t	Greg	Wilson	Greg Wilson	DEN	WR	6-0	185	Fordham	15	0000-00-00
2506	t	Mike	Davis	Mike Davis	OAK	WR	6-0	189	Texas	19	0000-00-00
2517	t	Javontee	Herndon	Javontee Herndon	SD	WR				8	1969-12-31
2527	t	Troy	Niklas	Troy Niklas	ARI	TE				87	1969-12-31
2535	t	Kiero	Small	Kiero Small	BAL	RB	5-9	242	Arkansas	35	0000-00-00
2545	t	Jordan	Harris	Jordan Harris	STL	WR	6-2	215	Bryant University	10	0000-00-00
2558	t	Ryan	Hewitt	Ryan Hewitt	CIN	TE				89	1969-12-31
2569	t	Jonathan	Krause	Jonathan Krause	PHI	WR				16	1969-12-31
2579	t	Jordan	Lynch	Jordan Lynch	CHI	RB	6-0	220	Northern Illinois	36	0000-00-00
2589	t	Jordan	Thompson	Jordan Thompson	DET	TE				82	1969-12-31
2599	t	Justin	Perillo	Justin Perillo	GB	TE				80	1969-12-31
2608	t	Tom	Savage	Tom Savage	HOU	QB				3	1969-12-31
2617	t	Chris	Boswell	Chris Boswell	PIT	K				6	1969-12-31
2627	t	Blake	Bortles	Blake Bortles	JAC	QB				5	1969-12-31
2641	t	Waymon	James	Waymon James	TEN	RB	5-8	203	Texas Christian	45	0000-00-00
1833	t	Ladarius	Green	Ladarius Green	SD	TE				89	1969-12-31
1847	t	Russell	Wilson	Russell Wilson	SEA	QB				3	1969-12-31
1862	t	Jamie	Childers	Jamie Childers	CAR	TE				0	1969-12-31
1875	t	Mohamed	Sanu	Mohamed Sanu	CIN	WR				12	1969-12-31
1887	t	Will	Johnson	Will Johnson	PIT	RB				46	1969-12-31
1903	t	Patrick	Edwards	Patrick Edwards	DET	WR	5-9	175	Houston	83	0000-00-00
1922	t	Kamar	Jorden	Kamar Jorden	MIN	WR	6-3	200	Bowling Green State	12	0000-00-00
1936	t	LaVon	Brazill	LaVon Brazill	IND	WR	5-11	194	Ohio U.	15	0000-00-00
1949	t	Kevin	Elliott	Kevin Elliott	BUF	WR	6-3	205	Florida A&M	18	0000-00-00
1963	t	Taylor	Thompson	Taylor Thompson	TEN	TE	6-6	268	SMU	84	0000-00-00
1978	t	Nick	Toon	Nick Toon	NO	WR				88	1969-12-31
1995	f	Jacory	Harris	Jacory Harris	PHI	QB	6-4	200	Miami (Fla.)	0	0000-00-00
2014	f	Jarrett	Lee	Jarrett Lee	SD	QB	6-1	210	LSU	16	0000-00-00
2032	f	Jamison	Berryhill	Jamison Berryhill	BAL	RB	5-11	239	Texas	39	0000-00-00
2042	t	Danny	Hrapmann	Danny Hrapmann	MIA	K	5-9	164	Southern Mississippi	32	0000-00-00
2052	t	Jarrett	Boykin	Jarrett Boykin	CAR	WR				15	1969-12-31
2065	f	Princeton	McCarty	Princeton McCarty	CAR	RB	5-7	197	Idaho	0	0000-00-00
2076	f	Aaron	Weaver	Aaron Weaver	KC	WR	6-2	220	Syracuse	13	0000-00-00
2093	f	Charles	Gilbert	Charles Gilbert	JAC	WR	5-11	167	Concordia (Minn.)	13	0000-00-00
2104	f	Anthony	Miller	Anthony Miller	ATL	TE	6-4	260	California	86	0000-00-00
2118	t	Marquise	Goodwin	Marquise Goodwin	BUF	WR				88	1969-12-31
2135	t	Quentin	Hines	Quentin Hines	NE	RB	5-11	190	Akron	0	0000-00-00
2145	f	David	Ruffer	David Ruffer	NE	K	6-1	192	Notre Dame	5	0000-00-00
2155	f	Dalton	Williams	Dalton Williams	DAL	QB	6-3	222	Akron	13	0000-00-00
2169	t	Matthew	Tucker	Matthew Tucker	PHI	RB	6-1	227	Texas Christian	39	0000-00-00
2187	t	Tavarres	King	Tavarres King	TB	WR				15	1969-12-31
2200	t	Tyler	Wilson	Tyler Wilson	TEN	QB	6-2	215	Arkansas	8	0000-00-00
2213	t	Michael	Hill	Michael Hill	DAL	RB				0	1969-12-31
2223	t	Jaron	Brown	Jaron Brown	ARI	WR				13	1969-12-31
2236	t	Marcus	Lattimore	Marcus Lattimore	SF	RB	5-11	221	South Carolina	38	0000-00-00
2246	f	Matt	Austin	Matt Austin	SEA	WR	6-2	202	Utah State	2	0000-00-00
2255	f	Eric	Stevens	Eric Stevens	STL	RB	5-11	240	California	47	0000-00-00
2267	f	Rashaad	Carter	Rashaad Carter	BAL	WR	6-3	205	Tusculum	18	0000-00-00
2280	t	Miguel	Maysonet	Miguel Maysonet	PIT	RB	5-10	210	Stony Brook	30	0000-00-00
2291	t	Justin	Brown	Justin Brown	BUF	WR	6-3	209	Oklahoma	0	0000-00-00
2308	t	Michael	Williams	Michael Williams	NE	TE				85	1969-12-31
2318	t	Alex	Gillett	Alex Gillett	GB	WR	6-1	215	Eastern Michigan	1	0000-00-00
2332	f	Ray	Graham	Ray Graham	HOU	RB	5-9	193	Pittsburgh	37	0000-00-00
2343	f	Dan	Moore	Dan Moore	IND	RB	5-11	240	Montana	48	0000-00-00
2353	t	Denard	Robinson	Denard Robinson	JAC	RB				16	1969-12-31
2367	t	Sean	Renfree	Sean Renfree	ATL	QB				12	1969-12-31
2377	f	Colby	Cameron	Colby Cameron	CAR	QB	6-2	210	Louisiana Tech	16	0000-00-00
2387	f	Brent	Leonard	Brent Leonard	NO	WR	6-2	186	Louisiana-Monroe	86	0000-00-00
2401	t	Toben	Opurum	Toben Opurum	NO	RB				44	1969-12-31
2418	t	Adam	Schiltz	Adam Schiltz	KC	TE				47	1969-12-31
2431	t	Caleb	Holley	Caleb Holley	BUF	WR	6-2	200	East Central	86	0000-00-00
2441	t	Jimmy	Garoppolo	Jimmy Garoppolo	NE	QB				10	1969-12-31
2455	t	Jace	Amaro	Jace Amaro	NYJ	TE				88	1969-12-31
2473	t	Blake	Annen	Blake Annen	CHI	TE				84	1969-12-31
2484	t	Zach	Hocker	Zach Hocker	STL	K				2	1969-12-31
2494	t	Aaron	Murray	Aaron Murray	KC	QB				7	1969-12-31
2507	t	Noel	Grigsby	Noel Grigsby	OAK	WR	5-10	185	San Jose State	0	0000-00-00
2519	t	Mike	Flacco	Mike Flacco	SD	TE	6-5	251	New Haven	46	0000-00-00
2529	f	Kory	Faulkner	Kory Faulkner	SF	QB	6-4	227	Southern Illinois	0	0000-00-00
2542	t	Trey	Watts	Trey Watts	STL	RB				42	1969-12-31
2553	t	Crockett	Gillmore	Crockett Gillmore	BAL	TE				80	1969-12-31
2563	t	Connor	Shaw	Connor Shaw	CLE	QB				9	1969-12-31
2577	t	Eric	Waters	Eric Waters	PIT	TE	6-5	245	Missouri	80	0000-00-00
2588	t	Jacob	Maxwell	Jacob Maxwell	DET	TE				47	1969-12-31
2596	t	Davante	Adams	Davante Adams	GB	WR				17	1969-12-31
2607	t	A.C.	Leonard	A.C. Leonard	MIN	TE	6-2	250	Tennessee State	0	0000-00-00
2616	t	C.J.	Fiedorowicz	C.J. Fiedorowicz	HOU	TE				87	1969-12-31
2626	f	Cody	Parkey	Cody Parkey	PHI	K				1	1969-12-31
2640	t	Antonio	Andrews	Antonio Andrews	TEN	RB				26	1969-12-31
2649	t	Travis	Coons	Travis Coons	CLE	K				6	1969-12-31
2659	t	Jacob	Pedersen	Jacob Pedersen	ATL	TE	6-5	240	Wisconsin	81	0000-00-00
2668	t	Brandon	Coleman	Brandon Coleman	NO	WR				16	1969-12-31
2678	t	Robert	Herron	Robert Herron	TB	WR				10	1969-12-31
2692	t	Michael	Palardy	Michael Palardy	OAK	K	5-11	191	Tennessee	0	0000-00-00
2701	t	Greg	Moore	Greg Moore	IND	WR	6-4	197	Lane	0	0000-00-00
2711	t	Kevin	Goessling	Kevin Goessling	OAK	K	6-0	210	--	6	0000-00-00
2726	t	Braylon	Bell	Braylon Bell	WAS	WR	6-6	205	--	17	0000-00-00
2736	t	Tony	Lippett	Tony Lippett	MIA	WR	6-2	200	Michigan State	14	0000-00-00
2747	t	Matt	Jones	Matt Jones	WAS	RB				31	1969-12-31
2760	t	David	Johnson	David Johnson	ARI	RB				31	1969-12-31
1860	f	Nick	Johnson	Nick Johnson	STL	WR	5-11	187	Henderson State	14	0000-00-00
1870	f	Rodney	Stewart	Rodney Stewart	CIN	RB	5-6	175	Colorado	0	0000-00-00
1879	f	Trent	Richardson	Trent Richardson	OAK	RB	5-9	230	Alabama	33	0000-00-00
1891	f	Connor	Dixon	Connor Dixon	PIT	WR	6-5	212	Duquesne	0	0000-00-00
1900	f	Stephfon	Green	Stephfon Green	DET	RB	5-10	193	Penn State	43	0000-00-00
1910	f	Nicholas	Cooper	Nicholas Cooper	GB	RB	5-10	249	Winston-Salem State	40	0000-00-00
1918	t	Derrick	Coleman	Derrick Coleman	SEA	RB				40	1969-12-31
1929	t	Phillip	Supernaw	Phillip Supernaw	TEN	TE				89	1969-12-31
1940	t	Dwayne	Allen	Dwayne Allen	IND	TE				83	1969-12-31
1953	t	Nick	Stephens	Nick Stephens	BAL	QB	6-3	221	Tarleton State	0	0000-00-00
1961	t	Kendall	Wright	Kendall Wright	TEN	WR				13	1969-12-31
1971	t	Adam	Nissley	Adam Nissley	ATL	TE	6-6	267	Central Florida	86	0000-00-00
1981	t	Doug	Martin	Doug Martin	TB	RB				22	1969-12-31
1990	t	Jamize	Olawale	Jamize Olawale	OAK	RB				49	1969-12-31
1998	f	Elvis	Akpla	Elvis Akpla	PHI	WR	6-1	190	Montana State	88	0000-00-00
2007	f	Brandon	Kinnie	Brandon Kinnie	KC	WR	6-3	220	Nebraska	17	0000-00-00
2017	f	Jason	Barnes	Jason Barnes	SD	WR	6-3	210	South Carolina	3	0000-00-00
2025	f	Tre	Gray	Tre Gray	ARI	WR	5-10	175	Richmond	16	0000-00-00
2035	f	Devin	Goda	Devin Goda	BAL	WR	6-2	218	Slippery Rock	85	0000-00-00
2048	t	Chris	Summers	Chris Summers	BUF	WR	6-5	215	Liberty	87	0000-00-00
2058	t	Dwight	Jones	Dwight Jones	NYJ	WR	6-3	230	North Carolina	0	0000-00-00
2068	t	Brenton	Bersin	Brenton Bersin	CAR	WR				11	1969-12-31
2081	f	Hubert	Anyiam	Hubert Anyiam	CAR	WR	6-0	205	Oklahoma State	16	0000-00-00
2099	f	Joe	Collins	Joe Collins	NYJ	WR	6-3	200	Weber State	4	0000-00-00
2113	f	Beau	Brinkley	Beau Brinkley	TEN	TE	6-4	237	Missouri	48	0000-00-00
2129	t	Chad	Bumphis	Chad Bumphis	JAC	WR	5-10	202	Mississippi State	15	0000-00-00
2139	t	Aaron	Dobson	Aaron Dobson	NE	WR				17	1969-12-31
2152	t	Chris	Pantale	Chris Pantale	CHI	TE				48	1969-12-31
2164	t	Ryan	Nassib	Ryan Nassib	NYG	QB				12	1969-12-31
2174	f	Will	Shaw	Will Shaw	PHI	TE	6-3	245	Youngstown State	47	0000-00-00
2186	f	Kemonte'	Bateman	Kemonte' Bateman	DEN	WR	6-1	185	New Mexico State	13	0000-00-00
2198	t	Travis	Kelce	Travis Kelce	KC	TE				87	1969-12-31
2211	f	Mike	Hermann	Mike Hermann	SD	QB	6-5	250	RPI	7	0000-00-00
2221	t	Andre	Ellington	Andre Ellington	ARI	RB				38	1969-12-31
2234	t	MarQueis	Gray	MarQueis Gray	BUF	TE				48	1969-12-31
2244	t	Christine	Michael	Christine Michael	SEA	RB				30	1969-12-31
2253	t	Benjamin	Cunningham	Benjamin Cunningham	STL	RB				36	1969-12-31
2264	f	Dayne	Crist	Dayne Crist	BAL	QB	6-4	235	Kansas	0	0000-00-00
2278	f	Roy	Roundtree	Roy Roundtree	CIN	WR	6-1	180	Michigan	86	0000-00-00
2289	t	Le'Veon	Bell	Le'Veon Bell	PIT	RB				26	1969-12-31
2299	t	Marquess	Wilson	Marquess Wilson	CHI	WR				10	1969-12-31
2313	t	Eddie	Lacy	Eddie Lacy	GB	RB				27	1969-12-31
2324	t	Bradley	Randle	Bradley Randle	MIN	RB	5-7	193	Nevada-Las Vegas	0	0000-00-00
2334	t	Cierre	Wood	Cierre Wood	BUF	RB				0	1969-12-31
2348	t	Justice	Cunningham	Justice Cunningham	STL	TE				48	1969-12-31
2359	f	Kyler	Reed	Kyler Reed	JAC	TE	6-3	225	Nebraska	49	0000-00-00
2369	t	Ronnie	Wingo Jr.	Ronnie Wingo Jr.	ATL	RB	6-3	231	Arkansas	0	0000-00-00
2383	t	Brandon	Williams	Brandon Williams	MIA	TE				86	1969-12-31
2393	t	Mike	James	Mike James	TB	RB				25	1969-12-31
2403	t	Greg	Herd	Greg Herd	CHI	WR	6-3	202	Eastern Washington	0	0000-00-00
2417	f	John	Rabe	John Rabe	PIT	TE	6-4	258	Minnesota	0	0000-00-00
2430	f	Maurice	Williams	Maurice Williams	IND	WR	6-1	190	Pittsburgh	11	0000-00-00
2440	t	Arthur	Lynch	Arthur Lynch	NYJ	TE				49	1969-12-31
2454	t	Jalen	Saunders	Jalen Saunders	NO	WR				11	1969-12-31
2466	t	Xavier	Grimble	Xavier Grimble	SF	TE				83	1969-12-31
2476	t	Silas	Redd	Silas Redd	WAS	RB				32	1969-12-31
2490	t	Bennie	Fowler	Bennie Fowler	DEN	WR				16	1969-12-31
2508	t	Seth	Roberts	Seth Roberts	OAK	WR				10	1969-12-31
2518	t	Tevin	Reese	Tevin Reese	CIN	WR				16	1969-12-31
2528	t	Chandler	Catanzaro	Chandler Catanzaro	ARI	K				7	1969-12-31
2541	t	Tre	Mason	Tre Mason	STL	RB				27	1969-12-31
2552	t	Jace	Davis	Jace Davis	HOU	WR				2	1969-12-31
2562	t	Johnny	Manziel	Johnny Manziel	CLE	QB				2	1969-12-31
2575	t	Martavis	Bryant	Martavis Bryant	PIT	WR				10	1969-12-31
2584	t	Carlin	Isles	Carlin Isles	DET	RB	5-8	158	Ashland	0	0000-00-00
2592	t	Rajion	Neal	Rajion Neal	GB	RB				34	1969-12-31
2601	t	Teddy	Bridgewater	Teddy Bridgewater	MIN	QB				5	1969-12-31
2610	t	Jay	Prosch	Jay Prosch	HOU	RB	6-1	256	Auburn	45	0000-00-00
2619	t	Zurlon	Tipton	Zurlon Tipton	IND	RB				37	1969-12-31
2628	t	Stephen	Morris	Stephen Morris	IND	QB				4	1969-12-31
2635	t	Allen	Robinson	Allen Robinson	JAC	WR				15	1969-12-31
2644	t	Jaz	Reynolds	Jaz Reynolds	TEN	WR	6-2	201	Oklahoma	16	0000-00-00
2653	t	Maurice	Hagens	Maurice Hagens	ATL	RB	5-11	250	Miami (Fla.)	45	0000-00-00
2663	t	Kelvin	Benjamin	Kelvin Benjamin	CAR	WR				13	1969-12-31
2670	t	Je'Ron	Hamm	Je'Ron Hamm	SF	TE				87	1969-12-31
2082	f	Nelson	Rosario	Nelson Rosario	CAR	TE	6-5	225	UCLA	80	0000-00-00
2089	f	Armahd	Lewis	Armahd Lewis	TB	WR	5-9	171	Central Washington	16	0000-00-00
2102	f	Tiger	Jones	Tiger Jones	PHI	WR	5-11	191	Louisville	86	0000-00-00
2109	f	Marques	Clark	Marques Clark	NO	WR	6-1	175	Henderson State	18	0000-00-00
2119	t	Brandon	Kaufman	Brandon Kaufman	BUF	WR	6-5	215	Eastern Washington	0	0000-00-00
2126	t	Cameron	Marshall	Cameron Marshall	MIA	RB	5-11	215	Arizona State	0	0000-00-00
2136	f	Akeem	Shavers	Akeem Shavers	NE	RB	5-11	203	Purdue	0	0000-00-00
2143	f	Brandon	Ford	Brandon Ford	NE	TE	6-3	240	Clemson	49	0000-00-00
2153	f	Mike	Shanahan	Mike Shanahan	NYJ	TE	6-4	241	Pittsburgh	48	0000-00-00
2160	t	Terrance	Williams	Terrance Williams	DAL	WR				83	1969-12-31
2170	t	Ifeanyi	Momah	Ifeanyi Momah	ARI	TE				80	1969-12-31
2178	f	Chip	Reeves	Chip Reeves	WAS	WR	5-10	176	Troy	17	0000-00-00
2188	f	Quincy	McDuffie	Quincy McDuffie	DEN	WR	5-10	178	Central Florida	16	0000-00-00
2195	t	Frankie	Hammond Jr.	Frankie Hammond	KC	WR				85	1969-12-31
2205	t	Sam	McGuffie	Sam McGuffie	NE	WR	5-10	200	Rice	0	0000-00-00
2214	t	Foswhitt	Whittaker	Fozzy Whittaker	CAR	RB				43	1969-12-31
2224	t	Dan	Buckner	Dan Buckner	ARI	WR	6-4	215	Arizona	81	0000-00-00
2231	t	Kyle	Auffray	Kyle Auffray	CLE	TE	6-6	254	New Hampshire	89	0000-00-00
2240	t	Kyle	Nelson	Kyle Nelson	SF	TE	6-2	240	New Mexico State	86	0000-00-00
2248	t	Justin	Veltung	Justin Veltung	STL	WR	5-11	182	Idaho	19	0000-00-00
2257	t	Tavon	Austin	Tavon Austin	STL	WR				11	1969-12-31
2262	f	Philip	Lutzenkirchen	Philip Lutzenkirchen	STL	TE	6-3	258	Auburn	43	0000-00-00
2272	f	Alex	Silvestro	Alex Silvestro	BAL	TE	6-3	260	Rutgers	48	0000-00-00
2282	t	Dominique	Croom	Dominique Croom	ATL	WR	6-2	190	Central Arkansas	0	0000-00-00
2288	t	Landry	Jones	Landry Jones	PIT	QB				3	1969-12-31
2298	f	Marcus	Rucker	Marcus Rucker	NYJ	WR	6-4	185	Memphis	18	0000-00-00
2305	t	Theo	Riddick	Theo Riddick	DET	RB				25	1969-12-31
2315	f	Jonathan	Amosa	Jonathan Amosa	GB	RB	5-11	247	Washington	48	0000-00-00
2323	f	James	Vandenberg	James Vandenberg	MIN	QB	6-3	220	Iowa	6	0000-00-00
2333	f	Dennis	Johnson	Dennis Johnson	HOU	RB	5-7	193	Arkansas	28	0000-00-00
2340	t	Uzoma	Nwachukwu	Uzoma Nwachukwu	HOU	WR				17	1969-12-31
2350	t	Jordan	Rodgers	Jordan Rodgers	TB	QB	6-1	212	Vanderbilt	0	0000-00-00
2358	t	Ryan	Otten	Ryan Otten	MIN	TE	6-6	241	San Jose State	89	0000-00-00
2368	f	Donald	Russell	Donald Russell	ATL	RB	5-11	212	Georgia State	33	0000-00-00
2374	f	Martel	Moore	Martel Moore	ATL	WR	6-0	183	Northern Illinois	14	0000-00-00
2384	f	Morgan	Lineberry	Morgan Lineberry	CAR	K	6-1	200	Abilene Christian	5	0000-00-00
2392	t	Mike	Glennon	Mike Glennon	TB	QB				8	1969-12-31
2402	t	Arceto	Clark	Arceto Clark	SEA	WR	5-10	180	Mississippi State	14	0000-00-00
2409	t	George	Winn	George Winn	DET	RB				38	1969-12-31
2419	f	Adam	Yates	Adam Yates	JAC	K	6-1	195	South Carolina	6	0000-00-00
2429	t	Jamaine	Cook	Jamaine Cook	CLE	RB	5-9	215	Youngstown State	0	0000-00-00
2439	t	Harold	Hoskins	Harold Hoskins	MIA	TE	6-2	248	Marshall	89	0000-00-00
2446	t	Derrick	Johnson	Derrick Johnson	NYG	WR				18	1969-12-31
2456	t	Dustin	Vaughan	Dustin Vaughan	DAL	QB				10	1969-12-31
2464	t	Andre	Williams	Andre Williams	NYG	RB				44	1969-12-31
2474	t	Trey	Burton	Trey Burton	PHI	TE				47	1969-12-31
2481	t	Kofi	Hughes	Kofi Hughes	HOU	WR	6-2	215	Indiana	0	0000-00-00
2491	t	Greg	Hardin	Greg Hardin	DEN	WR	5-10	171	North Dakota	87	0000-00-00
2499	t	Darryl	Surgent	Darryl Surgent	KC	WR	6-0	195	Louisiana-Lafayette	14	0000-00-00
2509	t	Jake	Murphy	Jake Murphy	DEN	TE				83	1969-12-31
2516	t	Micah	Hatfield	Micah Hatfield	SD	WR	6-1	182	Oregon State	7	0000-00-00
2526	t	Corey	Washington	Corey Washington	NYG	WR				88	1969-12-31
2534	t	Keith	Price	Keith Price	SEA	QB	6-1	204	Washington	0	0000-00-00
2544	t	Austin	Franklin	Austin Franklin	STL	WR	5-11	189	New Mexico State	15	0000-00-00
2551	t	Michael	Campanaro	Michael Campanaro	BAL	WR				15	1969-12-31
2561	t	James	Wright	James Wright	CIN	WR	6-1	201	LSU	0	0000-00-00
2568	t	Chandler	Jones	Chandler Jones	TB	WR	5-9	185	San Jose State	85	0000-00-00
2578	t	David	Fales	David Fales	CHI	QB				12	1969-12-31
2583	t	Chad	Abram	Chad Abram	DET	RB	6-0	229	Florida State	44	0000-00-00
2591	t	Chase	Rettig	Chase Rettig	SD	QB				16	1969-12-31
2603	t	Dominique	Williams	Dominique Williams	MIN	RB				43	1969-12-31
2612	t	Travis	Labhart	Travis Labhart	HOU	WR				19	1969-12-31
2621	t	Ryan	Lankford	Ryan Lankford	IND	WR				85	1969-12-31
2630	t	Terrance	Cobb	Terrance Cobb	OAK	RB	5-11	220	Cumberland College	99	0000-00-00
2637	t	Reggie	Jordan	Reggie Jordan	JAC	TE	6-3	240	Missouri Western State	0	0000-00-00
2646	t	Derel	Walker	Derel Walker	TEN	WR	6-1	188	Texas A&M	19	0000-00-00
2655	t	Julian	Jones	Julian Jones	ATL	WR	6-0	202	Arkansas State	16	0000-00-00
2665	t	Marcus	Lucas	Marcus Lucas	CAR	WR				83	1969-12-31
2672	t	Seantavius	Jones	Seantavius Jones	NO	WR				15	1969-12-31
2681	t	Cam	Brate	Cameron Brate	TB	TE				84	1969-12-31
2689	t	Andrew	Furney	Andrew Furney	NYJ	K	5-10	210	Washington State	8	0000-00-00
2698	t	Fitzgerald	Toussaint	Fitzgerald Toussaint	PIT	RB				43	1969-12-31
2095	t	Larry	Donnell	Larry Donnell	NYG	TE				84	1969-12-31
2106	f	Duane	Bennett	Duane Bennett	GB	RB	5-9	213	Minnesota	23	0000-00-00
2115	t	Jeff	Tuel	Jeff Tuel	BUF	QB				7	1969-12-31
2124	t	Dustin	Hopkins	Dustin Hopkins	WAS	K				3	1969-12-31
2131	f	Terrell	Sinkfield	Terrell Sinkfield	NYG	WR	6-1	192	Northern Iowa	2	0000-00-00
2141	t	T.J.	Moe	T.J. Moe	STL	WR	5-11	204	Missouri	87	0000-00-00
2148	f	Marcus	Davis	Marcus Davis	NYJ	WR	6-4	228	Virginia Tech	0	0000-00-00
2158	f	Anthony	Amos	Anthony Amos	DAL	WR	5-11	178	Middle Tennessee State	10	0000-00-00
2166	f	Jeremy	Wright	Jeremy Wright	PIT	RB	5-11	205	Louisville	37	0000-00-00
2176	t	Chris	Thompson	Chris Thompson	WAS	RB				25	1969-12-31
2182	t	Zac	Dysert	Zac Dysert	DEN	QB				2	1969-12-31
2192	t	Knile	Davis	Knile Davis	KC	RB				34	1969-12-31
2202	f	Deonte	Williams	Deonte Williams	OAK	RB	5-10	213	Cal Poly-S.L.O.	30	0000-00-00
2209	t	Brian	Leonhardt	Brian Leonhardt	OAK	TE				87	1969-12-31
2219	t	Dallas	Walker	Dallas Walker	DAL	TE				86	1969-12-31
2228	f	Michael	Rios	Michael Rios	ARI	WR	6-2	203	Marist	1	0000-00-00
2238	t	Will	Tukuafu	Will Tukuafu	SEA	RB	6-2	280	Oregon	46	0000-00-00
2249	t	Darren	Fells	Darren Fells	ARI	TE				85	1969-12-31
2259	t	Emory	Blake	Emory Blake	STL	WR				16	1969-12-31
2269	t	Aaron	Mellette	Aaron Mellette	BAL	WR	6-2	217	Elon	13	0000-00-00
2275	t	Onterio	McCalebb	Onterio McCalebb	CIN	WR	5-10	175	Auburn	86	0000-00-00
2285	t	Cordell	Roberson	Cordell Roberson	BUF	WR	6-4	205	Stephen F. Austin St.	14	0000-00-00
2293	t	Markus	Wheaton	Markus Wheaton	PIT	WR				11	1969-12-31
2302	f	Alex	Carder	Alex Carder	DET	QB	6-2	215	Western Michigan	0	0000-00-00
2310	f	Havard	Rugland	Havard Rugland	DET	K	6-2	240	No College	3	0000-00-00
2320	t	Tyrone	Walker	Tyrone Walker	MIN	WR	5-10	191	Illinois State	11	0000-00-00
2327	f	Erik	Highsmith	Erik Highsmith	MIN	WR	6-1	190	North Carolina	8	0000-00-00
2337	t	Andy	Cruse	Andy Cruse	MIN	WR	6-3	216	Miami (Ohio)	8	0000-00-00
2345	t	Kerwynn	Williams	Kerwynn Williams	ARI	RB				33	1969-12-31
2355	f	Jamal	Miles	Jamal Miles	JAC	WR	5-9	188	Arizona State	81	0000-00-00
2362	t	Rashad	Ross	Rashad Ross	WAS	WR				19	1969-12-31
2371	f	Deon	Goggins	Deon Goggins	ATL	RB	6-1	272	Syracuse	42	0000-00-00
2380	f	Taulib	Ikharo	Taulib Ikharo	CAR	WR	5-10	190	Louisiana Tech	83	0000-00-00
2389	t	Josh	Hill	Josh Hill	NO	TE				89	1969-12-31
2396	t	Tim	Wright	Tim Wright	DET	TE				83	1969-12-31
2405	f	Peter	Tuitupou	Peter Tuitupou	PIT	TE	6-4	246	San Jose State	47	0000-00-00
2414	f	David	Ball	David Ball	PHI	WR	6-1	200	New Hampshire	83	0000-00-00
2426	t	Rahsaan	Vaughn	Rahsaan Vaughn	OAK	WR	6-1	190	Oregon	13	0000-00-00
2433	t	Brock	Jensen	Brock Jensen	MIA	QB	6-3	225	North Dakota State	6	0000-00-00
2443	t	Stephen	Houston	Stephen Houston	NE	RB	6-0	225	Indiana	36	0000-00-00
2451	t	Quincy	Enunwa	Quincy Enunwa	NYJ	WR				81	1969-12-31
2461	t	L'Damian	Washington	L'Damian Washington	KC	WR				1	1969-12-31
2468	t	Henry	Josey	Henry Josey	MIN	RB	5-10	190	Missouri	33	0000-00-00
2478	t	Lee	Doss	Lee Doss	WAS	WR	6-1	178	Southern University	18	0000-00-00
2486	t	Kapri	Bibbs	Kapri Bibbs	DEN	RB				35	1969-12-31
2496	t	De'Anthony	Thomas	De'Anthony Thomas	KC	RB				13	1969-12-31
2503	t	Derek	Carr	Derek Carr	OAK	QB				4	1969-12-31
2513	t	Branden	Oliver	Branden Oliver	SD	RB				43	1969-12-31
2521	t	Zach	Bauman	Zach Bauman	ARI	RB	5-10	200	Northern Arizona	35	0000-00-00
2531	t	Trey	Millard	Trey Millard	SF	RB	6-2	247	Oklahoma	33	0000-00-00
2538	t	Rashaun	Allen	Rashaun Allen	NO	TE				87	1969-12-31
2548	t	Keith	Wenning	Keith Wenning	CIN	QB				3	1969-12-31
2555	t	Jeremy	Hill	Jeremy Hill	CIN	RB				32	1969-12-31
2565	t	Terrance	West	Terrance West	BAL	RB				35	1969-12-31
2572	t	James	Oboh	James Oboh	CLE	TE	6-4	250	Towson	0	0000-00-00
2581	t	Senorise	Perry	Senorise Perry	CHI	RB				32	1969-12-31
2594	t	Orwin	Smith	Orwin Smith	GB	RB	6-0	205	Georgia Tech	0	0000-00-00
2602	t	Jerick	McKinnon	Jerick McKinnon	MIN	RB				31	1969-12-31
2611	t	Lacoltan	Bester	Lacoltan Bester	HOU	WR	6-3	208	Oklahoma	12	0000-00-00
2620	t	Cameron	White	Cameron White	IND	RB	6-2	251	Hillsdale	43	0000-00-00
2629	t	Beau	Blankenship	Beau Blankenship	JAC	RB	5-9	206	Ohio U.	0	0000-00-00
2636	t	Marcel	Jensen	Marcel Jensen	WAS	TE				89	1969-12-31
2645	t	Josh	Stewart	Josh Stewart	TEN	WR				18	1969-12-31
2654	t	Geraldo	Boldewijn	Geraldo Boldewijn	ATL	WR	6-4	220	Boise State	15	0000-00-00
2664	t	Philly	Brown	Philly Brown	CAR	WR				10	1969-12-31
2671	t	Steve	Hull	Steve Hull	NO	WR	6-2	200	Illinois	19	0000-00-00
2680	t	Quintin	Payton	Quintin Payton	DET	WR	6-4	212	North Carolina State	86	0000-00-00
2688	t	Reese	Wiggins	Reese Wiggins	NE	WR	5-11	193	East Carolina	15	0000-00-00
2697	t	Diontae	Spencer	Diontae Spencer	STL	WR	5-9	161	McNeese State	0	0000-00-00
2705	t	Derrick	Strozier	Derrick Strozier	NO	RB	5-8	181	Tulane	48	0000-00-00
2715	f	Mason	Brodine	Mason Brodine	NE	TE	6-7	284	Nebraska-Kearney	86	0000-00-00
2721	t	De'Andre	Presley	De'Andre Presley	CAR	WR				10	1969-12-31
2731	t	Duron	Carter	Duron Carter	IND	WR				9	1969-12-31
2740	t	Devin	Smith	Devin Smith	NYJ	WR				84	1969-12-31
2132	f	Taylor	Stockemer	Taylor Stockemer	MIA	WR	6-4	210	Arkansas State	0	0000-00-00
2142	t	Kenbrell	Thompkins	Kenbrell Thompkins	NYJ	WR				85	1969-12-31
2149	f	Zach	Rogers	Zach Rogers	NYJ	WR	6-0	172	Tennessee	9	0000-00-00
2159	f	Eric	Rogers	Eric Rogers	DAL	WR	6-3	210	California Lutheran	14	0000-00-00
2167	f	Morgan	Newton	Morgan Newton	NYG	TE	6-4	240	Kentucky	0	0000-00-00
2177	t	Skye	Dawson	Skye Dawson	DET	WR	5-9	183	Texas Christian	11	0000-00-00
2183	f	Ryan	Katz	Ryan Katz	DEN	QB	6-1	217	San Diego State	8	0000-00-00
2193	f	Jordan	Roberts	Jordan Roberts	KC	RB	5-10	222	Charleston (WV)	45	0000-00-00
2203	t	Brice	Butler	Brice Butler	DAL	WR				12	1969-12-31
2210	t	Mychal	Rivera	Mychal Rivera	OAK	TE				81	1969-12-31
2220	f	Caleb	TerBush	Caleb TerBush	ARI	QB	6-5	225	Purdue	6	0000-00-00
2229	f	Tyler	Shaw	Tyler Shaw	PIT	WR	6-0	182	Northwest Missouri State	10	0000-00-00
2239	t	Chuck	Jacobs	Chuck Jacobs	SF	WR				17	1969-12-31
2250	f	Victor	Marshall	Victor Marshall	SEA	TE	6-4	225	British Columbia (Canada)	87	0000-00-00
2261	f	Scott	Pillar	Scott Pillar	STL	WR	6-2	192	Albright	17	0000-00-00
2271	t	Matt	Furstenburg	Matt Furstenburg	BAL	TE	6-3	244	Maryland	85	0000-00-00
2277	t	Cobi	Hamilton	Cobi Hamilton	MIA	WR				7	1969-12-31
2287	f	Brandon	Bogotay	Brandon Bogotay	TB	K	6-3	207	Georgia	8	0000-00-00
2295	t	Michael	Ford	Michael Ford	ATL	RB				38	1969-12-31
2304	t	Corey	Fuller	Corey Fuller	DET	WR				10	1969-12-31
2312	t	Johnathan	Franklin	Johnathan Franklin	GB	RB	5-10	205	UCLA	23	0000-00-00
2322	t	Jake	Stoneburner	Jake Stoneburner	MIA	TE				86	1969-12-31
2329	t	Rodney	Smith	Rodney Smith	DAL	WR				83	1969-12-31
2339	t	Alec	Lemon	Alec Lemon	HOU	WR	6-2	207	Syracuse	15	0000-00-00
2347	t	Lanear	Sampson	Lanear Sampson	PIT	WR	5-11	205	Baylor	17	0000-00-00
2357	t	Ace	Sanders	Ace Sanders	JAC	WR	5-7	178	South Carolina	18	0000-00-00
2364	t	Jack	Doyle	Jack Doyle	IND	TE				84	1969-12-31
2373	t	Darius	Johnson	Darius Johnson	ATL	WR	5-10	175	Southern Methodist	13	0000-00-00
2382	t	R.J.	Webb	R.J. Webb	CAR	WR	6-2	205	Furman	17	0000-00-00
2391	f	Jose	Carlos Maltos	Jose Carlos Maltos	NO	K	5-9	201	Unknown	2	0000-00-00
2398	f	Courtney	Gardner	Courtney Gardner	MIA	WR	6-3	215	Sierra Coll. CA (J.C.)	0	0000-00-00
2408	f	Matt	Brown	Matt Brown	TB	RB	5-5	165	Temple	33	0000-00-00
2416	f	Andrei	Lintz	Andrei Lintz	SEA	TE	6-5	250	Washington State	38	0000-00-00
2428	t	Quinn	Sharp	Quinn Sharp	CIN	K	6-1	205	Oklahoma State	3	0000-00-00
2435	t	Damien	Williams	Damien Williams	MIA	RB				34	1969-12-31
2445	t	Jeremy	Gallon	Jeremy Gallon	OAK	WR	5-8	185	Michigan	83	0000-00-00
2453	t	Terrence	Miller	Terrence Miller	NYJ	WR	6-4	234	Arizona	0	0000-00-00
2463	t	Evan	Wilson	Evan Wilson	DAL	TE	6-6	250	Illinois	0	0000-00-00
2470	t	Josh	Huff	Josh Huff	PHI	WR				13	1969-12-31
2480	t	Cody	Hoffman	Cody Hoffman	WAS	WR	6-4	210	Brigham Young	87	0000-00-00
2488	t	Juwan	Thompson	Juwan Thompson	DEN	RB				40	1969-12-31
2498	t	Weston	Dressler	Weston Dressler	KC	WR	5-7	179	North Dakota	13	0000-00-00
2505	t	Karl	Williams	Karl Williams	OAK	RB	6-0	244	Utah	40	0000-00-00
2515	t	Brelan	Chancellor	Brelan Chancellor	PIT	WR	5-8	188	North Texas	19	0000-00-00
2523	t	John	Brown	John Brown	ARI	WR				12	1969-12-31
2533	t	Asante	Cleveland	Asante Cleveland	SD	TE				44	1969-12-31
2540	t	Garrett	Gilbert	Garrett Gilbert	DET	QB				14	1969-12-31
2550	t	Jeremy	Butler	Jeremy Butler	BAL	WR				17	1969-12-31
2557	t	James	Wilder Jr.	James Wilder Jr.	CIN	RB	6-3	232	Florida State	34	0000-00-00
2567	t	Taylor	Gabriel	Taylor Gabriel	CLE	WR				18	1969-12-31
2574	f	Dri	Archer	Dri Archer	PIT	RB				13	1969-12-31
2585	t	TJ	Jones	TJ Jones	DET	WR				13	1969-12-31
2598	t	Colt	Lyerla	Colt Lyerla	GB	TE	6-4	247	Oregon	46	0000-00-00
2609	t	Alfred	Blue	Alfred Blue	HOU	RB				28	1969-12-31
2624	t	Tony	Washington	Tony Washington	IND	WR				7	1969-12-31
2642	t	Bishop	Sankey	Bishop Sankey	TEN	RB				20	1969-12-31
2652	t	Jerome	Smith	Jerome Smith	ATL	RB				34	1969-12-31
2662	t	Darrin	Reaves	Darrin Reaves	KC	RB				32	1969-12-31
2677	t	Mike	Evans	Mike Evans	TB	WR				13	1969-12-31
2693	t	Damien	Thigpen	Damien Thigpen	ARI	RB	5-8	180	UCLA	42	0000-00-00
2702	t	Nathan	Slaughter	Nathan Slaughter	ARI	WR	5-10	185	West Texas A&M	9	0000-00-00
2712	t	Glenn	Winston	Glenn Winston	CLE	RB				12	1969-12-31
2728	t	Mike	Zimmer	Mike Zimmer	SEA	RB	6-2	239	Illinois State	0	0000-00-00
2737	t	DeVante	Parker	DeVante Parker	MIA	WR				11	1969-12-31
2748	t	Jamison	Crowder	Jamison Crowder	WAS	WR				80	1969-12-31
2759	t	Melvin	Gordon	Melvin Gordon	SD	RB				28	1969-12-31
2768	t	Bradley	Pinion	Bradley Pinion	SF	K	6-5	229	Clemson	92	0000-00-00
2777	t	Maxx	Williams	Maxx Williams	BAL	TE				87	1969-12-31
2788	t	Ameer	Abdullah	Ameer Abdullah	DET	RB				21	1969-12-31
2798	t	Jaelen	Strong	Jaelen Strong	HOU	WR				11	1969-12-31
2804	t	Dorial	Green-Beckham	Dorial Green-Beckham	TEN	WR				17	1969-12-31
2813	t	Joey	Iosefa	Joey Iosefa	NE	RB	6-0	245	Hawaii	33	0000-00-00
2822	t	Deontay	Greenberry	Deontay Greenberry	SEA	WR	6-2	210	Houston	86	0000-00-00
2832	t	Dylan	Thompson	Dylan Thompson	SF	QB	6-3	218	South Carolina	13	0000-00-00
2140	t	Mark	Harrison	Mark Harrison	KC	WR	6-3	230	Rutgers	22	0000-00-00
2147	t	Tommy	Bohanon	Tommy Bohanon	NYJ	RB	6-1	247	Wake Forest	40	0000-00-00
2157	f	Joseph	Randle	Joseph Randle	DAL	RB				21	1969-12-31
2165	t	Michael	Cox	Michael Cox	NYG	RB	6-1	222	Massachusetts	29	0000-00-00
2175	t	Jawan	Jamison	Jawan Jamison	PIT	RB	5-7	203	Rutgers	44	0000-00-00
2181	t	Jordan	Reed	Jordan Reed	WAS	TE				86	1969-12-31
2191	t	Tyler	Bray	Tyler Bray	KC	QB				9	1969-12-31
2201	t	Latavius	Murray	Latavius Murray	OAK	RB				28	1969-12-31
2208	t	Nick	Kasa	Nick Kasa	OAK	TE	6-6	265	Colorado	88	0000-00-00
2218	f	David	Rolf	David Rolf	SD	TE	6-3	257	Utah	88	0000-00-00
2227	t	Javone	Lawson	Javone Lawson	ARI	WR	6-1	183	Louisiana-Lafayette	0	0000-00-00
2237	t	Alex	Debniak	Alex Debniak	SF	RB	6-2	240	Stanford	0	0000-00-00
2242	t	Vance	McDonald	Vance McDonald	SF	TE				89	1969-12-31
2251	t	Luke	Willson	Luke Willson	SEA	TE				82	1969-12-31
2258	t	Stedman	Bailey	Stedman Bailey	STL	WR				12	1969-12-31
2268	f	Omarius	Hines	Omarius Hines	GB	WR	6-2	217	Florida	5	0000-00-00
2274	t	Rex	Burkhead	Rex Burkhead	CIN	RB				33	1969-12-31
2284	f	Mike	Edwards	Mike Edwards	CLE	WR	5-11	200	Texas-El Paso	86	0000-00-00
2292	t	Reggie	Dunn	Reggie Dunn	DAL	WR				17	1969-12-31
2301	f	Austin	Signor	Austin Signor	CHI	K	6-4	230	Eastern Illinois	2	0000-00-00
2309	f	Sam	Martin	Sam Martin	DET	K	6-1	205	Appalachian State	0	0000-00-00
2319	t	Charles	Johnson	Charles Johnson	MIN	WR				12	1969-12-31
2326	t	Zach	Line	Zach Line	MIN	RB				48	1969-12-31
2336	t	Alan	Bonner	Alan Bonner	HOU	WR				16	1969-12-31
2344	f	Denodus	O'Bryant	Denodus O'Bryant	IND	RB	5-9	194	Lindenwood	25	0000-00-00
2354	t	Lonnie	Pryor	Lonnie Pryor	TB	RB	6-0	224	Florida State	45	0000-00-00
2361	t	Justin	Hunter	Justin Hunter	TEN	WR				15	1969-12-31
2370	f	Devonte	Campbell	Devonte Campbell	ATL	RB	6-2	255	Maryland	43	0000-00-00
2379	t	Michael	Zordich	Michael Zordich	CAR	RB	6-1	245	Penn State	39	0000-00-00
2388	t	Kenny	Stills	Kenny Stills	MIA	WR				10	1969-12-31
2395	f	Jerry	Johnson	Jerry Johnson	TB	WR	6-3	211	UCLA	14	0000-00-00
2404	f	Colby	Prince	Colby Prince	STL	TE	6-5	257	Oregon State	86	0000-00-00
2413	f	Andrell	Smith	Andrell Smith	MIA	WR	6-3	217	Louisville	15	0000-00-00
2425	f	Chris	Denton	Chris Denton	TB	WR	5-11	195	Mount Union	16	0000-00-00
2432	t	Sammy	Watkins	Sammy Watkins	BUF	WR				14	1969-12-31
2442	t	Roy	Finch	Roy Finch	NE	RB	5-7	180	Oklahoma	29	0000-00-00
2450	t	Chad	Young	Chad Young	NYJ	RB	5-10	240	San Diego State	47	0000-00-00
2460	t	Devin	Street	Devin Street	DAL	WR				15	1969-12-31
2467	t	David	Fluellen	David Fluellen	TEN	RB				31	1969-12-31
2477	t	Lache	Seastrunk	Lache Seastrunk	DAL	RB				26	1969-12-31
2485	t	Bryn	Renner	Bryn Renner	BAL	QB				2	1969-12-31
2495	t	James	Baker	James Baker	KC	RB	6-2	228	Idaho	0	0000-00-00
2502	t	Cairo	Santos	Cairo Santos	KC	K				5	1969-12-31
2512	t	Marion	Grice	Marion Grice	ARI	RB				23	1969-12-31
2520	t	Logan	Thomas	Logan Thomas	MIA	QB				6	1969-12-31
2530	t	Carlos	Hyde	Carlos Hyde	SF	RB				28	1969-12-31
2537	t	Paul	Richardson	Paul Richardson	SEA	WR				10	1969-12-31
2547	t	Alex	Bayer	Alex Bayer	STL	TE				82	1969-12-31
2554	t	AJ	McCarron	AJ McCarron	CIN	QB				5	1969-12-31
2564	t	Isaiah	Crowell	Isaiah Crowell	CLE	RB				34	1969-12-31
2571	t	Willie	Snead	Willie Snead	NO	WR				83	1969-12-31
2580	t	Ka'Deem	Carey	Ka'Deem Carey	CHI	RB				25	1969-12-31
2590	f	Nate	Freese	Nate Freese	DET	K	5-11	192	Boston College	3	0000-00-00
2600	t	Richard	Rodgers	Richard Rodgers	GB	TE				82	1969-12-31
2618	t	Seth	Lobato	Seth Lobato	TB	QB				4	1969-12-31
2634	t	Marqise	Lee	Marqise Lee	JAC	WR				11	1969-12-31
2651	t	Devonta	Freeman	Devonta Freeman	ATL	RB				24	1969-12-31
2661	t	Tyler	Gaffney	Tyler Gaffney	NE	RB	6-0	220	Stanford	36	0000-00-00
2676	t	Aaron	Burks	Aaron Burks	TB	WR	6-3	205	Boise State	0	0000-00-00
2685	t	Gerald	Ford	Gerald Ford	MIA	WR	6-3	220	Valdosta State	0	0000-00-00
2695	t	Kevin	Greene	Kevin Greene	CHI	TE				46	1969-12-31
2710	t	Jordan	Campbell	Jordan Campbell	KC	RB	5-11	240	New Mexico Highlands	38	0000-00-00
2727	t	Corbin	Louks	Corbin Louks	DEN	WR				6	1969-12-31
2735	t	Jay	Ajayi	Jay Ajayi	MIA	RB				33	1969-12-31
2746	t	Kyshoen	Jarrett	Kyshoen Jarrett	WAS	RB	5-10	200	Virginia Tech	28	0000-00-00
2758	t	Clive	Walford	Clive Walford	OAK	TE				88	1969-12-31
2767	t	Blake	Bell	Blake Bell	SF	TE				84	1969-12-31
2775	t	Darren	Waller	Darren Waller	BAL	WR				12	1969-12-31
2785	t	Jesse	James	Jesse James	PIT	TE				81	1969-12-31
2792	t	Ty	Montgomery	Ty Montgomery	GB	WR				88	1969-12-31
2802	t	David	Cobb	David Cobb	TEN	RB				44	1969-12-31
2820	t	Jameill	Showers	Jameill Showers	DAL	QB	6-1	230	UTEP	7	0000-00-00
2830	t	Paul	Lasike	Paul Lasike	ARI	RB	5-11	232	BYU	34	0000-00-00
2844	t	Mark	Weisman	Mark Weisman	CIN	RB	5-11	242	Iowa	35	0000-00-00
2861	t	Alonzo	Harris	Alonzo Harris	GB	RB	6-1	237	Louisiana Lafayette	46	0000-00-00
2872	t	Ezell	Ruffin	Ezell Ruffin	IND	WR	6-0	218	San Diego State	3	0000-00-00
2372	f	Rashad	Evans	Rashad Evans	ATL	WR	5-9	187	Fresno State	89	0000-00-00
2381	t	James	Shaw	James Shaw	PIT	WR	5-11	185	Jacksonville State	10	0000-00-00
2390	t	Keavon	Milton	Keavon Milton	SEA	TE				66	1969-12-31
2397	f	Evan	Landi	Evan Landi	NE	TE	6-3	236	South Florida	49	0000-00-00
2407	t	Austin	Johnson	Austin Johnson	NO	RB				35	1969-12-31
2415	f	Nicholas	Edwards	Nicholas Edwards	ARI	WR	6-3	200	Eastern Washington	19	0000-00-00
2427	f	Bryce	Davis	Bryce Davis	CIN	TE	6-3	245	Central Oklahoma	48	0000-00-00
2434	t	Orleans	Darkwa	Orleans Darkwa	NYG	RB				26	1969-12-31
2444	t	James	White	James White	NE	RB				28	1969-12-31
2452	t	Shaquelle	Evans	Shaquelle Evans	NYJ	WR				81	1969-12-31
2462	t	Jordan	Najvar	Jordan Najvar	DAL	TE	6-6	260	Baylor	87	0000-00-00
2469	t	Kadron	Boone	Kadron Boone	IND	WR	6-0	204	LSU	0	0000-00-00
2479	t	Ryan	Grant	Ryan Grant	WAS	WR				14	1969-12-31
2487	t	Brennan	Clay	Brennan Clay	DEN	RB	5-11	201	Oklahoma	34	0000-00-00
2497	t	Charcandrick	West	Charcandrick West	KC	RB				35	1969-12-31
2504	t	George	Atkinson III	George Atkinson III	OAK	RB				34	1969-12-31
2514	t	Torrence	Allen	Torrence Allen	SD	WR				3	1969-12-31
2522	t	Tim	Cornett	Tim Cornett	HOU	RB	6-0	210	Nevada-Las Vegas	32	0000-00-00
2532	t	Bruce	Ellington	Bruce Ellington	SF	WR				10	1969-12-31
2539	t	Chase	Dixon	Chase Dixon	WAS	TE				83	1969-12-31
2549	t	Lorenzo	Taliaferro	Lorenzo Taliaferro	BAL	RB				34	1969-12-31
2556	t	Jeff	Scott	Jeff Scott	CIN	RB	5-7	162	Mississippi	0	0000-00-00
2566	t	Ray	Agnew	Ray Agnew	DAL	RB				48	1969-12-31
2573	t	Brendon	Kay	Brendon Kay	PIT	QB	6-4	228	Cincinnati	8	0000-00-00
2582	t	James	Franklin	James Franklin	DET	QB	6-2	230	Missouri	14	0000-00-00
2593	t	LaDarius	Perkins	LaDarius Perkins	GB	RB	5-7	195	Mississippi State	36	0000-00-00
2604	t	Kain	Colter	Kain Colter	MIN	WR	6-0	195	Northwestern	13	0000-00-00
2613	t	Anthony	McClung	Anthony McClung	HOU	WR	6-0	177	Cincinnati	18	0000-00-00
2622	t	Donte	Moncrief	Donte Moncrief	IND	WR				10	1969-12-31
2631	t	Storm	Johnson	Storm Johnson	JAC	RB				34	1969-12-31
2638	t	D.J.	Tialavea	D.J. Tialavea	ATL	TE				0	1969-12-31
2647	t	Eric	Ward	Eric Ward	CIN	WR	5-11	203	Texas Tech	0	0000-00-00
2656	t	Freddie	Martino	Freddie Martino	PHI	WR				80	1969-12-31
2666	t	Logan	Kilgore	Logan Kilgore	NO	QB	6-3	206	Middle Tennessee State	5	0000-00-00
2673	t	Nic	Jacobs	Nic Jacobs	JAC	TE				83	1969-12-31
2682	t	Austin	Seferian-Jenkins	Austin Seferian-Jenkins	TB	TE				87	1969-12-31
2690	t	Jerry	Rice Jr.	Jerry Rice Jr.	WAS	WR	5-11	185	Nevada-Las Vegas	6	0000-00-00
2699	t	Jordan	Hall	Jordan Hall	PIT	RB	5-9	191	Ohio State	49	0000-00-00
2707	t	Jerome	Cunningham	Jerome Cunningham	NYG	TE				89	1969-12-31
2717	t	Tim	Smith	Tim Smith	CLE	WR	6-0	195	--	10	0000-00-00
2723	t	Corey	Knox	Corey Knox	BUF	RB	6-3	212	Buffalo	41	0000-00-00
2733	t	Karlos	Williams	Karlos Williams	BUF	RB				29	1969-12-31
2743	t	Geremy	Davis	Geremy Davis	NYG	WR	6-2	217	Connecticut	18	0000-00-00
2752	t	Jeff	Heuerman	Jeff Heuerman	DEN	TE				82	1969-12-31
2762	t	Gerald	Christian	Gerald Christian	ARI	TE				83	1969-12-31
2776	t	Nick	Boyle	Nick Boyle	BAL	TE				82	1969-12-31
2786	t	Jeremy	Langford	Jeremy Langford	CHI	RB				33	1969-12-31
2796	t	Kenny	Hilliard	Kenny Hilliard	HOU	RB	5-11	226	LSU	38	0000-00-00
2811	t	Marcus	Murphy	Marcus Murphy	NO	RB				23	1969-12-31
2825	t	Reggie	Bell	Reggie Bell	WAS	WR				18	1969-12-31
2836	t	Darius	Davis	Darius Davis	SF	WR	5-11	212	--	0	0000-00-00
2846	t	Matt	Lengel	Matt Lengel	CIN	TE	6-8	272	Eastern Kentucky	88	0000-00-00
2859	f	Kyle	Brindza	Kyle Brindza	TB	K				2	1969-12-31
2868	t	Mike	McFarland	Mike McFarland	HOU	TE	6-5	252	South Florida	89	0000-00-00
2878	t	T.J.	Yeldon	T.J. Yeldon	JAC	RB				4	1969-12-31
2890	t	Nick	Harwell	Nick Harwell	DAL	WR	5-11	195	Kansas	17	0000-00-00
2903	t	Eric	Tomlinson	Eric Tomlinson	PHI	TE	6-6	263	UTEP	83	0000-00-00
2913	t	Josh	Harper	Josh Harper	OAK	WR	6-1	185	Fresno State	19	0000-00-00
2924	t	Josh	Lambo	Josh Lambo	SD	K				2	1969-12-31
2937	t	Malcolm	Brown	Malcolm Brown	STL	RB				39	1969-12-31
2947	t	Kevin	Haplea	Kevin Haplea	CLE	TE	6-3	249	Florida State	0	0000-00-00
2959	t	Blake	Renaud	Blake Renaud	MIN	RB	6-1	252	Boise State	49	0000-00-00
2972	t	Lee	Ward	Lee Ward	CAR	RB	5-11	250	Stanford	46	0000-00-00
2982	t	Austin	Hill	Austin Hill	NYJ	WR	6-3	212	Arizona	16	0000-00-00
2993	t	Josh	Reese	Josh Reese	PHI	WR	6-0	180	UCF	89	0000-00-00
3007	t	Kenzel	Doe	Kenzel Doe	PIT	WR	5-8	176	Wisconsin	82	0000-00-00
3017	t	Avius	Capers	Avius Capers	CAR	WR				81	1969-12-31
2457	t	Ben	Malena	Ben Malena	DAL	RB				0	1969-12-31
2465	t	Odell	Beckham Jr.	Odell Beckham Jr.	NYG	WR				13	1969-12-31
2475	f	Carey	Spear	Carey Spear	CLE	K	5-10	190	Vanderbilt	4	0000-00-00
2489	t	Isaiah	Burse	Isaiah Burse	DEN	WR				19	1969-12-31
2501	t	Albert	Wilson	Albert Wilson	KC	WR				12	1969-12-31
2511	t	D.J.	Adams	D.J. Adams	SD	RB	5-10	210	Portland State	0	0000-00-00
2525	t	Walt	Powell	Walt Powell	OAK	WR				18	1969-12-31
2543	t	Kadeem	Jones	Kadeem Jones	STL	RB	5-10	266	Western Kentucky	45	0000-00-00
2560	t	Alex	Neutz	Alex Neutz	CIN	WR	6-3	205	Buffalo	0	0000-00-00
2576	t	Rob	Blanchflower	Rob Blanchflower	PIT	TE	6-4	256	Massachusetts	87	0000-00-00
2587	t	Eric	Ebron	Eric Ebron	DET	TE				85	1969-12-31
2597	t	Jeff	Janis	Jeff Janis	GB	WR				83	1969-12-31
2606	t	Erik	Lora	Erik Lora	MIN	WR				1	1969-12-31
2615	t	Anthony	Denham	Anthony Denham	HOU	TE				81	1969-12-31
2625	t	Erik	Swoope	Erik Swoope	IND	TE				86	1969-12-31
2633	t	Allen	Hurns	Allen Hurns	JAC	WR				88	1969-12-31
2643	t	Julian	Horton	Julian Horton	TEN	WR	6-1	205	Arkansas	18	0000-00-00
2658	t	Tramaine	Thompson	Tramaine Thompson	ATL	WR	5-8	167	Kansas State	87	0000-00-00
2669	t	Brandin	Cooks	Brandin Cooks	NO	WR				10	1969-12-31
2686	t	Jeremy	Johnson	Jeremy Johnson	NE	WR	6-0	179	Southern Methodist	0	0000-00-00
2703	f	Kasey	Redfern	Kasey Redfern	JAC	K	6-2	215	Wofford	0	0000-00-00
2713	t	Demitrius	Bronson	Demitrius Bronson	MIA	RB				32	1969-12-31
2729	t	Doug	McNeil	Doug McNeil	SEA	WR	6-3	200	Bowie State	0	0000-00-00
2745	t	Will	Murphy	Will Murphy	PHI	WR	6-2	193	Oregon	84	0000-00-00
2761	t	JJ	Nelson	JJ Nelson	ARI	WR				14	1969-12-31
2770	t	Sean	Mannion	Sean Mannion	STL	QB				14	1969-12-31
2780	t	C.J.	Uzomah	C.J. Uzomah	CIN	TE	6-6	271	Auburn	87	0000-00-00
2789	t	Michael	Burton	Michael Burton	DET	RB	6-0	247	Rutgers	46	0000-00-00
2799	t	Josh	Robinson	Josh Robinson	IND	RB				34	1969-12-31
2805	t	Tre	McBride	Tre McBride	TEN	WR	6-0	210	William & Mary	16	0000-00-00
2814	t	Kenny	Bell	Kenny Bell	TB	WR				80	1969-12-31
2823	t	Connor	Halliday	Connor Halliday	WAS	QB	6-3	196	--	12	0000-00-00
2833	t	Dres	Anderson	Dres Anderson	SF	WR				6	1969-12-31
2841	t	Cam	Worthy	Cam Worthy	BAL	WR	6-2	211	East Carolina	81	0000-00-00
2850	t	Cameron	Meredith	Cameron Meredith	CHI	WR	6-3	207	Illinois State	81	0000-00-00
2857	t	Vernon	Johnson	Vernon Johnson	DET	WR	6-0	196	Texas A&M-Commerce	83	0000-00-00
2866	t	Larry	Pinkard	Larry Pinkard	GB	WR	6-0	196	Old Dominion	11	0000-00-00
2876	t	Shane	Wynn	Shane Wynn	CLE	WR	5-6	167	Indiana	5	0000-00-00
2883	t	Devin	Gardner	Devin Gardner	NE	WR	6-4	216	Michigan	0	0000-00-00
2893	t	Kenneth	Harper	Kenneth Harper	NYG	RB	5-10	233	Temple	33	0000-00-00
2901	t	John	Harris	John Harris	ATL	WR	6-2	218	Texas	18	0000-00-00
2911	t	Cody	Fajardo	Cody Fajardo	OAK	QB				8	1969-12-31
2918	t	Dreamius	Smith	Dreamius Smith	SD	RB	5-11	225	West Virginia	7	0000-00-00
2928	t	Gannon	Sinclair	Gannon Sinclair	ARI	TE	6-7	270	Missouri State	89	0000-00-00
2935	t	Deshon	Foxx	Deshon Foxx	SEA	WR	5-10	173	Connecticut	14	0000-00-00
2945	t	Darius	Jennings	Darius Jennings	CLE	WR	5-10	169	Virginia	10	0000-00-00
2953	t	Ify	Umodu	Ify Umodu	CHI	WR	6-3	215	Northern Arizona	1	0000-00-00
2963	t	Gavin	Lutman	Gavin Lutman	MIN	WR	6-2	211	Pittsburg State	8	0000-00-00
2970	t	Mike	Meyer	Mike Meyer	TEN	K				3	1969-12-31
2980	t	Kai	De La Cruz	Kai De La Cruz	MIA	WR	6-0	193	Louisville	0	0000-00-00
2987	t	Bronson	Hill	Bronson Hill	BUF	RB	5-10	215	Eastern Michigan	57	0000-00-00
2998	t	Logan	Stokes	Logan Stokes	SD	TE	6-5	255	LSU	46	0000-00-00
3005	t	Jawon	Chisholm	Jawon Chisholm	PIT	RB				49	1969-12-31
3015	t	Paul	Browning	Paul Browning	CAR	WR				7	1969-12-31
2650	t	Jeff	Matthews	Jeff Matthews	ATL	QB	6-4	229	Cornell	9	0000-00-00
2660	t	Brian	Wozniak	Brian Wozniak	ATL	TE	6-4	255	Wisconsin	49	0000-00-00
2675	t	Charles	Sims	Charles Sims	TB	RB				34	1969-12-31
2684	t	Patrick	Murray	Patrick Murray	TB	K				7	1969-12-31
2694	t	Kevin	Ozier	Kevin Ozier	ARI	WR	6-2	200	Arizona State	16	0000-00-00
2709	t	Mitch	Ewald	Mitch Ewald	DEN	K	5-10	176	--	8	0000-00-00
2719	t	Akwasi	Owusu-Ansah	Akwasi Owusu-Ansah	DET	WR	6-0	210	Indiana (PA)	47	0000-00-00
2738	t	AJ	Derby	AJ Derby	NE	TE	6-4	255	Arkansas	86	0000-00-00
2754	t	Chris	Conley	Chris Conley	KC	WR				17	1969-12-31
2771	t	Todd	Gurley	Todd Gurley	STL	RB				30	1969-12-31
2779	t	Tyler	Kroft	Tyler Kroft	CIN	TE				81	1969-12-31
2791	t	Aaron	Ripkowski	Aaron Ripkowski	GB	RB	6-1	246	Oklahoma	22	0000-00-00
2801	t	Marcus	Mariota	Marcus Mariota	TEN	QB				8	1969-12-31
2807	t	Justin	Hardy	Justin Hardy	ATL	WR				16	1969-12-31
2816	t	Dez	Lewis	Dez Lewis	BUF	WR	6-4	214	Central Arkansas	13	0000-00-00
2826	t	Tony	Jones	Tony Jones	WAS	WR	5-11	201	Northwestern	13	0000-00-00
2835	t	DiAndre	Campbell	DiAndre Campbell	SF	WR				19	1969-12-31
2845	t	Jake	Kumerow	Jake Kumerow	CIN	WR	6-5	195	UW-Whitewater	84	0000-00-00
2852	t	Brian	Vogler	Brian Vogler	CHI	TE	6-7	262	Alabama	84	0000-00-00
2862	t	Javess	Blue	Javess Blue	GB	WR				10	1969-12-31
2869	t	Bryan	Bennett	Bryan Bennett	IND	QB				7	1969-12-31
2879	t	Andre	Davis	Andre Davis	BUF	WR	6-1	211	South Florida	82	0000-00-00
2888	t	Wes	Saxton	Wes Saxton	NYJ	TE	6-4	235	South Alabama	86	0000-00-00
2897	t	Matt	LaCosse	Matt LaCosse	NYJ	TE	6-6	245	Illinois	99	0000-00-00
2905	t	Quinton	Dunbar	Quinton Dunbar	WAS	WR	6-2	201	Florida	17	0000-00-00
2914	t	Milton	Williams III	Milton Williams III	OAK	WR	6-2	220	Delaware State	17	0000-00-00
2922	t	Eric	Frohnapfel	Eric Frohnapfel	SD	TE				83	1969-12-31
2931	t	Corey	Acosta	Corey Acosta	SF	K				21	1969-12-31
2940	t	Isiah	Ferguson	Isiah Ferguson	STL	WR	6-5	218	Arkansas-Pine Bluff	2	0000-00-00
2949	t	Ross	Scheuerman	Ross Scheuerman	PIT	RB	6-0	204	Lafayette	29	0000-00-00
2957	t	Harold	Spears	Harold Spears	NO	TE	6-4	248	New Hampshire	89	0000-00-00
2966	t	Connor	Neighbors	Connor Neighbors	TEN	RB	5-11	229	LSU	0	0000-00-00
2974	t	Malcome	Kennedy	Malcome Kennedy	NO	WR	6-0	205	Texas A&M	84	0000-00-00
2983	t	Gus	Johnson	Gus Johnson	DAL	RB	5-10	215	Stephen F Austin	37	0000-00-00
2991	t	Kevin	Monangai	Kevin Monangai	PHI	RB	5-8	209	Villanova	39	0000-00-00
3001	t	Tom	Nelson	Tom Nelson	BAL	WR	5-11	200	Illinois State	84	0000-00-00
3009	t	Jarrod	West	Jarrod West	PIT	WR	6-2	203	Syracuse	2	0000-00-00
3018	t	Jason	Myers	Jason Myers	JAC	K				2	1969-12-31
2657	t	Bernard	Reedy	Bernard Reedy	ATL	WR				89	1969-12-31
2667	t	Timothy	Flanders	Timothy Flanders	CLE	RB				49	1969-12-31
2674	t	Brendan	Bigelow	Brendan Bigelow	TB	RB	5-10	180	California	0	0000-00-00
2683	t	Ian	Thompson	Ian Thompson	TB	TE	6-4	234	Louisiana-Lafayette	49	0000-00-00
2691	t	Deon	Anthony	Deon Anthony	KC	WR	6-0	214	Troy	4	0000-00-00
2700	t	C.J.	Goodwin	C.J. Goodwin	PIT	WR				18	1969-12-31
2708	t	Stephen	Campbell	Stephen Campbell	WAS	RB	6-1	245	West Virginia Wesleyan	45	0000-00-00
2718	t	Josh	D. Harris	Josh D. Harris	PIT	RB	5-11	210	Wake Forest	1	0000-00-00
2724	t	Chris	Manhertz	Chris Manhertz	BUF	TE				47	1969-12-31
2734	t	Nick	O'Leary	Nick O'Leary	BUF	TE	6-3	252	Florida State	84	0000-00-00
2744	t	Nelson	Agholor	Nelson Agholor	PHI	WR				17	1969-12-31
2753	t	Da'Ron	Brown	Da'Ron Brown	KC	WR				4	1969-12-31
2763	t	Mike	Davis	Mike Davis	SF	RB				22	1969-12-31
2781	t	Duke	Johnson	Duke Johnson	CLE	RB				29	1969-12-31
2795	t	MyCole	Pruitt	MyCole Pruitt	MIN	TE				83	1969-12-31
2810	t	Garrett	Grayson	Garrett Grayson	NO	QB				18	1969-12-31
2819	t	Andrew	Franks	Andrew Franks	MIA	K				3	1969-12-31
2829	t	Ty	Long	Ty Long	WAS	K				3	1969-12-31
2843	t	Terrell	Watson	Terrell Watson	CIN	RB	6-1	242	Azusa Pacific	31	0000-00-00
2853	t	Jeremiah	Detmer	Jeremiah Detmer	CHI	K	5-8	180	--	85	0000-00-00
2863	t	Ricky	Collins	Ricky Collins	GB	WR	6-0	198	Texas A&M	13	0000-00-00
2870	t	Quan	Bray	Quan Bray	IND	WR				5	1969-12-31
2880	t	Damarr	Aultman	Damarr Aultman	MIA	WR				87	1969-12-31
2889	t	George	Farmer	George Farmer	DAL	WR	6-1	215	USC	19	0000-00-00
2898	t	Raheem	Mostert	Raheem Mostert	MIA	RB	5-10	195	Purdue	48	0000-00-00
2906	t	David	Porter	David Porter	DAL	WR	6-0	197	TCU	0	0000-00-00
2915	t	Austin	Willis	Austin Willis	BUF	WR	5-10	185	Emporia State	19	0000-00-00
2923	t	Brian	Parker	Brian Parker	KC	TE	6-4	265	Albany	82	0000-00-00
2932	t	Brandon	Cottom	Brandon Cottom	SEA	RB				44	1969-12-31
2941	t	Bradley	Marquez	Bradley Marquez	STL	WR	5-10	196	Texas Tech	15	0000-00-00
2950	t	Cameron	Stingily	Cameron Stingily	PIT	RB	6-1	235	Northern Illinois	0	0000-00-00
2958	t	Taylor	Heinicke	Taylor Heinicke	MIN	QB				6	1969-12-31
2967	t	Deon	Long	Deon Long	TEN	WR	6-0	192	Maryland	84	0000-00-00
2975	t	Kyle	Prater	Kyle Prater	NO	WR	6-5	231	Northwestern	0	0000-00-00
2984	t	Mack	Brown	Mack Brown	WAS	RB				42	1969-12-31
2992	t	Mike	Johnson	Mike Johnson	PHI	WR	6-2	210	Delaware	84	0000-00-00
3002	t	Trent	Steelman	Trent Steelman	BAL	WR	5-11	200	Army	6	0000-00-00
3010	t	Deon	Butler	Deon Butler	DET	TE				47	1969-12-31
3021	t	Rashad	Greene	Rashad Greene	JAC	WR	5'11	185	Florida State	0	1992-09-23
2679	t	Solomon	Patton	Solomon Patton	DEN	WR				0	1969-12-31
2687	t	Wilson	Van Hooser	Wilson Van Hooser	NE	WR	6-0	195	Troy	81	0000-00-00
2696	t	Kevin	Smith	Kevin Smith	SEA	WR				17	1969-12-31
2704	t	Sergio	Castillo	Sergio Castillo	ATL	K	5-11	195	West Texas A&M	0	0000-00-00
2714	t	Morrell	Presley	Morrell Presley	SEA	WR	6-4	225	--	87	0000-00-00
2720	t	Mario	Harvey	Mario Harvey	NYJ	RB	6-0	264	Marshall	0	0000-00-00
2730	t	Roosevelt	Nix-Jones	Roosevelt Nix-Jones	PIT	RB	5-11	248	Kent State	45	0000-00-00
2739	t	Bryce	Petty	Bryce Petty	NYJ	QB				9	1969-12-31
2749	t	Evan	Spencer	Evan Spencer	TB	WR				85	1969-12-31
2755	t	James	O'Shaughnessy	James O'Shaughnessy	KC	TE				80	1969-12-31
2764	t	Jarryd	Hayne	Jarryd Hayne	SF	RB	6-2	220	null	38	0000-00-00
2772	t	Bud	Sasser	Bud Sasser	STL	WR	6-2	210	Missouri	21	0000-00-00
2782	t	Vince	Mayle	Vince Mayle	DAL	WR				85	1969-12-31
2787	t	Kevin	White	Kevin White	CHI	WR				13	1969-12-31
2797	t	Keith	Mumphery	Keith Mumphery	HOU	WR				12	1969-12-31
2803	t	Jalston	Fowler	Jalston Fowler	TEN	RB	5-11	254	Alabama	45	0000-00-00
2812	t	Jameis	Winston	Jameis Winston	TB	QB				3	1969-12-31
2821	t	Antwan	Goodley	Antwan Goodley	DAL	WR	5-10	205	Baylor	85	0000-00-00
2831	t	Jaxon	Shipley	Jaxon Shipley	ARI	WR	6-0	190	Texas	16	0000-00-00
2839	t	Terrence	Magee	Terrence Magee	BAL	RB	5-9	215	LSU	30	0000-00-00
2848	t	Randall	Telfer	Randall Telfer	CLE	TE	6-4	250	USC	86	0000-00-00
2855	t	Rasheed	Williams	Rasheed Williams	DET	RB	6-1	215	--	99	0000-00-00
2864	t	Adrian	Coxson	Adrian Coxson	GB	WR	6-1	209	Stony Brook	1	0000-00-00
2874	t	Justin	Sinz	Justin Sinz	IND	TE	6-4	249	Purdue	44	0000-00-00
2881	t	Christion	Jones	Christion Jones	MIA	WR	5-11	190	Alabama	1	0000-00-00
2891	t	Rodney	Whitehead	Rodney Whitehead	DAL	WR	5-9	180	Florida Atlantic	13	0000-00-00
2899	t	Rasheed	Bailey	Rasheed Bailey	PHI	WR				18	1969-12-31
2909	t	Donatella	Luckett	Donatella Luckett	KC	WR	6-0	211	Harding	15	0000-00-00
2916	t	Gabe	Holmes	Gabe Holmes	OAK	TE	6-5	255	Purdue	82	0000-00-00
2926	t	Trevor	Harman	Trevor Harman	ARI	WR	6-3	205	Shippensburg	84	0000-00-00
2933	t	Thomas	Rawls	Thomas Rawls	SEA	RB				34	1969-12-31
2943	t	John	Peters	John Peters	CIN	TE	6-8	230	Mount St. Joseph	48	0000-00-00
2951	t	Eli	Rogers	Eli Rogers	PIT	WR	5-10	180	Louisville	6	0000-00-00
2961	t	Isaac	Fruechte	Isaac Fruechte	MIN	WR	6-3	210	Minnesota	15	0000-00-00
2968	t	Andrew	Turzilli	Andrew Turzilli	TEN	WR	6-4	195	Rutgers	86	0000-00-00
2978	t	Rannell	Hall	Rannell Hall	CLE	WR	6-1	200	UCF	16	0000-00-00
2985	t	Desmond	Lawrence	Desmond Lawrence	CIN	WR	5-10	181	North Carolina A&T	83	0000-00-00
2996	t	Jordan	Taylor	Jordan Taylor	DEN	WR	6-5	210	Rice	87	0000-00-00
3003	t	Michael	Bennett	Michael Bennett	CIN	WR				80	1969-12-31
3013	t	Tebucky	Jones	Tebucky Jones	TEN	WR	6-0	195	Fordham	5	0000-00-00
2706	t	Tyler	McDonald	Tyler McDonald	MIA	WR	6-3	190	South Carolina	0	0000-00-00
2716	t	Nikita	Whitlock	Nikita Whitlock	NYG	RB				49	1969-12-31
2722	t	Jordan	Gay	Jordan Gay	BUF	K				4	1969-12-31
2732	t	Taylor	Sloat	Taylor Sloat	TB	TE	6-4	245	UC Davis	0	0000-00-00
2741	t	Geoff	Swaim	Geoff Swaim	DAL	TE	6-4	260	Texas	87	0000-00-00
2751	t	Joe	Don Duncan	Joe Don Duncan	DEN	RB	6-4	270	Dixie State	42	0000-00-00
2757	t	Andre	Debose	Andre Debose	OAK	WR	6-0	190	Florida	0	0000-00-00
2766	t	Rory	Anderson	Rory Anderson	SF	TE				80	1969-12-31
2774	t	Breshad	Perriman	Breshad Perriman	BAL	WR				18	1969-12-31
2784	t	Sammie	Coates	Sammie Coates	PIT	WR				14	1969-12-31
2794	t	Stefon	Diggs	Stefon Diggs	MIN	WR				14	1969-12-31
2809	t	Devin	Funchess	Devin Funchess	CAR	WR				17	1969-12-31
2818	t	Nigel	King	Nigel King	SF	WR	6-3	210	Kansas	4	0000-00-00
2828	t	Devin	Mahina	Devin Mahina	WAS	TE	6-6	250	BYU	89	0000-00-00
2837	t	DeAndrew	White	DeAndrew White	SF	WR	6-0	192	Alabama	18	0000-00-00
2847	t	Tom	Obarski	Tom Obarski	CIN	K				4	1969-12-31
2860	t	John	Crockett	John Crockett	GB	RB				38	1969-12-31
2871	t	Tyler	Varga	Tyler Varga	IND	RB	5-11	225	Yale	38	0000-00-00
2885	t	Jimmay	Mundine	Jimmay Mundine	NE	TE	6-2	240	Kansas	81	0000-00-00
2895	t	Will	Tye	Will Tye	NYG	TE				46	1969-12-31
2907	t	Keshawn	Hill	Keshawn Hill	KC	RB	5-10	200	Sam Houston State	0	0000-00-00
2920	t	Tyrell	Williams	Tyrell Williams	SD	WR	6-4	205	Western Oregon	16	0000-00-00
2930	t	Mario	Hull	Mario Hull	SF	WR	6-1	200	Rice	3	0000-00-00
2942	t	Tyler	Slavin	Tyler Slavin	STL	WR	6-1	201	New Mexico Highlands	8	0000-00-00
2955	t	Jarred	Haggins	Jarred Haggins	DET	WR	5-11	192	Florida State	0	0000-00-00
2965	t	Abou	Toure	Abou Toure	IND	RB	6-2	230	Tennessee-Martin	39	0000-00-00
2976	t	Dominique	Brown	Dominique Brown	TB	RB				32	1969-12-31
2989	t	Tony	Creecy	Tony Creecy	NE	RB				0	1969-12-31
3000	t	Daniel	Brown	Daniel Brown	BAL	WR				83	1969-12-31
3011	t	James	Butler	James Butler	GB	WR				13	1969-12-31
2750	t	Trevor	Siemian	Trevor Siemian	DEN	QB	6-3	220	Northwestern	13	0000-00-00
2756	t	Amari	Cooper	Amari Cooper	OAK	WR				89	1969-12-31
2765	t	DeAndre	Smelter	DeAndre Smelter	SF	WR				15	1969-12-31
2773	t	Buck	Allen	Javorius Allen	BAL	RB				37	1969-12-31
2783	t	Malcolm	Johnson	Malcolm Johnson	CLE	RB				42	1969-12-31
2793	t	Kennard	Backman	Kennard Backman	GB	TE				86	1969-12-31
2808	t	Cameron	Artis-Payne	Cameron Artis-Payne	CAR	RB				34	1969-12-31
2817	t	Clay	Burton	Clay Burton	BUF	TE	6-4	258	Florida	88	0000-00-00
2827	t	Tyler	Rutenbeck	Tyler Rutenbeck	WAS	WR	6-2	186	Dubuque	85	0000-00-00
2838	t	Jerry	Lovelocke	Jerry Lovelocke	BAL	QB	6-4	248	Prairie View A&M	0	0000-00-00
2854	t	Anthony	Boone	Anthony Boone	DET	QB	6-0	231	--	7	0000-00-00
2873	t	Jean	Sifrin	Jean Sifrin	IND	TE	6-5	245	--	87	0000-00-00
2886	t	Jake	Heaps	Jake Heaps	NYJ	QB				0	1969-12-31
2896	t	Ben	Edwards	Ben Edwards	NYG	WR	5-10	197	Richmond	0	0000-00-00
2908	t	Kenny	Cook	Kenny Cook	KC	WR				6	1969-12-31
2925	t	Phillip	Sims	Phillip Sims	ARI	QB	6-4	210	--	1	0000-00-00
2938	t	Terrence	Franks	Terrence Franks	STL	RB	5-9	203	Texas State	0	0000-00-00
2948	t	Tyler	Murphy	Tyler Murphy	PIT	WR	6-1	214	Boston College	16	0000-00-00
2960	t	DaVaris	Daniels	DaVaris Daniels	MIN	WR	6-1	201	Notre Dame	16	0000-00-00
2977	t	Donteea	Dye	Donteea Dye	TB	WR	6-0	195	Heidelberg	17	0000-00-00
2995	t	Ernst	Brun	Ernst Brun	WAS	TE				43	1969-12-31
3012	t	Ed	Williams	Ed Williams	GB	WR	6-2	195	Fort Hays State	5	0000-00-00
2769	t	Tyler	Lockett	Tyler Lockett	SEA	WR				16	1969-12-31
2778	t	Mario	Alford	Mario Alford	CIN	WR				15	1969-12-31
2790	t	Brett	Hundley	Brett Hundley	GB	QB				7	1969-12-31
2800	t	Phillip	Dorsett	Phillip Dorsett	IND	WR				15	1969-12-31
2806	t	Tevin	Coleman	Tevin Coleman	ATL	RB				26	1969-12-31
2815	t	Kaelin	Clay	Kaelin Clay	TB	WR				14	1969-12-31
2824	t	Trey	Williams	Trey Williams	NE	RB	5-7	195	Texas A&M	30	0000-00-00
2834	t	Issac	Blakeney	Issac Blakeney	SF	WR				1	1969-12-31
2842	t	Justin	Manton	Justin Manton	BAL	K				3	1969-12-31
2851	t	Levi	Norwood	Levi Norwood	CHI	WR	6-0	197	Baylor	14	0000-00-00
2858	t	Casey	Pierce	Casey Pierce	DET	TE	6-3	244	Kent State	86	0000-00-00
2867	t	Khari	Lee	Khari Lee	CHI	TE	6-4	235	Bowie State	82	0000-00-00
2877	t	Beau	Gardner	Beau Gardner	ATL	TE	6-4	240	Northern Arizona	0	0000-00-00
2884	t	Chris	Harper	Chris Harper	NE	WR	5-11	185	California	14	0000-00-00
2894	t	Akeem	Hunt	Akeem Hunt	HOU	RB	5-10	190	Purdue	39	0000-00-00
2902	t	Andrew	Gleichert	Andrew Gleichert	PHI	TE				85	1969-12-31
2912	t	Michael	Dyer	Michael Dyer	OAK	RB				40	1969-12-31
2919	t	Titus	Davis	Titus Davis	SD	WR	6-1	200	Central Michigan	84	0000-00-00
2929	t	Jack	Tabb	Jack Tabb	NO	TE	6-3	250	North Carolina	0	0000-00-00
2936	t	Kasen	Williams	Kasen Williams	SEA	WR	6-1	219	Washington	18	0000-00-00
2946	t	Emmanuel	Bibbs	Emmanuel Bibbs	CLE	TE				49	1969-12-31
2954	t	Desmond	Martin	Desmond Martin	DET	RB	6-0	221	Wayne State (MI)	40	0000-00-00
2964	t	Chandler	Worthy	Chandler Worthy	HOU	WR	5-9	173	Troy	6	0000-00-00
2971	t	Terron	Ward	Terron Ward	ATL	RB	5-7	201	Oregon State	33	0000-00-00
2981	t	Zach	D'Orazio	Zach D'Orazio	NE	WR	6-2	217	Akron	83	0000-00-00
2988	t	Ricky	Seale	Ricky Seale	BUF	RB	5-9	203	Stanford	0	0000-00-00
2999	t	Daniel	Rodriguez	Daniel Rodriguez	STL	WR	5-8	180	Clemson	3	0000-00-00
3006	t	Braylon	Heard	Braylon Heard	PIT	RB	5-10	198	Kentucky	40	0000-00-00
3016	t	Damiere	Byrd	Damiere Byrd	CAR	WR				18	1969-12-31
2840	t	DeAndre	Carter	DeAndre Carter	BAL	WR				16	1969-12-31
2849	t	Shane	Carden	Shane Carden	CHI	QB				15	1969-12-31
2856	t	Zach	Zenner	Zach Zenner	DET	RB	5-11	222	South Dakota State	34	0000-00-00
2865	t	Jimmie	Hunt	Jimmie Hunt	GB	WR	6-0	208	Missouri	82	0000-00-00
2875	t	Marquez	Clark	Marquez Clark	ATL	WR				87	1969-12-31
2882	t	Tim	Semisch	Tim Semisch	MIA	TE	6-8	275	Northern Illinois	82	0000-00-00
2892	t	Ray	Hamilton	Ray Hamilton	PIT	TE	6-5	252	Iowa	80	0000-00-00
2900	t	Devante	Davis	Devante Davis	PHI	WR	6-3	220	UNLV	81	0000-00-00
2910	t	Jeret	Smith	Jeret Smith	KC	WR	6-0	215	McMurry	8	0000-00-00
2917	t	Jahwan	Edwards	Jahwan Edwards	SD	RB				30	1969-12-31
2927	t	Damond	Powell	Damond Powell	ARI	WR	5-11	180	--	22	0000-00-00
2934	t	Rod	Smith	Rod Smith	SEA	RB	6-3	226	Ohio State	45	0000-00-00
2944	t	Luke	Lundy	Luke Lundy	CLE	RB	6-0	230	null	42	0000-00-00
2952	t	Cameron	Clear	Cameron Clear	PIT	TE				85	1969-12-31
2962	t	Jordan	Leslie	Jordan Leslie	MIN	WR	6-2	209	BYU	9	0000-00-00
2969	t	Tevin	Westbrook	Tevin Westbrook	TB	TE	6-5	257	Florida	87	0000-00-00
2979	t	Adam	Humphries	Adam Humphries	TB	WR	5-11	195	Clemson	11	0000-00-00
2986	t	A.J.	Cruz	A.J. Cruz	CHI	WR	5-9	195	Brown	4	0000-00-00
2997	t	Dan	Light	Dan Light	DEN	TE	6-5	265	Fordham	84	0000-00-00
3004	t	Anthony	Ezeakunne	Anthony Ezeakunne	CLE	TE				89	1969-12-31
3014	t	Brandon	Wegher	Brandon Wegher	CAR	RB	5-10	215	Morningside	32	0000-00-00
2887	t	Jonathon	Rumph	Jonathon Rumph	NYJ	WR	6-5	218	Georgia	83	0000-00-00
2904	t	Justin	Tukes	Justin Tukes	PHI	TE	6-4	259	UCF	82	0000-00-00
2921	t	Demetrius	Wilson	Demetrius Wilson	SD	WR	6-1	185	Arkansas	85	0000-00-00
2939	t	Zach	Laskey	Zach Laskey	STL	RB	6-2	225	Georgia Tech	19	0000-00-00
2956	t	Mitchell	Henry	Mitchell Henry	DEN	TE	6-4	252	Western Kentucky	84	0000-00-00
2973	t	R.J.	Harris	R.J. Harris	NO	WR	6-0	194	New Hampshire	19	0000-00-00
2990	t	Jake	Bequette	Jake Bequette	NE	TE	6-5	265	Arkansas	85	0000-00-00
3008	t	Shakim	Phillips	Shakim Phillips	PIT	WR	6-3	206	Boston College	19	0000-00-00
\.


--
-- Data for Name: nfl_projections; Type: TABLE DATA; Schema: public; Owner: Raymond
--

COPY nfl_projections (name, p_yds, p_tds, p_ints, r_tds, r_yds, rec_catches, rec_yds, rec_tds, total, team, "position", status, dk_total, week, year, nfl_id, fum_td, tpc, fum_lost, opp, sacks, ints, fum_rec, safety, td, ret, pts_allow) FROM stdin;
Kevin Norwood	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	1	2015	2543689	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
James Jones	0	0	0	0	0	\N	25	0	2.50	GB	WR	\N	2.5	1	2015	2507183	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Jerricho Cotchery	0	0	0	0	0	\N	23	0	2.30	CAR	WR	\N	2.3	1	2015	2506004	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Knile Davis	0	0	0	0	14	\N	6	0	2.00	KC	RB	\N	2	1	2015	2540178	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Jaguars	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100015	\N	\N	\N	CAR	1	1	0	0	0	0	24
Kenbrell Thompkins	0	0	0	0	0	\N	0	0	0.00	NYJ	WR	\N	0	1	2015	2539214	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Stepfan Taylor	0	0	0	0	10	\N	6	0	1.60	ARI	RB	\N	1.6	1	2015	2539315	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Nick Toon	0	0	0	0	0	\N	0	0	0.00	STL	WR	\N	0	1	2015	2532960	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Terron Ward	0	0	0	0	0	\N	0	0	0.00	ATL	RB	\N	0	1	2015	2553762	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Jimmy Garoppolo	0	0	0	0	0	\N	0	0	0.00	NE	QB	\N	0	1	2015	2543801	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Ryan Mallett	0	0	0	0	0	\N	0	0	0.00	BAL	QB	\N	0	1	2015	2495443	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Bryce Petty	0	0	0	0	0	\N	0	0	0.00	NYJ	QB	\N	0	1	2015	2552369	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Andre Ellington	0	0	0	1	70	\N	28	0	15.80	ARI	RB	\N	15.8	1	2015	2539217	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Jeremy Hill	0	0	0	1	70	\N	13	0	14.30	CIN	RB	\N	14.3	1	2015	2543603	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Melvin Gordon	0	0	0	1	52	\N	8	0	12.00	SD	RB	\N	12	1	2015	2552469	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Michael Floyd	0	0	0	0	0	\N	54	1	11.40	ARI	WR	\N	11.4	1	2015	2532841	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Geno Smith	0	0	0	0	0	\N	0	0	0.00	NYJ	QB	\N	0	1	2015	2539335	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Myles White	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	1	2015	2541905	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Tyrell Williams	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	1	2015	2553913	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Darren Fells	0	0	0	0	0	\N	0	0	0.00	ARI	TE	\N	0	1	2015	2540928	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Josh Freeman	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	1	2015	79557	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Brandon Gibson	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	81288	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
James Develin	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	1	2015	2508101	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Marcus Easley	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	1	2015	497286	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Kamar Aiken	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	1	2015	2530660	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Rory Anderson	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	1	2015	2553465	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Brandon Coleman	0	0	0	0	0	\N	34	0	3.40	NO	WR	\N	3.4	1	2015	2550328	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Benny Cunningham	0	0	0	0	20	\N	29	0	2.90	STL	RB	\N	2.9000000000000004	1	2015	2541544	0	0	1	SEA	\N	\N	\N	\N	\N	\N	\N
Mike Tolbert	0	0	0	0	12	\N	15	0	2.70	CAR	RB	\N	2.7	1	2015	2507428	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Sammie Coates	0	0	0	0	0	\N	0	0	0.00	PIT	WR	\N	0	1	2015	2552470	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Jerome Cunningham	0	0	0	0	0	\N	0	0	0.00	NYG	TE	\N	0	1	2015	2551359	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Matt Hazel	0	0	0	0	0	\N	0	0	0.00	MIA	WR	\N	0	1	2015	2543792	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Ryan Hewitt	0	0	0	0	0	\N	0	0	0.00	CIN	TE	\N	0	1	2015	2550206	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Ryan Mathews	0	0	0	1	59	\N	16	0	13.50	PHI	RB	\N	13.5	1	2015	497188	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Chris Ivory	0	0	0	1	63	\N	9	0	13.20	NYJ	RB	\N	13.200000000000001	1	2015	2507999	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Sammy Watkins	0	0	0	0	2	\N	64	1	12.60	BUF	WR	\N	12.600000000000001	1	2015	2543457	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Eagles	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	6	1	2015	100025	\N	\N	\N	@ATL	2	1	1	0	0	0	25
Raiders	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	1	2015	100018	\N	\N	\N	CIN	2	1	0	0	0	0	20
Redskins	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	1	2015	100032	\N	\N	\N	MIA	2	1	0	0	0	0	24
Rishard Matthews	0	0	0	0	0	\N	0	0	0.00	MIA	WR	\N	0	1	2015	2532903	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Cameron Meredith	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	1	2015	2553568	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Jordy Nelson	0	0	0	0	0	\N	0	0	0.00	GB	WR	\N	0	1	2015	1032	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Levine Toilolo	0	0	0	0	0	\N	12	0	1.20	ATL	TE	\N	1.2	1	2015	2540203	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Travis Benjamin	0	0	0	0	0	\N	8	0	0.80	CLE	WR	\N	0.8	1	2015	2532790	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Andrew Quarless	0	0	0	0	0	\N	8	0	0.80	GB	TE	\N	0.8	1	2015	497258	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Jalen Saunders	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	1	2015	2543745	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Connor Shaw	0	0	0	0	0	\N	0	0	0.00	CLE	QB	\N	0	1	2015	2550252	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Delanie Walker	0	0	0	0	0	\N	50	1	11.00	TEN	TE	\N	11	1	2015	2495966	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Heath Miller	0	0	0	0	0	\N	48	1	10.80	PIT	TE	\N	10.8	1	2015	2506369	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Derek Carr	198	1	1	0	4	\N	0	0	10.32	OAK	QB	\N	11.32	1	2015	2543499	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Ladarius Green	0	0	0	0	0	\N	34	1	9.40	SD	TE	\N	9.4	1	2015	2532853	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Greg Olsen	0	0	0	0	0	\N	41	0	4.10	CAR	TE	\N	4.1	1	2015	2495700	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Danny Amendola	0	0	0	0	0	\N	38	0	3.80	NE	WR	\N	3.8	1	2015	2649	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Marlon Brown	0	0	0	0	0	\N	35	0	3.50	BAL	WR	\N	3.5	1	2015	2540005	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Kelvin Benjamin	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	1	2015	2543471	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Josh Boyce	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	2540181	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Daniel Brown	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	1	2015	2553900	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Eddie Lacy	0	0	0	1	121	\N	38	0	21.90	GB	RB	\N	24.900000000000002	1	2015	2540168	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Tony Romo	294	2	1	0	4	\N	0	0	18.16	DAL	QB	\N	19.159999999999997	1	2015	2505354	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Alex Smith	223	2	1	0	14	\N	0	0	16.32	KC	QB	\N	17.32	1	2015	2506340	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Nick Kasa	0	0	0	0	0	\N	0	0	0.00	DEN	TE	\N	0	1	2015	2540233	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Travaris Cadet	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	1	2015	2535890	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Kendall Hunter	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	1	2015	2495172	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Ryan Grant	0	0	0	0	0	\N	0	0	0.00	WAS	WR	\N	0	1	2015	2543759	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Cory Harkey	0	0	0	0	0	\N	0	0	0.00	STL	TE	\N	0	1	2015	2533353	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Dexter McCluster	0	0	0	0	34	\N	27	0	6.10	TEN	RB	\N	6.1	1	2015	497190	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Nick O'Leary	0	0	0	0	0	\N	0	0	0.00	BUF	TE	\N	0	1	2015	2552403	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Niles Paul	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	1	2015	2495211	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Nate Washington	0	0	0	0	0	\N	24	0	2.40	HOU	WR	\N	2.4	1	2015	2506313	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Chris Johnson	0	0	0	0	18	\N	5	0	2.30	ARI	RB	\N	2.3	1	2015	262	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Jermaine Gresham	0	0	0	0	0	\N	20	0	2.00	ARI	TE	\N	2	1	2015	497238	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Saints	\N	\N	\N	\N	\N	\N	\N	\N	2.00	\N	DST	\N	2	1	2015	100022	\N	\N	\N	@ARI	1	1	0	0	0	0	30
Rod Streater	0	0	0	0	0	\N	18	0	1.80	OAK	WR	\N	1.8	1	2015	2535869	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Dwayne Harris	0	0	0	0	0	\N	14	0	1.40	NYG	WR	\N	1.4	1	2015	2495159	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Case Keenum	0	0	0	0	0	\N	0	0	0.00	STL	QB	\N	0	1	2015	2532888	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Dan Orlovsky	0	0	0	0	0	\N	0	0	0.00	DET	QB	\N	0	1	2015	2506481	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Eli Manning	250	2	1	0	1	\N	0	0	16.10	NYG	QB	\N	17.1	1	2015	2505996	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
C.J. Anderson	0	0	0	1	64	\N	20	0	14.40	DEN	RB	\N	14.4	1	2015	2540269	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Brandin Cooks	0	0	0	0	8	\N	69	1	13.70	NO	WR	\N	13.7	1	2015	2543498	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Shane Vereen	0	0	0	1	27	\N	32	0	11.90	NYG	RB	\N	11.9	1	2015	2495473	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Isaiah Crowell	0	0	0	1	47	\N	8	0	11.50	CLE	RB	\N	11.5	1	2015	2550189	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Matthew Slater	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	4487	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Evan Spencer	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	1	2015	2553289	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Daryl Richardson	0	0	0	0	0	\N	0	0	0.00	CLE	RB	\N	0	1	2015	2534789	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Nick Williams	0	0	0	0	0	\N	0	0	0.00	ATL	WR	\N	0	1	2015	2540279	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Jerome Felton	0	0	0	0	0	\N	0	0	0.00	BUF	RB	\N	0	1	2015	230	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Isaac Fruechte	0	0	0	0	0	\N	0	0	0.00	MIN	WR	\N	0	1	2015	2553883	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Mike Gillislee	0	0	0	0	0	\N	0	0	0.00	BUF	RB	\N	0	1	2015	2539663	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Kellen Davis	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	1	2015	2507486	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Jack Doyle	0	0	0	0	0	\N	0	0	0.00	IND	TE	\N	0	1	2015	2540232	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Kevin White	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	1	2015	2553432	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
David Ausberry	0	0	0	0	0	\N	0	0	0.00	OAK	TE	\N	0	1	2015	2499277	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Joe Banyard	0	0	0	0	0	\N	0	0	0.00	JAC	RB	\N	0	1	2015	2535696	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Darren McFadden	0	0	0	0	28	\N	3	0	3.10	DAL	RB	\N	3.1	1	2015	284	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Steve Johnson	0	0	0	0	0	\N	29	0	2.90	SD	WR	\N	2.9	1	2015	768	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Derrick Coleman	0	0	0	0	0	\N	0	0	0.00	SEA	RB	\N	0	1	2015	2534871	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Alonzo Harris	0	0	0	0	0	\N	0	0	0.00	BAL	RB	\N	0	1	2015	2553533	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Robert Herron	0	0	0	0	0	\N	0	0	0.00	MIA	WR	\N	0	1	2015	2543777	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Michael Hoomanawanui	0	0	0	0	0	\N	0	0	0.00	NO	TE	\N	0	1	2015	497246	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Doug Martin	0	0	0	1	65	\N	9	0	13.40	TB	RB	\N	13.4	1	2015	2532899	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
DeAndre Hopkins	0	0	0	0	0	\N	69	1	12.90	HOU	WR	\N	12.9	1	2015	2540165	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Latavius Murray	0	0	0	1	56	\N	8	0	12.40	OAK	RB	\N	12.4	1	2015	2541161	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Lions	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	6	1	2015	100010	\N	\N	\N	@SD	2	1	1	0	0	0	24
Bills	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	1	2015	100003	\N	\N	\N	IND	3	1	0	0	0	0	30
Cardinals	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100026	\N	\N	\N	NO	1	1	0	0	0	0	25
Sean Mannion	0	0	0	0	0	\N	0	0	0.00	STL	QB	\N	0	1	2015	2552576	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Anthony McCoy	0	0	0	0	0	\N	0	0	0.00	SEA	TE	\N	0	1	2015	497250	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Louis Murphy	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	1	2015	80670	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Karlos Williams	0	0	0	0	12	\N	2	0	1.40	BUF	RB	\N	1.4	1	2015	2552380	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Matt Jones	0	0	0	0	9	\N	2	0	1.10	WAS	RB	\N	1.1	1	2015	2552635	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Riley Cooper	0	0	0	0	0	\N	8	0	0.80	PHI	WR	\N	0.8	1	2015	497282	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Jameill Showers	0	0	0	0	0	\N	0	0	0.00	DAL	QB	\N	0	1	2015	2553600	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Mohamed Sanu	0	0	0	0	3	\N	45	1	10.80	CIN	WR	\N	10.8	1	2015	2533040	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Bishop Sankey	0	0	0	1	35	\N	7	0	10.20	TEN	RB	\N	10.2	1	2015	2543682	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Jeremy Kerley	0	0	0	0	2	\N	41	0	4.30	NYJ	WR	\N	4.3	1	2015	2495189	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Terrance Williams	0	0	0	0	0	\N	37	0	3.70	DAL	WR	\N	3.7	1	2015	2539205	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Cameron Brate	0	0	0	0	0	\N	0	0	0.00	TB	TE	\N	0	1	2015	2550656	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Cam Newton	210	1	1	1	33	\N	0	0	19.70	CAR	QB	\N	20.7	1	2015	2495455	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Ryan Tannehill	263	2	1	0	19	\N	0	0	18.42	MIA	QB	\N	19.419999999999998	1	2015	2532956	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Joe Flacco	249	2	1	0	4	\N	0	0	16.36	BAL	QB	\N	17.36	1	2015	382	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Jonathan Krause	0	0	0	0	0	\N	0	0	0.00	PHI	WR	\N	0	1	2015	2550216	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Michael Burton	0	0	0	0	0	\N	0	0	0.00	DET	RB	\N	0	1	2015	2552636	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Duron Carter	0	0	0	0	0	\N	0	0	0.00	IND	WR	\N	0	1	2015	2552616	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Akeem Hunt	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	1	2015	2553660	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Damaris Johnson	0	0	0	0	0	\N	0	0	0.00	TEN	WR	\N	0	1	2015	2536031	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Corey Grant	0	0	0	0	0	\N	0	0	0.00	JAC	RB	\N	0	1	2015	2553650	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Todd Gurley	0	0	0	0	0	\N	0	0	0.00	STL	RB	\N	0	1	2015	2552475	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Donte Moncrief	0	0	0	0	2	\N	30	1	9.20	IND	WR	\N	9.2	1	2015	2543614	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Rueben Randle	0	0	0	0	0	\N	49	0	4.90	NYG	WR	\N	4.9	1	2015	2533533	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Michael Campanaro	0	0	0	0	0	\N	6	0	0.60	BAL	WR	\N	0.6	1	2015	2550163	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Derek Anderson	0	0	0	0	0	\N	0	0	0.00	CAR	QB	\N	0	1	2015	2506546	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
James O'Shaughnessy	0	0	0	0	0	\N	0	0	0.00	KC	TE	\N	0	1	2015	2553318	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Bear Pascoe	0	0	0	0	0	\N	0	0	0.00	DET	TE	\N	0	1	2015	79625	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Stedman Bailey	0	0	0	0	1	\N	24	0	2.50	STL	WR	\N	2.5	1	2015	2540157	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Cole Beasley	0	0	0	0	0	\N	23	0	2.30	DAL	WR	\N	2.3	1	2015	2535698	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Timothy Wright	0	0	0	0	0	\N	23	0	2.30	DET	TE	\N	2.3	1	2015	2541768	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
James White	0	0	0	0	9	\N	11	0	2.00	NE	RB	\N	2	1	2015	2543773	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Steelers	\N	\N	\N	\N	\N	\N	\N	\N	1.00	\N	DST	\N	1	1	2015	100027	\N	\N	\N	@NE	2	0	0	0	0	0	30
Brandon Tate	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	1	2015	81306	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Taylor Gabriel	0	0	0	0	0	\N	18	0	1.80	CLE	WR	\N	1.8	1	2015	2550617	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Jacob Tamme	0	0	0	0	0	\N	17	0	1.70	ATL	TE	\N	1.7	1	2015	324	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Chris Hogan	0	0	0	0	0	\N	15	0	1.50	BUF	WR	\N	1.5	1	2015	2530515	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Scott Tolzien	0	0	0	0	0	\N	0	0	0.00	GB	QB	\N	0	1	2015	2495425	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Darren Waller	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	1	2015	2552408	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Blaine Gabbert	0	0	0	0	0	\N	0	0	0.00	SF	QB	\N	0	1	2015	2495441	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Brandon LaFell	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	497302	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Brock Osweiler	0	0	0	0	0	\N	0	0	0.00	DEN	QB	\N	0	1	2015	2533436	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Jay Cutler	224	2	1	0	9	\N	0	0	15.86	CHI	QB	\N	16.86	1	2015	2495824	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Alshon Jeffery	0	0	0	0	2	\N	82	1	14.40	CHI	WR	\N	14.4	1	2015	2533039	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Jordan Cameron	0	0	0	0	0	\N	62	1	12.20	MIA	TE	\N	12.2	1	2015	2495267	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Blake Bortles	193	1	1	0	21	\N	0	0	11.82	JAC	QB	\N	12.819999999999999	1	2015	2543477	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
DeAndre Smelter	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	1	2015	2552627	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Neal Sterling	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	1	2015	2553451	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Thomas Rawls	0	0	0	0	0	\N	0	0	0.00	SEA	RB	\N	0	1	2015	2553733	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Michael Williams	0	0	0	0	0	\N	0	0	0.00	NE	TE	\N	0	1	2015	2539197	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Bennie Fowler	0	0	0	0	0	\N	0	0	0.00	DEN	WR	\N	0	1	2015	2550198	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Jim Dray	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	1	2015	497226	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Mario Alford	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	1	2015	2552650	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Phillip Dorsett	0	0	0	0	0	\N	31	0	3.10	IND	WR	\N	3.1	1	2015	2552424	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Andre Williams	0	0	0	0	24	\N	4	0	2.80	NYG	RB	\N	2.8	1	2015	2543565	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Orleans Darkwa	0	0	0	0	0	\N	0	0	0.00	NYG	RB	\N	0	1	2015	2550481	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Devin Hester	0	0	0	0	0	\N	0	0	0.00	ATL	WR	\N	0	1	2015	2506897	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Frank Gore	0	0	0	1	63	\N	13	0	13.60	IND	RB	\N	13.600000000000001	1	2015	2506404	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
LeSean McCoy	0	0	0	1	62	\N	12	0	13.40	BUF	RB	\N	13.4	1	2015	79607	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Brandon Marshall	0	0	0	0	0	\N	69	1	12.90	NYJ	WR	\N	12.9	1	2015	2495893	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Panthers	\N	\N	\N	\N	\N	\N	\N	\N	8.00	\N	DST	\N	8	1	2015	100004	\N	\N	\N	@JAC	2	2	1	0	0	0	23
Browns	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	4	1	2015	100007	\N	\N	\N	@NYJ	2	1	0	0	0	0	21
Chargers	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100028	\N	\N	\N	DET	1	1	0	0	0	0	23
Marc Mariani	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	1	2015	1037885	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Vance McDonald	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	1	2015	2540215	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Kellen Moore	0	0	0	0	0	\N	0	0	0.00	DAL	QB	\N	0	1	2015	2532917	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Rajion Neal	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	1	2015	2550233	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Brent Celek	0	0	0	0	0	\N	13	0	1.30	PHI	TE	\N	1.3	1	2015	2507218	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Khiry Robinson	0	0	0	0	9	\N	2	0	1.10	NO	RB	\N	1.1	1	2015	2542022	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Gavin Escobar	0	0	0	0	0	\N	8	0	0.80	DAL	TE	\N	0.8	1	2015	2540211	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Tevin Coleman	0	0	0	1	45	\N	6	0	11.10	ATL	RB	\N	11.1	1	2015	2552453	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Nick Foles	207	1	1	0	2	\N	0	0	10.48	STL	QB	\N	11.479999999999999	1	2015	2532842	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Jacquizz Rodgers	0	0	0	0	27	\N	17	0	4.40	CHI	RB	\N	4.4	1	2015	2495471	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Theo Riddick	0	0	0	0	22	\N	16	0	3.80	DET	RB	\N	3.8000000000000003	1	2015	2540020	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Gary Barnidge	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	1	2015	1060	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Brenton Bersin	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	1	2015	2535966	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Quan Bray	0	0	0	0	0	\N	0	0	0.00	IND	WR	\N	0	1	2015	2553467	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Andrew Luck	317	2	1	0	17	\N	0	0	20.38	IND	QB	\N	24.38	1	2015	2533031	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Adrian Peterson	0	0	0	1	109	\N	17	0	18.60	MIN	RB	\N	21.599999999999998	1	2015	2507164	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Andy Dalton	240	2	1	0	10	\N	0	0	16.60	CIN	QB	\N	17.6	1	2015	2495143	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Taiwan Jones	0	0	0	0	0	\N	0	0	0.00	OAK	RB	\N	0	1	2015	2495467	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Jake Kumerow	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	1	2015	2553548	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Zach Line	0	0	0	0	0	\N	0	0	0.00	MIN	RB	\N	0	1	2015	2539303	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Martavis Bryant	0	0	0	0	0	\N	0	0	0.00	PIT	WR	\N	0	1	2015	2543572	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Robert Hughes	0	0	0	0	0	\N	0	0	0.00	ARI	RB	\N	0	1	2015	2530516	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Malcolm Johnson	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	1	2015	2553446	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Charles Clay	0	0	0	0	0	\N	31	1	9.10	BUF	TE	\N	9.1	1	2015	2495139	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Jonathan Stewart	0	0	0	0	45	\N	10	0	5.50	CAR	RB	\N	5.5	1	2015	949	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Danny Woodhead	0	0	0	0	23	\N	25	0	4.80	SD	RB	\N	4.8	1	2015	4327	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Clive Walford	0	0	0	0	0	\N	7	0	0.70	OAK	TE	\N	0.7	1	2015	2552397	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Mike James	0	0	0	0	4	\N	1	0	0.50	TB	RB	\N	0.5	1	2015	2540224	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Chris Owusu	0	0	0	0	0	\N	0	0	0.00	NYJ	WR	\N	0	1	2015	2532923	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Aaron Dobson	0	0	0	0	0	\N	25	0	2.50	NE	WR	\N	2.5	1	2015	2539256	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Jared Cook	0	0	0	0	0	\N	23	0	2.30	STL	TE	\N	2.3	1	2015	71265	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Falcons	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100001	\N	\N	\N	PHI	2	1	0	0	0	0	32
Phillip Supernaw	0	0	0	0	0	\N	0	0	0.00	TEN	TE	\N	0	1	2015	2535535	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Pierre Thomas	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	1	2015	2507141	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Ted Ginn	0	0	0	0	0	\N	16	0	1.60	CAR	WR	\N	1.6	1	2015	2507166	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Stefon Diggs	0	0	0	0	0	\N	14	0	1.40	MIN	WR	\N	1.4	1	2015	2552608	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Will Tye	0	0	0	0	0	\N	0	0	0.00	NYG	TE	\N	0	1	2015	2553830	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Joe Webb	0	0	0	0	0	\N	0	0	0.00	CAR	QB	\N	0	1	2015	1037347	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Tarvaris Jackson	0	0	0	0	0	\N	0	0	0.00	SEA	QB	\N	0	1	2015	2495863	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Matt Moore	0	0	0	0	0	\N	0	0	0.00	MIA	QB	\N	0	1	2015	2507282	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Alfred Morris	0	0	0	1	83	\N	9	0	15.20	WAS	RB	\N	15.200000000000001	1	2015	2533457	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
DeMarco Murray	0	0	0	1	83	\N	19	0	14.20	PHI	RB	\N	14.2	1	2015	2495207	0	0	1	@ATL	\N	\N	\N	\N	\N	\N	\N
Josh McCown	224	1	1	0	11	\N	0	0	12.06	CLE	QB	\N	13.06	1	2015	2505076	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Nelson Agholor	0	0	0	0	0	\N	58	1	11.80	PHI	WR	\N	11.8	1	2015	2552600	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Travis Kelce	0	0	0	0	0	\N	54	1	11.40	KC	TE	\N	11.4	1	2015	2540258	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Alex Smith	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	1	2015	2506410	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
C.J. Spiller	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	1	2015	497204	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Luke Stocker	0	0	0	0	0	\N	0	0	0.00	TB	TE	\N	0	1	2015	2495234	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Darrin Reaves	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	1	2015	2550596	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Seth Roberts	0	0	0	0	0	\N	0	0	0.00	OAK	WR	\N	0	1	2015	2550597	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Nikita Whitlock	0	0	0	0	0	\N	0	0	0.00	NYG	RB	\N	0	1	2015	2550270	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Matt Flynn	0	0	0	0	0	\N	0	0	0.00	NO	QB	\N	0	1	2015	367	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Patrick DiMarco	0	0	0	0	0	\N	0	0	0.00	ATL	RB	\N	0	1	2015	2530763	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Jared Abbrederis	0	0	0	0	0	\N	0	0	0.00	GB	WR	\N	0	1	2015	2543774	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Kennard Backman	0	0	0	0	0	\N	0	0	0.00	GB	TE	\N	0	1	2015	2553292	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Vernon Davis	0	0	0	0	0	\N	33	0	3.30	DEN	TE	\N	3.3	1	2015	2495826	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Andre Roberts	0	0	0	0	1	\N	27	0	2.80	WAS	WR	\N	2.8000000000000003	1	2015	497320	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Demetrius Harris	0	0	0	0	0	\N	0	0	0.00	KC	TE	\N	0	1	2015	2541187	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Stephen Hill	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	1	2015	2533537	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Marques Colston	0	0	0	0	0	\N	74	1	13.40	NO	WR	\N	13.4	1	2015	2495821	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Eric Decker	0	0	0	0	0	\N	68	1	12.80	NYJ	WR	\N	12.8	1	2015	497284	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Bengals	\N	\N	\N	\N	\N	\N	\N	\N	8.00	\N	DST	\N	8	1	2015	100006	\N	\N	\N	@OAK	3	1	1	0	0	0	20
Titans	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	6	1	2015	100012	\N	\N	\N	@TB	2	1	1	0	0	0	26
Cowboys	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100008	\N	\N	\N	NYG	1	1	0	0	0	0	27
Bradley Marquez	0	0	0	0	0	\N	0	0	0.00	STL	WR	\N	0	1	2015	2553725	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Marlon Moore	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	1	2015	2507902	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
David Nelson	0	0	0	0	0	\N	0	0	0.00	PIT	WR	\N	0	1	2015	2507884	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Lance Kendricks	0	0	0	0	0	\N	13	0	1.30	STL	TE	\N	1.3	1	2015	2495187	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Leonard Hankerson	0	0	0	0	0	\N	8	0	0.80	BUF	WR	\N	0.8	1	2015	2495158	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Wes Saxton	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	1	2015	2553682	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Martellus Bennett	0	0	0	0	0	\N	49	1	10.90	CHI	TE	\N	10.9	1	2015	1062	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Malcom Floyd	0	0	0	0	0	\N	45	1	10.50	SD	WR	\N	10.5	1	2015	2505785	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Marcedes Lewis	0	0	0	0	0	\N	44	0	4.40	JAC	TE	\N	4.4	1	2015	2495888	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Jarius Wright	0	0	0	0	6	\N	35	0	4.10	MIN	WR	\N	4.1	1	2015	2532978	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Scott Chandler	0	0	0	0	0	\N	36	0	3.60	NE	TE	\N	3.6	1	2015	2495573	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Kapri Bibbs	0	0	0	0	0	\N	0	0	0.00	DEN	RB	\N	0	1	2015	2550542	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
T.J. Yates	0	0	0	0	0	\N	0	0	0.00	HOU	QB	\N	0	1	2015	2508123	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Matthew Stafford	307	2	1	0	6	\N	0	0	18.88	DET	QB	\N	22.880000000000003	1	2015	79860	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Dez Bryant	0	0	0	0	0	\N	109	1	16.90	DAL	WR	\N	19.9	1	2015	497278	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Kyle Juszczyk	0	0	0	0	0	\N	0	0	0.00	BAL	RB	\N	0	1	2015	2540230	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Brian Leonhardt	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	1	2015	2541512	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Trey Burton	0	0	0	0	0	\N	0	0	0.00	PHI	TE	\N	0	1	2015	2550284	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Brett Hundley	0	0	0	0	0	\N	0	0	0.00	GB	QB	\N	0	1	2015	2552588	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
David Johnson	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	1	2015	79589	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
MarQueis Gray	0	0	0	0	0	\N	0	0	0.00	BUF	TE	\N	0	1	2015	2540259	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Tyrod Taylor	131	1	1	0	18	\N	0	0	9.04	BUF	QB	\N	10.040000000000001	1	2015	2495240	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
DeAngelo Williams	0	0	0	0	48	\N	5	0	5.30	PIT	RB	\N	5.3	1	2015	2495979	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Cecil Shorts	0	0	0	0	0	\N	45	0	4.50	HOU	WR	\N	4.5	1	2015	2495341	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Bernard Pierce	0	0	0	0	5	\N	1	0	0.60	JAC	RB	\N	0.6	1	2015	2533545	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Cedric Peerman	0	0	0	0	4	\N	1	0	0.50	CIN	RB	\N	0.5	1	2015	79629	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Chase Daniel	0	0	0	0	0	\N	0	0	0.00	KC	QB	\N	0	1	2015	81284	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Jamize Olawale	0	0	0	0	0	\N	0	0	0.00	OAK	RB	\N	0	1	2015	2536044	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Brandon Pettigrew	0	0	0	0	0	\N	0	0	0.00	DET	TE	\N	0	1	2015	71431	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Cody Latimer	0	0	0	0	0	\N	24	0	2.40	DEN	WR	\N	2.4	1	2015	2543590	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Anthony Fasano	0	0	0	0	0	\N	23	0	2.30	TEN	TE	\N	2.3	1	2015	2495835	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Ed Dickson	0	0	0	0	0	\N	20	0	2.00	CAR	TE	\N	2	1	2015	497224	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Ravens	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100002	\N	\N	\N	@DEN	2	1	0	0	0	0	31
Lorenzo Taliaferro	0	0	0	0	0	\N	0	0	0.00	BAL	RB	\N	0	1	2015	2543704	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Logan Thomas	0	0	0	0	0	\N	0	0	0.00	MIA	QB	\N	0	1	2015	2543767	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Josh Robinson	0	0	0	0	16	\N	2	0	1.80	IND	RB	\N	1.8	1	2015	2553462	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Devin Smith	0	0	0	0	0	\N	16	0	1.60	NYJ	WR	\N	1.6	1	2015	2553434	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Crockett Gillmore	0	0	0	0	0	\N	14	0	1.40	BAL	TE	\N	1.4	1	2015	2543794	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Bryan Walters	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	1	2015	2528181	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Shaun Hill	0	0	0	0	0	\N	0	0	0.00	MIN	QB	\N	0	1	2015	2504833	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Zach Mettenberger	0	0	0	0	0	\N	0	0	0.00	TEN	QB	\N	0	1	2015	2543605	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Teddy Bridgewater	211	2	1	0	17	\N	0	0	16.14	MIN	QB	\N	17.139999999999997	1	2015	2543465	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Russell Wilson	214	1	1	0	39	\N	0	0	14.46	SEA	QB	\N	15.46	1	2015	2532975	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Amari Cooper	0	0	0	0	0	\N	78	1	13.80	OAK	WR	\N	13.8	1	2015	2552487	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Golden Tate	0	0	0	0	2	\N	57	1	11.90	DET	WR	\N	11.9	1	2015	497326	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Anquan Boldin	0	0	0	0	0	\N	56	1	11.60	SF	WR	\N	11.6	1	2015	2505587	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Matt Simms	0	0	0	0	0	\N	0	0	0.00	ATL	QB	\N	0	1	2015	2534889	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Walt Powell	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	1	2015	2543619	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Konrad Reuland	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	1	2015	2530540	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Wes Welker	0	0	0	0	0	\N	0	0	0.00	STL	WR	\N	0	1	2015	2505790	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Antonio Gates	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	1	2015	2505299	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Geremy Davis	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	1	2015	2552654	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Brandon Weeden	0	0	0	0	0	\N	0	0	0.00	HOU	QB	\N	0	1	2015	2532970	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Antonio Andrews	0	0	0	0	0	\N	0	0	0.00	TEN	RB	\N	0	1	2015	2550474	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Josh Huff	0	0	0	0	0	\N	34	0	3.40	PHI	WR	\N	3.4	1	2015	2543746	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Justin Hunter	0	0	0	0	0	\N	29	0	2.90	TEN	WR	\N	2.9	1	2015	2540151	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Jason Avant	0	0	0	0	0	\N	26	0	2.60	KC	WR	\N	2.6	1	2015	2495802	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
David Cobb	0	0	0	0	0	\N	0	0	0.00	TEN	RB	\N	0	1	2015	2552376	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Justice Cunningham	0	0	0	0	0	\N	0	0	0.00	STL	TE	\N	0	1	2015	2540257	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Cooper Helfet	0	0	0	0	0	\N	0	0	0.00	SEA	TE	\N	0	1	2015	2536071	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Darrius Heyward-Bey	0	0	0	0	0	\N	0	0	0.00	PIT	WR	\N	0	1	2015	80427	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Lamar Miller	0	0	0	1	62	\N	13	0	13.50	MIA	RB	\N	13.5	1	2015	2533034	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Julian Edelman	0	0	0	0	2	\N	68	1	13.00	NE	WR	\N	13	1	2015	238498	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Jeremy Maclin	0	0	0	0	0	\N	64	1	12.40	KC	WR	\N	12.4	1	2015	80429	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Jets	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	6	1	2015	100024	\N	\N	\N	CLE	3	1	0	0	0	0	17
Seahawks	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	1	2015	100030	\N	\N	\N	@STL	2	1	0	0	0	0	17
49ers	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100029	\N	\N	\N	MIN	1	1	0	0	0	0	25
Vince Mayle	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	1	2015	2552431	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Bruce Miller	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	1	2015	2495314	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Keith Mumphery	0	0	0	0	0	\N	0	0	0.00	HOU	WR	\N	0	1	2015	2552420	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Clay Harbor	0	0	0	0	0	\N	13	0	1.30	JAC	TE	\N	1.3	1	2015	497242	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Chris Conley	0	0	0	0	0	\N	8	0	0.80	KC	WR	\N	0.8	1	2015	2552652	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Dion Sims	0	0	0	0	0	\N	8	0	0.80	MIA	TE	\N	0.8	1	2015	2540200	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Dorial Green-Beckham	0	0	0	0	0	\N	48	1	10.80	TEN	WR	\N	10.8	1	2015	2552491	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Tyler Eifert	0	0	0	0	0	\N	42	1	10.20	CIN	TE	\N	10.2	1	2015	2540148	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Marvin Jones	0	0	0	0	0	\N	43	0	4.30	CIN	WR	\N	4.3	1	2015	2532884	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Robert Woods	0	0	0	0	0	\N	38	0	3.80	BUF	WR	\N	3.8	1	2015	2540169	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Kenny Bell	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	1	2015	2552421	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Nick Boyle	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	1	2015	2552402	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Aaron Rodgers	361	3	1	0	18	\N	0	0	26.24	GB	QB	\N	30.24	1	2015	2506363	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Odell Beckham	0	0	0	0	5	\N	110	1	17.50	NYG	WR	\N	20.5	1	2015	2543496	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Antonio Brown	0	0	0	0	2	\N	100	1	16.20	PIT	WR	\N	19.2	1	2015	2508061	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Malcolm Brown	0	0	0	0	0	\N	0	0	0.00	STL	RB	\N	0	1	2015	2552382	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Derek Carrier	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	1	2015	2534241	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Nic Jacobs	0	0	0	0	0	\N	0	0	0.00	JAC	TE	\N	0	1	2015	2550355	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Bruce Gradkowski	0	0	0	0	0	\N	0	0	0.00	PIT	QB	\N	0	1	2015	2495838	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Marion Grice	0	0	0	0	0	\N	0	0	0.00	ARI	RB	\N	0	1	2015	2543690	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Owen Daniels	0	0	0	0	0	\N	31	1	9.10	DEN	TE	\N	9.1	1	2015	2495825	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
David Johnson	0	0	0	0	38	\N	18	0	5.60	ARI	RB	\N	5.6	1	2015	2553435	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Marqise Lee	0	0	0	0	1	\N	47	0	4.80	JAC	WR	\N	4.8	1	2015	2543475	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Jordan Todman	0	0	0	0	4	\N	3	0	0.70	PIT	RB	\N	0.7	1	2015	2495472	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Matt Barkley	0	0	0	0	0	\N	0	0	0.00	ARI	QB	\N	0	1	2015	2539308	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Quinton Patton	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	1	2015	2539250	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Dwayne Allen	0	0	0	0	0	\N	24	0	2.40	IND	TE	\N	2.4	1	2015	2533046	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Rob Housler	0	0	0	0	0	\N	21	0	2.10	CHI	TE	\N	2.1	1	2015	2508112	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Cowboys	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	1	2015	100008	\N	\N	\N	NYG	1	1	0	0	0	0	27
Zach Sudfeld	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	1	2015	2541151	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Fred Jackson	0	0	0	0	8	\N	9	0	1.70	SEA	RB	\N	1.7000000000000002	1	2015	2506871	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Bryce Brown	0	0	0	0	13	\N	2	0	1.50	SEA	RB	\N	1.5	1	2015	2534796	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Tyler Varga	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	1	2015	2552578	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Trey Watts	0	0	0	0	0	\N	0	0	0.00	STL	RB	\N	0	1	2015	2550448	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Chad Henne	0	0	0	0	0	\N	0	0	0.00	JAC	QB	\N	0	1	2015	252	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Matt McGloin	0	0	0	0	0	\N	0	0	0.00	OAK	QB	\N	0	1	2015	2542065	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Demaryius Thomas	0	0	0	0	0	\N	102	1	16.20	DEN	WR	\N	19.2	1	2015	497328	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Jameis Winston	196	2	1	0	7	\N	0	0	14.54	TB	QB	\N	15.54	1	2015	2552033	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Jordan Matthews	0	0	0	0	0	\N	79	1	13.90	PHI	WR	\N	13.9	1	2015	2543500	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Jarvis Landry	0	0	0	0	2	\N	57	1	11.90	MIA	WR	\N	11.9	1	2015	2543488	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Michael Crabtree	0	0	0	0	0	\N	57	1	11.70	OAK	WR	\N	11.7	1	2015	71269	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Vincent Jackson	0	0	0	0	0	\N	53	1	11.30	TB	WR	\N	11.3	1	2015	2506400	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Rod Smith	0	0	0	0	0	\N	0	0	0.00	DAL	RB	\N	0	1	2015	2553743	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Jay Prosch	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	1	2015	2543695	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Lucky Whitehead	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	1	2015	2553621	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Glenn Winston	0	0	0	0	0	\N	0	0	0.00	CLE	RB	\N	0	1	2015	2551357	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Corey Fuller	0	0	0	0	0	\N	0	0	0.00	DET	WR	\N	0	1	2015	2540261	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Brittan Golden	0	0	0	0	0	\N	0	0	0.00	ARI	WR	\N	0	1	2015	2535988	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Mike Davis	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	1	2015	2553439	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Shaun Draughn	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	1	2015	2495461	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Charlie Whitehurst	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	1	2015	2495974	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Terrance West	0	0	0	0	29	\N	5	0	3.40	BAL	RB	\N	3.4	1	2015	2543664	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Mychal Rivera	0	0	0	0	0	\N	29	0	2.90	OAK	TE	\N	2.9	1	2015	2539995	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Kaelin Clay	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	1	2015	2552651	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
B.J. Daniels	0	0	0	0	0	\N	0	0	0.00	HOU	WR	\N	0	1	2015	2541429	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Mitchell Henry	0	0	0	0	0	\N	0	0	0.00	GB	TE	\N	0	1	2015	2553773	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Tim Hightower	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	1	2015	4383	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Darren Sproles	0	0	0	1	32	\N	42	0	13.40	PHI	RB	\N	13.4	1	2015	2506467	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
T.J. Yeldon	0	0	0	1	70	\N	14	0	12.40	JAC	RB	\N	12.4	1	2015	2552471	0	0	1	CAR	\N	\N	\N	\N	\N	\N	\N
Broncos	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	1	2015	100009	\N	\N	\N	BAL	2	1	0	0	0	0	22
Tre McBride	0	0	0	0	0	\N	0	0	0.00	TEN	WR	\N	0	1	2015	2552428	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Raheem Mostert	0	0	0	0	0	\N	0	0	0.00	CLE	RB	\N	0	1	2015	2553728	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Jamison Crowder	0	0	0	0	1	\N	12	0	1.30	WAS	WR	\N	1.3	1	2015	2552415	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Cordarrelle Patterson	0	0	0	0	3	\N	6	0	0.90	MIN	WR	\N	0.9	1	2015	2540145	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Jeremy Ross	0	0	0	0	0	\N	0	0	0.00	OAK	WR	\N	0	1	2015	2530598	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Tom Savage	0	0	0	0	0	\N	0	0	0.00	HOU	QB	\N	0	1	2015	2543640	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
De'Anthony Thomas	0	0	0	0	24	\N	27	1	11.10	KC	RB	\N	11.1	1	2015	2543638	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Brandon Bolden	0	0	0	1	33	\N	11	0	10.40	NE	RB	\N	10.4	1	2015	2532797	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Joique Bell	0	0	0	0	33	\N	9	0	4.20	DET	RB	\N	4.2	1	2015	497145	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Ronnie Hillman	0	0	0	0	24	\N	12	0	3.60	DEN	RB	\N	3.6	1	2015	2533437	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Justin Blackmon	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	1	2015	2533038	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Peyton Manning	362	3	1	0	-1	\N	0	0	24.38	DEN	QB	\N	28.38	1	2015	2501863	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
A.J. Green	0	0	0	0	2	\N	112	1	17.40	CIN	WR	\N	20.4	1	2015	2495450	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Landry Jones	0	0	0	0	0	\N	0	0	0.00	PIT	QB	\N	0	1	2015	2539287	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Jorvorskie Lane	0	0	0	0	0	\N	0	0	0.00	TB	RB	\N	0	1	2015	2536532	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Mack Brown	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	1	2015	2553867	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Garrett Celek	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	1	2015	2534820	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Jesse James	0	0	0	0	0	\N	0	0	0.00	PIT	TE	\N	0	1	2015	2552633	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Josh Johnson	0	0	0	0	0	\N	0	0	0.00	BUF	QB	\N	0	1	2015	264	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Rashad Greene	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	1	2015	2552425	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Maxx Williams	0	0	0	0	0	\N	32	1	9.20	BAL	TE	\N	9.2	1	2015	2552468	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Carlos Hyde	0	0	0	0	46	\N	7	0	5.30	SF	RB	\N	5.3	1	2015	2543743	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Roy Helu	0	0	0	0	23	\N	23	0	4.60	OAK	RB	\N	4.6	1	2015	2495161	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Rex Burkhead	0	0	0	0	4	\N	2	0	0.60	CIN	RB	\N	0.6000000000000001	1	2015	2539265	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Juwan Thompson	0	0	0	0	4	\N	1	0	0.50	DEN	RB	\N	0.5	1	2015	2550263	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Austin Davis	0	0	0	0	0	\N	0	0	0.00	CLE	QB	\N	0	1	2015	2533349	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Logan Paulsen	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	1	2015	2507828	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Ka'Deem Carey	0	0	0	0	17	\N	6	0	2.30	CHI	RB	\N	2.3	1	2015	2543551	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Jerick McKinnon	0	0	0	0	14	\N	8	0	2.20	MIN	RB	\N	2.2	1	2015	2543715	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Marquess Wilson	0	0	0	0	0	\N	20	0	2.00	CHI	WR	\N	2	1	2015	2540177	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Bears	\N	\N	\N	\N	\N	\N	\N	\N	0.00	\N	DST	\N	0	1	2015	100005	\N	\N	\N	GB	2	1	0	0	0	0	39
Geoff Swaim	0	0	0	0	0	\N	0	0	0.00	DAL	TE	\N	0	1	2015	2553454	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Julius Thomas	0	0	0	0	0	\N	0	0	0.00	JAC	TE	\N	0	1	2015	2495353	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Tyler Lockett	0	0	0	0	1	\N	17	0	1.80	SEA	WR	\N	1.8	1	2015	2552430	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Andre Holmes	0	0	0	0	0	\N	16	0	1.60	OAK	WR	\N	1.6	1	2015	2495453	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Luke Willson	0	0	0	0	0	\N	15	0	1.50	SEA	TE	\N	1.5	1	2015	2541199	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Brian Tyms	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	2535522	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Tony Washington	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	1	2015	2550415	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Robert Griffin III	0	0	0	0	0	\N	0	0	0.00	WAS	QB	\N	0	1	2015	2533033	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Luke McCown	0	0	0	0	0	\N	0	0	0.00	NO	QB	\N	0	1	2015	2506053	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Jamaal Charles	0	0	0	1	76	\N	26	0	16.20	KC	RB	\N	16.2	1	2015	925	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Emmanuel Sanders	0	0	0	0	2	\N	86	1	14.80	DEN	WR	\N	14.8	1	2015	497322	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Keenan Allen	0	0	0	0	0	\N	79	1	13.90	SD	WR	\N	13.9	1	2015	2540154	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Kenny Stills	0	0	0	0	0	\N	62	1	12.20	MIA	WR	\N	12.2	1	2015	2540202	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Pierre Garcon	0	0	0	0	0	\N	55	1	11.50	WAS	WR	\N	11.5	1	2015	2346	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Willie Snead	0	0	0	0	0	\N	0	0	0.00	NO	WR	\N	0	1	2015	2550256	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Terrelle Pryor	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	1	2015	2531332	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Aaron Ripkowski	0	0	0	0	0	\N	0	0	0.00	GB	RB	\N	0	1	2015	2552477	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Kerwynn Williams	0	0	0	0	0	\N	0	0	0.00	ARI	RB	\N	0	1	2015	2539980	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Quincy Enunwa	0	0	0	0	0	\N	0	0	0.00	NYJ	WR	\N	0	1	2015	2543828	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Tyler Gaffney	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	1	2015	2543655	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Josh Gordon	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	1	2015	2537931	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Dorin Dickerson	0	0	0	0	0	\N	0	0	0.00	TEN	TE	\N	0	1	2015	497222	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Matt Schaub	0	0	0	0	0	\N	0	0	0.00	BAL	QB	\N	0	1	2015	2505982	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Seyi Ajirotutu	0	0	0	0	0	\N	0	0	0.00	PHI	WR	\N	0	1	2015	497260	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Greg Jennings	0	0	0	0	0	\N	35	0	3.50	MIA	WR	\N	3.5	1	2015	2495867	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Duke Johnson	0	0	0	0	20	\N	10	0	3.00	CLE	RB	\N	3	1	2015	2552461	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Gerald Christian	0	0	0	0	0	\N	0	0	0.00	ARI	TE	\N	0	1	2015	2552462	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
DuJuan Harris	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	1	2015	2530690	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Kenny Hilliard	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	1	2015	2552476	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Markus Wheaton	0	0	0	0	2	\N	72	1	13.40	PIT	WR	\N	13.4	1	2015	2539291	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Rams	\N	\N	\N	\N	\N	\N	\N	\N	8.00	\N	DST	\N	8	1	2015	100017	\N	\N	\N	SEA	4	1	1	0	0	0	21
Buccaneers	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	3	1	2015	100031	\N	\N	\N	TEN	1	1	0	0	0	0	21
AJ McCarron	0	0	0	0	0	\N	0	0	0.00	CIN	QB	\N	0	1	2015	2543497	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Aaron Murray	0	0	0	0	0	\N	0	0	0.00	KC	QB	\N	0	1	2015	2543587	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Marcel Reece	0	0	0	0	4	\N	6	0	1.00	OAK	RB	\N	1	1	2015	4433	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Rashad Ross	0	0	0	0	0	\N	0	0	0.00	WAS	WR	\N	0	1	2015	2541940	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Anthony Sherman	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	1	2015	2495340	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Davante Adams	0	0	0	0	0	\N	46	1	10.60	GB	WR	\N	10.6	1	2015	2543495	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Jordan Reed	0	0	0	0	0	\N	37	1	9.70	WAS	TE	\N	9.7	1	2015	2540160	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Fozzy Whittaker	0	0	0	0	30	\N	14	0	4.40	CAR	RB	\N	4.4	1	2015	2540037	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Ameer Abdullah	0	0	0	0	28	\N	12	0	4.00	DET	RB	\N	4	1	2015	2552374	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Blake Bell	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	1	2015	2552478	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Ahmad Bradshaw	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	1	2015	2495560	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Darrel Young	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	1	2015	2507737	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Drew Brees	314	2	1	0	3	\N	0	0	18.86	NO	QB	\N	22.860000000000003	1	2015	2504775	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Seantavius Jones	0	0	0	0	0	\N	0	0	0.00	NO	WR	\N	0	1	2015	2550359	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Thad Lewis	0	0	0	0	0	\N	0	0	0.00	PHI	QB	\N	0	1	2015	497121	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Jeremy Butler	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	1	2015	2550183	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Joey Iosefa	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	1	2015	2552634	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Gus Johnson	0	0	0	0	0	\N	0	0	0.00	ATL	RB	\N	0	1	2015	2553662	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Garrett Grayson	0	0	0	0	0	\N	0	0	0.00	NO	QB	\N	0	1	2015	2552371	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Frankie Hammond	0	0	0	0	0	\N	0	0	0.00	KC	WR	\N	0	1	2015	2541828	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Coby Fleener	0	0	0	0	0	\N	28	1	8.80	IND	TE	\N	8.8	1	2015	2532838	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Jermaine Kearse	0	0	0	0	2	\N	50	0	5.20	SEA	WR	\N	5.2	1	2015	2532887	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Donald Brown	0	0	0	0	4	\N	3	0	0.70	SD	RB	\N	0.7	1	2015	2507779	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
MyCole Pruitt	0	0	0	0	0	\N	6	0	0.60	MIN	TE	\N	0.6	1	2015	2552401	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Justin Perillo	0	0	0	0	0	\N	0	0	0.00	GB	TE	\N	0	1	2015	2550243	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Garrett Graham	0	0	0	0	0	\N	21	0	2.10	HOU	TE	\N	2.1	1	2015	497234	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Reggie Bush	0	0	0	0	26	\N	13	0	1.90	SF	RB	\N	1.9000000000000004	1	2015	2506874	0	0	1	MIN	\N	\N	\N	\N	\N	\N	\N
Devin Street	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	1	2015	2543641	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Adam Thielen	0	0	0	0	0	\N	0	0	0.00	MIN	WR	\N	0	1	2015	2541785	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Deonte Thompson	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	1	2015	2534436	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Ricardo Lockette	0	0	0	0	1	\N	16	0	1.70	SEA	WR	\N	1.7000000000000002	1	2015	2495306	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Cameron Artis-Payne	0	0	0	0	11	\N	4	0	1.50	CAR	RB	\N	1.5	1	2015	2552375	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
C.J. Uzomah	0	0	0	0	0	\N	0	0	0.00	CIN	TE	\N	0	1	2015	2552559	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Terrell Watson	0	0	0	0	0	\N	0	0	0.00	CIN	RB	\N	0	1	2015	2553617	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Matt Hasselbeck	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	1	2015	2501078	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Colt McCoy	0	0	0	0	0	\N	0	0	0.00	WAS	QB	\N	0	1	2015	497123	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Randall Cobb	0	0	0	0	7	\N	95	1	16.20	GB	WR	\N	16.2	1	2015	2495448	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Rashad Jennings	0	0	0	1	66	\N	20	0	14.60	NYG	RB	\N	14.6	1	2015	71345	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Rob Gronkowski	0	0	0	0	0	\N	79	1	13.90	NE	TE	\N	13.9	1	2015	497240	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Justin Forsett	0	0	0	1	48	\N	13	0	12.10	BAL	RB	\N	12.100000000000001	1	2015	927	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Harry Douglas	0	0	0	0	0	\N	58	1	11.80	TEN	WR	\N	11.8	1	2015	222	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Brian Hoyer	226	1	1	0	3	\N	0	0	11.34	HOU	QB	\N	12.34	1	2015	81294	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Kevin Smith	0	0	0	0	0	\N	0	0	0.00	SEA	WR	\N	0	1	2015	2550733	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
John Phillips	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	1	2015	71435	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Sean Renfree	0	0	0	0	0	\N	0	0	0.00	ATL	QB	\N	0	1	2015	2540263	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
DeAndrew White	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	1	2015	2552657	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
George Winn	0	0	0	0	0	\N	0	0	0.00	DET	RB	\N	0	1	2015	2541910	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Jalston Fowler	0	0	0	0	0	\N	0	0	0.00	TEN	RB	\N	0	1	2015	2552464	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Marquise Goodwin	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	1	2015	2539964	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Drew Stanton	0	0	0	0	0	\N	0	0	0.00	ARI	QB	\N	0	1	2015	2495748	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
RaShaun Allen	0	0	0	0	0	\N	0	0	0.00	NO	TE	\N	0	1	2015	2550608	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Bobby Rainey	0	0	0	0	19	\N	16	0	3.50	TB	RB	\N	3.5	1	2015	2534571	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Brian Quick	0	0	0	0	0	\N	30	0	3.00	STL	WR	\N	3	1	2015	2532933	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Toby Gerhart	0	0	0	0	20	\N	7	0	2.70	JAC	RB	\N	2.7	1	2015	497176	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Asante Cleveland	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	1	2015	2550454	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
John Crockett	0	0	0	0	0	\N	0	0	0.00	GB	RB	\N	0	1	2015	2553511	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Dan Herron	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	1	2015	2532864	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Larry Fitzgerald	0	0	0	0	0	\N	75	1	13.50	ARI	WR	\N	13.5	1	2015	2506106	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Marshawn Lynch	0	0	0	1	60	\N	13	0	13.30	SEA	RB	\N	13.3	1	2015	2495663	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Allen Robinson	0	0	0	0	0	\N	67	1	12.70	JAC	WR	\N	12.7	1	2015	2543509	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Vikings	\N	\N	\N	\N	\N	\N	\N	\N	8.00	\N	DST	\N	8	1	2015	100020	\N	\N	\N	@SF	3	1	1	0	0	0	20
Colts	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	1	2015	100014	\N	\N	\N	@BUF	2	1	0	0	0	0	17
Packers	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	1	2015	100011	\N	\N	\N	@CHI	2	1	0	0	0	0	24
Chris Matthews	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	1	2015	2531049	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Ifeanyi Momah	0	0	0	0	0	\N	0	0	0.00	ARI	TE	\N	0	1	2015	2541096	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Ryan Nassib	0	0	0	0	0	\N	0	0	0.00	NYG	QB	\N	0	1	2015	2539961	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Troy Niklas	0	0	0	0	0	\N	0	0	0.00	ARI	TE	\N	0	1	2015	2543628	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
C.J. Fiedorowicz	0	0	0	0	0	\N	12	0	1.20	HOU	TE	\N	1.2	1	2015	2543721	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Albert Wilson	0	0	0	0	0	\N	9	0	0.90	KC	WR	\N	0.9	1	2015	2550272	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Justin Hardy	0	0	0	0	0	\N	8	0	0.80	ATL	WR	\N	0.8	1	2015	2552418	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Steve Smith	0	0	0	0	0	\N	49	1	10.90	BAL	WR	\N	10.9	1	2015	2504595	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Richard Rodgers	0	0	0	0	0	\N	44	1	10.40	GB	TE	\N	10.4	1	2015	2550313	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Eddie Royal	0	0	0	0	0	\N	36	1	9.60	CHI	WR	\N	9.6	1	2015	1990	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Corey Brown	0	0	0	0	5	\N	37	0	4.20	CAR	WR	\N	4.2	1	2015	2550546	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Jeremy Langford	0	0	0	0	25	\N	14	0	3.90	CHI	RB	\N	3.9	1	2015	2552379	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Chris Polk	0	0	0	0	26	\N	10	0	3.60	HOU	RB	\N	3.6	1	2015	2533037	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Le'Veon Bell	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	1	2015	2540175	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Tommy Bohanon	0	0	0	0	0	\N	0	0	0.00	NYJ	RB	\N	0	1	2015	2540225	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Zach Zenner	0	0	0	0	0	\N	0	0	0.00	DET	RB	\N	0	1	2015	2553631	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Matt Forte	0	0	0	1	90	\N	45	0	19.50	CHI	RB	\N	19.5	1	2015	234	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Sam Bradford	269	2	1	0	4	\N	0	0	17.16	PHI	QB	\N	18.159999999999997	1	2015	497095	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Will Johnson	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	1	2015	163982	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Ben Koyack	0	0	0	0	0	\N	0	0	0.00	JAC	TE	\N	0	1	2015	2552396	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Josh Lenz	0	0	0	0	0	\N	0	0	0.00	HOU	WR	\N	0	1	2015	2541853	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Adam Humphries	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	1	2015	2553895	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Darius Jennings	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	1	2015	2553896	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Chris Gragg	0	0	0	0	0	\N	0	0	0.00	BUF	TE	\N	0	1	2015	2539202	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Xavier Grimble	0	0	0	0	0	\N	0	0	0.00	PIT	TE	\N	0	1	2015	2550521	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Chris Harper	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	2553652	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Kirk Cousins	142	1	1	0	4	\N	0	0	8.08	WAS	QB	\N	9.08	1	2015	2532820	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Breshad Perriman	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	1	2015	2552597	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Branden Oliver	0	0	0	0	11	\N	10	0	2.10	SD	RB	\N	2.1	1	2015	2550658	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Jake Stoneburner	0	0	0	0	0	\N	0	0	0.00	MIA	TE	\N	0	1	2015	2539283	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Randall Telfer	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	1	2015	2552660	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Chris Thompson	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	1	2015	2540011	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Dion Lewis	0	0	0	0	14	\N	3	0	1.70	NE	RB	\N	1.7	1	2015	2495469	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Benjamin Watson	0	0	0	0	0	\N	16	0	1.60	NO	TE	\N	1.6	1	2015	2506122	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Bilal Powell	0	0	0	0	9	\N	5	0	1.40	NYJ	RB	\N	1.4	1	2015	2495328	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Robert Turbin	0	0	0	0	0	\N	0	0	0.00	DAL	RB	\N	0	1	2015	2533460	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Corey Washington	0	0	0	0	0	\N	0	0	0.00	DET	WR	\N	0	1	2015	2550413	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Jonas Gray	0	0	0	0	0	\N	0	0	0.00	JAC	RB	\N	0	1	2015	2533030	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Johnny Manziel	0	0	0	0	0	\N	0	0	0.00	CLE	QB	\N	0	1	2015	2543462	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Stevan Ridley	0	0	0	0	0	\N	0	0	0.00	NYJ	RB	\N	0	1	2015	2495470	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Ryan Fitzpatrick	206	2	1	0	8	\N	0	0	15.04	NYJ	QB	\N	16.040000000000003	1	2015	2506581	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
T.Y. Hilton	0	0	0	0	2	\N	78	1	14.00	IND	WR	\N	14	1	2015	2532865	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Roddy White	0	0	0	0	0	\N	59	1	11.90	ATL	WR	\N	11.9	1	2015	2506366	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Larry Donnell	0	0	0	0	0	\N	56	1	11.60	NYG	TE	\N	11.6	1	2015	2537933	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Jerome Simpson	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	1	2015	318	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Matt Spaeth	0	0	0	0	0	\N	0	0	0.00	PIT	TE	\N	0	1	2015	2507182	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Craig Stevens	0	0	0	0	0	\N	0	0	0.00	TEN	TE	\N	0	1	2015	1992	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Allen Reisner	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	1	2015	2495437	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Charcandrick West	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	1	2015	2550268	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Kyle Williams	0	0	0	0	0	\N	0	0	0.00	DEN	WR	\N	0	1	2015	2508027	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
David Fales	0	0	0	0	0	\N	0	0	0.00	CHI	QB	\N	0	1	2015	2543751	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Arian Foster	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	1	2015	79555	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Andre Debose	0	0	0	0	0	\N	0	0	0.00	OAK	WR	\N	0	1	2015	2553453	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Donteea Dye	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	1	2015	2553876	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Jay Ajayi	0	0	0	0	0	\N	0	0	0.00	MIA	RB	\N	0	1	2015	2552582	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Allen Hurns	0	0	0	0	0	\N	28	0	2.80	JAC	WR	\N	2.8	1	2015	2550353	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Chase Coffman	0	0	0	0	0	\N	0	0	0.00	SEA	TE	\N	0	1	2015	71255	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Javontee Herndon	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	1	2015	2550459	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Mike Wallace	0	0	0	0	2	\N	74	1	13.60	MIN	WR	\N	13.600000000000001	1	2015	2507763	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
Colin Kaepernick	187	1	1	0	33	\N	0	0	12.78	SF	QB	\N	13.780000000000001	1	2015	2495186	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Texans	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	6	1	2015	100013	\N	\N	\N	KC	2	1	1	0	0	0	24
Keshawn Martin	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	2532900	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Tony Moeaki	0	0	0	0	0	\N	0	0	0.00	ATL	TE	\N	0	1	2015	497256	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Marcus Murphy	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	1	2015	2552647	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Hakeem Nicks	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	1	2015	80672	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Jeff Janis	0	0	0	0	0	\N	10	0	1.00	GB	WR	\N	1	1	2015	2543750	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Greg Salas	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	1	2015	2495223	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Russell Shepard	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	1	2015	2541944	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Jason Witten	0	0	0	0	0	\N	50	1	11.00	DAL	TE	\N	11	1	2015	2505629	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Marcus Mariota	167	1	1	0	20	\N	0	0	10.68	TEN	QB	\N	11.68	1	2015	2552466	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Percy Harvin	0	0	0	0	6	\N	34	1	10.00	BUF	WR	\N	10	1	2015	80425	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
DeSean Jackson	0	0	0	0	2	\N	41	0	4.30	WAS	WR	\N	4.3	1	2015	1581	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Devonta Freeman	0	0	0	0	28	\N	10	0	3.80	ATL	RB	\N	3.8	1	2015	2543583	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Chris Givens	0	0	0	0	0	\N	35	0	3.50	BAL	WR	\N	3.5	1	2015	2535634	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
E.J. Bibbs	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	1	2015	2553791	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
James Wright	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	1	2015	2550162	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Carson Palmer	351	2	1	0	4	\N	0	0	20.44	ARI	QB	\N	24.439999999999998	1	2015	2505245	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Ben Roethlisberger	290	2	1	0	2	\N	0	0	17.80	PIT	QB	\N	18.8	1	2015	2506109	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Julio Jones	0	0	0	0	1	\N	102	1	16.30	ATL	WR	\N	19.299999999999997	1	2015	2495454	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Tavarres King	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	1	2015	2539235	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Dezmin Lewis	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	1	2015	2552411	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Brice Butler	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	1	2015	2541388	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Dontrelle Inman	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	1	2015	2530700	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Marcel Jensen	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	1	2015	2550356	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Virgil Green	0	0	0	0	0	\N	0	0	0.00	DEN	TE	\N	0	1	2015	2495288	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Eric Ebron	0	0	0	0	0	\N	32	1	9.20	DET	TE	\N	9.2	1	2015	2543466	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Jaelen Strong	0	0	0	0	0	\N	61	0	6.10	HOU	WR	\N	6.1	1	2015	2552463	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Lance Moore	0	0	0	0	0	\N	7	0	0.70	DET	WR	\N	0.7	1	2015	2506287	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Jonathan Grimes	0	0	0	0	4	\N	1	0	0.50	HOU	RB	\N	0.5	1	2015	2534770	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Jimmy Clausen	0	0	0	0	0	\N	0	0	0.00	BAL	QB	\N	0	1	2015	497108	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
James Starks	0	0	0	0	18	\N	7	0	2.50	GB	RB	\N	2.5	1	2015	497206	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Jeff Cumberland	0	0	0	0	0	\N	20	0	2.00	NYJ	TE	\N	2	1	2015	2507883	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Giants	\N	\N	\N	\N	\N	\N	\N	\N	0.00	\N	DST	\N	0	1	2015	100023	\N	\N	\N	@DAL	1	0	0	0	0	0	29
Jordan Taylor	0	0	0	0	0	\N	0	0	0.00	DEN	WR	\N	0	1	2015	2553606	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Christine Michael	0	0	0	0	16	\N	2	0	1.80	SEA	RB	\N	1.8	1	2015	2539322	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
DeVante Parker	0	0	0	0	0	\N	16	0	1.60	MIA	WR	\N	1.6	1	2015	2552409	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Bruce Ellington	0	0	0	0	1	\N	13	0	1.40	SF	WR	\N	1.4000000000000001	1	2015	2543646	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Fitzgerald Toussaint	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	1	2015	2550738	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Spencer Ware	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	1	2015	2540204	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Mike Glennon	0	0	0	0	0	\N	0	0	0.00	TB	QB	\N	0	1	2015	2539275	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
EJ Manuel	0	0	0	0	0	\N	0	0	0.00	BUF	QB	\N	0	1	2015	2539228	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Dennis Pitta	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	1	2015	497254	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Calvin Johnson	0	0	0	0	0	\N	90	1	15.00	DET	WR	\N	15	1	2015	2495647	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Mike Evans	0	0	0	0	0	\N	81	1	14.10	TB	WR	\N	14.1	1	2015	2543468	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Torrey Smith	0	0	0	0	0	\N	60	1	12.00	SF	WR	\N	12	1	2015	2495459	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Kyle Rudolph	0	0	0	0	0	\N	58	1	11.80	MIN	TE	\N	11.8	1	2015	2495438	0	0	0	@SF	\N	\N	\N	\N	\N	\N	\N
John Brown	0	0	0	0	0	\N	53	1	11.30	ARI	WR	\N	11.3	1	2015	2543847	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Lee Smith	0	0	0	0	0	\N	0	0	0.00	OAK	TE	\N	0	1	2015	2495347	0	0	0	CIN	\N	\N	\N	\N	\N	\N	\N
Silas Redd	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	1	2015	2550529	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Brandon Wegher	0	0	0	0	0	\N	0	0	0.00	CAR	RB	\N	0	1	2015	2553618	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Brandon Williams	0	0	0	0	0	\N	0	0	0.00	MIA	TE	\N	0	1	2015	2539645	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Trey Williams	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	1	2015	2553486	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Daniel Fells	0	0	0	0	0	\N	0	0	0.00	NYG	TE	\N	0	1	2015	2506619	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Kendall Gaskins	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	1	2015	2541778	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
A.J. Derby	0	0	0	0	0	\N	0	0	0.00	NE	TE	\N	0	1	2015	2552580	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Mike Vick	0	0	0	0	0	\N	0	0	0.00	PIT	QB	\N	0	1	2015	2504531	0	0	0	@NE	\N	\N	\N	\N	\N	\N	\N
Jace Amaro	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	1	2015	2543481	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Montee Ball	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	1	2015	2539341	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Lance Dunbar	0	0	0	0	28	\N	3	0	3.10	DAL	RB	\N	3.1	1	2015	2535708	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Andre Caldwell	0	0	0	0	0	\N	29	0	2.90	DEN	WR	\N	2.9	1	2015	206	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Ryan Griffin	0	0	0	0	0	\N	27	0	2.70	HOU	TE	\N	2.7	1	2015	2541316	0	0	0	KC	\N	\N	\N	\N	\N	\N	\N
Tyler Clutts	0	0	0	0	0	\N	0	0	0.00	DAL	RB	\N	0	1	2015	2508096	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Victor Cruz	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	1	2015	2507855	0	0	0	@DAL	\N	\N	\N	\N	\N	\N	\N
Jarryd Hayne	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	1	2015	2553117	0	0	0	MIN	\N	\N	\N	\N	\N	\N	\N
Jeff Heuerman	0	0	0	0	0	\N	0	0	0.00	DEN	TE	\N	0	1	2015	2552399	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Jimmy Graham	0	0	0	0	0	\N	75	1	13.50	SEA	TE	\N	13.5	1	2015	497236	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
Devin Funchess	0	0	0	0	0	\N	72	1	13.20	CAR	WR	\N	13.2	1	2015	2552458	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Zach Ertz	0	0	0	0	0	\N	66	1	12.60	PHI	TE	\N	12.6	1	2015	2540158	0	0	0	@ATL	\N	\N	\N	\N	\N	\N	\N
Chiefs	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	6	1	2015	100016	\N	\N	\N	@HOU	3	1	0	0	0	0	20
Dolphins	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	1	2015	100019	\N	\N	\N	@WAS	2	1	0	0	0	0	18
Patriots	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	1	2015	100021	\N	\N	\N	PIT	2	0	1	0	0	0	22
Erik Lorig	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	1	2015	496812	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Sean McGrath	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	1	2015	2535943	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
J.J. Nelson	0	0	0	0	0	\N	0	0	0.00	ARI	WR	\N	0	1	2015	2552656	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Brandon Myers	0	0	0	0	0	\N	12	0	1.20	TB	TE	\N	1.2	1	2015	89766	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Chase Ford	0	0	0	0	0	\N	8	0	0.80	BAL	TE	\N	0.8	1	2015	2534244	0	0	0	@DEN	\N	\N	\N	\N	\N	\N	\N
Jaxon Shipley	0	0	0	0	0	\N	0	0	0.00	ARI	WR	\N	0	1	2015	2553599	0	0	0	NO	\N	\N	\N	\N	\N	\N	\N
Andre Johnson	0	0	0	0	0	\N	48	1	10.80	IND	WR	\N	10.8	1	2015	2505551	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Charles Sims	0	0	0	1	31	\N	13	0	10.40	TB	RB	\N	10.4	1	2015	2543770	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Zac Stacy	0	0	0	1	25	\N	10	0	9.50	NYJ	RB	\N	9.5	1	2015	2540273	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Andrew Hawkins	0	0	0	0	1	\N	40	0	4.10	CLE	WR	\N	4.1	1	2015	2508097	0	0	0	@NYJ	\N	\N	\N	\N	\N	\N	\N
Ty Montgomery	0	0	0	0	0	\N	39	0	3.90	GB	WR	\N	3.9	1	2015	2552429	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Kenny Britt	0	0	0	0	1	\N	34	0	3.50	STL	WR	\N	3.5	1	2015	71217	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Josh Bellamy	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	1	2015	2535964	0	0	0	GB	\N	\N	\N	\N	\N	\N	\N
Brandon Bostick	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	1	2015	2536342	0	0	0	CLE	\N	\N	\N	\N	\N	\N	\N
Da'Ron Brown	0	0	0	0	0	\N	0	0	0.00	KC	WR	\N	0	1	2015	2552479	0	0	0	@HOU	\N	\N	\N	\N	\N	\N	\N
Tom Brady	315	3	1	0	3	\N	0	0	22.90	NE	QB	\N	26.900000000000002	1	2015	2504211	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Matt Ryan	319	2	1	0	7	\N	0	0	19.46	ATL	QB	\N	23.459999999999997	1	2015	310	0	0	0	PHI	\N	\N	\N	\N	\N	\N	\N
Philip Rivers	269	2	1	0	2	\N	0	0	16.96	SD	QB	\N	17.959999999999997	1	2015	2506121	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
T.J. Jones	0	0	0	0	0	\N	0	0	0.00	DET	WR	\N	0	1	2015	2543836	0	0	0	@SD	\N	\N	\N	\N	\N	\N	\N
Ryan Lindley	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	1	2015	2532894	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Isaiah Burse	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	1	2015	2550182	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
DeAndre Carter	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	1	2015	2553502	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Steven Jackson	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	1	2015	2505936	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Austin Johnson	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	1	2015	2534437	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
James Hanna	0	0	0	0	0	\N	0	0	0.00	DAL	TE	\N	0	1	2015	2533028	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Tre Mason	0	0	0	0	51	\N	13	0	6.40	STL	RB	\N	6.4	1	2015	2543502	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Denard Robinson	0	0	0	0	47	\N	11	0	5.80	JAC	RB	\N	5.800000000000001	1	2015	2539260	0	0	0	CAR	\N	\N	\N	\N	\N	\N	\N
Giovani Bernard	0	0	0	0	31	\N	17	0	4.80	CIN	RB	\N	4.8	1	2015	2540156	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Paul Richardson	0	0	0	0	0	\N	7	0	0.70	SEA	WR	\N	0.7	1	2015	2543491	0	0	0	@STL	\N	\N	\N	\N	\N	\N	\N
John Kuhn	0	0	0	0	2	\N	1	0	0.30	GB	RB	\N	0.30000000000000004	1	2015	2506147	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Kendall Wright	0	0	0	0	3	\N	48	0	5.10	TEN	WR	\N	5.1	1	2015	2532977	0	0	0	@TB	\N	\N	\N	\N	\N	\N	\N
Josh Hill	0	0	0	0	0	\N	7	0	0.70	NO	TE	\N	0.7	1	2015	2541834	0	0	0	@ARI	\N	\N	\N	\N	\N	\N	\N
Anthony Dixon	0	0	0	0	4	\N	1	0	0.50	BUF	RB	\N	0.5	1	2015	497172	0	0	0	IND	\N	\N	\N	\N	\N	\N	\N
Matt Cassel	0	0	0	0	0	\N	0	0	0.00	DAL	QB	\N	0	1	2015	2506562	0	0	0	NYG	\N	\N	\N	\N	\N	\N	\N
Tavon Austin	0	0	0	0	23	\N	26	0	4.90	STL	WR	\N	4.9	1	2015	2539336	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Tyler Kroft	0	0	0	0	0	\N	7	0	0.70	CIN	TE	\N	0.7	1	2015	2552586	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
LeGarrette Blount	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	1	2015	497149	0	0	0	PIT	\N	\N	\N	\N	\N	\N	\N
Kellen Clemens	0	0	0	0	0	\N	0	0	0.00	SD	QB	\N	0	1	2015	2506895	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Bernard Pierce	0	0	0	0	5	\N	1	0	0.60	JAC	RB	\N	0.6	2	2015	2533545	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Juwan Thompson	0	0	0	0	5	\N	1	0	0.60	DEN	RB	\N	0.6	2	2015	2550263	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
MyCole Pruitt	0	0	0	0	0	\N	6	0	0.60	MIN	TE	\N	0.6	2	2015	2552401	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Anthony Dixon	0	0	0	0	4	\N	1	0	0.50	BUF	RB	\N	0.5	2	2015	497172	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Mike James	0	0	0	0	4	\N	1	0	0.50	TB	RB	\N	0.5	2	2015	2540224	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Dion Lewis	0	0	0	0	4	\N	1	0	0.50	NE	RB	\N	0.5	2	2015	2495469	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
John Kuhn	0	0	0	0	2	\N	1	0	0.30	GB	RB	\N	0.30000000000000004	2	2015	2506147	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Derek Anderson	0	0	0	0	0	\N	0	0	0.00	CAR	QB	\N	0	2	2015	2506546	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Matt Barkley	0	0	0	0	0	\N	0	0	0.00	ARI	QB	\N	0	2	2015	2539308	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Matt Cassel	0	0	0	0	0	\N	0	0	0.00	DAL	QB	\N	0	2	2015	2506562	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Jimmy Clausen	0	0	0	0	0	\N	0	0	0.00	BAL	QB	\N	0	2	2015	497108	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Kellen Clemens	0	0	0	0	0	\N	0	0	0.00	SD	QB	\N	0	2	2015	2506895	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Chase Daniel	0	0	0	0	0	\N	0	0	0.00	KC	QB	\N	0	2	2015	81284	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Austin Davis	0	0	0	0	0	\N	0	0	0.00	CLE	QB	\N	0	2	2015	2533349	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Stefon Diggs	0	0	0	0	0	\N	0	0	0.00	MIN	WR	\N	0	2	2015	2552608	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Rhett Ellison	0	0	0	0	0	\N	0	0	0.00	MIN	TE	\N	0	2	2015	2532835	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
C.J. Fiedorowicz	0	0	0	0	0	\N	0	0	0.00	HOU	TE	\N	0	2	2015	2543721	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Blaine Gabbert	0	0	0	0	0	\N	0	0	0.00	SF	QB	\N	0	2	2015	2495441	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Jimmy Garoppolo	0	0	0	0	0	\N	0	0	0.00	NE	QB	\N	0	2	2015	2543801	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Mike Glennon	0	0	0	0	0	\N	0	0	0.00	TB	QB	\N	0	2	2015	2539275	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Robert Griffin III	0	0	0	0	0	\N	0	0	0.00	WAS	QB	\N	0	2	2015	2533033	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Todd Gurley	0	0	0	0	0	\N	0	0	0.00	STL	RB	\N	0	2	2015	2552475	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Matt Hasselbeck	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	2	2015	2501078	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Chad Henne	0	0	0	0	0	\N	0	0	0.00	JAC	QB	\N	0	2	2015	252	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Shaun Hill	0	0	0	0	0	\N	0	0	0.00	MIN	QB	\N	0	2	2015	2504833	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Jeremy Maclin	0	0	0	0	0	\N	62	1	12.20	KC	WR	\N	12.2	2	2015	80429	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Isaiah Crowell	0	0	0	1	53	\N	8	0	12.10	CLE	RB	\N	12.100000000000001	2	2015	2550189	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Latavius Murray	0	0	0	1	53	\N	8	0	12.10	OAK	RB	\N	12.100000000000001	2	2015	2541161	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Torrey Smith	0	0	0	0	0	\N	61	1	12.10	SF	WR	\N	12.1	2	2015	2495459	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Jordan Cameron	0	0	0	0	0	\N	60	1	12.00	MIA	TE	\N	12	2	2015	2495267	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Kenny Stills	0	0	0	0	0	\N	60	1	12.00	MIA	WR	\N	12	2	2015	2540202	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Michael Crabtree	0	0	0	0	0	\N	59	1	11.90	OAK	WR	\N	11.9	2	2015	71269	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Pierre Garcon	0	0	0	0	0	\N	57	1	11.70	WAS	WR	\N	11.7	2	2015	2346	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Jason Avant	0	0	0	0	0	\N	26	0	2.60	KC	WR	\N	2.6	2	2015	2495802	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Jerricho Cotchery	0	0	0	0	0	\N	23	0	2.30	CAR	WR	\N	2.3	2	2015	2506004	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Eric Ebron	0	0	0	0	0	\N	19	0	1.90	DET	TE	\N	1.9	2	2015	2543466	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Tre McBride	0	0	0	0	0	\N	0	0	0.00	TEN	WR	\N	0	2	2015	2552428	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Marcus Mariota	225	2	2	0	29	\N	0	0	15.90	TEN	QB	\N	17.9	2	2015	2552466	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
C.J. Anderson	0	0	0	1	68	\N	19	0	14.70	DEN	RB	\N	14.700000000000001	2	2015	2540269	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
DeAndre Hopkins	0	0	0	0	0	\N	79	1	13.90	HOU	WR	\N	13.9	2	2015	2540165	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Dez Bryant	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	2	2015	497278	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Landry Jones	0	0	0	0	0	\N	0	0	0.00	PIT	QB	\N	0	2	2015	2539287	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Brian Leonhardt	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	2	2015	2541512	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Darius Jennings	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	2	2015	2553896	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Rashad Ross	0	0	0	0	0	\N	0	0	0.00	WAS	WR	\N	0	2	2015	2541940	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Wes Saxton	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	2	2015	2553682	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Alfred Blue	0	0	0	1	64	\N	12	0	13.60	HOU	RB	\N	13.6	2	2015	2543600	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Calvin Johnson	0	0	0	0	0	\N	72	1	13.20	DET	WR	\N	13.2	2	2015	2495647	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Tre Mason	0	0	0	1	54	\N	14	0	12.80	STL	RB	\N	12.8	2	2015	2543502	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Le'Veon Bell	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	2	2015	2540175	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Justin Blackmon	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	2	2015	2533038	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Daniel Brown	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	2	2015	2553900	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Justice Cunningham	0	0	0	0	0	\N	0	0	0.00	STL	TE	\N	0	2	2015	2540257	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Danny Woodhead	0	0	0	1	25	\N	25	0	11.00	SD	RB	\N	11	2	2015	4327	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Zach Ertz	0	0	0	0	0	\N	43	1	10.30	PHI	TE	\N	10.3	2	2015	2540158	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Delanie Walker	0	0	0	0	0	\N	40	1	10.00	TEN	TE	\N	10	2	2015	2495966	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Cowboys	\N	\N	\N	\N	\N	\N	\N	\N	2.00	\N	DST	\N	2	2	2015	100008	\N	\N	\N	@PHI	1	1	0	0	0	0	30
MarQueis Gray	0	0	0	0	0	\N	0	0	0.00	BUF	TE	\N	0	2	2015	2540259	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
James Hanna	0	0	0	0	0	\N	0	0	0.00	DAL	TE	\N	0	2	2015	2533028	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Zach Mettenberger	0	0	0	0	0	\N	0	0	0.00	TEN	QB	\N	0	2	2015	2543605	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Drew Stanton	0	0	0	0	0	\N	0	0	0.00	ARI	QB	\N	0	2	2015	2495748	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Rajion Neal	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	2	2015	2550233	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Lucky Whitehead	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	2	2015	2553621	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Cordarrelle Patterson	0	0	0	0	10	\N	24	0	3.40	MIN	WR	\N	3.4	2	2015	2540145	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Andre Caldwell	0	0	0	0	0	\N	28	0	2.80	DEN	WR	\N	2.8	2	2015	206	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Ryan Mathews	0	0	0	0	58	\N	16	0	7.40	PHI	RB	\N	7.4	2	2015	497188	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Clay Harbor	0	0	0	0	0	\N	13	0	1.30	JAC	TE	\N	1.3	2	2015	497242	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Lance Moore	0	0	0	0	0	\N	7	0	0.70	DET	WR	\N	0.7	2	2015	2506287	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Terrell Watson	0	0	0	0	0	\N	0	0	0.00	CIN	RB	\N	0	2	2015	2553617	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Carson Palmer	315	2	1	0	3	\N	0	0	18.90	ARI	QB	\N	22.900000000000002	2	2015	2505245	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Sam Bradford	245	2	1	0	3	\N	0	0	16.10	PHI	QB	\N	17.1	2	2015	497095	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Kennard Backman	0	0	0	0	0	\N	0	0	0.00	GB	TE	\N	0	2	2015	2553292	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Arian Foster	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	2	2015	79555	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Josh Gordon	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	2	2015	2537931	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Andrew Hawkins	0	0	0	0	1	\N	40	0	4.10	CLE	WR	\N	4.1	2	2015	2508097	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Ty Montgomery	0	0	0	0	0	\N	35	0	3.50	GB	WR	\N	3.5	2	2015	2552429	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Andrew Quarless	0	0	0	0	0	\N	17	0	1.70	GB	TE	\N	1.7	2	2015	497258	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Timothy Wright	0	0	0	0	0	\N	15	0	1.50	DET	TE	\N	1.5	2	2015	2541768	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Panthers	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	6	2	2015	100004	\N	\N	\N	HOU	2	1	1	0	0	0	22
Dolphins	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100019	\N	\N	\N	@JAC	2	1	0	0	0	0	23
Matt Simms	0	0	0	0	0	\N	0	0	0.00	ATL	QB	\N	0	2	2015	2534889	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Neal Sterling	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	2	2015	2553451	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Rashad Jennings	0	0	0	1	45	\N	14	0	11.90	NYG	RB	\N	11.9	2	2015	71345	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Jarvis Landry	0	0	0	0	2	\N	55	1	11.70	MIA	WR	\N	11.7	2	2015	2543488	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Jeff Janis	0	0	0	0	0	\N	26	0	2.60	GB	WR	\N	2.6	2	2015	2543750	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Cody Latimer	0	0	0	0	0	\N	23	0	2.30	DEN	WR	\N	2.3	2	2015	2543590	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
James White	0	0	0	0	9	\N	10	0	1.90	NE	RB	\N	1.9	2	2015	2543773	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Bradley Marquez	0	0	0	0	0	\N	0	0	0.00	STL	WR	\N	0	2	2015	2553725	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Vance McDonald	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	2	2015	2540215	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Julio Jones	0	0	0	0	1	\N	90	1	15.10	ATL	WR	\N	15.1	2	2015	2495454	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Demaryius Thomas	0	0	0	0	0	\N	80	1	14.00	DEN	WR	\N	14	2	2015	497328	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Will Johnson	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	2	2015	163982	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Jake Kumerow	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	2	2015	2553548	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Ryan Lindley	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	2	2015	2532894	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Joey Iosefa	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	2	2015	2552634	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
David Johnson	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	2	2015	79589	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Marques Colston	0	0	0	0	0	\N	74	1	13.40	NO	WR	\N	13.4	2	2015	2495821	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
A.J. Green	0	0	0	0	1	\N	69	1	13.00	CIN	WR	\N	13	2	2015	2495450	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Justin Forsett	0	0	0	1	52	\N	14	0	12.60	BAL	RB	\N	12.6	2	2015	927	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Josh Boyce	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	2540181	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Kaelin Clay	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	2	2015	2552651	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
John Crockett	0	0	0	0	0	\N	0	0	0.00	GB	RB	\N	0	2	2015	2553511	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Doug Baldwin	0	0	0	0	1	\N	49	1	11.00	SEA	WR	\N	11	2	2015	2530747	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Malcom Floyd	0	0	0	0	0	\N	44	1	10.40	SD	WR	\N	10.4	2	2015	2505785	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Davante Adams	0	0	0	0	0	\N	41	1	10.10	GB	WR	\N	10.1	2	2015	2543495	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Seahawks	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	2	2015	100030	\N	\N	\N	@GB	2	1	0	0	0	0	29
Ryan Grant	0	0	0	0	0	\N	0	0	0.00	WAS	WR	\N	0	2	2015	2543759	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Ryan Griffin	0	0	0	0	0	\N	0	0	0.00	HOU	TE	\N	0	2	2015	2541316	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Tarvaris Jackson	0	0	0	0	0	\N	0	0	0.00	SEA	QB	\N	0	2	2015	2495863	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Luke McCown	0	0	0	0	0	\N	0	0	0.00	NO	QB	\N	0	2	2015	2506053	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Brock Osweiler	0	0	0	0	0	\N	0	0	0.00	DEN	QB	\N	0	2	2015	2533436	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Jordan Todman	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	2	2015	2495472	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Raheem Mostert	0	0	0	0	0	\N	0	0	0.00	CLE	RB	\N	0	2	2015	2553728	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Geremy Davis	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	2	2015	2552654	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
James Develin	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	2	2015	2508101	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Andre Ellington	0	0	0	0	0	\N	0	0	0.00	ARI	RB	\N	0	2	2015	2539217	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Glenn Winston	0	0	0	0	0	\N	0	0	0.00	CLE	RB	\N	0	2	2015	2551357	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Lance Dunbar	0	0	0	0	27	\N	3	0	3.00	DAL	RB	\N	3	2	2015	2535708	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Jeff Cumberland	0	0	0	0	0	\N	28	0	2.80	NYJ	TE	\N	2.8	2	2015	2507883	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Heath Miller	0	0	0	0	0	\N	33	1	9.30	PIT	TE	\N	9.3	2	2015	2506369	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Dexter McCluster	0	0	0	0	36	\N	26	0	6.20	TEN	RB	\N	6.2	2	2015	497190	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Kendall Wright	0	0	0	0	3	\N	46	0	4.90	TEN	WR	\N	4.9	2	2015	2532977	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Brandon Myers	0	0	0	0	0	\N	12	0	1.20	TB	TE	\N	1.2	2	2015	89766	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Donald Brown	0	0	0	0	5	\N	3	0	0.80	SD	RB	\N	0.8	2	2015	2507779	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Clive Walford	0	0	0	0	0	\N	7	0	0.70	OAK	TE	\N	0.7	2	2015	2552397	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Nick Toon	0	0	0	0	0	\N	0	0	0.00	STL	WR	\N	0	2	2015	2532960	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Darren Waller	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	2	2015	2552408	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Trey Watts	0	0	0	0	0	\N	0	0	0.00	STL	RB	\N	0	2	2015	2550448	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Javontee Herndon	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	2	2015	2550459	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Ryan Hewitt	0	0	0	0	0	\N	0	0	0.00	CIN	TE	\N	0	2	2015	2550206	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Andrew Luck	361	3	1	0	18	\N	0	0	26.24	IND	QB	\N	30.24	2	2015	2533031	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Aaron Rodgers	268	2	1	0	14	\N	0	0	18.12	GB	QB	\N	19.119999999999997	2	2015	2506363	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Joe Flacco	255	2	1	0	5	\N	0	0	16.70	BAL	QB	\N	17.7	2	2015	382	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Kerwynn Williams	0	0	0	0	0	\N	0	0	0.00	ARI	RB	\N	0	2	2015	2539980	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Rory Anderson	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	2	2015	2553465	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Bennie Fowler	0	0	0	0	0	\N	0	0	0.00	DEN	WR	\N	0	2	2015	2550198	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Mike Gillislee	0	0	0	0	0	\N	0	0	0.00	BUF	RB	\N	0	2	2015	2539663	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Terrelle Pryor	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	2	2015	2531332	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Daryl Richardson	0	0	0	0	0	\N	0	0	0.00	CLE	RB	\N	0	2	2015	2534789	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Phillip Supernaw	0	0	0	0	0	\N	0	0	0.00	TEN	TE	\N	0	2	2015	2535535	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Randall Telfer	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	2	2015	2552660	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Jacquizz Rodgers	0	0	0	0	27	\N	18	0	4.50	CHI	RB	\N	4.5	2	2015	2495471	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Ameer Abdullah	0	0	0	0	28	\N	12	0	4.00	DET	RB	\N	4	2	2015	2552374	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Chris Polk	0	0	0	0	27	\N	10	0	3.70	HOU	RB	\N	3.7	2	2015	2533037	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Brandon Coleman	0	0	0	0	0	\N	34	0	3.40	NO	WR	\N	3.4	2	2015	2550328	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Logan Paulsen	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	2	2015	2507828	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Fred Jackson	0	0	0	0	8	\N	8	0	1.60	SEA	RB	\N	1.6	2	2015	2506871	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
DeVante Parker	0	0	0	0	0	\N	15	0	1.50	MIA	WR	\N	1.5	2	2015	2552409	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Golden Tate	0	0	0	0	2	\N	57	1	11.90	DET	WR	\N	11.9	2	2015	497326	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Austin Seferian-Jenkins	0	0	0	0	0	\N	57	1	11.70	TB	TE	\N	11.7	2	2015	2543683	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Gavin Escobar	0	0	0	0	0	\N	25	0	2.50	DAL	TE	\N	2.5	2	2015	2540211	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Ed Dickson	0	0	0	0	0	\N	20	0	2.00	CAR	TE	\N	2	2	2015	497224	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Sean Mannion	0	0	0	0	0	\N	0	0	0.00	STL	QB	\N	0	2	2015	2552576	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Josh McCown	0	0	0	0	0	\N	0	0	0.00	CLE	QB	\N	0	2	2015	2505076	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Ryan Fitzpatrick	213	2	1	0	8	\N	0	0	15.32	NYJ	QB	\N	16.32	2	2015	2506581	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Alshon Jeffery	0	0	0	0	2	\N	85	1	14.70	CHI	WR	\N	14.7	2	2015	2533039	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Mark Ingram	0	0	0	1	69	\N	10	0	13.90	NO	RB	\N	13.9	2	2015	2495466	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Martavis Bryant	0	0	0	0	0	\N	0	0	0.00	PIT	WR	\N	0	2	2015	2543572	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Garrett Celek	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	2	2015	2534820	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Jonathan Krause	0	0	0	0	0	\N	0	0	0.00	PHI	WR	\N	0	2	2015	2550216	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Kendall Hunter	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	2	2015	2495172	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Damaris Johnson	0	0	0	0	0	\N	0	0	0.00	TEN	WR	\N	0	2	2015	2536031	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Jordan Matthews	0	0	0	0	0	\N	76	1	13.60	PHI	WR	\N	13.6	2	2015	2543500	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Rob Gronkowski	0	0	0	0	0	\N	67	1	12.70	NE	TE	\N	12.7	2	2015	497240	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Brandon Bostick	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	2	2015	2536342	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Mack Brown	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	2	2015	2553867	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
James Jones	0	0	0	0	0	\N	53	1	11.30	GB	WR	\N	11.3	2	2015	2507183	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Charles Sims	0	0	0	1	30	\N	13	0	10.30	TB	RB	\N	10.3	2	2015	2543770	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Chris Harper	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	2553652	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Stevan Ridley	0	0	0	0	0	\N	0	0	0.00	NYJ	RB	\N	0	2	2015	2495470	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Zach Miller	0	0	0	0	0	\N	0	0	0.00	CHI	TE	\N	0	2	2015	238457	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
J.J. Nelson	0	0	0	0	0	\N	0	0	0.00	ARI	WR	\N	0	2	2015	2552656	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Jim Dray	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	2	2015	497226	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
DeAndrew White	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	2	2015	2552657	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Trey Williams	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	2	2015	2553486	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Josh Huff	0	0	0	0	0	\N	33	0	3.30	PHI	WR	\N	3.3	2	2015	2543746	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Steve Johnson	0	0	0	0	0	\N	29	0	2.90	SD	WR	\N	2.9	2	2015	768	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Toby Gerhart	0	0	0	0	20	\N	7	0	2.70	JAC	RB	\N	2.7	2	2015	497176	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Maxx Williams	0	0	0	0	0	\N	32	1	9.20	BAL	TE	\N	9.2	2	2015	2552468	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Tavon Austin	0	0	0	0	25	\N	29	0	5.40	STL	WR	\N	5.4	2	2015	2539336	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Cecil Shorts	0	0	0	0	0	\N	46	0	4.60	HOU	WR	\N	4.6	2	2015	2495341	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Taylor Gabriel	0	0	0	0	0	\N	9	0	0.90	CLE	WR	\N	0.9	2	2015	2550617	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Justin Hardy	0	0	0	0	0	\N	8	0	0.80	ATL	WR	\N	0.8	2	2015	2552418	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Tyler Varga	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	2	2015	2552578	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
DuJuan Harris	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	2	2015	2530690	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Devin Hester	0	0	0	0	0	\N	0	0	0.00	ATL	WR	\N	0	2	2015	2506897	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Matt Forte	0	0	0	1	87	\N	47	0	19.40	CHI	RB	\N	19.4	2	2015	234	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Colin Kaepernick	200	2	1	0	34	\N	0	0	17.40	SF	QB	\N	18.4	2	2015	2495186	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Philip Rivers	243	2	1	0	2	\N	0	0	15.92	SD	QB	\N	16.919999999999998	2	2015	2506121	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Montee Ball	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	2	2015	2539341	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Josh Freeman	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	2	2015	79557	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Jay Prosch	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	2	2015	2543695	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Jake Stoneburner	0	0	0	0	0	\N	0	0	0.00	MIA	TE	\N	0	2	2015	2539283	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Julius Thomas	0	0	0	0	0	\N	0	0	0.00	JAC	TE	\N	0	2	2015	2495353	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Theo Riddick	0	0	0	0	22	\N	16	0	3.80	DET	RB	\N	3.8000000000000003	2	2015	2540020	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Kevin Norwood	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	2	2015	2543689	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Niles Paul	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	2	2015	2495211	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Ricardo Lockette	0	0	0	0	1	\N	15	0	1.60	SEA	WR	\N	1.6	2	2015	2495306	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Jamison Crowder	0	0	0	0	1	\N	13	0	1.40	WAS	WR	\N	1.4000000000000001	2	2015	2552415	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Titans	\N	\N	\N	\N	\N	\N	\N	\N	6.00	\N	DST	\N	10	2	2015	100012	\N	\N	\N	@CLE	2	1	1	0	0	0	9
Broncos	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100009	\N	\N	\N	@KC	2	1	0	0	0	0	24
Redskins	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100032	\N	\N	\N	STL	2	1	0	0	0	0	22
Dion Sims	0	0	0	0	0	\N	0	0	0.00	MIA	TE	\N	0	2	2015	2540200	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Matt Spaeth	0	0	0	0	0	\N	0	0	0.00	PIT	TE	\N	0	2	2015	2507182	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Anquan Boldin	0	0	0	0	0	\N	58	1	11.80	SF	WR	\N	11.8	2	2015	2505587	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Roddy White	0	0	0	0	0	\N	57	1	11.70	ATL	WR	\N	11.7	2	2015	2506366	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Jared Cook	0	0	0	0	0	\N	25	0	2.50	STL	TE	\N	2.5	2	2015	71265	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Marquess Wilson	0	0	0	0	0	\N	21	0	2.10	CHI	WR	\N	2.1	2	2015	2540177	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Erik Lorig	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	2	2015	496812	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Brian Hoyer	229	2	1	0	3	\N	0	0	15.46	HOU	QB	\N	16.46	2	2015	81294	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
T.J. Yeldon	0	0	0	1	69	\N	14	0	14.30	JAC	RB	\N	14.3	2	2015	2552471	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
C.J. Spiller	0	0	0	1	50	\N	27	0	13.70	NO	RB	\N	13.7	2	2015	497204	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Michael Burton	0	0	0	0	0	\N	0	0	0.00	DET	RB	\N	0	2	2015	2552636	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
DeAndre Carter	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	2553502	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Taiwan Jones	0	0	0	0	0	\N	0	0	0.00	OAK	RB	\N	0	2	2015	2495467	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Josh Lenz	0	0	0	0	0	\N	0	0	0.00	HOU	WR	\N	0	2	2015	2541853	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Akeem Hunt	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	2	2015	2553660	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Anthony Sherman	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	2	2015	2495340	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Brandon Marshall	0	0	0	0	0	\N	71	1	13.10	NYJ	WR	\N	13.1	2	2015	2495893	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Emmanuel Sanders	0	0	0	0	2	\N	65	1	12.70	DEN	WR	\N	12.7	2	2015	497322	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Ahmad Bradshaw	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	2	2015	2495560	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
David Cobb	0	0	0	0	0	\N	0	0	0.00	TEN	RB	\N	0	2	2015	2552376	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
De'Anthony Thomas	0	0	0	0	23	\N	26	1	10.90	KC	RB	\N	10.9	2	2015	2543638	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Percy Harvin	0	0	0	0	5	\N	34	1	9.90	BUF	WR	\N	9.9	2	2015	80425	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Chris Gragg	0	0	0	0	0	\N	0	0	0.00	BUF	TE	\N	0	2	2015	2539202	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Brandon LaFell	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	497302	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Dennis Pitta	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	2	2015	497254	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
David Nelson	0	0	0	0	0	\N	0	0	0.00	PIT	WR	\N	0	2	2015	2507884	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Shaun Draughn	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	2	2015	2495461	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Nikita Whitlock	0	0	0	0	0	\N	0	0	0.00	NYG	RB	\N	0	2	2015	2550270	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Marqise Lee	0	0	0	0	1	\N	33	0	3.40	JAC	WR	\N	3.4	2	2015	2543475	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Mychal Rivera	0	0	0	0	0	\N	30	0	3.00	OAK	TE	\N	3	2	2015	2539995	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Brian Hartline	0	0	0	0	0	\N	28	0	2.80	CLE	WR	\N	2.8	2	2015	81831	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Kirk Cousins	170	1	1	0	4	\N	0	0	9.20	WAS	QB	\N	10.200000000000001	2	2015	2532820	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
DeAngelo Williams	0	0	0	0	50	\N	5	0	5.50	PIT	RB	\N	5.5	2	2015	2495979	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Mohamed Sanu	0	0	0	0	3	\N	44	0	4.70	CIN	WR	\N	4.7	2	2015	2533040	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Marcel Reece	0	0	0	0	4	\N	6	0	1.00	OAK	RB	\N	1	2	2015	4433	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Chase Ford	0	0	0	0	0	\N	8	0	0.80	BAL	TE	\N	0.8	2	2015	2534244	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Dylan Thompson	0	0	0	0	0	\N	0	0	0.00	SF	QB	\N	0	2	2015	2553480	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Robert Turbin	0	0	0	0	0	\N	0	0	0.00	DAL	RB	\N	0	2	2015	2533460	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Terron Ward	0	0	0	0	0	\N	0	0	0.00	ATL	RB	\N	0	2	2015	2553762	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Demetrius Harris	0	0	0	0	0	\N	0	0	0.00	KC	TE	\N	0	2	2015	2541187	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Robert Herron	0	0	0	0	0	\N	0	0	0.00	MIA	WR	\N	0	2	2015	2543777	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Stephen Hill	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	2	2015	2533537	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Eddie Lacy	0	0	0	1	115	\N	34	0	20.90	GB	RB	\N	23.9	2	2015	2540168	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Nick Foles	290	2	1	0	3	\N	0	0	17.90	STL	QB	\N	18.900000000000002	2	2015	2532842	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Andy Dalton	234	2	1	0	10	\N	0	0	16.36	CIN	QB	\N	17.36	2	2015	2495143	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Jay Ajayi	0	0	0	0	0	\N	0	0	0.00	MIA	RB	\N	0	2	2015	2552582	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Daniel Fells	0	0	0	0	0	\N	0	0	0.00	NYG	TE	\N	0	2	2015	2506619	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Tyler Gaffney	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	2	2015	2543655	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Marquise Goodwin	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	2	2015	2539964	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Walt Powell	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	2	2015	2543619	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Sean Renfree	0	0	0	0	0	\N	0	0	0.00	ATL	QB	\N	0	2	2015	2540263	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Aaron Ripkowski	0	0	0	0	0	\N	0	0	0.00	GB	RB	\N	0	2	2015	2552477	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Pierre Thomas	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	2	2015	2507141	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Devonta Freeman	0	0	0	0	28	\N	10	0	3.80	ATL	RB	\N	3.8	2	2015	2543583	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Zac Stacy	0	0	0	0	25	\N	11	0	3.60	NYJ	RB	\N	3.6	2	2015	2540273	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Jordan Norwood	0	0	0	0	0	\N	0	0	0.00	DEN	WR	\N	0	2	2015	71417	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Jamize Olawale	0	0	0	0	0	\N	0	0	0.00	OAK	RB	\N	0	2	2015	2536044	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Quinton Patton	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	2	2015	2539250	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Tyler Lockett	0	0	0	0	1	\N	16	0	1.70	SEA	WR	\N	1.7000000000000002	2	2015	2552430	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Cameron Artis-Payne	0	0	0	0	11	\N	4	0	1.50	CAR	RB	\N	1.5	2	2015	2552375	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Lance Kendricks	0	0	0	0	0	\N	14	0	1.40	STL	TE	\N	1.4	2	2015	2495187	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Chiefs	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	4	2	2015	100016	\N	\N	\N	DEN	3	1	0	0	0	0	28
Chargers	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100028	\N	\N	\N	@CIN	1	1	0	0	0	0	20
Matthew Slater	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	4487	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Craig Stevens	0	0	0	0	0	\N	0	0	0.00	TEN	TE	\N	0	2	2015	1992	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Dwayne Bowe	0	0	0	0	0	\N	58	1	11.80	CLE	WR	\N	11.8	2	2015	2495558	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Blake Bortles	189	1	1	0	21	\N	0	0	11.66	JAC	QB	\N	12.659999999999998	2	2015	2543477	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Marvin Jones	0	0	0	0	0	\N	25	0	2.50	CIN	WR	\N	2.5	2	2015	2532884	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Jordan Reed	0	0	0	0	0	\N	23	0	2.30	WAS	TE	\N	2.3	2	2015	2540160	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Travaris Cadet	0	0	0	0	8	\N	10	0	1.80	NO	RB	\N	1.8	2	2015	2535890	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Vince Mayle	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	2	2015	2552431	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Antonio Brown	0	0	0	0	2	\N	97	1	15.90	PIT	WR	\N	15.9	2	2015	2508061	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Jay Cutler	207	2	1	0	7	\N	0	0	14.98	CHI	QB	\N	15.98	2	2015	2495824	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Brice Butler	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	2	2015	2541388	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Nick Kasa	0	0	0	0	0	\N	0	0	0.00	DEN	TE	\N	0	2	2015	2540233	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Thad Lewis	0	0	0	0	0	\N	0	0	0.00	PHI	QB	\N	0	2	2015	497121	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Dontrelle Inman	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	2	2015	2530700	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Terrance Williams	0	0	0	0	0	\N	76	1	13.60	DAL	WR	\N	13.6	2	2015	2539205	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Eric Decker	0	0	0	0	0	\N	70	1	13.00	NYJ	WR	\N	13	2	2015	497284	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Sammy Watkins	0	0	0	0	2	\N	65	1	12.70	BUF	WR	\N	12.7	2	2015	2543457	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Brenton Bersin	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	2	2015	2535966	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Cameron Brate	0	0	0	0	0	\N	0	0	0.00	TB	TE	\N	0	2	2015	2550656	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Chase Coffman	0	0	0	0	0	\N	0	0	0.00	SEA	TE	\N	0	2	2015	71255	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Tevin Coleman	0	0	0	1	46	\N	6	0	11.20	ATL	RB	\N	11.2	2	2015	2552453	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
John Brown	0	0	0	0	0	\N	45	1	10.50	ARI	WR	\N	10.5	2	2015	2543847	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Steve Smith	0	0	0	0	0	\N	43	1	10.30	BAL	WR	\N	10.3	2	2015	2504595	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Saints	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	2	2015	100022	\N	\N	\N	TB	1	1	0	0	0	0	25
Corey Grant	0	0	0	0	0	\N	0	0	0.00	JAC	RB	\N	0	2	2015	2553650	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Marion Grice	0	0	0	0	0	\N	0	0	0.00	ARI	RB	\N	0	2	2015	2543690	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
EJ Manuel	0	0	0	0	0	\N	0	0	0.00	BUF	QB	\N	0	2	2015	2539228	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Dan Orlovsky	0	0	0	0	0	\N	0	0	0.00	DET	QB	\N	0	2	2015	2506481	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Lorenzo Taliaferro	0	0	0	0	0	\N	0	0	0.00	BAL	RB	\N	0	2	2015	2543704	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Ifeanyi Momah	0	0	0	0	0	\N	0	0	0.00	ARI	TE	\N	0	2	2015	2541096	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Marcus Murphy	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	2	2015	2552647	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Hakeem Nicks	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	2	2015	80672	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
A.J. Derby	0	0	0	0	0	\N	0	0	0.00	NE	TE	\N	0	2	2015	2552580	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Donteea Dye	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	2	2015	2553876	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Wes Welker	0	0	0	0	0	\N	0	0	0.00	STL	WR	\N	0	2	2015	2505790	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Nick Williams	0	0	0	0	0	\N	0	0	0.00	ATL	WR	\N	0	2	2015	2540279	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Bobby Rainey	0	0	0	0	18	\N	16	0	3.40	TB	RB	\N	3.4000000000000004	2	2015	2534571	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Stedman Bailey	0	0	0	0	2	\N	26	0	2.80	STL	WR	\N	2.8000000000000003	2	2015	2540157	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Phillip Dorsett	0	0	0	0	0	\N	33	1	9.30	IND	WR	\N	9.3	2	2015	2552424	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Darren Sproles	0	0	0	0	31	\N	40	0	7.10	PHI	RB	\N	7.1	2	2015	2506467	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Rueben Randle	0	0	0	0	0	\N	51	0	5.10	NYG	WR	\N	5.1	2	2015	2533533	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Karlos Williams	0	0	0	0	11	\N	2	0	1.30	BUF	RB	\N	1.3	2	2015	2552380	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Travis Benjamin	0	0	0	0	0	\N	8	0	0.80	CLE	WR	\N	0.8	2	2015	2532790	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Paul Richardson	0	0	0	0	0	\N	7	0	0.70	SEA	WR	\N	0.7	2	2015	2543491	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Scott Tolzien	0	0	0	0	0	\N	0	0	0.00	GB	QB	\N	0	2	2015	2495425	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Tony Washington	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	2	2015	2550415	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Mitchell Henry	0	0	0	0	0	\N	0	0	0.00	GB	TE	\N	0	2	2015	2553773	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Jeff Heuerman	0	0	0	0	0	\N	0	0	0.00	DEN	TE	\N	0	2	2015	2552399	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Michael Hoomanawanui	0	0	0	0	0	\N	0	0	0.00	NO	TE	\N	0	2	2015	497246	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Russell Wilson	205	2	1	0	40	\N	0	0	18.20	SEA	QB	\N	19.2	2	2015	2532975	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Mike Evans	0	0	0	0	0	\N	109	1	16.90	TB	WR	\N	19.9	2	2015	2543468	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Charlie Whitehurst	0	0	0	0	0	\N	0	0	0.00	IND	QB	\N	0	2	2015	2495974	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Jace Amaro	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	2	2015	2543481	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Blake Bell	0	0	0	0	0	\N	0	0	0.00	SF	TE	\N	0	2	2015	2552478	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Isaac Fruechte	0	0	0	0	0	\N	0	0	0.00	MIN	WR	\N	0	2	2015	2553883	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Brandon Gibson	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	81288	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
John Phillips	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	2	2015	71435	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Allen Reisner	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	2	2015	2495437	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Devin Street	0	0	0	0	0	\N	0	0	0.00	DAL	WR	\N	0	2	2015	2543641	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Logan Thomas	0	0	0	0	0	\N	0	0	0.00	MIA	QB	\N	0	2	2015	2543767	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Jarius Wright	0	0	0	0	6	\N	35	0	4.10	MIN	WR	\N	4.1	2	2015	2532978	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Roy Helu	0	0	0	0	18	\N	19	0	3.70	OAK	RB	\N	3.7	2	2015	2495161	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Scott Chandler	0	0	0	0	0	\N	34	0	3.40	NE	TE	\N	3.4	2	2015	2495573	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Chris Owusu	0	0	0	0	0	\N	0	0	0.00	NYJ	WR	\N	0	2	2015	2532923	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Levine Toilolo	0	0	0	0	0	\N	18	0	1.80	ATL	TE	\N	1.8	2	2015	2540203	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Jacob Tamme	0	0	0	0	0	\N	16	0	1.60	ATL	TE	\N	1.6	2	2015	324	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Bruce Ellington	0	0	0	0	1	\N	13	0	1.40	SF	WR	\N	1.4000000000000001	2	2015	2543646	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Bills	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	2	2015	100003	\N	\N	\N	NE	3	1	0	0	0	0	27
Larry Donnell	0	0	0	0	0	\N	58	1	11.80	NYG	TE	\N	11.8	2	2015	2537933	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Nelson Agholor	0	0	0	0	0	\N	56	1	11.60	PHI	WR	\N	11.6	2	2015	2552600	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Matt Asiata	0	0	0	0	17	\N	7	0	2.40	MIN	RB	\N	2.4	2	2015	2495258	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Branden Oliver	0	0	0	0	12	\N	10	0	2.20	SD	RB	\N	2.2	2	2015	2550658	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Josh Robinson	0	0	0	0	16	\N	2	0	1.80	IND	RB	\N	1.8	2	2015	2553462	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Marc Mariani	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	2	2015	1037885	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Anthony McCoy	0	0	0	0	0	\N	0	0	0.00	SEA	TE	\N	0	2	2015	497250	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Ryan Tannehill	197	2	1	0	15	\N	0	0	15.38	MIA	QB	\N	16.38	2	2015	2532956	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Amari Cooper	0	0	0	0	0	\N	81	1	14.10	OAK	WR	\N	14.1	2	2015	2552487	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Trey Burton	0	0	0	0	0	\N	0	0	0.00	PHI	TE	\N	0	2	2015	2550284	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Tavarres King	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	2	2015	2539235	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Robert Hughes	0	0	0	0	0	\N	0	0	0.00	ARI	RB	\N	0	2	2015	2530516	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
DeSean Jackson	0	0	0	0	0	\N	0	0	0.00	WAS	WR	\N	0	2	2015	1581	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Connor Shaw	0	0	0	0	0	\N	0	0	0.00	CLE	QB	\N	0	2	2015	2550252	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Devin Funchess	0	0	0	0	0	\N	73	1	13.30	CAR	WR	\N	13.3	2	2015	2552458	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
David Johnson	0	0	0	1	46	\N	24	0	13.00	ARI	RB	\N	13	2	2015	2553435	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Melvin Gordon	0	0	0	1	57	\N	8	0	12.50	SD	RB	\N	12.5	2	2015	2552469	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
E.J. Bibbs	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	2	2015	2553791	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Quan Bray	0	0	0	0	0	\N	0	0	0.00	IND	WR	\N	0	2	2015	2553467	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Asante Cleveland	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	2	2015	2550454	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Victor Cruz	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	2	2015	2507855	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
LeGarrette Blount	0	0	0	1	48	\N	2	0	11.00	NE	RB	\N	11	2	2015	497149	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Martellus Bennett	0	0	0	0	0	\N	43	1	10.30	CHI	TE	\N	10.3	2	2015	1062	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Ladarius Green	0	0	0	0	0	\N	41	1	10.10	SD	TE	\N	10.1	2	2015	2532853	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Steelers	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	2	2015	100027	\N	\N	\N	SF	2	0	0	0	0	0	19
Virgil Green	0	0	0	0	0	\N	0	0	0.00	DEN	TE	\N	0	2	2015	2495288	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Jesse James	0	0	0	0	0	\N	0	0	0.00	PIT	TE	\N	0	2	2015	2552633	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Colt McCoy	0	0	0	0	0	\N	0	0	0.00	WAS	QB	\N	0	2	2015	497123	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Cedric Peerman	0	0	0	0	0	\N	0	0	0.00	CIN	RB	\N	0	2	2015	79629	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Mike Vick	0	0	0	0	0	\N	0	0	0.00	PIT	QB	\N	0	2	2015	2504531	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Kellen Moore	0	0	0	0	0	\N	0	0	0.00	DAL	QB	\N	0	2	2015	2532917	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Aaron Murray	0	0	0	0	0	\N	0	0	0.00	KC	QB	\N	0	2	2015	2543587	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Kellen Davis	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	2	2015	2507486	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Dorin Dickerson	0	0	0	0	0	\N	0	0	0.00	TEN	TE	\N	0	2	2015	497222	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Marcus Easley	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	2	2015	497286	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Charcandrick West	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	2	2015	2550268	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Kyle Williams	0	0	0	0	0	\N	0	0	0.00	DEN	WR	\N	0	2	2015	2508027	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Greg Jennings	0	0	0	0	0	\N	34	0	3.40	MIA	WR	\N	3.4	2	2015	2495867	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Darren McFadden	0	0	0	0	27	\N	3	0	3.00	DAL	RB	\N	3	2	2015	284	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Garrett Graham	0	0	0	0	0	\N	28	0	2.80	HOU	TE	\N	2.8	2	2015	497234	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Donte Moncrief	0	0	0	0	2	\N	31	1	9.30	IND	WR	\N	9.3	2	2015	2543614	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Jonathan Stewart	0	0	0	0	48	\N	11	0	5.90	CAR	RB	\N	5.9	2	2015	949	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Giovani Bernard	0	0	0	0	31	\N	17	0	4.80	CIN	RB	\N	4.8	2	2015	2540156	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Matt Jones	0	0	0	0	9	\N	2	0	1.10	WAS	RB	\N	1.1	2	2015	2552635	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Sammie Coates	0	0	0	0	0	\N	8	0	0.80	PIT	WR	\N	0.8	2	2015	2552470	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Rex Burkhead	0	0	0	0	4	\N	2	0	0.60	CIN	RB	\N	0.6000000000000001	2	2015	2539265	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Fitzgerald Toussaint	0	0	0	0	0	\N	0	0	0.00	PIT	RB	\N	0	2	2015	2550738	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Bryan Walters	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	2	2015	2528181	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Matt Hazel	0	0	0	0	0	\N	0	0	0.00	MIA	WR	\N	0	2	2015	2543792	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Darrius Heyward-Bey	0	0	0	0	0	\N	0	0	0.00	PIT	WR	\N	0	2	2015	80427	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Tony Romo	317	3	1	0	4	\N	0	0	23.08	DAL	QB	\N	27.08	2	2015	2505354	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Tom Brady	291	2	1	0	3	\N	0	0	17.94	NE	QB	\N	18.94	2	2015	2504211	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Eli Manning	259	2	1	0	2	\N	0	0	16.56	NYG	QB	\N	17.56	2	2015	2505996	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Jared Abbrederis	0	0	0	0	0	\N	0	0	0.00	GB	WR	\N	0	2	2015	2543774	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Antonio Andrews	0	0	0	0	0	\N	0	0	0.00	TEN	RB	\N	0	2	2015	2550474	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Joe Banyard	0	0	0	0	0	\N	0	0	0.00	JAC	RB	\N	0	2	2015	2535696	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Jerome Felton	0	0	0	0	0	\N	0	0	0.00	BUF	RB	\N	0	2	2015	230	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Corey Fuller	0	0	0	0	0	\N	0	0	0.00	DET	WR	\N	0	2	2015	2540261	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Brittan Golden	0	0	0	0	0	\N	0	0	0.00	ARI	WR	\N	0	2	2015	2535988	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Konrad Reuland	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	2	2015	2530540	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Geoff Swaim	0	0	0	0	0	\N	0	0	0.00	DAL	TE	\N	0	2	2015	2553454	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Adam Thielen	0	0	0	0	0	\N	0	0	0.00	MIN	WR	\N	0	2	2015	2541785	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Chris Johnson	0	0	0	0	34	\N	10	0	4.40	ARI	RB	\N	4.4	2	2015	262	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Kenny Britt	0	0	0	0	1	\N	37	0	3.80	STL	WR	\N	3.8000000000000003	2	2015	71217	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Danny Amendola	0	0	0	0	0	\N	36	0	3.60	NE	WR	\N	3.6	2	2015	2649	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Benny Cunningham	0	0	0	0	21	\N	33	0	3.40	STL	RB	\N	3.4000000000000004	2	2015	2541544	0	0	1	@WAS	\N	\N	\N	\N	\N	\N	\N
Andre Johnson	0	0	0	0	0	\N	58	1	11.80	IND	WR	\N	11.8	2	2015	2505551	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Dwayne Allen	0	0	0	0	0	\N	25	0	2.50	IND	TE	\N	2.5	2	2015	2533046	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Jermaine Gresham	0	0	0	0	0	\N	21	0	2.10	ARI	TE	\N	2.1	2	2015	497238	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Tony Lippett	0	0	0	0	0	\N	0	0	0.00	MIA	WR	\N	0	2	2015	2552427	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
AJ McCarron	0	0	0	0	0	\N	0	0	0.00	CIN	QB	\N	0	2	2015	2543497	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Sean McGrath	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	2	2015	2535943	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Randall Cobb	0	0	0	0	7	\N	86	1	15.30	GB	WR	\N	15.3	2	2015	2495448	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
LeSean McCoy	0	0	0	1	67	\N	14	0	14.10	BUF	RB	\N	14.1	2	2015	79607	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Duron Carter	0	0	0	0	0	\N	0	0	0.00	IND	WR	\N	0	2	2015	2552616	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Kyle Juszczyk	0	0	0	0	0	\N	0	0	0.00	BAL	RB	\N	0	2	2015	2540230	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Dezmin Lewis	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	2	2015	2552411	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Austin Johnson	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	2	2015	2534437	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Tyler Eifert	0	0	0	0	0	\N	74	1	13.40	CIN	TE	\N	13.4	2	2015	2540148	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Tommy Bohanon	0	0	0	0	0	\N	0	0	0.00	NYJ	RB	\N	0	2	2015	2540225	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Jaron Brown	0	0	0	0	0	\N	0	0	0.00	ARI	WR	\N	0	2	2015	2541966	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Derrick Coleman	0	0	0	0	0	\N	0	0	0.00	SEA	RB	\N	0	2	2015	2534871	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
B.J. Daniels	0	0	0	0	0	\N	0	0	0.00	HOU	WR	\N	0	2	2015	2541429	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Travis Kelce	0	0	0	0	0	\N	52	1	11.20	KC	TE	\N	11.2	2	2015	2540258	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Michael Floyd	0	0	0	0	0	\N	48	1	10.80	ARI	WR	\N	10.8	2	2015	2532841	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Eddie Royal	0	0	0	0	0	\N	38	1	9.80	CHI	WR	\N	9.8	2	2015	1990	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Giants	\N	\N	\N	\N	\N	\N	\N	\N	1.00	\N	DST	\N	1	2	2015	100023	\N	\N	\N	ATL	1	0	0	0	0	0	24
Rashad Greene	0	0	0	0	0	\N	0	0	0.00	JAC	WR	\N	0	2	2015	2552425	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Ryan Mallett	0	0	0	0	0	\N	0	0	0.00	BAL	QB	\N	0	2	2015	2495443	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Matt Schaub	0	0	0	0	0	\N	0	0	0.00	BAL	QB	\N	0	2	2015	2505982	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Keith Mumphery	0	0	0	0	0	\N	0	0	0.00	HOU	WR	\N	0	2	2015	2552420	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Andre Debose	0	0	0	0	0	\N	0	0	0.00	OAK	WR	\N	0	2	2015	2553453	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Myles White	0	0	0	0	0	\N	0	0	0.00	NYG	WR	\N	0	2	2015	2541905	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
George Winn	0	0	0	0	0	\N	0	0	0.00	DET	RB	\N	0	2	2015	2541910	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Jermaine Kearse	0	0	0	0	1	\N	32	0	3.30	SEA	WR	\N	3.3000000000000003	2	2015	2532887	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Andre Roberts	0	0	0	0	1	\N	28	0	2.90	WAS	WR	\N	2.9	2	2015	497320	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Justin Hunter	0	0	0	0	0	\N	27	0	2.70	TEN	WR	\N	2.7	2	2015	2540151	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Charles Clay	0	0	0	0	0	\N	26	1	8.60	BUF	TE	\N	8.6	2	2015	2495139	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Damien Williams	0	0	0	0	9	\N	5	0	1.40	MIA	RB	\N	1.4	2	2015	2550512	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Josh Hill	0	0	0	0	0	\N	7	0	0.70	NO	TE	\N	0.7	2	2015	2541834	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Brian Tyms	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	2535522	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Corey Washington	0	0	0	0	0	\N	0	0	0.00	DET	WR	\N	0	2	2015	2550413	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Kenny Hilliard	0	0	0	0	0	\N	0	0	0.00	HOU	RB	\N	0	2	2015	2552476	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Matthew Stafford	301	2	1	0	6	\N	0	0	18.64	DET	QB	\N	22.64	2	2015	79860	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Cam Newton	211	2	1	0	32	\N	0	0	17.64	CAR	QB	\N	18.639999999999997	2	2015	2495455	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Teddy Bridgewater	213	2	1	0	15	\N	0	0	16.02	MIN	QB	\N	17.02	2	2015	2543465	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Mario Alford	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	2	2015	2552650	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Gary Barnidge	0	0	0	0	0	\N	0	0	0.00	CLE	TE	\N	0	2	2015	1060	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Kendall Gaskins	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	2	2015	2541778	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Brandon Pettigrew	0	0	0	0	0	\N	0	0	0.00	DET	TE	\N	0	2	2015	71431	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Darrin Reaves	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	2	2015	2550596	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Seth Roberts	0	0	0	0	0	\N	0	0	0.00	OAK	WR	\N	0	2	2015	2550597	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Brandon Tate	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	2	2015	81306	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Kenbrell Thompkins	0	0	0	0	0	\N	0	0	0.00	NYJ	WR	\N	0	2	2015	2539214	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Joique Bell	0	0	0	0	32	\N	9	0	4.10	DET	RB	\N	4.1000000000000005	2	2015	497145	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Jaelen Strong	0	0	0	0	0	\N	38	0	3.80	HOU	WR	\N	3.8	2	2015	2552463	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Corey Brown	0	0	0	0	4	\N	30	0	3.40	CAR	WR	\N	3.4	2	2015	2550546	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Justin Perillo	0	0	0	0	0	\N	0	0	0.00	GB	TE	\N	0	2	2015	2550243	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Richard Rodgers	0	0	0	0	0	\N	16	0	1.60	GB	TE	\N	1.6	2	2015	2550313	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Knile Davis	0	0	0	0	10	\N	4	0	1.40	KC	RB	\N	1.4	2	2015	2540178	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Texans	\N	\N	\N	\N	\N	\N	\N	\N	7.00	\N	DST	\N	6	2	2015	100013	\N	\N	\N	@CAR	2	1	1	0	0	0	21
Falcons	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100001	\N	\N	\N	@NYG	2	1	0	0	0	0	25
Jerome Simpson	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	2	2015	318	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Rod Smith	0	0	0	0	0	\N	0	0	0.00	DAL	RB	\N	0	2	2015	2553743	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Marshawn Lynch	0	0	0	1	48	\N	10	0	11.80	SEA	RB	\N	11.8	2	2015	2495663	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Vincent Jackson	0	0	0	0	0	\N	54	1	11.40	TB	WR	\N	11.4	2	2015	2506400	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Ka'Deem Carey	0	0	0	0	17	\N	7	0	2.40	CHI	RB	\N	2.4	2	2015	2543551	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Rob Housler	0	0	0	0	0	\N	21	0	2.10	CHI	TE	\N	2.1	2	2015	2508112	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Keshawn Martin	0	0	0	0	0	\N	0	0	0.00	NE	WR	\N	0	2	2015	2532900	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Jamaal Charles	0	0	0	1	72	\N	25	0	15.70	KC	RB	\N	15.7	2	2015	925	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
DeMarco Murray	0	0	0	1	71	\N	16	0	14.70	PHI	RB	\N	14.7	2	2015	2495207	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Keenan Allen	0	0	0	0	0	\N	78	1	13.80	SD	WR	\N	13.8	2	2015	2540154	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Jeremy Butler	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	2	2015	2550183	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Seantavius Jones	0	0	0	0	0	\N	0	0	0.00	NO	WR	\N	0	2	2015	2550359	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Jorvorskie Lane	0	0	0	0	0	\N	0	0	0.00	TB	RB	\N	0	2	2015	2536532	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Brett Hundley	0	0	0	0	0	\N	0	0	0.00	GB	QB	\N	0	2	2015	2552588	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Nic Jacobs	0	0	0	0	0	\N	0	0	0.00	JAC	TE	\N	0	2	2015	2550355	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Josh Johnson	0	0	0	0	0	\N	0	0	0.00	BUF	QB	\N	0	2	2015	264	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Jalen Saunders	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	2	2015	2543745	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Larry Fitzgerald	0	0	0	0	0	\N	76	1	13.60	ARI	WR	\N	13.6	2	2015	2506106	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Alfred Morris	0	0	0	1	65	\N	7	0	13.20	WAS	RB	\N	13.2	2	2015	2533457	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Jason Witten	0	0	0	0	0	\N	68	1	12.80	DAL	TE	\N	12.8	2	2015	2505629	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Josh Bellamy	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	2	2015	2535964	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Bryce Brown	0	0	0	0	0	\N	0	0	0.00	SEA	RB	\N	0	2	2015	2534796	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Greg Olsen	0	0	0	0	0	\N	49	1	10.90	CAR	TE	\N	10.9	2	2015	2495700	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Redskins	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100032	\N	\N	\N	STL	2	1	0	0	0	0	22
Garrett Grayson	0	0	0	0	0	\N	0	0	0.00	NO	QB	\N	0	2	2015	2552371	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Cory Harkey	0	0	0	0	0	\N	0	0	0.00	STL	TE	\N	0	2	2015	2533353	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Johnny Manziel	0	0	0	0	0	\N	0	0	0.00	CLE	QB	\N	0	2	2015	2543462	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Mark Sanchez	0	0	0	0	0	\N	0	0	0.00	PHI	QB	\N	0	2	2015	79858	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Louis Murphy	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	2	2015	80670	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Jack Doyle	0	0	0	0	0	\N	0	0	0.00	IND	TE	\N	0	2	2015	2540232	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Eric Weems	0	0	0	0	0	\N	0	0	0.00	ATL	WR	\N	0	2	2015	2507029	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Brandon Williams	0	0	0	0	0	\N	0	0	0.00	MIA	TE	\N	0	2	2015	2539645	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Tyrell Williams	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	2	2015	2553913	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Brent Celek	0	0	0	0	0	\N	32	0	3.20	PHI	TE	\N	3.2	2	2015	2507218	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Andre Williams	0	0	0	0	25	\N	4	0	2.90	NYG	RB	\N	2.9	2	2015	2543565	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Owen Daniels	0	0	0	0	0	\N	30	1	9.00	DEN	TE	\N	9	2	2015	2495825	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Lamar Miller	0	0	0	0	45	\N	9	0	5.40	MIA	RB	\N	5.4	2	2015	2533034	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Luke Willson	0	0	0	0	0	\N	14	0	1.40	SEA	TE	\N	1.4	2	2015	2541199	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Chris Conley	0	0	0	0	0	\N	7	0	0.70	KC	WR	\N	0.7	2	2015	2552652	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Will Tye	0	0	0	0	0	\N	0	0	0.00	NYG	TE	\N	0	2	2015	2553830	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Spencer Ware	0	0	0	0	0	\N	0	0	0.00	KC	RB	\N	0	2	2015	2540204	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Cooper Helfet	0	0	0	0	0	\N	0	0	0.00	SEA	TE	\N	0	2	2015	2536071	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Matt Ryan	312	2	1	0	7	\N	0	0	19.18	ATL	QB	\N	23.18	2	2015	310	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Alex Smith	243	2	1	0	14	\N	0	0	17.12	KC	QB	\N	18.119999999999997	2	2015	2506340	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
RaShaun Allen	0	0	0	0	0	\N	0	0	0.00	NO	TE	\N	0	2	2015	2550608	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Darren Fells	0	0	0	0	0	\N	0	0	0.00	ARI	TE	\N	0	2	2015	2540928	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Silas Redd	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	2	2015	2550529	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Luke Stocker	0	0	0	0	0	\N	0	0	0.00	TB	TE	\N	0	2	2015	2495234	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Jordan Taylor	0	0	0	0	0	\N	0	0	0.00	DEN	WR	\N	0	2	2015	2553606	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Fozzy Whittaker	0	0	0	0	30	\N	14	0	4.40	CAR	RB	\N	4.4	2	2015	2540037	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Jeremy Langford	0	0	0	0	24	\N	14	0	3.80	CHI	RB	\N	3.8	2	2015	2552379	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Terrance West	0	0	0	0	31	\N	5	0	3.60	BAL	RB	\N	3.6	2	2015	2543664	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Nick O'Leary	0	0	0	0	0	\N	0	0	0.00	BUF	TE	\N	0	2	2015	2552403	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Bear Pascoe	0	0	0	0	0	\N	0	0	0.00	DET	TE	\N	0	2	2015	79625	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Andre Holmes	0	0	0	0	0	\N	16	0	1.60	OAK	WR	\N	1.6	2	2015	2495453	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Chris Hogan	0	0	0	0	0	\N	15	0	1.50	BUF	WR	\N	1.5	2	2015	2530515	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Rams	\N	\N	\N	\N	\N	\N	\N	\N	8.00	\N	DST	\N	12	2	2015	100017	\N	\N	\N	@WAS	4	1	1	0	0	0	13
Jaguars	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	2	2015	100015	\N	\N	\N	MIA	2	1	0	0	0	0	18
T.J. Yates	0	0	0	0	0	\N	0	0	0.00	HOU	QB	\N	0	2	2015	2508123	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Alex Smith	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	2	2015	2506410	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Allen Robinson	0	0	0	0	0	\N	58	1	11.80	JAC	WR	\N	11.8	2	2015	2543509	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Tyrod Taylor	177	1	1	0	23	\N	0	0	11.38	BUF	QB	\N	12.379999999999999	2	2015	2495240	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Aaron Dobson	0	0	0	0	0	\N	24	0	2.40	NE	WR	\N	2.4	2	2015	2539256	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Duke Johnson	0	0	0	0	15	\N	6	0	2.10	CLE	RB	\N	2.1	2	2015	2552461	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Chris Matthews	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	2	2015	2531049	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Adrian Peterson	0	0	0	1	100	\N	17	0	15.70	MIN	RB	\N	18.7	2	2015	2507164	0	0	1	DET	\N	\N	\N	\N	\N	\N	\N
Jameis Winston	198	2	1	0	7	\N	0	0	14.62	TB	QB	\N	15.62	2	2015	2552033	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Brandin Cooks	0	0	0	0	8	\N	69	1	13.70	NO	WR	\N	13.7	2	2015	2543498	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Isaiah Burse	0	0	0	0	0	\N	0	0	0.00	SD	WR	\N	0	2	2015	2550182	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Derek Carrier	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	2	2015	2534241	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
T.J. Jones	0	0	0	0	0	\N	0	0	0.00	DET	WR	\N	0	2	2015	2543836	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Marcel Jensen	0	0	0	0	0	\N	0	0	0.00	WAS	TE	\N	0	2	2015	2550356	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Greg Salas	0	0	0	0	0	\N	0	0	0.00	BUF	WR	\N	0	2	2015	2495223	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Russell Shepard	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	2	2015	2541944	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Markus Wheaton	0	0	0	0	2	\N	70	1	13.20	PIT	WR	\N	13.2	2	2015	2539291	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Julian Edelman	0	0	0	0	2	\N	65	1	12.70	NE	WR	\N	12.7	2	2015	238498	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Kelvin Benjamin	0	0	0	0	0	\N	0	0	0.00	CAR	WR	\N	0	2	2015	2543471	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Nick Boyle	0	0	0	0	0	\N	0	0	0.00	BAL	TE	\N	0	2	2015	2552402	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Tyler Clutts	0	0	0	0	0	\N	0	0	0.00	DAL	RB	\N	0	2	2015	2508096	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Orleans Darkwa	0	0	0	0	0	\N	0	0	0.00	NYG	RB	\N	0	2	2015	2550481	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Shane Vereen	0	0	0	1	24	\N	28	0	11.20	NYG	RB	\N	11.2	2	2015	2495473	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Cole Beasley	0	0	0	0	0	\N	47	1	10.70	DAL	WR	\N	10.7	2	2015	2535698	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Cardinals	\N	\N	\N	\N	\N	\N	\N	\N	3.00	\N	DST	\N	3	2	2015	100026	\N	\N	\N	@CHI	1	1	0	0	0	0	22
Frankie Hammond	0	0	0	0	0	\N	0	0	0.00	KC	WR	\N	0	2	2015	2541828	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Matt Moore	0	0	0	0	0	\N	0	0	0.00	MIA	QB	\N	0	2	2015	2507282	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Tony Moeaki	0	0	0	0	0	\N	0	0	0.00	ATL	TE	\N	0	2	2015	497256	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Jordy Nelson	0	0	0	0	0	\N	0	0	0.00	GB	WR	\N	0	2	2015	1032	0	0	0	SEA	\N	\N	\N	\N	\N	\N	\N
Brandon Wegher	0	0	0	0	0	\N	0	0	0.00	CAR	RB	\N	0	2	2015	2553618	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Harry Douglas	0	0	0	0	0	\N	32	0	3.20	TEN	WR	\N	3.2	2	2015	222	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Coby Fleener	0	0	0	0	0	\N	35	1	9.50	IND	TE	\N	9.5	2	2015	2532838	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Christine Michael	0	0	0	1	27	\N	3	0	9.00	SEA	RB	\N	9	2	2015	2539322	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Carlos Hyde	0	0	0	0	45	\N	7	0	5.20	SF	RB	\N	5.2	2	2015	2543743	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Albert Wilson	0	0	0	0	0	\N	9	0	0.90	KC	WR	\N	0.9	2	2015	2550272	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Tyler Kroft	0	0	0	0	0	\N	7	0	0.70	CIN	TE	\N	0.7	2	2015	2552586	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Jarryd Hayne	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	2	2015	2553117	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Peyton Manning	311	2	1	0	-1	\N	0	0	18.34	DEN	QB	\N	22.339999999999996	2	2015	2501863	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Seyi Ajirotutu	0	0	0	0	0	\N	0	0	0.00	PHI	WR	\N	0	2	2015	497260	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Matt Flynn	0	0	0	0	0	\N	0	0	0.00	NO	QB	\N	0	2	2015	367	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Antonio Gates	0	0	0	0	0	\N	0	0	0.00	SD	TE	\N	0	2	2015	2505299	0	0	0	@CIN	\N	\N	\N	\N	\N	\N	\N
Chris Thompson	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	2	2015	2540011	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Robert Woods	0	0	0	0	0	\N	38	0	3.80	BUF	WR	\N	3.8	2	2015	2540169	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
James O'Shaughnessy	0	0	0	0	0	\N	0	0	0.00	KC	TE	\N	0	2	2015	2553318	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Rod Streater	0	0	0	0	0	\N	18	0	1.80	OAK	WR	\N	1.8	2	2015	2535869	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Devin Smith	0	0	0	0	0	\N	16	0	1.60	NYJ	WR	\N	1.6	2	2015	2553434	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Vikings	\N	\N	\N	\N	\N	\N	\N	\N	7.00	\N	DST	\N	7	2	2015	100020	\N	\N	\N	DET	3	1	1	0	0	0	23
Patriots	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	2	2015	100021	\N	\N	\N	@BUF	2	0	1	0	0	0	16
Lions	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100010	\N	\N	\N	@MIN	2	1	0	0	0	0	25
Zach Zenner	0	0	0	0	0	\N	0	0	0.00	DET	RB	\N	0	2	2015	2553631	0	0	0	@MIN	\N	\N	\N	\N	\N	\N	\N
Geno Smith	0	0	0	0	0	\N	0	0	0.00	NYJ	QB	\N	0	2	2015	2539335	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Bishop Sankey	0	0	0	1	49	\N	9	0	11.80	TEN	RB	\N	11.8	2	2015	2543682	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Mike Tolbert	0	0	0	0	12	\N	15	0	2.70	CAR	RB	\N	2.7	2	2015	2507428	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Nate Washington	0	0	0	0	0	\N	24	0	2.40	HOU	WR	\N	2.4	2	2015	2506313	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Jerick McKinnon	0	0	0	0	13	\N	8	0	2.10	MIN	RB	\N	2.1	2	2015	2543715	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Rishard Matthews	0	0	0	0	0	\N	0	0	0.00	MIA	WR	\N	0	2	2015	2532903	0	0	0	@JAC	\N	\N	\N	\N	\N	\N	\N
Cameron Meredith	0	0	0	0	0	\N	0	0	0.00	CHI	WR	\N	0	2	2015	2553568	0	0	0	ARI	\N	\N	\N	\N	\N	\N	\N
Jeremy Hill	0	0	0	1	79	\N	14	0	15.30	CIN	RB	\N	15.3	2	2015	2543603	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Jimmy Graham	0	0	0	0	0	\N	80	1	14.00	SEA	TE	\N	14	2	2015	497236	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Reggie Bush	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	2	2015	2506874	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Ben Koyack	0	0	0	0	0	\N	0	0	0.00	JAC	TE	\N	0	2	2015	2552396	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Adam Humphries	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	2	2015	2553895	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Steven Jackson	0	0	0	0	0	\N	0	0	0.00	NE	RB	\N	0	2	2015	2505936	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Gus Johnson	0	0	0	0	0	\N	0	0	0.00	ATL	RB	\N	0	2	2015	2553662	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Jeremy Ross	0	0	0	0	0	\N	0	0	0.00	OAK	WR	\N	0	2	2015	2530598	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Tom Savage	0	0	0	0	0	\N	0	0	0.00	HOU	QB	\N	0	2	2015	2543640	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Jaxon Shipley	0	0	0	0	0	\N	0	0	0.00	ARI	WR	\N	0	2	2015	2553599	0	0	0	@CHI	\N	\N	\N	\N	\N	\N	\N
Chris Ivory	0	0	0	1	62	\N	10	0	13.20	NYJ	RB	\N	13.2	2	2015	2507999	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Doug Martin	0	0	0	1	60	\N	9	0	12.90	TB	RB	\N	12.9	2	2015	2532899	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Frank Gore	0	0	0	1	53	\N	12	0	12.50	IND	RB	\N	12.5	2	2015	2506404	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Kapri Bibbs	0	0	0	0	0	\N	0	0	0.00	DEN	RB	\N	0	2	2015	2550542	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Da'Ron Brown	0	0	0	0	0	\N	0	0	0.00	KC	WR	\N	0	2	2015	2552479	0	0	0	DEN	\N	\N	\N	\N	\N	\N	\N
Jerome Cunningham	0	0	0	0	0	\N	0	0	0.00	NYG	TE	\N	0	2	2015	2551359	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Kyle Rudolph	0	0	0	0	0	\N	50	1	11.00	MIN	TE	\N	11	2	2015	2495438	0	0	0	DET	\N	\N	\N	\N	\N	\N	\N
Brandon Bolden	0	0	0	1	33	\N	10	0	10.30	NE	RB	\N	10.3	2	2015	2532797	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Derek Carr	190	1	1	0	4	\N	0	0	10.00	OAK	QB	\N	11	2	2015	2543499	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Buccaneers	\N	\N	\N	\N	\N	\N	\N	\N	2.00	\N	DST	\N	2	2	2015	100031	\N	\N	\N	@NO	1	1	0	0	0	0	31
Jonas Gray	0	0	0	0	0	\N	0	0	0.00	JAC	RB	\N	0	2	2015	2533030	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Xavier Grimble	0	0	0	0	0	\N	0	0	0.00	PIT	TE	\N	0	2	2015	2550521	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Case Keenum	0	0	0	0	0	\N	0	0	0.00	STL	QB	\N	0	2	2015	2532888	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Matt McGloin	0	0	0	0	0	\N	0	0	0.00	OAK	QB	\N	0	2	2015	2542065	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Bryce Petty	0	0	0	0	0	\N	0	0	0.00	NYJ	QB	\N	0	2	2015	2552369	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Brandon Weeden	0	0	0	0	0	\N	0	0	0.00	HOU	QB	\N	0	2	2015	2532970	0	0	0	@CAR	\N	\N	\N	\N	\N	\N	\N
Marlon Moore	0	0	0	0	0	\N	0	0	0.00	CLE	WR	\N	0	2	2015	2507902	0	0	0	TEN	\N	\N	\N	\N	\N	\N	\N
Ryan Nassib	0	0	0	0	0	\N	0	0	0.00	NYG	QB	\N	0	2	2015	2539961	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Mike Davis	0	0	0	0	0	\N	0	0	0.00	SF	RB	\N	0	2	2015	2553439	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Patrick DiMarco	0	0	0	0	0	\N	0	0	0.00	ATL	RB	\N	0	2	2015	2530763	0	0	0	@NYG	\N	\N	\N	\N	\N	\N	\N
Quincy Enunwa	0	0	0	0	0	\N	0	0	0.00	NYJ	WR	\N	0	2	2015	2543828	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Michael Williams	0	0	0	0	0	\N	0	0	0.00	NE	TE	\N	0	2	2015	2539197	0	0	0	@BUF	\N	\N	\N	\N	\N	\N	\N
Javorius Allen	0	0	0	0	21	\N	10	0	3.10	BAL	RB	\N	3.1	2	2015	2552631	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Dorial Green-Beckham	0	0	0	0	0	\N	28	0	2.80	TEN	WR	\N	2.8	2	2015	2552491	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Brian Quick	0	0	0	0	0	\N	33	1	9.30	STL	WR	\N	9.3	2	2015	2532933	0	0	0	@WAS	\N	\N	\N	\N	\N	\N	\N
Denard Robinson	0	0	0	0	47	\N	11	0	5.80	JAC	RB	\N	5.800000000000001	2	2015	2539260	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Chris Givens	0	0	0	0	0	\N	47	0	4.70	BAL	WR	\N	4.7	2	2015	2535634	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Khiry Robinson	0	0	0	0	9	\N	2	0	1.10	NO	RB	\N	1.1	2	2015	2542022	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Riley Cooper	0	0	0	0	0	\N	8	0	0.80	PHI	WR	\N	0.8	2	2015	497282	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Michael Campanaro	0	0	0	0	0	\N	6	0	0.60	BAL	WR	\N	0.6	2	2015	2550163	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
C.J. Uzomah	0	0	0	0	0	\N	0	0	0.00	CIN	TE	\N	0	2	2015	2552559	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
Alonzo Harris	0	0	0	0	0	\N	0	0	0.00	BAL	RB	\N	0	2	2015	2553533	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Dan Herron	0	0	0	0	0	\N	0	0	0.00	IND	RB	\N	0	2	2015	2532864	0	0	0	NYJ	\N	\N	\N	\N	\N	\N	\N
Tim Hightower	0	0	0	0	0	\N	0	0	0.00	NO	RB	\N	0	2	2015	4383	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Drew Brees	317	3	1	0	3	\N	0	0	22.98	NO	QB	\N	26.98	2	2015	2504775	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Odell Beckham	0	0	0	0	5	\N	114	1	17.90	NYG	WR	\N	20.9	2	2015	2543496	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Ben Roethlisberger	259	2	1	0	2	\N	0	0	16.56	PIT	QB	\N	17.56	2	2015	2506109	0	0	0	SF	\N	\N	\N	\N	\N	\N	\N
Kamar Aiken	0	0	0	0	0	\N	0	0	0.00	BAL	WR	\N	0	2	2015	2530660	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
David Ausberry	0	0	0	0	0	\N	0	0	0.00	OAK	TE	\N	0	2	2015	2499277	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Kenjon Barner	0	0	0	0	0	\N	0	0	0.00	PHI	RB	\N	0	2	2015	2539289	0	0	0	DAL	\N	\N	\N	\N	\N	\N	\N
Jalston Fowler	0	0	0	0	0	\N	0	0	0.00	TEN	RB	\N	0	2	2015	2552464	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Thomas Rawls	0	0	0	0	0	\N	0	0	0.00	SEA	RB	\N	0	2	2015	2553733	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Zach Sudfeld	0	0	0	0	0	\N	0	0	0.00	NYJ	TE	\N	0	2	2015	2541151	0	0	0	@IND	\N	\N	\N	\N	\N	\N	\N
Marcedes Lewis	0	0	0	0	0	\N	43	0	4.30	JAC	TE	\N	4.3	2	2015	2495888	0	0	0	MIA	\N	\N	\N	\N	\N	\N	\N
Marlon Brown	0	0	0	0	0	\N	36	0	3.60	BAL	WR	\N	3.6	2	2015	2540005	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Leonard Hankerson	0	0	0	0	0	\N	16	0	1.60	BUF	WR	\N	1.6	2	2015	2495158	0	0	0	NE	\N	\N	\N	\N	\N	\N	\N
Anthony Fasano	0	0	0	0	0	\N	15	0	1.50	TEN	TE	\N	1.5	2	2015	2495835	0	0	0	@CLE	\N	\N	\N	\N	\N	\N	\N
Dwayne Harris	0	0	0	0	0	\N	14	0	1.40	NYG	WR	\N	1.4	2	2015	2495159	0	0	0	ATL	\N	\N	\N	\N	\N	\N	\N
Eagles	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	2	2015	100025	\N	\N	\N	DAL	2	1	1	0	0	0	32
Bears	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	3	2	2015	100005	\N	\N	\N	ARI	2	1	0	0	0	0	28
Ted Ginn	0	0	0	0	0	\N	16	0	1.60	CAR	WR	\N	1.6	2	2015	2507166	0	0	0	HOU	\N	\N	\N	\N	\N	\N	\N
Benjamin Watson	0	0	0	0	0	\N	16	0	1.60	NO	TE	\N	1.6	2	2015	2506122	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
Crockett Gillmore	0	0	0	0	0	\N	14	0	1.40	BAL	TE	\N	1.4	2	2015	2543794	0	0	0	@OAK	\N	\N	\N	\N	\N	\N	\N
Colts	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	5	2	2015	100014	\N	\N	\N	NYJ	2	1	0	0	0	0	20
49ers	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	3	2	2015	100029	\N	\N	\N	@PIT	1	1	0	0	0	0	21
Raiders	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100018	\N	\N	\N	BAL	2	1	0	0	0	0	22
Jameill Showers	0	0	0	0	0	\N	0	0	0.00	DAL	QB	\N	0	2	2015	2553600	0	0	0	@PHI	\N	\N	\N	\N	\N	\N	\N
Kevin Smith	0	0	0	0	0	\N	0	0	0.00	SEA	WR	\N	0	2	2015	2550733	0	0	0	@GB	\N	\N	\N	\N	\N	\N	\N
Bengals	\N	\N	\N	\N	\N	\N	\N	\N	7.00	\N	DST	\N	7	2	2015	100006	\N	\N	\N	SD	3	1	1	0	0	0	23
Packers	\N	\N	\N	\N	\N	\N	\N	\N	5.00	\N	DST	\N	4	2	2015	100011	\N	\N	\N	SEA	2	1	0	0	0	0	21
Jets	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100024	\N	\N	\N	@IND	3	1	0	0	0	0	32
Darrel Young	0	0	0	0	0	\N	0	0	0.00	WAS	RB	\N	0	2	2015	2507737	0	0	0	STL	\N	\N	\N	\N	\N	\N	\N
Lee Smith	0	0	0	0	0	\N	0	0	0.00	OAK	TE	\N	0	2	2015	2495347	0	0	0	BAL	\N	\N	\N	\N	\N	\N	\N
Browns	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100007	\N	\N	\N	TEN	2	1	0	0	0	0	25
James Wright	0	0	0	0	0	\N	0	0	0.00	CIN	WR	\N	0	2	2015	2550162	0	0	0	SD	\N	\N	\N	\N	\N	\N	\N
DeAndre Smelter	0	0	0	0	0	\N	0	0	0.00	SF	WR	\N	0	2	2015	2552627	0	0	0	@PIT	\N	\N	\N	\N	\N	\N	\N
Evan Spencer	0	0	0	0	0	\N	0	0	0.00	TB	WR	\N	0	2	2015	2553289	0	0	0	@NO	\N	\N	\N	\N	\N	\N	\N
Ravens	\N	\N	\N	\N	\N	\N	\N	\N	4.00	\N	DST	\N	4	2	2015	100002	\N	\N	\N	@OAK	1	1	0	0	0	0	17
Trevor Siemian	0	0	0	0	0	\N	0	0	0.00	DEN	QB	\N	0	2	2015	2553457	0	0	0	@KC	\N	\N	\N	\N	\N	\N	\N
Willie Snead	0	0	0	0	0	\N	0	0	0.00	NO	WR	\N	0	2	2015	2550256	0	0	0	TB	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: saved_lineups; Type: TABLE DATA; Schema: public; Owner: Raymond
--

COPY saved_lineups (qb, rb1, rb2, wr1, wr2, wr3, te, dst, flex, user_id, week, year, created) FROM stdin;
396886	1691422	520386	1272524	1623794	1824823	1995451	1905	1272852	1	1	2015	2016-04-14 20:27:41.752594
235197	1984260	1880820	1824823	1623794	1114965	1725130	1913	1272852	1	1	2015	2016-04-14 20:56:32.684504
235197	1984260	1880820	1824823	1623794	1114965	1725130	1913	1272852	1	1	2015	2016-04-17 13:26:58.184471
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: Raymond
--

COPY users (id, fname, lname, email, google_token, google_id, fb_id, fb_token, display_name, username, pw) FROM stdin;
1	\N	\N	\N	\N	\N	\N	\N	\N	admin	$2a$10$RopqD9yATkEDADBm8EU8s.FNJTXM6bQHSebegtmPuBlA1avfaxVeG
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Raymond
--

SELECT pg_catalog.setval('users_id_seq', 2, false);


--
-- Name: public; Type: ACL; Schema: -; Owner: Raymond
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM "Raymond";
GRANT ALL ON SCHEMA public TO "Raymond";
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

