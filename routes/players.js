//API KEY: f45euk7vacg8
var express = require('express');
var router = express.Router();
var request = require('request');
var pg = require('../config/dbConnection');
var fs = require('fs');

// SELECT *, (dk_total / salary) as dk_value FROM dk LEFT OUTER JOIN cbs_players AS c ON (c.full_name = dk.name AND c.position = dk.position) LEFT OUTER JOIN espn_projections AS e ON (dk.name like '%' || e.name || '%') WHERE dk.week = 1 AND e.week = 1 AND dk.year = 2016 AND e.year = 2016 ORDER BY dk_value DESC

router.get('/list', function (req, res) {
    var query;
    if (req.query.salaries === 'DK' && req.query.projections === 'ESPN')
        query = "SELECT *, avgpointspergame as dk_total, (avgpointspergame/salary) as dk_value FROM dk LEFT OUTER JOIN cbs_players AS c ON (c.full_name = dk.name AND c.position = dk.position) WHERE dk.week = $1 AND dk.year = $2 ORDER BY dk_value";
    else if (req.query.salaries === 'DK' && req.query.projections === 'NFL')
        query = "SELECT *, (dk_total / salary) as dk_value FROM dk LEFT OUTER JOIN cbs_players AS c ON (c.full_name = dk.name AND c.position = dk.position) LEFT OUTER JOIN nfl_projections AS n ON (c.full_name = n.name and dk.position = n.position) WHERE dk.week = $1 AND n.week = $1 AND dk.year = $2 AND n.year = $2 ORDER BY dk_value DESC";

    pg.query(query, [getWeek(), getYear()], function (err, response) {
        if (err)
            res.send(500);
        else {
            console.log(response.rows.length, response.rowCount);
            res.json(response.rows);
        }
    });
});

router
    .route('/lineups')
    .get(function (req, res) {
        pg.query('SELECT count(*), qb, rb1, rb2, wr1, wr2, wr3, te, flex, dst FROM saved_lineups WHERE user_id = $1 and week = $2 and year = $3 group by qb, rb1, rb2, wr1, wr2, wr3, te, flex, dst', [req.user.id, getWeek(), getYear()], function (err, response) {
            pg.query('SELECT * FROM dk', null, function (dataErr, dataRes) {
                var mappedData = {};
                for (var i in dataRes.rows) {
                    mappedData[dataRes.rows[i].id] = dataRes.rows[i];
                }
                res.json({lineups: response.rows, data: mappedData});
            });
        });
    })
    .post(function (req, res) {
        pg.query('INSERT INTO saved_lineups (qb, rb1, rb2, wr1, wr2, wr3, te, flex, dst, user_id, week, year) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)', [req.body.qb.id, req.body.rb1.id, req.body.rb2.id, req.body.wr1.id, req.body.wr2.id, req.body.wr3.id, req.body.te.id, req.body.flex.id, req.body.dst.id, req.user.id, getWeek(), getYear()], function (err, response) {
            if (!err)
                res.send(200);
            else
                res.send(500);
        });
    });

router.post('/dk_data', function (req, res) {
    var path = "./data/dk_" + getWeek() + "_" + getYear() + ".csv";
    fs.writeFile(path, req.body.data, function (err) {
        if (err) {
            res.send(500, err);
        }
    });
});

router.get('/news', function (req, res) {
    var query = 'SELECT * FROM fantasy_headlines ORDER BY date DESC';

    if (req.query.limit)
        query += ' LIMIT ' + req.query.limit;

    pg.query(query, null, function (err, result) {
        if (!err)
            res.json(result.rows);
        else
            res.status(500).send();
    })
});

router.get('/story/:id', function (req, res) {
    pg.query('SELECT * FROM fantasy_stories as s JOIN fantasy_headlines as h ON (s.id = h.id) WHERE s.id = $1', [req.params.id], function (err, result) {
        if (!err && result.rowCount)
            res.json(result.rows[0]);
        else {
            var url = 'http://api.cbssports.com/fantasy/news/story?version=3.0&SPORT=football&response_format=json&id=' + req.params.id;

            request(url, function (error, response, body) {
                if (!error) {
                    var json = JSON.parse(body);
                    var story = json.body.fantasy_story;

                    pg.query('INSERT INTO fantasy_stories (id, story_body) VALUES ($1, $2) RETURNING *', [story.id, story.story_body], function (storyErr, storyResult) {
                        if (!storyErr) {
                            pg.query('SELECT * FROM fantasy_stories as s JOIN fantasy_headlines as h ON (s.id = h.id) WHERE s.id = $1', [req.params.id], function (newStoryErr, newStoryResult) {
                                if (!newStoryErr)
                                    res.json(newStoryResult.rows[0]);
                                else
                                    res.status(500).send();
                            });
                        }
                        else
                            res.status(500).send();
                    })
                } else {
                    console.log("We’ve encountered an error: " + error);
                }
            });
        }
    });
});

// Date.prototype.getNFLWeek = function() {
// 	var date = new Date(this.getTime());
// 	date.setHours(0, 0, 0, 0);
// 	// Thursday in current week decides the year.
// 	date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
// 	// January 4 is always in week 1.
// 	var week1 = new Date(date.getFullYear(), 0, 4);
// 	// Adjust to Thursday in week 1 and count number of weeks from date to week1.
// 	return (1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
// 			- 3 + (week1.getDay() + 6) % 7) / 7)) - 35;
// };

Date.prototype.getNFLWeek = function () {
    // The number of milliseconds in one week
    var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
    // Convert both dates to milliseconds
    var date1_ms = this.getTime();
    var date2_ms = new Date('08-29-16'); // 1 week before first monday of NFL season
    // Calculate the difference in milliseconds
    var difference_ms = date1_ms - date2_ms;
    // Convert back to weeks and return hole weeks
    return Math.floor(difference_ms / ONE_WEEK);
};

function getWeek() {
    return Math.max(new Date().getNFLWeek(), 1);
}

function getYear() {
    return new Date().getFullYear();
}

module.exports = router;
