var express = require('express');
var router = express.Router();
var pg = require('../config/dbConnection');
var passport = require('passport');
require('../config/passport')(passport);

router.get('/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
// the callback after google has authenticated the user
router.get('/google/callback',
    passport.authenticate('google', {
            successRedirect : '/#/lineup',
            failureRedirect : '/'
    }));

router.get('/facebook', passport.authenticate('facebook', { scope : ['email', 'public_profile'] }));
router.get('/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/#/lineup',
            failureRedirect : '/'
        }));

// process the signup form
router.post('/signup', passport.authenticate('local-signup'), function (req, res) {
    res.send(200);
});

router.post('/login', passport.authenticate('local-login'), function (req, res) {
    res.send(200);
});

router.get('/check', function (req, res) {
	if (req.isAuthenticated())
		res.json({ name: req.user.display_name || req.user.username });
    else
    	res.send(false);
});


router.get('/signout', function (req, res) {
	req.logout();
	res.redirect('/');
});

module.exports = router;
