var pg = require('../config/dbConnection');

var request = require("request"),
    cheerio = require("cheerio");

function getEspnProjections() {
    for (var i = 0; i <= 25; i++) {
        var url = "http://games.espn.go.com/ffl/tools/projections?&scoringPeriodId=1&startIndex=" + i * 40;

        request(url, function (error, response, body) {
            if (!error) {
                var $ = cheerio.load(body);
                var players = $(".pncPlayerRow");

                $('.pncPlayerRow').each(function () {
                    var tr = $(this);

                    var td = tr.children().eq(0).text();
                    var id = tr.children().eq(0).attr('id');
                    var id_number = id.slice(id.indexOf('_') + 1);
                    var data = td.split(',')[1] || td;

                    var team = data.split(String.fromCharCode(160))[0].trim();
                    var name = td.split(',')[0];
                    var pos = data.split(String.fromCharCode(160))[1];
                    if (pos === 'D/ST') {
                        name = team.split(' ')[0];
                        pos = 'DST';
                    }
                    var status = data.split(String.fromCharCode(160))[3] || '';

                    var p = {
                        id: id_number,
                        name: name,
                        team: team.length <= 3 ? team.toUpperCase() : '',
                        pos: pos,
                        status: status,
                        p_c: tr.children().eq(3).text().split('/')[0] === '--' ? 0 : tr.children().eq(3).text().split('/')[0],
                        p_a: tr.children().eq(3).text().split('/')[1] === '--' ? 0 : tr.children().eq(3).text().split('/')[1],
                        p_yds: tr.children().eq(4).text() === '--' ? 0 : tr.children().eq(4).text(),
                        p_tds: tr.children().eq(5).text() === '--' ? 0 : tr.children().eq(5).text(),
                        p_ints: tr.children().eq(6).text() === '--' ? 0 : tr.children().eq(6).text(),
                        r_avg: tr.children().eq(7).text() === '--' ? 0 : tr.children().eq(7).text(),
                        r_yds: tr.children().eq(8).text() === '--' ? 0 : tr.children().eq(8).text(),
                        r_tds: tr.children().eq(9).text() === '--' ? 0 : tr.children().eq(9).text(),
                        rec_catches: tr.children().eq(10).text() === '--' ? 0 : tr.children().eq(10).text(),
                        rec_yds: tr.children().eq(11).text() === '--' ? 0 : tr.children().eq(11).text(),
                        rec_tds: tr.children().eq(12).text() === '--' ? 0 : tr.children().eq(12).text(),
                        total: tr.children().eq(13).text() === '--' ? 0 : tr.children().eq(13).text() || 0
                    };

                    upsertESPNPlayer(p);
                });

            } else {
                console.log("We’ve encountered an error: " + error);
            }
        });

    }
}

function upsertESPNPlayer(p) {
    pg.query('SELECT EXISTS(SELECT 1 FROM espn_projections WHERE week = $1 and year = $2 and espn_id = $3)', [getWeek(), getYear(), p.id], function (err, result) {
        if (result.rows[0].exists) {
            pg.query('UPDATE espn_projections SET name = $1, p_c = $2, p_a = $3, p_yds = $4, p_tds = $5, p_ints = $6, r_avg = $7, r_yds = $8, r_tds = $9, rec_catces = $10, rec_yds = $11, rec_tds = $12, total = $13, team = $14, position = $15, status = $16, dk_total = $17 WHERE espn_id = $18', [p.name, p.p_c, p.p_a, p.p_yds, p.p_tds, p.p_ints, p.r_avg, p.r_yds, p.r_tds, p.rec_catches, p.rec_yds, p.rec_tds, p.total, p.team, p.pos, p.status, draftKingsCalculation(p), p.id], function (err, result) {
                if (err)
                    console.log('error', err);
            });
        }
        else {
            pg.query('INSERT INTO espn_projections (espn_id, name, p_c, p_a, p_yds, p_tds, p_ints, r_avg, r_yds, r_tds, rec_catches, rec_yds, rec_tds, total, team, position, status, dk_total, week, year) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20)', [p.id, p.name, p.p_c, p.p_a, p.p_yds, p.p_tds, p.p_ints, p.r_avg, p.r_yds, p.r_tds, p.rec_catches, p.rec_yds, p.rec_tds, p.total, p.team, p.pos, p.status, draftKingsCalculation(p), getWeek(), getYear()], function (err, response) {
                if (err)
                    console.log('error', err);
            });
        }
    })
}

function getNFLProjections() {
    for (var i = 0; i < 34; i++) {
        var url = 'http://fantasy.nfl.com/research/projections?offset=' + i * 26 + '&position=O&sort=projectedPts&statCategory=projectedStats&statSeason=' + getYear() + '&statType=weekProjectedStats&statWeek=' + getWeek();

        request(url, function (error, response, body) {
            if (!error) {
                var $ = cheerio.load(body);

                $('tbody tr').each(function () {
                    var tr = $(this);

                    var posAndTeam = tr.find('em').text();

                    var p = {
                        id: tr.attr('class').split(' ')[0].split('-')[1],
                        name: tr.find('.playerName').text(),
                        pos: posAndTeam.split(' - ')[0],
                        team: posAndTeam.split(' - ')[1],
                        opp: tr.find('.playerOpponent').text(),
                        p_yds: tr.find('.stat.stat_5').text() === '-' ? 0 : tr.find('.stat.stat_5').text(),
                        p_tds: tr.find('.stat.stat_6').text() === '-' ? 0 : tr.find('.stat.stat_6').text(),
                        p_ints: tr.find('.stat.stat_7').text() === '-' ? 0 : tr.find('.stat.stat_7').text(),
                        r_yds: tr.find('.stat.stat_14').text() === '-' ? 0 : tr.find('.stat.stat_14').text(),
                        r_tds: tr.find('.stat.stat_15').text() === '-' ? 0 : tr.find('.stat.stat_15').text(),
                        rec_yds: tr.find('.stat.stat_21').text() === '-' ? 0 : tr.find('.stat.stat_21').text(),
                        rec_tds: tr.find('.stat.stat_22').text() === '-' ? 0 : tr.find('.stat.stat_22').text(),
                        fum_td: tr.find('.stat.stat_29').text() === '-' ? 0 : tr.find('.stat.stat_29').text(),
                        tpc: tr.find('.stat.stat_32').text() === '-' ? 0 : tr.find('.stat.stat_32').text(),
                        fum_lost: tr.find('.stat.stat_30').text() === '-' ? 0 : tr.find('.stat.stat_30').text(),
                        total: tr.find('.stat.projected').text() === '-' ? 0 : tr.find('.stat.projected').text()
                    };

                    upsertNFLPlayer(p);
                });
            } else {
                console.log("We’ve encountered an error: " + error);
            }
        });

    }
    for (var j = 0; j < 2; j++) {
        var url = 'http://fantasy.nfl.com/research/projections?offset=' + j * 25 + '&position=8&statCategory=projectedStats&statSeason=' + getYear() + '&statType=weekProjectedStats&statWeek=' + getWeek();

        request(url, function (error, response, body) {
            if (!error) {
                var $ = cheerio.load(body);

                $('tbody tr').each(function () {
                    var tr = $(this);

                    var defName = tr.find('.playerName').text().split(' ');
                    defName = defName[defName.length - 1];

                    var p = {
                        id: tr.attr('class').split(' ')[0].split('-')[1],
                        name: defName,
                        pos: 'DST',
                        opp: tr.find('.playerOpponent').text(),
                        sacks: tr.find('.stat.stat_45').text() === '-' ? 0 : tr.find('.stat.stat_45').text(),
                        ints: tr.find('.stat.stat_46').text() === '-' ? 0 : tr.find('.stat.stat_46').text(),
                        fum_rec: tr.find('.stat.stat_47').text() === '-' ? 0 : tr.find('.stat.stat_47').text(),
                        safety: tr.find('.stat.stat_49').text() === '-' ? 0 : tr.find('.stat.stat_49').text(),
                        td: tr.find('.stat.stat_50').text() === '-' ? 0 : tr.find('.stat.stat_50').text(),
                        ret: tr.find('.stat.stat_53').text() === '-' ? 0 : tr.find('.stat.stat_53').text(),
                        pts_allow: tr.find('.stat.stat_54').text() === '-' ? 0 : tr.find('.stat.stat_54').text(),
                        total: tr.find('.stat.projected').text()
                    };

                    upsertNFLDef(p);
                });
            } else {
                console.log("We’ve encountered an error: " + error);
            }
        });
    }
}

function upsertNFLPlayer(p) {
    pg.query('SELECT EXISTS(SELECT 1 FROM nfl_projections WHERE week = $1 and year = $2 and nfl_id = $3)', [getWeek(), getYear(), p.id], function (err, result) {
        if (result.rows[0].exists) {
            pg.query('UPDATE nfl_projections SET name = $1, position = $2, team = $3, opp = $4, p_yds = $5, p_tds = $6, p_ints = $7, r_yds = $8, r_tds = $9, rec_yds = $10, rec_tds = $11, fum_td = $12, tpc = $13, fum_lost = $14, total = $15, dk_total = $16 WHERE nfl_id = $17', [p.name, p.pos, p.team, p.opp, p.p_yds, p.p_tds, p.p_ints, p.r_yds, p.r_tds, p.rec_yds, p.rec_tds, p.fum_td, p.tpc, p.fum_lost, p.total, draftKingsCalculation(p), p.id], function (err, result) {
                if (err)
                    console.log('error', err);
            });
        }
        else {
            pg.query('INSERT INTO nfl_projections (nfl_id, name, position, team, opp, p_yds, p_tds, p_ints, r_yds, r_tds, rec_yds, rec_tds, fum_td, tpc, fum_lost, total, dk_total, week, year) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19)', [p.id, p.name, p.pos, p.team, p.opp, p.p_yds, p.p_tds, p.p_ints, p.r_yds, p.r_tds, p.rec_yds, p.rec_tds, p.fum_td, p.tpc, p.fum_lost, p.total, draftKingsCalculation(p), getWeek(), getYear()], function (err, result) {
                if (err)
                    console.log('error', err);
            });
        }
    });
}

function upsertNFLDef(p) {
    pg.query('SELECT EXISTS(SELECT 1 FROM nfl_projections WHERE week = $1 and year = $2 and nfl_id = $3)', [getWeek(), getYear(), p.id], function (err, result) {
        if (result.rows[0].exists) {
            pg.query('UPDATE nfl_projections SET name = $1, position = $2, opp = $3, sacks = $4, ints = $5, fum_rec = $6, safety = $7, td = $8, ret = $9, pts_allow = $10, total = $11, dk_total = $12 WHERE nfl_id = $13', [p.name, p.pos, p.opp, p.sacks, p.ints, p.fum_rec, p.safety, p.td, p.ret, p.pts_allow, p.total, draftKingsCalculation(p, true), p.id], function (err, result) {
                if (err)
                    console.log('error', err);
            });
        }
        else {
            pg.query('INSERT INTO nfl_projections (nfl_id, name, position, opp, sacks, ints, fum_rec, safety, td, ret, pts_allow, total, dk_total, week, year) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)', [p.id, p.name, p.pos, p.opp, p.sacks, p.ints, p.fum_rec, p.safety, p.td, p.ret, p.pts_allow, p.total, draftKingsCalculation(p, true), getWeek(), getYear()], function (err, result) {
                if (err)
                    console.log('error', err);
            });
        }
    });

}

// getting player data from ffn api
function getFFNPlayers() {
    var url = 'http://www.fantasyfootballnerd.com/service/players/json/f45euk7vacg8/';

    request(url, function (error, response, body) {
        if (!error) {
            var json = JSON.parse(body);
            for (var i = 0, len = json.Players.length; i < len; i++) {
                pg.query('insert into ffn_players (ffn_id, active, jersey, fname, lname, full_name, team, position, height, weight, dob, college) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)', [parseInt(json.Players[i].playerId), Boolean(parseInt(json.Players[i].active)), json.Players[i].jersey, json.Players[i].fname, json.Players[i].lname, json.Players[i].displayName, json.Players[i].team, json.Players[i].position, json.Players[i].height, json.Players[i].weight, json.Players[i].dob, json.Players[i].college], function (err, response) {
                    if (err)
                        console.log('Err', err);
                    else
                        console.log(response);
                });
            }
        } else {
            console.log("We’ve encountered an error: " + error);
        }
    });
}

// getting player data fro cbs api
function getCBSPlayers() {
    var url = 'http://api.cbssports.com/fantasy/players/list?version=3.0&SPORT=football&response_format=json';

    request(url, function (error, response, body) {
        if (!error) {
            var json = JSON.parse(body);
            var players = json.body.players;
            players.map(function (elem) {
                upsertCBSPlayer(elem);
            });
        } else {
            console.log("We’ve encountered an error: " + error);
        }
    });
}

function upsertCBSPlayer(player) {
    pg.query('SELECT EXISTS(SELECT 1 FROM cbs_players WHERE cbs_id = $1)', [player.id], function (err, response) {
        if (response.rows[0].exists) {
            pg.query('UPDATE cbs_players set team = $1, full_name = $2, status = $3, fname = $4, lname = $5, position = $6, photo = $7, jersey = $8, bye_weeks = $9, age = $10, elias_id = $11 WHERE cbs_id = $12', [player.pro_team, player.fullname, player.pro_status, player.firstname, player.lastname, player.position, player.photo, player.jersey, player.bye_week, parseInt(player.age) || 0, player.elias_id, player.id], function (updateErr, updateRes) {
                if (updateErr)
                    console.log(updateErr);
            });
        }
        else {
            pg.query('INSERT INTO cbs_players (cbs_id, team, full_name, status, fname, lname, position, photo, jersey, bye_weeks, age, elias_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)', [player.id, player.pro_team, player.fullname, player.pro_status, player.firstname, player.lastname, player.position, player.photo, player.jersey, player.bye_week, parseInt(player.age) || 0, player.elias_id], function (insertErr, insertRes) {
                if (insertErr)
                    console.log(insertErr);
            });
        }
    });
}

function draftKingsCalculation(player, def) {
    var total = 0;

    if (!def) {
        // passing
        total += parseFloat(player.p_yds) / 25; // passing yards
        total += parseFloat(player.p_tds) * 4; // passing tds
        total += parseFloat(player.p_ints) * -1; // interception
        if (parseFloat(player.p_yds) >= 300) // 300+ pass yards bonus
            total += 3;
        // rushing
        total += parseFloat(player.r_yds) / 10; // rushing yards
        total += parseFloat(player.r_tds) * 6; // rushing tds
        if (parseFloat(player.r_yds) >= 100) // 100+ rushing yards bonus
            total += 3;
        // receiving
        total += parseFloat(player.rec_catches) || 0; // catches
        total += parseFloat(player.rec_yds) / 10; // receiving yards
        total += parseFloat(player.rec_tds) * 6; // receiving tds
        if (parseFloat(player.rec_yds) >= 100) // 100+ receiving yards bonus
            total += 3;

        // misc
        total += parseFloat(player.fum_td) * 6 || 0;
        total += parseFloat(player.tpc) * 2 || 0;
        total += parseFloat(player.fum_lost) * -2 || 0;

        return parseFloat(Math.max(total, player.total));
    }
    else {
        total += parseFloat(player.sacks);
        total += parseFloat(player.ints) * 2;
        total += parseFloat(player.fum_rec) * 2;
        total += parseFloat(player.safety) * 2;
        total += parseFloat(player.ret) * 6;
        total += parseFloat(player.td) * 6;

        var allowed = parseInt(player.pts_allow);
        if (allowed === 0)
            total += 10;
        else if (allowed >= 1 && allowed <= 6)
            total += 7;
        else if (allowed >= 7 && allowed <= 13)
            total += 4;
        else if (allowed >= 14 && allowed <= 20)
            total += 1;
        else if (allowed >= 21 && allowed <= 27)
            total += 0;
        else if (allowed >= 28 && allowed <= 34)
            total += -1;
        else if (allowed >= 35)
            total += -4;

        return total;
    }
}

function getPlayerNews() {
    var url = 'http://api.cbssports.com/fantasy/news/headlines?version=3.0&SPORT=football&response_format=json&limit=20&offset=0';

    request(url, function (error, response, body) {
        if (!error) {
            var json = JSON.parse(body);
            var news = json.body.fantasy_headlines;
            var success = 0;

            news.map(function (elem) {
                pg.query('INSERT INTO fantasy_headlines (id, author, title, synopsis, date, photos) VALUES ($1, $2, $3, $4, $5, $6)', [elem.id, elem.author, elem.title, elem.synopsis, new Date(elem.timestamp * 1000), JSON.stringify(elem.photos)], function (newsErr, result) {
                    if (!newsErr) {
                        success++;
                        console.log('News insert success');
                        if (success % 20 === 0)
                            getPlayerNews(offset + 20);
                    }
                    else {
                        console.log('reached old stories');
                    }
                })
            });
        } else {
            console.log("We’ve encountered an error: " + error);
        }
    });
}

Date.prototype.getNFLWeek = function () {
    // The number of milliseconds in one week
    var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
    // Convert both dates to milliseconds
    var date1_ms = this.getTime();
    var date2_ms = new Date('08-29-16'); // 1 week before first monday of NFL season
    // Calculate the difference in milliseconds
    var difference_ms = date1_ms - date2_ms;
    // Convert back to weeks and return hole weeks
    return Math.floor(difference_ms / ONE_WEEK);
};

function getWeek() {
    return Math.max(new Date().getNFLWeek(), 1);
}

function getYear() {
    return new Date().getFullYear();
}

getCBSPlayers();
// getEspnProjections();
// getNFLProjections();
//getPlayerNews();